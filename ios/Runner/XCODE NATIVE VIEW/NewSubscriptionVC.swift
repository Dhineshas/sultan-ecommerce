//
//  NewSubscriptionVC.swift
//  Runner
//
//  Created by sultan Al-Kuwari on 18/09/2022.
//

import Foundation
import UIKit
import StoreKit

class NewSubscriptionVC: UIViewController,SKPaymentTransactionObserver
{
    var result: FlutterResult?
    var dataFromFlutter : [String : AnyObject]!
    
    let storyBoard  = UIStoryboard(name: "Main", bundle: nil)
    
    var productList = [ "com.expresspos.standard.Non_Renewing_Subscription" , "com.expresspos.premium.Auto_Renewing_Subscription"]
    
    var skProductList = [SKProduct]()
    var request: SKProductsRequest!
    var latestRecieptArray = [LatestRecieptInfoDataArray]()//array for details fetching from device storage of appstore account holder's all purchases
    var activeSubPlan = -1
    var standardRemainingDays = -1
    var premiumRemainingDays = -1
    
    var planStandardDescription = NSMutableAttributedString()
    var planPremiumDescription = NSMutableAttributedString()
    
    
    
    @IBOutlet weak var subListTableVw: UITableView!
    @IBOutlet weak var subListCollecVw: UICollectionView!
    @IBOutlet weak var myVisualVw: UIVisualEffectView!
    @IBOutlet weak var myLoaderIndicator: UIActivityIndicatorView!
    
    
    @IBOutlet weak var subscriptionLbl: UILabel!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        print("data from flutter 55=====   \(self.dataFromFlutter)")
    
        self.setSubscriptionTextAttributes()
//        self.getAppStoreDetailsDeviceStorage()
        SKPaymentQueue.default().add(self)
        
        //if true means flutter just called all details only to retrieve and send back, flse meanse we are showing native view and we need to get products & reload table.
        if (self.dataFromFlutter["isTableUpdate"] != nil) && (self.dataFromFlutter["isTableUpdate"] as! Bool == true)
        {
            self.fetchIAPproducts(productIdentifiers: productList)
        }
        
        
        // 0. no active subscription or trial   1.trail is active   2.premium plan is active    3.standard plan is active
        if (self.dataFromFlutter["activeSub"] != nil)
        {
            self.activeSubPlan = self.dataFromFlutter["activeSub"] as! Int
            print("active plan id = ",activeSubPlan)
        }
        
        
        //calculating dates of subscription remaining days
        if (self.dataFromFlutter["standard"] != nil)
        {
            self.standardRemainingDays = self.dataFromFlutter["standard"] as! Int
            print("standard remaining days = ",standardRemainingDays)
        }
        if (self.dataFromFlutter["premium"] != nil)
        {
            self.premiumRemainingDays = self.dataFromFlutter["premium"] as! Int
            print("premium remaining days = ",premiumRemainingDays)
        }
    
        let refresh = SKReceiptRefreshRequest()
        refresh.delegate = self
        refresh.start()
    }

    func setSubscriptionTextAttributes()
    {
//        self.planPremiumDescription = "✔ Create up-to 5 projects \n✔ Unlimited access to Graphs \n✔ Unlimited access to Reports \n✔ Generate invoice\n "
//        self.planStandardDescription = "✔ Create 1 project \n❌ No access to Graphs \n❌ No access to Reports \n✔ Generate invoice\n "
        
       
        // create our NSTextAttachment GREEN
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = UIImage(systemName: "checkmark.circle.fill")
        image1Attachment.image = image1Attachment.image?.withTintColor(UIColor.systemGreen)
        // wrap the attachment in its own attributed string so we can append it
        let image1String = NSMutableAttributedString(attachment: image1Attachment)
        
        
         // create our NSTextAttachment GREEN
         let image2Attachment = NSTextAttachment()
         image2Attachment.image = UIImage(systemName: "xmark.circle.fill")
         image2Attachment.image = image2Attachment.image?.withTintColor(UIColor.systemRed)
         // wrap the attachment in its own attributed string so we can append it
         let image2String = NSMutableAttributedString(attachment: image2Attachment)

        
        self.planPremiumDescription.append(image1String)
        self.planPremiumDescription.append(NSAttributedString(string: " Create up-to 5 projects\n"))
        self.planPremiumDescription.append(image1String)
        self.planPremiumDescription.append(NSAttributedString(string: " Unlimited access to Graphs\n"))
        self.planPremiumDescription.append(image1String)
        self.planPremiumDescription.append(NSAttributedString(string: " Unlimited access to Reports\n"))
        self.planPremiumDescription.append(image1String)
        self.planPremiumDescription.append(NSAttributedString(string: " Generate invoice"))
        
        
        self.planStandardDescription.append(image1String)
        self.planStandardDescription.append(NSAttributedString(string: " Create 1 project\n"))
        self.planStandardDescription.append(image2String)
        self.planStandardDescription.append(NSAttributedString(string: " No access to Graphs\n"))
        self.planStandardDescription.append(image2String)
        self.planStandardDescription.append(NSAttributedString(string: " No access to Reports\n"))
        self.planStandardDescription.append(image1String)
        self.planStandardDescription.append(NSAttributedString(string: " Generate invoice\n\n\n\n"))

//        //// create an NSMutableAttributedString that we'll append everything to
//        let fullString1 = NSMutableAttributedString(string: " Create up-to 5 projects\n")
////         add the NSTextAttachment wrapper to our full string, then add some more text.
//        image1String.append(fullString1)
//        fullString1.append(image1String)
//        let fullString2 = NSMutableAttributedString(string: " Unlimited access to Graphs\n")
//        fullString1.append(fullString2)
//        fullString2.append(image1String)
//        let fullString3 = NSMutableAttributedString(string: " Unlimited access to Reports\n")
//        fullString2.append(fullString3)
//        fullString3.append(image1String)
//        image1String.append(fullString3)
//        let fullString4 = NSMutableAttributedString(string: " Generate invoice")
//        image1String.append(fullString4)
//        self.planPremiumDescription = image1String//.append(image1String)
        
        
        
        
        
        print("nnnenwenwnew = ",self.planPremiumDescription)
        print("mmmmmmmmmmm = ",self.planStandardDescription)
        
////        self.planPremiumDescription = makeAttributerText(
////            customStr: [" Create up-to 5 projects"," Unlimited access to Graphs"," Unlimited access to Reports"," Generate invoice"],
////            customImg: [UIImage(systemName: "checkmark"),
////                        UIImage(systemName: "checkmark"),
////                        UIImage(systemName: "checkmark"),
////                        UIImage(systemName: "checkmark"),
////                       ]
////        )
////        self.planStandardDescription = makeAttributerText(
////            customStr: [" Create 1 project"," No access to Graphs"," No access to Reports"," Generate invoice"],
////            customImg: [UIImage(systemName: "checkmark"),
////                        UIImage(systemName: "checkmark"),
////                        UIImage(systemName: "checkmark"),
////                        UIImage(systemName: "checkmark"),
////                       ]
////        )
    }
//    func makeAttributerText(customStr : [String] , customImg : [UIImage]) -> NSMutableAttributedString
//    {
//        for each in customStr
//        {
//            // create our NSTextAttachment
//            let image1Attachment = NSTextAttachment()
//            image1Attachment.image = customImg[each]
//
//            // wrap the attachment in its own attributed string so we can append it
//            let image1String = NSMutableAttributedString(attachment: image1Attachment!)
//
//
//            //// create an NSMutableAttributedString that we'll append everything to
//            let fullString = NSMutableAttributedString(string: " \(customStr[each]!)\n")
//
//
//    //         add the NSTextAttachment wrapper to our full string, then add some more text.
//            image1String.append(fullString)
//        }
//        return image1String
////        return NSMutableAttributedString()
//    }
    
    func fetchIAPproducts(productIdentifiers: [String])
    {
        if(SKPaymentQueue.canMakePayments())
        {
            let productIdentifiers = Set(productIdentifiers)

            request = SKProductsRequest(productIdentifiers: productIdentifiers)
            request.delegate = self
            request.start()
        }
        else
        {
            showAlertUser(ttl: "Alert", msg: "Appstore is not ready yet")
        }
    }
 
    
    func transferAllApstoreDataToFlutter(msg : String?)
    {
        if(msg != nil && msg != "")
        {
            result!(msg)
        }
        else
        {
            result!(FlutterError(code: "UNAVAILABLE",
                             message: msg,//"Apple server not available now.",
                             details: nil))
        }
    }
    
    
    func updateTableWIthNewdata(msg : String?)
    {
        if (self.dataFromFlutter["isTableUpdate"] != nil)
        {
            if (self.dataFromFlutter["isTableUpdate"] as! Bool == true)
            {
                if(msg != nil && msg != "")
                {
                    DispatchQueue.main.async
                    {
                        self.subListTableVw.reloadData()
                        self.subListCollecVw.reloadData()
                    }
                }
                else
                {
                    self.showAlertUser(ttl: "error", msg: "No Elements found")
                }
            }
        }
    }
    func hideShowMyView(val : Bool)
    {
        if (self.dataFromFlutter["isTableUpdate"] != nil)
        {
            if (self.dataFromFlutter["isTableUpdate"] as! Bool == true)
            {
                print("hideing viewview")
                DispatchQueue.main.async
                {
                    self.myVisualVw.isHidden = val
                }
            }
        }
    }
    fileprivate func showAlertUser(ttl : String? , msg : String)
    {
        DispatchQueue.main.async {
            
                let alert = UIAlertController(title: ttl, message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel))
                self.present(alert, animated: true)
        }
    }
    
    
    fileprivate func getRemainDays(_ prod: LatestRecieptInfoDataArray?) -> Int
    {
        if(prod?.purchase_date != "" && prod?.purchase_date != nil)
        {
            let formatter =  DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"

            let calendar = Calendar.current

            // Replace the hour (time) of both dates with 00:00
            let date1 = calendar.startOfDay(for: Date())
            let date2 = calendar.startOfDay(for: formatter.date(from: (prod?.purchase_date)!)!)

            let components = calendar.dateComponents([.day], from: date2, to: date1)
            print("\(prod?.product_id) and days are= ",components.day!.description)
            return components.day ?? 0

        }
        return 0
    }
    
}
//MARK: - Button Actions section
extension NewSubscriptionVC
{
    @IBAction func goBackClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func privacyPolicyClicked(_ sender: UIButton)
    {
        let viewcontroller = self.storyBoard.instantiateViewController(withIdentifier: "webViewVC") as! webViewVC
        DispatchQueue.main.async
        {
            viewcontroller.isTerms = false
            viewcontroller.modalPresentationStyle = .pageSheet
            self.present(viewcontroller, animated: true)
        }
    }
    
    @IBAction func termsConditionClicked(_ sender: UIButton)
    {
        
        let viewcontroller = self.storyBoard.instantiateViewController(withIdentifier: "webViewVC") as! webViewVC
        viewcontroller.isTerms = true
        viewcontroller.modalPresentationStyle = .pageSheet
        self.present(viewcontroller, animated: true)
    }
    
    @IBAction func subscriptionManageClicked(_ sender: UIButton)
    {
        guard let url = URL(string: "https://apps.apple.com/account/subscriptions") else { return }
        UIApplication.shared.open(url, options: [:])
    }
    
}


//MARK: - Tableview section
extension NewSubscriptionVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.skProductList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell : ProductIAPCollectionCell = self.subListCollecVw.dequeueReusableCell(withReuseIdentifier: "iapProdCell", for: indexPath) as! ProductIAPCollectionCell
    
        cell.contentView.clipsToBounds = true
        cell.contentView.layer.cornerRadius = 15
        cell.backVw.clipsToBounds = true
        cell.backVw.layer.cornerRadius = 15
        cell.backVw.layer.borderWidth = 0.6
        cell.backVw.layer.borderColor = UIColor.systemGray4.cgColor
        cell.productRecommendBtn.isHidden = true
        
        
        cell.productTitleLbl.text = self.skProductList[indexPath.row].localizedTitle
        cell.productPriceLbl.text = self.skProductList[indexPath.row].price.description + " " + self.skProductList[indexPath.row].priceLocale.currencyCode!
        cell.productDetailLbl.text = self.skProductList[indexPath.row].localizedDescription
        if(self.skProductList[indexPath.row].productIdentifier.contains("premium") == true)
        {
            cell.productRecommendBtn.isHidden = false
            cell.productDetailLbl.text = self.skProductList[indexPath.row].localizedDescription //+ "\n" + self.planPremiumDescription
            cell.productBuyBtn.setTitle("Subscribe Premium", for: .normal)
            
            let des = NSMutableAttributedString(string: self.skProductList[indexPath.row].localizedDescription + "\n\n")
            des.append(self.planPremiumDescription)
            des.append(NSAttributedString(string: "\n\nYou can cancel any time before each renewal date from manage Subscription"))
            cell.productDetailLbl.attributedText = des
            
        }
        else
        {
            cell.productDetailLbl.text = self.skProductList[indexPath.row].localizedDescription //+ "\n" + self.planStandardDescription
            cell.productBuyBtn.setTitle("Subscribe Standard", for: .normal)
            
            let des = NSMutableAttributedString(string: self.skProductList[indexPath.row].localizedDescription + "\n\n")
            des.append(self.planStandardDescription)
            cell.productDetailLbl.attributedText = des
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let prod = self.latestRecieptArray.first(where: { str in
            str.product_id == self.skProductList[indexPath.row].productIdentifier
        })

        let remainingdayes = self.getRemainDays(prod)
        print("daysssss = \(prod?.product_id)",remainingdayes)
        
        
        if(self.activeSubPlan == 2)
        {
            let alert = UIAlertController(title: "Alert", message: "You are already subscribed to Premium plan. You can cancel the subscription from Appstore at any time.Please ignore this message if you already cancelled it.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            alert.addAction(UIAlertAction(title: "Go to Apstore", style: .default,handler: { alr in
                guard let url = URL(string: "https://apps.apple.com/account/subscriptions") else { return }
                UIApplication.shared.open(url, options: [:])
            }))
            self.present(alert, animated: true)
            return
        }
        else if(self.activeSubPlan == 3) && (self.skProductList[indexPath.row].productIdentifier.contains("standard") == true)
        {
            self.showAlertUser(ttl: "Alert", msg: "You are already subscribed to Standard.")
            return
        }
        else
        {
            DispatchQueue.main.async
            {
                self.buyProduct(self.skProductList[indexPath.row])
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: self.subListCollecVw.frame.size.width * 0.8, height: self.subListCollecVw.frame.size.height)
    }
}
//MARK: - Tableview section
extension NewSubscriptionVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.skProductList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : ProductIAPTableCell = self.subListTableVw.dequeueReusableCell(withIdentifier: "iapProdCell", for: indexPath) as! ProductIAPTableCell
    
        cell.contentView.clipsToBounds = true
        cell.contentView.layer.cornerRadius = 15
        cell.productRecommendBtn.isHidden = true
        
        
        cell.productTitleLbl.text = self.skProductList[indexPath.row].localizedTitle
        cell.productPriceLbl.text = self.skProductList[indexPath.row].price.description + " " + self.skProductList[indexPath.row].priceLocale.currencyCode!
        cell.productDetailLbl.text = self.skProductList[indexPath.row].localizedDescription
        if(self.skProductList[indexPath.row].productIdentifier.contains("premium") == true)
        {
            cell.productRecommendBtn.isHidden = false
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let prod = self.latestRecieptArray.first(where: { str in
            str.product_id == self.skProductList[indexPath.row].productIdentifier
        })

        let remainingdayes = self.getRemainDays(prod)
        print("daysssss = \(prod?.product_id)",remainingdayes)
        
        
        if(self.activeSubPlan == 2)
        {
//            self.showAlertUser(ttl: "Alert", msg: "You are already subscribed to Premium plan. You can cancel the subscription from Appstore at any time.Please ignore this message if you already cancelled it.")
            
            let alert = UIAlertController(title: "Alert", message: "You are already subscribed to Premium plan. You can cancel the subscription from Appstore at any time.Please ignore this message if you already cancelled it.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            alert.addAction(UIAlertAction(title: "Go to Apstore", style: .default,handler: { alr in
                guard let url = URL(string: "https://apps.apple.com/account/subscriptions") else { return }
                UIApplication.shared.open(url, options: [:])
            }))
            self.present(alert, animated: true)
            return
        }
        else if(self.activeSubPlan == 3) && (self.skProductList[indexPath.row].productIdentifier.contains("standard") == true)
        {
            self.showAlertUser(ttl: "Alert", msg: "You are already subscribed to Standard.")
            return
        }
        else
        {
            DispatchQueue.main.async {
                self.buyProduct(self.skProductList[indexPath.row])
            }
        }
    
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    
    func buyProduct(_ product: SKProduct)
    {
        print("Buying \(product.productIdentifier)...")
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
}

//MARK: - IAP REQUEST section
extension NewSubscriptionVC : SKProductsRequestDelegate,SKRequestDelegate
{
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse)
    {
        print("Fetchingggg")
        if !response.products.isEmpty
        {
            self.skProductList = response.products
            print("Fetched items are",response.products.debugDescription)
            DispatchQueue.main.async
            {
                self.subListTableVw.reloadData()
                self.subListCollecVw.reloadData()
            }
            self.hideShowMyView(val: true)
        }
        else
        {
            self.hideShowMyView(val: true)
            self.showAlertUser(ttl: "Sorry", msg: "We couldn't find any subscription plans now. Contact support team for more info.")
            return
        }

        for eer in self.skProductList
        {
            print("\nitems are \(eer.productIdentifier) && \(eer.localizedTitle)")
        }
        for invalidIdentifier in response.invalidProductIdentifiers
        {
           // Handle any invalid product identifiers as appropriate.
            print("invalidIdentifier",invalidIdentifier)
        }
    }
    func request(_ request: SKRequest, didFailWithError error: Error)
    {
        if(error != nil)
        {
            print("didFailWithError,\(error.localizedDescription)")
//            showAlertUser(ttl: "Alert", msg: error.localizedDescription)
        }
    }
    
    func requestDidFinish(_ request: SKRequest)
    {
            print("requestDidFinish")
    }
}

//MARK: - IAP PAYMENT section
extension NewSubscriptionVC : SKStoreProductViewControllerDelegate,SKPaymentQueueDelegate
{
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction])
    {
        transactions.forEach
        { (transaction) in
            switch transaction.transactionState
            {
            case .purchased:
                self.hideShowMyView(val: true)
                print("purchased ites is = \(transaction.transactionIdentifier!) and status = \(transaction.transactionState.rawValue)"  )
                SKPaymentQueue.default().finishTransaction(transaction)
                self.getAppStoreDetailsDeviceStorage()
            case .restored:
                self.hideShowMyView(val: true)
                print("purchased paymentQueueRestoreCompleted is = \(transaction.transactionIdentifier!) and status = \(transaction.transactionState.rawValue)"  )
                SKPaymentQueue.default().finishTransaction(transaction)
//                self.getAppStoreDetailsDeviceStorage()
            case .failed:
                self.hideShowMyView(val: true)
                if let error = transaction.error as? SKError
                {
                    print("Some other eroror = ",error.localizedDescription)
                    if error.code == .paymentCancelled
                    {
                        self.showAlertUser(ttl: "", msg: "User has cancelled the purchase.")
                    }
                    else
                    {
                        self.showAlertUser(ttl: "", msg: "An Error has found while purchacing that \(error.localizedDescription) .")
                    }
                }
                SKPaymentQueue.default().finishTransaction(transaction)
            case .purchasing:
                self.hideShowMyView(val: false)
            case.deferred:
                self.hideShowMyView(val: true)
                break
            @unknown default:
                self.hideShowMyView(val: true)
                break
            }
        }
    }
    
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error)
    {
        self.hideShowMyView(val: true)
        if let error = error as? SKError
        {
            if error.code != .paymentCancelled
            {
                print("IAP Restore Error:", error.localizedDescription)
                self.showAlertUser(ttl: "", msg: "User has cancelled the purchase.")
            }
            else
            {
                print("IAP Restore Error:", error.localizedDescription)
                self.showAlertUser(ttl: "", msg: "An Error has found while purchacing that \(error.localizedDescription) ..")
            }
        }
    }
}

//MARK: - IAP DEVICE RECIEPT section
extension NewSubscriptionVC
{
    func getAppStoreDetailsDeviceStorage()
    {
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,FileManager.default.fileExists(atPath: appStoreReceiptURL.path)
        {
            self.hideShowMyView(val: false)
            do
            {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
                print("receiptData ==== ",receiptData)

                let receiptString = receiptData.base64EncodedString(options: [])
                // Read receiptData.
                self.validateAppReceipt(receiptData)
            }
            catch
            {
                print("Couldn't read receipt data with error: " + error.localizedDescription)
                
                // there is no app receipt, don't panic, ask apple to refresh it
                let appReceiptRefreshRequest = SKReceiptRefreshRequest(receiptProperties: nil)
                appReceiptRefreshRequest.delegate = self
                appReceiptRefreshRequest.start()
                
                self.hideShowMyView(val: true)
                self.showAlertUser(ttl: "Alert", msg: "Couldn't find the purchaces now.Please try again later")
                // If all goes well control will land in the requestDidFinish() delegate method.
                // If something bad happens control will land in didFailWithError.
            }
        }
        else
        {
            self.hideShowMyView(val: true)
        }
    }
    
    //Validate reciept after getting store properties details stored in device of the specific user
    func validateAppReceipt(_ receipt: Data)
    {

        /*  Note 1: This is not local validation, the app receipt is sent to the app store for validation as explained here:
                https://developer.apple.com/library/content/releasenotes/General/ValidateAppStoreReceipt/Chapters/ValidateRemotely.html#//apple_ref/doc/uid/TP40010573-CH104-SW1
            Note 2: Refer to the url above. For good reasons apple recommends receipt validation follow this flow:
                device -> your trusted server -> app store -> your trusted server -> device
            In order to be a working example the validation url in this code simply points to the app store's sandbox servers.
            Depending on how you set up the request on your server you may be able to simply change the
            structure of requestDictionary and the contents of validationURLString.
        */
        let base64encodedReceipt = receipt.base64EncodedString()
        let requestDictionary = ["receipt-data":base64encodedReceipt ,"password" : "7cd3cd01ed964edeb97d9fca1dc6e400" , "exclude-old-transactions": true] as [String : Any]
        guard JSONSerialization.isValidJSONObject(requestDictionary)
        else
        {
            print("requestDictionary is not valid JSON");
            return
        }
       
        
        do
        {
            //checking whether  the app is production or development
//            #if DEBUG
//                let urlString = "https://sandbox.itunes.apple.com/verifyReceipt"
//            #else
//                let urlString = "https://buy.itunes.apple.com/verifyReceipt"
//            #endif
            
            let requestData = try JSONSerialization.data(withJSONObject: requestDictionary)
            let validationURLString = "https://buy.itunes.apple.com/verifyReceipt"  // this works but as noted above it's best to use your own trusted server
            guard let validationURL = URL(string: validationURLString)
            else
            {
                print("the validation url could not be created, unlikely error");
                return
            }
            let session = URLSession(configuration: URLSessionConfiguration.default)
            var request = URLRequest(url: validationURL)
            request.httpMethod = "POST"
            request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
            let task = session.uploadTask(with: request, from: requestData)
            { (data, response, error) in
                self.hideShowMyView(val: true)
                if let data = data , error == nil
                {
                    do
                    {
                        if let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        {
                            print("\n\n\nsuccess. here is the json representation of the app receipt: \(jsonResponse)")
                            if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray
                            {
                                print("\n\n\n\n\n receiptInfo :",receiptInfo)
                                do
                                {
                                    let dddat = try JSONSerialization.data(withJSONObject: receiptInfo, options: .fragmentsAllowed)
                                    let string = String(decoding: dddat, as: UTF8.self)
                                    print("aaaaaaaa ======  ",string)
                                    
                                    
                                    do{
                                        // Parsing the data:
                                        let decoder = JSONDecoder()
                                        self.latestRecieptArray = try JSONDecoder().decode([LatestRecieptInfoDataArray].self, from: string.data(using: .utf8)!)

                                        for eee in  self.latestRecieptArray
                                        {
                                            print("dates areee \(String(describing: eee.purchase_date))")
                                        }
                                    }
                                    catch let error as NSError
                                    {
                                        print("errrrrrsss -===  ",error.debugDescription)
                                    }
                                    
                                    self.transferAllApstoreDataToFlutter(msg: string)
                                    self.updateTableWIthNewdata(msg: string)
                                }
                                catch
                                {
                                    print("fffuuuull erroor1111")
                                    self.transferAllApstoreDataToFlutter(msg: "ERROR")
                                }
                            }
                            else
                            {
                                self.showAlertUser(ttl: "NLRI", msg: "Couldn't find any purchaces now.Please try again later.")
                            }
                        }
                        else
                        {
                            self.showAlertUser(ttl: "NJR", msg: "Couldn't find the purchaces now.Please try again later.")
                        }
                    }
                    catch let error as NSError
                    {
                        print("json serialization failed with error: \(error)")
                        self.showAlertUser(ttl: "Alert", msg: "Couldn't find the purchaces now.Please try again later.")
                    }
                }
                else
                {
                    print("the upload task returned an error: \(error)")
                    self.showAlertUser(ttl: "Alert", msg: "Couldn't find the purchaces now.Please try again later..")
                }
            }
            task.resume()
        }
        catch let error as NSError
        {
            print("json serialization failed with error: \(error)")
            self.showAlertUser(ttl: "Alert", msg: "Couldn't find the purchaces now.Please try again later...")
        }
    }

}




//OLD TABLE VIEW CEL WITH CELL SUBSCRIBED VIEW
//func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//{
//    let cell : ProductIAPTableCell = self.subListTableVw.dequeueReusableCell(withIdentifier: "iapProdCell", for: indexPath) as! ProductIAPTableCell
//
//    cell.contentView.clipsToBounds = true
//    cell.contentView.layer.cornerRadius = 15
//
//    cell.productRecommendBtn.isHidden = true
//
//    //this IF will wor only if apple device reciept validation only
//    if (self.latestRecieptArray.count > 0 && self.latestRecieptArray != nil)
//    {
//        cell.productRecommendBtn.isHidden = true
//
//        let prod = self.latestRecieptArray.first(where: { str in
//            str.product_id == self.skProductList[indexPath.row].productIdentifier
//        })
//
//        var remainingdayes = 0
//        print("\n\n prod ==== AAA",prod?.purchase_date)
//        remainingdayes = getRemainDays(prod)
//
//        if(prod?.product_id?.contains("standard") == true) && (remainingdayes >= 0 && remainingdayes <= 90)
//        {
//            cell.productTitleLbl.text = "Currently Subscribed"
//            cell.productPriceLbl.text = "\(90 - remainingdayes) days remaining"
//            return cell
//        }
//        else if(prod?.product_id?.contains("premium") == true) && (remainingdayes >= 0 && remainingdayes <= 365)
//        {
//            cell.productTitleLbl.text = "Currently Subscribed"
//            cell.productPriceLbl.text = "\(365 - remainingdayes) DAYS remaining"
//            return cell
//        }
//        else
//        {
//            cell.productTitleLbl.text = self.skProductList[indexPath.row].localizedTitle
//            cell.productDetailLbl.text = self.skProductList[indexPath.row].localizedDescription
//            cell.productPriceLbl.text = self.skProductList[indexPath.row].price.description + " " + self.skProductList[indexPath.row].priceLocale.currencyCode!
//            return cell
//        }
//    }
//    else
//    {
//        print("\n\n prod ==== BBB")
//        cell.productTitleLbl.text = self.skProductList[indexPath.row].localizedTitle
//        cell.productDetailLbl.text = self.skProductList[indexPath.row].localizedDescription
//        if(self.skProductList[indexPath.row].productIdentifier.contains("premium") == true)
//        {
//            cell.productRecommendBtn.isHidden = false
//            cell.productTitleLbl.text = self.skProductList[indexPath.row].localizedTitle //+ (" (1 year)")
//        }
//        else
//        {
//            cell.productTitleLbl.text = self.skProductList[indexPath.row].localizedTitle //+ (" (3 Months)")
//        }
//        if(self.skProductList[indexPath.row].productIdentifier.contains("premium") && self.premiumRemainingDays >= 0 )//&& self.activeSubPlan == 2 )
//        {
//            print("\n\n prod ==== ccc")
//            cell.productPriceLbl.text = "\(self.premiumRemainingDays) DAYS remaining"
//        }
//        else if(self.skProductList[indexPath.row].productIdentifier.contains("standard") && self.standardRemainingDays >= 0 )//&& self.activeSubPlan == 3 )
//        {
//            print("\n\n prod ==== ddd")
//            cell.productPriceLbl.text = "\(self.standardRemainingDays) DAYS remaining"
//        }
//        else
//        {
//            print("\n\n prod ==== eee")
//            cell.productPriceLbl.text = self.skProductList[indexPath.row].price.description + " " + self.skProductList[indexPath.row].priceLocale.currencyCode!
//        }
//        return cell
//    }
//
//}

extension UILabel {
    
    func addTrailing(image: UIImage, text:String) {
        let attachment = NSTextAttachment()
        attachment.image = image

        let attachmentString = NSAttributedString(attachment: attachment)
        let string = NSMutableAttributedString(string: text, attributes: [:])

        string.append(attachmentString)
        self.attributedText = string
    }
    
    func addLeading(image: UIImage, text:String) {
        let attachment = NSTextAttachment()
        attachment.image = image

        let attachmentString = NSAttributedString(attachment: attachment)
        let mutableAttributedString = NSMutableAttributedString()
        mutableAttributedString.append(attachmentString)
        
        let string = NSMutableAttributedString(string: text, attributes: [:])
        mutableAttributedString.append(string)
        self.attributedText = mutableAttributedString
    }
}

//extension NSMutableAttributedString {
//   static func addImageInBetweenString(firstSentence: String, image: UIImage?, lastSentence: String) -> NSMutableAttributedString {
//     // create an NSMutableAttributedString that we'll append everything to
//     let fullString = NSMutableAttributedString(string: firstSentence)
//
//     // create our NSTextAttachment
//     let image1Attachment = NSTextAttachment()
//     image1Attachment.image = image
//     //image1Attachment.setImageHeight(height: 15.0)
//     Image1Attachment.bounds = CGRect(x: 0, y: -5, width: 15, height: 15)
//
//     // wrap the attachment in its own attributed string so we can append it
//     let image1String = NSAttributedString(attachment: image1Attachment)
//
//    // add the NSTextAttachment wrapper to our full string, then add some more text.
//    fullString.append(image1String)
//    fullString.append(NSAttributedString(string: lastSentence))
//
//    return fullString
//   }
//}
//
//
//
//extension NSTextAttachment {
//   func setImageHeight(height: CGFloat) {
//      guard let image = image else { return }
//      let ratio = image.size.width / image.size.height
//      bounds = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: ratio * height, height: height)
//  }
// }

