//
//  ProductTableCell.swift
//  Test_IAP
//
//  Created by sultan Al-Kuwari on 17/08/2022.
//

import UIKit

//SubscriptionVC PRODUCT CELL
class ProductTableCell: UITableViewCell {

    
    @IBOutlet weak var productTitleLbl: UILabel!
    @IBOutlet weak var productDetailLbl: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
 
//NewSubscriptionVC PRODUCT TABLE CELL
class ProductIAPTableCell: UITableViewCell {

    
    @IBOutlet weak var productRecommendBtn: UIButton!
    @IBOutlet weak var productTitleLbl: UILabel!
    @IBOutlet weak var productDetailLbl: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    @IBOutlet weak var productImgVw: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
 
//NewSubscriptionVC PRODUCT COLLECTION CELL
class ProductIAPCollectionCell: UICollectionViewCell {

    
    @IBOutlet weak var backVw: UIView!
    @IBOutlet weak var productRecommendBtn: UIButton!
    @IBOutlet weak var productTitleLbl: UILabel!
    @IBOutlet weak var productDetailLbl: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    @IBOutlet weak var productImgVw: UIImageView!
    @IBOutlet weak var productBuyBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


}
 
