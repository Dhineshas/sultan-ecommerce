//
//  webViewVC.swift
//  Runner
//
//  Created by sultan Al-Kuwari on 31/12/2022.
//

import Foundation

import WebKit

class webViewVC: UIViewController
{
    
    var isTerms : Bool!
    var loadURL = ""
    @IBOutlet var webHeadLbl: UILabel!
    @IBOutlet var webView: WKWebView!
    
    var webSlug = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

    }
        
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        if let val = isTerms
        {
            print("isterms issss =AAAA", isTerms)
            if val == true
            {
                self.loadURL = "https://technologylab.global/express_pos/termsandconditions.html"
                self.webHeadLbl.text = "TERMS AND CONDITIONS"
            }
            else
            {
                self.loadURL = "https://technologylab.global/express_pos/privacypolicy.html"
                self.webHeadLbl.text = "PRIVACY POLICY"
            }
            print("isterms issss =BBBB", isTerms)
            self.getPage()
        }
        else
        {
            print("isterms issss =CCCC", isTerms)
            showAlertUser(ttl: "", msg: "URL not found. \ncontact support.")
        }
        
    }
    
    fileprivate func showAlertUser(ttl : String? , msg : String)
    {
        DispatchQueue.main.async {
            
                let alert = UIAlertController(title: ttl, message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel))
                self.present(alert, animated: true)
        }
    }
    
    //MARK: - API Call
    func getPage()
    {
        if(self.loadURL != "")
        {
            self.webView.load(URLRequest(url: URL(string: self.loadURL)!))
        }
    }
    

    // MARK: - Navigation
    @IBAction func goBackClicked(_ sender: UIButton)
    {
        self.dismiss(animated: true)
    }
    

}
