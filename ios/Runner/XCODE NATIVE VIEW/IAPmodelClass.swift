//
//  IAPmodelClass.swift
//  Runner
//
//  Created by sultan Al-Kuwari on 05/09/2022.
//

import Foundation

struct LatestRecieptInfoDataArray: Decodable
{
    let in_app_ownership_type: String?
    let expires_date_ms: String?
    let expires_date_pst: String?
    
    let is_in_intro_offer_period: String?
    let is_trial_period : String?
    
    let original_purchase_date: String?
    let original_purchase_date_ms: String?
    let original_purchase_date_pst: String?
    
    let original_transaction_id: String?
    let product_id: String?
    
    let purchase_date: String?
    let purchase_date_ms: String?
    let purchase_date_pst: String?
    
    let quantity: String?
    let subscription_group_identifier: String?
    let transaction_id: String?
    let web_order_line_item_id: String?

//    enum CodingKeys: String, CodingKey {
////        case expiresDate = "expires_date"
//        case expiresDateMS = "expires_date_ms"
//        case expiresDatePst = "expires_date_pst"
//        case inAppOwnershipType = "in_app_ownership_type"
//        case isInIntroOfferPeriod = "is_in_intro_offer_period"
//        case isTrialPeriod = "is_trial_period"
//        case originalPurchaseDate = "original_purchase_date"
//        case originalPurchaseDateMS = "original_purchase_date_ms"
//        case originalPurchaseDatePst = "original_purchase_date_pst"
//        case originalTransactionID = "original_transaction_id"
//        case productID = "product_id"
//        case purchaseDate = "purchase_date"
//        case purchaseDateMS = "purchase_date_ms"
//        case purchaseDatePst = "purchase_date_pst"
//        case quantity = "quantity"
//        case subscriptionGroupIdentifier = "subscription_group_identifier"
//        case transactionID = "transaction_id"
//        case webOrderLineItemID = "web_order_line_item_id"
//    }

}





//struct LatestRecieptInfoDataArray : Codable
//{
//    var iapId : Int
//    var productId : String
//    var userId : Int?
//    var purchaseId : String
//    var purchaseStatus : String
//    var purchaseTransactionDate : Int?
//    var purchaseVerificationDataOrToken : String?
//    var purchaseDateStarting : Int?
//    var purchaseDateEnding : Int?
//    var purchaseSource : String?
//    var purchaseDevice : String?
//    var purchaseIsAutoRenewing : Int?
//
//    init(iapId: Int,productId : String,userId : Int?,purchaseId : String,purchaseStatus : String,purchaseTransactionDate : Int?,purchaseVerificationDataOrToken : String?,purchaseDateStarting : Int?,purchaseDateEnding : Int?,
//         purchaseSource : String?,purchaseDevice : String?,purchaseIsAutoRenewing : Int?)
//    {
//        self.iapId = iapId
//        self.productId = productId
//        self.purchaseId = productId
//        self.purchaseStatus = purchaseStatus
//        self.purchaseTransactionDate = purchaseTransactionDate
//        self.purchaseVerificationDataOrToken = purchaseVerificationDataOrToken
//        self.purchaseDateStarting = purchaseDateStarting
//        self.purchaseDateEnding = purchaseDateEnding
//        self.purchaseSource = purchaseSource
//        self.purchaseDevice = purchaseDevice
//        self.purchaseIsAutoRenewing = purchaseIsAutoRenewing
//    }
//
//}



// MARK: - Welcome
//class LatestRecieptInfoDataArray: Codable
//{
//    let expiresDate: String?
//    let expiresDateMS: Int?
//    let expiresDatePst, inAppOwnershipType: String?
//    let isInIntroOfferPeriod, isTrialPeriod: Bool?
//    let originalPurchaseDate: String?
//    let originalPurchaseDateMS: Int?
//    let originalPurchaseDatePst: String?
//    let originalTransactionID: Int?
//    let productID, purchaseDate: String?
//    let purchaseDateMS: Int?
//    let purchaseDatePst: String?
//    let quantity: String?
//    let subscriptionGroupIdentifier, transactionID: Int?
//    let webOrderLineItemID: String?
//
//    enum CodingKeys: String, CodingKey {
//        case expiresDate = "expires_date"
//        case expiresDateMS = "expires_date_ms"
//        case expiresDatePst = "expires_date_pst"
//        case inAppOwnershipType = "in_app_ownership_type"
//        case isInIntroOfferPeriod = "is_in_intro_offer_period"
//        case isTrialPeriod = "is_trial_period"
//        case originalPurchaseDate = "original_purchase_date"
//        case originalPurchaseDateMS = "original_purchase_date_ms"
//        case originalPurchaseDatePst = "original_purchase_date_pst"
//        case originalTransactionID = "original_transaction_id"
//        case productID = "product_id"
//        case purchaseDate = "purchase_date"
//        case purchaseDateMS = "purchase_date_ms"
//        case purchaseDatePst = "purchase_date_pst"
//        case quantity = "quantity"
//        case subscriptionGroupIdentifier = "subscription_group_identifier"
//        case transactionID = "transaction_id"
//        case webOrderLineItemID = "web_order_line_item_id"
//    }
//
//    init(expiresDate: String?, expiresDateMS: Int?, expiresDatePst: String?, inAppOwnershipType: String?, isInIntroOfferPeriod: Bool?, isTrialPeriod: Bool?, originalPurchaseDate: String?, originalPurchaseDateMS: Int?, originalPurchaseDatePst: String?, originalTransactionID: Int?, productID: String?, purchaseDate: String?, purchaseDateMS: Int?, purchaseDatePst: String?, quantity: Str?, subscriptionGroupIdentifier: Int?, transactionID: Int?, webOrderLineItemID: String?) {
//        self.expiresDate = expiresDate
//        self.expiresDateMS = expiresDateMS
//        self.expiresDatePst = expiresDatePst
//        self.inAppOwnershipType = inAppOwnershipType
//        self.isInIntroOfferPeriod = isInIntroOfferPeriod
//        self.isTrialPeriod = isTrialPeriod
//        self.originalPurchaseDate = originalPurchaseDate
//        self.originalPurchaseDateMS = originalPurchaseDateMS
//        self.originalPurchaseDatePst = originalPurchaseDatePst
//        self.originalTransactionID = originalTransactionID
//        self.productID = productID
//        self.purchaseDate = purchaseDate
//        self.purchaseDateMS = purchaseDateMS
//        self.purchaseDatePst = purchaseDatePst
//        self.quantity = quantity
//        self.subscriptionGroupIdentifier = subscriptionGroupIdentifier
//        self.transactionID = transactionID
//        self.webOrderLineItemID = webOrderLineItemID
//    }
//}
//
//
//
//
//
//
//
//
//
//
//
//
////(
////            {
////        "expires_date" = "2022-09-08 01:54:34 Etc/GMT";
////        "expires_date_ms" = 1662602074000;
////        "expires_date_pst" = "2022-09-07 18:54:34 America/Los_Angeles";
////        "in_app_ownership_type" = PURCHASED;
////        "is_in_intro_offer_period" = false;
////        "is_trial_period" = false;
////        "original_purchase_date" = "2022-09-07 13:54:36 Etc/GMT";
////        "original_purchase_date_ms" = 1662558876000;
////        "original_purchase_date_pst" = "2022-09-07 06:54:36 America/Los_Angeles";
////        "original_transaction_id" = 2000000148934411;
////        "product_id" = "com.expresspos.projectcreation.Renewing_Subscription_2_year";
////        "purchase_date" = "2022-09-08 00:54:34 Etc/GMT";
////        "purchase_date_ms" = 1662598474000;
////        "purchase_date_pst" = "2022-09-07 17:54:34 America/Los_Angeles";
////        quantity = 1;
////        "subscription_group_identifier" = 21008075;
////        "transaction_id" = 2000000149273026;
////        "web_order_line_item_id" = 2000000010603499;
////    },
////            {
////        "in_app_ownership_type" = PURCHASED;
////        "is_trial_period" = false;
////        "original_purchase_date" = "2022-09-07 14:02:33 Etc/GMT";
////        "original_purchase_date_ms" = 1662559353000;
////        "original_purchase_date_pst" = "2022-09-07 07:02:33 America/Los_Angeles";
////        "original_transaction_id" = 2000000148940664;
////        "product_id" = "com.expresspos.projectcreation.Non_Renewing_Subscription";
////        "purchase_date" = "2022-09-07 14:02:33 Etc/GMT";
////        "purchase_date_ms" = 1662559353000;
////        "purchase_date_pst" = "2022-09-07 07:02:33 America/Los_Angeles";
////        quantity = 1;
////        "transaction_id" = 2000000148940664;
////    },
////            {
////        "in_app_ownership_type" = PURCHASED;
////        "is_trial_period" = false;
////        "original_purchase_date" = "2022-08-23 12:31:20 Etc/GMT";
////        "original_purchase_date_ms" = 1661257880000;
////        "original_purchase_date_pst" = "2022-08-23 05:31:20 America/Los_Angeles";
////        "original_transaction_id" = 2000000136666139;
////        "product_id" = "com.expresspos.projectcreation.non_consumable";
////        "purchase_date" = "2022-08-23 12:31:20 Etc/GMT";
////        "purchase_date_ms" = 1661257880000;
////        "purchase_date_pst" = "2022-08-23 05:31:20 America/Los_Angeles";
////        quantity = 1;
////        "transaction_id" = 2000000136666139;
////    }
////)
