import UIKit
import StoreKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate
{
    
//old concept for each function call and tr==return ios to flutter and flnativeview
//    var productList = ["com.expresspos.projectcreation.non_consumable" , "com.expresspos.projectcreation.Non_Renewing_Subscription" , "com.expresspos.projectcreation.Renewing_Subscription_2_year"]
//    var productListStatus = [false , false , false]
//    var skProductList = [SKProduct]()
//    var request: SKProductsRequest!
    
    
    var navigationController: UINavigationController?
    var CHANNEL: FlutterMethodChannel?

    
    override func application(_ application: UIApplication,didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
        
        weak var registrar = self.registrar(forPlugin: "plugin-name")

        
        //FOR NATIVE VIEW
        let methodChannel = FlutterMethodChannel(name: "navigationToIos", binaryMessenger:controller.binaryMessenger)
        self.CHANNEL = methodChannel
        methodChannel.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
          // This method is invoked on the UI thread.
            guard call.method == "iapNativeView.xcode"
            else
            {
                print("result(FlutterMethodNotImplemented)")
                result(FlutterMethodNotImplemented)
                return
            }
            self?.fromFlutterViewToNativeViewController(result, args: call.arguments as! [String : Any])
        })
        
        //FOR ALL PURCHASED DETAILS OF CURRENT DEVICE STORE
        let iapAvailabilityCheck = FlutterMethodChannel(name: "xpressPos.storeListCheck",binaryMessenger: controller.binaryMessenger)
        iapAvailabilityCheck.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
          // This method is invoked on the UI thread.
            guard call.method == "purchacedRecieptCheck"
            else
            {
                result(FlutterMethodNotImplemented)
                return
            }
            self?.getAllPurchaseddata(result, args: call.arguments as! [String : Any])
        })
        
        GeneratedPluginRegistrant.register(with: self)
        
        navigationController = UINavigationController(rootViewController: controller)
        navigationController?.isNavigationBarHidden = true
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func fromFlutterViewToNativeViewController(_ result: @escaping FlutterResult,args : [String : Any]?)
    {
        let storyboard : UIStoryboard? = UIStoryboard.init(name: "Main", bundle: nil)
        let window: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let subscriptionVC = storyboard!.instantiateViewController(withIdentifier: "NewSubscriptionVC") as! NewSubscriptionVC
        subscriptionVC.result = result
        subscriptionVC.dataFromFlutter = args as [String : AnyObject]?
        navigationController?.pushViewController(subscriptionVC, animated: true)
    }
    func getAllPurchaseddata(_ result: @escaping FlutterResult,args : [String : Any]?)
    {
        let storyboard : UIStoryboard? = UIStoryboard.init(name: "Main", bundle: nil)
        let window: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let subscriptionVC = storyboard!.instantiateViewController(withIdentifier: "NewSubscriptionVC") as! NewSubscriptionVC
        subscriptionVC.result = result
        subscriptionVC.dataFromFlutter = args as [String : AnyObject]?
        subscriptionVC.getAppStoreDetailsDeviceStorage()
    }
}

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
}





