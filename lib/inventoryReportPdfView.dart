import 'dart:io';
import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/PdfReport.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';


class InventoryReportPdfViewScreenState extends StatefulWidget {
  const InventoryReportPdfViewScreenState({Key? key}) : super(key: key);

  @override
  _InventoryReportPdfViewScreenState createState() => _InventoryReportPdfViewScreenState();
}

class _InventoryReportPdfViewScreenState extends State<InventoryReportPdfViewScreenState> {
  @override
  void setState(VoidCallback fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  @override
  void initState() {
    Future.delayed( Duration(milliseconds: 300), () {
      getPdf();
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
          title: Text(
            'Report',
            style: TextStyle(color: Colors.black),
          ),
          leading: IconButton(
            color: MyColors.black,
            icon: Icon(Icons.arrow_back),
            onPressed: () =>
                Navigator.pop(context),
          ),
        ),
        bottomNavigationBar:Container(height :40,child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child:
              InkWell(onTap:(){
                sharePdf();
              },child:  Stack(alignment: Alignment.center, children: <Widget>[
                Container(

                  color:MyColors.white_chinese_e0_bg
                  //BoxDecoration
                ),Wrap(crossAxisAlignment:WrapCrossAlignment.center,direction:Axis.horizontal,alignment:WrapAlignment.center,children: <Widget>[Icon(CupertinoIcons.share,color: MyColors.black),Container( margin: EdgeInsets.all(5),child:Text(
                  "Share PDF",
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 15.0,
                    color: MyColors.black
                  ),
                ) ,)],)

              ]) //Container
              ),),
            Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child:InkWell(onTap:(){
                  Navigator.pop(context);
                },child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Container(

                      color: MyColors.red_ff_bg,
                      //BoxDecoration
                    ),
                    Wrap(crossAxisAlignment:WrapCrossAlignment.center,direction:Axis.horizontal,alignment:WrapAlignment.center,children: <Widget>[Icon(CupertinoIcons.xmark_circle,color: Colors.white,),Container( margin: EdgeInsets.all(5),child: Text(
                      "Close PDF",
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 15.0,
                        color: MyColors.white,
                      ),
                    ),)],)
                  ],
                ) //Container
                ))
          ],
        )),


        body:pdfFile==null? Container():

        SfPdfViewer.file(
            pdfFile!),




    );
  }

  File? pdfFile;



/*  void getPdf() async {

    await generateDocument().then((value) =>
        setState(() {
      pdfFile = value;
      print('generatedPdfFile   ${pdfFile!.path}');
    }));
}*/


  void getPdf() async {
    await MyPref.getProject().then((project)async {
      if(project!=null){
    await PdfReport.generateDocument(project.id,currentYear).then((value) =>
        setState(() {
          pdfFile = value;
          print('generatedPdfFile   ${pdfFile!.path}');
        }));}else{
        showCustomAlertDialog(context, 'Please create a project ');
      }});
  }

sharePdf()async{
  pdfFile!=null?  await Share.shareFiles(['${pdfFile!.path}'], text: 'Share Inventory Report'): context.showSnackBar('Something went wrong..');
}
}
