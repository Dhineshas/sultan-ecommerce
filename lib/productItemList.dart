import 'dart:convert';
import 'dart:io';
import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/productItemDetails.dart';
import 'package:ecommerce/switchProductCategory.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/MyCustomSlidableAction.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'addProductNew.dart';
import 'database/orders_operations.dart';
import 'database/product_item_operations.dart';
import 'model/ProductCategory.dart';
import 'model/ProductItem.dart';

class ProductItemListScreenState extends StatefulWidget {
  final ProductCategory? productCategory;

  const ProductItemListScreenState({@required this.productCategory, Key? key})
      : super(key: key);

  @override
  _ProductItemListScreenState createState() => _ProductItemListScreenState();
}

class _ProductItemListScreenState extends State<ProductItemListScreenState> {
  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 1000), () {
      _getData();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MyColors.white_f1_bg,
        appBar: PreferredSize(
            preferredSize: Size(MediaQuery.of(context).size.width, 120),
            child: Wrap(
              children: [
                MyWidgets.appBar(
                  title: Text(
                    '${widget.productCategory?.name}',
                    style: TextStyle(color: Colors.black),
                  ),
                  leading: IconButton(
                    color: MyColors.black,
                    icon: Icon(Icons.arrow_back),
                    onPressed: () =>
                        Navigator.pop(context, isProductItemAddedOrRemoved),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8, bottom: 8),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                          flex: 7,
                          child: ElevatedButton.icon(
                            icon: const Icon(
                              Icons.create_new_folder_rounded,
                              color: MyColors.black,
                              size: 30,
                            ),
                            onPressed: () async {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          AddProductNewScreenState(
                                            productCategory:
                                                widget.productCategory,
                                          ))).then((value) => value == true
                                  ? {
                                      isProductItemAddedOrRemoved = true,
                                      _getData()
                                    }
                                  : {});
                            },
                            label: Text(
                              "New Product",
                              style: MyStyles.customTextStyle(fontSize: 13),
                            ),
                            style: ElevatedButton.styleFrom(
                              elevation: 0,
                              primary: MyColors.white,
                              fixedSize: const Size(0, 44),
                              shape: MyDecorations.roundedRectangleBorder(10),
                            ),
                          )),
                      SizedBox(
                        width: 15,
                      ),
                      Expanded(
                          flex: 3,
                          child: MyWidgets.shadowContainer(
                              height: 44,
                              blurRadius: 0,
                              cornerRadius: 10,
                              spreadRadius: 0,
                              shadowColor: MyColors.white_f1_bg,
                              child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          child: Center(
                                            child: Icon(
                                              Icons.list,
                                              color: MyColors.black,
                                            ),
                                          ),
                                          onTap: () {
                                            setState(() {
                                              this.isListView = true;
                                            });
                                          },
                                        )),
                                    Container(
                                        width: 0,
                                        height: 30,
                                        child: VerticalDivider(
                                            color: MyColors.grey_9d_bg)),
                                    Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          child: Center(
                                            child: Icon(
                                              Icons.grid_on_rounded,
                                              color: MyColors.black,
                                            ),
                                          ),
                                          onTap: () {
                                            setState(() {
                                              this.isListView = false;
                                            });
                                          },
                                        )
                                    )
                                  ]))),
                      SizedBox(
                        width: 10,
                      ),
                    ],
                  ),
                )
              ],
            )),

        /*MyWidgets.appBar(
          elevation:0,toolbarHeight:130,
          title: Text(
            '${widget.productCategory?.productCategoryName}',
            style: TextStyle(color: Colors.black),
          ),
          flexibleSpace: Column(children: [MyWidgets.appBar(titleSpacing: 10,elevation:0,title:Row(mainAxisSize:MainAxisSize.max,crossAxisAlignment: CrossAxisAlignment.center,mainAxisAlignment: MainAxisAlignment.center,children: [
          Expanded(flex:7,child: ElevatedButton.icon(
            icon: const Icon(
              Icons.create_new_folder_rounded,
              color: MyColors.black,
              size: 30,
            ),
            onPressed: ()async {

            },
            label: Text(
              "Create Folder",
              style: MyStyles.customTextStyle(fontSize: 13),
            ),
            style: ElevatedButton.styleFrom(
              elevation: 0,
              primary: MyColors.white,
              fixedSize: const Size(0, 44),
              shape: MyDecorations.roundedRectangleBorder(10),
            ),
          )),
          SizedBox(width: 15,),
          Expanded(flex:3,child:MyWidgets.shadowContainer(height: 44,blurRadius: 0,cornerRadius: 10,spreadRadius: 0,shadowColor: MyColors.white_f1_bg,
              child: Row(mainAxisSize:MainAxisSize.max,crossAxisAlignment: CrossAxisAlignment.center,mainAxisAlignment: MainAxisAlignment.center,children: [
                Expanded(flex:1,child:Center(child: Icon(
                  Icons.list,
                  color: MyColors.black,
                ), )),Container(width:0,height: 30, child: VerticalDivider(color: MyColors.grey_9d_bg)),
                Expanded(flex:1,child:Center(child:Icon(
                  Icons.grid_on_rounded,
                  color: MyColors.black,
                ),))
              ])))],),),
          ]),
          leading: IconButton(
            color: MyColors.black,
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, isProductItemAddedOrRemoved),
        ),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.add,
                color: Colors.blue,
              ),
              onPressed: () {
                // do something
                Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddProductScreenState(
                                  productCategory: widget.productCategory,
                                )))
                    .then((value) => value == true ? {isProductItemAddedOrRemoved=true,_getData()} : {});
              },
             )
          ],
        ),*/
        bottomNavigationBar: isLongPressEnabled?FadeInUp(child: MyWidgets.addNewButton(text:'Move To',onAddOrMovePressed: (){
          showBottomSheet(SwitchProductCategoryScreenState(isFromProductItemList:true,productCategoryIdArray: productCategoryIdArray,productItemIdArray: productItemIdArray,),maxHeight: 0.60);
        })):null,
        body:
            //getAllInventoryItem()
            createListView(context));
  }

  var items = <ProductItem>[];
  var values = <ProductItem>[];
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool insertItem = false;
  var _scrollController = ScrollController();
  final teNameController = TextEditingController();
  var productItemOperations = ProductItemOperations();

  //var relationOperations = RelationOperations();
  //var workerRelationOperations = WorkerRelationOperations();
  //var cartRelationOperations = CartRelationOperations();
  var orderOperations = OrderOperations();

  //var storageOperations = StorageOperations();
  bool isLoading = true;
  bool isListView = true;
  bool isProductItemAddedOrRemoved = false;

  /// Get all users data
  /*getAllInventoryItem() {
    return FutureBuilder(
        future: _getData(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return createListView(context, snapshot);
        });
  }*/

  ///Fetch data from database
  Future<List<ProductItem>> _getData() async {
    await productItemOperations
        .productItemsByProductCategoryId(widget.productCategory!)
        .then((value) {
      items = value;
      items.isNotEmpty?
      productCategoryIdArray.add(items.first.productCategoryId!):productCategoryIdArray.clear();
    });
    setState(() {
      isLoading = false;
    });

    return items;
  }

  ///create List View with
  Widget createListView(BuildContext context) {
    if (items.isNotEmpty) {
      return FadeIn(
          child: Container(
              height: double.infinity,
              width: double.infinity,
              child: this.isListView ? showListView() : showGridView()));
    } else
      return isLoading
          ? MyWidgets.buildCircularProgressIndicator()
          : Center(
              child: Text(
                'No Product Items.',
                style: TextStyle(
                  fontSize: 17.0,
                  color: Colors.grey,
                ),
              ),
            );
  }

  Widget showListView() {
    return SafeArea(bottom: true,child: ListView.builder(
        key: _listKey,
        physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) {
          //return _buildListItem(context, items[index], index);
          return _buildItemList(context, items[index], index);
        }));
  }

  Widget showGridView() {
    return SafeArea(bottom: true,child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 1.2,
            crossAxisSpacing: 00,
            mainAxisSpacing: 10,
            crossAxisCount: 2
        ),
        key: _listKey,
        physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) {
          //return _buildListItem(context, items[index], index);
          return _buildItemGrid(context, items[index], index);
        }));
  }


  Widget _buildItemList(BuildContext context, ProductItem values, int index) {
    return Padding(
        padding: EdgeInsets.only(left: 0, right: 10),
        child: MySlidableWidgets.slidable(
            // The end action pane is the one at the right or the bottom side.
            endActionPane: ActionPane(
              motion: BehindMotion(),
              extentRatio: 0.25,
              children: <Widget>[
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    onEdit(items[index], index, context);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 94,
                    color: MyColors.grey_9d_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.recycling_outlined,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Edit',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) async {
                    return await showCustomCallbackAlertDialog(
                        context: context,
                        positiveText: 'Delete',
                        msg:
                            'Are you sure you wish to delete all project related data?',
                        positiveClick: () {
                          onDeleteAllDataAndProductItem(index);
                        },
                        negativeClick: () {
                          Navigator.of(context).pop(false);
                        });
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginR: 2,
                    height: 94,
                    topRightCornerRadius: 10,
                    bottomRightCornerRadius: 10,
                    color: MyColors.red_ff_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.close_rounded,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Delete',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),

            // The child of the Slidable is what the user sees when the
            // component is not dragged.
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 0),
              child: ListTile(
                  hoverColor: MyColors.transparent,
                  contentPadding: EdgeInsets.all(0),
                  onLongPress: (){
                    isLongPressEnabled=true;
                    toggleSelection(values);
                  },
                  onTap: () {
                    if(isLongPressEnabled)
                      toggleSelection(values);
                    else
                    onItemClick(values);},
                  title: MyWidgets.shadowContainer(
                      height: 96,
                      paddingL: 15,
                      paddingT: 15,
                      spreadRadius: 2,
                      cornerRadius: 10,
                      shadowColor: MyColors.white_f1_bg,
                      color: values.isSelected==1?MyColors.white_chinese_e0_bg:MyColors.white,

                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                width: 66.0,
                                height: 66.0,
                                margin: EdgeInsets.only(bottom: 15),
                                child: values.imageBase != null &&
                                        values.imageBase.toString().isNotEmpty
                                    //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                                    ? ClipRRect(
                                        borderRadius: BorderRadius.circular(35),
                                        child: Image.memory(
                                          base64Decode(
                                              values.imageBase.toString()),
                                          fit: BoxFit.cover,
                                          gaplessPlayback: true,
                                        ),
                                      )
                                    : ClipRRect(
                                        borderRadius: BorderRadius.circular(35),
                                        child: Container(
                                          color: MyColors.grey_70,
                                        ),
                                      )),
                            SizedBox(
                              width: 3,
                            ),
                            Expanded(
                                flex: 8,
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Padding(
                                      padding:
                                          EdgeInsets.only(left: 8, right: 8),
                                      child: Text(
                                        values.name!,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: MyStyles.customTextStyle(
                                            fontSize: 15,
                                            color: MyColors.black,
                                            fontWeight: FontWeight.w500),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: EdgeInsets.only(left: 8),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.max,
                                            children: [
                                              MyText.priceCurrencyText(context,
                                                  price:
                                                      'Cost: ${double.parse((values.cost!).toString())}',
                                                  fontSize: 15,
                                                  fontWeight:
                                                      FontWeight.normal),
                                              Spacer(),
                                              Icon(
                                                Icons.chevron_right_rounded,
                                                size: 20,
                                              ),
                                              SizedBox(
                                                width: 8,
                                              )
                                            ],
                                          ),
                                        )),
                                    Expanded(
                                        flex: 1,
                                        child: MyWidgets.container(
                                            paddingL: 8,
                                            bottomRightCornerRadius: 10,
                                            color: MyColors.white_chinese_e0_bg,
                                            child: Align(
                                              alignment: Alignment.centerLeft,
                                              child: MyText.priceCurrencyText(
                                                  context,
                                                  price:
                                                      'Selling Price: ${values.sellingPrice}',
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.bold),
                                              // Text(
                                              //   'Selling Price: ${values.sellingPrice}',
                                              //   maxLines: 1,
                                              //   overflow: TextOverflow.ellipsis,
                                              //   style: MyStyles.customTextStyle(
                                              //       fontSize: 15,
                                              //       color: MyColors.black,
                                              //       fontWeight:
                                              //           FontWeight.w500),
                                              //   textAlign: TextAlign.left,
                                              // ),
                                            )))
                                  ],
                                )),
                          ]))),
            )));
  }

  Widget _buildItemGrid(BuildContext context, ProductItem values, int index) {
    return Padding(
        padding: EdgeInsets.only(left: 0, right: 10),
        child: MySlidableWidgets.slidable(
            // The end action pane is the one at the right or the bottom side.
            endActionPane: ActionPane(
              motion: BehindMotion(),
              extentRatio: 0.45,
              children: <Widget>[
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    onEdit(items[index], index, context);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 145,
                    color: MyColors.grey_9d_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.recycling_outlined,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Edit',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) async {
                    return await showCustomCallbackAlertDialog(
                        context: context,
                        positiveText: 'Delete',
                        msg:
                            'Are you sure you wish to delete all project related data?',
                        positiveClick: () {
                          onDeleteAllDataAndProductItem(index);
                        },
                        negativeClick: () {
                          Navigator.of(context).pop(false);
                        });
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginR: 2,
                    height: 145,
                    topRightCornerRadius: 10,
                    bottomRightCornerRadius: 10,
                    color: MyColors.red_ff_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.close_rounded,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Delete',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),

            // The child of the Slidable is what the user sees when the
            // component is not dragged.
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 0),
              child: ListTile(
                  hoverColor: MyColors.transparent,
                  contentPadding: EdgeInsets.all(0),
                  onLongPress: (){
                    isLongPressEnabled=true;
                    toggleSelection(values);
                  },
                  onTap: () {
                    if(isLongPressEnabled)
                      toggleSelection(values);
                    else
                      onItemClick(values);},
                  title: MyWidgets.shadowContainer(
                      height: 145,
                      paddingL: 10,
                      paddingT: 10,
                      spreadRadius: 2,
                      cornerRadius: 10,
                      shadowColor: MyColors.white_f1_bg,
                      color: values.isSelected==1?MyColors.white_chinese_e0_bg:MyColors.white,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                  width: 66.0,
                                  height: 66.0,
                                  margin: EdgeInsets.only(bottom: 0),
                                  child: values.imageBase != null &&
                                          values.imageBase.toString().isNotEmpty
                                      //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                                      ? ClipRRect(
                                          borderRadius: BorderRadius.circular(35),
                                          child: Image.memory(
                                            base64Decode(
                                                values.imageBase.toString()),
                                            fit: BoxFit.cover,
                                            gaplessPlayback: true,
                                          ),
                                        )
                                      : ClipRRect(
                                          borderRadius: BorderRadius.circular(35),
                                          child: Container(
                                            color: MyColors.grey_70,
                                          ),
                                        )
                              ),
                              Icon(
                                Icons.chevron_right_rounded,
                                size: 20,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Expanded(
                              flex: 8,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(left: 8, right: 8),
                                    child: Text(
                                      values.name!,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: MyStyles.customTextStyle(
                                          fontSize: 15,
                                          color: MyColors.black,
                                          fontWeight: FontWeight.w500),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  Expanded(
                                      flex: 1,
                                      child: Padding(
                                        padding: EdgeInsets.only(left: 8),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                            MyText.priceCurrencyText(context,
                                                price:
                                                    'Cost: ${double.parse((values.cost!).toString())}',
                                                fontSize: 15,
                                                fontWeight: FontWeight.normal),
                                          ],
                                        ),
                                      )),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Expanded(flex: 1, child: Container()),
                                      Expanded(
                                          flex: 2,
                                          child: MyWidgets.container(
                                              height: 25,
                                              paddingL: 8,
                                              bottomRightCornerRadius: 10,
                                              color:
                                                  MyColors.white_chinese_e0_bg,
                                              child: Align(
                                                alignment: Alignment.center,
                                                child: MyText.priceCurrencyText(
                                                    context,
                                                    price:
                                                        '${values.sellingPrice}',
                                                    fontSize: 15,
                                                    fontWeight:
                                                        FontWeight.bold),
                                                // Text(
                                                //   this.isListView ? 'Selling Price: ${values.sellingPrice}' : '${values.sellingPrice}',
                                                //   maxLines: 1,
                                                //   overflow: TextOverflow.ellipsis,
                                                //   style: MyStyles.customTextStyle(
                                                //       fontSize: 15,
                                                //       color: MyColors.black,
                                                //       fontWeight:
                                                //           FontWeight.w500),
                                                //   textAlign: TextAlign.left,
                                                // ),
                                              ))),
                                    ],
                                  )
                                ],
                              )),
                        ],
                      ))),
            )));
  }

  // Widget _buildListItem(BuildContext context, ProductItem values, int index) {
  //   return InkWell(
  //     onTap: () {
  //       onItemClick(values);
  //     },
  //     child: Container(
  //         margin: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
  //         decoration: BoxDecoration(
  //             border: Border.all(
  //               color: values.isInventoryItemUpdated == 1
  //                   ? (Colors.redAccent)
  //                   : (Colors.teal[900])!,
  //             ),
  //             borderRadius: BorderRadius.all(Radius.circular(15))),
  //         child: Column(
  //           mainAxisSize: MainAxisSize.max,
  //           children: <Widget>[
  //             Row(
  //                 mainAxisSize: MainAxisSize.max,
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 children: <Widget>[
  //                   //image
  //                   Container(
  //                     margin: EdgeInsets.fromLTRB(5, 5, 10, 5),
  //                     child: values.imageBase != null &&
  //                             values.imageBase.toString().isNotEmpty
  //                         ? ClipRRect(
  //                             borderRadius: BorderRadius.circular(10),
  //                             child: Image.memory(
  //                               base64Decode(values.imageBase.toString()),
  //                               width: 80,
  //                               height: 80,
  //                               fit: BoxFit.fitHeight,
  //                               gaplessPlayback: true,
  //                             ),
  //                           )
  //                         : Container(
  //                             decoration: BoxDecoration(
  //                                 color: Colors.grey[200],
  //                                 borderRadius: BorderRadius.circular(10)),
  //                             width: 80,
  //                             height: 80,
  //                           ),
  //                   ),
  //                   //name and count
  //
  //                   Column(
  //                     crossAxisAlignment: CrossAxisAlignment.start,
  //                     mainAxisSize: MainAxisSize.min,
  //                     children: <Widget>[
  //                       Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0)),
  //                       Text(
  //                         values.name!,
  //                         style: TextStyle(
  //                           fontWeight: FontWeight.w500,
  //                           fontSize: 18.0,
  //                           color: Colors.black,
  //                         ),
  //                         textAlign: TextAlign.left,
  //                         maxLines: 2,
  //                       ),
  //                       Padding(
  //                           padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),
  //
  //                       Text(
  //                         'Cost : ${double.parse((values.cost!).toStringAsFixed(2))} QAR',
  //                         style: TextStyle(
  //                           fontWeight: FontWeight.w300,
  //                           fontSize: 15.0,
  //                           color: Colors.black,
  //                         ),
  //                         textAlign: TextAlign.left,
  //                         maxLines: 2,
  //                       ),
  //
  //                       //Padding(padding: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 10.0)),
  //                     ],
  //                   ),
  //                   Spacer(),
  //                   /*Column(
  //                   children: <Widget>[
  //                     IconButton(
  //                         color: Colors.black,
  //                         icon: new Icon(Icons.edit),
  //                         onPressed: () => onEdit(values, index, context)),
  //                     IconButton(
  //                         color: Colors.black,
  //                         icon: new Icon(Icons.delete),
  //                         onPressed: () => onDelete(index)),
  //                   ],
  //                 ),*/
  //                   //Spacer()
  //                 ]),
  //             Row(
  //               mainAxisAlignment: MainAxisAlignment.center,
  //               children: [
  //                 Flexible(
  //                     flex: 1,
  //                     fit: FlexFit.tight,
  //                     child:
  //                         Stack(alignment: Alignment.center, children: <Widget>[
  //                       Container(
  //                         height: 35,
  //                         decoration: BoxDecoration(
  //                           borderRadius: BorderRadius.only(
  //                               bottomLeft: Radius.circular(14),
  //                               bottomRight: Radius.circular(14)),
  //                           color: (Colors.teal[900]),
  //                         ), //BoxDecoration
  //                       ),
  //                       Text(
  //                         "Selling Price : ${double.parse((values.sellingPrice!).toStringAsFixed(2))} QAR",
  //                         style: TextStyle(
  //                           fontWeight: FontWeight.bold,
  //                           fontSize: 15.0,
  //                           color: Colors.white,
  //                         ),
  //                       )
  //                     ]) //Container
  //                     ) /*,Flexible(
  //                 flex: 1,
  //                 fit: FlexFit.tight,
  //                 child: Stack( alignment: Alignment.center,children: <Widget>[Container(
  //
  //                   height: 35,
  //                   decoration: BoxDecoration(
  //                     borderRadius: BorderRadius.only(bottomRight:Radius.circular(14.5)),
  //                     color: (Colors.teal[900])!,
  //                   ), //BoxDecoration
  //                 ), Text("History",style: TextStyle(
  //                   fontWeight: FontWeight.w500,
  //                   fontSize: 15.0,
  //                   color: Colors.white,
  //                 ),)],)//Container
  //             )*/
  //               ],
  //             ),
  //           ],
  //         )),
  //   );
  // }

  ///On Item Click
  onItemClick(ProductItem productItem) async {
    if (productItem.isInventoryItemUpdated == 1) {
      setState(() {
        productItem.isInventoryItemUpdated = 0;
      });

      await productItemOperations
          .updateProductItemIsInventoryItemStatusByProductItemId(productItem)
          .then((value) => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProductItemDetailsScreenState(
                              productCategory: widget.productCategory,
                              productItemId: productItem.id,
                            ))).then((value) => value == true ? _getData() : {})
              });
      return;
    } else
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ProductItemDetailsScreenState(
                    productCategory: widget.productCategory,
                productItemId: productItem.id,
                  ))).then((value) => value == true ? _getData() : {});
  }

  /// Delete Click and delete item
  /*onDelete(int index) async {
    var id = items[index].id;
    // 1st delete all product item related data from relation table on product item delete
   await relationOperations.deleteRelationByProductItemId(id!).then((value)async {
        // 2nd delete all product item related data from worker relation table on product item delete
       await workerRelationOperations.deleteWorkerRelationByProductItemId(id).then((value)async {
            // 3rd delete all product item related data from cart table on product item delete
             await cartRelationOperations.deleteCartRelationItemByProductItemId(id).then((value) async{
                // 4th delete all product item related data from orders table on product item delete
               await orderOperations.deleteOrderItemByProductItemId(id).then((value)async {
                 // 5th delete all product item related data from storage table on product item delete
                 await storageOperations.deleteStorageItemByProductItemId(id).then((value)async{
                    // 6th then delete the product item from product_item table
                   await productItemOperations.deleteProductItem(id).then((value) {
                     isProductItemAddedOrRemoved=true;
                      setState(() {
                        items.removeAt(index);

                      });
                      */ /*items.removeAt(index);
              if(items.isEmpty){
                setState(() {

                });}*/ /*
                    });});});});});});

  }*/

  onDeleteAllDataAndProductItem(int index) async {
    var id = items[index].id;
    await orderOperations.isOrderNewExistByProductItemId(id).then((bool) async {
      if (bool) {
        Navigator.of(context).pop(false);
        showCustomAlertDialog(context,
            'In order proceed, either deliver or cancel the order related to the current project.');
      } else {
        await productItemOperations
            .deleteAllDataAndProductItem(id)
            .then((value) async {
          print('onSuccessError  ');
          isProductItemAddedOrRemoved = true;
          setState(() {
            items.removeAt(index);
            Navigator.of(context).pop(true);
          });
        }).onError((error, stackTrace) {
          Navigator.of(context).pop(false);
          print('onDeleteError  ${error}  ${stackTrace}');
        });
      }
    });
  }

  ///edit User
  editUser(int id, BuildContext context) async {
    if (teNameController.text.isNotEmpty) {
      var productItem = ProductItem();
      productItem.id = id;
      productItem.name = teNameController.text;

      await productItemOperations
          .updateRawProductItem(productItem)
          .then((value) {
        teNameController.text = "";

        Navigator.of(context).pop();
        showtoast("Data Saved successfully");

        _getData();
      });
    } else {
      showtoast("Please fill all the fields");
    }
  }


  /// Edit Click
  onEdit(ProductItem productItem, int index, BuildContext context) {
    //openAlertBox(productItem, context);
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => AddProductNewScreenState(
                  productCategory: widget.productCategory,
                  productItemId: productItem.id,
                ))).then((value) =>
        value == true ? {isProductItemAddedOrRemoved = true, _getData()} : {});
  }

  /// openAlertBox to add/edit user
  openAlertBox(ProductItem productItem, BuildContext context) {
    if (productItem != null) {
      teNameController.text = productItem.name!;
    } else {
      teNameController.text = "";
    }

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25.0))),
            contentPadding: EdgeInsets.only(top: 10.0),
            content: Container(
              width: 300.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        /*flag ?*/
                        "Edit Product Item" /*: "Add User"*/,
                        style: TextStyle(fontSize: 28.0),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Divider(
                    color: Colors.grey,
                    height: 4.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 30.0, right: 30.0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                            controller: teNameController,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              hintText: "Add Product Item Name",
                              fillColor: Colors.grey[300],
                              border: InputBorder.none,
                            ),
                            /*validator: validateName,
                            onSaved: (String val) {
                              teNameController.text = val;
                            },*/
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () => /* flag ?*/ editUser(
                        productItem.id!, context) /*: addUser()*/,
                    child: Container(
                      padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                      decoration: BoxDecoration(
                        color: Color(0xff01A0C7),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(25.0),
                            bottomRight: Radius.circular(25.0)),
                      ),
                      child: Text(
                        /*flag ?*/
                        "Edit Product Item" /*: "Add User"*/,
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
  showBottomSheet(StatefulWidget statefulWidget,{double maxHeight=0.75}) {
    print('statefulWidgetRunType ${statefulWidget.runtimeType}');
    MyBottomSheet().showBottomSheet(context,
        maxHeight:maxHeight, callback: (value) {
          if (value != null && value == true) {
            isProductItemAddedOrRemoved=true;
            isLongPressEnabled=false;
            productItemIdArray.clear();
            productCategoryIdArray.clear();
            _getData();
          }
        }, child: statefulWidget);
  }
  var isLongPressEnabled = false;
  var productItemIdArray = <int>[];
  var productCategoryIdArray = <int>[];
  void toggleSelection(ProductItem values) {
    setState(() {
      if (values.isSelected==0) {
        values.isSelected=1;
        productItemIdArray.add(values.id!);
        print('productItemIdArray $productItemIdArray  $productCategoryIdArray');
      } else {
        values.isSelected=0;
        productItemIdArray.remove(values.id!);
        print('productItemIdArray $productItemIdArray  $productCategoryIdArray');
        final isAllSelectionClear = items.map((e) => e.isSelected).every((element) => element==0);
        if(isAllSelectionClear){
          productItemIdArray.clear();
          isLongPressEnabled=!isAllSelectionClear;
        }

        print('toggleTest $isAllSelectionClear');

      }
    });
  }
}
