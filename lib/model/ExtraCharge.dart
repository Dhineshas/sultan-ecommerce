class ExtraCharge{
  int? extraChargeId;
  int? projectId;
  String? extraChargeDetails;
  double? extraChargeCost;
  int? extraChargeDate=DateTime.now().millisecondsSinceEpoch;


  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'project_id': projectId,
      'extra_charge_details': extraChargeDetails,
      'extra_charge_cost': extraChargeCost,
      'extra_charge_date': extraChargeDate,
    };
    map['extra_charge_id'] = extraChargeId;
    return map;
  }

  ExtraCharge();

  @override
  String toString() {
    return 'ExtraCharge{extra_charge_id: $extraChargeId,project_id: $projectId, extra_charge_details: $extraChargeDetails, extra_charge_cost: $extraChargeCost, extra_charge_date: $extraChargeDate}';
  }
}