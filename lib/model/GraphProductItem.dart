class GraphProductItem{
  int? id;
  String? name;
  int? orderQuantityTotal=0;
  double? orderGrossTotal=0;
  double? orderNetTotal=0;
  double? orderTotalProductCost=0;
  double? orderTotalProductProfit=0;
  int? orderYear=0;
  int? orderMonth=0;



  GraphProductItem();

  @override
  String toString() {
    return 'GraphProductItem{product_item_id: $id, product_item_name: $name,sum_order_quantity: $orderQuantityTotal,sum_order_gross_total: $orderGrossTotal, sum_order_net_total: $orderNetTotal, sum_order_total_product_cost: $orderTotalProductCost, sum_order_total_product_profit: $orderTotalProductProfit, order_year: $orderYear, order_month: $orderMonth}';
  }
}