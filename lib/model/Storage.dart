class Storage{
  int? id;
  int? productItemId;
  int? quantity;



  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'product_item_id': productItemId,
      'quantity': quantity,
    };
    map['storage_id'] = id;
    return map;
  }

  Storage();

  @override
  String toString() {
    return 'Storage{storage_id: $id, product_item_id: $productItemId, quantity: $quantity}';
  }
}