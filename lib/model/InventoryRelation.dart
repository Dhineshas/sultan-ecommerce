class InventoryRelation{
  int? id;
  int? productItemId;
  int? inventoryItemId;
  double? inventoryItemQuantity;


  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'product_item_id': productItemId,
      'inventory_item_id': inventoryItemId,
      'inventory_item_quantity': inventoryItemQuantity,
    };
    map['relation_id'] = id;
    return map;
  }

  InventoryRelation();

  @override
  String toString() {
    return 'InventoryRelation{relation_id: $id, product_item_id: $productItemId, inventory_item_id: $inventoryItemId, inventory_item_quantity: $inventoryItemQuantity}';
  }
}