class UserCredentials{
  int? id;
  String? userName;
  String? userPhoto;
  String? userEmail;
  String? userUid;
  String? userMobile;
  String? userProvider;
  String? userDbUrl;
  int? userIsLogin;

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'user_name': userName,
      'user_photo': userPhoto,
      'user_email': userEmail,
      'user_uid': userUid,
      'user_mobile': userMobile,
      'user_provider': userProvider,
      'user_db_url': userDbUrl,
      'user_is_login': userIsLogin,
    };
    map['user_id'] = id;
    return map;
  }

  UserCredentials();

  @override
  String toString() {
    return 'UserCredentials{user_id:$id,user_name: $userName,user_photo: $userPhoto, user_email: $userEmail, user_uid: $userUid, user_mobile: $userMobile,user_provider: $userProvider, userDbUrl: $userDbUrl, user_is_login: $userIsLogin}';
  }
}