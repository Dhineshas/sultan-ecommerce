class FirestoreUser{
  String? userName;
  String? userEmail;
  String? userPassword;
  String? userStorageUrl;
  DateTime? userCreatedAt=DateTime.now();
  DateTime? userUpdatedAt=DateTime.now();



  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'user_name': userName,
      'user_email': userEmail,
      'user_password': userPassword,
      'user_storage_url':userStorageUrl,
      'user_created_at':userCreatedAt,
      'user_updated_at':userUpdatedAt,


    };
    return map;
  }

  FirestoreUser();

  @override
  String toString() {
    return 'FirestoreUser{user_name: $userName, user_email: $userEmail,user_password: $userPassword,user_storage_url: $userStorageUrl,user_created_at: $userCreatedAt,user_updated_at: $userUpdatedAt}';
  }
}