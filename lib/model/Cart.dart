class Cart{
  int? id;
  int? projectId;
  double? cartDeliveryCharge=0.00;
  double? cartDiscount=0.00;



  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'project_id': projectId,
      'cart_delivery_charge': cartDeliveryCharge,
      'cart_discount': cartDiscount,
    };
    map['cart_id'] = id;
    return map;
  }

  Cart();

  @override
  String toString() {
    return 'Cart{cart_id: $id, project_id: $projectId, cart_delivery_charge: $cartDeliveryCharge, cart_discount: $cartDiscount}';
  }
}