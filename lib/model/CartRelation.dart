class CartRelation{
  int? id;
  int? cartId;
  int? productItemId;
  int? quantity;




  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'cart_id': cartId,
      'product_item_id': productItemId,
      'cart_quantity': quantity,
    };
    map['cart_relation_id'] = id;
    return map;
  }

  CartRelation();

  @override
  String toString() {
    return 'CartRelation{cart_relation_id: $id, cart_id: $cartId, product_item_id: $productItemId, cart_quantity: $quantity}';
  }
}