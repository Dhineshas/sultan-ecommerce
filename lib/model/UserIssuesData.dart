class UserIssuesData{

  int? issueId;
  String? username;
  String? userMobile;
  String? userEmailId;
  String? userIssue;
  int? complaint_start_Date;



  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'user_name': username,
      'user_Mobile': userMobile,
      'user_EmailId': userEmailId,
      'user_Issue': userIssue,
      'complaint_start_Date': complaint_start_Date,
    };
    map['issueId'] = issueId;
    return map;
  }

  UserIssuesData.fromJson(Map<String, dynamic> json) {
    issueId = json['issue_Id'];
    userMobile = json['user_Mobile'];
    username = json['user_name'];
    userEmailId = json['user_EmailId'];
    userIssue = json['user_Issue'];
    complaint_start_Date = json['complaint_start_Date'];


  }
  UserIssuesData();

  @override
  String toString() {
    return 'IAPPurchaseData{issue_Id:$issueId,user_name: $username,user_Mobile: $userMobile,user_EmailId: $userEmailId, user_Issue: $userIssue,complaint_start_Date: $complaint_start_Date';
  }
}