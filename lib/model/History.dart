class History{
  int? id;
  int? historyDate=DateTime.now().millisecondsSinceEpoch;
  int? inventoryItemId;
  int? historyItemType;
  int? historyTotalType;
  int? historyPiecesInType;
  double? historyCostPerType=0.00;
  double? historyCostPerItemInType=0.00;

  String? historyFlag='purchase';
  double? historyStock;



  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'inventory_item_id': inventoryItemId,
      'history_date': historyDate,

      'history_inventory_item_type': historyItemType,
      'history_inventory_item_total_type': historyTotalType,
      'history_inventory_item_pieces_in_type': historyPiecesInType,
      'history_inventory_item_cost_per_type': historyCostPerType,
      'history_inventory_item_cost_per_item_in_type_new': historyCostPerItemInType,

      'history_inventory_item_flag': historyFlag,
      'history_inventory_item_stock': historyStock,
    };
    map['history_id'] = id;
    return map;
  }

  History();

  @override
  String toString() {
    return 'History{history_id: $id,history_date:$historyDate, product_item_id: $inventoryItemId,history_inventory_item_type: $historyItemType,history_inventory_item_total_type: $historyTotalType,history_inventory_item_pieces_in_type: $historyPiecesInType,history_inventory_item_cost_per_type: $historyCostPerType,history_inventory_item_cost_per_item_in_type_new: $historyCostPerItemInType,history_inventory_item_flag: $historyFlag, history_inventory_item_stock: $historyStock}';
  }
}