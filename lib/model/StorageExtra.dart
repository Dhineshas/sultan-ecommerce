class StorageExtra{
  int? id;
  int? projectId;
  String? name;
  String? imageBase;
  double? cost=0.0;
  double? sellingPrice=0.0;
  int? storageQuantity;
  int? storageSourceStatus=0;



  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'project_id': projectId,
      'storage_extra_product_item_name': name,
      'storage_extra_product_item_image': imageBase,
      'storage_extra_product_item_cost': cost,
      'storage_extra_product_item_selling_price': sellingPrice,
      'quantity': storageQuantity,
    };
    map['storage_extra_product_id'] = id;
    return map;
  }

  StorageExtra();

  @override
  String toString() {
    return 'StorageExtra{storage_extra_product_id: $id, project_id: $projectId, storage_extra_product_item_name: $name, storage_extra_product_item_image: $imageBase,storage_extra_product_item_cost: $cost,storage_extra_product_item_selling_price: $sellingPrice, quantity: $storageQuantity, storage_extra_product_item_status: $storageSourceStatus}';
  }
}