

class IAPPurchaseData{

  int? iapId;
  String? productId;
  int? userId;
  String? purchaseId;
  String? purchaseStatus;
  int? purchaseTransactionDate;
  String? purchaseVerificationDataOrToken;
  int? purchaseDateStarting;
  int? purchaseDateEnding;
  String? purchaseSource;
  String? purchaseDevice;
  int? purchaseIsAutoRenewing;
//for fire store subscription
  String? userEmail;


  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'product_id': productId,
      'user_id': userId,
      'purchase_id': purchaseId,
      'purchase_status': purchaseStatus,
      'purchase_transaction_date': purchaseTransactionDate,
      'purchase_verification_data_or_token': purchaseVerificationDataOrToken,
      'purchase_date_starting': purchaseDateStarting,
      'purchase_date_ending': purchaseDateEnding,
      'purchase_source': purchaseSource,
      'purchase_device': purchaseDevice,
      'purchase_is_auto_renewing': purchaseIsAutoRenewing,
    };
    map['iap_id'] = iapId;
    return map;
  }

  IAPPurchaseData.fromJson(Map<String, dynamic> json) {
    iapId = json['iap_id'];
    userId = json['user_id'];
    productId = json['product_id'];
    purchaseId = json['purchase_id'];
    purchaseStatus = json['purchase_status'];
    purchaseTransactionDate = json['purchase_transaction_date'];
    purchaseVerificationDataOrToken = json['purchase_verification_data_or_token'];
    purchaseDateStarting = json['purchase_date_starting'];
    purchaseDateEnding = json['purchase_date_ending'];
    purchaseSource = json['purchase_source'];
    purchaseDevice = json['purchase_device'];
    purchaseIsAutoRenewing = json['purchase_is_auto_renewing'];


  }
  IAPPurchaseData();

  @override
  String toString() {
    return 'IAPPurchaseData{iap_id:$iapId,user_id: $userId,product_id: $productId,purchase_id: $purchaseId, purchase_status: $purchaseStatus,purchase_transaction_date: $purchaseTransactionDate, purchase_verification_data_or_token: $purchaseVerificationDataOrToken,purchase_date_starting: $purchaseDateStarting, purchase_date_ending: $purchaseDateEnding,purchase_source: $purchaseSource,purchase_device: $purchaseDevice,purchase_is_auto_renewing: $purchaseIsAutoRenewing}';
  }
}