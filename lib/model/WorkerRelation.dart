class WorkerRelation{
  int? id;
  int? productItemId;
  int? workerId;
  int? workerHour;


  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'product_item_id': productItemId,
      'worker_id': workerId,
      'work_duration': workerHour,
    };
    map['relation_id'] = id;
    return map;
  }

  WorkerRelation();

  @override
  String toString() {
    return 'WorkerRelation{relation_id: $id, product_item_id: $productItemId, worker_id: $workerId, work_duration: $workerHour}';
  }
}