class ProductCategory{
  int? id;
  int? projectId;
  String? name;
  String? imageBase;
  // for listing product category and product item in productCategoryList screen

  int? productItemCount;
  double? cost;
  double? sellingPrice;
  int? hasCategory=0;
  int? isSelected=0;
  int? isInventoryItemUpdated=0;


  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'project_id': projectId,
      'product_category_name': name,
      'product_category_image': imageBase,
    };
    map['product_category_id'] = id;
    return map;
  }

  ProductCategory();

  @override
  String toString() {
    return 'ProductCategory{product_category_id: $id,project_id: $projectId, product_category_name: $name, product_category_image: $imageBase, product_item_count: $productItemCount}';
  }
}