import 'package:flutter/cupertino.dart';

class HomeGridItems{
  Color? backgroundColor;
  IconData? iconData;
  String? title;
  double? value=0.0;
  HomeGridItems({ this.backgroundColor,this.iconData,this.title,this.value});
}

class UnitMeasureResult{
  double? stockConversionResult=0.00;
  double? perStockConversionCost=0.00;
  dynamic stockMultiplierValue=0.00;
  UnitMeasureResult({ this.stockConversionResult,this.perStockConversionCost,this.stockMultiplierValue});
}

