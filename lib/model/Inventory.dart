class Inventory{
  int? id;
  int? projectId;
  String? name;
  String? imageBase;
  // for listing inventory category and inventory item in inventoryCategoryList screen

  int? inventoryItemCount=0;
  double? inventoryItemStock=0.0;
  int? hasCategory=0;
  int? isSelected=0;
  int? inventoryItemType;
  double? costPerItemInTypeNew=0.00;


  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'project_id': projectId,
      'inventory_name': name,
      'inventory_image': imageBase,
    };
    map['inventory_id'] = id;
    return map;
  }

  Inventory();

  @override
  String toString() {
    return 'Inventory{inventory_id: $id,project_id: $projectId, inventory_name: $name, inventory_image: $imageBase,inventory_item_count: $inventoryItemCount,inventory_item_has_category: $hasCategory, inventory_item_cost_per_item_in_type_new: $costPerItemInTypeNew, inventory_item_type: $inventoryItemType}';
  }
}