class ProductItem{
  int? id;
  String? name;
  String? imageBase;
  double? cost;
  double? sellingPrice;
  int? productCategoryId;
  String? productCategoryName;
// for cart operation products
  int? cartQuantity=0;
  int? cartId=0;
  double? cartDeliverCharge=0.0;
  double? cartDiscount=0.0;
  int? cartRelationId=0;

  // for storage operation products
  int? storageQuantity=0;

// for order operation products
  int? orderId;
  int? createdAt=0;
  double? orderDeliveryCharge=0.0;
  double? orderDiscount=0.0;
  double? orderGrossTotal=0.0;
  double? orderNetTotal=0.0;
  int? orderQuantity=0;
  int? orderStatus=0;

  int? priceByPercentage=0;
  int? isInventoryItemUpdated=0;

  int? checkedState=0;
  int? projectId;
  int? hasCategory=0;
  int? isSelected=0;
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'product_item_name': name,
      'product_item_image': imageBase,
      'product_item_cost': cost,
      'product_item_selling_price': sellingPrice,
      'product_category_id': productCategoryId,
      'product_item_price_by_percentage': priceByPercentage,
      'product_item_is_inventory_item_updated': isInventoryItemUpdated,
      'project_id': projectId,
      'product_item_has_category': hasCategory,
    };
    map['product_item_id'] = id;
    return map;
  }

  ProductItem();

  @override
  String toString() {
    return 'ProductItem{product_item_id: $id, product_item_name: $name, product_item_image: $imageBase, product_item_cost: $cost, product_item_selling_price: $sellingPrice, product_item_price_by_percentage: $priceByPercentage, product_item_is_inventory_item_updated: $isInventoryItemUpdated, product_category_id: $productCategoryId,product_category_name: $productCategoryName, cart_quantity: $cartQuantity, cart_id: $cartId,cart_delivery_charge: $cartDeliverCharge,cart_discount: $cartDiscount,cart_relation_id: $cartRelationId, order_id: $orderId, order_created: $createdAt, order_delivery_charge: $orderDeliveryCharge, order_discount: $orderDiscount, order_gross_total: $orderGrossTotal, order_net_total: $orderNetTotal,order_quantity: $orderQuantity, order_status: $orderStatus, project_id: $projectId,product_item_has_category: $hasCategory }';
  }
}