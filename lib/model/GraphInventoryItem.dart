class GraphInventoryItem{
  int? id;
  String? name;
  String? imageBase;
  String? inventoryName;
  int? itemType;
  // for conversion of inventory type on add product
  int? convertedItemType;
  int? totalType;
  int? piecesInType;
  double? costPerTypeNew=0.00;
  double? costPerTypeOld=0.00;

  double? costPerItemInTypeNew=0.00;
  double? costPerItemInTypeOld=0.00;
  int? isPiecesInTypeUpdated=0;

  int? isCostUpdated=0;
  double? stock=0;
  int? inventoryId;
  int? projectId;
  int? hasCategory=0;
  int? checkedState=0;
  double? selectedQuantity=1;

  int? isSelected=0;

  //for inventoriesItemsByHistory() in inventory_item_operations
  int? historyId;
  int? historyAddedDate=0;
  int? historyInventoryItemType;
  int? historyInventoryItemTotalType;
  int? historyInventoryItemPiecesInType;
  double? historyInventoryItemCostPerType=0.00;
  double? historyInventoryItemCostPerItemInType=0.00;
  double? historyAddedStock=0.0;
  double? historyStockConsumed=0.0;
  double? inventoryTotalPurchaseCost=0.0;

  double? maxOfTotalType=0;
  int? historyYear=0;
  int? historyMonth=0;


 /* Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'inventory_item_name': name,
      'inventory_item_image': imageBase,
      'inventory_item_type': itemType,
      'inventory_item_total_type': totalType,
      'inventory_item_pieces_in_type': piecesInType,
      'inventory_item_cost_per_type_old': costPerTypeOld,
      'inventory_item_cost_per_type_new': costPerTypeNew,
      'inventory_item_is_cost_updated': isCostUpdated,

      'inventory_item_is_pieces_in_type_updated': isPiecesInTypeUpdated,
      'inventory_item_cost_per_item_in_type_old': costPerItemInTypeOld,
      'inventory_item_cost_per_item_in_type_new': costPerItemInTypeNew,


      'inventory_item_stock': stock,
      'inventory_id': inventoryId,
      'project_id': projectId,
      'inventory_item_has_category': hasCategory,

    };
    map['inventory_item_id'] = id;
    return map;
  }*/

  GraphInventoryItem();

  @override
  String toString() {
    return 'GraphInventoryItem{inventory_item_id: $id, inventory_item_name: $name, inventory_item_image: $imageBase, inventory_item_type: $itemType, inventory_item_total_type: $totalType, inventory_item_pieces_in_type: $piecesInType, inventory_item_cost_per_type_old: $costPerTypeOld, inventory_item_cost_per_type_new: $costPerTypeNew,inventory_item_is_cost_updated: $isCostUpdated,inventory_item_is_pieces_in_type_updated: $isPiecesInTypeUpdated,inventory_item_cost_per_item_in_type_old: $costPerItemInTypeOld,inventory_item_cost_per_item_in_type_new: $costPerItemInTypeNew, inventory_item_stock: $stock, inventory_id: $inventoryId,inventory_id: $projectId,inventory_item_has_category: $hasCategory, inventory_name: $inventoryName,inventory_item_quantity: $selectedQuantity,history_id: $historyId,history_date: $historyAddedDate,history_inventory_item_type: $historyInventoryItemType,history_inventory_item_total_type: $historyInventoryItemTotalType,history_inventory_item_pieces_in_type: $historyInventoryItemPiecesInType,history_inventory_item_cost_per_type: $historyInventoryItemCostPerType,history_inventory_item_cost_per_item_in_type_new: $historyInventoryItemCostPerItemInType,history_inventory_item_stock: $historyAddedStock, max_value_item_type: $maxOfTotalType,history_year: $historyYear, history_month: $historyMonth, history_inventory_item_stock_added: $historyStockConsumed, history_inventory_total_purchase_cost: $inventoryTotalPurchaseCost}';
  }
}