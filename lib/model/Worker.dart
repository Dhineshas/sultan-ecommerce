class Worker{
  int? id;
  int? projectId;
  String? name;
  int? salary;
  int? hoursPerDay;
  double? wagePerMinOld=0.00;
  double? wagePerMinNew=0.00;
  int? checkedState=0;
 // var selectedHoursTxt;
  int? selectedMins=0;


  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'project_id': projectId,
      'worker_name': name,
      'worker_salary': salary,
      'worker_hours': hoursPerDay,
      'worker_wage_per_min_old': wagePerMinOld,
      'worker_wage_per_min_new': wagePerMinNew,
    };
    map['worker_id'] = id;
    return map;
  }

  Worker();

  @override
  String toString() {
    return 'Worker{worker_id: $id,project_id: $projectId, worker_name: $name, worker_salary: $salary,worker_hours: $hoursPerDay,worker_wage_per_min_old: $wagePerMinOld,worker_wage_per_min_new: $wagePerMinNew, work_duration: $selectedMins}';
  }
}