class Project{
  int? id;
  int? userId;
  String? projectName;
  String? imageBase;
  String? contactNo;
  String? email;




  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'user_id': userId,
      'project_name': projectName,
      'project_image': imageBase,
      'project_contact_no': contactNo,
      'project_email': email,
    };
    map['project_id'] = id;
    return map;
  }

  Project();

  @override
  String toString() {
    return 'Project{project_id: $id,user_id: $id, project_name: $projectName, project_image: $imageBase, project_contact_no: $contactNo, project_email: $email}';
  }


  Project.fromJson(Map<String, dynamic> json) {
    id = json['project_id'];
    userId = json['user_id'];
    projectName = json['project_name'];
    imageBase = json['project_image'];
    contactNo = json['project_contact_no'];
    email = json['project_email'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['project_id'] = this.id;
    data['user_id'] = this.userId;
    data['project_name'] = this.projectName;
    data['project_image'] = this.imageBase;
    data['project_contact_no'] = this.contactNo;
    data['project_email'] = this.email;
    return data;
  }
}