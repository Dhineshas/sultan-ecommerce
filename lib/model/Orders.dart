class Orders{
  int? id;
  int? orderCreated/*=DateTime.now().millisecondsSinceEpoch*/;
  int? productItemId;
  int? quantity;
  int? orderStatus=0;
  double? orderDeliveryCharge=0.0;
  double? orderDiscount=0.0;
  double? orderGrossTotal=0.0;
  double? orderNetTotal=0.0;
  //for setting profit graph
  double? totalProfit=0.0;
  double? totalSales=0.0;
  double? totalCost=0.0;
  int? orderYear=0;
  int? orderMonth=0;



  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'order_created': orderCreated,
      'product_item_id': productItemId,
      'order_delivery_charge': orderDeliveryCharge,
      'order_discount': orderDiscount,
      'order_gross_total': orderGrossTotal,
      'order_net_total': orderNetTotal,
      'order_quantity': quantity,
      'order_status': orderStatus,
    };
    map['order_id'] = id;
    return map;
  }

  Orders();

  @override
  String toString() {
    return 'Orders{cart_id: $id,order_created: $orderCreated, product_item_id: $productItemId, order_delivery_charge: $orderDeliveryCharge, order_discount: $orderDiscount, order_gross_total: $orderGrossTotal, order_net_total: $orderNetTotal, order_quantity: $quantity, order_status: $orderStatus, sum_order_total_product_profit: $totalProfit,sum_order_total_product_price: $totalSales, sum_order_total_product_cost: $totalCost, order_year: $orderYear, order_month: $orderMonth}';
  }
}