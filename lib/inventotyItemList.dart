import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:math';
import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/addInventoryCategory.dart';
import 'package:ecommerce/addInventoryItemNew.dart';
import 'package:ecommerce/inventotyItemHistoryNew2.dart';
import 'package:ecommerce/model/Inventory.dart';
import 'package:ecommerce/model/InventoryItem.dart';
import 'package:ecommerce/switchInventoryCategory.dart';
import 'package:ecommerce/switchMeasure.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/MyCustomSlidableAction.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'addInventoryItem.dart';
import 'database/DatabaseHelper.dart';
import 'database/history_operations.dart';
import 'database/inventory_item_operations.dart';
import 'database/inventory_operations.dart';
import 'database/relation_operations.dart';
import 'editInventoryItem.dart';
import 'inventoryItemDetails.dart';
import 'inventotyItemHistory.dart';
import 'inventotyItemHistoryNew.dart';
import 'inventoryUnitMeasures.dart';
import 'model/ProductCategory.dart';

class InventoryItemListScreenState extends StatefulWidget {
  final Inventory? inventory;

  const InventoryItemListScreenState({@required this.inventory, Key? key})
      : super(key: key);

  @override
  _InventoryItemListScreenState createState() =>
      _InventoryItemListScreenState();
}

class _InventoryItemListScreenState
    extends State<InventoryItemListScreenState> {
  var inventoryRelationOperations = RelationOperations();
  var historyItemOperations = HistoryOperations();
  bool isInventoryItemAddedOrRemoved = false;

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 1000), () {
      _getData();
    });
    super.initState();
    print("INIT");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MyColors.white_f1_bg,
        appBar: PreferredSize(
            preferredSize: Size(MediaQuery.of(context).size.width, 120),
            child: Wrap(
              children: [
                MyWidgets.appBar(
                  title: Text(
                    '${widget.inventory?.name}',
                    style: TextStyle(color: Colors.black),
                  ),
                  leading: IconButton(
                    color: MyColors.black,
                    icon: Icon(Icons.arrow_back),
                    onPressed: () =>
                        Navigator.pop(context, true),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8, bottom: 8),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                          flex: 7,
                          child: ElevatedButton.icon(
                            icon: const Icon(
                              Icons.create_new_folder_rounded,
                              color: MyColors.black,
                              size: 30,
                            ),
                            onPressed: () async {
                              /*Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddInventoryItemScreenState(inventory: widget.inventory,))).then((value) => value==true? {isInventoryItemAddedOrRemoved=true,_getData()} :{});*/

                              //showAddInventoryItemBottomSheet();
                              showInventoryItemOperationsBottomSheet(
                                  AddInventoryItemScreenStateNew(
                                inventory: widget.inventory,
                              ));
                            },
                            label: Text(
                              "Add New Inventory",
                              style: MyStyles.customTextStyle(fontSize: 13),
                            ),
                            style: ElevatedButton.styleFrom(
                              elevation: 0,
                              primary: MyColors.white,
                              fixedSize: const Size(0, 44),
                              shape: MyDecorations.roundedRectangleBorder(10),
                            ),
                          )),
                      SizedBox(
                        width: 15,
                      ),
                      Expanded(
                          flex: 3,
                          child: MyWidgets.shadowContainer(
                              height: 44,
                              blurRadius: 0,
                              cornerRadius: 10,
                              spreadRadius: 0,
                              shadowColor: MyColors.white_f1_bg,
                              child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          child: Center(
                                            child: Icon(
                                              Icons.list,
                                              color: MyColors.black,
                                            ),
                                          ),
                                          onTap: () {
                                            setState(() {
                                              this.isListView = true;
                                            });
                                          },
                                        )),
                                    Container(
                                        width: 0,
                                        height: 30,
                                        child: VerticalDivider(
                                            color: MyColors.grey_9d_bg)),
                                    Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          child: Center(
                                            child: Icon(
                                              Icons.grid_on_rounded,
                                              color: MyColors.black,
                                            ),
                                          ),
                                          onTap: () {
                                            setState(() {
                                              this.isListView = false;
                                            });
                                          },
                                        ))
                                  ]))),
                      SizedBox(
                        width: 10,
                      ),
                    ],
                  ),
                )
              ],
            )),

        /*AppBar(
          title: Text(
            '${widget.inventory?.name}',
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, isInventoryItemAddedOrRemoved),
        ),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.add,
                color: Colors.blue,
              ),
              onPressed: () {
                // do something

                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AddInventoryItemScreenState(inventory: widget.inventory,))).then((value) => value==true? {isInventoryItemAddedOrRemoved=true,_getData()} :{});
              },
            )
          ],
        ),*/
        bottomNavigationBar: isLongPressEnabled?FadeInUp(child: MyWidgets.addNewButton(text:'Move To',onAddOrMovePressed: (){
          showInventoryItemOperationsBottomSheet(SwitchInventoryCategoryScreenState(isFromInventoryItemList:true,inventoryIdArray: inventoryIdArray,inventoryItemIdArray: inventoryItemIdArray,),maxHeight: 0.60);
        })):null,
        body: createListView(context));
  }

  var items = <InventoryItem>[];
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool insertItem = false;
  var _scrollController = ScrollController();
  final teNameController = TextEditingController();
  var inventoryItemOperations = InventoryItemOperations();
  bool isLoading = true;
  bool isListView = true;

  ///Fetch data from database
  Future<List<InventoryItem>> _getData() async {
    await inventoryItemOperations
        .inventoriesItemsByInventoryId(widget.inventory!)
        .then((value) {
      items = value;
      items.isNotEmpty?
      inventoryIdArray.add(items.first.inventoryId!):inventoryIdArray.clear();
    });

    setState(() {
      isLoading = false;
    });
    return items;
  }

  ///create List View with
  Widget createListView(BuildContext context) {
    if (items.isNotEmpty) {
      return FadeIn(
          child: Container(
              height: double.infinity,
              width: double.infinity,
              child: this.isListView ? showListView() : showGridView()));
    } else
      return isLoading
          ? MyWidgets.buildCircularProgressIndicator()
          : Center(
              child: Text(
                'Inventory item list is empty.',
                style: TextStyle(
                  fontSize: 17.0,
                  color: Colors.grey,
                ),
              ),
            );
  }

  Widget showListView() {
    return SafeArea(bottom: true,child: ListView.builder(
        physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
        key: _listKey,
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) {
          /*return Dismissible( background: Container(
            color: (Colors.teal[900])!,
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: Row(
                children: [
                  Icon(Icons.edit_outlined, color: Colors.white),
                  Text('Edit', style: TextStyle(color: Colors.white)),
                ],
              ),
            ),
          ),secondaryBackground: Container(
            color: Colors.red,
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Icon(Icons.delete_forever_outlined, color: Colors.white),
                  Text('Delete', style: TextStyle(color: Colors.white)),
                ],
              ),
            ),
          ),key: Key(items[index].name!),confirmDismiss: (DismissDirection direction) async {
            if(direction == DismissDirection.endToStart){

              return await showCustomCallbackAlertDialog(context:context,positiveText: 'Delete',msg: 'Are you sure you wish to delete all project related data?',positiveClick:() {
                onDeleteAllDataAndInventoryItem(index);

              },negativeClick: (){
                Navigator.of(context).pop(false);
              });
            }else{

              onEditInventoryItem(items[index]);
            }},child:  _buildListItem(context, items[index], index),);*/

          return _buildListItem(context, items[index], index);
        }));
  }

  Widget showGridView() {
    return SafeArea(bottom: true,child:  GridView.builder(
        gridDelegate:
            const SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 1.2,
                crossAxisSpacing: 00,
                mainAxisSpacing: 10,
                crossAxisCount: 2
            ),
        physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
        key: _listKey,
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) {
          return _buildItemGrid(context, items[index], index);
        }));
  }

  ///Construct cell for List View
  Widget _buildListItem(BuildContext context, InventoryItem values, int index) {
    return Padding(
        padding: EdgeInsets.only(left: 0, right: 10),
        child: MySlidableWidgets.slidable(

          // The end action pane is the one at the right or the bottom side.
            endActionPane: ActionPane(
              motion: BehindMotion(),
              extentRatio: 0.50,
              children: <Widget>[
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {

                    showInventoryItemOperationsBottomSheet(EditInventoryItemScreenState(
                      inventory: widget.inventory,
                      /*inventoryItem: values,*/
                      inventoryItemId: values.id,
                    ),maxHeight: 0.80);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 94,
                    color: MyColors.grey_9d_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.recycling_outlined,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Edit',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    showInventoryItemOperationsBottomSheet(SwitchMeasureScreenState(inventoryItemId: values.id,),);

                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 94,
                    color: MyColors.yellow_ff,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.av_timer_rounded,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Switch',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    onHistoryClick(values);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 94,
                    color: MyColors.grey_9d_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.history,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'History',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) async {
                    return await showCustomCallbackAlertDialog(
                        context: context,
                        positiveText: 'Delete',
                        msg:
                        'Are you sure you wish to delete all project related data?',
                        positiveClick: () {
                          onDeleteAllDataAndInventoryItem(index);
                        },
                        negativeClick: () {
                          Navigator.of(context).pop(false);
                        });
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                      marginR: 2,
                      height: 94,
                      color: MyColors.red_ff_bg,
                      topRightCornerRadius: 10,
                      bottomRightCornerRadius: 10,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.close_rounded,
                            size: 20,
                            color: MyColors.white,
                          ),
                          Text(
                            'Delete',
                            style: MyStyles.customTextStyle(
                              fontSize: 11,
                              color: MyColors.white,
                            ),
                          )
                        ],
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: MyColors.grey_9d_bg,
                          blurRadius: 1.0,
                          offset: Offset(0.7, 0),
                        ),
                      ]),
                ),
              ],
            ),

            // The child of the Slidable is what the user sees when the
            // component is not dragged.
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 0),
              child: ListTile(
                  hoverColor: MyColors.transparent,
                  contentPadding: EdgeInsets.all(0),
                  onLongPress: (){
                    isLongPressEnabled=true;
                    toggleSelection(values);
                  },
                  onTap: () {

                    if(isLongPressEnabled)
                        toggleSelection(values);
                    else
                  showInventoryItemOperationsBottomSheet(InventoryItemDetailsScreenState(

                    inventoryItemId: values.id,
                  ));},
                  title: MyWidgets.shadowContainer(
                      height: 95,
                      paddingL: 10,
                      // spreadRadius: 2,
                      cornerRadius: 10,
                      shadowColor: MyColors.white_f1_bg,
                      color: values.isSelected==1?MyColors.white_chinese_e0_bg:MyColors.white,
                      child: Row(
                        children: [
                          Container(
                              width: 66.0,
                              height: 66.0,
                              child: values.imageBase != null &&
                                  values.imageBase.toString().isNotEmpty
                              //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                                  ? ClipRRect(
                                borderRadius: BorderRadius.circular(8),
                                child: Image.memory(
                                  base64Decode(
                                      values.imageBase.toString()),
                                  fit: BoxFit.cover,
                                  gaplessPlayback: true,
                                ),
                              )
                                  : ClipRRect(
                                borderRadius: BorderRadius.circular(8),
                                child: Container(
                                  color: MyColors.grey_70,
                                ),
                              )
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                              flex: 9,
                              child: Container(
                                  margin: EdgeInsets.only(top: 5, bottom: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Spacer(),
                                      Text(
                                        values.name!,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: MyStyles.customTextStyle(
                                            fontSize: 15,
                                            color: MyColors.black,
                                            fontWeight: FontWeight.w500),
                                        textAlign: TextAlign.left,
                                      ),
                                      Spacer(),
                                      MyText.priceCurrencyText(context,
                                          price:
                                          'Cost: ${values.costPerItemInTypeNew}',
                                          fontSize: 15,
                                          fontWeight: FontWeight.normal,
                                          fontColor: MyColors.grey_9d_bg),
                                      Spacer(),
                                      /*Text('Stock: ${values.stock!} ${InventoryUnitMeasure.inventoryUnitMeasureArray[values.itemType!]}',
                            style: MyStyles.customTextStyle(fontSize: 15,fontWeight:FontWeight.w400 ),
                            textAlign: TextAlign.left,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),*/
                                      values.itemType == 0
                                          ? MyText.priceCurrencyText(context,
                                          price:
                                          'Stock: ${values.stock!} Items',
                                          exponent: '(Box)',
                                          fontSize: 15,
                                          fontWeight: FontWeight.w400)
                                          : Text(
                                        'Stock: ${values.stock!} ${InventoryUnitMeasure.inventoryUnitMeasureArray[values.itemType!]}',
                                        style: MyStyles.customTextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w400),
                                        textAlign: TextAlign.left,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      Spacer(),
                                    ],
                                  ))
                          ),
                          Container(
                            width: 45,
                            decoration: BoxDecoration(
                              color: MyColors.white_chinese_e0_bg,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10),
                                  bottomRight: Radius.circular(10)),
                            ),
                            child: Center(
                              child: Icon(
                                Icons.chevron_right_rounded,
                                size: 25,
                                color: MyColors.black,
                              ),
                            ),
                          ),
                        ],
                      )
                  )
              ),
            )
        )
    );

    // return InkWell(
    //     onTap: () async {
    //       onSelectInventoryItem(values);
    //     },
    //     child: Container(
    //         margin: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
    //         decoration: BoxDecoration(
    //             border: Border.all(
    //               color: values.isCostUpdated == 1
    //                   ? (Colors.redAccent)
    //                   : (Colors.teal[900])!,
    //             ),
    //             borderRadius: BorderRadius.all(Radius.circular(15))),
    //         child: Column(
    //           mainAxisSize: MainAxisSize.max,
    //           children: <Widget>[
    //             Row(
    //                 mainAxisSize: MainAxisSize.max,
    //                 crossAxisAlignment: CrossAxisAlignment.start,
    //                 children: <Widget>[
    //                   //image
    //                   Container(
    //                     margin: EdgeInsets.fromLTRB(5, 5, 10, 5),
    //                     child: values.imageBase != null &&
    //                             values.imageBase.toString().isNotEmpty
    //                         ? ClipRRect(
    //                             borderRadius: BorderRadius.circular(10),
    //                             child: Image.memory(
    //                               base64Decode(values.imageBase.toString()),
    //                               width: 80,
    //                               height: 80,
    //                               fit: BoxFit.fitHeight,
    //                               gaplessPlayback: true,
    //                             ),
    //                           )
    //                         : Container(
    //                             decoration: BoxDecoration(
    //                                 color: Colors.grey[200],
    //                                 borderRadius: BorderRadius.circular(10)),
    //                             width: 80,
    //                             height: 80,
    //                           ),
    //                   ),
    //                   //name and count
    //
    //                   Column(
    //                     crossAxisAlignment: CrossAxisAlignment.start,
    //                     mainAxisSize: MainAxisSize.min,
    //                     children: <Widget>[
    //                       Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0)),
    //                       Container(
    //                         width: 200,
    //                         child: RichText(
    //                           maxLines: 1,
    //                           overflow: TextOverflow.ellipsis,
    //                           strutStyle: StrutStyle(fontSize: 12.0),
    //                           text: TextSpan(
    //                             style: TextStyle(
    //                                 fontWeight: FontWeight.w500,
    //                                 fontSize: 18,
    //                                 color: Colors.black),
    //                             text: values.name!,
    //                           ),
    //                         ),
    //                       ),
    //                       Padding(
    //                           padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),
    //                       Text(
    //                         //'Cost: ${double.parse(((values.costPerTypeNew! / values.piecesInType!)).toStringAsFixed(2))} QAR / Item',
    //                         values.itemType == 0
    //                             ? 'Cost: ${values.costPerItemInTypeNew} QAR / Item'
    //                             : 'Cost: ${values.costPerItemInTypeNew} QAR / ${InventoryUnitMeasure.inventoryUnitMeasureArray[values.itemType!]}',
    //                         style: TextStyle(
    //                           fontWeight: FontWeight.w300,
    //                           fontSize: 15.0,
    //                           color: Colors.black,
    //                         ),
    //                         textAlign: TextAlign.left,
    //                         maxLines: 2,
    //                       ),
    //                     ],
    //                   ),
    //                   Spacer(),
    //                 ]),
    //             Row(
    //               mainAxisAlignment: MainAxisAlignment.center,
    //               children: [
    //                 Flexible(
    //                     flex: 1,
    //                     fit: FlexFit.tight,
    //                     child: Stack(
    //                         alignment: Alignment.center,
    //                         children: <Widget>[
    //                           Container(
    //                             height: 35,
    //                             decoration: BoxDecoration(
    //                               borderRadius: BorderRadius.only(
    //                                   bottomLeft: Radius.circular(14)),
    //                               color: Colors.grey,
    //                             ), //BoxDecoration
    //                           ),
    //                           Text(
    //                             "Stock: ${values.stock} Items",
    //                             style: TextStyle(
    //                               fontWeight: FontWeight.w500,
    //                               fontSize: 15.0,
    //                               color: Colors.white,
    //                             ),
    //                           )
    //                         ]) //Container
    //                     ),
    //                 Flexible(
    //                     flex: 1,
    //                     fit: FlexFit.tight,
    //                     child: InkWell(
    //                       onTap: () {
    //                         onHistoryClick(values);
    //                       },
    //                       child: Stack(
    //                         alignment: Alignment.center,
    //                         children: <Widget>[
    //                           Container(
    //                             height: 35,
    //                             decoration: BoxDecoration(
    //                               borderRadius: BorderRadius.only(
    //                                   bottomRight: Radius.circular(14)),
    //                               color: (Colors.teal[900])!,
    //                             ), //BoxDecoration
    //                           ),
    //                           Text(
    //                             "History",
    //                             style: TextStyle(
    //                               fontWeight: FontWeight.w500,
    //                               fontSize: 15.0,
    //                               color: Colors.white,
    //                             ),
    //                           )
    //                         ],
    //                       ),
    //                     ) //Container
    //                     )
    //               ],
    //             ),
    //           ],
    //         )));
  }

  Widget _buildItemGrid(BuildContext context, InventoryItem values, int index) {
    return Padding(
        padding: EdgeInsets.only(left: 0, right: 10),
        child: MySlidableWidgets.slidable(

            // The end action pane is the one at the right or the bottom side.
            endActionPane: ActionPane(
              motion: BehindMotion(),
              extentRatio: 0.60,
              children: <Widget>[
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    showInventoryItemOperationsBottomSheet(EditInventoryItemScreenState(
                      inventory: widget.inventory,

                      inventoryItemId: values.id,
                    ),maxHeight: 0.80);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 145,
                    color: MyColors.grey_9d_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.recycling_outlined,
                          size: 20,
                          color: MyColors.white,
                        ),
                        // Text(
                        //   'Edit',
                        //   style: MyStyles.customTextStyle(
                        //     fontSize: 11,
                        //     color: MyColors.white,
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    showInventoryItemOperationsBottomSheet(SwitchMeasureScreenState(
                      inventoryItemId:values.id,
                    ));
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 145,
                    color: MyColors.yellow_ff,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.av_timer_rounded,
                          size: 20,
                          color: MyColors.white,
                        ),
                        // Text(
                        //   'Switch',
                        //   style: MyStyles.customTextStyle(
                        //     fontSize: 11,
                        //     color: MyColors.white,
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    onHistoryClick(values);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 145,
                    color: MyColors.grey_9d_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.history,
                          size: 20,
                          color: MyColors.white,
                        ),
                        // Text(
                        //   'History',
                        //   style: MyStyles.customTextStyle(
                        //     fontSize: 11,
                        //     color: MyColors.white,
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) async {
                    return await showCustomCallbackAlertDialog(
                        context: context,
                        positiveText: 'Delete',
                        msg:
                            'Are you sure you wish to delete all project related data?',
                        positiveClick: () {
                          onDeleteAllDataAndInventoryItem(index);
                        },
                        negativeClick: () {
                          Navigator.of(context).pop(false);
                        });
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                      marginR: 2,
                      height: 145,
                      color: MyColors.red_ff_bg,
                      topRightCornerRadius: 10,
                      bottomRightCornerRadius: 10,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.close_rounded,
                            size: 20,
                            color: MyColors.white,
                          ),
                          // Text(
                          //   'Delete',
                          //   style: MyStyles.customTextStyle(
                          //     fontSize: 11,
                          //     color: MyColors.white,
                          //   ),
                          // )
                        ],
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: MyColors.grey_9d_bg,
                          blurRadius: 1.0,
                          offset: Offset(0.7, 0),
                        ),
                      ]),
                ),
              ],
            ),

            // The child of the Slidable is what the user sees when the
            // component is not dragged.
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 0),
              child: ListTile(
                  hoverColor: MyColors.transparent,
                  contentPadding: EdgeInsets.all(0),
                  onTap: () => /*onSelectInventoryItem(values)*/
                      //showInventoryItemDetailsBottomSheet(values),
                      showInventoryItemOperationsBottomSheet(InventoryItemDetailsScreenState(
                        //inventory: widget.inventory,
                        inventoryItemId: values.id,
                      )),
                  title: MyWidgets.shadowContainer(
                      height: 145,
                      paddingL: 10,
                      spreadRadius: 2,
                      cornerRadius: 10,
                      shadowColor: MyColors.white_f1_bg,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                                child: Container(
                                    width: 66.0,
                                    height: 66.0,
                                    child: values.imageBase != null &&
                                            values.imageBase.toString().isNotEmpty
                                        //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                                        ? ClipRRect(
                                            borderRadius: BorderRadius.circular(8),
                                            child: Image.memory(
                                              base64Decode(
                                                  values.imageBase.toString()),
                                              fit: BoxFit.cover,
                                              gaplessPlayback: true,
                                            ),
                                          )
                                        : ClipRRect(
                                            borderRadius: BorderRadius.circular(8),
                                            child: Container(
                                              color: MyColors.grey_70,
                                            ),
                                          )
                                ),
                              ),
                              Container(
                                height: 40,
                                width: 40,
                                decoration: BoxDecoration(
                                  color: MyColors.white_chinese_e0_bg,
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(10),
                                      bottomLeft: Radius.circular(10)),
                                ),
                                child: Center(
                                  child: Icon(
                                    Icons.chevron_right_rounded,
                                    size: 20,
                                    color: MyColors.black,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Expanded(
                              flex: 9,
                              child: Container(
                                  margin: EdgeInsets.only(top: 5, bottom: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Spacer(),
                                      Text(
                                        values.name!,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: MyStyles.customTextStyle(
                                            fontSize: 15,
                                            color: MyColors.black,
                                            fontWeight: FontWeight.w500),
                                        textAlign: TextAlign.left,
                                      ),
                                      Spacer(),
                                      MyText.priceCurrencyText(context,
                                          price:
                                              'Cost: ${values.costPerItemInTypeNew}',
                                          fontSize: 14,
                                          fontWeight: FontWeight.normal,
                                          fontColor: MyColors.grey_9d_bg),
                                      Spacer(),
                                      /*Text('Stock: ${values.stock!} ${InventoryUnitMeasure.inventoryUnitMeasureArray[values.itemType!]}',
                            style: MyStyles.customTextStyle(fontSize: 15,fontWeight:FontWeight.w400 ),
                            textAlign: TextAlign.left,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),*/
                                      values.itemType == 0
                                          ? MyText.priceCurrencyText(context,
                                              price:
                                                  'Stock: ${values.stock!} Items',
                                              exponent: '(Box)',
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400)
                                          : Text(
                                              'Stock: ${values.stock!} ${InventoryUnitMeasure.inventoryUnitMeasureArray[values.itemType!]}',
                                              style: MyStyles.customTextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400),
                                              textAlign: TextAlign.left,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                      Spacer(),
                                    ],
                                  ))
                          ),
                        ],
                      )
                  )
              ),
            )
        )
    );
  }

  ///On Item Click
  onItemClick(InventoryItem values) {
    print("Clicked position is ${values.name}");
  }

  onHistoryClick(InventoryItem values) {
    print("Clicked position is ${values.name}");
    Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    InventoryItemHistoryNewScreenState2(inventoryItemId: values.id)))
        .then((value) {
      value == true ? _getData() : {};
    });
  }

  /// Delete Click and delete item
  /*onDelete(int index)async {
    var id = items[index].id;

    await inventoryRelationOperations.relationItemCountFromProductRelationByInventoryItemId(id).then((productCount)async{
      if(productCount==0){
        await historyItemOperations.deleteHistoryItemByInventoryItemId(id!).then((value)async {
        await inventoryItemOperations.deleteInventoryItem(id).then((value) {
          isInventoryItemAddedOrRemoved=true;
        setState(() { items.removeAt(index);});
      });
        });
        Navigator.of(context).pop(true);
      }
      else{
        Navigator.of(context).pop(false);
        context.showSnackBar("Can\''nt delete! Inventory is being used by some products");}

    });


  }*/

  /// Delete Click and delete item
  onDeleteAllDataAndInventoryItem(int index) async {
    var id = items[index].id;

    await inventoryRelationOperations
        .relationItemCountFromProductRelationByInventoryItemId(id)
        .then((productCount) async {
      if (productCount == 0) {
        await inventoryItemOperations
            .deleteAllDataAndInventoryItem(id)
            .then((value) {
          setState(() {
            items.removeAt(index);
            Navigator.of(context).pop(true);
          });
        }).onError((error, stackTrace) {
          Navigator.of(context).pop(false);
          print('onDeleteError  ${error}  ${stackTrace}');
        });
      } else {
        Navigator.of(context).pop(false);
        context.showSnackBar(
            "Can\''nt delete! Inventory is being used by some products");
      }
    });
  }

  ///edit User
  editUser(int id, BuildContext context) {
    if (teNameController.text.isNotEmpty) {
      var inventoryItem = InventoryItem();
      inventoryItem.id = id;
      inventoryItem.name = teNameController.text;

      inventoryItemOperations
          .updateRawInventoryItem(inventoryItem)
          .then((value) {
        teNameController.text = "";

        Navigator.of(context).pop();
        showtoast("Data Saved successfully");

        _getData();
      });
    } else {
      showtoast("Please fill all the fields");
    }
  }

  /// Edit Click
  onSelectItem(
      InventoryItem inventoryItem, bool checked, BuildContext context) {
    print("checkboxstate $checked");
    var inventItem = InventoryItem();
    inventItem.id = inventoryItem.id;
    inventItem.name = inventoryItem.name;

    inventoryItemOperations.updateInventoryItem(inventItem).then((value) {
      showtoast("Data Saved successfully");

      setState(() {});
    });
  }

  onSelectInventoryItem(InventoryItem inventoryItem) async {
    if (inventoryItem.isCostUpdated == 1) {
      setState(() {
        inventoryItem.isCostUpdated = 0;
      });

      await inventoryItemOperations
          .updateInventoryItemIsUpdatedStatus(inventoryItem)
          .then((value) => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => InventoryItemDetailsScreenState(
                              //inventory: widget.inventory,
                              inventoryItemId: inventoryItem.id,
                            ))).then((value) => {_getData()})
              });
      return;
    } else
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => InventoryItemDetailsScreenState(
                    //inventory: widget.inventory,
                inventoryItemId: inventoryItem.id,
                  ))).then((value) => {_getData()});
  }

  onEditInventoryItem(InventoryItem inventoryItem) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => EditInventoryItemScreenState(
                  inventory: widget.inventory,
                  inventoryItemId: inventoryItem.id,
                ))).then((value) => value == true ? {_getData()} : {});
  }

  showInventoryItemOperationsBottomSheet(StatefulWidget statefulWidget,{double maxHeight=0.75}) {
    print('statefulWidgetRunType ${statefulWidget.runtimeType}');
    MyBottomSheet().showBottomSheet(context,
        maxHeight:maxHeight, callback: (value) {
          if (value != null && value == true) {
            if(statefulWidget.runtimeType==SwitchInventoryCategoryScreenState)
              isInventoryItemAddedOrRemoved=true;
            isLongPressEnabled=false;
            inventoryItemIdArray.clear();
            inventoryIdArray.clear();
            _getData();
          }
        }, child: statefulWidget);
  }
/*
  showAddInventoryItemBottomSheet(){
    MyBottomSheet().showBottomSheet(context,maxHeight: 0.75, callback: (value){
      if(value!=null&&value==true){
        _getData();
      }
    },child:


    AddInventoryItemScreenState(inventory: widget.inventory,)
    );
  }
  showInventoryItemDetailsBottomSheet(InventoryItem inventoryItem){
    MyBottomSheet().showBottomSheet(context,maxHeight: 0.75, callback: (value){
      if(value!=null&&value==true){
        _getData();
      }
    },child:


    InventoryItemDetailsScreenState(inventory: widget.inventory,inventoryItem: inventoryItem,)
    );
  }
  showEditInventoryItemBottomSheet(InventoryItem inventoryItem){
    MyBottomSheet().showBottomSheet(context,maxHeight: 0.80, callback: (value){
      if(value!=null&&value==true){
        _getData();
      }
    },child:


    EditInventoryItemScreenState(inventory: widget.inventory,inventoryItem: inventoryItem,)
    );
  }*/

  var isLongPressEnabled = false;
  var inventoryItemIdArray = <int>[];
  var inventoryIdArray = <int>[];
  void toggleSelection(InventoryItem values) {
    setState(() {
      if (values.isSelected==0) {
        values.isSelected=1;
        inventoryItemIdArray.add(values.id!);
        print('inventoryItemIdAArray $inventoryItemIdArray');
      } else {
        values.isSelected=0;
        inventoryItemIdArray.remove(values.id!);
        print('inventoryItemIdAArray $inventoryItemIdArray');
        final isAllSelectionClear = items.map((e) => e.isSelected).every((element) => element==0);
        if(isAllSelectionClear){
          inventoryItemIdArray.clear();
          isLongPressEnabled=!isAllSelectionClear;
        }

        print('toggleTest $isAllSelectionClear');

      }
    });
  }

}
