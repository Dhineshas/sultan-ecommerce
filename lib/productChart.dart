
/*import 'dart:math';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/database/inventory_item_operations.dart';
import 'package:ecommerce/model/InventoryItem.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:draw_graph/draw_graph.dart';
import 'package:draw_graph/models/feature.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

class ProductChart extends StatefulWidget {
  const ProductChart({Key? key}) : super(key: key);

  @override
  State<ProductChart> createState() => _ProductChartState();
}

class _ProductChartState extends State<ProductChart> {

  List<Feature> features = [];

  final monthDateFormat = DateFormat('MMM');

  // final List<Feature> features = [
  //   Feature(
  //     title: "Drink Water",
  //     color: Colors.primaries[Random().nextInt(Colors.primaries.length)],//Colors.blue,
  //     data: [0.2, 0.8, 0.4, 0.7, 0.6],
  //   ),
  //   Feature(
  //     title: "Exercise",
  //     color: Colors.primaries[Random().nextInt(Colors.primaries.length)],//Colors.pink,
  //     data: [1, 0.8, 0.6, 0.7, 0.3],
  //   ),
  //   Feature(
  //     title: "Study",
  //     color: Colors.primaries[Random().nextInt(Colors.primaries.length)],//Colors.cyan,
  //     data: [0.5, 0.4, 0.85, 0.4, 0.7],
  //   ),
  //   Feature(
  //     title: "Water Plants",
  //     color: Colors.primaries[Random().nextInt(Colors.primaries.length)],//Colors.green,
  //     data: [0.6, 0.2, 0, 0.1, 1],
  //   ),
  //   Feature(
  //     title: "Exercise One",
  //     color: Colors.primaries[Random().nextInt(Colors.primaries.length)],//Colors.pink,
  //     data: [1, 0.8, 0.6, 0.7, 0.3],
  //   ),
  //   Feature(
  //     title: "Grocery Shopping",
  //     color: Colors.primaries[Random().nextInt(Colors.primaries.length)],//Colors.amber,
  //     data: [0.25, 1, 0.3, 0.8, 0.6],
  //   ),
  //   Feature(
  //     title: "Drink Water One",
  //     color: Colors.primaries[Random().nextInt(Colors.primaries.length)],//Colors.blue,
  //     data: [0.2, 0.8, 0.4, 0.7, 0.6],
  //   ),
  // ];

  @override
  void initState() {

    Future.delayed(Duration(milliseconds: 1), ()
    {
      _getData();
    });

    super.initState();
  }


  var inventoryHistoryitems = <InventoryItem>[];
  var inventoryItems = <InventoryItem>[];
  var graphMonthsX = ['Jan', 'Feb', 'Mar', 'Apr', 'May','Jun', 'Jul', 'Aug', 'Sep', 'Oct','Nov','Dec'];//<String>[];
  var graphCostsY = <String>[];
  var graphCostsValuesY = <double>[];
  bool isLoading = true;
  var inventoryItemOperations = InventoryItemOperations();


  final inventoryItemsnames = ['no data'];
  final picker = ImagePicker();
  int selectedTypeIndex = 0;
  String dropdownSelectedTypeValue = 'no data';


  @override
  Widget build(BuildContext context) {

    final toggleButton = Container(
        width: 215,
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.grey[350],
        ),
        child: Center(
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: inventoryItems.length,
            itemBuilder: (BuildContext context, int index) => InkWell(
              child: Card(
                color: selectedTypeIndex == index
                    ? Colors.white
                    : Colors.grey[350],
                elevation: selectedTypeIndex == index ? 5 : 0,
                child: Container(
                  width: 100,
                  child: Center(child: Text('${inventoryItems[index].name}')),
                ),
              ),
              onTap: () => setState(() {
                selectedTypeIndex = index;
              }),
            ),
          ),
        ));

    final addDropDownButton = FadeInUp(delay: Duration(milliseconds: 300),child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
        elevation: 5,
        child: Padding(
            padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
            child: DropdownButton<String>(
              value: dropdownSelectedTypeValue,
              icon: const Icon(Icons.keyboard_arrow_down),
              elevation: 8,
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              style: const TextStyle(color: Colors.black),
              underline: Container(
                height: 2,
                color: Colors.blue,
              ),
              onChanged: (String? newValue) {
                setState(() {
                  selectedTypeIndex = inventoryItemsnames.indexOf(newValue.toString());
                  dropdownSelectedTypeValue = newValue!;

                  _getHistoryData().then(
                          (value) => _getMonthlyCostData().then(
                              (value) => _setData()
                      )
                  );
                  // print('onchaged selectedTypeIndex = $selectedTypeIndex && dropdownSelecTypVal = $dropdownSelectedTypeValue');
                  // _setData();
                });
              },
              items: inventoryItemsnames.map((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            )))
    );
    
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Sales Analysis',
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
        ),
        body:(this.features.isNotEmpty && this.features.length > 0)//(this.inventoryItems.isNotEmpty && this.inventoryHistoryitems.isNotEmpty && this.inventoryItems.length > 0 && this.inventoryHistoryitems.length > 0)
            ?
        SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 32.0),
                child: Text(
                  "Tasks Track",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 2,
                  ),
                ),
              ),
              addDropDownButton,
              LineGraph(
                features: features,
                size: Size(500, 500),
                labelX: this.graphMonthsX,//['JAN', 'FEB', 'MAR', 'APR', 'MAY','JUN', 'JUL', 'AUG', 'SEP', 'OCT','NOV','DEC'],
                labelY: this.graphCostsY,//['20%', '40%', '60%', '80%', '100%'],
                showDescription: true,
                graphColor:  Colors.grey,//Colors.primaries[Random().nextInt(Colors.primaries.length)],//Colors.grey,
                graphOpacity: 0.1,
                verticalFeatureDirection: false,
                // descriptionHeight: 250,
              ),
              SizedBox(
                height: 10,
              )
            ],
          )),
        )
           :
            Center(child: Text('no elements')),
    );

  }

  Future<List<InventoryItem>> _getData() async
  {
    await inventoryItemOperations.inventoriesItems().then((value)
    {
      var items = value;
      setState(() {
        this.inventoryItems = value;
      });
    });
    if(inventoryItems.isNotEmpty)
      {
        _getHistoryData().then(
                (value) => _getMonthlyCostData().then(
                        (value) => _setData()
                )
        );
        // _setData();
      }
    return this.inventoryItems;
  }
  void _setData() async
  {
    if(this.inventoryItems.length > 0) {
      // print('data count inv ${inventoryItems.length}');
      // print('data count feat ${features.length}');

      List<List<double>> rand  = [[0.2, 0.0, 0.4, 0.7, 0.6,1, 0.8, 0.6, 0.0, 0.3, 0.4, 0.7, 0.6,1]];//,[1, 0.8, 0.6, 0.4, 0.0, 0.4, 0.7, 0.6, 0.7, 0.6,1, 0.0, 0.3]];

      setState(()
      {
        int randomIndex = Random().nextInt(rand.length);
        features.add(Feature(
            color: Colors.primaries[Random().nextInt(Colors.primaries.length)],
            title: inventoryItems[this.selectedTypeIndex].name.toString(),
            data: this.graphCostsValuesY
        ));
        if(features.length > 1)
          {
            features.removeAt(0);
          }
        if(inventoryItemsnames.first == 'no data')
          {
            inventoryItemsnames.removeAt(0);
            inventoryItems.forEach((element) {
              this.inventoryItemsnames.add(element.name.toString());
            });

          }
        dropdownSelectedTypeValue = this.inventoryItemsnames[this.selectedTypeIndex];
      });
    }
    else
      {
        print('no data foud');
      }
  }
  Future<List<InventoryItem>> _getHistoryData() async
  {
    await inventoryItemOperations.inventoriesItemsByHistory(this.inventoryItems[this.selectedTypeIndex].id).then((value)
    {
      // setState(() {
        inventoryHistoryitems = value;
      // });
      print('data foud ${inventoryHistoryitems.length}');
    });
    setState(() {
      isLoading = false;
    });
    return inventoryHistoryitems;
  }

  Future<List<String>> _getMonthlyCostData() async
  {
    var graphCostsYTemp = <String>[];

    print('data foud Name ${inventoryHistoryitems.first.name}');
    this.graphMonthsX.forEach((elementM)
    {
      var monthlyTotal = 0.0;
      this.inventoryHistoryitems.forEach((elementH)
      {
        if(monthDateFormat.format(DateTime.fromMillisecondsSinceEpoch(elementH.historyAddedDate!)) == elementM)
        {
          monthlyTotal = monthlyTotal+(elementH.historyInventoryItemTotalType!*elementH.historyInventoryItemCostPerType!).toDouble();
          // print('$elementM && ${elementH.historyAddedDate}');
        }
      });
      graphCostsYTemp.add(monthlyTotal.toString());
      print('$elementM of total = ${monthlyTotal}');
    });
    setState(() {

      inventoryHistoryitems = inventoryHistoryitems;
      graphCostsYTemp.forEach((element) { 
        this.graphCostsValuesY.add(double.parse(element));
      });
      // this.graphCostsValuesY = graphCostsYTemp.cast<double>();
      // print('graph Array Values= ${this.graphCostsValuesY}');
      graphCostsYTemp.sort();
      print('graph Array Values after sort = ${graphCostsYTemp}');
      var highestNum = double.parse(graphCostsYTemp.last);
      highestNum = highestNum+(100 - (highestNum % 100));

      print('Next Best Value = ${highestNum}');
      var tenDivArray = <int>[];
      tenDivArray.add(0);
      var addingVal = (highestNum*0.10).toInt();

      print('addingVal = ${addingVal}');
      for(int i = 0; i < 10; i++)
        {
          tenDivArray.add(addingVal);
          addingVal+=tenDivArray[1];
        }
      setState(() {
        this.graphCostsY.clear();
        tenDivArray.forEach((element) {
          this.graphCostsY.add(element.toString() + ' QR') ;
        });
        print('graph y elements = ${this.graphCostsY.toList()}');
      });
      // this.graphCostsValuesY.forEach((element) {
      //   tenDivArray.add(addingVal);
      //   tenDivArray.add(addingVal+((highestNum*0.10).toInt()));
      // });
      // print('graph Y head Values= ${this.graphCostsY}');
    });
    return this.graphCostsY;
  }
}*/
