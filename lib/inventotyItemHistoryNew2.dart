import 'dart:convert';
import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/inventoryItemStockBarChart.dart';
import 'package:ecommerce/model/Inventory.dart';
import 'package:ecommerce/upgrade.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/MyCustomSlidableAction.dart';
import 'package:ecommerce/utils/Subscription.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';

import 'inventoryItemPurchaseLineChart.dart';
import 'inventoryUnitMeasures.dart';
import 'database/history_operations.dart';
import 'database/inventory_item_operations.dart';
import 'database/inventory_operations.dart';
import 'database/product_item_operations.dart';
import 'database/relation_operations.dart';
import 'model/History.dart';
import 'model/InventoryItem.dart';

class InventoryItemHistoryNewScreenState2 extends StatefulWidget {
  final int? inventoryItemId;

  const InventoryItemHistoryNewScreenState2(
      {@required this.inventoryItemId, Key? key})
      : super(key: key);

  @override
  _InventoryItemHistoryNewScreenState createState() =>
      _InventoryItemHistoryNewScreenState();
}

class _InventoryItemHistoryNewScreenState
    extends State<InventoryItemHistoryNewScreenState2> {
  @override
  void initState() {
    if (widget.inventoryItemId != null)
      Future.delayed(Duration(milliseconds: 1000), () {
        _getInventoryDetailsData();
      });

    super.initState();
    print("INIT FFF");
    // _scrollController = new ScrollController();
    //_scrollController.addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
            title: MyWidgets.textView(text:
            'Inventory History',
              style: TextStyle(color: Colors.black),
            ),

            leading: IconButton(color: MyColors.black_0101,
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, true),
            ),
            actions: <Widget>[IconButton(
              icon: Icon(
                Icons.multiline_chart_rounded,
                color: MyColors.black_0101,
              ), onPressed: () async {

              await validateSubscriptionAndNavigate(InventoryItemPurchaseLineChart(inventoryItemId: widget.inventoryItemId,));


            },),
              IconButton(
                icon: Icon(
                  Icons.stacked_bar_chart,
                  color: MyColors.black_0101,
                ), onPressed: () async {
                await validateSubscriptionAndNavigate(InventoryItemStockBarChart(inventoryItemId: widget.inventoryItemId,));
              },)
            ]
        ),
        body:
        createListView(context));
  }

  Future validateSubscriptionAndNavigate(Widget widget)async{
    await Subscription.getActiveSubscription().then((value)async {
      if(value==0){

        await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    UpgradeScreenState()));

      }else await  goto(context,widget);
    });

  }

  var items = <InventoryItem>[];

  final _listKey = GlobalKey<AnimatedListState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  var _scrollController = ScrollController();

  var inventoryOperations = InventoryOperations();
  var inventoryItemOperations = InventoryItemOperations();
  var historyItemOperations = HistoryOperations();
  var relationOperations = RelationOperations();
  var inventoryRelationOperations = RelationOperations();
  var productItemOperations = ProductItemOperations();

  final purchaseDateFormat = DateFormat('dd-MMM-yyyy');

  /// Get all users data
  /* getAllInventory() {
    return FutureBuilder(
        future: _getData(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return createListView(context, snapshot);
        });
  }*/

  ///Fetch data from database
  Future<List<InventoryItem>> _getInventoryItemHistoryData() async {
    if (inventoryItem != null)
      await inventoryItemOperations
          .inventoriesItemsByHistory(widget.inventoryItemId)
          .then((value) {
        items = value;
      });

    items.forEach((element) {
      print('InventoryItems type: ${element.historyInventoryItemType},total type : ${element.historyInventoryItemTotalType}, pieces in type : ${element.historyInventoryItemPiecesInType}, cost per type ${element.historyInventoryItemCostPerType}, cost per item in type ${element.historyInventoryItemCostPerItemInType}, stock ${element.historyAddedStock}  ');
    });
    setState(() {
      isLoading = false;
    });

/*await historyItemOperations.history().then((value) =>
value.forEach((element) {print('adddedHistory  ${element.id}  ${element.inventoryItemId}  ${element.updatedQuantity}');})
);*/
    return items;
  }

  InventoryItem? inventoryItem;

  Future<InventoryItem> _getInventoryDetailsData() async {
    await inventoryItemOperations
        .inventoriesItemsByInventoryItemId(widget.inventoryItemId)
        .then((value) {
      inventoryItem = value.first;

      _getInventoryItemHistoryData();
    });

    return inventoryItem!;
  }

  bool isLoading = true;

  ///create List View with
  Widget createListView(BuildContext context) {
    if (items.isNotEmpty) {
      return FadeIn(
          child: Container(
              height: double.infinity,
              width: double.infinity,
              child:SafeArea(bottom: true,child:  ListView.builder(
                  physics: AlwaysScrollableScrollPhysics(
                      parent: BouncingScrollPhysics()),
                  key: _listKey,
                  controller: _scrollController,
                  shrinkWrap: true,
                  itemCount: items.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _buildListItem(context, items[index],index);

                    // return Dismissible(
                    //   background: Container(
                    //     color: (Colors.teal[900])!,
                    //     child: Padding(
                    //       padding: const EdgeInsets.all(15),
                    //       child: Row(
                    //         children: [
                    //           Icon(Icons.edit_outlined, color: Colors.white),
                    //           Text('Edit', style: TextStyle(color: Colors.white)),
                    //         ],
                    //       ),
                    //     ),
                    //   ),
                    //   secondaryBackground: Container(
                    //     color: Colors.red,
                    //     child: Padding(
                    //       padding: const EdgeInsets.all(15),
                    //       child: Row(
                    //         mainAxisAlignment: MainAxisAlignment.end,
                    //         children: [
                    //           Icon(Icons.delete_forever_outlined,
                    //               color: Colors.white),
                    //           Text('Delete',
                    //               style: TextStyle(color: Colors.white)),
                    //         ],
                    //       ),
                    //     ),
                    //   ),
                    //   key: Key(items[index].name!),
                    //   confirmDismiss: (DismissDirection direction) async {
                    //     if (direction == DismissDirection.endToStart) {
                    //       /*return await showDialog(
                    //         context: context,
                    //         builder: (BuildContext context) {
                    //           if(Platform.isIOS){
                    //             return  CupertinoAlertDialog(
                    //               //title: Text(msg),
                    //               title: const Text("Confirm"),
                    //               content: const Text("Are you sure you wish to delete this item?"),
                    //               actions: <Widget>[
                    //                 CupertinoDialogAction(
                    //                   child: Text('DELETE'),
                    //                   onPressed: () => {onDelete(index),
                    //                     Navigator.of(context).pop(true)
                    //                   },
                    //                 ),
                    //                 CupertinoDialogAction(
                    //                   child: Text('CANCEL'),
                    //                   onPressed: () => Navigator.of(context).pop(false),
                    //                 ),
                    //               ],
                    //             );}else return AlertDialog(
                    //             shape: RoundedRectangleBorder(
                    //               borderRadius: BorderRadius.circular(15),
                    //             ),
                    //             title: const Text("Confirm"),
                    //             content: const Text("Are you sure you wish to delete this item?"),
                    //             actions: <Widget>[
                    //               TextButton(
                    //                   onPressed: () => {onDelete(index),
                    //                     Navigator.of(context).pop(true)
                    //                   },
                    //                   child: const Text("DELETE")
                    //               ),
                    //               TextButton(
                    //                 onPressed: () => Navigator.of(context).pop(false),
                    //                 child: const Text("CANCEL"),
                    //               ),
                    //             ],
                    //           );
                    //         },
                    //       );*/
                    //
                    //       return await showCustomCallbackAlertDialog(
                    //           context: context,
                    //           positiveText: 'Delete',
                    //           msg:
                    //               'Are you sure you wish to delete all project related data?',
                    //           positiveClick: () {
                    //             onDelete(index);
                    //             Navigator.of(context).pop(true);
                    //           },
                    //           negativeClick: () {
                    //             Navigator.of(context).pop(false);
                    //           });
                    //     } else {
                    //       onEditInventory(items[index]);
                    //     }
                    //   },
                    //   child: _buildItem(context, items[index], index),
                    // );
                  }),)
          ));
    } else
      return isLoading
          ? MyWidgets.buildCircularProgressIndicator()
          : Center(
        child: Text(
          'Inventory History is empty.',
          style: TextStyle(
            fontSize: 17.0,
            color: Colors.grey,
          ),
        ),
      );
  }

  ///Construct cell for List View
  Widget _buildItem(BuildContext context, InventoryItem values, int index) {
    return Container(
      child: ListTile(
          onTap: () {},
          title: Column(children: <Widget>[
            Row(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      child: values.imageBase != null &&
                          values.imageBase.toString().isNotEmpty
                          ? ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.memory(
                          base64Decode(values.imageBase.toString()),
                          width: 80,
                          height: 80,
                          fit: BoxFit.fitHeight,
                          gaplessPlayback: true,
                        ),
                      )
                          : Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(10)),
                        width: 80,
                        height: 80,
                      ),
                    )
                  ],
                ),
                Padding(padding: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0)),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 0.0, 5.0)),
                    Container(
                      width: 150,
                      child: RichText(
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        strutStyle: StrutStyle(fontSize: 12.0),
                        text: TextSpan(
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 15,
                              color: Colors.black),
                          text: values.name!,
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),
                    Text(
                      '${values.historyInventoryItemTotalType} ${InventoryUnitMeasure.inventoryUnitMeasureArray[values.itemType!]}',
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 15.0,
                        color: Colors.green,
                      ),
                      textAlign: TextAlign.left,
                      maxLines: 2,
                    ),
                    Padding(padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),
                    Text(
                      'Purchased: ${purchaseDateFormat.format(DateTime.fromMillisecondsSinceEpoch(values.historyAddedDate!))}',
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: 15.0,
                        color: Colors.black,
                      ),
                      textAlign: TextAlign.left,
                      maxLines: 2,
                    ),
                    Padding(padding: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 10.0)),
                  ],
                ),
                Spacer(), /*Column(
                    children: <Widget>[
                      IconButton(
                          color: Colors.black,
                          icon: new Icon(Icons.edit),
                          onPressed: () => onEdit(values, index, context)),
                      IconButton(
                          color: Colors.black,
                          icon: new Icon(Icons.delete),
                          onPressed: () => onDelete(index)),
                    ],
                  )*/
              ],
            ),
            Divider(
              height: 1,
            )
          ])),
    );
  }


  ///Construct cell for List View
  Widget _buildListItem(BuildContext context, InventoryItem values, int index) {
    return Padding(
        padding: EdgeInsets.only(left: 0, right: 10),
        child: MySlidableWidgets.slidable(

          // The end action pane is the one at the right or the bottom side.
            endActionPane: ActionPane(
              motion: BehindMotion(),
              extentRatio: 0.25,
              children: <Widget>[
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    onEditInventory(items[index]);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: Container(
                      margin: EdgeInsets.only(left: 1),
                      height: 94,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.recycling_outlined,
                            size: 20,
                            color: MyColors.white,
                          ),
                          Text(
                            'Edit',
                            style: MyStyles.customTextStyle(
                              fontSize: 11,
                              color: MyColors.white,
                            ),
                          ),
                        ],
                      ),
                      decoration: BoxDecoration(
                        color: MyColors.grey_9d_bg,
                        boxShadow: [
                          BoxShadow(
                            color: MyColors.grey_9d_bg,
                            blurRadius: 1.0,
                            offset: Offset(0.7, 0),
                          ),
                        ],
                      )),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) async {
                    onDelete(index);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: Container(
                      margin: EdgeInsets.only(right: 2),
                      height: 94,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.close_rounded,
                            size: 20,
                            color: MyColors.white,
                          ),
                          Text(
                            'Delete',
                            style: MyStyles.customTextStyle(
                              fontSize: 11,
                              color: MyColors.white,
                            ),
                          )
                        ],
                      ),
                      decoration: BoxDecoration(
                        color: MyColors.red_ff_bg,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(0),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(0),
                            bottomRight: Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                            color: MyColors.grey_9d_bg,
                            blurRadius: 1.0,
                            offset: Offset(0.7, 0),
                          ),
                        ],
                      )),
                ),
              ],
            ),

            // The child of the Slidable is what the user sees when the
            // component is not dragged.
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 0),
              child: ListTile(
                  hoverColor: MyColors.transparent,
                  contentPadding: EdgeInsets.all(0),

                  onTap: () {

                  },
                  title: MyWidgets.shadowContainer(
                      height: 95,
                      paddingL: 10,
                      // spreadRadius: 2,
                      cornerRadius: 10,
                      shadowColor: MyColors.white_f1_bg,
                      color: values.isSelected==1?MyColors.white_chinese_e0_bg:MyColors.white,
                      child: Row(
                        children: [
                          Container(
                              width: 66.0,
                              height: 66.0,
                              child: values.imageBase != null &&
                                  values.imageBase.toString().isNotEmpty
                              //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                                  ? ClipRRect(
                                borderRadius: BorderRadius.circular(8),
                                child: Image.memory(
                                  base64Decode(
                                      values.imageBase.toString()),
                                  fit: BoxFit.cover,
                                  gaplessPlayback: true,
                                ),
                              )
                                  : ClipRRect(
                                borderRadius: BorderRadius.circular(8),
                                child: Container(
                                  color: MyColors.grey_70,
                                ),
                              )
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                              flex: 9,
                              child: Container(
                                  margin: EdgeInsets.only(top: 5, bottom: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Spacer(),
                                      MyWidgets.textView(text:
                                      values.historyInventoryItemFlag!,
                                        maxLine: 1,
                                        style: MyStyles.customTextStyle(
                                            fontSize: 15,
                                            color: MyColors.black,
                                            fontWeight: FontWeight.w500),
                                        textAlign: TextAlign.left,
                                      ),

                                      Spacer(),
                                      MyText.priceCurrencyText(context,
                                          price:
                                          'Cost: ${values.historyInventoryItemCostPerItemInType}',
                                          fontSize: 15,
                                          fontWeight: FontWeight.normal,
                                          fontColor: MyColors.grey_9d_bg),
                                      Spacer(),
                                      values.historyInventoryItemType == 0
                                          ? MyText.priceCurrencyText(context,
                                          price:
                                          'Stock: ${values.historyAddedStock!} Items',
                                          exponent: '(Box)',
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold)
                                          : MyWidgets.textView(text:
                                      'Stock: ${values.historyAddedStock!} ${InventoryUnitMeasure.inventoryUnitMeasureArray[values.historyInventoryItemType!]}',
                                        style: MyStyles.customTextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w400),
                                        textAlign: TextAlign.left,
                                        maxLine: 2,

                                      ),
                                      Spacer(),
                                      MyWidgets.textView(text:
                                      'Purchased: ${purchaseDateFormat.format(DateTime.fromMillisecondsSinceEpoch(values.historyAddedDate!))}',
                                        style: MyStyles.customTextStyle(
                                          fontWeight: FontWeight.w300,
                                          fontSize: 15.0,
                                          color: MyColors.grey_9d_bg,
                                        ),
                                        textAlign: TextAlign.left,
                                        maxLine: 2,
                                      ),
                                    ],
                                  ))
                          ),
                          Container(
                            width: 45,
                            decoration: BoxDecoration(
                              color: MyColors.white_chinese_e0_bg,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10),
                                  bottomRight: Radius.circular(10)),
                            ),
                            child: Center(
                              child: Icon(
                                Icons.chevron_right_rounded,
                                size: 30,
                              ),
                            ),
                          ),
                        ],
                      )
                  )
              ),
            )
        )
    );

    // return InkWell(
    //     onTap: () async {
    //       onSelectInventoryItem(values);
    //     },
    //     child: Container(
    //         margin: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
    //         decoration: BoxDecoration(
    //             border: Border.all(
    //               color: values.isCostUpdated == 1
    //                   ? (Colors.redAccent)
    //                   : (Colors.teal[900])!,
    //             ),
    //             borderRadius: BorderRadius.all(Radius.circular(15))),
    //         child: Column(
    //           mainAxisSize: MainAxisSize.max,
    //           children: <Widget>[
    //             Row(
    //                 mainAxisSize: MainAxisSize.max,
    //                 crossAxisAlignment: CrossAxisAlignment.start,
    //                 children: <Widget>[
    //                   //image
    //                   Container(
    //                     margin: EdgeInsets.fromLTRB(5, 5, 10, 5),
    //                     child: values.imageBase != null &&
    //                             values.imageBase.toString().isNotEmpty
    //                         ? ClipRRect(
    //                             borderRadius: BorderRadius.circular(10),
    //                             child: Image.memory(
    //                               base64Decode(values.imageBase.toString()),
    //                               width: 80,
    //                               height: 80,
    //                               fit: BoxFit.fitHeight,
    //                               gaplessPlayback: true,
    //                             ),
    //                           )
    //                         : Container(
    //                             decoration: BoxDecoration(
    //                                 color: Colors.grey[200],
    //                                 borderRadius: BorderRadius.circular(10)),
    //                             width: 80,
    //                             height: 80,
    //                           ),
    //                   ),
    //                   //name and count
    //
    //                   Column(
    //                     crossAxisAlignment: CrossAxisAlignment.start,
    //                     mainAxisSize: MainAxisSize.min,
    //                     children: <Widget>[
    //                       Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0)),
    //                       Container(
    //                         width: 200,
    //                         child: RichText(
    //                           maxLines: 1,
    //                           overflow: TextOverflow.ellipsis,
    //                           strutStyle: StrutStyle(fontSize: 12.0),
    //                           text: TextSpan(
    //                             style: TextStyle(
    //                                 fontWeight: FontWeight.w500,
    //                                 fontSize: 18,
    //                                 color: Colors.black),
    //                             text: values.name!,
    //                           ),
    //                         ),
    //                       ),
    //                       Padding(
    //                           padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),
    //                       Text(
    //                         //'Cost: ${double.parse(((values.costPerTypeNew! / values.piecesInType!)).toStringAsFixed(2))} QAR / Item',
    //                         values.itemType == 0
    //                             ? 'Cost: ${values.costPerItemInTypeNew} QAR / Item'
    //                             : 'Cost: ${values.costPerItemInTypeNew} QAR / ${InventoryUnitMeasure.inventoryUnitMeasureArray[values.itemType!]}',
    //                         style: TextStyle(
    //                           fontWeight: FontWeight.w300,
    //                           fontSize: 15.0,
    //                           color: Colors.black,
    //                         ),
    //                         textAlign: TextAlign.left,
    //                         maxLines: 2,
    //                       ),
    //                     ],
    //                   ),
    //                   Spacer(),
    //                 ]),
    //             Row(
    //               mainAxisAlignment: MainAxisAlignment.center,
    //               children: [
    //                 Flexible(
    //                     flex: 1,
    //                     fit: FlexFit.tight,
    //                     child: Stack(
    //                         alignment: Alignment.center,
    //                         children: <Widget>[
    //                           Container(
    //                             height: 35,
    //                             decoration: BoxDecoration(
    //                               borderRadius: BorderRadius.only(
    //                                   bottomLeft: Radius.circular(14)),
    //                               color: Colors.grey,
    //                             ), //BoxDecoration
    //                           ),
    //                           Text(
    //                             "Stock: ${values.stock} Items",
    //                             style: TextStyle(
    //                               fontWeight: FontWeight.w500,
    //                               fontSize: 15.0,
    //                               color: Colors.white,
    //                             ),
    //                           )
    //                         ]) //Container
    //                     ),
    //                 Flexible(
    //                     flex: 1,
    //                     fit: FlexFit.tight,
    //                     child: InkWell(
    //                       onTap: () {
    //                         onHistoryClick(values);
    //                       },
    //                       child: Stack(
    //                         alignment: Alignment.center,
    //                         children: <Widget>[
    //                           Container(
    //                             height: 35,
    //                             decoration: BoxDecoration(
    //                               borderRadius: BorderRadius.only(
    //                                   bottomRight: Radius.circular(14)),
    //                               color: (Colors.teal[900])!,
    //                             ), //BoxDecoration
    //                           ),
    //                           Text(
    //                             "History",
    //                             style: TextStyle(
    //                               fontWeight: FontWeight.w500,
    //                               fontSize: 15.0,
    //                               color: Colors.white,
    //                             ),
    //                           )
    //                         ],
    //                       ),
    //                     ) //Container
    //                     )
    //               ],
    //             ),
    //           ],
    //         )));
  }
  ///On Item Click

  /// Delete Click and delete item
  onDelete(int index) async {
    var id = items[index].historyId;
    await historyItemOperations
        .deleteHistory(id!)
        .then((productCount) async {

      setState(() {
        items.removeAt(index);
      });
    });
  }

  ///edit User
  editUser(int id, BuildContext context) async {
    if (teInvOfTypeController.text.isNotEmpty) {
      var inventory = Inventory();
      inventory.id = id;
      inventory.name = teInvOfTypeController.text;

      await inventoryOperations.updateRawInventory(inventory).then((value) {
        teInvOfTypeController.text = "";

        Navigator.of(context).pop();
        showtoast("Data Saved successfully");

        _getInventoryItemHistoryData();
      });
    } else {
      showtoast("Please fill all the fields");
    }
  }

  /// Edit Click
  onSelectItem(Inventory inventory, bool checked, BuildContext context) {
    print("checkboxstate $checked");
    var invent = Inventory();
    invent.id = inventory.id;
    invent.name = inventory.name;

    inventoryOperations.updateInventory(invent).then((value) {
      showtoast("Data Saved successfully");

      setState(() {});
    });
  }

  /// Edit Click
  onEditInventory(InventoryItem inventoryItem) {
    openAlertBox(inventoryItem);
  }

  final teInvOfTypeController = TextEditingController();
  final teInvPiecesPerTypeController = TextEditingController();
  final teInvTotalCostPerTypeController = TextEditingController();

  /// openAlertBox to add/edit user
  openAlertBox(InventoryItem inventoryItem) async{
    teInvOfTypeController.text = '${inventoryItem.historyInventoryItemTotalType}';
    teInvPiecesPerTypeController.text =
    '${inventoryItem.historyInventoryItemPiecesInType}';
    teInvTotalCostPerTypeController.text =
    '${inventoryItem.historyInventoryItemCostPerType}';

    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            contentPadding: EdgeInsets.only(top: 15.0 ),
            content: Container(
              width: 300.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[


                  MyWidgets.textView(text:
                  /*flag ?*/
                  "Last updated details" /*: "Add User"*/,
                    style: MyStyles.customTextStyle(fontSize: 17.0,fontWeight: FontWeight.bold,color: MyColors.grey_70),
                  ),
                  SizedBox(height: 10,),

                  // Spacer(),
                  /*IconButton(
                        iconSize: 20,
                        icon: Icon(
                          Icons.close_rounded,
                          color: MyColors.grey_70,
                        ), onPressed: () {

                      },)*/


                  Divider(
                    color: Colors.grey,
                    height: 4.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 15.0, right: 15.0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child: Text(
                                    inventoryItem.itemType == 0 ?
                                      'No. of ${InventoryUnitMeasure.inventoryUnitMeasureArray[inventoryItem.itemType!]} added'
                                       : 'No. of Boxes added',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w200,
                                      )) //Container
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                  flex: 2,
                                  fit: FlexFit.tight,
                                  child: TextFormField(
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        FilteringTextInputFormatter.digitsOnly
                                      ],
                                      controller: teInvOfTypeController,
                                      obscureText: false,
                                      decoration: InputDecoration()) //Container
                              )
                            ],
                          ),
                          // inventoryItem.itemType == 0 ?
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child: Text(
                                      inventoryItem.itemType == 0 ?
                                      'No. of items per ${InventoryUnitMeasure.inventoryUnitMeasureArray[inventoryItem.itemType!]}'
                                      :'${InventoryUnitMeasure.inventoryUnitMeasureArray[inventoryItem.itemType!]} per box',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w200,
                                      )) //Container
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                  flex: 2,
                                  fit: FlexFit.tight,
                                  child: TextFormField(
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        FilteringTextInputFormatter
                                            .digitsOnly
                                      ],
                                      controller:
                                      teInvPiecesPerTypeController,
                                      obscureText: false,
                                      decoration:
                                      InputDecoration()) //Container
                              )
                            ],
                          ),
                          // : Container(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child: Text(
                                    inventoryItem.itemType == 0 ?
                                      'Cost per ${InventoryUnitMeasure.inventoryUnitMeasureArray[inventoryItem.itemType!]}'
                                        : 'Total Cost',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w200,
                                      )) //Container
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                  flex: 2,
                                  fit: FlexFit.tight,
                                  child: TextFormField(
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        FilteringTextInputFormatter.allow(
                                            RegExp(r'^\d+\.?\d{0,2}')),
                                      ],
                                      controller:
                                      teInvTotalCostPerTypeController,
                                      obscureText: false,
                                      decoration: InputDecoration()) //Container
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  InkWell(
                    onTap: () => {
                      if (_validate()) /*updateHistory(inventoryItem)*/ updateHistoryWithTransaction(
                          inventoryItem)
                    },
                    child: Container(
                      padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                      decoration: BoxDecoration(
                        color: MyColors.white_chinese_e0_bg,
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20.0),
                            bottomRight: Radius.circular(20.0)),
                      ),
                      child: MyWidgets.textView(text:
                      "Save Changes",
                        style: MyStyles.customTextStyle(color: MyColors.grey_70),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  ///update history and select and update inventory item and product item step by step
  updateHistory(InventoryItem inventoryItem) async {
    var costPerTypeControllerText =
    double.tryParse(teInvTotalCostPerTypeController.text.trim());
    double? currentCostPerItemInType = 0.00;
    currentCostPerItemInType = double.tryParse(
        (double.tryParse(teInvTotalCostPerTypeController.text.trim())! /
            int.parse(teInvPiecesPerTypeController.text.trim()))
            .toStringAsFixed(2));
    var history = History();
    history.id = inventoryItem.historyId;
    history.historyTotalType = int.tryParse(teInvOfTypeController.text.trim());
    history.historyPiecesInType =
        int.tryParse(teInvPiecesPerTypeController.text.trim());
    history.historyCostPerType = costPerTypeControllerText;
    history.historyCostPerItemInType = currentCostPerItemInType;

    await historyItemOperations.updateHistory(history).then((value) async {
      //check currently entered cost per item in box > inventory cost per item in box
      /* if(currentCostPerItemInType!>(widget.inventoryItem?.costPerItemInTypeNew)!){
      //update inventory with the currently entered cost per box and cost per item in box
      inventoryItem.costPerTypeOld=widget.inventoryItem?.costPerTypeNew;
      inventoryItem.costPerTypeNew=costPerTypeControllerText;


      inventoryItem.costPerItemInTypeOld=widget.inventoryItem?.costPerItemInTypeNew;
      inventoryItem.costPerItemInTypeNew=currentCostPerItemInType;
      history.historyCostPerItemInType=currentCostPerItemInType;
    }//check currently entered cost per item in box < inventory cost per item in box
    else if(currentCostPerItemInType<(widget.inventoryItem?.costPerItemInTypeNew)!){
      // 1st remove the current inventory history from array
      // 2nd update inventory with max of cost per box and cost per item in box from array
      inventoryItem.costPerTypeOld=widget.inventoryItem?.costPerTypeOld;


      inventoryItem.costPerItemInTypeOld=  widget.inventoryItem?.costPerItemInTypeOld;

      var items2 = <InventoryItem>[];
      items2.clear();
      items2.addAll(items);
      // items2.forEach((element) {print('hgftrdtrdrd   ${element.historyInventoryItemCostPerItemInType}');});

      items2.remove(inventoryItem);
      items2.sort((a, b) => a.costPerItemInTypeNew!.compareTo(b.costPerItemInTypeNew!));
      inventoryItem.costPerItemInTypeNew=items2.last.historyInventoryItemCostPerItemInType;
      history.historyCostPerItemInType=items2.last.historyInventoryItemCostPerItemInType;
      inventoryItem.costPerTypeNew=items2.last.historyInventoryItemCostPerType;
      // print('gfcgfcgfc  ${inventoryItem.costPerItemInTypeNew}');
      //items2.forEach((element) {print('hgftrdtrdrd   ${element.historyInventoryItemCostPerItemInType}');});

    }//check currently entered cost per item in box == inventory cost per item in box
    else  if(currentCostPerItemInType==(widget.inventoryItem?.costPerItemInTypeNew)!){
      //update the inventory cost per box and cost per item in box with same values

      inventoryItem.costPerTypeOld=widget.inventoryItem?.costPerTypeOld;
      inventoryItem.costPerTypeNew=widget.inventoryItem?.costPerTypeNew;

      inventoryItem.costPerItemInTypeOld= widget.inventoryItem?.costPerItemInTypeOld;
      inventoryItem.costPerItemInTypeNew=widget.inventoryItem?.costPerItemInTypeNew;
      history.historyCostPerItemInType=widget.inventoryItem?.costPerItemInTypeNew;
    }*/

      /*if(costPerTypeControllerText!>(widget.inventoryItem?.costPerTypeNew)!){
      inventoryItem.costPerTypeOld=widget.inventoryItem?.costPerTypeNew;
      inventoryItem.costPerTypeNew=costPerTypeControllerText;


    }else if(costPerTypeControllerText<(widget.inventoryItem?.costPerTypeNew)!){
      inventoryItem.costPerTypeOld=costPerTypeControllerText;
      inventoryItem.costPerTypeNew=widget.inventoryItem?.costPerTypeNew;


    }else if(costPerTypeControllerText==(widget.inventoryItem?.costPerTypeNew)!){
      inventoryItem.costPerTypeOld=widget.inventoryItem?.costPerTypeOld;
      inventoryItem.costPerTypeNew=widget.inventoryItem?.costPerTypeNew;

    }*/

      /*inventoryItem.isCostUpdated=costPerTypeControllerText!=(widget.inventoryItem?.costPerTypeNew)! ?1:0;
    inventoryItem.isPiecesInTypeUpdated=int.tryParse(teInvNoPiecesPerTypeController.text.trim())!=(widget.inventoryItem?.piecesInType)! ?1:0;

    var historyTotalItems=(inventoryItem.historyInventoryItemTotalType!*inventoryItem.historyInventoryItemPiecesInType!);
    var currentTotalItems=(int.tryParse(teInvNoOfTypeController.text.trim())!*int.tryParse(teInvNoPiecesPerTypeController.text.trim())!);
    if(historyTotalItems>currentTotalItems){
      if((widget.inventoryItem?.stock)!>=(historyTotalItems-currentTotalItems))
      inventoryItem.stock=(widget.inventoryItem?.stock)!-(historyTotalItems-currentTotalItems);
      else
        inventoryItem.stock=0;
    }else if(historyTotalItems<currentTotalItems){
      inventoryItem.stock=(widget.inventoryItem?.stock)!+(currentTotalItems-historyTotalItems);
    }else {
      inventoryItem.stock=(widget.inventoryItem?.stock);
    }

    inventoryItem.inventoryId = inventoryItem.inventoryId;*/

      _getInventoryItemHistoryData().then((value) async {
        // await inventoryItemOperations.inventoriesItemsByHistory(widget.inventoryItem?.id).then((value)async {

        if (items.isNotEmpty) {
          items.sort((a, b) => a.historyInventoryItemCostPerItemInType!
              .compareTo(b.historyInventoryItemCostPerItemInType!));
          InventoryItem maxCostInventoryItem = items.last;
          items.forEach((element) {
            print(
                'vfbevefhbv  ${element.historyInventoryItemCostPerItemInType}   ${maxCostInventoryItem.historyInventoryItemCostPerItemInType}');
          });

          inventoryItem.costPerTypeOld = inventoryItem.costPerTypeNew;
          inventoryItem.costPerTypeNew =
              maxCostInventoryItem.historyInventoryItemCostPerType;
          inventoryItem.costPerItemInTypeOld =
              inventoryItem.costPerItemInTypeNew;
          inventoryItem.costPerItemInTypeNew =
              maxCostInventoryItem.historyInventoryItemCostPerItemInType;
          // inventoryItem.piecesInType= maxCostInventoryItem.historyInventoryItemPiecesInType;
          inventoryItem.isCostUpdated =
          costPerTypeControllerText != (inventoryItem.costPerTypeNew)!
              ? 1
              : 0;
          inventoryItem.isPiecesInTypeUpdated =
          int.tryParse(teInvPiecesPerTypeController.text.trim()) !=
              (inventoryItem.piecesInType)!
              ? 1
              : 0;

          var historyTotalItems =
          (inventoryItem.historyInventoryItemTotalType! *
              inventoryItem.historyInventoryItemPiecesInType!);
          var currentTotalItems =
          (int.tryParse(teInvOfTypeController.text.trim())! *
              int.tryParse(teInvPiecesPerTypeController.text.trim())!);
          if (historyTotalItems > currentTotalItems) {
            if ((inventoryItem.stock)! >=
                (historyTotalItems - currentTotalItems))
              inventoryItem.stock = (inventoryItem.stock)! -
                  (historyTotalItems - currentTotalItems);
            else
              inventoryItem.stock = 0;
          } else if (historyTotalItems < currentTotalItems) {
            inventoryItem.stock = (inventoryItem.stock)! +
                (currentTotalItems - historyTotalItems);
          } else {
            inventoryItem.stock = (inventoryItem.stock);
          }
          inventoryItem.inventoryId = inventoryItem.inventoryId;

          //update inventory item with the updated inventory
          await inventoryItemOperations
              .updateInventoryItem(inventoryItem)
              .then((value) async {
            inventoryItem.isCostUpdated == 1 ||
                (currentCostPerItemInType !=
                    inventoryItem.costPerItemInTypeNew)
                ?
            //get all product id with inventory id used
            await inventoryRelationOperations
                .relationItemFromProductRelationByInventoryItemId(
                inventoryItem.id)
                .then((productIds) async {
              //recalculate and update all product item cost with updated inventory cost(above)
              if (productIds.isNotEmpty)
                await productItemOperations
                    .getProductItemIdAndRecalculateCostByProductItemId(
                    productIds)
                    .then((value) async {
                  clearControllerAndPop();
                });
              else
                clearControllerAndPop();
            })
                : clearControllerAndPop();
          });
        }
      });
    });
  }

  ///update history and select and update inventory item and product item with transaction(rollback the state on exception)- any doubt refer above step by step [updateHistory]
  updateHistoryWithTransaction(InventoryItem inventoryItem) async {
    var costPerTypeControllerText =
    double.tryParse(teInvTotalCostPerTypeController.text.trim());
    double? currentCostPerItemInType = 0.00;
    //currentCostPerItemInType=double.tryParse((double.tryParse(teInvCostPerTypeController.text.trim())!/int.parse(teInvNoPiecesPerTypeController.text.trim())).toStringAsFixed(2));

//calculate cost per item on type box and others in separate
    // if (inventoryItem.itemType == 0) {
    currentCostPerItemInType = double.tryParse(((double.tryParse(teInvTotalCostPerTypeController.text.trim())! / int.parse(teInvOfTypeController.text.trim()))/int.parse(teInvPiecesPerTypeController.text.trim())).toStringAsFixed(2));
    inventoryItem.isPiecesInTypeUpdated =
    int.tryParse(teInvPiecesPerTypeController.text.trim()) !=
        (inventoryItem.piecesInType)!
        ? 1
        : 0;
    /*} else {
      currentCostPerItemInType = double.tryParse(
          (double.tryParse(teInvTotalCostPerTypeController.text.trim())! /
                  int.parse(teInvOfTypeController.text.trim()))
              .toStringAsFixed(2));
      inventoryItem.isPiecesInTypeUpdated = 0;
    }*/

    var history = History();
    history.id = inventoryItem.historyId;
    history.historyTotalType = int.tryParse(teInvOfTypeController.text.trim());
    history.historyPiecesInType = int.tryParse(teInvPiecesPerTypeController.text.trim());
    history.historyCostPerType = costPerTypeControllerText;
    history.historyCostPerItemInType = currentCostPerItemInType;

    inventoryItem.costPerTypeOld = inventoryItem.costPerTypeNew;

    inventoryItem.costPerItemInTypeOld = inventoryItem.costPerItemInTypeNew;

    inventoryItem.isCostUpdated =
    costPerTypeControllerText != (inventoryItem.costPerTypeNew)! ? 1 : 0;
    //inventoryItem.isPiecesInTypeUpdated = int.tryParse(teInvNoPiecesPerTypeController.text.trim()) != (widget.inventoryItem?.piecesInType)! ? 1 : 0;

    var historyTotalItems = (inventoryItem.historyInventoryItemTotalType! *
        inventoryItem.historyInventoryItemPiecesInType!);
    var currentTotalItems = (int.tryParse(teInvOfTypeController.text.trim())! *
        int.tryParse(teInvPiecesPerTypeController.text.trim())!);
    if (historyTotalItems > currentTotalItems) {
      if ((inventoryItem.stock)! >= (historyTotalItems - currentTotalItems))
        inventoryItem.stock =
            (inventoryItem.stock)! - (historyTotalItems - currentTotalItems);
      else
        inventoryItem.stock = 0;
    } else if (historyTotalItems < currentTotalItems) {
      inventoryItem.stock =
          (inventoryItem.stock)! + (currentTotalItems - historyTotalItems);
    } else {
      inventoryItem.stock = (inventoryItem.stock);
    }

    history.historyStock=inventoryItem.stock;

    inventoryItem.inventoryId = inventoryItem.inventoryId;

    inventoryItem.isCostUpdated == 1 || (currentCostPerItemInType != inventoryItem.costPerItemInTypeNew)
        ? await historyItemOperations
        .updateHistoryWithTransactionAndAddHistory(inventoryItem, history)
        .then((value) {
      print('onUpdateHistorySuccess ');
      clearControllerAndPop();
    }).onError((error, stackTrace) {
      print('onUpdateHistoryError  ${error}  ${stackTrace}');
      clearControllerAndPop(isSuccess: false);
      context.showSnackBar('$error');
    })
        : await historyItemOperations.updateHistory(history).then((value) {
      clearControllerAndPop();
    }).onError((error, stackTrace) {
      clearControllerAndPop(isSuccess: false);
      context.showSnackBar('$error');
    });
  }

  clearControllerAndPop({bool isSuccess = true}) async {
    teInvOfTypeController.text = '';
    teInvPiecesPerTypeController.text = '';
    teInvTotalCostPerTypeController.text = '';
    Navigator.pop(context);
    if (isSuccess) _getInventoryItemHistoryData();
  }

  bool _validate() {
    if (teInvOfTypeController.text.isEmpty ||
        int.tryParse(teInvOfTypeController.text.trim()) == 0) {
      context.showSnackBar(
          "Enter No. of ${InventoryUnitMeasure.inventoryUnitMeasureArray[(inventoryItem?.itemType)!]}");
      return false;
    } else if (teInvPiecesPerTypeController.text.isEmpty ||
        int.tryParse(teInvPiecesPerTypeController.text.trim()) == 0) {
      context.showSnackBar(
          "Enter No. of Items in ${InventoryUnitMeasure.inventoryUnitMeasureArray[(inventoryItem?.itemType)!]}");
      return false;
    } else if (teInvTotalCostPerTypeController.text.isEmpty ||
        double.tryParse(teInvTotalCostPerTypeController.text.trim()) == 0) {
      context.showSnackBar(
          "Enter Cost per ${InventoryUnitMeasure.inventoryUnitMeasureArray[(inventoryItem?.itemType)!]}");
      return false;
    }
    return true;
  }
}

