import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/contactSupport.dart';
import 'package:ecommerce/database/firestore_operations.dart';
import 'package:ecommerce/database/user_operation.dart';
import 'package:ecommerce/main.dart';
import 'package:ecommerce/model/FirestoreUser.dart';
import 'package:ecommerce/model/UserCredentials.dart';
import 'package:ecommerce/signUpView.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/FirebaseUtils.dart';
import 'package:ecommerce/utils/InAppPurchaseHandler.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:ecommerce/webView.dart';
import 'package:flutter/material.dart';
import 'package:crypto/crypto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:flutter/services.dart';

import 'forgotPasswordView.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final teEmailController = TextEditingController();
  final tePasswordController = TextEditingController();

  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  bool isRememberMeSelected = false;
  bool isTermsAndConditionChecked = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MyColors.transparent.withOpacity(0.9),
        body: SingleChildScrollView(
            child: Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                    child: Image.asset('assets/images/login_top_view_map.png'),
                    fit: BoxFit.cover,
                  ),
                ),
                Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(40, 70, 0, 0),
                      child: Container(
                          height: 220,
                          width: MediaQuery.of(context).size.width * 0.55,
                          child: Text(
                            'Make your Business Easy and Professionals',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30,
                              color: Colors.black54,
                              // overflow: TextOverflow.clip
                            ),
                            // overflow: TextOverflow.visible,
                            // strutStyle: StrutStyle(fontSize: 30,fontWeight: FontWeight.w900),
                            textAlign: TextAlign.left,
                          )),
                    ),
                    Container(
                      // color: MyColors.grey_607,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 40,
                          ),
                          emailTextVw(context),
                          passwordTextVw(context),
                          rememberMeVw(context),
                          SignInOptionsVw(context),
                          contactUsVw(context),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            )));
  }

  /// Generates a cryptographically secure random nonce, to be included in a
  /// credential request.
  String generateNonce([int length = 32]) {
    final charset =
        '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._';
    final random = Random.secure();
    return List.generate(length, (_) => charset[random.nextInt(charset.length)])
        .join();
  }

  /// Returns the sha256 hash of [input] in hex notation.
  String sha256ofString(String input) {
    final bytes = utf8.encode(input);
    final digest = sha256.convert(bytes);
    return digest.toString();
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final emailTextFController = TextEditingController();
  final passwordTextFController = TextEditingController();
  final confirmPasswordTextFController = TextEditingController();

  bool _validatePassword(UserCredential cred)
  {
    if(cred.user?.email == null || cred.user?.email == '' || cred.user?.email?.contains('@privaterelay.appleid.com') == true)
    {
      showCustomAlertDialog(context, "We can't find your email id as you have opted hide your Email While sign in with Apple. If you are trying to sign in with Apple please login without hiding your email.To Sign in again with apple please remove Express POS from Settings->AppleAccount->Password&Security->Apps using AppleID. If problem still exist please contact us.");
      // context.showSnackBar("We can't find your email id as you have opted hide your Email. If you trying sign in with social media please login without hiding your email(do not select hide Email option.)");
      return false;
    }
     else if (passwordTextFController.text.isEmpty)
    {
      context.showSnackBar(
          "Please enter a password to continue");
      return false;
    }
    else if (RegExp(r"\s").hasMatch(passwordTextFController.text.toString()))
    {
      context.showSnackBar(
          "Please remove empty spaces");
      return false;
    }
    else if (passwordTextFController.text != confirmPasswordTextFController.text)
    {
      context.showSnackBar("Password do not match.");
      return false;
    }
    else if(passwordTextFController.text.length<=7)
    {
      context.showSnackBar(
          "Please enter minimum 8 character to continue");
      return false;
    }
    return true;
  }

  openAlertBoxForPassword(UserCredential cred) async
  {
    showNonCancellableCircularLoader(
        context, 'Please wait...');
    bool passwordExist = false;
    var fireStoreUser = FirestoreUser();
    fireStoreUser.userEmail=cred.user?.email;
    await fireStoreOperations.checkDocumentExist(fireStoreUser.userEmail).then((bool)async
    {
      if(bool)
      {
        await fireStoreOperations.checkDocumentPasswordExist(fireStoreUser.userEmail)
            .then((value) => {
              passwordExist = value
        });
      }
    });

    if(passwordExist)
      {
        saveCredential(cred);
        return;
      }
    Navigator.pop(context);
    print('not going down');
    emailTextFController.text = '${cred.user?.email}';
    passwordTextFController.text = '';
    confirmPasswordTextFController.text = '';

    //checking whether password is already stored in firestore for this email id.
    await showDialog(
        context: context,
        // barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            contentPadding: EdgeInsets.only(top: 15.0 ),
            content: Container(
              width: 300.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  MyWidgets.textView(text: "Please Enter a password" , style: MyStyles.customTextStyle(fontSize: 17.0,fontWeight: FontWeight.bold,color: MyColors.grey_70),),
                  SizedBox(height: 10,),
                  Divider(
                    color: Colors.grey,
                    height: 4.0,
                  ),
                  Padding(padding: EdgeInsets.all(10),
                    child: MyWidgets.textView(text: "Please provide a password so that you can login with Email & this password from any devices." , style: MyStyles.customTextStyle(fontSize: 15.0,fontWeight: FontWeight.normal,color: MyColors.grey_70),),
                  ),
                  SizedBox(height: 10,),
                  Padding(
                    padding: EdgeInsets.only(left: 15.0, right: 15.0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child: Text('Email',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w200,
                                      )) //Container
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                  flex: 2,
                                  fit: FlexFit.tight,
                                  child: Text('${cred.user?.email}',style: TextStyle(fontSize: 18,color: Colors.black),) //Container
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child: Text('New Password',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w200,
                                      )) //Container
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                  flex: 2,
                                  fit: FlexFit.tight,
                                  child: TextFormField(
                                      keyboardType: TextInputType.visiblePassword,
                                      controller: passwordTextFController,
                                      obscureText: false,
                                      decoration: InputDecoration()) //Container
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child: Text('Confirm Password',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w200,
                                      )) //Container
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                  flex: 2,
                                  fit: FlexFit.tight,
                                  child: TextFormField(
                                      keyboardType: TextInputType.visiblePassword,
                                      controller: confirmPasswordTextFController,
                                      obscureText: false,
                                      decoration: InputDecoration()) //Container
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  InkWell(
                    onTap: ()async => {
                      if (_validatePassword(cred))
                      {
                      Navigator.pop(context),
                        if(cred.user!=null)
                        {
                        showNonCancellableCircularLoader(context, 'Please wait...'),
                          if(!cred.user!.providerData.first.providerId.contains('google') || !cred.user!.providerData.first.providerId.contains('apple'))
                            {
                              await cred.user?.updatePassword(confirmPasswordTextFController.text.toString().trim()).then((value)
                              {
                                saveCredential(cred);
                              })
                            }
                          else
                            {
                              saveCredential(cred)
                            }
                        }
                      }
                      },
                    child: Container(
                      padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                      decoration: BoxDecoration(
                        color: MyColors.white_chinese_e0_bg,
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20.0),
                            bottomRight: Radius.circular(20.0)),
                      ),
                      child: MyWidgets.textView(text:
                      "Save Changes",
                        style: MyStyles.customTextStyle(color: MyColors.grey_70),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }


  void saveCredential(UserCredential cred) async {

    print('provide id = ${cred.user?.providerData.first.providerId}');
    print(cred.credential?.token);
    print(cred.user?.providerData.first.email);
    print(cred.user?.displayName.toString());
    print(cred.additionalUserInfo?.username.toString());
    print(cred.user?.displayName);
    print(
        'uuuuuuuuser photo url ==== ${cred.user?.providerData.first.photoURL}');
    print('uuuuuuuuser photo url ==== ${cred.user?.photoURL}');
    if (cred.user?.providerData.first.email != null &&
        cred.user?.providerData.first.email != '' &&
        cred.user?.uid != null &&
        cred.user?.uid != '') {
      var userOperations = UserOperations();
      var userCred = UserCredentials();

      userCred.userEmail = cred.user?.providerData.first.email;
      userCred.userUid = cred.user?.uid;
      userCred.userName = cred.user?.providerData.first.displayName;
      userCred.userPhoto = cred.user?.providerData.first.photoURL;
      userCred.userMobile = cred.user?.phoneNumber;
      userCred.userProvider = cred.user?.providerData.first.providerId;
      userCred.userIsLogin = 1;
      await addOrUpdateFireStoreUser(userCred).then((value)async {
        await userOperations.updateAllIsLoggedInState().then((value) async {
          await userOperations
              .userEmailExist(userCred.userEmail)
              .then((value) async {
            if (value) {
              print('isUserUid Exist true');

              print("new email is ${cred.user?.providerData.first.email}");
              await userOperations
                  .updateUserByEmail(userCred)
                  .then((value) async {

                await InAppPurchaseHandler.updateFireStoreSubscriptionToLocalDb(userCred.userEmail!).then((value)
                    {

                      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                          MyHomePage()), (Route<dynamic> route) => false);


                    });
              });
            } else {
              print('isUserUid Exist false');
              await userOperations.insertUser(userCred).then((value) async {
                await InAppPurchaseHandler.updateFireStoreSubscriptionToLocalDb(userCred.userEmail!).then((value) {

                  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                      MyHomePage()), (Route<dynamic> route) => false);
                });

              });
            }
          });
        });
      });
    }
    //}
  }

  Future<void> signInWithGoogleFunc() async {
    checkInternetConnectivity().then((bool) async {
      if (bool) {
        if(await _validateTermsAndConditions()){
        final GoogleSignInAccount? googleUser = await _googleSignIn.signIn();
        if (googleUser != null) {
          final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;

          final AuthCredential credential = GoogleAuthProvider.credential(
            accessToken: googleAuth.accessToken,
            idToken: googleAuth.idToken,
          );
          try {
            final UserCredential user = await _auth.signInWithCredential(
                credential);
            print('signed in ${user.user}');
            // saveCredential(user);
            openAlertBoxForPassword(user);
          }
          on FirebaseAuthException catch (e) {
            if (e.code == 'user-disabled') {
              // User is disabled.
              context.showSnackBar(
                  "The user account has been disabled by an administrator. Please contact us to resolve this issue sooner.");
            }
          }
        } else {
          print('signed in error else');
        }
      }
      } else
        showCustomAlertDialog(
            context, 'Please check your internet connectivity and try again');
    });
  }

  void signInWithAppleFunc() {
    if (Platform.isIOS)
      checkInternetConnectivity().then((bool) async {
        if (bool) {
          if(await _validateTermsAndConditions()){
          callSignInWithApple().then((value) =>
          // value != null ? saveCredential(value) : null
          value != null ? openAlertBoxForPassword(value) : null
          );}
        } else
          showCustomAlertDialog(
              context, 'Please check your internet connectivity and try again');
      });
    else
      showCustomAlertDialog(
          context, 'Sorry.. Apple login is currently unavailable');
  }

  Future<UserCredential?> callSignInWithApple() async {
    final rawNonce = generateNonce();
    final nonce = sha256ofString(rawNonce);

    var redirectURL =
        "https://xpress-pos-37245.firebaseapp.com/__/auth/handler";
    var clientID = "com.technologylab.xpressposAppleSignIn";

    // Request credential for the currently signed in Apple account.
    final appleCredential = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        //will ask for show/hide email (will result email issues)
        AppleIDAuthorizationScopes.fullName,
      ],
      webAuthenticationOptions: WebAuthenticationOptions(
        clientId: clientID,
        redirectUri: Uri.parse(redirectURL),
      ),
      nonce: nonce,
    );
    print(
        'credential status = ${appleCredential.email} \n ${appleCredential.authorizationCode.toString()} \n UID = ${appleCredential.userIdentifier} \n ${appleCredential.identityToken} \n  \n\n');
    // Create an `OAuthCredential` from the credential returned by Apple.

    try
    {
      final oauthCredential = OAuthProvider("apple.com").credential(
        idToken: appleCredential.identityToken,
        rawNonce: rawNonce,
      );
      return await FirebaseAuth.instance.signInWithCredential(oauthCredential);
    }
    on FirebaseAuthException catch (e)
    {
      if (e.code == 'user-disabled') {
        // User is disabled.
        context.showSnackBar("The user account has been disabled by an administrator. Please contact us to resolve this issue sooner.");

      }
      return null;
    }
  }

  Widget emailTextVw(BuildContext context) {
    return FadeInUp(
        delay: Duration(milliseconds: 10),
        child: Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child: Text('Email',
                      style: TextStyle(
                        // fontWeight: FontWeight.w300,
                        fontSize: 14,
                        color: Colors.black,
                      )),
                ),
                Container(
                  height: 45,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7),
                      color: Colors.white54),
                  // color: Colors.white54,
                  child: Row(
                    children: [
                      Flexible(
                        child: SizedBox(
                          height: 40,
                          width: 50,
                          child: Icon(
                            Icons.email,
                            size: 30,
                          ),
                        ),
                      ),
                      Container(
                        width: 1,
                        height: 50,
                        color: Colors.black45,
                      ),
                      // Divider(height: 10,color: Colors.red,thickness: 5,indent: 1,),
                      SizedBox(
                        height: 45,
                        width: MediaQuery.of(context).size.width - 125,
                        child: TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            textCapitalization: TextCapitalization.words,
                            controller: teEmailController,
                            obscureText: false,
                            decoration: InputDecoration(
                              contentPadding:
                              EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                              border: InputBorder.none,
                              hintText: "Email address",
                            )),
                      ),
                    ],
                  ),
                ),
              ],
            )));
  }

  Widget passwordTextVw(BuildContext context) {
    return FadeInUp(
        delay: Duration(milliseconds: 10),
        child: Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                  child: Text('Password',
                      style: TextStyle(
                        // fontWeight: FontWeight.w300,
                        fontSize: 14,
                        color: Colors.black,
                      )),
                ),
                Container(
                  height: 45,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7),
                      color: Colors.white54),
                  // color: Colors.white54,
                  child: Row(
                    children: [
                      Flexible(
                        child: SizedBox(
                          height: 40,
                          width: 50,
                          child: Icon(
                            Icons.lock,
                            size: 30,
                          ),
                        ),
                      ),
                      Container(
                        width: 1,
                        height: 50,
                        color: Colors.black45,
                      ),
                      // Divider(height: 10,color: Colors.red,thickness: 5,indent: 1,),
                      SizedBox(
                        height: 45,
                        width: MediaQuery.of(context).size.width - 125,
                        child: TextFormField(
                            keyboardType: TextInputType.visiblePassword,
                            textCapitalization: TextCapitalization.words,
                            controller: tePasswordController,
                            obscureText: true,
                            decoration: InputDecoration(
                              contentPadding:
                              EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                              border: InputBorder.none,
                              hintText: "Password",
                            )),
                      ),
                    ],
                  ),
                ),
              ],
            )));
  }

  Widget rememberMeVw(BuildContext context) {
    return FadeInUp(
        delay: Duration(milliseconds: 0),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(35, 10, 30, 0),
          child: Column(children: [
            Row(children: [
              /*InkWell(
                  onTap: () {
                    setState(() {
                      this.isRememberMeSelected = this.isRememberMeSelected ==
                          true
                          ? false
                          : true; //isRememberMeSelected==true? false : true) as Bool;
                    });
                  },
                  child: Container(
                      height: 30,
                      width: 30,

                      child: this.isRememberMeSelected == true
                          ? Icon(Icons.check_box_outlined)
                          : Icon(Icons.check_box_outline_blank))),
              Container(
                width: 10,
              ),
              Text(
                'Remember me',
                style: TextStyle(fontSize: 16),
              ),*/
              Spacer(),
              InkWell(
                onTap: () {
                  goto(context, ForgotPasswordScreen());
                },
                child: Text(
                  'Forgot Password?',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
                ),
              )
            ]),
            Container(
              height: 10,
            ),
          ]),
        ));
  }

  Widget SignInOptionsVw(BuildContext context) {
    final SigninBtn = FadeInUp(
        delay: Duration(milliseconds: 300),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(10.0),
          color: (Colors.black),
          child: MaterialButton(
            minWidth: MediaQuery.of(context).size.width,
            onPressed: () async {
    checkInternetConnectivity().then((bool) async {
    if (bool) {
              if (await _validateCredentialsInputs()) {

                showCircularLoader(context, '');
                logonFunction();
              }

    } else
      showCustomAlertDialog(
          context, 'Please check your internet connectivity and try again');});
            },
            child: Text(
              'Sign In',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ));

    return FadeInUp(
        delay: Duration(milliseconds: 10),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(30, 10, 30, 0),
          child: Column(
            children: [
              Row(children: [

                InkWell(
                    onTap: () {
                      setState(() {
                        this.isTermsAndConditionChecked = this.isTermsAndConditionChecked ==
                            true
                            ? false
                            : true; //isRememberMeSelected==true? false : true) as Bool;
                      });
                    },
                    child: Container(

                        child: this.isTermsAndConditionChecked == true
                            ? Icon(Icons.check_box_outlined,size: 23,)
                            : Icon(Icons.check_box_outline_blank,size: 23,))),
                Container(
                  width: 5,
                ),
               InkWell(onTap:(){
                  goto(context, WebViewScreenState());
               },child:  Text(
                  'Terms And Conditions',
                  style: TextStyle(fontSize: 13,fontWeight:FontWeight.w500,decoration: TextDecoration.underline,),

                )),
                Spacer(),

              ]),
              SizedBox(height: 10,),
              SigninBtn,
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: Text(
                  'Or Sign In with',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 16, color: Colors.black),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 60,
                    width: 60,
                    child: InkWell(
                      onTap: signInWithAppleFunc,
                      child: FittedBox(
                        child: Image.asset('assets/images/apple_logo.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Container(
                    height: 60,
                    width: 60,
                    child: InkWell(
                      onTap: signInWithGoogleFunc,
                      child: FittedBox(
                        child: Image.asset('assets/images/google_login.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ],
              ),
              InkWell(
                onTap: () async{
                  goto(context, SignUpScreen());

                },
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Don\'t have an account?',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 16, color: Colors.black),
                      ),
                      Text(
                        'Create',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Widget contactUsVw(BuildContext context) {
    return FadeInUp(
        delay: Duration(milliseconds: 10),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(30, 00, 30, 0),
          child: Column(
            children: [
              InkWell(
                onTap: () async{
                  goto(context, ContactSupportpage());
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Contact Us here.',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 16, color: Colors.black),
                    ),
                    // Text(
                    //   'Privacy Policy',
                    //   textAlign: TextAlign.center,
                    //   style: TextStyle(
                    //       fontSize: 16,
                    //       fontWeight: FontWeight.bold,
                    //       color: Colors.black),
                    // ),
                  ],
                ),
              ),
              SizedBox(height: 40,)
            ],
          ),
        ));
  }

  Future<bool> _validateCredentialsInputs() async {
    if (this.teEmailController.text.isEmpty) {
      context.showSnackBar("Enter Email Address");
      return false;
    } else if (this.tePasswordController.text.isEmpty) {
      context.showSnackBar("Enter Password");
      return false;
    } else if(!await _validateTermsAndConditions()){
      return false;
    }
    final gg = await checkInternetConnectivity();
    if (gg) {
      return true;
    } else {
      showCustomAlertDialog(
          context, 'Please check your internet connectivity and try again');
      return false;
    }
  }
  Future<bool> _validateTermsAndConditions()async  {
    if(!isTermsAndConditionChecked){
      context.showSnackBar("Please verify & accept terms and conditions");
      return false;
    }
    return true;
  }

  void resendVerificationCodeToEmail(UserCredential user) async {
    await FirebaseAuth.instance.currentUser?.sendEmailVerification().then(
            (value) => showCustomAlertDialog(
            context, 'Verification Email send successfully.'));
  }

  void logonFunction() async {
    try {
      final credentialRes = await FirebaseAuth.instance
          .signInWithEmailAndPassword(
          email: this.teEmailController.text.trim(),
          password: this.tePasswordController.text)
          .then((value) {
        Navigator.of(context).pop();
        print('sign is success ${value.user?.email}    ${value.user?.displayName}     ${value.user?.uid}  ');
        if (value.user?.emailVerified == true) {
          print('email verifieddd ${value.additionalUserInfo?.providerId}');
          // saveCredential(value);
          openAlertBoxForPassword(value);
        }
        else
        {
          print('email not verified');
          // showCustomAlertDialog(context,
          //     'Please Go to your Email and  verify by click the Link. If you have not received yet click Resend.' );
          showCustomCallbackAlertDialog(
            context: context,
            msg:
            'Please Go to your Email and  verify by click the Link. If you have not received yet click Resend.',
            positiveText: 'OK',
            positiveClick: () {
              Navigator.of(context).pop(true);
            },
            negativeText: 'Resend',
            negativeClick: () {
              Navigator.of(context).pop(true);
              resendVerificationCodeToEmail(value);
            },
          );
        }
      });
    } on FirebaseAuthException catch (e) {
      Navigator.of(context).pop();
      var val = FirebaseUtils().determineFirebaseAuthException(e);
      context.showSnackBar('${val.name.toString()}');
    } catch (e) {
      Navigator.of(context).pop();
      if (e != null) {
        context.showSnackBar('$e');
      } else {}
    }
    return;
  }
  var fireStoreOperations = FireStoreOperations();
  Future addOrUpdateFireStoreUser(UserCredentials cred)async{
    var fireStoreUser = FirestoreUser();
    fireStoreUser.userName=cred.userName;
    fireStoreUser.userEmail=cred.userEmail;
    await fireStoreOperations.checkDocumentExist(fireStoreUser.userEmail).then((bool)async
    {
      if(bool)
      {
        await fireStoreOperations.updateFireStoreUserData(fireStoreUser);
      }
      else
      {
        fireStoreUser.userPassword = this.confirmPasswordTextFController.text!.trim();
        await fireStoreOperations.insertFireStoreUserData(fireStoreUser);
      }

    });

  }


}
