import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewScreenState extends StatefulWidget {
  final bool? isTerms;
  const WebViewScreenState({
    Key? key,
     this.isTerms=true,
  }) : super(key: key);

  @override
  _WebViewScreenState createState() => _WebViewScreenState();
}

class _WebViewScreenState extends State<WebViewScreenState> {
  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
          title: MyWidgets.textView(
            text: widget.isTerms==true?'Terms And Conditions':'Privacy Policy',
            style: TextStyle(color: Colors.black),
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.black,
            ),
            onPressed: () => Navigator.pop(context),
          ),
        ),
        body: Container(
          padding: EdgeInsets.all(8),
          child: WebViewWidget(
            controller: WebViewController()
              ..setJavaScriptMode(JavaScriptMode.unrestricted)
              ..setBackgroundColor(const Color(0x00000000))

              ..setNavigationDelegate(
                NavigationDelegate(
                  onProgress: (int progress) {
                    // Update loading bar.
                  },
                  onPageStarted: (String url) {
                    showNonCancellableCircularLoader(context, '');
                  },
                  onPageFinished: (String url) {
                    Navigator.pop(context);
                  },

                  onWebResourceError: (WebResourceError error) {
                    Navigator.pop(context);
                  },
                ),
              )

              ..loadRequest(Uri.parse(widget.isTerms==true?'https://technologylab.global/express_pos/termsandconditions.html':'https://technologylab.global/express_pos/privacypolicy.html')),

          ),
        ));
  }
}
