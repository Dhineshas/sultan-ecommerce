import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'dart:math';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/database/user_operation.dart';
import 'package:ecommerce/main.dart';
import 'package:ecommerce/model/UserCredentials.dart';
import 'package:ecommerce/signUpView.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/material.dart';

import 'package:crypto/crypto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  State<ForgotPasswordScreen> createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final teEmailController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: FittedBox(
            child: Image.asset('assets/images/background_login.png'),
            fit: BoxFit.cover,
          ),
        ),
        Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                    child: Image.asset('assets/images/login_top_view_map.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ],
            )
          ],
        ),
        Scaffold(
            backgroundColor: Colors.transparent,
            body: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(40, 70, 0, 0),
                    child: Container(
                        height: 220,
                        width: MediaQuery.of(context).size.width * 0.55,
                        child: Text(
                          'Make your Business Easy and Professionals',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 30,
                            color: Colors.black54,
                            // overflow: TextOverflow.clip
                          ),
                          // overflow: TextOverflow.visible,
                          // strutStyle: StrutStyle(fontSize: 30,fontWeight: FontWeight.w900),
                          textAlign: TextAlign.left,
                        )),
                  ),
                  emailTextVw(context),
                  sendCodeBtnVw(context)
                ],
              ),
            ))
      ],
    );
  }


  Widget emailTextVw(BuildContext context) {
    return FadeInUp(
        delay: Duration(milliseconds: 10),
        child: Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child: Text('Email',
                      style: TextStyle(
                        // fontWeight: FontWeight.w300,
                        fontSize: 14,
                        color: Colors.black,
                      )),
                ),
                Container(
                  height: 45,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7),
                      color: Colors.white54),
                  // color: Colors.white54,
                  child: Row(
                    children: [
                      Flexible(
                        child: SizedBox(
                          height: 40,
                          width: 50,
                          child: Icon(
                            Icons.email,
                            size: 30,
                          ),
                        ),
                      ),
                      Container(
                        width: 1,
                        height: 50,
                        color: Colors.black45,
                      ),
                      // Divider(height: 10,color: Colors.red,thickness: 5,indent: 1,),
                      SizedBox(
                        height: 45,
                        width: MediaQuery.of(context).size.width - 125,
                        child: TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            textCapitalization: TextCapitalization.words,
                            controller: teEmailController,
                            obscureText: false,
                            decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                              border: InputBorder.none,
                              hintText: "Email address",
                            )),
                      ),
                    ],
                  ),
                ),
              ],
            )));
  }

  Widget sendCodeBtnVw(BuildContext context) {
    return FadeInUp(
        delay: Duration(milliseconds: 300),
        child: Padding(
          padding: const EdgeInsets.all(25.0),
          child: Material(
            elevation: 5.0,
            borderRadius: BorderRadius.circular(10.0),
            color: (Colors.black),
            child: MaterialButton(
              minWidth: MediaQuery.of(context).size.width,
              onPressed: (){
                sendVerificationCodeToEmail();
              },
              child: Text(
                'Send Verification Code',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ));

  }

  bool _validate() {
    // this.teEmailController.text = 'q.developer.fahanas@gmail.com';

    if (this.teEmailController.text.isEmpty) {
      context.showSnackBar("Enter Email Address to send the OTP.");
      return false;
    }
    checkInternetConnectivity().then((bool) async {
      if (bool) {
        {
          return true;
        }
      }
      showCustomAlertDialog(
          context, 'Please check your internet connectivity and try again');
      return false;
    });
    return true;
  }

  void sendVerificationCodeToEmail()async
  {
    if (_validate()) {
      try {
        await FirebaseAuth.instance.sendPasswordResetEmail(email: this.teEmailController.text).
        then((value) =>
            showCustomAlertDialog(
                context, 'A link has been sent to your Email ID to Reset your Password.').
            then((value) =>
                Navigator.of(context).pop()
            )
        );
      } on FirebaseAuthException catch (e) {
        var val = _determineError(e);
        context.showSnackBar('${val.name.toString()}');
      } catch (e) {
        if (e != null) {
          print('catch error create accFFF $e');
          context.showSnackBar('$e');
        } else {
          print('catch error create accEEEELSE');
        }
      }
    }
  }


  // void logonFunction() async {
  //   if (_validate()) {
  //     try {
  //       final credentialRes = await FirebaseAuth.instance
  //           .signInWithEmailAndPassword(
  //               email: this.teEmailController.text,
  //               password: this.tePasswordController.text)
  //           .then((value) {
  //         print(
  //             'sign is success ${value.user?.email}    ${value.user?.displayName}     ${value.user?.uid}  ');
  //         if (value.user?.emailVerified == true) {
  //           print('email verifieddd ${value.additionalUserInfo?.providerId}');
  //
  //         } else {
  //           print('email not verified');
  //           // showCustomAlertDialog(context,
  //           //     'Please Go to your Email and  verify by click the Link. If you have not received yet click Resend.' );
  //           showCustomCallbackAlertDialog(
  //               context: context,
  //               msg: 'Please Go to your Email and  verify by click the Link. If you have not received yet click Resend.',
  //               positiveText: 'OK',
  //               positiveClick:
  //               () {
  //                 Navigator.of(context).pop(true);
  //               },
  //               negativeText: 'Resend',
  //               negativeClick: () {
  //                 Navigator.of(context).pop(true);
  //                 resendVerificationCodeToEmail(value);
  //               },
  //           );
  //         }
  //       });
  //     } on FirebaseAuthException catch (e) {
  //       var val = _determineError(e);
  //       context.showSnackBar('${val.name.toString()}');
  //
  //       // if (e.code == 'weak-password') {
  //       //   print('The password provided is too weak.');
  //       //   context.showSnackBar('The password provided is too weak.');
  //       // }
  //       // else if (e.code == 'email-already-in-use') {
  //       //   print('The account already exists for that email.');
  //       //   context.showSnackBar('The account already exists for that email.');
  //       // }
  //       // else if(e.code == "invalid-email")
  //       //   {
  //       //     print('invalid-email.');
  //       //     context.showSnackBar('invalid-email.');
  //       //   }
  //       // else
  //       //   {
  //       //     print('Some other errors ${e.code}');
  //       //   }
  //     } catch (e) {
  //       if (e != null) {
  //         print('catch error create accFFF $e');
  //         context.showSnackBar('$e');
  //       } else {
  //         print('catch error create accEEEELSE');
  //       }
  //     }
  //
  //     return;
  //   }
  // }

  AuthError _determineError(FirebaseAuthException exception) {
    switch (exception.code) {
      case 'invalid-email':
        return AuthError.invalidEmail;
      case 'user-disabled':
        return AuthError.userDisabled;
      case 'user-not-found':
        return AuthError.userNotFound;
      case 'wrong-password':
        return AuthError.wrongPassword;
      case 'email-already-in-use':
      case 'account-exists-with-different-credential':
        return AuthError.emailAlreadyInUse;
      case 'invalid-credential':
        return AuthError.invalidCredential;
      case 'operation-not-allowed':
        return AuthError.operationNotAllowed;
      case 'weak-password':
        return AuthError.weakPassword;
      case 'ERROR_MISSING_GOOGLE_AUTH_TOKEN':
      default:
        return AuthError.error;
    }
  }
}
