import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/upgrade.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Subscription.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'database/inventory_item_operations.dart';

class InventoryItemStockBarChart extends StatefulWidget {
  final int? inventoryItemId;

  const InventoryItemStockBarChart({@required this.inventoryItemId, Key? key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => InventoryItemStockBarChartState();
}

class InventoryItemStockBarChartState extends State<InventoryItemStockBarChart> {
  final Color leftBarColor = const Color(0xff53fdd7);
  final Color rightBarColor = const Color(0xffff5182);
  final double width = 7;

  late List<BarChartGroupData> rawBarGroups;
  late List<BarChartGroupData> showingBarGroups;

  int touchedGroupIndex = -1;

  @override
  void initState() {
    super.initState();
    /*final barGroup1 = makeGroupData(0, 5, 12);
    final barGroup2 = makeGroupData(1, 16, 12);
    final barGroup3 = makeGroupData(2, 18, 5);
    final barGroup4 = makeGroupData(3, 20, 16);
    final barGroup5 = makeGroupData(4, 17, 6);
    final barGroup6 = makeGroupData(5, 19, 1.5);
    final barGroup7 = makeGroupData(6, 10, 1.5);

    final barGroup8 = makeGroupData(7, 10, 1.5);
    final barGroup9 = makeGroupData(8, 10, 1.5);
    final barGroup10 = makeGroupData(9, 10, 1.5);
    final barGroup11 = makeGroupData(10, 10, 1.5);
    final barGroup12 = makeGroupData(11, 10, 1.5);*/

    /*final barGroup1 = makeGroupData(0, 0, 0);
    final barGroup2 = makeGroupData(1, 0, 0);
    final barGroup3 = makeGroupData(2, 0, 0);
    final barGroup4 = makeGroupData(3,0, 0);
    final barGroup5 = makeGroupData(4, 0, 0);
    final barGroup6 = makeGroupData(5, 0, 0);
    final barGroup7 = makeGroupData(6, 0, 0);

    final barGroup8 = makeGroupData(7, 0, 0);
    final barGroup9 = makeGroupData(8, 0, 0);
    final barGroup10 = makeGroupData(9, 0, 0);
    final barGroup11 = makeGroupData(10, 0, 0);
    final barGroup12 = makeGroupData(11, 0, 0);

    final items = [
      barGroup1,
      barGroup2,
      barGroup3,
      barGroup4,
      barGroup5,
      barGroup6,
      barGroup7,

      barGroup8,
      barGroup9,
      barGroup10,
      barGroup11,
      barGroup12
    ];

    rawBarGroups = items;

    showingBarGroups = rawBarGroups;*/
    final barGroup1 = makeGroupData(0, 0, 0);
    final items = [barGroup1];
    rawBarGroups = items;

    showingBarGroups = rawBarGroups;

    setYearListAndGetData();
  }

  @override
  Widget build(BuildContext context) {
    final typeDropDownButton =   Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: DropdownButton<int>(
          dropdownColor: MyColors.white_chinese_e0_bg,
          value: selectedYear,
          icon: const Icon(Icons.keyboard_arrow_down),
          elevation: 8,
          borderRadius: BorderRadius.all(Radius.circular(8)),
          style: const TextStyle(color: Colors.black),
          underline: Container(
            height: 1,
            color: MyColors.white_chinese_e0_bg,
          ),
          onChanged: (int? newValue) {
            setState(() {
              selectedYear=newValue!;
              getData(selectedYear);
            });
          },
          items: yearList.map<DropdownMenuItem<int>>((int value) {
            return DropdownMenuItem<int>(
              value: value,
              child: Text(value.toString()),
            );
          }).toList(),
        ));


    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
          title: MyWidgets.textView(text: 'Stock Analysis',style: TextStyle(color: Colors.black),),
          leading: IconButton(
            icon: Icon(Icons.arrow_back,color: MyColors.black,),
            onPressed: () => Navigator.pop(context),
          ),

          actions: <Widget>[
            yearList.isNotEmpty?FadeIn(delay: Duration(milliseconds: 300),child:Card(color: MyColors.white_chinese_e0_bg,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                ),
                elevation: 0,
                child:Row(children: [
                  SizedBox(width: 5,),
                  Icon(
                    Icons.calendar_month_rounded,
                    color: MyColors.grey_70,
                  ),
                  typeDropDownButton
                ],)),):Container()

          ],
        ),

        body: AspectRatio(
          aspectRatio: 1,
          child: Card(
            elevation: 0,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
            color: const Color(0xff2c4260),
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      makeTransactionsIcon(),
                      const SizedBox(
                        width: 38,
                      ),
                      const Text(
                        'Transactions',
                        style: TextStyle(color: Colors.white, fontSize: 22),
                      ),
                      const SizedBox(
                        width: 4,
                      ),
                      const Text(
                        'state',
                        style:
                            TextStyle(color: Color(0xff77839a), fontSize: 16),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 38,
                  ),
                  Expanded(
                    child: barChartGroupData.isNotEmpty?FadeIn(child: BarChart(
                      BarChartData(
                        maxY: max,
                        barTouchData: BarTouchData(
                            touchTooltipData: BarTouchTooltipData(
                              tooltipBgColor: Colors.grey,
                              getTooltipItem: (_a, _b, _c, _d) => null,
                            ),
                            touchCallback: (FlTouchEvent event, response) {
                              if (response == null || response.spot == null) {
                                setState(() {
                                  touchedGroupIndex = -1;
                                  showingBarGroups = List.of(rawBarGroups);
                                });
                                return;
                              }

                              touchedGroupIndex =
                                  response.spot!.touchedBarGroupIndex;

                              setState(() {
                                if (!event.isInterestedForInteractions) {
                                  touchedGroupIndex = -1;
                                  showingBarGroups = List.of(rawBarGroups);
                                  return;
                                }
                                showingBarGroups = List.of(rawBarGroups);
                                if (touchedGroupIndex != -1) {
                                  var sum = 0.0;
                                  for (var rod
                                      in showingBarGroups[touchedGroupIndex]
                                          .barRods) {
                                    sum += rod.toY;
                                  }
                                  final avg = sum /
                                      showingBarGroups[touchedGroupIndex]
                                          .barRods
                                          .length;

                                  showingBarGroups[touchedGroupIndex] =
                                      showingBarGroups[touchedGroupIndex]
                                          .copyWith(
                                    barRods: showingBarGroups[touchedGroupIndex]
                                        .barRods
                                        .map((rod) {
                                      return rod.copyWith(toY: avg);
                                    }).toList(),
                                  );
                                }
                              });
                            }),
                        titlesData: FlTitlesData(
                          show: true,
                          rightTitles: AxisTitles(
                            sideTitles: SideTitles(showTitles: false),
                          ),
                          topTitles: AxisTitles(
                            sideTitles: SideTitles(showTitles: false),
                          ),
                          bottomTitles: AxisTitles(
                            sideTitles: SideTitles(
                              showTitles: true,
                              getTitlesWidget: bottomTitles,
                              reservedSize: 42,
                            ),
                          ),
                          leftTitles: AxisTitles(
                            sideTitles: SideTitles(
                              showTitles: true,
                              reservedSize: 28,
                              interval: 1,
                              getTitlesWidget: leftTitles,
                            ),
                          ),
                        ),
                        borderData: FlBorderData(
                          show: false,
                        ),
                        barGroups: showingBarGroups,
                        gridData: FlGridData(show: false),
                      ),
                    )):Center(
                      child: Text(
                        'No Data Found.',
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.grey,
                        ),
                      ),
                    )),

                  const SizedBox(
                    height: 12,
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  Widget leftTitles(double value, TitleMeta meta) {
    const style = TextStyle(
      color: Color(0xff7589a2),
      fontWeight: FontWeight.bold,
      fontSize: 14,
    );
    String text;
    if (value.toInt() == 0) {
      text = '0';
    } else if (value == intermediate.toInt()) {
      text = '${intermediate.toInt()}';
    } else if (value == max.toInt()) {
      text = '${max.toInt()}';
    } else {
      return Container();
    }
    return SideTitleWidget(
      axisSide: meta.axisSide,
      space: 0,
      child: Text(text, style: style),
    );
  }

  Widget bottomTitles(double value, TitleMeta meta) {
    const style = TextStyle(
      color: Color(0xff7589a2),
      fontWeight: FontWeight.bold,
      fontSize: 14,
    );
    Widget text;
    switch (value.toInt()) {
      case 0:
        text = const Text(
          'Jan',
          style: style,
        );
        break;
      case 1:
        text = const Text(
          'Feb',
          style: style,
        );
        break;
      case 2:
        text = const Text(
          'Mar',
          style: style,
        );
        break;
      case 3:
        text = const Text(
          'Apr',
          style: style,
        );
        break;
      case 4:
        text = const Text(
          'May',
          style: style,
        );
        break;
      case 5:
        text = const Text(
          'Jun',
          style: style,
        );
        break;
      case 6:
        text = const Text(
          'Jul',
          style: style,
        );
        break;
      case 7:
        text = const Text(
          'Aug',
          style: style,
        );
        break;
      case 8:
        text = const Text(
          'Sep',
          style: style,
        );
        break;
      case 9:
        text = const Text(
          'Oct',
          style: style,
        );
        break;
      case 10:
        text = const Text(
          'Nov',
          style: style,
        );
        break;
      case 11:
        text = const Text(
          'Dec',
          style: style,
        );
        break;
      default:
        text = const Text(
          '',
          style: style,
        );
        break;
    }
    return SideTitleWidget(
      axisSide: meta.axisSide,
      space: 16,
      child: text,
    );
  }

  BarChartGroupData makeGroupData(int x, double y1, double y2) {
    return BarChartGroupData(barsSpace: 4, x: x, barRods: [
      BarChartRodData(
        toY: y1,
        color: leftBarColor,
        width: width,
      ),
      BarChartRodData(
        toY: y2,
        color: rightBarColor,
        width: width,
      ),
    ]);
  }

  Widget makeTransactionsIcon() {
    const width = 4.5;
    const space = 3.5;
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          width: width,
          height: 10,
          color: Colors.white.withOpacity(0.4),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 28,
          color: Colors.white.withOpacity(0.8),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 42,
          color: Colors.white.withOpacity(1),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 28,
          color: Colors.white.withOpacity(0.8),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 10,
          color: Colors.white.withOpacity(0.4),
        ),
      ],
    );
  }

  var inventoryItemOperations = InventoryItemOperations();
  double max = 1;
  double intermediate = 0;
  var barChartGroupData = <BarChartGroupData>[];
  List<int> yearList = <int>[];
  final dateFormat = DateFormat("yyyy-MM-dd hh:mm:ss.SSS");
  int selectedYear=0;
  void setYearListAndGetData()async{
    final trialStartDate= await MyPref.getDbTrialStartDate();
    selectedYear=currentYear;

    if(trialStartDate!=null&& trialStartDate!=''){

      DateTime parseDate = dateFormat.parse(trialStartDate.toString());

      for(var i=currentYear; i>parseDate.year-1; i--){
        yearList.add(i);
      }

    }

    await getData(selectedYear);
  }
  Future getData(int selectedYear) async {
    final activeSubscription= await Subscription.getActiveSubscription();
    if(activeSubscription==1||activeSubscription==2)

    await MyPref.getProject().then((project) async {
      if (project != null) {
        await inventoryItemOperations
            .inventoriesItemHistoryById(widget.inventoryItemId,selectedYear)
            .then((value) {
          // value.sort((a, b) => a.name!.compareTo(b.name!));
          barChartGroupData.clear();
          if (value.isNotEmpty)
            setState(() {
              //max=value.first.maxOfTotalType!.toDouble();
              if(value.first.itemType==0)
                max = value.map((e) => e.maxOfTotalType).reduce((value, element) => (value! > element!) ? value : element)!;

              else
              max = value.map((e) => e.historyInventoryItemTotalType?.toDouble()).reduce((value, element) => (value! > element!) ? value : element)!;
              intermediate = max / 2;
              for (int i = 0; i <= 11; i++) {
                barChartGroupData.add(makeGroupData(i, 0, 0));
              }
              value.forEach((element) {
                print('data_ ${element.name}  ${element.historyInventoryItemTotalType}  ${element.historyAddedStock}   ${element.historyAddedDate}   ${element.historyYear}   ${element.historyMonth}  ${element.maxOfTotalType}  ${element.historyStockConsumed}');

                if (element.itemType == 0)
                  barChartGroupData[element.historyMonth!.toInt() - 1] =
                      makeGroupData(
                          element.historyMonth!.toInt() - 1,
                          element.historyAddedStock!.toDouble(),
                          element.historyStockConsumed!.toDouble());
                else
                  barChartGroupData[element.historyMonth!.toInt() - 1] =
                      makeGroupData(
                          element.historyMonth!.toInt() - 1,
                          element.historyInventoryItemTotalType!.toDouble(),
                          element.historyStockConsumed!.toDouble());
              });
              rawBarGroups = barChartGroupData;
              showingBarGroups = rawBarGroups;
            });
          else{

            setState(() {

            });}

        });
      } else {
        showCustomAlertDialog(context, 'Please create a project ');
      }
    });

    else
      showCustomCallbackAlertDialog(context: context, msg: 'No active subscription plan found, Please upgrade to pro to continue.',positiveText: 'Upgrade', positiveClick: ()async{
        Navigator.pop(context);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    UpgradeScreenState()) );
      }, negativeClick: (){
        Navigator.pop(context);
      });
  }
}
