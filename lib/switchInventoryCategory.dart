import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'database/inventory_item_operations.dart';
import 'database/inventory_operations.dart';
import 'database/relation_operations.dart';
import 'inventoryUnitMeasures.dart';
import 'model/Inventory.dart';
import 'model/InventoryItem.dart';

class SwitchInventoryCategoryScreenState extends StatefulWidget {
  final List<int>? inventoryIdArray;
  final List<int>? inventoryItemIdArray;
  final bool? isFromInventoryItemList;
  const SwitchInventoryCategoryScreenState({@required this.inventoryIdArray,@required this.inventoryItemIdArray,@required this.isFromInventoryItemList,Key? key}) : super(key: key);

  @override
  _SwitchInventoryCategoryScreenState createState() => _SwitchInventoryCategoryScreenState();
}

class _SwitchInventoryCategoryScreenState extends State<SwitchInventoryCategoryScreenState> {
  int? selectedIndex = -1;
  var inventoryOperations = InventoryOperations();
  var inventoryItemOperations = InventoryItemOperations();
  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 1000), () {
      _getData();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: createListView(),
    );
  }

  final _listKey = GlobalKey<AnimatedListState>();
  var _scrollController = ScrollController();

  Widget createListView() {
    if(isLoading)
    return MyWidgets.buildCircularProgressIndicator();
    else
      return Padding(
          padding: EdgeInsets.fromLTRB(6, 0, 10, 20),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 20,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child:Padding(
                  padding: EdgeInsets.only(left: 8),child: MyWidgets.textView(
                  text: 'Select folder',
                  style: MyStyles.customTextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 24,
                  ),
                ),
              )),
              SizedBox(
                height: 10,
              ),
              Expanded(
                  flex: 1,
                  child: Wrap(
                      children: <Widget>[
                        items.isNotEmpty?ListView.builder(
                      physics: AlwaysScrollableScrollPhysics(
                          parent: BouncingScrollPhysics()),
                      key: _listKey,
                      controller: _scrollController,
                      shrinkWrap: true,
                      itemCount: items.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _buildItems(context, items[index], index);
                      }):Container(),widget.isFromInventoryItemList==true?ListTile(
                          contentPadding: EdgeInsets.only(left: 0.0, right: 0.0),
                        onTap: () {
                          setState(() {
                            selectedIndex=items.length+1;
                            inventory=null;
                          });
                        },
                          leading:Image.asset('assets/images/icon_folder.png', fit: BoxFit.fill,width: 75,) , //Icon(Icons.folder),
                        title: MyWidgets.textView(text: 'Root folder', textAlign: TextAlign.start),
                        trailing: Icon(
                          selectedIndex == items.length+1
                              ? Icons.radio_button_checked_rounded
                              : Icons.radio_button_off_rounded,
                          color: MyColors.black,
                          size: 20,
                        ),
                      ):Container()])),
              SizedBox(
                height: 10,
              ),
              MyWidgets.materialButton(context,
                  text: 'Move',
                  textColor: MyColors.black,
                  color: MyColors.white_chinese_e0_bg,
                  onPressed: () {updateCategoryId();})
            ],
          ));

  }

  Widget _buildItems(BuildContext context, Inventory value, int index) {
    return ListTile(
      contentPadding: EdgeInsets.only(left: 0.0, right: 0.0),
      onTap: () {
        setState(() {
          selectedIndex = index;
          inventory = value;
        });
      },
      leading://Icon(Icons.folder),
      Image.asset('assets/images/icon_folder.png', fit: BoxFit.fill,width: 75,) ,
      //Container(width: 30,height: 30,color: MyColors.red_ff_bg,),
      title: MyWidgets.textView(text: value.name!, textAlign: TextAlign.start),
      trailing: Icon(
        selectedIndex == index
            ? Icons.radio_button_checked_rounded
            : Icons.radio_button_off_rounded,
        color: MyColors.black,
        size: 20,
      ),
    );
  }

  var items = <Inventory>[];
  bool isLoading = true;
  Inventory? inventory;

  Future<List<Inventory>> _getData() async {
    await MyPref.getProject().then((project) async {
      if (project != null) {
        await inventoryOperations
            .inventoriesByProjectIdWithoutSelectedCategory(project.id,widget.inventoryIdArray!)
            .then((value) {
          items = value;
        });
      } else
        showCustomAlertDialog(context, 'Please create a project ');
      setState(() {
        isLoading = false;
      });
    });

    return items;
  }
  updateCategoryId()async{
    if(selectedIndex==-1)
      showCustomAlertDialog(context, 'Please select folder');
    else
      showCustomCallbackAlertDialog(context: context, msg: 'Are you sure want to move to ${inventory?.name}?',positiveText: 'Move', positiveClick: ()async{
        Navigator.pop(context);
        await inventoryItemOperations.updateInventoryItemCategoryId(inventory?.id, widget.inventoryItemIdArray!).then((value) => Navigator.pop(context, true));

      }, negativeClick:(){
        Navigator.pop(context, false);
      });
  }
}
