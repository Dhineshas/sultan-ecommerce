import 'package:ecommerce/model/homeGridItems.dart';

class InventoryUnitMeasure {
  //static var  inventoryUnitMeasureArray = <String>['Box', 'Length','Meter','Centimeter','Millimetre','Kilogram','Gram','Milligram','Liter','Milliliter'];
  static var inventoryUnitMeasureArray = InventoryUnitMeasure._measuresMap.keys.toList();
  // final inventoryUnitMeasureArray1 = InventoryUnitMeasure._measuresMap.keys.toList();

/*
  static Map<String, int> _measuresMap = {
    'Box': 0,
    'Meters': 1,
    'Kilometers': 2,
    'Grams': 3,
    'Kilograms': 4,
    'Feet': 5,
    'Miles': 6,
    'Pounds (lbs)': 7,
    'Ounces': 8,
    'Milliliter': 9,
    'Liter': 10,
  };

  dynamic _formulas = {
    '0': [0,0, 0, 0, 0, 0, 0, 0, 0,0,0],
    '1': [0,1, 0.001, 0, 0, 3.28084, 0.000621371, 0, 0,0,0],
    '2': [0,1000, 1, 0, 0, 3280.84, 0.621371, 0, 0,0,0],
    '3': [0,0, 0, 1, 0.001, 0, 0, 0.00220462, 0.035274,0,0],
    '4': [0,0, 0, 1000, 1, 0, 0, 2.20462, 35.274,0,0],
    '5': [0,0.3048, 0.0003048, 0, 0, 1, 0.000189394, 0, 0,0,0],
    '6': [0,1609.34, 1.60934, 0, 0, 5280, 1, 0, 0,0,0],
    '7': [0,0, 0, 453.592, 0.453592, 0, 0, 1, 16,0,0],
    '8': [0,0, 0, 28.3495, 0.0283495, 3.28084, 0, 0.0625, 1,0,0],
    '9': [0,0, 0, 0, 0, 0, 0,0, 0.033814,1,0.001],
    '10':[0,0, 0, 0, 0, 0, 0,0, 33.814,1000,1],
  };

*/



  static Map<String, int> _measuresMap = {
    'Box': 0,
    'Meters': 1,
    'Centimeter': 2,
    'Grams': 3,
    'Kilograms': 4,
    'Feet': 5,
    'Pounds (lbs)': 6,
    'Milliliter': 7,
    'Liter': 8,
  };

  dynamic _formulas = {
    '0': [0,0, 0, 0, 0, 0, 0, 0,0],
    '1': [0,1, 100, 0, 0, 3.28084, 0, 0,0],
    '2': [0,0.01, 1, 0, 0, 0.0328084, 0,0,0],
    '3': [0,0, 0, 1, 0.001, 0, 0.00220462, 0,0],
    '4': [0,0, 0, 1000, 1, 0, 2.20462,0,0],
    '5': [0,0.3048, 0.0003048, 0, 0, 1, 0,0,0],
    '6': [0,0, 0, 453.592, 0.453592, 0, 1,0,0],
    '7': [0,0, 0, 0, 0, 0, 0, 1,0.001],
    '8': [0,0, 0, 0, 0, 0,0,1000,1],
  };

  Future<double?>convert(double value, String from, String to)async {
    int? nFrom = _measuresMap[from];
    int? nTo = _measuresMap[to];
    var multiplier = _formulas[nFrom.toString()][nTo];
    var result = value * multiplier;
    /*if (result == 0) {
       _resultMessage = 'This conversion cannot be performed';
      return _resultMessage;
    } else {
      _resultMessage =
      '${_numberForm.toString()} $_startMeasure are ${result.toString()} $_convertedMeasure';
    }*/
    return result;
  }


  Future<UnitMeasureResult?>convert2(double totalStock,double costPerStock, String from, String to)async {
    int? nFrom = _measuresMap[from];
    int? nTo = _measuresMap[to];

    var multiplier = _formulas[nFrom.toString()][nTo];
    print('from $nFrom  to $nTo formula $multiplier');
    var result = totalStock * multiplier;

    var cost =costPerStock>0? costPerStock / multiplier:0.0;

    var unitMeasureResult= UnitMeasureResult();
    unitMeasureResult.stockConversionResult=result;
    unitMeasureResult.perStockConversionCost=cost;
    unitMeasureResult.stockMultiplierValue=multiplier;
    return unitMeasureResult;
  }

}