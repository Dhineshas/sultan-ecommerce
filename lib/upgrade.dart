import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/database/iap_operation.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/FirebaseUtils.dart';
import 'package:ecommerce/utils/InAppPurchaseHandler.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Subscription.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:ecommerce/webView.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';

class UpgradeScreenState extends StatefulWidget {
  const UpgradeScreenState({Key? key}) : super(key: key);

  @override
  _UpgradeScreenState createState() => _UpgradeScreenState();
}

class _UpgradeScreenState extends State<UpgradeScreenState> {
  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      showCustomAlertDialog(context,
          'Caution.! Always make sure to upload the database with the cloud to avoid data loss. Before upload make sure to download the cloud database and sync with the latest device you logged in, because the cloud database is considered as the primary device database. For more please verify the terms and conditions from settings.');
    });
    Subscription.getActiveSubscription().then((value) => {
          activeSubscription2 = value,
          if (activeSubscription2 == 1)
            {
              Subscription.getTrialExpiryDays().then((value) async {
                trialExpiryDays = value;
              }),
            },
          setState(() {})
        });
    /*getActiveSubscription().then((value) {

      activeSubscription2=value;
    if (activeSubscription2 == 1) {
      Subscription.getTrialExpiryDays().then((value) async => setState(() {
        trialExpiryDays = value;
      }));

    }
      setState(() { });
    } );*/
  }

  bool isIapPurchased = false;
  int? trialExpiryDays = 0;
  int? activeSubscription2 = -1;

  @override
  Widget build(BuildContext context) {
    final trialText = activeSubscription2 == 2
        ? MyWidgets.textView(
            text: 'Your premium subscription is active',
            style: MyStyles.customTextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w300,
                color: MyColors.green_8c))
        : activeSubscription2 == 3
            ? MyWidgets.textView(
                text: 'Your standard subscription is active',
                style: MyStyles.customTextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w300,
                    color: MyColors.green_8c))
            : activeSubscription2 == 1
                ? MyWidgets.textView(
                    text: 'Your trial version expires in $trialExpiryDays days',
                    style: MyStyles.customTextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                        color: MyColors.red_ff_bg))
                : activeSubscription2 == 0
                    ? MyWidgets.textView(
                        text: 'Your trial/subscription has been expired',
                        style: MyStyles.customTextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w300,
                            color: MyColors.red_ff_bg))
                    : Container();
    final image = Center(
        child: Image.asset(
      'assets/images/splas_pos_xpress.png',
      height: 170,
      alignment: Alignment.topCenter,
    ));

    final upgradeText = Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          MyWidgets.textView(
              text: 'UPGRADE',
              style: MyStyles.customTextStyle(
                  fontSize: 25, fontWeight: FontWeight.bold)),
          SizedBox(
            width: 5,
          ),
          MyWidgets.container(
              topRightCornerRadius: 5,
              topLeftCornerRadius: 5,
              bottomRightCornerRadius: 5,
              bottomLeftCornerRadius: 5,
              color: MyColors.white_chinese_e0_bg,
              height: 22,
              width: 50,
              child: Center(
                  child: MyWidgets.textView(
                      text: 'Pro',
                      style: MyStyles.customTextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                          color: MyColors.black))))
        ]);

    final joinText = MyWidgets.textView(
        text: 'Enjoy hustle free services',
        style: MyStyles.customTextStyle(
            fontSize: 25, fontWeight: FontWeight.w300));

    final benefitsTExt = Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
              flex: 1,
              child: Padding(
                  padding: EdgeInsets.all(2),
                  child: MyWidgets.shadowContainer(
                      cornerRadius: 8,
                      child: Padding(
                          padding: EdgeInsets.all(5),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 8,
                              ),
                              MyWidgets.textView(
                                  text: 'Premium/Trial',
                                  style: MyStyles.customTextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.w400)),
                              SizedBox(
                                height: 3,
                              ),
                              MyWidgets.textView(
                                  text: '1 Year Auto Renewing',
                                  style: MyStyles.customTextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.normal)),
                              SizedBox(
                                height: 4,
                              ),
                              setBenefitFeatures('Create up-to 5 projects'),
                              setBenefitFeatures('Unlimited access to graphs'),
                              setBenefitFeatures('Unlimited access to reports'),
                              setBenefitFeatures('Generate invoice'),
                              SizedBox(
                                height: 12,
                              ),
                            ],
                          ))))),
          Expanded(
              flex: 1,
              child: Padding(
                  padding: EdgeInsets.all(2),
                  child: MyWidgets.shadowContainer(
                      cornerRadius: 8,
                      child: Padding(
                          padding: EdgeInsets.all(5),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 8,
                              ),
                              MyWidgets.textView(
                                  text: 'Standard ',
                                  style: MyStyles.customTextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.w400)),
                              SizedBox(
                                height: 3,
                              ),
                              MyWidgets.textView(
                                  text: '3 Months Non Renewing',
                                  style: MyStyles.customTextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.normal)),
                              SizedBox(
                                height: 4,
                              ),
                              setBenefitFeatures('Create 1 project'),
                              setBenefitFeatures('No access to graphs'),
                              setBenefitFeatures('No access to reports'),
                              setBenefitFeatures('Generate invoice'),
                              SizedBox(
                                height: 12,
                              ),
                            ],
                          )))))
        ]);

    final unlockButton = Container(
        margin: EdgeInsets.all(12),
        height: 45,
        child: Material(
            elevation: 1,
            borderRadius: BorderRadius.circular(8),
            color: MyColors.white_dutch_ec_bg,
            child: MaterialButton(
                onPressed: () async {
                  await InAppPurchaseHandler.handleInAppPurchase(context)
                      .then((iapPurchased) {
                    isIapPurchased = iapPurchased;
                    if (iapPurchased) {
                      Navigator.pop(context, isIapPurchased);
                    }
                  });
                },
                child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      MyWidgets.textView(text: 'Upgrade to Pro'),
                      SizedBox(
                        width: 5,
                      ),
                      ImageIcon(
                        AssetImage('assets/images/icon_awesome_crown.png'),
                        size: 20,
                      ),
                    ]))));

    final restoreText = InkWell(
        onTap: () async {
          //await InAppPurchaseHandler.getActiveInAppPurchasesList(context);
          if (await checkInternetConnectivity()) {
            try {
              await InAppPurchaseHandler.updateFireStoreSubscriptionToLocalDb(
                      FirebaseAuth.instance.currentUser!.email!)
                  .then((value) => showCustomAlertDialog(
                      context, 'Successfully restored the subscription.'));
            } catch (e) {
              print("Failed to get ${e}");
            }
          } else {
            showCustomAlertDialog(context,
                'Please check your internet connectivity and try again');
          }
        },
        child: Container(
            margin: EdgeInsets.all(12),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  MyWidgets.textView(text: 'Restore purchase'),
                  SizedBox(
                    width: 5,
                  ),
                  Icon(
                    Icons.arrow_forward_rounded,
                    size: 20,
                  ),
                ])));
    final termsTitle = Container(margin:EdgeInsets.only(left: 8,right: 8),child: Align(alignment: AlignmentDirectional.topStart,child:MyWidgets.textView(text: 'Terms Of Service and Privacy Policy',textAlign: TextAlign.start,style: MyStyles.customTextStyle(fontWeight: FontWeight.bold))));
    final termsText = Padding(padding: EdgeInsets.all(7),child:RichText(
        textAlign: TextAlign.start,
        text: TextSpan(children: [
      TextSpan(
          text:
          'Payment will be charged to the ${Platform.isAndroid?'PlayStore':'App store'} Account at confirmation of purchase. Subscription automatically renews unless auto-renew is turned off at least 24-hours before the end of the current period. Account will be charged for renewal within 24-hours prior to the end of the current period, and identify the cost of the renewal. Subscriptions may be managed by the user and auto-renewal may be turned off by going to the user\'s Account Settings after purchase. No cancellation of the current subscription is allowed during the active subscription period. Any unused portion of a free trial period, if offered, will be forfeited when the user purchases a subscription to that publication, where applicable. ',
          style: TextStyle(fontSize: 12, color: Colors.black,)),
      TextSpan(
          text: 'Terms Of Service',
          style: TextStyle(
              fontSize: 13,
              color: Colors.blue,
              decoration: TextDecoration.underline),
          recognizer: TapGestureRecognizer()..onTap = () {
            goto(context, WebViewScreenState());
          }),
      TextSpan(
          text: ' and ', style: TextStyle(fontSize: 12, color: Colors.black)),
      TextSpan(
          text: 'Privacy Policy',
          style: TextStyle(
              fontSize: 12,
              color: Colors.blue,
              decoration: TextDecoration.underline),
          recognizer: TapGestureRecognizer()..onTap = () {
           
            goto(context, WebViewScreenState(isTerms: false,));
          })
    ])));
    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
          leading: IconButton(
            icon: Icon(
              Icons.close_rounded,
              color: MyColors.black,
            ),
            onPressed: () => Navigator.pop(context, isIapPurchased),
          ),
        ),
        body: Container(
            height: double.infinity,
            child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(
                    parent: BouncingScrollPhysics()),
                child: Padding(
                    padding: EdgeInsets.only(left: 5, right: 5),
                    child: Container(
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                          Stack(
                            children: [
                              Image.asset('assets/images/login_top_view.png'),
                              image
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          upgradeText,
                          SizedBox(
                            height: 8,
                          ),
                          joinText,
                          SizedBox(
                            height: 5,
                          ),
                          trialText,
                          SizedBox(
                            height: 30,
                          ),
                          benefitsTExt,
                          SizedBox(
                            height: 20,
                          ),
                          unlockButton,
                          SizedBox(
                            height: 5,
                          ),
                          restoreText,
                          SizedBox(
                            height: 10,
                          ),
                              termsTitle,
                              SizedBox(
                                height: 3,
                              ),
                          termsText,

                          SizedBox(
                            height: 8,
                          ),
                        ]))))));
  }

  Widget setBenefitFeatures(String text) {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Row(children: [
          Icon(
            text == 'No access to graphs' || text == 'No access to reports'
                ? Icons.close_rounded
                : Icons.check_rounded,
            size: 13,
            color:
                text == 'No access to graphs' || text == 'No access to reports'
                    ? MyColors.red_ff_bg
                    : MyColors.green_8c,
          ),
          MyWidgets.textView(
              text: text,
              style: MyStyles.customTextStyle(
                fontSize: 12,
                fontWeight: FontWeight.normal,
              )),
          SizedBox(
            width: 5,
          ),
        ]));
  }
}
