
import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/database/workers_operations.dart';
import 'package:ecommerce/model/Project.dart';
import 'package:ecommerce/model/Worker.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/MyCustomSlidableAction.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'addProject.dart';
import 'addWorker.dart';
import 'database/product_item_operations.dart';
import 'database/worker_relation_operations.dart';



class WorkersListScreenState extends StatefulWidget {
  const WorkersListScreenState({Key? key}) : super(key: key);

  @override
  _WorkersListScreenState createState() => _WorkersListScreenState();
}

class _WorkersListScreenState extends State<WorkersListScreenState> {
  var workerRelationOperations = WorkerRelationOperations();
  @override
  void setState(VoidCallback fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  @override
  void initState() {
    Future.delayed( Duration(milliseconds: 1000), () {
      _getData();

    });
    super.initState();
    print("INIT");

  }

  @override
  Widget build(BuildContext context) {
    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
          titleSpacing: 10,
          elevation: 0,
            title:Row(mainAxisSize:MainAxisSize.max,crossAxisAlignment: CrossAxisAlignment.center,mainAxisAlignment: MainAxisAlignment.center,children: [
              Expanded(flex:7,child: ElevatedButton.icon(
                icon: const Icon(
                  Icons.create_new_folder_rounded,
                  color: MyColors.black,
                  size: 30,
                ),
                onPressed: () {
                  //openAlertBox(null, context);
                  gotoAddWorker(null);
                },
                label: Text(
                  "Add Workers",
                  style: MyStyles.customTextStyle(fontSize: 13),
                ),
                style: ElevatedButton.styleFrom(
                  elevation: 0,
                  primary: MyColors.white,
                  fixedSize: const Size(0, 44),
                  shape: MyDecorations.roundedRectangleBorder(10),
                ),
              )),
              SizedBox(width: 15,),
              Expanded(flex:3,child:MyWidgets.shadowContainer(height: 44,blurRadius: 0,cornerRadius: 10,spreadRadius: 0,shadowColor: MyColors.white_f1_bg,
                  child: Row(mainAxisSize:MainAxisSize.max,crossAxisAlignment: CrossAxisAlignment.center,mainAxisAlignment: MainAxisAlignment.center,children: [
                    Expanded(flex:1,child:Center(child: Icon(
                      Icons.list,
                      color: MyColors.black,
                    ), )),Container(width:0,height: 30, child: VerticalDivider(color: MyColors.grey_9d_bg)),
                    Expanded(flex:1,child:Center(child:Icon(
                      Icons.grid_on_rounded,
                      color: MyColors.black,
                    ),))
                  ])))],)

          /*title: Text(
            'Workers List',
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.add,
                color: Colors.blue,
              ),
              onPressed: () {
               openAlertBox(null, context);
              },
            )
          ],*/
        ),
        body:
            /*ElevatedButton(
          onPressed: () {
            // Navigate back to first route when tapped.
            Navigator.pop(context);
          },
          child: Text('Go back!'),

        )*/
            //getAllInventory());
    createListView(context));

  }

  var items = <Worker>[];

  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  var _scrollController = ScrollController();
  final teNameController = TextEditingController();
  final teSalaryController = TextEditingController();
  final teHoursController = TextEditingController();
  var workersOperations = WorkersOperations();
  var productItemOperations = ProductItemOperations();
  Project? project;
  bool isLoading = true;

  ///Fetch data from database
  Future<List<Worker>> _getData() async {
    await MyPref.getProject().then((project)async {
      this.project=project;
      if(project!=null){
        await workersOperations.workersByProjectId(project.id).then((value) {
          items = value;

        });
      }else
        showCustomAlertDialog(context, 'Please create a project ');
      setState(() {
        isLoading=false;
      });
    });


    return items;
  }

  ///create List View with
  Widget createListView(BuildContext context) {
    if (items.isNotEmpty) {
   return FadeIn(child:Container(
       height: double.infinity,
       width: double.infinity,child:SafeArea(bottom: true,child:  ListView.builder(
       physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
       key: _listKey,
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) {

          return _buildItem(context, items[index], index);

        }))));}else
        return isLoading?MyWidgets.buildCircularProgressIndicator():  Center(child: Text(
      'No Workers.',
      style: TextStyle(
        fontSize: 17.0,
        color: Colors.grey,
      ),
    ),);
  }



  Widget _buildItem(BuildContext context, Worker values, int index) {
    return


      Padding(padding:EdgeInsets.only(left: 0,right: 10),child: MySlidableWidgets.slidable(


          // The end action pane is the one at the right or the bottom side.
          endActionPane:  ActionPane(

            motion: BehindMotion(),
            extentRatio: 0.13,

            children: <Widget>[

              /*MyCustomSlidableAction(
                flex: 1,
                onPressed: (BuildContext slidableContext){
                  openAlertBox(items[index], context);
                },
                backgroundColor: MyColors.transparent,
                foregroundColor: Colors.transparent,
                child: Container(margin:EdgeInsets.only(left: 1),height: 94,child:

                Column(mainAxisSize:MainAxisSize.min,crossAxisAlignment:CrossAxisAlignment.center,mainAxisAlignment:MainAxisAlignment.center,children: [
                  Icon(
                    Icons.recycling_outlined,
                    size: 20,
                    color: MyColors.white,
                  ),Text('Edit',style: MyStyles.customTextStyle(fontSize: 11,color: MyColors.white,),),

                ],),


                    decoration: BoxDecoration(
                      color: MyColors.grey_9d_bg,

                      boxShadow: [
                        BoxShadow(
                          color: MyColors.grey_9d_bg,
                          blurRadius: 1.0,
                          offset: Offset(0.7, 0),
                        ),
                      ],
                    )),


              ),*/
              MyCustomSlidableAction(
                flex: 1,
                onPressed: (BuildContext slidableContext)async{
                  return await showCustomCallbackAlertDialog(context:context,positiveText: 'Delete',msg: 'Are you sure you wish to delete all project related data?',positiveClick:() {
                    onDelete(index);

                  },negativeClick: (){
                    Navigator.of(context).pop(false);
                  });
                },
                backgroundColor: MyColors.transparent,
                foregroundColor: Colors.transparent,
                child: MyWidgets.container(marginR:2,height: 94,
                    color: MyColors.red_ff_bg,
                    topRightCornerRadius: 10,
                    bottomRightCornerRadius: 10,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(crossAxisAlignment:CrossAxisAlignment.center,mainAxisAlignment:MainAxisAlignment.center,children: [Icon(
                      Icons.close_rounded,
                      size: 20,
                      color: MyColors.white,
                    ),Text('Delete',style: MyStyles.customTextStyle(fontSize: 11,color: MyColors.white,),)],),),


              ),



            ],
          ),

          // The child of the Slidable is what the user sees when the
          // component is not dragged.
          child: Padding(padding:EdgeInsets.only(left: 10,right: 0),child:ListTile(
              hoverColor: MyColors.transparent,

              contentPadding: EdgeInsets.all(0),

              onTap: () => /*openAlertBox(items[index], context),*/gotoAddWorker(values),

              title:
              MyWidgets.shadowContainer(height:96,paddingL:15,spreadRadius: 2, cornerRadius:10,shadowColor:MyColors.white_f1_bg,
                  child:
                  Row(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(

                            width: 66.0,
                            height: 66.0,
                            child:
                                ClipRRect(
                              borderRadius: BorderRadius.circular(35),
                              child: Image.asset(
                                'assets/images/profile_avatar_icon.png',
                                fit: BoxFit.cover,
                                gaplessPlayback: true,
                              ),
                            )
                        ),
                        SizedBox(width: 15,),
                        Expanded(flex:9,child:Container(margin: EdgeInsets.only(top: 10,bottom: 10),child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Spacer(),
                            Text(
                              values.name!,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: MyStyles.customTextStyle(fontSize: 15,color: MyColors.black,fontWeight:FontWeight.w500 ),
                              textAlign: TextAlign.left,

                            ),
                           Spacer(),
                            Text(

                              '${values.salary}',
                              style: MyStyles.customTextStyle(fontSize: 15,fontWeight:FontWeight.w300 ),
                              textAlign: TextAlign.left,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                            Spacer(),
                            Text(

                              '${values.hoursPerDay!} Hours',
                              style: MyStyles.customTextStyle(fontSize: 15,fontWeight:FontWeight.w300 ),
                              textAlign: TextAlign.left,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                            Spacer(),
                          ],
                        )))
                        ,

                        Expanded(flex:2,child:Container(

                          decoration: BoxDecoration(
                            color: MyColors.white_chinese_e0_bg,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10),
                              bottomRight: Radius.circular(10)
                          ),
                        ),child: Center( child: Icon(
                          Icons.chevron_right_rounded,
                          size: 20,
                        ),),))


                      ]) )

          ),)
      ));

  }


  ///On Item Click
  onItemClick(Worker values) {
    print("Clicked position is ${values.name}");
  }

  /// Delete Click and delete item
  onDelete(int index)async {
    var id = items[index].id;
   await workerRelationOperations.relationItemCountFromWorkerRelationByWorkerId(id).then((productCount) async {
     if(productCount==0){
    await workersOperations.deleteWorker(id!).then((value) {
    setState(() { items.removeAt(index);});

    });
    Navigator.of(context).pop(true);
     }
     else{
       Navigator.of(context).pop(false);
       context.showSnackBar("Can\''nt delete! Worker is being assigned to some products ");}

    });

  }

  ///edit User
  updateWorker(Worker w, BuildContext context) async{

      var worker = Worker();
      worker.id = w.id;
      worker.projectId = w.projectId;
      worker.name = teNameController.text;
      worker.salary = int.parse(teSalaryController.text.trim());
      worker.hoursPerDay = int.parse(teHoursController.text.trim());
      double? currentPerMinWage=0.00;
      currentPerMinWage=(((worker.salary)!/(worker.hoursPerDay!*30))/60).toDouble();
if(currentPerMinWage>w.wagePerMinNew!){
  worker.wagePerMinOld=w.wagePerMinNew;
  worker.wagePerMinNew=currentPerMinWage;
}else if(currentPerMinWage<w.wagePerMinNew!){
  worker.wagePerMinOld= currentPerMinWage;
  worker.wagePerMinNew=w.wagePerMinNew;
}else  if(currentPerMinWage==w.wagePerMinNew!){
  worker.wagePerMinOld= w.wagePerMinOld;
  worker.wagePerMinNew=w.wagePerMinNew;
}


     await workersOperations.updateWorker(worker).then((value) async{


        (currentPerMinWage!>w.wagePerMinNew!)?
            await workerRelationOperations.relationItemFromWorkerRelationByWorkerId(w.id).then((productIds)async {
              if(productIds.isNotEmpty)
                await productItemOperations.getProductItemIdAndRecalculateCostByProductItemId(productIds).then((value)async {
                  clearControllerAndPop();
              });
              else
                clearControllerAndPop();
            }):clearControllerAndPop();


      });

  }

  updateWorkerWithTransaction(Worker w, BuildContext context) async{

    var worker = Worker();
    worker.id = w.id;
    worker.projectId = w.projectId;
    worker.name = teNameController.text;
    worker.salary = int.parse(teSalaryController.text.trim());
    worker.hoursPerDay = int.parse(teHoursController.text.trim());
    double? currentPerMinWage=0.00;
    currentPerMinWage=(((worker.salary)!/(worker.hoursPerDay!*30))/60).toDouble();
    if(currentPerMinWage>w.wagePerMinNew!){
      worker.wagePerMinOld=w.wagePerMinNew;
      worker.wagePerMinNew=currentPerMinWage;
    }else if(currentPerMinWage<w.wagePerMinNew!){
      worker.wagePerMinOld= currentPerMinWage;
      worker.wagePerMinNew=w.wagePerMinNew;
    }else  if(currentPerMinWage==w.wagePerMinNew!){
      worker.wagePerMinOld= w.wagePerMinOld;
      worker.wagePerMinNew=w.wagePerMinNew;
    }


    (currentPerMinWage>w.wagePerMinNew!)?
    await workersOperations.updateWorkerWithTransaction(worker).then((value) {clearControllerAndPop();print('onUpdateIntSuccess ');}).onError((error, stackTrace) {clearControllerAndPop(isSuccess: false);print('onUpdateInvError  ${error}  ${stackTrace}');context.showSnackBar('$error');})
        :
    await workersOperations.updateWorker(worker).then((value){clearControllerAndPop();}).onError((error, stackTrace) {clearControllerAndPop(isSuccess: false);context.showSnackBar('$error');});
  }
  clearControllerAndPop({bool isSuccess = true})async{
    teNameController.text = '';
    teSalaryController.text='';
    teHoursController.text='';
    Navigator.of(context).pop();
    showtoast("Data Saved successfully");
    if(isSuccess)
    _getData();
  }
  addWorker() async{
      if(project!=null){
    var worker = Worker();
    worker.projectId=project?.id;
    worker.name = teNameController.text.trim();
    worker.salary=int.parse(teSalaryController.text.trim());
    worker.hoursPerDay=int.parse(teHoursController.text.trim());
    worker.wagePerMinNew=(((worker.salary)!/(worker.hoursPerDay!*30))/60);
    await workersOperations.insertWorker(worker).then((value) {
      clearControllerAndPop();
    });
    print('vkbvfvb ${worker.wagePerMinNew}  ${(((worker.salary)!/(worker.hoursPerDay!*30))/60)}');}else showCustomAlertDialog(context, 'Please create a project ');
  }

  bool _validate() {
  if (teNameController.text.isEmpty) {
    context.showSnackBar("Enter Worker Name");
      return false;
    }else   if (teSalaryController.text.isEmpty||int.tryParse(teSalaryController.text.trim())==0) {
    context.showSnackBar("Enter Worker Salary");
    return false;
  }else   if (teHoursController.text.isEmpty||int.tryParse(teHoursController.text.trim())==0) {
    context.showSnackBar("Enter Worker Hours per Day");
    return false;
  }else   if (int.tryParse(teHoursController.text.trim())!>24) {
    context.showSnackBar("Please Check Worker Hours per Day");
    return false;
  }
    return true;
  }
  /// Edit Click
  onEdit(Worker worker, int index, BuildContext context) {
    openAlertBox(worker, context);
  }

  /// openAlertBox to add/edit user
  openAlertBox(Worker? worker, BuildContext context) {
    if(project==null){
      showCustomAlertDialog(context, 'Please create a project ');
    }else{
    if (worker != null) {
      teNameController.text = worker.name!;
      teSalaryController.text = '${worker.salary!}';
      teHoursController.text = '${worker.hoursPerDay!}';

    } else {
      teNameController.text = "";
      teSalaryController.text = "";
      teHoursController.text = "";
    }

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25.0))),
            contentPadding: EdgeInsets.only(top: 10.0),
            content: Container(
              width: 300.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        /*flag ?*/
                        "Cooker Details" /*: "Add User"*/,
                        style: TextStyle(fontSize: 22.0),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Divider(
                    color: Colors.grey,
                    height: 4.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 15.0, right: 15.0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[

                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                  flex: 0,
                                  fit: FlexFit.tight,
                                  child:Text('Name',style: TextStyle(fontWeight: FontWeight.w400,)) //Container
                              ),SizedBox(width: 10,),Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child:TextFormField(
                                      keyboardType: TextInputType.name,
                                      controller: teNameController,
                                      obscureText: false,
                                      decoration: InputDecoration(

                                        hintText: "Add Name",
                                      ))//Container
                              )
                            ],
                          ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Flexible(
                              flex: 0,
                              fit: FlexFit.tight,
                              child:Text('Salary',style: TextStyle(fontWeight: FontWeight.w400,)) //Container
                          ),SizedBox(width: 10,),Flexible(
                              flex: 1,
                              fit: FlexFit.tight,
                              child:TextFormField(
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                                  controller: teSalaryController,
                                  obscureText: false,
                                  decoration: InputDecoration(

                                    hintText: "Salary / Month",
                                  ))//Container
                          )
                        ],
                      ),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                  flex: 0,
                                  fit: FlexFit.tight,
                                  child:Text('Hours',style: TextStyle(fontWeight: FontWeight.w400,)) //Container
                              ),SizedBox(width: 10,),Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child:TextFormField(
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                                      controller: teHoursController,
                                      obscureText: false,
                                      decoration: InputDecoration(

                                        hintText: "Hours / Day",
                                      ))//Container
                              )
                            ],
                          )
                         ,
                        ],
                      ),
                    ),
                  ),SizedBox(height: 10,),
                  InkWell(
                    onTap: () =>{if(_validate()){worker!=null?  /*updateWorker(worker, context)*/updateWorkerWithTransaction(worker, context):addWorker()}},
                    child: Container(
                      padding: EdgeInsets.only(top: 12.0, bottom: 12.0),
                      decoration: BoxDecoration(
                        color: Color(0xff01A0C7),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20.0),
                            bottomRight: Radius.circular(20.0)),
                      ),
                      child: Text(
                        worker==null?"Add Workers":"Edit Worker",
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }}


  gotoAddWorker(Worker? worker){
    this.project ==null
        ? showCustomAlertDialog(context, 'Please create a project ')
        :
    MyBottomSheet().showBottomSheet(context, callback: (value){
      print('fileCallback  ${value}');
      if(value!=null&&value==true){
        _getData();
      }
    },child:   AddWorkerScreenState(worker: worker,)
     );


  }
}
