
import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/database/worker_relation_operations.dart';
import 'package:ecommerce/database/workers_operations.dart';
import 'package:ecommerce/model/Worker.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'model/ProductItem.dart';
import 'model/WorkerRelation.dart';



class AllWorkersListScreenState extends StatefulWidget {
  final ProductItem? productItem;
  final List<Worker>? selectedWorkers;
  const AllWorkersListScreenState({@required this.productItem,@required this.selectedWorkers,Key? key}) : super(key: key);

  @override
  _AllWorkersListScreenState createState() => _AllWorkersListScreenState();
}

class _AllWorkersListScreenState extends State<AllWorkersListScreenState> {

  @override
  void initState() {
    Future.delayed( Duration(milliseconds: 1000), () {
      _getData2();

    });
    super.initState();

  }
  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MyWidgets.scaffold(
        appBar:  MyWidgets.appBar(
          title:MyWidgets.textView(text:
           'Select Worker',
            style: MyStyles.black0E__HR_16,
          ),
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back_ios_new_rounded,color: MyColors.black,),
              onPressed: () {
                Navigator.pop(context);
              }
          ),
            actions: <Widget>[
              IconButton(
                icon: /*Icon(
                Icons.check,
                color: Colors.blue,
              ),*/MyWidgets.textView(text: 'Done',style: MyStyles.customTextStyle(fontSize: 15,fontWeight: FontWeight.w500)),
                onPressed: () {
                  onBackPressed();
                },
              )

            ]
        ),
        body:
            /*ElevatedButton(
          onPressed: () {
            // Navigate back to first route when tapped.
            Navigator.pop(context);
          },
          child: Text('Go back!'),

        )*/
            //getAllInventory());
    createListView(context));

  }

  var workersItems = <Worker>[];

  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  var _scrollController = ScrollController();
  final teNameController = TextEditingController();
  final teSalaryController = TextEditingController();
  final teHoursController = TextEditingController();
  var workersOperations = WorkersOperations();
  var workRelationOperations = WorkerRelationOperations();
  var workRelations = <WorkerRelation>[];
  bool isLoading = true;

  ///Fetch data from database old
  Future<List<Worker>> _getData() async {
    await workersOperations.workers().then((value) {
      workersItems = value;

    });
    if(this.widget.selectedWorkers!=null&&this.widget.selectedWorkers!.isNotEmpty){
      //inventory list for edit product item
      for (var worker in workersItems) {
        for (var selectedWorker in widget.selectedWorkers!) {
          if (worker.id == selectedWorker.id) {
            worker.checkedState = 1;
          }
        }

      }
    }else{
    await workRelationOperations.workerRelationsItems().then((value) {
      workRelations = value;
    });
    for (var worker in workersItems) {
      for (var workRelation in workRelations) {
        if (widget.productItem?.id == workRelation.productItemId &&
            worker.id == workRelation.workerId) {
          worker.checkedState = 1;
        }


      }

      }
    }
setState(() {
  isLoading=false;
});
    return workersItems;
  }

  ///Fetch data from database
  Future<List<Worker>> _getData2() async {
   await MyPref.getProject().then((project) async{
     if(project!=null){
       await workersOperations.workersNyProjectId(project.id).then((value)async {
         workersItems = value;

         if(this.widget.selectedWorkers!=null&&this.widget.selectedWorkers!.isNotEmpty){
           //inventory list for edit product item
           workersItems.forEach((worker)async {
             widget.selectedWorkers?.forEach((selectedWorker)async {
               if (worker.id == selectedWorker.id) {
                 worker.checkedState = 1;
               }
             });
           });

         }else{
           await workRelationOperations.workerRelationsItems().then((value)async {
             workRelations = value;


             workersItems.forEach((worker)async {
               workRelations.forEach((workRelation)async {
                 if (widget.productItem?.id == workRelation.productItemId &&
                     worker.id == workRelation.workerId) {
                   worker.checkedState = 1;
                 }
               });
             });
           });
         }
       });
     }else{
       showCustomAlertDialog(context, 'Please create a project ');
     }
   });

    setState(() {
      isLoading=false;
    });
    return workersItems;
  }

  ///create List View with
  Widget createListView(BuildContext context) {
    if(workersItems.isNotEmpty){
   return FadeIn( child:Container(
       height: double.infinity,
       width: double.infinity,child:ListView.builder(
       physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
       key: _listKey,
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: workersItems.length,
        itemBuilder: (BuildContext context, int index) {
          //return _buildItem(context, items[index], index);
          return _buildItem(context, workersItems[index],index);
        })));}
    else
     return isLoading?MyWidgets.buildCircularProgressIndicator(): Center(child: Text(
        'No Workers.',
        style: TextStyle(
          fontSize: 17.0,
          color: Colors.grey,
        ),
      ),);
  }

  ///Construct cell for List View
  Widget _buildItem(BuildContext context, Worker values, int index) {

    return
      Container(
        child: ListTile(
            onTap: () => onItemClick(values),
            title:
            Column( children: <Widget>[
              Row(
                children: <Widget>[
                 Column(
                    children: <Widget>[


                          Icon(
                            Icons.account_circle_outlined,
                            size: 40,
                          ),



                    ],
                  ),
                  Padding(padding: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0)),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 0.0, 5.0)),

                      Container(
                        width: 200,
                        child: RichText(
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          strutStyle: StrutStyle(fontSize: 12.0),
                          text: TextSpan(
                            style: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 15, color: Colors.black),
                            text:'Name : ${values.name!}' ,
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),
                      Text(
                        'Salary : ${values.salary!}' ,
                        style: TextStyle(
                          fontWeight: FontWeight.w300,
                          fontSize: 15.0,
                          color: Colors.black,
                        ),
                        textAlign: TextAlign.left,
                        maxLines: 2,
                      ), Padding(padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),Text(
                        'Working Hours : ${values.hoursPerDay!}' ,
                        style: TextStyle(
                          fontWeight: FontWeight.w300,
                          fontSize: 15.0,
                          color: Colors.black,
                        ),
                        textAlign: TextAlign.left,
                        maxLines: 2,
                      ),
                      Padding(padding: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 10.0)),
                    ],
                  ),Spacer(), IconButton(
                      color:values.checkedState == 1? Colors.black: Colors.transparent,
                      icon: new Icon(Icons.check),
                      onPressed: () => onItemClick(values)),
                ],
              ),Divider(height: 1,)
            ])
        ),
      );
  }

  ///On Item Click
  onItemClick(Worker values) {
    setState(() {
    print("Clicked position is ${values.name}");
    if (values.checkedState == 0) {
      values.checkedState = 1;
    } else {
      values.checkedState = 0;
    }
    print("Clicked position is ${values.name}  ${values.checkedState}");

    });
  }

  onBackPressed(){
    var selectedItems = <Worker>[];
    workersItems.forEach((element) {
      if(element.checkedState==1)
        selectedItems.add(element);
    });
    Navigator.pop(context, selectedItems);
  }
}
