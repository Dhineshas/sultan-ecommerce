import 'dart:convert';
import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/model/Inventory.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'database/inventory_operations.dart';
import 'package:image_picker/image_picker.dart';

import 'database/product_category_operations.dart';
import 'model/Project.dart';


class AddInventoryCategoryScreenState extends StatefulWidget {
  final Inventory? inventory;
  const AddInventoryCategoryScreenState({@required this.inventory,Key? key}) : super(key: key);

  @override
  _AddInventoryCategoryScreenState createState() => _AddInventoryCategoryScreenState();
}

class _AddInventoryCategoryScreenState extends State<AddInventoryCategoryScreenState> {
  File? _image;

  final picker = ImagePicker();
  final teInvNameController = TextEditingController();
  var inventoryOperations = InventoryOperations();
  var productItemImageBase64;
  @override
  void setState(VoidCallback fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  @override
  void initState() {
    if (widget.inventory != null) {
      teInvNameController.text='${widget.inventory?.name}';
      productItemImageBase64 = widget.inventory?.imageBase;
    }
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final addInventoryCategory =
    Align(alignment: Alignment.centerLeft,
      child: MyWidgets.textView(text: 'Add Inventory Category',
        style: MyStyles.customTextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 24,
        ),
      ),
    );

    final imageView =
    GestureDetector(
        onTap: () {
          _showPicker(context);

        },
        child: Container(
          decoration: BoxDecoration(
            // color: Colors.grey[200],
              borderRadius: BorderRadius.circular(75)
          ),
          height: 104,
          width: 104,
          child: productItemImageBase64 != null?
          ClipRRect(
            borderRadius: BorderRadius.circular(75),
            child: Image.memory(
              base64Decode('$productItemImageBase64'),
              width: 100,
              height: 100,
              fit: BoxFit.cover,
              gaplessPlayback: true,
            ),
          ):_image != null
              ? ClipRRect(
            borderRadius: BorderRadius.circular(75),
            child: Image.file(
              _image!,
              width: 100,
              height: 100,
              fit: BoxFit.cover,
              gaplessPlayback: true,
            ),
          )
              : Container(
            decoration: BoxDecoration(
              // color: Colors.grey[200],
                border: Border.all(color: Colors.black,width: 1),
                borderRadius: BorderRadius.circular(75)
            ),
            width: 100,
            height: 100,
            child: Icon(
              Icons.image_rounded,
              color: Colors.grey[800],
            ),
          ),
        ));

    final addInventoryCategoryEditTxt = /*FadeInUp(delay: Duration(milliseconds: 200),child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: TextFormField(
            keyboardType: TextInputType.name,
            textCapitalization: TextCapitalization.words,
            controller: teInvNameController,
            obscureText: false,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
              hintText: "Inventory Name",

            ))))*/
    Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text: 'Name'),
          SizedBox(height: 10,),
    MyWidgets.textViewContainer(

            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child: Align(alignment:Alignment.centerLeft,child:MyWidgets.textFromField(

              contentPaddingL: 15.0,

              contentPaddingR: 15.0,

              cornerRadius: 8,
              keyboardType: TextInputType.name,
              controller: teInvNameController,
            ),
          )),
        ]
    );
    final addInventoryCategoryButton = /*FadeInUp(delay: Duration(milliseconds: 300),child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(10.0),
          color: (Colors.teal[900])!,
          child: MaterialButton(
            minWidth: MediaQuery.of(context).size.width,
            onPressed: () {
              //_validate()  ? addInventory() : {};
              if(_validate()){
                widget.inventory==null? getSelectedProjectAndAddInventoryCategory() : updateInventory();
              }
            },
            child: Text(
              widget.inventory==null?"Add Inventory":"Update Inventory",
              textAlign: TextAlign.center, style: TextStyle(
                 color: Colors.white),
            ),
          ),
        )))*/

    MyWidgets.materialButton(context,text: widget.inventory==null?"Add Inventory":"Update Inventory",onPressed:() { if(_validate()){
      widget.inventory==null? getSelectedProjectAndAddInventoryCategory() : updateInventory();
    }});
    return Scaffold(
      /*appBar: AppBar(
        title: Text(
          'Add Inventory',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),*/
      body: SingleChildScrollView(
          padding: EdgeInsets.only(left: 30, right: 30, top: 25, bottom: 20),

              child: Column(
                children: <Widget>[
                  addInventoryCategory,
                  SizedBox(
                    height: 30,
                  ),
                  // imageView,
                  // SizedBox(
                  //   height: 20,
                  // ),
                  addInventoryCategoryEditTxt,
                  SizedBox(
                    height: 30,
                  ),
                  addInventoryCategoryButton
                ],
              )),
    );
  }

  void _showPicker(context) {
    /*showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  ListTile(
                    leading: Icon(Icons.photo_camera),
                    title: Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });*/

    MyImagePickerBottomSheet.showPicker(context, fileCallback: (image) {
      productItemImageBase64 = null;
      setState(() {
        _image = image!=null?File(image.path):null;
      });
      return null;
    });
  }

 /* _imgFromCamera() async {
    var image =
    await picker.pickImage(source: ImageSource.camera, imageQuality: 0,maxHeight:150,maxWidth: 150);
    productItemImageBase64 = null;
    setState(() {
      _image = image!=null?File(image.path):null;
    });
  }

  _imgFromGallery() async {
    var image =
    await picker.pickImage(source: ImageSource.gallery, imageQuality: 0,maxHeight:150,maxWidth: 150);
    productItemImageBase64 = null;
    setState(() {
      _image = image!=null?File(image.path):null;


    });
  }*/

  bool _validate()  {
    // if (productItemImageBase64 == null && _image == null) {
    //   context.showSnackBar("Select Inventory Image");
    //   return false;
    // } else
      if (teInvNameController.text.isEmpty) {
      context.showSnackBar("Enter Inventory Category Name");

      return false;
    }
    //   else if (teInvNameController.text.startsWith(RegExp(r'^\d+\.?\d{0,2}'))) {
    //   context.showSnackBar("Can\'t Start Inventory Category Name with Number");
    //   return false;
    // }

    return true;
  }

  /*String getImageBase64() {
    if (_image != null) {
      var bytes = _image?.readAsBytesSync();
      var base64Image = base64Encode(bytes!);
      return base64Image.toString();
    }
    return "";
  }*/
  var productCategoryOperations = ProductCategoryOperations();
  addInventory()async {

    var inventory = Inventory();
    inventory.projectId = project?.id;
    inventory.name = teInvNameController.text.trim().toString();
    inventory.imageBase=getImageBase64(_image);
   await inventoryOperations.insertInventory(inventory).then((value)async {
      teInvNameController.text = "";

      showtoast("Successfully Added Data");
      Navigator.pop(context,true);


    });
  }

  updateInventory() async{
    if (widget.inventory != null) {
      var inventory = Inventory();
      inventory.id = widget.inventory?.id;
      inventory.projectId = widget.inventory?.projectId;
      inventory.name = teInvNameController.text.trim().toString();
      inventory.imageBase=productItemImageBase64 != null
          ? productItemImageBase64
          : getImageBase64(_image);
      await inventoryOperations.updateInventory(inventory).then((value) {
        teInvNameController.text = "";

        Navigator.of(context).pop(true);
        showtoast("Data Saved successfully");

      });
    }
  }
  Project? project ;
  getSelectedProjectAndAddInventoryCategory()async{
    await MyPref.getProject().then((value) {
      project=value;
      if(value!=null)
        addInventory();
      else
        context.showSnackBar("Select project");
    });
  }
}
