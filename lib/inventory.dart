
import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/productCategoryList.dart';
import 'package:ecommerce/storage.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'inventoryCategoryList.dart';

class InventoryScreenState extends StatefulWidget {
  const InventoryScreenState({Key? key}) : super(key: key);

  @override
  _InventoryScreenState createState() => _InventoryScreenState();
}

class _InventoryScreenState extends State<InventoryScreenState> {
  @override
  void setState(VoidCallback fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          backgroundColor: MyColors.white_f1_bg,
        appBar:PreferredSize(preferredSize: Size(MediaQuery.of(context).size.width,60),
        child:Container(margin:EdgeInsets.fromLTRB(10, 10, 10, 0),decoration: MyDecorations.whiteBGRounded10,child: TabBar(
          padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
          indicator: MyDecorations.greenShadowBGRounded10,
          tabs: [
            Tab(
              child: Text(
                'Items',
                style: TextStyle(fontSize:12,color: Colors.black),
              ),
            ),
            Tab(
              child: Text(
                'Products',
                style: TextStyle(fontSize:12,color: Colors.black),
              ),
            ),
            Tab(
              child: Text(
                'Storage',
                style: TextStyle(fontSize:12,color: Colors.black),
              ),
            ),
          ],
        ),) ,) ,

        body:
        FadeIn( child:
        TabBarView(
          physics: AlwaysScrollableScrollPhysics(
              parent: BouncingScrollPhysics()),
          children: [
            InventoryCategoryListScreenState(),
            ProductCategoryListScreenState(),
            StorageScreenState(),
          ],
        ),)
      ),
    );
  }

}
