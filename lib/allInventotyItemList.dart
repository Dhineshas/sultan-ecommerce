import 'dart:convert';
import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/model/InventoryItem.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';
import 'database/inventory_item_operations.dart';
import 'database/relation_operations.dart';
import 'model/ProductItem.dart';
import 'model/InventoryRelation.dart';

class AllInventoryItemListScreenState extends StatefulWidget {
  final ProductItem? productItem;
  //final items = <InventoryItem?>[];

  final List<InventoryItem>? selectedInventoryItem;

   const AllInventoryItemListScreenState({@required this.productItem,@required this.selectedInventoryItem, Key? key})
      : super(key: key);

  @override
  _AllInventoryItemListScreenState createState() =>
      _AllInventoryItemListScreenState();
}

class _AllInventoryItemListScreenState
    extends State<AllInventoryItemListScreenState> {
  @override
  void initState() {
    Future.delayed( Duration(milliseconds: 1000), () {
      _getData2();

    });
    super.initState();
    print("INIT");
    /*myFuture = _getData();*/
  }
  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(

          leading: new IconButton(
              icon: new Icon(Icons.arrow_back_ios_new_rounded,color: MyColors.black,),
              onPressed: () {
                //onBackPressed();
                Navigator.pop(context);
              }
          ),
          title: Text(/*widget.productItem==null?*/'Select Item'
                /*:'${widget.productItem?.name}'*/,
            style: TextStyle(color: Colors.black),
          ),

          actions: <Widget>[
            IconButton(
              icon: /*Icon(
                Icons.check,
                color: Colors.blue,
              ),*/MyWidgets.textView(text: 'Done',style: MyStyles.customTextStyle(fontSize: 15,fontWeight: FontWeight.w500)),
              onPressed: () {
                onBackPressed();
              },
            )

          ],
        ),
        body:items.isNotEmpty
            ? /*ListView.builder(
            key: _listKey,
            controller: _scrollController,
            shrinkWrap: true,
            itemCount: items.length,
            itemBuilder: (BuildContext context, int index) {
              return _buildItem(context, items[index]);
            })*/

        FadeIn(child:Container(
            height: double.infinity,
            width: double.infinity,child: ListView.builder(physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),itemCount: inventoryCategory.length,itemBuilder: (context, headerIndex) {
          return StickyHeader(
            header: Container(
              height: 30.0,
              color: MyColors.grey_70,
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              alignment: Alignment.centerLeft,
              child:MyWidgets.textView(text: '${inventoryCategory[headerIndex]}',style: MyStyles.customTextStyle(fontSize: 14,color: MyColors.white)),
            ),
            content: Column(
              children: List<InventoryItem>.generate(items.where((element) => inventoryCategory[headerIndex]==element.inventoryName).length, (index) => items.where((element) => inventoryCategory[headerIndex]==element.inventoryName).elementAt(index)).map((item) =>
                //_buildItem(context, item))
          _buildListItem(item))
                  .toList(),
            )

          );})))
            :isLoading?MyWidgets.buildCircularProgressIndicator():  Center(child: Text(
          'No Inventories.',
          style: TextStyle(
            fontSize: 17.0,
            color: Colors.grey,
          ),
        ),)
            );
  }

  var inventoryCategory = <String>[];
  var items = <InventoryItem>[];
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool insertItem = false;
  var _scrollController = ScrollController();
  final teNameController = TextEditingController();
  var inventoryItemOperations = InventoryItemOperations();
  var relations = <InventoryRelation>[];
  bool isLoading = true;



  ///Fetch data from database   old
  Future<List<InventoryItem>> _getData() async {
    await MyPref.getProject().then((project)async {
      if(project!=null){


    await inventoryItemOperations.inventoriesItemsByProjectIdWithCategory(project.id).then((value) {
      items = value;

    });

    /*if (widget.productItem != null) {
      await relationOperations.relationsItems().then((value) {
        relations = value;
      });

      for (var inventoryItem in items) {
        for (var relationItem in relations) {
          if (widget.productItem?.id == relationItem.productItemId &&
              inventoryItem.id == relationItem.inventoryItemId) {
            inventoryItem.checkedState = 1;
          }
          if(!inventoryCategory.contains(inventoryItem.inventoryName)){
            inventoryCategory.add(inventoryItem.inventoryName.toString());
          }

        }
      }
    }else if(this.widget.selectedInventoryItem!=null&&this.widget.selectedInventoryItem!.isNotEmpty){
      for (var inventoryItem in items) {
        for (var selectedInventoryItem in widget.selectedInventoryItem!) {
          if (inventoryItem.id == selectedInventoryItem.id) {
            inventoryItem.checkedState = 1;
          }
        }

      }
    }*/


    if(this.widget.selectedInventoryItem!=null&&this.widget.selectedInventoryItem!.isNotEmpty){
      //inventory list for edit product item
      for (var inventoryItem in items) {
        for (var selectedInventoryItem in widget.selectedInventoryItem!) {
          if (inventoryItem.id == selectedInventoryItem.id) {
            inventoryItem.checkedState = 1;
          }
        }
        if(!inventoryCategory.contains(inventoryItem.inventoryName)){
          inventoryCategory.add(inventoryItem.inventoryName.toString());
        }
      }
    }else{
      //inventory list for new product item
      await relationOperations.relationsItems().then((value) {
        relations = value;
      });

      for (var inventoryItem in items) {
        for (var relationItem in relations) {
          if (widget.productItem!=null&&widget.productItem?.id == relationItem.productItemId &&
              inventoryItem.id == relationItem.inventoryItemId) {
            inventoryItem.checkedState = 1;
          }


        }
        if(!inventoryCategory.contains(inventoryItem.inventoryName)){
          inventoryCategory.add(inventoryItem.inventoryName.toString());
        }
      }
    }
      }else{
        showCustomAlertDialog(context, 'Please create a project ');
      }
    });
setState(() {
  isLoading=false;
});
    return items;
  }


  ///Fetch data from database neww
  Future<List<InventoryItem>> _getData2() async {
    await MyPref.getProject().then((project)async {
      if(project!=null){
        await inventoryItemOperations.inventoriesItemsByProjectIdWithCategory(project.id).then((value) async{
          items = value;



        if(this.widget.selectedInventoryItem!=null&&this.widget.selectedInventoryItem!.isNotEmpty){
          //inventory list for edit product item
           items.forEach((inventoryItem)async {
              widget.selectedInventoryItem?.forEach((selectedInventoryItem) {
                if (inventoryItem.id == selectedInventoryItem.id) {
                  inventoryItem.checkedState = 1;
                }
             });
              if(!inventoryCategory.contains(inventoryItem.inventoryName)){
                inventoryCategory.add(inventoryItem.inventoryName.toString());
              }
          });
        }else{
          //inventory list for new product item
          await relationOperations.relationsItems().then((value)async {
            relations = value;


          items.forEach((inventoryItem) async{
            relations.forEach((relationItem)async {
              if (widget.productItem!=null&&widget.productItem?.id == relationItem.productItemId &&
                  inventoryItem.id == relationItem.inventoryItemId) {
                inventoryItem.checkedState = 1;
              }
            });
            if(!inventoryCategory.contains(inventoryItem.inventoryName)){
              inventoryCategory.add(inventoryItem.inventoryName.toString());
            }
          });
          });
        }
        });
      }else{
        showCustomAlertDialog(context, 'Please create a project ');
      }
    });
    setState(() {
      isLoading=false;
    });
    return items;
  }

  Widget _buildListItem(InventoryItem values) {

    return InkWell(
        onTap: (){
          onItemClick(values);
        },child:Container(
        margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
        child: Column(
          children: <Widget>[
            Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[

                  Container(
                    margin: EdgeInsets.all(4.0),
                    child: values.imageBase != null &&
                        values.imageBase.toString().isNotEmpty
                        ? ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.memory(
                        base64Decode(values.imageBase.toString()),
                        width: 40,
                        height: 40,
                        gaplessPlayback: true,
                        fit: BoxFit.fitHeight,
                      ),
                    )
                        : Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(10)),
                      width: 40,
                      height: 40,
                    ),
                  ),
                 SizedBox(width: 5,),
                 // Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0)),
                  /*Text(
                    values.name!,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17.0,
                      color: Colors.black,
                    ),
                    textAlign: TextAlign.left,
                    maxLines: 2,
                  ),*/
                  MyWidgets.textView(text: values.name!,style: MyStyles.customTextStyle(fontSize: 14,fontWeight: FontWeight.w500),textAlign: TextAlign.left,
                    maxLine: 2,),
                  Spacer(), IconButton(
                        color:values.checkedState == 1? Colors.black: Colors.transparent,
                        icon: new Icon(Icons.check),
                        onPressed: () => onItemClick(values)),


                  //name and count
                ]),Divider(height: 1),
          ],
        )));
  }
  ///On Item Click
  onItemClick(InventoryItem values) {


    /*if(widget.productItem==null){*/
    setState(() {
      if (values.checkedState == 0) {
        values.checkedState = 1;
      } else {
        values.checkedState = 0;
      }
      print("Clicked position is ${values.name}  ${values.checkedState}");

      });
    /*}else{
      addRelation(values);
  }*/}

  var relationOperations = RelationOperations();

  addRelation(InventoryItem inventoryItem) async{
    if (inventoryItem.checkedState == 0) {
      var relation = InventoryRelation();
      relation.productItemId = widget.productItem?.id;
      relation.inventoryItemId = inventoryItem.id;
     await relationOperations.insertRelation(relation).then((value)async {

       await _getData2().then((value){setState(() {

        });});
      });
    } else {
     await relationOperations
          .deleteRelationByProductItemIdAndInventoryItemId(
              widget.productItem?.id, inventoryItem.id!)
          .then((value) async{
       await _getData2().then((value){setState(() {

        });});


      });
    }
  }

  onBackPressed(){
    var selectedItems = <InventoryItem>[];
    items.forEach((element) {
      if(element.checkedState==1)
        selectedItems.add(element);
    });
    Navigator.pop(context, selectedItems);
  }
}
