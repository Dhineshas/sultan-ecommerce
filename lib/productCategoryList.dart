import 'dart:convert';
import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/database/product_category_operations.dart';
import 'package:ecommerce/model/ProductItem.dart';
import 'package:ecommerce/productItemDetails.dart';
import 'package:ecommerce/productItemList.dart';
import 'package:ecommerce/switchProductCategory.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyCustomSlidableAction.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'addProductCategory.dart';
import 'addProductNew.dart';
import 'database/DatabaseHelper.dart';
import 'database/cart_relation_operations.dart';
import 'database/orders_operations.dart';
import 'database/product_item_operations.dart';
import 'database/project_product_relation_operations.dart';
import 'database/relation_operations.dart';
import 'database/storage_operations.dart';
import 'model/ProductCategory.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'model/Project.dart';

class ProductCategoryListScreenState extends StatefulWidget {
  const ProductCategoryListScreenState({Key? key}) : super(key: key);

  @override
  _ProductCategoryListScreenState createState() =>
      _ProductCategoryListScreenState();
}

class _ProductCategoryListScreenState
    extends State<ProductCategoryListScreenState> {
  @override
  void initState() {
    /*MyPref.getProject().then((value) =>
     value==null
         ? {
     setState(() { isLoading=false;}),
       showCustomAlertDialog(context, 'Please create a project ',)

   } : Future.delayed( Duration(milliseconds: 1000), () {
       _getData();

     })

   );*/
    Future.delayed(Duration(milliseconds: 1000), () {
      _getData();
    });
    super.initState();

    // _scrollController = new ScrollController();
    //_scrollController.addListener(() => setState(() {}));
  }

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
          titleSpacing: 10,
          elevation: 0,
          title: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  flex: 7,
                  child: ElevatedButton.icon(
                    icon: const Icon(
                      Icons.create_new_folder_rounded,
                      color: MyColors.black,
                      size: 30,
                    ),
                    onPressed: () async {
                      showAddProductCategoryBottomSheet(null);
                    },
                    label: Text(
                      "Product Category",
                      style: MyStyles.customTextStyle(fontSize: 13),
                    ),
                    style: ElevatedButton.styleFrom(
                      elevation: 0,
                      primary: MyColors.white,
                      fixedSize: const Size(0, 44),
                      shape: MyDecorations.roundedRectangleBorder(10),
                    ),
                  )),
              SizedBox(
                width: 15,
              ),
              Expanded(
                  flex: 3,
                  child: MyWidgets.shadowContainer(
                      height: 44,
                      blurRadius: 0,
                      cornerRadius: 10,
                      spreadRadius: 0,
                      shadowColor: MyColors.white_f1_bg,
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  child: Center(
                                    child: Icon(
                                      Icons.list,
                                      color: MyColors.black,
                                    ),
                                  ),
                                  onTap: (){
                                    setState(() {
                                      this.isListView = true;
                                    });
                                  },
                                )),
                            Container(
                                width: 0,
                                height: 30,
                                child: VerticalDivider(
                                    color: MyColors.grey_9d_bg)),
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  child: Center(
                                    child: Icon(
                                      Icons.grid_on_rounded,
                                      color: MyColors.black,
                                    ),
                                  ),
                                  onTap: (){
                                    setState(() {
                                      this.isListView = false;
                                    });
                                  },
                                ))
                          ])))
            ],
          ),
        )
        /*bottomNavigationBar: MyWidgets.shadowContainer(child: Row(mainAxisSize: MainAxisSize.min,  mainAxisAlignment: MainAxisAlignment.spaceEvenly,crossAxisAlignment: CrossAxisAlignment.center,
          children: [
          Container(
            height: 70 ,
            width: 70,
            child: FittedBox(
              child: Image.asset('assets/images/group_25.png'),
              fit: BoxFit.cover,
            ),
          ),
            Expanded(flex:1,child: ElevatedButton.icon(

              icon: const Icon(
                Icons.add_circle_outline_rounded,
                color: MyColors.black,
              ),
              onPressed: (){
                gotoAddProductCategory();
              },
              label: Text(
                "Add New",
                style: MyStyles.customTextStyle(fontSize: 13),
              ),
              style: ElevatedButton.styleFrom(
                primary: MyColors.white_chinese_e0_bg,
                fixedSize: const Size(0, 44),
                shape: MyDecorations.roundedRectangleBorder(8),
              ),
            ))
          ,SizedBox( height:5,width: 15,)],))*/
        ,
        bottomNavigationBar: MyWidgets.addNewButton(text:isLongPressEnabled?'Move To':'Add New',onAddOrMovePressed: (){
          this.project ==null
              ? showCustomAlertDialog(context, 'Please create a project ')
              :isLongPressEnabled?showBottomSheet(SwitchProductCategoryScreenState(isFromProductItemList: false,productCategoryIdArray: [],productItemIdArray: productItemIdArray,)):
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      AddProductNewScreenState(
                        productCategory: null,
                      ))).then((value) => value == true
              ? {
            _getData()
          }
              : {});
        }),
        body: createListView(context));
  }

  var items = <ProductCategory>[];
  var values = <ProductCategory>[];

  //late List<Dog> values;
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool insertItem = false;
  var _scrollController = ScrollController();
  final teNameController = TextEditingController();
  var productCategoryOperations = ProductCategoryOperations();


  //var productItemRelationOperations = RelationOperations();
  //var workerRelationOperations = WorkerRelationOperations();

  //var cartRelationOperations = CartRelationOperations();
  var orderOperations = OrderOperations();
  var productItemOperations = ProductItemOperations();
  //var storageOperations = StorageOperations();
  bool isLoading = true;
  bool isListView = true;
  Project? project;

  /// Get all users data
  /* getAllProductCategory() {
    return FutureBuilder(
        future: _getData(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return createListView(context, snapshot);
        });
  }*/

  ///Fetch data from database
  Future<List<ProductCategory>> _getData() async {
    MyPref.getProject().then((project) async {
      this.project = project;
      if (project != null) {
        await productCategoryOperations
            .productCategoriesByProjectWithNullProductItemIdAndProductItemCount(project.id)
            .then((value)async {
          items = value;


        });

      }
      setState(() {
        isLoading = false;
      });
    });

    return items;
  }

  ///create List View with Animation
  Widget createListView(BuildContext context) {
    if (items.isNotEmpty) {
      return FadeIn(
          child: Container(
              // padding: EdgeInsets.all(15),
              height: double.infinity,
              width: double.infinity,
              child: this.isListView ? showListView() : showGridView()));
    } else
      return isLoading
          ? MyWidgets.buildCircularProgressIndicator()
          : Center(
              child: Text(
                'No Product Category.',
                style: TextStyle(
                  fontSize: 17.0,
                  color: Colors.grey,
                ),
              ),
            );
  }

  Widget showListView() {
    return SafeArea(bottom: true,child: ListView.builder(
                key: _listKey,
                physics: AlwaysScrollableScrollPhysics(
                    parent: BouncingScrollPhysics()),
                controller: _scrollController,
                shrinkWrap: true,
                itemCount: items.length,
                itemBuilder: (BuildContext context, int index) {
                  if(items[index].hasCategory!=null&&items[index].hasCategory==-1)
                  return _buildProductCategoryList(context, items[index], index);
                  else
                    return _buildProductItemList(context, items[index], index);
                }));
  }

  Widget showGridView() {
    return SafeArea(bottom: true,child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 1.2,
            crossAxisSpacing: 00,
            mainAxisSpacing: 10,
            crossAxisCount: 2
        ),
        key: _listKey,
        physics: AlwaysScrollableScrollPhysics(
            parent: BouncingScrollPhysics()),
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) {
          if(items[index].hasCategory!=null&&items[index].hasCategory==-1)
            return _buildProductCategoryGrid(context, items[index], index);
          else
            return _buildProductItemGrid(context, items[index], index);
        }));
  }

  ///Construct cell for List View
  Widget _buildProductCategoryList(BuildContext context, ProductCategory values, int index) {
    print('hasCategory ${values.hasCategory}');
    return Padding(
        padding: EdgeInsets.only(left: 0, right: 10),
        child: MySlidableWidgets.slidable(
            // The end action pane is the one at the right or the bottom side.
            endActionPane: ActionPane(
              motion: BehindMotion(),
              extentRatio: 0.25,
              children: <Widget>[
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    // editProductCategory2(items[index],context);
                    showAddProductCategoryBottomSheet(values);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 94,
                    color: MyColors.grey_9d_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.recycling_outlined,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Edit',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) async {
                    return await showCustomCallbackAlertDialog(
                        context: context,
                        positiveText: 'Delete',
                        msg:
                            'Are you sure you wish to delete all project related data?',
                        positiveClick: () {
                          onDeleteAllDataAndProductCategory(index);
                        },
                        negativeClick: () {
                          Navigator.of(context).pop(false);
                        });
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginR: 2,
                    height: 94,
                    topRightCornerRadius: 10,
                    bottomRightCornerRadius: 10,
                    color: MyColors.red_ff_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.close_rounded,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Delete',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),

            // The child of the Slidable is what the user sees when the
            // component is not dragged.
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 0),
              child: ListTile(
                  hoverColor: MyColors.transparent,
                  contentPadding: EdgeInsets.all(0),
                  onTap: () {
                    if(!isLongPressEnabled){
                      onItemClick(values);
                    }
                    },
                  title: MyWidgets.shadowContainer(
                      height: 96,
                      //paddingL: 15,
                      paddingT: 15,
                      paddingR: 15,
                      paddingB: 15,
                      spreadRadius: 2,
                      cornerRadius: 10,
                      shadowColor: MyColors.white_f1_bg,
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                width: 85.0,
                                height: 66.0,
                                child: /*values.imageBase != null &&
                                        values.imageBase
                                            .toString()
                                            .isNotEmpty
                                    //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                                    ? ClipRRect(
                                        borderRadius: BorderRadius.circular(35),
                                        child:values.hasCategory==-1?Icon(Icons.folder,size: 30,): Image.memory(
                                          base64Decode(values
                                              .imageBase
                                              .toString()),
                                          fit: BoxFit.cover,
                                          gaplessPlayback: true,
                                        ),
                                      )
                                    : ClipRRect(
                                        borderRadius: BorderRadius.circular(35),
                                        child: Container(
                                          color: MyColors.grey_70,
                                        ),
                                      )*/Image.asset('assets/images/icon_folder.png',gaplessPlayback: true,
                                  fit: BoxFit.fill,)),
                            SizedBox(
                              width: 15,
                            ),
                            Expanded(
                                flex: 8,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text(
                                      values.name!,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: MyStyles.customTextStyle(
                                          fontSize: 15,
                                          color: MyColors.black,
                                          fontWeight: FontWeight.w500),
                                      textAlign: TextAlign.left,
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      '${values.productItemCount} Items',
                                      style: MyStyles.customTextStyle(
                                          fontSize: 15,
                                          color: MyColors.grey_70,
                                          fontWeight: FontWeight.w300),
                                      textAlign: TextAlign.left,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ],
                                )),
                            Icon(
                              Icons.chevron_right_rounded,
                              size: 20,
                            ),
                          ]))),
            )));
  }
  Widget _buildProductItemList(BuildContext context, ProductCategory values, int index) {
    return Padding(
        padding: EdgeInsets.only(left: 0, right: 10),
        child: MySlidableWidgets.slidable(
          // The end action pane is the one at the right or the bottom side.
            endActionPane: ActionPane(
              motion: BehindMotion(),
              extentRatio: 0.25,
              children: <Widget>[
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    onEditProductItem(items[index], index, context);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 94,
                    color: MyColors.grey_9d_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.recycling_outlined,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Edit',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) async {
                    return await showCustomCallbackAlertDialog(
                        context: context,
                        positiveText: 'Delete',
                        msg:
                        'Are you sure you wish to delete all project related data?',
                        positiveClick: () {
                          onDeleteAllDataAndProductItem(index);
                        },
                        negativeClick: () {
                          Navigator.of(context).pop(false);
                        });
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginR: 2,
                    height: 94,
                    topRightCornerRadius: 10,
                    bottomRightCornerRadius: 10,
                    color: MyColors.red_ff_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.close_rounded,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Delete',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),

            // The child of the Slidable is what the user sees when the
            // component is not dragged.
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 0),
              child: ListTile(
                  hoverColor: MyColors.transparent,
                  contentPadding: EdgeInsets.all(0),
                  onLongPress: (){
                    isLongPressEnabled=true;
                    toggleSelection(values);
                  },
                  onTap: () {
                    if(isLongPressEnabled)
                      toggleSelection(values);
                    else
                      onProductItemClick(values);
                    },
                  title: MyWidgets.shadowContainer(
                      height: 96,
                      paddingL: 15,
                      paddingT: 15,
                      spreadRadius: 2,
                      cornerRadius: 10,
                      shadowColor: MyColors.white_f1_bg,
                      color: values.isSelected==1?MyColors.white_chinese_e0_bg:MyColors.white,

                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                width: 66.0,
                                height: 66.0,
                                margin: EdgeInsets.only(bottom: 15),
                                child: values.imageBase != null &&
                                    values.imageBase.toString().isNotEmpty
                                //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                                    ? ClipRRect(
                                  borderRadius: BorderRadius.circular(35),
                                  child: Image.memory(
                                    base64Decode(
                                        values.imageBase.toString()),
                                    fit: BoxFit.cover,
                                    gaplessPlayback: true,
                                  ),
                                )
                                    : ClipRRect(
                                  borderRadius: BorderRadius.circular(35),
                                  child: Container(
                                    color: MyColors.grey_70,
                                  ),
                                )),
                            SizedBox(
                              width: 3,
                            ),
                            Expanded(
                                flex: 8,
                                child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.stretch,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Padding(
                                      padding:
                                      EdgeInsets.only(left: 8, right: 8),
                                      child: Text(
                                        values.name!,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: MyStyles.customTextStyle(
                                            fontSize: 15,
                                            color: MyColors.black,
                                            fontWeight: FontWeight.w500),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: EdgeInsets.only(left: 8),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.max,
                                            children: [
                                              MyText.priceCurrencyText(context,
                                                  price:
                                                  'Cost: ${double.parse((values.cost!).toString())}',
                                                  fontSize: 15,
                                                  fontWeight:
                                                  FontWeight.normal),
                                              Spacer(),
                                              Icon(
                                                Icons.chevron_right_rounded,
                                                size: 20,
                                              ),
                                              SizedBox(
                                                width: 8,
                                              )
                                            ],
                                          ),
                                        )),
                                    Expanded(
                                        flex: 1,
                                        child: MyWidgets.container(
                                            paddingL: 8,
                                            bottomRightCornerRadius: 10,
                                            color: MyColors.white_chinese_e0_bg,
                                            child: Align(
                                              alignment: Alignment.centerLeft,
                                              child: MyText.priceCurrencyText(
                                                  context,
                                                  price:
                                                  'Selling Price: ${values.sellingPrice!}',
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.bold),
                                              // Text(
                                              //   'Selling Price: ${values.sellingPrice}',
                                              //   maxLines: 1,
                                              //   overflow: TextOverflow.ellipsis,
                                              //   style: MyStyles.customTextStyle(
                                              //       fontSize: 15,
                                              //       color: MyColors.black,
                                              //       fontWeight:
                                              //           FontWeight.w500),
                                              //   textAlign: TextAlign.left,
                                              // ),
                                            )))
                                  ],
                                )),
                          ]))),
            )));
  }

  Widget _buildProductCategoryGrid(BuildContext context, ProductCategory values, int index) {
    return Padding(
        padding: EdgeInsets.only(left: 0, right: 10),
        child: MySlidableWidgets.slidable(
          // The end action pane is the one at the right or the bottom side.
            endActionPane: ActionPane(
              motion: BehindMotion(),
              extentRatio: 0.45,
              children: <Widget>[
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    // editProductCategory2(items[index],context);
                    showAddProductCategoryBottomSheet(values);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 145,
                    color: MyColors.grey_9d_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.recycling_outlined,
                          size: 20,
                          color: MyColors.white,
                        ),
                        // Text(
                        //   'Edit',
                        //   style: MyStyles.customTextStyle(
                        //     fontSize: 11,
                        //     color: MyColors.white,
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) async {
                    return await showCustomCallbackAlertDialog(
                        context: context,
                        positiveText: 'Delete',
                        msg:
                        'Are you sure you wish to delete all project related data?',
                        positiveClick: () {
                          onDeleteAllDataAndProductCategory(index);
                        },
                        negativeClick: () {
                          Navigator.of(context).pop(false);
                        });
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginR: 2,
                    height: 145,
                    topRightCornerRadius: 10,
                    bottomRightCornerRadius: 10,
                    color: MyColors.red_ff_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.close_rounded,
                          size: 20,
                          color: MyColors.white,
                        ),
                        // Text(
                        //   'Delete',
                        //   style: MyStyles.customTextStyle(
                        //     fontSize: 11,
                        //     color: MyColors.white,
                        //   ),
                        // )
                      ],
                    ),
                  ),
                ),
              ],
            ),

            // The child of the Slidable is what the user sees when the
            // component is not dragged.
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 0),
              child: ListTile(
                  hoverColor: MyColors.transparent,
                  contentPadding: EdgeInsets.all(0),
                  // onTap: () => onItemClick(values),
                  onTap: () {
                    if(!isLongPressEnabled){
                      onItemClick(values);
                    }
                  },
                  title: MyWidgets.shadowContainer(
                      height: 145,
                      //paddingL: 15,
                      paddingT: 10,
                      paddingR: 15,
                      paddingB: 15,
                      spreadRadius: 2,
                      cornerRadius: 10,
                      shadowColor: MyColors.white_f1_bg,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                  width: 100.0,
                                  height: 77.0,
                                  child: /*values.imageBase != null &&
                                      values.imageBase
                                          .toString()
                                          .isNotEmpty
                                  //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                                      ? ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child:values.hasCategory==-1?Icon(Icons.folder,size: 50,): Image.memory(
                                      base64Decode(values
                                          .imageBase
                                          .toString()),
                                      fit: BoxFit.cover,
                                      gaplessPlayback: true,
                                    ),
                                  )
                                      : ClipRRect(
                                    borderRadius: BorderRadius.circular(35),
                                    child: Container(
                                      color: MyColors.grey_70,
                                    ),
                                  )*/Image.asset('assets/images/icon_folder.png', fit: BoxFit.fill,)
                              ),
                              Icon(
                                Icons.chevron_right_rounded,
                                size: 20,
                              ),
                            ],
                          ),
                          /*SizedBox(
                            width: 25,
                          ),*/
                          Expanded(
                              flex: 1,
                              child: Padding(padding:EdgeInsets.only(left: 15),child:Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    values.name!,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: MyStyles.customTextStyle(
                                        fontSize: 15,
                                        color: MyColors.black,
                                        fontWeight: FontWeight.w500),
                                    textAlign: TextAlign.left,
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    '${values.productItemCount} Items',
                                    style: MyStyles.customTextStyle(
                                        fontSize: 15,
                                        color: MyColors.grey_70,
                                        fontWeight: FontWeight.w300),
                                    textAlign: TextAlign.left,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ],
                              ))
                          ),
                        ],
                      ))),
            )));
  }
  Widget _buildProductItemGrid(BuildContext context, ProductCategory values, int index) {
    return Padding(
        padding: EdgeInsets.only(left: 0, right: 10),
        child: MySlidableWidgets.slidable(
          // The end action pane is the one at the right or the bottom side.
            endActionPane: ActionPane(
              motion: BehindMotion(),
              extentRatio: 0.45,
              children: <Widget>[
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    // editProductCategory2(items[index],context);
                    // showAddProductCategoryBottomSheet(values);
                    onEditProductItem(items[index], index, context);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 145,
                    color: MyColors.grey_9d_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.recycling_outlined,
                          size: 20,
                          color: MyColors.white,
                        ),
                        // Text(
                        //   'Edit',
                        //   style: MyStyles.customTextStyle(
                        //     fontSize: 11,
                        //     color: MyColors.white,
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) async {
                    return await showCustomCallbackAlertDialog(
                        context: context,
                        positiveText: 'Delete',
                        msg:
                        'Are you sure you wish to delete all project related data?',
                        positiveClick: () {
                          onDeleteAllDataAndProductItem(index);
                        },
                        negativeClick: () {
                          Navigator.of(context).pop(false);
                        });
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginR: 2,
                    height: 145,
                    topRightCornerRadius: 10,
                    bottomRightCornerRadius: 10,
                    color: MyColors.red_ff_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.close_rounded,
                          size: 20,
                          color: MyColors.white,
                        ),
                        // Text(
                        //   'Delete',
                        //   style: MyStyles.customTextStyle(
                        //     fontSize: 11,
                        //     color: MyColors.white,
                        //   ),
                        // )
                      ],
                    ),
                  ),
                ),
              ],
            ),

            // The child of the Slidable is what the user sees when the
            // component is not dragged.
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 0),
              child: ListTile(
                  hoverColor: MyColors.transparent,
                  contentPadding: EdgeInsets.all(0),
                  onLongPress: (){
                    isLongPressEnabled=true;
                    toggleSelection(values);
                  },
                  onTap: () {
                    if(isLongPressEnabled)
                      toggleSelection(values);
                    else
                      onProductItemClick(values);
                  },
                  title: MyWidgets.shadowContainer(
                      height: 145,
                      paddingL: 15,
                      paddingT: 10,
                      //paddingR: 15,
                      //paddingB: 15,
                      spreadRadius: 2,
                      cornerRadius: 10,
                      shadowColor: MyColors.white_f1_bg,
                      color: values.isSelected==1?MyColors.white_chinese_e0_bg:MyColors.white,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                  width: 56.0,
                                  height: 56.0,
                                  child: values.imageBase != null &&
                                      values.imageBase
                                          .toString()
                                          .isNotEmpty
                                  //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                                      ? ClipRRect(
                                    borderRadius: BorderRadius.circular(35),
                                    child: Image.memory(
                                      base64Decode(values
                                          .imageBase
                                          .toString()),
                                      fit: BoxFit.cover,
                                      gaplessPlayback: true,
                                    ),
                                  )
                                      : ClipRRect(
                                    borderRadius: BorderRadius.circular(35),
                                    child: Container(
                                      color: MyColors.grey_70,
                                    ),
                                  )
                              ),
                              Icon(
                                Icons.chevron_right_rounded,
                                size: 20,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          Expanded(
                              flex: 8,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Text(
                                    values.name!,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: MyStyles.customTextStyle(
                                        fontSize: 15,
                                        color: MyColors.black,
                                        fontWeight: FontWeight.w500),
                                    textAlign: TextAlign.left,
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  MyText.priceCurrencyText(context,
                                      price:
                                      '${double.parse((values.cost!).toStringAsFixed(2))}',
                                      fontSize: 15,
                                      fontWeight:
                                      FontWeight.normal),
                                  Row(mainAxisSize: MainAxisSize.max,children: [Expanded(

                                    flex: 5,
                                    child:Container()),
                                    Expanded(

                                      flex: 7,
                                      child:MyWidgets.container(bottomRightCornerRadius:10,height:34,color: MyColors.white_chinese_e0_bg,
                                      child:Align(
                                        alignment: Alignment.center,
                                        child: MyText.priceCurrencyText(
                                            context,
                                            price:
                                            '${values.sellingPrice!}',
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold),

                                      )))],)
                                ],
                              )
                          ),
                        ],
                      )
                  )
              ),
            )
        )
    );
  }


  ///On Item Click
  onItemClick(ProductCategory values) {
    print("Clicked position is ${values.name}");
    Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    ProductItemListScreenState(productCategory: values)))
        .then((value) {
      value == true ? _getData() : {};
    });
  }

  /// Delete Click and delete item
  /*onDelete( int index) async{
    var id = items[index].productCategoryId;
    // 1st delete all product category related data from relation table on product category delete
   await productItemRelationOperations.deleteRelationByProductId(id!).then((value)async {
      // 2nd delete all product category related data from worker relation table on product category delete
      await workerRelationOperations.deleteWorkerRelationByProductId(id).then((value)async {
        // 3rd delete all product category related data from cart relation table on product category delete
       await cartRelationOperations.deleteCartRelationItemByProductId(id).then((value)async {
        // 4th delete all product category related data from orders table on product category delete
       await orderOperations.deleteOrderItemByProductId(id).then((value)async {
         // 5th delete all product category related data from storage table on product category delete
         await storageOperations.deleteStorageItemByProductId(id).then((value)async {
        //6th delete all product items related with product category on product category delete
       await productItemOperations.deleteProductItemByProductCategoryId(id).then((value)async {
         // 7th then delete product category data from project product category relation
            // 8th then delete product category
               await productCategoryOperations.deleteProductCategory(id).then((value)async {
                  setState(() {
                    items.removeAt(index);});
                  */ /*items.removeAt(index);
                  if(items.isEmpty){
                    setState(() {

                    });
                  }*/ /*
                });});});});});});});
  }*/

  onDeleteAllDataAndProductCategory(int index) async {
    var id = items[index].id;
    await orderOperations.isOrderNewExistByProductId(id).then((bool) async {
      if (bool) {
        Navigator.of(context).pop(false);
        showCustomAlertDialog(context,
            'In order proceed, either deliver or cancel the order related to the current project.');
      } else {
        await productCategoryOperations
            .deleteAllDataAndProductCategory(id)
            .then((value) async {
          print('onSuccess');
          setState(() {
            items.removeAt(index);
            Navigator.of(context).pop(true);
          });
        }).onError((error, stackTrace) {
          Navigator.of(context).pop(false);
          print('onDeleteError  ${error}  ${stackTrace}');
        });
      }
    });
  }
  onDeleteAllDataAndProductItem(int index) async {
    var id = items[index].id;
    await orderOperations.isOrderNewExistByProductItemId(id).then((bool) async {
      if (bool) {
        Navigator.of(context).pop(false);
        showCustomAlertDialog(context,
            'In order proceed, either deliver or cancel the order related to the current project.');
      } else {
        await productItemOperations
            .deleteAllDataAndProductItem(id)
            .then((value) async {
          print('onSuccessError  ');
          //isProductItemAddedOrRemoved = true;
          setState(() {
            items.removeAt(index);
            Navigator.of(context).pop(true);
          });
        }).onError((error, stackTrace) {
          Navigator.of(context).pop(false);
          print('onDeleteError  ${error}  ${stackTrace}');
        });
      }
    });
  }
  onProductItemClick(ProductCategory productCategory) async {
    if (productCategory.isInventoryItemUpdated == 1) {

        productCategory.isInventoryItemUpdated = 0;

var productItem=ProductItem();
productItem.id=productCategory.id;
   productItem.isInventoryItemUpdated=productCategory.isInventoryItemUpdated;
      await productItemOperations
          .updateProductItemIsInventoryItemStatusByProductItemId(productItem)
          .then((value) => {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductItemDetailsScreenState(
                  productCategory: null,
                  productItemId: productCategory.id,
                ))).then((value) => value == true ? _getData() : {})
      });
      return;
    } else
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ProductItemDetailsScreenState(
                productCategory: null,
                productItemId: productCategory.id,
              ))).then((value) => value == true ? _getData() : {});
  }
  ///edit User
  editProductCategory(int id, BuildContext context) async {
    if (teNameController.text.isNotEmpty) {
      var productCategory = ProductCategory();
      productCategory.id = id;
      productCategory.name = teNameController.text;

      await productCategoryOperations
          .updateRawProductCategory(productCategory)
          .then((value) {
        teNameController.text = "";

        Navigator.of(context).pop();
        showtoast("Data Saved successfully");

        _getData();
      });
    } else {
      showtoast("Please fill all the fields");
    }
  }

  editProductCategory2(ProductCategory values, BuildContext context) async {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => AddProductCategoryScreenState(
                  productCategory: values,
                ))).then((value) => value == true ? _getData() : {});
  }
  onEditProductItem(ProductCategory productCategory, int index, BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => AddProductNewScreenState(
              productCategory: null,
              productItemId: productCategory.id,
            ))).then((value) =>
    value == true ? { _getData()} : {});
  }
  showAddProductCategoryBottomSheet(ProductCategory? productCategory) =>
      project == null
          ? showCustomAlertDialog(
              context,
              'Please create a project ',
            )
          : MyBottomSheet().showBottomSheet(context, callback: (value) {
              if (value != null && value == true) {
                _getData();
              }
            },
              child: AddProductCategoryScreenState(
                productCategory: productCategory,
              ));

  var isLongPressEnabled = false;
  var productItemIdArray = <int>[];
  void toggleSelection(ProductCategory values) {
    setState(() {
      if (values.isSelected==0) {
        values.isSelected=1;
        productItemIdArray.add(values.id!);
        print('productItemIdArray $productItemIdArray');
      } else {
        values.isSelected=0;
        productItemIdArray.remove(values.id!);
        print('productItemIdArray $productItemIdArray');
        final isAllSelectionClear = items.map((e) => e.isSelected).every((element) => element==0);
        if(isAllSelectionClear){
          productItemIdArray.clear();
          isLongPressEnabled=!isAllSelectionClear;
        }

        print('toggleTest $isAllSelectionClear');

      }
    });
  }



  showBottomSheet(StatefulWidget statefulWidget,{double maxHeight=0.60})=>
      this.project ==null
          ? showCustomAlertDialog(context, 'Please create a project ')
          : MyBottomSheet().showBottomSheet(context,
          maxHeight:maxHeight, callback: (value) {
            if (value != null && value == true) {
              isLongPressEnabled=false;
              productItemIdArray.clear();
              _getData();
            }
          }, child: statefulWidget);
}
