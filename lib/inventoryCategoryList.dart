import 'dart:convert';
import 'dart:io';
import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/addInventoryCategory.dart';
import 'package:ecommerce/addInventoryItemNew.dart';
import 'package:ecommerce/inventotyItemHistoryNew2.dart';
import 'package:ecommerce/inventotyItemList.dart';
import 'package:ecommerce/switchInventoryCategory.dart';
import 'package:ecommerce/model/Inventory.dart';
import 'package:ecommerce/inventoryReportPdfView.dart';
import 'package:ecommerce/switchMeasure.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/MyCustomSlidableAction.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'addInventoryItem.dart';
import 'database/DatabaseHelper.dart';
import 'database/history_operations.dart';
import 'database/inventory_item_operations.dart';
import 'database/inventory_operations.dart';
import 'database/relation_operations.dart';
import 'editInventoryItem.dart';
import 'inventoryItemDetails.dart';
import 'inventotyItemHistoryNew.dart';
import 'inventoryUnitMeasures.dart';
import 'model/Project.dart';

class InventoryCategoryListScreenState extends StatefulWidget {
  const InventoryCategoryListScreenState({Key? key}) : super(key: key);

  @override
  _InventoryCategoryListScreenState createState() =>
      _InventoryCategoryListScreenState();
}

class _InventoryCategoryListScreenState
    extends State<InventoryCategoryListScreenState> {
  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    /*MyPref.getProject().then((value) =>
    value==null
        ? {
      setState(() { isLoading=false;}),
      showAlertDialog(context, 'Please create a project ')

    } :
    Future.delayed( Duration(milliseconds: 1000), () {
      _getData();

    }));*/
    Future.delayed(Duration(milliseconds: 1000), () {
      _getData();
    });
    super.initState();
    print("INIT");
    // _scrollController = new ScrollController();
    //_scrollController.addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

        backgroundColor: MyColors.white_f1_bg,
        appBar: MyWidgets.appBar(
          titleSpacing: 10,
          elevation: 0,
          title: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  flex: 7,
                  child: ElevatedButton.icon(
                    icon: const Icon(
                      Icons.create_new_folder_rounded,
                      color: MyColors.black,
                      size: 30,
                    ),
                    onPressed: () async {
                      showAddInventoryCategoryBottomSheet(null);
                    },
                    label: MyWidgets.textView(
                      text: "Inventory Category",
                      style: MyStyles.customTextStyle(
                        fontSize: 13,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      elevation: 0,
                      primary: MyColors.white,
                      fixedSize: const Size(0, 44),
                      shape: MyDecorations.roundedRectangleBorder(10),
                    ),
                  )),
              SizedBox(
                width: 15,
              ),
              Expanded(
                  flex: 3,
                  child: MyWidgets.shadowContainer(
                      height: 44,
                      blurRadius: 0,
                      cornerRadius: 10,
                      spreadRadius: 0,
                      shadowColor: MyColors.white_f1_bg,
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  child: Center(
                                    child: Icon(
                                      Icons.list,
                                      color: MyColors.black,
                                    ),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      this.isListView = true;
                                    });
                                  },
                                )),
                            Container(
                                width: 0,
                                height: 30,
                                child: VerticalDivider(
                                    color: MyColors.grey_9d_bg)),
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  child: Center(
                                    child: Icon(
                                      Icons.grid_on_rounded,
                                      color: MyColors.black,
                                    ),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      this.isListView = false;
                                    });
                                  },
                                ))
                          ])))
            ],
          ),
        ),
        bottomNavigationBar: MyWidgets.addNewButton(
          onArrowPressed: (){
            goto(context, InventoryReportPdfViewScreenState());
          },
            text: isLongPressEnabled ? 'Move To' : 'Add New',
            onAddOrMovePressed: () {
              isLongPressEnabled
                  ? showInventoryCategoryAndItemOperationsBottomSheet(
                      SwitchInventoryCategoryScreenState(
                          isFromInventoryItemList: false,
                          inventoryIdArray: [],
                          inventoryItemIdArray: inventoryItemIdArray),
                      maxHeight: 0.60)
                  : showInventoryCategoryAndItemOperationsBottomSheet(
                      AddInventoryItemScreenStateNew(
                      inventory: null,
                    ));
            }),
        body:
            /*ElevatedButton(
          onPressed: () {
            // Navigate back to first route when tapped.
            Navigator.pop(context);
          },
          child: Text('Go back!'),

        )*/
            //getAllInventory());
            createListView(context));
  }

  var items = <Inventory>[];

  // final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  final _listKey = GlobalKey<AnimatedListState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool insertItem = false;
  var _scrollController = ScrollController();
  final teNameController = TextEditingController();
  var inventoryOperations = InventoryOperations();
  var inventoryRelationOperations = RelationOperations();

  //var inventoryItemOperations = InventoryItemOperations();
  var relationOperations = RelationOperations();

  //var historyItemOperations = HistoryOperations();
  Project? project;

  /// Get all users data
  /* getAllInventory() {
    return FutureBuilder(
        future: _getData(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return createListView(context, snapshot);
        });
  }*/

  ///Fetch data from database
  Future<List<Inventory>> _getData() async {
    await MyPref.getProject().then((project) async {
      this.project = project;
      if (project != null) {
        await inventoryOperations
            .inventoriesByProjectIdWithNullInventoryItemIdAndInventoryItemCount(
                project.id)
            .then((value) {
          items = value;
        });
      } else
        showCustomAlertDialog(context, 'Please create a project ');
      setState(() {
        isLoading = false;
      });
    });

    return items;
  }

  bool isLoading = true;
  bool isListView = true;

  ///create List View with
  Widget createListView(BuildContext context) {
    if (items.isNotEmpty) {
      return Column(children: <Widget>[
        Expanded(
            flex: 1,
            child: FadeIn(
              child: this.isListView ? showListView() : showGridView(),
            )), /*Expanded(flex: 2,child: Padding(
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
          child:FadeInUp(delay: Duration(milliseconds: 400),child: Material(
            elevation: 5.0,
            borderRadius: BorderRadius.circular(10.0),
            color: (Colors.teal[900])!,
            child: MaterialButton(
              minWidth: MediaQuery.of(context).size.width,
              onPressed: () {
                goto(context, PdfViewScreenState());
              },
              child: Text(
                'Report',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ))))*/
      ]);
    } else
      return isLoading
          ? MyWidgets.buildCircularProgressIndicator()
          : Center(
        child: Text(
          'Inventory Category is empty.',
          style: TextStyle(
            fontSize: 17.0,
            color: Colors.grey,
          ),
        ),
      );
  }

  Widget showListView() {
    return
      SafeArea(bottom: true,child:  ListView.builder(
            physics:
                AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
            key: _listKey,
            controller: _scrollController,
            shrinkWrap: true,
            itemCount: items.length,
            itemBuilder: (BuildContext context, int index) {
              if (items[index].hasCategory != null && items[index].hasCategory == -1)
                return _buildInventoryCategoryList(
                    context, items[index], index);
              else
                return _buildInventoryItemList(context, items[index], index);


            }));
  }

  Widget showGridView() {
    return SafeArea(bottom: true,child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            /*crossAxisCount: 2,
          childAspectRatio: 1.2,
          crossAxisSpacing: 00,
          mainAxisSpacing: 8,*/
            maxCrossAxisExtent: 200,
            childAspectRatio: 1.2,
            crossAxisSpacing: 0,
            mainAxisSpacing: 6),
        physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
        //key: _listKey,
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) {
          if (items[index].hasCategory != null &&
              items[index].hasCategory == -1) {
            return _buildICategoryItemsGrid(context, items[index], index);
          } else {
            return _buildInventoryItemsGrid(context, items[index], index);
          }
        }));
  }

  ///Construct cell for List View
  Widget _buildInventoryCategoryList(
      BuildContext context, Inventory values, int index) {
    return Padding(
        padding: EdgeInsets.only(left: 0, right: 10),
        child: MySlidableWidgets.slidable(
          // The end action pane is the one at the right or the bottom side.
          endActionPane: ActionPane(
            motion: BehindMotion(),
            extentRatio: 0.25,
            children: <Widget>[
              MyCustomSlidableAction(
                flex: 1,
                onPressed: (BuildContext slidableContext) {
                  values.hasCategory == -1
                      ? showAddInventoryCategoryBottomSheet(values)
                      : showInventoryCategoryAndItemOperationsBottomSheet(
                          EditInventoryItemScreenState(
                            inventory: null,
                            inventoryItemId: values.id,
                          ),
                        );
                },
                backgroundColor: MyColors.transparent,
                foregroundColor: Colors.transparent,
                child: Container(
                    margin: EdgeInsets.only(left: 1),
                    height: 94,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.recycling_outlined,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Edit',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                      color: MyColors.grey_9d_bg,
                      boxShadow: [
                        BoxShadow(
                          color: MyColors.grey_9d_bg,
                          blurRadius: 1.0,
                          offset: Offset(0.7, 0),
                        ),
                      ],
                    )),
              ),
              MyCustomSlidableAction(
                flex: 1,
                onPressed: (BuildContext slidableContext) async {
                  return await showCustomCallbackAlertDialog(
                      context: context,
                      positiveText: 'Delete',
                      msg:
                          'Are you sure you wish to delete all project related data?',
                      positiveClick: () {
                        onDeleteAllDataAndInventory(index);
                      },
                      negativeClick: () {
                        Navigator.of(context).pop(false);
                      });
                },
                backgroundColor: MyColors.transparent,
                foregroundColor: Colors.transparent,
                child: Container(
                    margin: EdgeInsets.only(right: 2),
                    height: 94,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.close_rounded,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Delete',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        )
                      ],
                    ),
                    decoration: BoxDecoration(
                      color: MyColors.red_ff_bg,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(0),
                          topRight: Radius.circular(10),
                          bottomLeft: Radius.circular(0),
                          bottomRight: Radius.circular(10)),
                      boxShadow: [
                        BoxShadow(
                          color: MyColors.grey_9d_bg,
                          blurRadius: 1.0,
                          offset: Offset(0.7, 0),
                        ),
                      ],
                    )),
              ),
            ],
          ),

          // The child of the Slidable is what the user sees when the
          // component is not dragged.
          child: Padding(
              padding: EdgeInsets.only(left: 10, right: 0),
              child:
              ListTile(
                  hoverColor: MyColors.transparent,
                  contentPadding: EdgeInsets.all(0),
                  onTap: () {
                    if (!isLongPressEnabled) {
                      onItemClick(values);
                    }
                  },
                  /*onLongPress: (){
                   if(values.hasCategory!>-1){
                    isLongPressEnabled=true;
                    toggleSelection(values);}
                  },*/
                  title: MyWidgets.shadowContainer(
                      height: 96,
                      // paddingL: 15,
                      //paddingT: 15,
                      //paddingR: 15,
                      //paddingB: 15,
                      spreadRadius: 2,
                      cornerRadius: 10,
                      shadowColor: MyColors.white_f1_bg,
                      color: values.isSelected == 1
                          ? MyColors.white_chinese_e0_bg
                          : MyColors.white,
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(

                                width: 85.0,
                                height: 66.0,
                                child: /*values.imageBase != null &&
                                        values.imageBase.toString().isNotEmpty
                                    //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                                    ? ClipRRect(
                                        borderRadius: BorderRadius.circular(35),
                                        child: values.hasCategory == -1
                                            ? Icon(
                                                Icons.folder,
                                                size: 30,
                                              )
                                            : Image.memory(
                                                base64Decode(values.imageBase
                                                    .toString()),
                                                fit: BoxFit.cover,
                                                gaplessPlayback: true,
                                              ),
                                      )
                                    : ClipRRect(
                                        borderRadius: BorderRadius.circular(35),
                                        child: Container(
                                          color: MyColors.grey_70,
                                        ),
                                      )*/
                                Image.asset('assets/images/icon_folder.png',gaplessPlayback: true,
                                    fit: BoxFit.fill,)
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                                flex: 8,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text(
                                      values.name!,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: MyStyles.customTextStyle(
                                          fontSize: 15,
                                          color: MyColors.black,
                                          fontWeight: FontWeight.w500),
                                      textAlign: TextAlign.left,
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      '${values.inventoryItemCount} Items',
                                      style: MyStyles.customTextStyle(
                                          fontSize: 15,
                                          color: MyColors.grey_70,
                                          fontWeight: FontWeight.w300),
                                      textAlign: TextAlign.left,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ],
                                )),
                            Icon(
                              Icons.chevron_right_rounded,
                              size: 25,
                              color: MyColors.black,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                          ]))),
        )));
  }

  Widget _buildInventoryItemList(
      BuildContext context, Inventory values, int index) {
    return Padding(
        padding: EdgeInsets.only(left: 0, right: 10),
        child: MySlidableWidgets.slidable(

            // The end action pane is the one at the right or the bottom side.
            endActionPane: ActionPane(
              motion: BehindMotion(),
              extentRatio: 0.50,
              children: <Widget>[
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    showInventoryCategoryAndItemOperationsBottomSheet(
                      EditInventoryItemScreenState(
                        inventory: null,
                        inventoryItemId: values.id,
                      ),
                    );
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 94,
                    color: MyColors.grey_9d_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.recycling_outlined,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Edit',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    showInventoryCategoryAndItemOperationsBottomSheet(
                        SwitchMeasureScreenState(
                      inventoryItemId: values.id,
                    ));
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 94,
                    color: MyColors.yellow_ff,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.av_timer_rounded,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Switch',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    onHistoryClick(values);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 94,
                    color: MyColors.grey_9d_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.history,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'History',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) async {
                    return await showCustomCallbackAlertDialog(
                        context: context,
                        positiveText: 'Delete',
                        msg:
                            'Are you sure you wish to delete all project related data?',
                        positiveClick: () {
                          onDeleteAllDataAndInventoryItem(index);
                        },
                        negativeClick: () {
                          Navigator.of(context).pop(false);
                        });
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                      marginR: 2,
                      height: 94,
                      color: MyColors.red_ff_bg,
                      topRightCornerRadius: 10,
                      bottomRightCornerRadius: 10,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.close_rounded,
                            size: 20,
                            color: MyColors.white,
                          ),
                          Text(
                            'Delete',
                            style: MyStyles.customTextStyle(
                              fontSize: 11,
                              color: MyColors.white,
                            ),
                          )
                        ],
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: MyColors.grey_9d_bg,
                          blurRadius: 1.0,
                          offset: Offset(0.7, 0),
                        ),
                      ]),
                ),
              ],
            ),

            // The child of the Slidable is what the user sees when the
            // component is not dragged.
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 0),
              child: ListTile(
                  hoverColor: MyColors.transparent,
                  contentPadding: EdgeInsets.all(0),
                  onLongPress: () {
                    isLongPressEnabled = true;
                    toggleSelection(values);
                  },
                  onTap: () {
                    if (isLongPressEnabled)
                      toggleSelection(values);
                    else
                      showInventoryCategoryAndItemOperationsBottomSheet(
                          InventoryItemDetailsScreenState(
                        inventoryItemId: values.id,
                      ));
                  },
                  title: MyWidgets.shadowContainer(
                      height: 95,
                      paddingL: 10,
                      // spreadRadius: 2,
                      cornerRadius: 10,
                      shadowColor: MyColors.white_f1_bg,
                      color: values.isSelected == 1
                          ? MyColors.white_chinese_e0_bg
                          : MyColors.white,
                      child: Row(
                        children: [
                          Container(
                              width: 66.0,
                              height: 66.0,
                              child: values.imageBase != null &&
                                      values.imageBase.toString().isNotEmpty
                                  //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                                  ? ClipRRect(
                                      borderRadius: BorderRadius.circular(8),
                                      child: Image.memory(
                                        base64Decode(
                                            values.imageBase.toString()),
                                        fit: BoxFit.cover,
                                        gaplessPlayback: true,
                                      ),
                                    )
                                  : ClipRRect(
                                      borderRadius: BorderRadius.circular(8),
                                      child: Container(
                                        color: MyColors.grey_70,
                                      ),
                                    )),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                              flex: 9,
                              child: Container(
                                  margin: EdgeInsets.only(top: 5, bottom: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Spacer(),
                                      Text(
                                        values.name!,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: MyStyles.customTextStyle(
                                            fontSize: 15,
                                            color: MyColors.black,
                                            fontWeight: FontWeight.w500),
                                        textAlign: TextAlign.left,
                                      ),
                                      Spacer(),
                                      MyText.priceCurrencyText(context,
                                          price:
                                              'Cost: ${values.costPerItemInTypeNew}',
                                          fontSize: 15,
                                          fontWeight: FontWeight.normal,
                                          fontColor: MyColors.grey_9d_bg),
                                      Spacer(),
                                      values.inventoryItemType == 0
                                          ? MyText.priceCurrencyText(context,
                                              price:
                                                  'Stock: ${values.inventoryItemStock!} Items',
                                              exponent: '(Box)',
                                              fontSize: 15,
                                              fontWeight: FontWeight.w400)
                                          : Text(
                                              'Stock: ${values.inventoryItemStock!} ${InventoryUnitMeasure.inventoryUnitMeasureArray[values.inventoryItemType!]}',
                                              style: MyStyles.customTextStyle(
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w400),
                                              textAlign: TextAlign.left,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                      Spacer(),
                                    ],
                                  ))),
                          Container(
                            width: 45,
                            decoration: BoxDecoration(
                              color: MyColors.white_chinese_e0_bg,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10),
                                  bottomRight: Radius.circular(10)),
                            ),
                            child: Center(
                              child: Icon(
                                Icons.chevron_right_rounded,
                                size: 25,
                                color: MyColors.black,
                              ),
                            ),
                          ),
                        ],
                      ))),
            )));

    // return InkWell(
    //     onTap: () async {
    //       onSelectInventoryItem(values);
    //     },
    //     child: Container(
    //         margin: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
    //         decoration: BoxDecoration(
    //             border: Border.all(
    //               color: values.isCostUpdated == 1
    //                   ? (Colors.redAccent)
    //                   : (Colors.teal[900])!,
    //             ),
    //             borderRadius: BorderRadius.all(Radius.circular(15))),
    //         child: Column(
    //           mainAxisSize: MainAxisSize.max,
    //           children: <Widget>[
    //             Row(
    //                 mainAxisSize: MainAxisSize.max,
    //                 crossAxisAlignment: CrossAxisAlignment.start,
    //                 children: <Widget>[
    //                   //image
    //                   Container(
    //                     margin: EdgeInsets.fromLTRB(5, 5, 10, 5),
    //                     child: values.imageBase != null &&
    //                             values.imageBase.toString().isNotEmpty
    //                         ? ClipRRect(
    //                             borderRadius: BorderRadius.circular(10),
    //                             child: Image.memory(
    //                               base64Decode(values.imageBase.toString()),
    //                               width: 80,
    //                               height: 80,
    //                               fit: BoxFit.fitHeight,
    //                               gaplessPlayback: true,
    //                             ),
    //                           )
    //                         : Container(
    //                             decoration: BoxDecoration(
    //                                 color: Colors.grey[200],
    //                                 borderRadius: BorderRadius.circular(10)),
    //                             width: 80,
    //                             height: 80,
    //                           ),
    //                   ),
    //                   //name and count
    //
    //                   Column(
    //                     crossAxisAlignment: CrossAxisAlignment.start,
    //                     mainAxisSize: MainAxisSize.min,
    //                     children: <Widget>[
    //                       Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0)),
    //                       Container(
    //                         width: 200,
    //                         child: RichText(
    //                           maxLines: 1,
    //                           overflow: TextOverflow.ellipsis,
    //                           strutStyle: StrutStyle(fontSize: 12.0),
    //                           text: TextSpan(
    //                             style: TextStyle(
    //                                 fontWeight: FontWeight.w500,
    //                                 fontSize: 18,
    //                                 color: Colors.black),
    //                             text: values.name!,
    //                           ),
    //                         ),
    //                       ),
    //                       Padding(
    //                           padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),
    //                       Text(
    //                         //'Cost: ${double.parse(((values.costPerTypeNew! / values.piecesInType!)).toStringAsFixed(2))} QAR / Item',
    //                         values.itemType == 0
    //                             ? 'Cost: ${values.costPerItemInTypeNew} QAR / Item'
    //                             : 'Cost: ${values.costPerItemInTypeNew} QAR / ${InventoryUnitMeasure.inventoryUnitMeasureArray[values.itemType!]}',
    //                         style: TextStyle(
    //                           fontWeight: FontWeight.w300,
    //                           fontSize: 15.0,
    //                           color: Colors.black,
    //                         ),
    //                         textAlign: TextAlign.left,
    //                         maxLines: 2,
    //                       ),
    //                     ],
    //                   ),
    //                   Spacer(),
    //                 ]),
    //             Row(
    //               mainAxisAlignment: MainAxisAlignment.center,
    //               children: [
    //                 Flexible(
    //                     flex: 1,
    //                     fit: FlexFit.tight,
    //                     child: Stack(
    //                         alignment: Alignment.center,
    //                         children: <Widget>[
    //                           Container(
    //                             height: 35,
    //                             decoration: BoxDecoration(
    //                               borderRadius: BorderRadius.only(
    //                                   bottomLeft: Radius.circular(14)),
    //                               color: Colors.grey,
    //                             ), //BoxDecoration
    //                           ),
    //                           Text(
    //                             "Stock: ${values.stock} Items",
    //                             style: TextStyle(
    //                               fontWeight: FontWeight.w500,
    //                               fontSize: 15.0,
    //                               color: Colors.white,
    //                             ),
    //                           )
    //                         ]) //Container
    //                     ),
    //                 Flexible(
    //                     flex: 1,
    //                     fit: FlexFit.tight,
    //                     child: InkWell(
    //                       onTap: () {
    //                         onHistoryClick(values);
    //                       },
    //                       child: Stack(
    //                         alignment: Alignment.center,
    //                         children: <Widget>[
    //                           Container(
    //                             height: 35,
    //                             decoration: BoxDecoration(
    //                               borderRadius: BorderRadius.only(
    //                                   bottomRight: Radius.circular(14)),
    //                               color: (Colors.teal[900])!,
    //                             ), //BoxDecoration
    //                           ),
    //                           Text(
    //                             "History",
    //                             style: TextStyle(
    //                               fontWeight: FontWeight.w500,
    //                               fontSize: 15.0,
    //                               color: Colors.white,
    //                             ),
    //                           )
    //                         ],
    //                       ),
    //                     ) //Container
    //                     )
    //               ],
    //             ),
    //           ],
    //         )));
  }

  Widget _buildICategoryItemsGrid(
      BuildContext context, Inventory values, int index) {
    return Padding(
        padding: EdgeInsets.only(left: 0, right: 10, bottom: 0),
        child: MySlidableWidgets.slidable(
            // The end action pane is the one at the right or the bottom side.
            endActionPane: ActionPane(
              motion: BehindMotion(),
              extentRatio: 0.45,
              children: <Widget>[
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    values.hasCategory == -1
                        ? showAddInventoryCategoryBottomSheet(values)
                        : showInventoryCategoryAndItemOperationsBottomSheet(
                            EditInventoryItemScreenState(
                            inventory: null,
                            inventoryItemId: values.id,
                          ));
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: Container(
                      margin: EdgeInsets.only(left: 1),
                      height: 145,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.recycling_outlined,
                            size: 20,
                            color: MyColors.white,
                          ),
                          // Text(
                          //   'Edit',
                          //   style: MyStyles.customTextStyle(
                          //     fontSize: 11,
                          //     color: MyColors.white,
                          //   ),
                          // ),
                        ],
                      ),
                      decoration: BoxDecoration(
                        color: MyColors.grey_9d_bg,
                        boxShadow: [
                          BoxShadow(
                            color: MyColors.grey_9d_bg,
                            blurRadius: 1.0,
                            offset: Offset(0.7, 0),
                          ),
                        ],
                      )),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) async {
                    return await showCustomCallbackAlertDialog(
                        context: context,
                        positiveText: 'Delete',
                        msg:
                            'Are you sure you wish to delete all project related data?',
                        positiveClick: () {
                          onDeleteAllDataAndInventory(index);
                        },
                        negativeClick: () {
                          Navigator.of(context).pop(false);
                        });
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: Container(
                      margin: EdgeInsets.only(right: 2),
                      height: 145,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.close_rounded,
                            size: 20,
                            color: MyColors.white,
                          ),
                          // Text(
                          //   'Delete',
                          //   style: MyStyles.customTextStyle(
                          //     fontSize: 11,
                          //     color: MyColors.white,
                          //   ),
                          // )
                        ],
                      ),
                      decoration: BoxDecoration(
                        color: MyColors.red_ff_bg,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(0),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(0),
                            bottomRight: Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                            color: MyColors.grey_9d_bg,
                            blurRadius: 1.0,
                            offset: Offset(0.7, 0),
                          ),
                        ],
                      )),
                ),
              ],
            ),

            // The child of the Slidable is what the user sees when the
            // component is not dragged.
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 0),
              child: ListTile(
                  hoverColor: MyColors.transparent,
                  contentPadding: EdgeInsets.all(0),
                  onTap: () {
                    if (!isLongPressEnabled) {
                      onItemClick(values);
                    }
                  },
                  //   isLongPressEnabled?toggleSelection(values):
                  //   onItemClick(values);},
                  title: MyWidgets.shadowContainer(
                      height: 145,
                      paddingL: 0,
                      paddingT: 10,
                      paddingR: 15,
                      paddingB: 5,
                      spreadRadius: 2,
                      cornerRadius: 10,
                      shadowColor: MyColors.white_f1_bg,
                      color: MyColors.white,
                      child:Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  width: 100.0,
                                  height: 70.0,
                                  child:
                                      //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                                       Image.asset('assets/images/icon_folder.png', fit: BoxFit.fill,)
                                     ),
                              Icon(
                                Icons.chevron_right_rounded,
                                size: 20,
                                color: MyColors.black,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 0,
                          ),
                          Expanded(
                              flex: 8,
                              child:Padding(padding:EdgeInsets.only(left: 15),child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    values.name!,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: MyStyles.customTextStyle(
                                        fontSize: 15,
                                        color: MyColors.black,
                                        fontWeight: FontWeight.w600),
                                    textAlign: TextAlign.left,
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    '${values.inventoryItemCount} Items',
                                    style: MyStyles.customTextStyle(
                                        fontSize: 14,
                                        color: MyColors.grey_70,
                                        fontWeight: FontWeight.w300),
                                    textAlign: TextAlign.left,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ],
                              ))),
                        ],
                      ))),
            )));
  }

  Widget _buildInventoryItemsGrid(
      BuildContext context, Inventory values, int index) {
    return Padding(
        padding: EdgeInsets.only(left: 0, right: 10, bottom: 0),
        child: MySlidableWidgets.slidable(

            // The end action pane is the one at the right or the bottom side.
            endActionPane: ActionPane(
              motion: BehindMotion(),
              extentRatio: 0.80,
              children: <Widget>[
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    showInventoryCategoryAndItemOperationsBottomSheet(
                      EditInventoryItemScreenState(
                        inventory: null,
                        inventoryItemId: values.id,
                      ),
                    );
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 145,
                    color: MyColors.grey_9d_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.recycling_outlined,
                          size: 20,
                          color: MyColors.white,
                        ),
                        // Text(
                        //   'Edit',
                        //   style: MyStyles.customTextStyle(
                        //     fontSize: 11,
                        //     color: MyColors.white,
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    showInventoryCategoryAndItemOperationsBottomSheet(
                        SwitchMeasureScreenState(
                      inventoryItemId: values.id,
                    ));
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 145,
                    color: MyColors.yellow_ff,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.av_timer_rounded,
                          size: 20,
                          color: MyColors.white,
                        ),
                        // Text(
                        //   'Switch',
                        //   style: MyStyles.customTextStyle(
                        //     fontSize: 11,
                        //     color: MyColors.white,
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    onHistoryClick(values);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 145,
                    color: MyColors.grey_9d_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.history,
                          size: 20,
                          color: MyColors.white,
                        ),
                        // Text(
                        //   'History',
                        //   style: MyStyles.customTextStyle(
                        //     fontSize: 11,
                        //     color: MyColors.white,
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) async {
                    return await showCustomCallbackAlertDialog(
                        context: context,
                        positiveText: 'Delete',
                        msg:
                            'Are you sure you wish to delete all project related data?',
                        positiveClick: () {
                          onDeleteAllDataAndInventoryItem(index);
                        },
                        negativeClick: () {
                          Navigator.of(context).pop(false);
                        });
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                      marginR: 2,
                      height: 145,
                      color: MyColors.red_ff_bg,
                      topRightCornerRadius: 10,
                      bottomRightCornerRadius: 10,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.close_rounded,
                            size: 20,
                            color: MyColors.white,
                          ),
                          // Text(
                          //   'Delete',
                          //   style: MyStyles.customTextStyle(
                          //     fontSize: 11,
                          //     color: MyColors.white,
                          //   ),
                          // )
                        ],
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: MyColors.grey_9d_bg,
                          blurRadius: 1.0,
                          offset: Offset(0.7, 0),
                        ),
                      ]),
                ),
              ],
            ),

            // The child of the Slidable is what the user sees when the
            // component is not dragged.
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 0),
              child: ListTile(
                  hoverColor: MyColors.transparent,
                  contentPadding: EdgeInsets.all(0),
                  onLongPress: () {
                    isLongPressEnabled = true;
                    toggleSelection(values);
                  },
                  onTap: () {
                    if (isLongPressEnabled)
                      toggleSelection(values);
                    else
                      showInventoryCategoryAndItemOperationsBottomSheet(
                          InventoryItemDetailsScreenState(
                        inventoryItemId: values.id,
                      ));
                  },
                  title: MyWidgets.shadowContainer(
                      height: 145,
                      paddingL: 15,
                      //paddingT: 10,
                      paddingT: 0,
                      //paddingR: 15,
                      paddingR: 0,
                      paddingB: 5,
                      spreadRadius: 2,
                      cornerRadius: 10,
                      shadowColor: MyColors.white_f1_bg,
                      color: values.isSelected == 1
                          ? MyColors.white_chinese_e0_bg
                          : MyColors.white,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                             Padding(padding: EdgeInsets.only(top: 10),child: Container(
                                  width: 56.0,
                                  height: 56.0,
                                  child: values.imageBase != null &&
                                          values.imageBase.toString().isNotEmpty
                                      //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                                      ? ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: Image.memory(
                                            base64Decode(
                                                values.imageBase.toString()),
                                            fit: BoxFit.cover,
                                            gaplessPlayback: true,
                                          ),
                                        )
                                      : ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: Container(
                                            color: MyColors.grey_70,
                                          ),
                                        ))),

                              MyWidgets.container(
                                height: 40,
                                width: 40,
                                bottomLeftCornerRadius: 8,
                                topRightCornerRadius: 8,
                                color: MyColors.white_chinese_e0_bg,
                                child:Icon(
                                  Icons.chevron_right_rounded,
                                  size: 20,
                                  color: MyColors.black,
                                ),
                              )

                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Expanded(
                              flex: 8,
                              child: Padding(padding: EdgeInsets.only(right: 15),child:  Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    values.name!,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: MyStyles.customTextStyle(
                                        fontSize: 15,
                                        color: MyColors.black,
                                        fontWeight: FontWeight.w600),
                                    textAlign: TextAlign.left,
                                  ),
                                  SizedBox(
                                    height: 4,
                                  ),
                                  MyText.priceCurrencyText(context,
                                price:
                                'Cost: ${values.costPerItemInTypeNew}',
                                fontSize: 15,
                                fontWeight: FontWeight.normal,
                                fontColor: MyColors.grey_9d_bg),
                                  SizedBox(
                                    height: 3,
                                  ),
                                  values.inventoryItemType == 0
                                      ? MyText.priceCurrencyText(context,
                                      price:
                                      'Stock: ${values.inventoryItemStock!} Items',
                                      exponent: '(Box)',
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400)
                                      : Text(
                                    'Stock: ${values.inventoryItemStock!} ${InventoryUnitMeasure.inventoryUnitMeasureArray[values.inventoryItemType!]}',
                                    style: MyStyles.customTextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.w300),
                                    textAlign: TextAlign.left,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ],
                              ))),
                        ],
                      ))),
            )));
  }

  ///On Item Click
  onItemClick(Inventory values) {
    print("Clicked position is ${values.name}");
    values.hasCategory == -1
        ? Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        InventoryItemListScreenState(inventory: values)))
            .then((value) {
            value == true ? _getData() : {};
          })
        : showInventoryCategoryAndItemOperationsBottomSheet(
            InventoryItemDetailsScreenState(
            //inventory: widget.inventory,
            inventoryItemId: values.id,
          ));
  }

  /// Delete Click and delete item
  /*onDelete(int index)async {
    var id = items[index].id;

    await relationOperations.relationItemCountFromProductRelationByInventoryId(id!).then((productCount) async{
      if(productCount==0){
        await historyItemOperations.deleteHistoryItemByInventoryId(id).then((value)async {
        await inventoryItemOperations.deleteInventoryItemsByInventoryId(id).then((value)async {
           await inventoryOperations.deleteInventory(id).then((value)async {
          setState(() { items.removeAt(index);});

        });});
        });
        Navigator.of(context).pop(true);
      }else{
        Navigator.of(context).pop(false);
        context.showSnackBar("Can\''nt delete! Some inventories is being used by some products ");
      }

    });

  }*/

  /// Delete Click and delete item
  onDeleteAllDataAndInventory(int index) async {
    var id = items[index].id;

    await relationOperations
        .relationItemCountFromProductRelationByInventoryId(id!)
        .then((productCount) async {
      if (productCount == 0) {
        await inventoryOperations.deleteAllDataAndInventory(id).then((value) {
          setState(() {
            items.removeAt(index);
            Navigator.of(context).pop(true);
          });
        }).onError((error, stackTrace) {
          Navigator.of(context).pop(false);
          print('onDeleteError  ${error}  ${stackTrace}');
        });
      } else {
        Navigator.of(context).pop(false);
        context.showSnackBar(
            "Can\''nt delete! Some inventories is being used by some products ");
      }
    });
  }

  /// Delete Click and delete item
  onDeleteAllDataAndInventoryItem(int index) async {
    var id = items[index].id;

    await inventoryRelationOperations
        .relationItemCountFromProductRelationByInventoryItemId(id)
        .then((productCount) async {
      if (productCount == 0) {
        await inventoryItemOperations
            .deleteAllDataAndInventoryItem(id)
            .then((value) {
          setState(() {
            items.removeAt(index);
            Navigator.of(context).pop(true);
          });
        }).onError((error, stackTrace) {
          Navigator.of(context).pop(false);
          print('onDeleteError  ${error}  ${stackTrace}');
        });
      } else {
        Navigator.of(context).pop(false);
        context.showSnackBar(
            "Can\''nt delete! Inventory is being used by some products");
      }
    });
  }

  ///edit User
  editUser(int id, BuildContext context) async {
    if (teNameController.text.isNotEmpty) {
      var inventory = Inventory();
      inventory.id = id;
      inventory.name = teNameController.text;

      /*var dbHelper = DatabaseHelper();
    dbHelper.updateDog(product).then((update) {
      teNameController.text = "";
      teAgeController.text = "";

      Navigator.of(context).pop();
      showtoast("Data Saved successfully");
      setState(() {

      });
    });*/
      await inventoryOperations.updateRawInventory(inventory).then((value) {
        teNameController.text = "";

        Navigator.of(context).pop();
        showtoast("Data Saved successfully");

        _getData();
      });
    } else {
      showtoast("Please fill all the fields");
    }
  }

  /// Edit Click
  onSelectItem(Inventory inventory, bool checked, BuildContext context) {
    print("checkboxstate $checked");
    var invent = Inventory();
    invent.id = inventory.id;
    invent.name = inventory.name;

    inventoryOperations.updateInventory(invent).then((value) {
      showtoast("Data Saved successfully");

      setState(() {});
    });
  }

  /// Edit Click
  onEdit(Inventory inventory, int index, BuildContext context) {
    openAlertBox(inventory, context);
  }

  onEditInventory(Inventory inventory) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => AddInventoryCategoryScreenState(
                  inventory: inventory,
                ))).then((value) => value == true ? _getData() : {});
  }

  /// openAlertBox to add/edit user
  openAlertBox(Inventory inventory, BuildContext context) {
    if (inventory != null) {
      teNameController.text = inventory.name!;
    } else {
      teNameController.text = "";
    }

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25.0))),
            contentPadding: EdgeInsets.only(top: 10.0),
            content: Container(
              width: 300.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        /*flag ?*/
                        "Edit User" /*: "Add User"*/,
                        style: TextStyle(fontSize: 28.0),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Divider(
                    color: Colors.grey,
                    height: 4.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 30.0, right: 30.0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                            controller: teNameController,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              hintText: "Add Name",
                              fillColor: Colors.grey[300],
                              border: InputBorder.none,
                            ),
                            /*validator: validateName,
                            onSaved: (String val) {
                              teNameController.text = val;
                            },*/
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () => /* flag ?*/ editUser(
                        inventory.id!, context) /*: addUser()*/,
                    child: Container(
                      padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                      decoration: BoxDecoration(
                        color: Color(0xff01A0C7),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(25.0),
                            bottomRight: Radius.circular(25.0)),
                      ),
                      child: Text(
                        /*flag ?*/
                        "Edit User" /*: "Add User"*/,
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  showAddInventoryCategoryBottomSheet(Inventory? inventory) => this.project ==
          null
      ? showCustomAlertDialog(context, 'Please create a project ')
      :
      /*Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => AddInventoryCategoryScreenState(inventory: null,)))
        .then((value) =>value==true? _getData():{})*/
      MyBottomSheet().showBottomSheet(context, callback: (value) {
          if (value != null && value == true) {
            _getData();
          }
        },
          child: AddInventoryCategoryScreenState(
            inventory: inventory,
          ));

  showInventoryCategoryAndItemOperationsBottomSheet(
          StatefulWidget statefulWidget,
          {double maxHeight = 0.75}) =>
      this.project == null
          ? showCustomAlertDialog(context, 'Please create a project ')
          : MyBottomSheet().showBottomSheet(context, maxHeight: maxHeight,
              callback: (value) {
              if (value != null && value == true) {
                isLongPressEnabled = false;
                inventoryItemIdArray.clear();
                _getData();
              }
            }, child: statefulWidget);

  var isLongPressEnabled = false;
  var inventoryItemIdArray = <int>[];

  void toggleSelection(Inventory values) {
    setState(() {
      if (values.isSelected == 0) {
        values.isSelected = 1;
        inventoryItemIdArray.add(values.id!);
        print('inventoryItemIdAArray $inventoryItemIdArray');
      } else {
        values.isSelected = 0;
        inventoryItemIdArray.remove(values.id!);
        print('inventoryItemIdAArray $inventoryItemIdArray');
        final isAllSelectionClear =
            items.map((e) => e.isSelected).every((element) => element == 0);
        if (isAllSelectionClear) {
          inventoryItemIdArray.clear();
          isLongPressEnabled = !isAllSelectionClear;
        }

        print('toggleTest $isAllSelectionClear');
      }
    });
  }

  onHistoryClick(Inventory values) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => InventoryItemHistoryNewScreenState2(
                inventoryItemId: values.id))).then((value) {
      value == true ? _getData() : {};
    });
  }
}
