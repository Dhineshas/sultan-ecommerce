import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:ecommerce/database/inventory_item_operations.dart';
import 'package:ecommerce/model/InventoryItem.dart';
import 'package:ecommerce/model/Project.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:shared_preferences/shared_preferences.dart';

import '../database/iap_operation.dart';
import '../database/user_operation.dart';
import '../inventoryUnitMeasures.dart';
import '../loginView.dart';
import '../model/ProductItem.dart';
import 'CustomDialog.dart';


Future<void> showCircularLoader(BuildContext context,String? msg) async {
  return await showDialog<Null>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return SimpleDialog(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          children: <Widget>[
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(MyColors.white_chinese_e0_bg),strokeWidth: 6,),
                  SizedBox(height: 20,),
                  if(msg != null)

                    Text('$msg',style: TextStyle(color: MyColors.grey_bright_ef_bg,fontSize: 16,fontWeight: FontWeight.w800),textAlign: TextAlign.center,)
                ],
              ),
            )
          ],
        );
      });
}
Future<void> showNonCancellableCircularLoader(BuildContext context,String? msg) async {
  return await showDialog<Null>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
            onWillPop: _onWillPop,
            child:SimpleDialog(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          children: <Widget>[
             Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(MyColors.white_chinese_e0_bg),strokeWidth: 6,),
                  SizedBox(height: 20,),
                  if(msg != null)

                    Text('$msg',style: TextStyle(color: MyColors.grey_bright_ef_bg,fontSize: 16,fontWeight: FontWeight.w800),textAlign: TextAlign.center,)
                ],
              ),
            )
          ],
        ));
      });
}
Future<bool> _onWillPop() async {
  return false;
}
showtoast(String string) {
  Fluttertoast.showToast(
      msg: "$string",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: Colors.grey[800],
      textColor: Colors.white,
      fontSize: 16.0);
}




goto(BuildContext context,Widget widget) {
  Navigator.push(context, MaterialPageRoute(builder: (context) => widget));
}
//showAlertDialog

Future<void>showAlertDialog(BuildContext context,String msg) async {

  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context){
      if(Platform.isIOS){
        return  CupertinoAlertDialog(
          //title: Text(msg),
          content: Text(msg),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text('ok'),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );}else{
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          title:  Text("Message"),
          content:  Text(msg),
          actions: <Widget>[
            TextButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: const Text("OK")
            ),

          ],
        );


      }
    },
  );
}

Future<void>showCustomAlertDialog(BuildContext context,String msg) async {


 await showDialog(
    context: context,barrierDismissible: false,
    // ignore: missing_required_param
    builder: (BuildContext context) => CustomDialog(
      message: msg,
      positiveText: 'Ok',
      positiveClick: () {
        Navigator.of(context).pop();
      },
    ),
  );
}
showCustomAlertOkActionDialog({required BuildContext context,required String msg,String positiveText='Ok',required VoidCallback positiveClick}) async {


  await showDialog(
    context: context,barrierDismissible: false,
    // ignore: missing_required_param
    builder: (BuildContext context) => CustomDialog(
      message: msg,
      positiveText: positiveText,
      positiveClick: () {
        positiveClick();
      },
    ),
  );
}

showCustomCallbackAlertDialog({required BuildContext context,String positiveText='Ok',String negativeText='Cancel',required String msg,required VoidCallback positiveClick,required VoidCallback negativeClick}) async {
 await showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) => CustomDialog(
      isOkCancelDialog: true,
      message: msg,
      positiveText:positiveText,
      negativeText: negativeText,
      positiveClick: () {
        positiveClick();

      },
      negativeClick: (){
        negativeClick();
      },
    ),
  );
}
showCustomOkCallbackAlertDialog({required BuildContext context,String positiveText='Ok',String negativeText='Cancel',required String msg,required VoidCallback positiveClick,required VoidCallback negativeClick}) async {
  await showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) => WillPopScope(onWillPop: _onWillPop, child:CustomDialog(
      isOkCancelDialog: false,
      message: msg,
      positiveText:positiveText,
      negativeText: negativeText,
      positiveClick: () {
        positiveClick();

      },
      negativeClick: (){
        negativeClick();
      },
    ),
  ));
}
class MyImagePickerBottomSheet {
  static showPicker(context, {required VoidCallback? fileCallback(File? file)}) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery(galleryCallback:(file)=>
                          fileCallback(file)

                         );
                        Navigator.of(context).pop();
                      }),
                  ListTile(
                    leading: Icon(Icons.photo_camera),
                    title: Text('Camera'),
                    onTap: () {
                      _imgFromCamera(cameraCallback:(file)=>
                        fileCallback(file)

                       );
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );


        });
  }
  static final picker = ImagePicker();
  static File? _image;
   static _imgFromCamera({required VoidCallback? cameraCallback(File? file)}) async {
     var image = await picker.pickImage(source: ImageSource.camera, imageQuality: 0,maxHeight:200,maxWidth: 200);

     _image = image!=null?File(image.path):null;
     cameraCallback(_image);

   }

  static _imgFromGallery({required VoidCallback? galleryCallback(File? file)}) async {
     var image =
     await picker.pickImage(source: ImageSource.gallery, imageQuality: 0,maxHeight:200,maxWidth: 200);

     _image = image!=null?File(image.path):null;
     galleryCallback(_image);
   }
}
class MyBottomSheet{
/*
  final Function(int) callback;

  MyBottomSheet(this.callback);
*/

   showBottomSheet(context, {required Function(dynamic value) callback,double maxHeight=0.65,Widget? child}) async{

   await showModalBottomSheet(
  context: context,

  isScrollControlled: true,
       clipBehavior: Clip.antiAliasWithSaveLayer,
  shape: RoundedRectangleBorder(
  borderRadius: BorderRadius.only(
  topLeft: Radius.circular(15), topRight: Radius.circular(15)),
  ),

  builder: (builder) {
  return Container(

  height:MediaQuery.of(context).size.height * maxHeight,


  child : child!
  );



  }).then((value) => callback(value));}




}
String getImageBase64(File? _image) {
  if (_image != null) {
    var bytes = _image.readAsBytesSync();
    var base64Image = base64Encode(bytes);

    return base64Image.toString();
  }
  return "";
}

double? tryToDouble({double? value,int decimalLength=1})=>value!=null ? double.tryParse(value.toStringAsFixed(decimalLength)):0.0;

extension ExtendedSnack on BuildContext {
Future<void> showSnackBar(String msg) async {
final snackBar = SnackBar(
  content:  Text(msg),
  duration: Duration( milliseconds: 900),
  action: SnackBarAction(label: 'Ok', onPressed: () {

  },

  ),
);

// Find the ScaffoldMessenger in the widget tree
// and use it to show a SnackBar.
ScaffoldMessenger.of(this).showSnackBar(snackBar);
}}


var inventoryItemOperations = InventoryItemOperations();
Future<File> generateDocument(int? projectID) async {
  var items = <InventoryItem>[];
  var inventoryCategory = <String>[];
  await inventoryItemOperations.inventoriesItemsWithCategory(projectID,currentYear).then((value) {
    value.sort((a, b) => a.name!.compareTo(b.name!));
    items = value;

  });
  for (var inventoryItem in items) {
    if(!inventoryCategory.contains(inventoryItem.inventoryName)){
      inventoryCategory.add(inventoryItem.inventoryName.toString());
      inventoryCategory.sort((a, b) => a.compareTo(b));
    }
  }



  final pw.Document doc = pw.Document();

  doc.addPage(pw.MultiPage(
      pageFormat:PdfPageFormat.a4,
      crossAxisAlignment: pw.CrossAxisAlignment.start,
      header: (pw.Context context) {
        return pw.Container(
          height: 20,
            alignment: pw.Alignment.centerRight,
            margin: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
            padding: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
            decoration: const pw.BoxDecoration(
                border: pw.Border(top: pw.BorderSide(color: PdfColors.amber100,),bottom:pw.BorderSide(color: PdfColors.amber100,),right: pw.BorderSide(color: PdfColors.amber100,),left: pw.BorderSide(color: PdfColors.amber100,) ),),
            child: pw.Text('Inventory Report ',

                style: pw.Theme.of(context)
                    .defaultTextStyle
                    .copyWith(color: PdfColors.grey)));
      },
      build: (pw.Context context) => <pw.Widget>[




      pw.Table.fromTextArray(
      defaultColumnWidth: pw.FixedColumnWidth(10.0),
  cellHeight: 25,
  context: context,
  border:pw.TableBorder(
  verticalInside: pw.BorderSide(color: PdfColors.grey,),horizontalInside: pw.BorderSide(color: PdfColors.grey,),right: pw.BorderSide(color: PdfColors.grey,),left: pw.BorderSide(color: PdfColors.grey,),top: pw.BorderSide(color: PdfColors.grey,),bottom: pw.BorderSide(color: PdfColors.grey,),
  ),data: [ <String>['Name', 'Stock / Pieces'],]),

        for (int i = 0; i < inventoryCategory.length; i++)
        pw.Table.fromTextArray(
          defaultColumnWidth: pw.FixedColumnWidth(10.0),
            cellHeight: 25,
            context: context,
            border:pw.TableBorder(
              verticalInside: pw.BorderSide(color: PdfColors.grey,),horizontalInside: pw.BorderSide(color: PdfColors.grey,),right: pw.BorderSide(color: PdfColors.grey,),left: pw.BorderSide(color: PdfColors.grey,),top: pw.BorderSide(color: PdfColors.grey,),bottom: pw.BorderSide(color: PdfColors.grey,),
            ),

            headerAlignment: pw.Alignment.centerLeft,
            data: <List<dynamic>>[

              <String>[inventoryCategory.elementAt(i)],
              //<String>['Name', 'Boxes / Pieces'],

                  for (int j = 0; j < items.length; j++)
                    if(inventoryCategory.elementAt(i)==items.elementAt(j).inventoryName)
                      <String>['${items.elementAt(j).name}',items.elementAt(j).isPiecesInTypeUpdated==0? '${items.elementAt(j).totalType} ${InventoryUnitMeasure.inventoryUnitMeasureArray[items.elementAt(j).itemType!]} \'s':'${items.elementAt(j).stock} Items'],


                   ],

        ),

        pw.Paragraph(text: ""),
        pw.Padding(padding: const pw.EdgeInsets.all(10)),

      ]));
  final output = await getTemporaryDirectory();
  final file = File('${output.path}/inventory_report.pdf');
  await file.writeAsBytes(await doc.save());
  return file;
  //return doc.save();
}


  extension CustomDoubleRoundOffExtension on String {
    double get toCustomDoubleRoundOff {
  final double value=double.tryParse(this)!;
  if(value.toString().contains('.')){
    int? decimal= int.tryParse(value.toString().split('.')[1].substring(0,1));
    if(decimal!>0&&decimal<=5){
      final double result=(value.toInt())+(0.5);
      return result;
    }else{
      final double result=value.roundToDouble();
      return result;
    }
  }else{
    return value;
  }
}
}

class MyPref {

  static setProject(Project? project) async {
    (await SharedPreferences.getInstance()).setString("project",project!=null? jsonEncode(project.toJson()):'');
  }

  static Future<Project?> getProject() async {
    final s = (await SharedPreferences.getInstance()).getString("project");
    return s?.isNotEmpty == true ? Project.fromJson(jsonDecode(s!)) : null;
  }

  static setDbTrialExpiryDate(String? time) async {
    (await SharedPreferences.getInstance()).setString("db_trial_expiry_date",time!=null? time:'');
  }

  static Future<String?> getDbTrialExpiryDate() async {
    final s = (await SharedPreferences.getInstance()).getString("db_trial_expiry_date");
    return s?.isNotEmpty == true ? s : '';
  }

  static setDbTrialStartDate(String? time) async {
    (await SharedPreferences.getInstance()).setString("db_trial_start_date",time!=null? time:'');
  }

  static Future<String?> getDbTrialStartDate() async {
    final s = (await SharedPreferences.getInstance()).getString("db_trial_start_date");
    return s?.isNotEmpty == true ? s : '';
  }
}

  Future<bool> checkInternetConnectivity() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        return true;
      }
      return false;
    } on SocketException catch (_) {
      print('not connected');
      return false;
  }
}

var userOperations = UserOperations();

Future logoutFunction(BuildContext context) async {
  if (Platform.isAndroid) {
    await userOperations.updateAllIsLoggedInState().then((user) async {
      await FirebaseAuth.instance.signOut().then((value) async {
        GoogleSignIn _googleSignIn = GoogleSignIn();
        await _googleSignIn.signOut().then((value) async {
          MyPref.setProject(null);

          /*Navigator.of(context).pushReplacement(
                 MaterialPageRoute(builder: (context) => LoginScreen()));*/

          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => LoginScreen()),
              (Route<dynamic> route) => false);
        });
      });
    });
  } else {
    await userOperations.updateAllIsLoggedInState().then((user) async {
      await FirebaseAuth.instance.signOut().then((value) async {
        MyPref.setProject(null);

        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => LoginScreen()),
            (Route<dynamic> route) => false);
      });
    });
  }
}

Future<String?> getDeviceName() async {
  var deviceInfo = DeviceInfoPlugin();
  if (Platform.isIOS) {
    // import 'dart:io'
    var iosDeviceInfo = await deviceInfo.iosInfo;
    // print('${iosDeviceInfo.name!} +  AND + ${iosDeviceInfo.model!}  ${iosDeviceInfo.localizedModel!}  ${iosDeviceInfo.utsname!}  ${iosDeviceInfo.systemVersion!}');
    return iosDeviceInfo.name! + '++' + iosDeviceInfo.systemName!;
  } else {
    var androidDeviceInfo = await deviceInfo.androidInfo;
    return androidDeviceInfo.androidId!; // Unique ID on Android
  }
}

int get currentYear => DateTime.now().year;
