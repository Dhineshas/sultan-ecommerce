import 'dart:io';

import 'package:intl/intl.dart';

import '../database/iap_operation.dart';
import 'Utils.dart';

class Subscription{
  static String  premiumSubscriptionProductID='premium';
  static String  standardSubscriptionProductID='standard';
  static final dateFormat = DateFormat("yyyy-MM-dd hh:mm:ss.SSS");
  static const dateFormat2 = '';
  static Future<bool> isTrialVersionActive() async {
    var expiryDate=await MyPref.getDbTrialExpiryDate();
    if(expiryDate!=null&& expiryDate!=''){

      DateTime parseDate = dateFormat.parse(expiryDate.toString());
      final currentDate = DateTime.now();
      //DateTime(parseDate.year, parseDate.month, parseDate.day+15,parseDate.hour,parseDate.minute,parseDate.second);

      //print('datedifference $expiryDate  $currentDate  ${parseDate.difference(currentDate).inDays}   ${parseDate.difference(currentDate).isNegative}');
      if(parseDate.difference(currentDate).isNegative){
        return false;
      }else
        return true;

    }else
      return false;
  }

  static Future<int> getTrialExpiryDays() async {
    var expiryDate=await MyPref.getDbTrialExpiryDate();
    if(expiryDate!=null&& expiryDate!=''){

      DateTime parseDate = dateFormat.parse(expiryDate.toString());
      final currentDate = DateTime.now();
      //DateTime(parseDate.year, parseDate.month, parseDate.day+15,parseDate.hour,parseDate.minute,parseDate.second);

      //print('datedifference $expiryDate  $currentDate  ${parseDate.difference(currentDate).inDays}   ${parseDate.difference(currentDate).isNegative}');
     return parseDate.difference(currentDate).inDays;

    }else
      return -1;
  }
  static var iapOperation =  IAPOperations();
  // 0. no active subscription or trial
  // 1. trail is active
  // 2. premium plan is active
  // 3. standard plan is active
  static Future<int> getActiveSubscription() async {
    //final isTrialActive=await isTrialVersionActive();
    /* final isTrialActive=await getTrialExpiryDays();


    if(isTrialActive>0)
        return 1;
      final isPremiumPlanActive =await iapOperation.isSubscriptionPlanActive(premiumSubscriptionProductID);
      if(isPremiumPlanActive)
        return 2;
      final isStandardPlanActive =await iapOperation.isSubscriptionPlanActive(standardSubscriptionProductID);
      if(isStandardPlanActive)
        return 3;
      return 0;*/
    final isPremiumPlanActive =await iapOperation.isSubscriptionPlanActive(Subscription.premiumSubscriptionProductID);
    if(isPremiumPlanActive)
      return 2;
    final isStandardPlanActive =await iapOperation.isSubscriptionPlanActive(Subscription.standardSubscriptionProductID);
    if(isStandardPlanActive)
      return 3;
    final isTrialActive=await getTrialExpiryDays();
    if(isTrialActive>0)
      return 1;
    return 0;


  }


}