import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'MyColors.dart';
import 'dart:io';

class MyDecorations {
  static const whiteBGRounded10 = const BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(10)),
      color:  MyColors.white);


  static const greenShadowBGRounded10 =const BoxDecoration(
      boxShadow: [
        BoxShadow(
          color: MyColors.white_chinese_e0_bg,
          offset: Offset(0.0, 1.0), //(x,y)
          blurRadius: 6.0,
        ),
      ],
      borderRadius: BorderRadius.all(Radius.circular(10)),// Creates border
      color: MyColors.white_chinese_e0_bg);
  static BoxDecoration decoration1(
      {Color color = const Color(0xffFFFFFF),
        double borderRadius = 15,
        bool noBorderBottom = false,
        double blurRadius = 10,
        double dx = 0,
        double dy = 3,
        Color shadowColor = const Color(0x29000000)}) {
    return BoxDecoration(
      boxShadow: [
        BoxShadow(
          color: shadowColor,
          blurRadius: blurRadius,
          // has the effect of softening the shadow
          spreadRadius: 0.0,
          // has the effect of extending the shadow
          offset: Offset(
            dx, // horizontal, move right 10
            dy, // vertical, move down 10
          ),
        )
      ],
      borderRadius: noBorderBottom
          ? BorderRadius.only(
          topLeft: Radius.circular(borderRadius),
          topRight: Radius.circular(borderRadius))
          : BorderRadius.all(Radius.circular(borderRadius)),
      color: color,
    );
  }
  static RoundedRectangleBorder roundedRectangleBorder( [double borderRadius = 10]){
    return RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
        );
  }



}
class MyStyles {
  /// style name  format:  colorCode_FONTNAME_FONTSIZE*/
  static const white_HR_16 = TextStyle(
      color: MyColors.white, /*fontFamily: MyFonts.helveticaRegular,*/ fontSize: 16);

  static const black0E__HR_16 = TextStyle(
    /* fontFamily: MyFonts.helveticaRegular,*/
      fontSize: 15,
      color: MyColors.black_0101);

  static const grey_70__HR_16 = TextStyle(
    /* fontFamily: MyFonts.helveticaRegular,*/

      fontSize: 15,
      color: MyColors.grey_70);
  static const black_17_w500= TextStyle(

      fontSize: 17,
      fontWeight: FontWeight.w500,
      color: MyColors.black_0101);
  static TextStyle customTextStyle( {double fontSize = 14,Color color=MyColors.black_0101,FontWeight fontWeight=FontWeight.normal}){
    return TextStyle(
      fontWeight: fontWeight,
        fontSize: fontSize,
        color: color

    );
  }

 static WidgetSpan buildCustomTextSpan({required String text, required TextStyle style,required PlaceholderAlignment alignment}) {
    return WidgetSpan(
      alignment: alignment,
      child: Text(text, style: style,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,),
    );
  }
  }
class MyText{
  static priceCurrencyText(BuildContext context, {String price='',String exponent=' QR',double fontSize=14,FontWeight fontWeight=FontWeight.bold, Color fontColor=MyColors.black0E}){
    return RichText(
      text: TextSpan(
        style: DefaultTextStyle.of(context).style,
        children: [
          MyStyles.buildCustomTextSpan(
              text: price,
              style: MyStyles.customTextStyle(
                  fontSize: fontSize,
                  color: fontColor,
                  fontWeight: fontWeight),
              alignment:
              PlaceholderAlignment.middle),
          MyStyles.buildCustomTextSpan(
              text: exponent,
              style: MyStyles.customTextStyle(
                fontSize: 10,
                color: fontColor,
              ),
              alignment: PlaceholderAlignment.top)
        ],
      ),
    );}
}
class MyWidgets {
  static buildCircularProgressIndicator(){
    return  Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [SizedBox(
            width: 30,
            height: 30,
            child: CircularProgressIndicator(color: MyColors.grey_70,),
          ),
            Padding(
              padding: EdgeInsets.only(top: 16),
              child: Text('Awaiting result...'),
            )],
        ));
  }
  static scaffold({Widget? body,AppBar? appBar,Widget? bottomNavigationBar,Widget? bottomSheet}) {
    return Scaffold(
      backgroundColor: MyColors.white_f1_bg,
      appBar: appBar,
      bottomNavigationBar: bottomNavigationBar,
      bottomSheet: bottomSheet,
      body: body ?? Container(),
    );

  }

  static appBar({double elevation=0,double? titleSpacing,double? toolbarHeight,Color? color=MyColors.white_f1_bg,Widget? leading,List<Widget>? actions,Widget? title,Widget? flexibleSpace}){
    return AppBar(
        automaticallyImplyLeading: false,
      toolbarHeight: toolbarHeight,
      elevation: elevation,
      titleSpacing: titleSpacing,
      title: title,
      flexibleSpace: flexibleSpace,
      centerTitle: true,
      backgroundColor: color,
      leading: leading,
      actions: actions
    );
  }
  static shadowContainer({double? height,double? width,double paddingL=0,double paddingT=0,double paddingR=0,double paddingB=0,double marginL=0,double marginT=0,double marginR=0,double marginB=0,Color? color=MyColors.white,double cornerRadius=0,Color shadowColor=Colors.grey,double spreadRadius=5,double blurRadius=5,Widget? child}) {
    return Container(
      margin: EdgeInsets.fromLTRB(marginL,marginT,marginR,marginB),
      padding: EdgeInsets.fromLTRB(paddingL, paddingT, paddingR,paddingB),
      height:height ,
      width: width,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(cornerRadius),
            topRight: Radius.circular(cornerRadius),
            bottomLeft: Radius.circular(cornerRadius),
            bottomRight: Radius.circular(cornerRadius)
        ),
        boxShadow: [
          BoxShadow(
            color: shadowColor.withOpacity(0.1),
            spreadRadius: spreadRadius,
            blurRadius: blurRadius,
            offset: Offset(0, 4),
          ),
        ],
      ),
      child: child,
    );
  }

  static container({double? height,double? width,double paddingL=0,double paddingT=0,double paddingR=0,double paddingB=0,double marginL=0,double marginT=0,double marginR=0,double marginB=0,Color? color=Colors.white,double topLeftCornerRadius=0,double topRightCornerRadius=0,double bottomLeftCornerRadius=0,double bottomRightCornerRadius=0,List<BoxShadow>? boxShadow,Widget? child}){
    return Container(
        margin: EdgeInsets.fromLTRB(marginL,marginT,marginR,marginB),
    padding: EdgeInsets.fromLTRB(paddingL, paddingT, paddingR,paddingB),
    height:height ,
    width: width,
    decoration: BoxDecoration(
    color: color,
    borderRadius: BorderRadius.only(
    topLeft: Radius.circular(topLeftCornerRadius),
    topRight: Radius.circular(topRightCornerRadius),
    bottomLeft: Radius.circular(bottomLeftCornerRadius),
    bottomRight: Radius.circular(bottomRightCornerRadius)
    ),boxShadow: boxShadow),child: child,);
  }
  static textViewContainer({double? height,double? width,double paddingL=0,double paddingT=0,double paddingR=0,double paddingB=0,double marginL=0,double marginT=0,double marginR=0,double marginB=0,Color? color=Colors.white,double cornerRadius=0,Widget? child}){
    return Container(
      margin: EdgeInsets.fromLTRB(marginL,marginT,marginR,marginB),
      padding: EdgeInsets.fromLTRB(paddingL, paddingT, paddingR,paddingB),
      height:height ,
      width: width,
      decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(cornerRadius)),boxShadow: [ BoxShadow(color: Colors.grey.shade400, blurRadius: 2.0, spreadRadius: 0.4)
      ]),child: child,);
  }

  static textView({String text='',TextAlign textAlign=TextAlign.center,TextStyle? style,int? maxLine}){
    return Text(text,
      maxLines: maxLine,
      textAlign: textAlign,
        style:style==null?  MyStyles.customTextStyle(fontSize: 14,fontWeight: FontWeight.normal,):style
    );
  }
  static textFromField({TextInputType? keyboardType=TextInputType.name,TextEditingController? controller,List<TextInputFormatter>? inputFormatters,double contentPaddingL=0,double contentPaddingT=0,double contentPaddingR=0,double contentPaddingB=0,double cornerRadius=0,Color color=MyColors.white,bool readOnly=false, String? hintText}){
    return TextFormField(
      readOnly: readOnly,
      keyboardType: keyboardType,
      controller: controller,
      textCapitalization: TextCapitalization.words,
      obscureText: false,
      inputFormatters: inputFormatters,

      decoration:
      InputDecoration(
        helperText: hintText,
        contentPadding: EdgeInsets.fromLTRB(contentPaddingL, contentPaddingT, contentPaddingR, contentPaddingB),
        border: OutlineInputBorder(
            borderRadius:  BorderRadius.circular(cornerRadius),
            borderSide: BorderSide.none
        ),
        filled: true,
        fillColor: color,
      ),
    );
  }
  static materialButton(BuildContext context,{String text='',Color color = MyColors.black,Color textColor = MyColors.white,double elevation=5,VoidCallback? onPressed}){
   return Material(
      elevation: elevation,
      borderRadius: BorderRadius.circular(10.0),
      color: (color),
      child: MaterialButton(
        minWidth: MediaQuery
            .of(context)
            .size
            .width,
        onPressed: onPressed,
        child: MyWidgets.textView(text:
        text,
          style: MyStyles.customTextStyle(
              color: textColor),
        ),
      ),
    );
  }
  static addNewButton({String text="Add New",VoidCallback? onArrowPressed,VoidCallback? onAddOrMovePressed}){
    return MyWidgets.shadowContainer(
        // marginB :25,
      paddingB: Platform.isIOS ? 20 : 0,
        child: Row(mainAxisSize: MainAxisSize.min,  mainAxisAlignment: MainAxisAlignment.spaceEvenly,crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          height: 70 ,
          width: 70,
          child:InkWell(child :FittedBox(
            child: Image.asset('assets/images/group_25.png'),
            fit: BoxFit.cover,
          ),onTap: onArrowPressed,)
        ),
        Expanded(flex:1,child: ElevatedButton.icon(
          icon: const Icon(
            Icons.add_circle_outline_rounded,
            color: MyColors.black,
          ),
          onPressed: onAddOrMovePressed,
          label: MyWidgets.textView(text:
            text,
            style: MyStyles.customTextStyle(fontSize: 13),
          ),
          style: ElevatedButton.styleFrom(
            primary: MyColors.white_chinese_e0_bg,
            fixedSize: const Size(0, 44),
            shape: MyDecorations.roundedRectangleBorder(8),
          ),
        ))
        ,SizedBox( height:5,width: 15,)],));
  }
 }
class MySlidableWidgets {
  static slidable({ActionPane? startActionPane,ActionPane? endActionPane,Widget? child}) {
    return Slidable(
      // Specify a key if the Slidable is dismissible.
      key:  ValueKey(0),
      // The start action pane is the one at the left or the top side.
      startActionPane: /*ActionPane(
          // A motion is a widget used to control how the pane animates.
          motion:  ScrollMotion(),

          // A pane can dismiss the Slidable.
          dismissible: DismissiblePane(onDismissed: () {}),

          // All actions are defined in the children parameter.
          children:  [
            // A SlidableAction can have an icon and/or a label.
            SlidableAction(
              onPressed: (BuildContext context){

              },
              backgroundColor: Color(0xFFFE4A49),
              foregroundColor: Colors.white,
              icon: Icons.delete,
              label: 'Delete',
            ),
            SlidableAction(
              onPressed: (BuildContext context){

              },
              backgroundColor: Color(0xFF21B7CA),
              foregroundColor: Colors.white,
              icon: Icons.share,
              label: 'Share',
            ),
          ],
        ),*/
      startActionPane,
        endActionPane: /*ActionPane(

      motion: BehindMotion(),
    extentRatio: 0.25,

    children: <Widget>[

    SlidableAction(
              // An action can be bigger than the others.
              //flex: 2,
              onPressed: (BuildContext context){

              },
              backgroundColor: MyColors.grey_9d_bg,
              foregroundColor: Colors.white,
              icon: Icons.recycling_outlined,
              label: 'Archive',
            ),
            SlidableAction(
              onPressed: (BuildContext context){

              },
              backgroundColor: MyColors.red_ff_bg,
              foregroundColor: Colors.white,
              icon: Icons.close,
              label: 'Delete',
            ),]),*/
      endActionPane,
      child: child!,
    );
  }
}
enum AuthError {
  invalidEmail,
  userDisabled,
  userNotFound,
  wrongPassword,
  emailAlreadyInUse,
  invalidCredential,
  operationNotAllowed,
  weakPassword,
  error,
  object_not_found,
  bucket_not_found,
  project_not_found,
  quota_exceeded,
  unauthenticated,
  unauthorized,
  retry_limit_exceeded,
  invalid_checksum,
  canceled,
  invalid_event_name,
  invalid_url,
  invalid_argument,
  cannot_slice_blob,
  server_file_wrong_size,
}
