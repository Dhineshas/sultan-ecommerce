import 'dart:io';
import 'dart:io' as io;
import 'package:device_info_plus/device_info_plus.dart';
import 'package:ecommerce/database/firestore_operations.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/InAppPurchaseHandler.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart' as pathh;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
//import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:path_provider/path_provider.dart';
import '../database/user_operation.dart';
import 'Utils.dart';

//final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

class FirebaseUtils {
  //var firebase= FirebaseAuth.instance;
  final dateFormat = DateFormat("yyyy-MM-dd hh:mm:ss.SSS");
  //final storageRef = FirebaseStorage.instance.refFromURL('gs://xpress-pos-37245.appspot.com/${FirebaseAuth.instance.currentUser?.providerData.first.email}${FirebaseAuth.instance.currentUser?.providerData.first.providerId}/${FirebaseAuth.instance.currentUser?.providerData.first.email}${FirebaseAuth.instance.currentUser?.providerData.first.providerId}.db');
  final storageRef = FirebaseStorage.instance.refFromURL('gs://xpress-pos-37245.appspot.com/${FirebaseAuth.instance.currentUser?.providerData.first.email}/${FirebaseAuth.instance.currentUser?.providerData.first.email}.db');
  var fireStoreOperations = FireStoreOperations();
  var userOperations = UserOperations();
  var firebaseUserEmail =FirebaseAuth.instance.currentUser?.providerData.first.email;

  Future<UploadTask?> firebaseUploadFile(BuildContext context,
      {String fileUploadDate = ''}) async {
    await checkInternetConnectivity().then((value) async {
      if (value) {
        //if (Platform.isIOS) {
          showNonCancellableCircularLoader(context, 'Uploading..');
       // }
        Future uploadTask({String getFileUploadDate = ''}) async {
          try {
            io.Directory documentsDirectory =
                await getApplicationDocumentsDirectory();

            //var firebaseUser = FirebaseAuth.instance.currentUser;
            if (firebaseUserEmail != null) {
             // var databaseName = '${firebaseUser.providerData.first.email}${firebaseUser.providerData.first.providerId}.db';
              var databaseName = '${firebaseUserEmail}.db';
              print(
                  'databaseName     $databaseName   $firebaseUserEmail');
              String path = pathh.join(documentsDirectory.path, "$databaseName");
              final file = File(path);
              if (!file.existsSync()) {
                context.showSnackBar('No file was selected');
                return null;
              }

              UploadTask uploadTask;

              // Create a Reference to the file
             /* Reference ref = FirebaseStorage.instance
                  .ref()
                  .child('${firebaseUser.providerData.first.email}${firebaseUser.providerData.first.providerId}')
                  .child('/${firebaseUser.providerData.first.email}${firebaseUser.providerData.first.providerId}.db');*/
              Reference ref = FirebaseStorage.instance
                  .ref()
                  .child('${firebaseUserEmail}')
                  .child('/${firebaseUserEmail}.db');
              await /*_getDeviceId()*/ _getDeviceBrandAndModel()
                  .then((value) async {
                final metadata = SettableMetadata(
                  contentType: 'database/db',
                  customMetadata: {
                    'picked-file-path': file.path,
                    'last-device': value.deviceId,
                    'last-device-details': value.deviceManufacturer,
                    'first-upload-date': getFileUploadDate
                  },
                );

                if (kIsWeb) {
                  uploadTask = ref.putData(await file.readAsBytes(), metadata);
                } else {
                  uploadTask = ref.putFile(io.File(file.path), metadata);
                }

// Listen for state changes, errors, and completion of the upload.
                uploadTask.snapshotEvents
                    .listen((TaskSnapshot taskSnapshot) async {
                  switch (taskSnapshot.state) {
                    case TaskState.running:
                      {
                        final progress = 100.0 *
                            (taskSnapshot.bytesTransferred /
                                taskSnapshot.totalBytes);
                        print("Upload is $progress% complete.");
                        //await _showProgressNotification(isUploadFile: true, progress: progress.toInt());
                      }
                      break;
                    case TaskState.paused:
                      print("Upload is paused.");
                      break;
                    case TaskState.canceled:
                      popContext(context);
                      print("Upload was canceled");
                      break;
                    case TaskState.error:
                      // Handle unsuccessful uploads
                      popContext(context);
                      //await flutterLocalNotificationsPlugin.cancelAll();
                      break;
                    case TaskState.success:
                      // Handle successful uploads on complete
                      // ...
                      {
                        popContext(context);

                        print("Upload successful");
                      }

                      break;
                  }
                });

                return Future.value(uploadTask);
              });


              await storageRef.getDownloadURL().then((downloadUrl) async {
                if (downloadUrl.isNotEmpty) {
                  print("downloadUrl  $downloadUrl");
                  await fireStoreOperations.updateFireStoreUserStorageUrl(firebaseUserEmail,downloadUrl).then((value) async{
                  await userOperations.getLastLoggedInUser().then((user) async {
                    user.first.userDbUrl = downloadUrl;
                    await userOperations
                        .updateUserDbUrl(user.first)
                        .then((value) async {
                      await this.getFirebaseDbFirstUploadMetaData(context);
                      //await flutterLocalNotificationsPlugin.cancelAll();
                      //showCustomAlertDialog(context, 'Upload successful \n $downloadUrl');
                    });
                  });
                  });
                } else
                  await showCustomAlertDialog(context, 'something went wrong');
              });
            }
          } catch (e) {
            popContext(context);
            //await flutterLocalNotificationsPlugin.cancelAll();
          }
        }

        try {
          final metadata = await storageRef.getMetadata();
          if (metadata.customMetadata != null &&
              metadata.customMetadata!['last-device'] != await _getDeviceId()) {
            print('metadatacondition 1');
            await showCustomCallbackAlertDialog(
                context: context,
                msg:
                    'Caution data lose! you are using a new device, your old data will be re-written.',
                positiveText: 'Upload',
                positiveClick: () async {
                  Navigator.of(context).pop();
                  await uploadTask(
                      getFileUploadDate: fileUploadDate.isNotEmpty
                          ? fileUploadDate
                          : metadata.customMetadata!['first-upload-date']
                              .toString());
                },
                negativeClick: () {
                  Navigator.of(context).pop();
                });
          } else if (metadata.customMetadata != null) {
            await uploadTask(
                getFileUploadDate: fileUploadDate.isNotEmpty
                    ? fileUploadDate
                    : metadata.customMetadata!['first-upload-date'].toString());
          } else {
            await uploadTask(getFileUploadDate: DateTime.now().toString());
          }
        } on FirebaseException catch (e) {
          if (e.code == 'object-not-found') {
            await uploadTask(getFileUploadDate: DateTime.now().toString());
            return;
          } else {
            await showCustomAlertDialog(
                context, 'Error found. ${e.code}=${e.message}');
          }
          //uploadTask();
          print(e);
        } catch (e) {
          print(e);
        }
      } else
        showAlertDialog(context, 'No internet connection');
    });
  }

  Future getFirebaseDbFirstUploadMetaData(BuildContext context) async {
    await checkInternetConnectivity().then((value) async {
      if (value) {
        try {
          final metadata = await storageRef.getMetadata();
          print('customMetadata  ${metadata.customMetadata}');
          if (metadata.customMetadata != null &&
              metadata.customMetadata!['first-upload-date']
                  .toString()
                  .isNotEmpty) {
            print(
                'first_upload_date  ${metadata.customMetadata!['first-upload-date']}');
            DateTime parseDate = dateFormat.parse(
                metadata.customMetadata!['first-upload-date'].toString());
            var newDate = DateTime(
                parseDate.year,
                parseDate.month,
                parseDate.day + 7,
                parseDate.hour,
                parseDate.minute,
                parseDate.second);
            await MyPref.setDbTrialStartDate(parseDate.toString());
            await MyPref.setDbTrialExpiryDate(newDate.toString());
            print('getDbFirstUploadTime ${newDate}');
          }
          return 'success';
        } on FirebaseException catch (e) {
          print('customMetadataFirebaseException  ${e}');
          if (e.code == 'object-not-found') {
            await this.firebaseUploadFile(context,
                fileUploadDate: DateTime.now().toString());
          }
        }
      }
    });
  }

  Future<void> firebaseDownloadFile(BuildContext context,
      {required VoidCallback? callback(TaskState? taskState)}) async {
    await checkInternetConnectivity().then((value) async {
      if (value) {
        await showCustomCallbackAlertDialog(
            context: context,
            msg:
                'Caution.! Always make sure that the latest data is sync with cloud before downloading.',
            positiveText: 'Download',
            negativeText: 'Sync',
            positiveClick: () async {
              Navigator.of(context).pop();

              try {
                showNonCancellableCircularLoader(
                    context, 'Downloading Database...');
                io.Directory documentsDirectory =
                    await getApplicationDocumentsDirectory();

                var downloadFilePath =
                    File('${documentsDirectory.path}/file_db');
                final downloadTask = storageRef.writeToFile(downloadFilePath);

                downloadTask.snapshotEvents.listen((taskSnapshot) async {
                  switch (taskSnapshot.state) {
                    case TaskState.running:
                      {
                        final progress = 100.0 *
                            (taskSnapshot.bytesTransferred /
                                taskSnapshot.totalBytes);
                        print("Download is $progress% complete.");
                       // await _showProgressNotification(isUploadFile: false, progress: progress.toInt());
                      }
                      break;
                    case TaskState.paused:
                      popContext(context);
                      break;

                    case TaskState.canceled:
                      popContext(context);
                      print("Download is canceled.");
                      break;
                    case TaskState.error:
                      popContext(context);
                      await showCustomAlertDialog(context,
                          'No database found or something went wrong.');
                     // await flutterLocalNotificationsPlugin.cancelAll();
                      break;
                    case TaskState.success:
                      {
                        if (await downloadFilePath.exists()) {
                          //await downloadFilePath.copy('${documentsDirectory.path}/${FirebaseAuth.instance.currentUser?.providerData.first.providerId}.db').then((value)async =>
                          await downloadFilePath
                              //.copy('${documentsDirectory.path}/${FirebaseAuth.instance.currentUser?.providerData.first.email}${FirebaseAuth.instance.currentUser?.providerData.first.providerId}.db')
                              .copy('${documentsDirectory.path}/${firebaseUserEmail}.db')
                              .then((value) async =>
                                  await downloadFilePath.delete());
                        }
                        await MyPref.setProject(null);
                        //await flutterLocalNotificationsPlugin.cancelAll();
                        popContext(context);

                        await this.getFirebaseDbFirstUploadMetaData(context);
                        //update subscription to db on successful download.
                      try {
                      await InAppPurchaseHandler.updateFireStoreSubscriptionToLocalDb(
                      FirebaseAuth.instance.currentUser!.email!);
                      } catch (e) {
                      print("Failed to get ${e}");
                      }
                        callback(taskSnapshot.state);

                      }
                      break;
                  }
                });

                downloadTask.onError((error, stackTrace) async {
                  await showCustomAlertDialog(
                      context, 'No database found / Something went wrong.');
                  //await flutterLocalNotificationsPlugin.cancelAll();
                  return downloadTask.snapshot;
                });
              } on FirebaseException catch (e) {
                print("error download = ${e.message}");
                context.showSnackBar('EEE == $e');
                //await flutterLocalNotificationsPlugin.cancelAll();
              } catch (e) {
                if (e != null) {
                  print("error  = ${e}");
                  context.showSnackBar('FFF == $e');
                } else {
                  print('catch error create accEEEELSE');
                }
               // await flutterLocalNotificationsPlugin.cancelAll();
              }
            },
            negativeClick: () async {
              Navigator.of(context).pop();
              await this.firebaseUploadFile(context);
            });
      } else
        await showAlertDialog(context, 'No internet connection');
    });
  }

  Future<FullMetadata?> getLastDatabaseUploadedDate(
      BuildContext context) async {
    final gg = await checkInternetConnectivity();
    if (gg) {
      try {
        // showCircularLoader(context, 'checking last synced data..'); // Get metadata properties
        final metadata = await storageRef.getMetadata();
        return metadata;
      } on FirebaseException catch (e) {
        print('customMetadataFirebaseException  ${e}');
        if (e.code == 'object-not-found') {}
      }
      return null;
    } else {
      showAlertDialog(context, 'No internet connection');
      return null;
    }
  }

  /*Future<void> _showProgressNotification(
      {bool isUploadFile = true, int progress = 0}) async {
    const int maxProgress = 100;
    //for (int i = 0; i <= maxProgress; i++) {
    //  await Future<void>.delayed(const Duration(seconds: 1), () async {
    final AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails('progress channel', 'progress channel',
            channelShowBadge: false,
            importance: Importance.max,
            priority: Priority.high,
            onlyAlertOnce: true,
            showProgress: true,
            maxProgress: maxProgress,
            progress: progress);
    final NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0,
        isUploadFile ? 'Syncing Database' : 'Download Database',
        isUploadFile
            ? 'Database sync in progress'
            : 'Database download in progress',
        platformChannelSpecifics,
        payload: 'item x');
    // });
  }*/

//}

  Future<String?> _getDeviceId() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      // import 'dart:io'
      var iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // Unique ID on iOS
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // Unique ID on Android
    }
  }

  Future<DeviceInfo> _getDeviceBrandAndModel() async {
    var deviceInfo = DeviceInfoPlugin();
    var device = DeviceInfo();
    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfo.iosInfo;
      device.deviceId = iosDeviceInfo.identifierForVendor!;
      device.deviceManufacturer =
          '${iosDeviceInfo.name} ${iosDeviceInfo.model}';
      return device;
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      device.deviceId = androidDeviceInfo.androidId!;
      device.deviceManufacturer =
          '${androidDeviceInfo.manufacturer} ${androidDeviceInfo.model}';
      return device;
    }
  }

  popContext(BuildContext context) {
    //if (Platform.isIOS) {
      Navigator.of(context).pop();
    //}
  }

  AuthError determineFirebaseException(FirebaseException exception) {
    /*switch (exception.code) {
      case 'invalid-email':
        return AuthError.invalidEmail;
      case 'user-disabled':
        return AuthError.userDisabled;
      case 'user-not-found':
        return AuthError.userNotFound;
      case 'wrong-password':
        return AuthError.wrongPassword;
      case 'email-already-in-use':
      case 'account-exists-with-different-credential':
        return AuthError.emailAlreadyInUse;
      case 'invalid-credential':
        return AuthError.invalidCredential;
      case 'operation-not-allowed':
        return AuthError.operationNotAllowed;
      case 'weak-password':
        return AuthError.weakPassword;
      case 'object-not-found':
        return AuthError.object_not_found;
      case 'ERROR_MISSING_GOOGLE_AUTH_TOKEN':
      default:
        return AuthError.error;
    }*/
    return returnException(exception.code);
  }

  AuthError determineFirebaseAuthException(FirebaseAuthException exception) {
    /*switch (exception.code) {
      case 'invalid-email':
        return AuthError.invalidEmail;
      case 'user-disabled':
        return AuthError.userDisabled;
      case 'user-not-found':
        return AuthError.userNotFound;
      case 'wrong-password':
        return AuthError.wrongPassword;
      case 'email-already-in-use':
      case 'account-exists-with-different-credential':
        return AuthError.emailAlreadyInUse;
      case 'invalid-credential':
        return AuthError.invalidCredential;
      case 'operation-not-allowed':
        return AuthError.operationNotAllowed;
      case 'weak-password':
        return AuthError.weakPassword;
      case 'object-not-found':
        return AuthError.object_not_found;
      case 'ERROR_MISSING_GOOGLE_AUTH_TOKEN':

      default:
        return AuthError.error;
    }*/

    return returnException(exception.code);
  }

  AuthError returnException(String exception) {
    switch (exception) {
      case 'invalid-email':
        return AuthError.invalidEmail;
      case 'user-disabled':
        return AuthError.userDisabled;
      case 'user-not-found':
        return AuthError.userNotFound;
      case 'wrong-password':
        return AuthError.wrongPassword;
      case 'email-already-in-use':
      case 'account-exists-with-different-credential':
        return AuthError.emailAlreadyInUse;
      case 'invalid-credential':
        return AuthError.invalidCredential;
      case 'operation-not-allowed':
        return AuthError.operationNotAllowed;
      case 'weak-password':
        return AuthError.weakPassword;
      case 'object-not-found':
        return AuthError.object_not_found;
      case 'ERROR_MISSING_GOOGLE_AUTH_TOKEN':

      default:
        return AuthError.error;
    }
  }
}

class DeviceInfo {
  String deviceId = '';
  String deviceManufacturer = '';
}
