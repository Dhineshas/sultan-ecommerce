import 'package:animate_do/animate_do.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Constants.dart';
import 'MyColors.dart';

class CustomDialog extends StatelessWidget {
  final bool? isOkCancelDialog;
  final /*  title,*/ message, positiveText,negativeText;
  //final Image image;
  final VoidCallback? positiveClick;
  final VoidCallback? negativeClick;

  CustomDialog({
    /* @required this.title,*/
    this.isOkCancelDialog=false,
    @required this.message,
    @required this.positiveText,
    this.negativeText='',
    @required this.positiveClick,
    @required this.negativeClick,
   // this.image,
  });

  Widget build(BuildContext context) {
    return FadeIn(child:  Dialog(
     /* shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),*/
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),


    ));
  }

  dialogContent(BuildContext context) {
    return Container(

      decoration: MyDecorations.decoration1(),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(padding: EdgeInsets.only(left: 30,right: 30,bottom: 20,top: 40),child: Text(
            message,
            style: MyStyles.black0E__HR_16,
          ),),
          Container(
            height: 20,
          ),
    /*ButtonTheme(
            height: 45,
            minWidth: 100,
            child: MaterialButton(
              onPressed:positiveClick,
              color: MyColors.white_chinese_e0_bg,
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(23.0),
                  side: BorderSide(color: MyColors.white_chinese_e0_bg)),
              child: Text(
                positiveText,
                style: MyStyles.grey_70__HR_16,
              ),
            ),
          )*/
          Row(mainAxisSize:MainAxisSize.max,mainAxisAlignment: MainAxisAlignment.spaceEvenly,crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                flex: 1,
                child: ButtonTheme(
                  padding: const EdgeInsets.all(0),
                  height: 46,
                  minWidth: 100,
                  child: MaterialButton(
                    onPressed:positiveClick,
                    color: MyColors.white_chinese_e0_bg,
                    shape: RoundedRectangleBorder(
                        borderRadius:isOkCancelDialog==true?
                        BorderRadius.only(bottomLeft:Radius.circular(13.0)):
                        BorderRadius.only(bottomLeft:Radius.circular(13.0),bottomRight:Radius.circular(13.0) ),
                        side: BorderSide(color: MyColors.white_chinese_e0_bg)),
                    child: Text(
                      positiveText,
                      style: MyStyles.grey_70__HR_16,
                    ),
                  ),
                )),
              isOkCancelDialog==true?Expanded(
                  flex: 1,
                  child: ButtonTheme(
                    padding: const EdgeInsets.all(0),
                    height: 46,
                    minWidth: 100,
                    child: MaterialButton(
                      onPressed:negativeClick,
                      color: MyColors.white_chinese_e0_bg,

                      shape: RoundedRectangleBorder(

                          borderRadius: BorderRadius.only(bottomRight:Radius.circular(13.0)),
                          side: BorderSide(color: MyColors.white_chinese_e0_bg)),
                      child: Text(
                        negativeText,
                        style: MyStyles.grey_70__HR_16,
                      ),
                    ),
                  ))
            :Container()],),
        ],
      ),
    );
  }
}
