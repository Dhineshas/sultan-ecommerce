import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:ecommerce/model/Project.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/widgets.dart' as pw;
import '../model/ProductItem.dart';

class CustomRow {
  final String itemName;
  final String itemQuantity;
  final String unitPrice;
  final String total;
  CustomRow(this.itemName, this.itemQuantity, this.unitPrice, this.total);
}
class PdfInvoiceService {

  static final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<File> createInvoice(List<ProductItem> productList/*,List<OrderedProduct> soldProducts*/,Project projectCurrent,bool isSingleProductInvoice) async
  {
    final pdf = pw.Document();

    final List<CustomRow> headElements = [
      CustomRow("Items", "Quantity", "Unit Price", "Total"),
      CustomRow("  \n", " \n ", "   \n", " \n")
    ];



    final List<CustomRow> elements = [
      for (var product in productList)
        CustomRow(
          product.name!,
          product.orderQuantity!.toStringAsFixed(2),
          product.sellingPrice!.toStringAsFixed(2),
          (product.sellingPrice! * product.orderQuantity!).toStringAsFixed(2),
        ),
    ];


    final List<CustomRow> footerElements = [
      CustomRow("  \n", " \n ", "   \n", " \n"),
      CustomRow("  \n", " \n ", "   \n", " \n"),
      CustomRow(
        "Sub Total",
        "",
        "",
        isSingleProductInvoice==true?"${(productList.first.sellingPrice! * productList.first.orderQuantity!).toStringAsFixed(2)}  QR":"${productList.first.orderGrossTotal} QR",
      ),
      CustomRow(
        "Delivery Charge",
        "",
        "",
        "${productList.first.orderDeliveryCharge} QR",
      ),
      CustomRow(
        "Discount Amount",
        "",
        "",
        "${productList.first.orderDiscount} QR",
      ),
      CustomRow(
        "Total Amount",
        "",
        "",
        isSingleProductInvoice==true?"${((productList.first.sellingPrice! * productList.first.orderQuantity!)+(productList.first.orderDeliveryCharge!)-(productList.first.orderDiscount!)).toStringAsFixed(2)}  QR":"${productList.first.orderNetTotal}  QR",
      )
    ];


    final image = ( /*await rootBundle.load("assets/images/starbucks_reflected_logo.jpeg")*/base64.decode(projectCurrent.imageBase!)).buffer.asUint8List();

    // final Container image = Container(child: ClipRRect(
    //   borderRadius: BorderRadius.circular(75),
    //   child: Image.memory(
    //     base64Decode('${projectCurrent.imageBase}'),
    //     width: 100,
    //     height: 100,
    //     fit: BoxFit.cover,
    //   ),
    // ),
    // );

    final orderIdDateFormat = DateFormat('ddMMMyyyyhhmmss');

    pdf.addPage(
        pw.MultiPage(
          // header:  _buildHeader ,
            build: (context) {
              return [
                pw.Row(
                    mainAxisAlignment: pw.MainAxisAlignment.center,
                    children: [
                      pw.Image(pw.MemoryImage(image), width: 100, height: 100, fit: pw.BoxFit.cover)
                    ]),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: pw.CrossAxisAlignment.start,
                  children: [
                    pw.Column(
                      // mainAxisAlignment: pw.MainAxisAlignment.start,
                      crossAxisAlignment: pw.CrossAxisAlignment.start,
                      children: [
                        pw.Text("Customer Name",textAlign: pw.TextAlign.left),
                        pw.Text("Customer Invoice No.",textAlign: pw.TextAlign.left),
                        // pw.Text("Customer City",textAlign: pw.TextAlign.left),
                      ],
                    ),
                    pw.Column(
                      // mainAxisAlignment: pw.MainAxisAlignment.start,
                      crossAxisAlignment: pw.CrossAxisAlignment.start,
                      children: [
                        pw.Text(_auth.currentUser?.providerData.first.displayName ?? _auth.currentUser?.providerData.first.email ?? "GUEST USER",textAlign: pw.TextAlign.right),
                        pw.Text("#Invoice${orderIdDateFormat.format(DateTime.fromMillisecondsSinceEpoch(productList.first.createdAt!))}",textAlign: pw.TextAlign.right)
                        // pw.Text("Max Weber",textAlign: pw.TextAlign.right),
                        // pw.Text("Weird Street Name 1",textAlign: pw.TextAlign.right),
                        // pw.Text("77662 Not my City",textAlign: pw.TextAlign.right),
                        // pw.Text("Vat-id: 123456",textAlign: pw.TextAlign.right),
                        // pw.Text("Invoice-Nr: 00001",textAlign: pw.TextAlign.right)
                      ],
                    )
                  ],
                ),
                pw.SizedBox(height: 50),
                pw.Text(
                    "Dear Customer, thanks for shopping from our ${projectCurrent.projectName}, Your items are shown below.For any queries with refund/return contact us."),
                pw.SizedBox(height: 25),
                itemColumn(headElements),
                itemColumn(elements),
                itemColumn(footerElements),
              ];
            })
    );

    final output = await getTemporaryDirectory();
    final file = File('${output.path}/inventory_report.pdf');
    await file.writeAsBytes(await pdf.save());
    return file;
    // return pdf.save();
  }

  pw.Column itemColumn(List<CustomRow> elements) {
    return pw.Column(
        children: [
          for (var element in elements)
            pw.Row(
                children: [
                  pw.Expanded(child: pw.Text(element.itemName, textAlign: pw.TextAlign.left)),
                  pw.Expanded(child: pw.Text(element.itemQuantity, textAlign: pw.TextAlign.right)),
                  pw.Expanded(child: pw.Text(element.unitPrice, textAlign: pw.TextAlign.right)),
                  pw.Expanded(child: pw.Text(element.total, textAlign: pw.TextAlign.right)),
                ])
        ]);
  }


  Future<void> savePdfFile(String fileName, Uint8List byteList) async {
    final output = await getTemporaryDirectory();
    var filePath = "${output.path}/$fileName.pdf";
    final file = File(filePath);
    await file.writeAsBytes(byteList);
    // await OpenDocument.openDocument(filePath: filePath);
  }



}