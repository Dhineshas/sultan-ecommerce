import 'dart:convert';
import 'dart:io';

import 'package:ecommerce/database/firestore_operations.dart';
import 'package:ecommerce/model/UserCredentials.dart';
import 'package:ecommerce/utils/Subscription.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import '../database/iap_operation.dart';
import '../model/IAPPurchaceData.dart';
import 'Utils.dart';

class InAppPurchaseHandler
{
  static const platform = MethodChannel('samples.flutter.dev/iap');
  static var iapOperation =  IAPOperations();
  static var fireStoreOperations = FireStoreOperations();
  static bool isIapPurchased = false;

  //going to native display to list all IAP
  static Future<bool> handleInAppPurchase(BuildContext context,) async
  {
    var standardRemainingDays = -1;
    var premiumRemainingDays = -1;
    final subList = await FireStoreOperations().getFireStoreSubscriptionDocument(FirebaseAuth.instance!.currentUser!.email.toString());
    subList.forEach((element) async {
      if(element.productId!.contains('standard') && element.purchaseStatus == 'purchased')
        {
          standardRemainingDays = await getDateDifference(element.purchaseDateEnding);
          print('${element.productId} == sss === $standardRemainingDays');
        }
      if(element.productId!.contains('premium') && element.purchaseStatus == 'purchased')
      {
        premiumRemainingDays = await getDateDifference(element.purchaseDateEnding);
        print('${element.productId} == ppp === $premiumRemainingDays');
      }
    });
    if (Platform.isAndroid)
    {
      if (await checkInternetConnectivity())
        try {
          await userOperations.getLastLoggedInUser().then((user) async {

            if (user.isNotEmpty) {
              if (user.isNotEmpty && FirebaseAuth.instance.currentUser!.providerData.first.email ==
                  user.first.userEmail) {
                final actSub =await Subscription.getActiveSubscription();
                final String result = await platform.invokeMethod('getInAppPurchase',{"activeSub" : actSub});
                print("data__ ${result}");
                if (result.isNotEmpty) {
                  //convert the string to json.
                  final jsonResponse = jsonDecode(result);
                  if (jsonResponse['orderId'] != null) {
                    print("data2__ ${jsonResponse['orderId']}   ${jsonResponse['purchaseToken']}");
                    isIapPurchased=true;
                    await insertOrUpdatesSubscriptionANDROID(context, jsonResponse, user.first);

                  }
                }
              } else
                showCustomCallbackAlertDialog(context: context,
                    msg: 'Something went wrong. Please login again',
                    positiveClick:await logoutFunction(context),
                    negativeClick:await logoutFunction(context));
            } else
              showCustomAlertDialog(context, 'Please login to continue');
          });
        } on PlatformException catch (e) {
          print("Failed to get ${e.message}");
        }
      else
        showCustomAlertDialog(
            context, 'Please check your internet connectivity and try again');

    }
    else
    {
      if (await checkInternetConnectivity())
      {
        try {
          await userOperations.getLastLoggedInUser().then((user) async
          {
            if (user.isNotEmpty)
            {
              if (user.isNotEmpty && FirebaseAuth.instance.currentUser!.providerData.first.email == user.first.userEmail)
              {
                final  iapAvailabilityCheckplatform = MethodChannel('navigationToIos');
                final actSub =await Subscription.getActiveSubscription();
                    // .then((value) =>
                    // print('new actsub = $value'));
                try
                {

                  final String result = await iapAvailabilityCheckplatform.invokeMethod('iapNativeView.xcode', {"isTableUpdate" : true,"activeSub" : actSub,"standard" : standardRemainingDays, "premium" : premiumRemainingDays});

                  print("\n\n\ncame back from NATIVE After new  purchase is\n$result.");
                  if(result.isNotEmpty)
                  {
                    //convert the string to json.
                    var purchases = <dynamic>[];
                    purchases = jsonDecode(result);
                    if (purchases.isNotEmpty)
                    {
                      isIapPurchased=true;
                      print("purchace nnnnnnnn count ${purchases.length}");
                      for (var element in purchases){
                        await insertOrUpdatesSubscriptionIOS(context,element,user.first);
                      }
                    }
                  }
                  else
                  {
                    await iapOperation.updateAlliosIAPDataToExpired('expired')
                        .then((value) => showCustomAlertDialog(
                        context, 'set user all purchase status to expired'));
                    return;
                  }
                }
                on PlatformException catch (e)
                {
                  print("Failed came back from NATIVE: '${e.message}'.");
                }
              }
              else
                showCustomCallbackAlertDialog(context: context,
                    msg: 'Something went wrong. Please login again',
                    positiveClick:await logoutFunction(context),
                    negativeClick: await logoutFunction(context));
            } else
              showCustomAlertDialog(context, 'Please login to continue');
          });
        } on PlatformException catch (e) {
          print("Failed to get ${e.message}");
        }
      }
      else
      {
        showCustomAlertDialog(
            context, 'Please check your internet connectivity and try again');
      }
    }
    return isIapPurchased;
  }

  //getting from respective stores account.
  static Future getActiveInAppPurchasesList(BuildContext context,) async
  {
    if (Platform.isAndroid)
    {
      if (await checkInternetConnectivity()) {
        try {
          await userOperations.getLastLoggedInUser().then((user) async {
            if (user.isNotEmpty) {
              if (user.isNotEmpty &&
                  FirebaseAuth.instance.currentUser!.providerData.first.email ==
                      user.first.userEmail) {
                final String? result = await platform.invokeMethod('activeInAppPurchases');
                print("data__ ${result}");
                //set user all purchase status to expired(3)
                //0 to 2 are the purchase status from google
                //0.purchased
                //1.canceled
                //2.pending
                //3.expired
                if(result!=null)
                  await iapOperation.updateAllIAPDataToExpired('expired').then((value) async {
                    if (result.isNotEmpty && result != '[]') {
                      //convert the string to json.
                      var purchases = <dynamic>[];
                      purchases = jsonDecode(result);


                      if (purchases.isNotEmpty) {
                        for (var element in purchases) {
                          await insertOrUpdatesSubscriptionANDROID(context, element,
                              user.first);
                        }
                        await showCustomAlertDialog(context, 'Successfully restored the previous purchase.');
                      }
                    }else
                      await showCustomAlertDialog(context, 'No previous purchase found.');
                  });
                /*}else if(result=='[]'){
                  //set user all purchase status to expired(3)
                  //0 to 2 are the purchase status from google
                  //0.purchased
                  //1.canceled
                  //2.pending
                  //3.expired
                  await iapOperation.updateAllIAPDataToExpired(3)
                      .then((value) => showCustomAlertDialog(
                      context, 'set user all purchase status to expired'));
                  return;
                }*/
              } else
                showCustomCallbackAlertDialog(context: context,
                    msg: 'Something went wrong. Please login again',
                    positiveClick: await logoutFunction(context),
                    negativeClick: await logoutFunction(context));
            } else
              showCustomAlertDialog(context, 'Please login to continue');
          });
        }
        on PlatformException catch (e) {
          print("Failed to get ${e.message}");
        }
      }
      else
      {
        try {
          await userOperations.getLastLoggedInUser().then((user) async {
            if (user.isNotEmpty) {
              await iapOperation.getAllPurchase().then((purchases) async {
                for (var element in purchases) {
                  final dateDifference = await getDateDifference(
                      element.purchaseDateEnding);
                  if (dateDifference < 0) {
                    //0 to 2 are the purchase status from google
                    //0.purchased
                    //1.canceled
                    //2.pending
                    //3.expired
                    element.purchaseStatus = 'expired';
                    await iapOperation.updateIAPDataStatus(element);
                  }
                }
              });
            }
          });
        } on PlatformException catch (e) {
          print("Failed to get ${e.message}");
        }
      }
    }
    else
    {
      {
        if (await checkInternetConnectivity())
        {
          try {
            await userOperations.getLastLoggedInUser().then((user) async {
              if (user.isNotEmpty) {
                if(user.isNotEmpty&&FirebaseAuth.instance.currentUser!.providerData.first.email==user.first.userEmail)
                {
                  final  iapAvailabilityCheckplatform = MethodChannel('xpressPos.storeListCheck');
                  final String result = await iapAvailabilityCheckplatform.invokeMethod('purchacedRecieptCheck',{"isTableUpdate" : false});

                  print("\n\n\ncame back from NATIVE is get purchacedRecieptCheck List \n$result.");
                  if (result.isNotEmpty)//&&result != '[]')
                      {
                    //convert the string to json.
                    var purchases = <dynamic>[];
                    purchases = jsonDecode(result);
                    if (purchases.isNotEmpty) {
                      print("purchace mmmmmmm count ${purchases.length}");
                      for (var element in purchases){
                        await insertOrUpdatesSubscriptionIOS(context,element,user.first);
                      }
                      await showCustomAlertDialog(context, 'Successfully restored the previous purchase.');
                    }
                  }
                  // else// if(result=='[]')
                  //     {
                  //   //set user all purchase status to expired(3)
                  //   //0 to 2 are the purchase status from google
                  //   //0.purchased
                  //   //1.canceled
                  //   //2.pending
                  //   //3.expired
                  //   await iapOperation.updateAlliosIAPDataToExpired('expired')
                  //       .then((value) => showCustomAlertDialog(
                  //       context, 'No previous purchase found.'));
                  //   return;
                  // }
                }else  showCustomCallbackAlertDialog(context: context,msg: 'Something went wrong. Please login again',positiveClick:await logoutFunction(context) ,negativeClick:await logoutFunction(context));

              }else
                showCustomAlertDialog(context, 'Please login to continue');
            });
          }
          on PlatformException catch (e)
          {
            print("Failed to get ${e.message}");
          }
        }
        else {
          try {
            await userOperations.getLastLoggedInUser().then((user) async {
              if (user.isNotEmpty) {
                await iapOperation.getAllPurchase().then((
                    purchases) async {
                  for (var element in purchases) {
                    final dateDifference = await getDateDifference(
                        element.purchaseDateEnding);
                    if (dateDifference < 0) {
                      //0 to 2 are the purchase status from google
                      //0.purchased
                      //1.canceled
                      //2.pending
                      //3.expired
                      element.purchaseStatus = 'expired';
                      await iapOperation.updateIosIAPDataStatus(element);
                    }
                  }
                });
              }
            });
          } on PlatformException catch (e) {
            print("Failed to get ${e.message}");
          }
        }
      }

    }
  }
  static Future insertOrUpdatesSubscriptionIOS(BuildContext context,element,UserCredentials user)async
  {
    var iapPurchaseData = IAPPurchaseData();
    iapPurchaseData.purchaseId = element['transaction_id'];

    if(element['product_id'].toString().contains('com.expresspos.premium.Auto_Renewing_Subscription')){
      iapPurchaseData.productId = 'premium';
    }
    else if(element['product_id'].toString().contains('com.expresspos.standard.Non_Renewing_Subscription')){
      iapPurchaseData.productId = 'standard';
    }
    else
    {
      iapPurchaseData.productId = '';
    }
    // iapPurchaseData.productId = element['product_id'];
    iapPurchaseData.userId = user.id;
    //for ios: data and android : token
    iapPurchaseData.purchaseVerificationDataOrToken = element['original_transaction_id'];
    iapPurchaseData.purchaseDevice = await getDeviceName();
    iapPurchaseData.purchaseSource = 'apple.com';

    iapPurchaseData.purchaseDateStarting = int.tryParse(element['purchase_date_ms']);
    iapPurchaseData.purchaseTransactionDate = int.tryParse(element['purchase_date_ms']);

    print('purchased date ms = ${iapPurchaseData.purchaseTransactionDate}    && ${int.tryParse(element['purchase_date_ms'])}');
    //auto renewing 1 year
    if (iapPurchaseData.productId!.contains( 'premium'))
    {
      iapPurchaseData.purchaseDateEnding = await getSubscriptionEndByAddingMonths( int.tryParse(element['purchase_date_ms'])!, 12);
    }
    //non auto renewing 3 months
    else if (iapPurchaseData.productId!.contains( 'standard'))
    {
      iapPurchaseData.purchaseDateEnding =await getSubscriptionEndByAddingMonths( int.tryParse(element['purchase_date_ms'])!, 3);
    }
    //life time
    else if (iapPurchaseData.productId!.contains( 'non_consumable'))
    {
      iapPurchaseData.purchaseDateEnding = DateTime.now().millisecondsSinceEpoch;
    }


    iapPurchaseData.purchaseStatus = 'purchased';


    // case purchasing = 0 // Transaction is being added to the server queue.
    // case purchased = 1 // Transaction is in queue, user has been charged.  Client should complete the transaction.
    // case failed = 2 // Transaction was cancelled or failed before being added to the server queue.
    // case restored = 3 // Transaction was restored from user's purchase history.  Client should complete the transaction.
    // case deferred = 4 //
    switch (element['purchaseStatus'])
        {
      case 0:
        iapPurchaseData.purchaseStatus = 'purchasing';
        break;
      case 1:
        iapPurchaseData.purchaseStatus = 'purchased';
        break;
      case 2:
        iapPurchaseData.purchaseStatus = 'failed';
        break;
      case 3:
        iapPurchaseData.purchaseStatus = 'restored';
        break;
      case 4:
        iapPurchaseData.purchaseStatus = 'deferred';
        break;
    }

    switch (iapPurchaseData.productId!.contains( 'premium'))
    {
      case true:
        iapPurchaseData.purchaseIsAutoRenewing = 1;
        break;
      case false:
        iapPurchaseData.purchaseIsAutoRenewing = 0;
        break;
    }

    print("\n\n\nall data is before updating sub to firestore ${iapPurchaseData.toString()}");


    /*final isExist = await iapOperation.isProductIDExistForSource(iapPurchaseData);
    if (isExist)
    {
      print("\n\n\n isExist true iapOperation.isProductIDExistForSource");
      await iapOperation.updateIAPDataByProductId(iapPurchaseData);
      //     .then((value) =>
      //     showCustomAlertDialog(context, 'Purchase updated Successfully')
      // );
      // return;
    }
    else
    {
      print("\n\n\n isExist false iapOperation.isProductIDExistForSource");
      await iapOperation.insertIAPdata(iapPurchaseData);
      //     .then((value) async {
      //   showCustomAlertDialog(context, 'Purchase inserted Successfully');
      // });
      // return;
    }*/

    iapPurchaseData.userEmail=user.userEmail;

    await fireStoreOperations.addFireStoreSubscription(iapPurchaseData).then((value) async{



      await insertOrUpdateUserIapPurchaseData(iapPurchaseData);
      /*final isExist = await iapOperation
       .isProductIDExistForSource(iapPurchaseData);
   if (isExist) {
     await iapOperation.updateIAPDataByProductId(
         iapPurchaseData)
         .then((value) async{ });
   } else {
     await iapOperation.insertIAPdata(iapPurchaseData)
         .then((value) async {

     });
   }*/

    });
  }
  static Future insertOrUpdatesSubscriptionANDROID(BuildContext context,element, UserCredentials user)async{
    var iapPurchaseData = IAPPurchaseData();
    iapPurchaseData.purchaseId = element['orderId'];

    if(element['productId'].toString().contains('com.expresspos.premium.auto_renewing')){
      iapPurchaseData.productId = 'premium';
    }
    else if(element['productId'].toString().contains('com.expresspos.standard.non_renewing')){
      iapPurchaseData.productId = 'standard';
    }else{
      iapPurchaseData.productId = '';
    }
    //iapPurchaseData.productId = element['productId'];
    iapPurchaseData.userId = user.id;
    //for ios: data and android : token
    iapPurchaseData.purchaseVerificationDataOrToken =
    element['purchaseToken'];
    iapPurchaseData.purchaseDevice = await getDeviceName();
    iapPurchaseData.purchaseSource = 'google.com';

    iapPurchaseData.purchaseDateStarting =
    element['purchaseTime'];
    iapPurchaseData.purchaseTransactionDate =
    element['purchaseTime'];
    //auto renewing 12 months
    if (iapPurchaseData.productId!.contains(
        'premium')) {
      iapPurchaseData.purchaseDateEnding =
      await getSubscriptionEndByAddingMonths(
          element['purchaseTime'], 12);
    }
    //non  renewing 3 months (prepaid)
    else if (iapPurchaseData.productId!.contains(
        'standard')) {
      iapPurchaseData.purchaseDateEnding =
      await getSubscriptionEndByAddingMonths(
          element['purchaseTime'], 3);

    }
    //print('  ${element['purchaseTime']} ${iapPurchaseData.purchaseDateEnding}');
    //0 to 2 are the purchase status from google
    //0.purchased
    //1.canceled
    //2.pending
    //3.expired
    switch (element['purchaseState']) {
      case 0:
        iapPurchaseData.purchaseStatus = 'purchased';
        break;
      case 1:
        iapPurchaseData.purchaseStatus = 'canceled';
        break;
      case 2:
        iapPurchaseData.purchaseStatus = 'pending';
        break;
      default:
        iapPurchaseData.purchaseStatus = element['purchaseState'].toString();
    }

    switch (element['autoRenewing']) {
      case true:
        iapPurchaseData.purchaseIsAutoRenewing = 1;
        break;
      case false:
        iapPurchaseData.purchaseIsAutoRenewing = 0;
        break;
    }

    iapPurchaseData.userEmail=user.userEmail;

    await fireStoreOperations.addFireStoreSubscription(iapPurchaseData).then((value) async{



      await insertOrUpdateUserIapPurchaseData(iapPurchaseData);
      /*final isExist = await iapOperation
       .isProductIDExistForSource(iapPurchaseData);
   if (isExist) {
     await iapOperation.updateIAPDataByProductId(
         iapPurchaseData)
         .then((value) async{ });
   } else {
     await iapOperation.insertIAPdata(iapPurchaseData)
         .then((value) async {

     });
   }*/

    });
  }
  static  Future<int> getSubscriptionEndByAddingMonths(int subscribedDate,int monthsAdd)
  async{
    final subDate = DateTime.fromMillisecondsSinceEpoch(subscribedDate);
    final lastingDate = DateTime(subDate.year, subDate.month + monthsAdd, subDate.day);
    print('Subscribed Ending Months = $lastingDate');

    return lastingDate.millisecondsSinceEpoch;
  }

  static  Future<int> getDateDifference(int? subscribeEndDate)
  async
  {

    final currentDate =  DateTime.now();
    final newSubscribedDate = DateTime.fromMillisecondsSinceEpoch(subscribeEndDate!, isUtc:true);
    final difference = newSubscribedDate.difference(currentDate).inDays;
    print('difference btw days = ${difference}');
    return difference;
  }

  static Future updateFireStoreSubscriptionToLocalDb(String userEmail)async{
    await fireStoreOperations.getFireStoreSubscriptionDocument(userEmail).then((iapPurchaseData) async{
      await iapOperation.updateAllIAPDataToExpired('expired').then((value)async{
      if(iapPurchaseData.isNotEmpty){

        for (var element in iapPurchaseData){
          element.userEmail=userEmail;
          await insertOrUpdateUserIapPurchaseData(element);
        }

      }}); });
  }

  static Future insertOrUpdateUserIapPurchaseData(IAPPurchaseData element)async{

    if(element.purchaseStatus=='purchased'){
      final dateDifference = await getDateDifference(element.purchaseDateEnding);
      print("ddddaysss = $dateDifference");
      if (dateDifference < 0) {
        element.purchaseStatus='expired';
       await fireStoreOperations.updateFireStoreSubscriptionStatusToExpired(element);

      }
    }

    final isExist = await iapOperation
        .isProductIDExistForSource(element);
    if (isExist) {
      await iapOperation.updateIAPDataByProductId(
          element)
          .then((value) async{
      });
    } else {
      await iapOperation.insertIAPdata(element)
          .then((value) async {

      });
    }
  }
}