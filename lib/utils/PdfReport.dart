import 'dart:io';

import 'package:ecommerce/utils/MyColors.dart';
import 'package:path_provider/path_provider.dart';

import '../database/inventory_item_operations.dart';
import '../inventoryUnitMeasures.dart';
import '../model/InventoryItem.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

class PdfReport {

  //var inventoryItemOperations = InventoryItemOperations();
 static Future<File> generateDocument(int? projectID, int currentYear) async {
    var items = <InventoryItem>[];
    var inventoryCategory = <String>[];

    //var tempBoxItems = <InventoryItem>[];

    await InventoryItemOperations().inventoriesItemsWithCategory(projectID,currentYear).then((value) {
      value.sort((a, b) => a.name!.compareTo(b.name!));
      /*int? id;
      int? piecesInType;
      value.forEach((element) {

          if(id!=null&&element.itemType==0&&element.id==id&&piecesInType!=null&&piecesInType==element.historyInventoryItemPiecesInType){
          tempBoxItems.add(element);
          print('item_type_box   ${element.id}  ${element.name}  ${element.historyInventoryItemPiecesInType}');
        }
        id=element.id;
        piecesInType=element.historyInventoryItemPiecesInType;
        
      });

      value.removeWhere((item) => tempBoxItems.contains(item));*/

      items=value;

    });
    for (var inventoryItem in items) {
      if(!inventoryCategory.contains(inventoryItem.inventoryName)){
        inventoryCategory.add(inventoryItem.inventoryName.toString());
        inventoryCategory.sort((a, b) => a.compareTo(b));
      }

    }



    final pw.Document doc = pw.Document();

    doc.addPage(pw.MultiPage(
        pageFormat:PdfPageFormat.a4,
        crossAxisAlignment: pw.CrossAxisAlignment.start,
        header: (pw.Context context) {
          return pw.Container(
              height: 20,
              alignment: pw.Alignment.centerRight,
              margin: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
              padding: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
              decoration: const pw.BoxDecoration(
                border: pw.Border(top: pw.BorderSide(color: PdfColors.amber100,),bottom:pw.BorderSide(color: PdfColors.amber100,),right: pw.BorderSide(color: PdfColors.amber100,),left: pw.BorderSide(color: PdfColors.amber100,) ),),
              child: pw.Text('Inventory Report ',

                  style: pw.Theme.of(context)
                      .defaultTextStyle
                      .copyWith(color: PdfColors.grey)));
        },
        build: (pw.Context context) => <pw.Widget>[




          pw.Table.fromTextArray(
              defaultColumnWidth: pw.FixedColumnWidth(10.0),
              cellHeight: 25,
              context: context,
              border:pw.TableBorder(
                verticalInside: pw.BorderSide(color: PdfColors.white,),horizontalInside: pw.BorderSide(color: PdfColors.white,),right: pw.BorderSide(color: PdfColors.white,),left: pw.BorderSide(color: PdfColors.white,),top: pw.BorderSide(color: PdfColors.white,),bottom: pw.BorderSide(color: PdfColors.white,),
              ),data: [<String>['Name', 'Quantity or Items/Box', 'Available stock'],]),

          for (int i = 0; i < inventoryCategory.length; i++)
            pw.Column(
              children: [
                pw.Padding(padding: const pw.EdgeInsets.all(5)),
                pw.Container(
                    height: 20,
                    alignment: pw.Alignment.centerLeft,child: pw.Text(inventoryCategory.elementAt(i),textAlign: pw.TextAlign.left)),

                pw.Table.fromTextArray(
                    defaultColumnWidth: pw.FixedColumnWidth(10.0),
                    cellHeight: 25,
                    context: context,
                    border:pw.TableBorder(
                      verticalInside: pw.BorderSide(color: PdfColors.grey,),horizontalInside: pw.BorderSide(color: PdfColors.grey,),right: pw.BorderSide(color: PdfColors.grey,),left: pw.BorderSide(color: PdfColors.grey,),top: pw.BorderSide(color: PdfColors.grey,),bottom: pw.BorderSide(color: PdfColors.grey,),
                    ),

                    headerAlignment: pw.Alignment.centerLeft,
                    data: <List<dynamic>>[

                      //<String>[inventoryCategory.elementAt(i)],



                      for (int j = 0; j < items.length; j++)
                        if(inventoryCategory.elementAt(i)==items.elementAt(j).inventoryName)
                          if(items.elementAt(j).itemType==0)
                            if(j+1<items.length&&items.elementAt(j).id!=items.elementAt(j+1).id||j==items.length-1)
                              <String>['${items.elementAt(j).name}', items.elementAt(j).isPiecesInTypeUpdated==0? '${items.elementAt(j).historyInventoryItemPiecesInType} ${InventoryUnitMeasure.inventoryUnitMeasureArray[items.elementAt(j).itemType!]} \'s':'${items.elementAt(j).historyInventoryItemPiecesInType} Items per ${InventoryUnitMeasure.inventoryUnitMeasureArray[items.elementAt(j).itemType!]}', '${items.elementAt(j).stock} Items']

                            else
                                    <String>['${items.elementAt(j).name}', items.elementAt(j).isPiecesInTypeUpdated==0? '${items.elementAt(j).historyInventoryItemPiecesInType} ${InventoryUnitMeasure.inventoryUnitMeasureArray[items.elementAt(j).itemType!]} \'s':'${items.elementAt(j).historyInventoryItemPiecesInType} Items per ${InventoryUnitMeasure.inventoryUnitMeasureArray[items.elementAt(j).itemType!]}']

                          else
                            <String>['${items.elementAt(j).name}', '${items.elementAt(j).historyInventoryItemTotalType} ${InventoryUnitMeasure.inventoryUnitMeasureArray[items.elementAt(j).itemType!]} \'s','${items.elementAt(j).stock} ${InventoryUnitMeasure.inventoryUnitMeasureArray[items.elementAt(j).itemType!]} \'s'],



                    ]

                )





                /*for (int j = 0; j < items.length; j++)
                  if(inventoryCategory.elementAt(i)==items.elementAt(j).inventoryName)
                    if(items.elementAt(j).itemType==0)





 pw.Column(
   children: [
     pw.Table.fromTextArray(
         defaultColumnWidth: pw.FixedColumnWidth(10.0),
         cellHeight: 25,
         context: context,
         border:pw.TableBorder(
           verticalInside: pw.BorderSide(color: PdfColors.grey,),horizontalInside: pw.BorderSide(color: PdfColors.grey,),right: pw.BorderSide(color: PdfColors.grey,),left: pw.BorderSide(color: PdfColors.grey,),top: pw.BorderSide(color: PdfColors.grey,),bottom: pw.BorderSide(color: PdfColors.grey,),
         ),

         headerAlignment: pw.Alignment.centerLeft,
         data: <List<dynamic>>[
           <String>['${items.elementAt(j).name}', items.elementAt(j).isPiecesInTypeUpdated==0? '${items.elementAt(j).historyInventoryItemPiecesInType} ${InventoryUnitMeasure.inventoryUnitMeasureArray[items.elementAt(j).itemType!]} \'s':'${items.elementAt(j).historyInventoryItemPiecesInType} Items per ${InventoryUnitMeasure.inventoryUnitMeasureArray[items.elementAt(j).itemType!]}']

         ]

     ),
     if(j+1<items.length&&items.elementAt(j).id!=items.elementAt(j+1).id||j==items.length-1)
     pw.Container(
         height: 20,
         alignment: pw.Alignment.centerRight,child: pw.Text('Available Stock ${items.elementAt(j).stock}',textAlign: pw.TextAlign.left))
     *//*else if(j==items.length-1)
       pw.Container(
           height: 20,
           alignment: pw.Alignment.centerRight,child: pw.Text('bbbbb',textAlign: pw.TextAlign.left))*//*

   ])
                else

                      pw.Table.fromTextArray(

                          defaultColumnWidth: pw.FixedColumnWidth(10.0),
                          cellHeight: 25,
                          context: context,
                          border:pw.TableBorder(
                            verticalInside: pw.BorderSide(color: PdfColors.grey,),horizontalInside: pw.BorderSide(color: PdfColors.grey,),right: pw.BorderSide(color: PdfColors.grey,),left: pw.BorderSide(color: PdfColors.grey,),top: pw.BorderSide(color: PdfColors.grey,),bottom: pw.BorderSide(color: PdfColors.grey,),
                          ),

                          headerAlignment: pw.Alignment.centerLeft,
                          data: <List<dynamic>>[
                            <String>['${items.elementAt(j).name}', '${items.elementAt(j).stock} ${InventoryUnitMeasure.inventoryUnitMeasureArray[items.elementAt(j).itemType!]} \'s'],
                          ]

                      )*/

              ],
            )

          ,

          pw.Paragraph(text: ""),
          pw.Padding(padding: const pw.EdgeInsets.all(10)),

        ]));
    final output = await getTemporaryDirectory();
    final file = File('${output.path}/inventory_report.pdf');
    await file.writeAsBytes(await doc.save());
    return file;
    //return doc.save();
  }

}