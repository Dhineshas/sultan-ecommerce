import 'package:flutter/material.dart';

class MyColors {
  static const white = Colors.white;
  static const black = Colors.black;
  //static const transparent = Colors.transparent;
  static const transparent = Color(0xFF00FFFFFF);
  static const black_0101 = Color(0xFF010101);
  static const white_f1_bg = Color(0xFFF1F1F1);
  static const white_chinese_e0_bg = Color(0xFFE0EADF);
  static const platinum_e4_bg = Color(0xFFE4E2EC);
  static const grey_bright_ef_bg = Color(0xFFEFE5ED);
  static const white_dutch_ec_bg = Color(0xFFECDEC1);
  static const platinum_ec_bg = Color(0xFFECE4E5);
  static const red_ff_bg = Color(0xFFFF5353);
  static const grey_9d_bg = Color(0xFF9D9D9D);
  static const blue40 = Color(0xff40B9E5);
  static const grey_70 = Color(0xFF707070);
  static const grey_607 = Color(0x2F707070);
  static const black0E = Color(0xFF0E0E0E);
  static const white_f8 = Color(0xFFF8F7F8);
  static const yellow_ff = Color(0xFFFFCB66);
  static const green_8c = Color(0xFF8cb47d);


}