import 'dart:async';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:ecommerce/database/iap_operation.dart';
import 'package:ecommerce/model/IAPPurchaceData.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:in_app_purchase_storekit/in_app_purchase_storekit.dart';
import 'package:in_app_purchase_android/billing_client_wrappers.dart';
import 'package:in_app_purchase_android/in_app_purchase_android.dart';
import 'package:in_app_purchase_storekit/store_kit_wrappers.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'dart:io';

import 'package:intl/intl.dart';

import 'database/user_operation.dart';


const String _kNonConsumableId = 'com.expresspos.projectcreation.non_consumable';                  //Non Consumable
const String _kSilverSubscriptionId1 = 'com.expresspos.projectcreation.Non_Renewing_Subscription'; //Non Auto Renewable Subscription
const String _kGoldSubscriptionId1 = 'com.expresspos.projectcreation.Renewing_Subscription_2_year'; //Auto Renewable Subscription


const String _kIdNonConsumable = 'com.expresspos.projectcreation.non_consumable';                  //Non Consumable
const String _kIdPrepaid = 'com.exprepos.project.n_r_s';
const String _kIdPrepaid2 = 'com.exprepos.project.n_r_s_2';

/*const String _kIdPrepaidbase = 'pos-prepaid-3-months';
const String _kIdAutoRenewingbase = 'pos-auto-renewing-6-months';*/


const List<String> _iOSkProductIds = <String>[
  _kNonConsumableId,
  _kSilverSubscriptionId1,
  _kGoldSubscriptionId1,
];
const List<String> _androidKProductIds = <String>[
  _kIdNonConsumable,
 // _kIdPrepaidbase,
  _kIdPrepaid,
  _kIdPrepaid2
 // _kIdAutoRenewingbase
];


class IAPProductList extends StatefulWidget {
  const IAPProductList({Key? key}) : super(key: key);

  @override
  State<IAPProductList> createState() => _IAPProductListState();
}

class _IAPProductListState extends State<IAPProductList>with WidgetsBindingObserver
{

  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  var _scrollController = ScrollController();

  final InAppPurchase _inAppPurchase = InAppPurchase.instance;
  StreamSubscription<List<PurchaseDetails>>? _subscription;
  List<String> _notFoundIds = <String>[];
  List<ProductDetails> _productsList = <ProductDetails>[];
  List<PurchaseDetails> _purchases = <PurchaseDetails>[];

  bool _isAvailable = false;
  bool _loading = true;

  var userOperations = UserOperations();
  var iapOperation = IAPOperations();

  List<IAPPurchaseData> iapDataList = <IAPPurchaseData>[];//purchased local database
  final subscribedDateFormat = DateFormat('dd-MMM-yyyy');

  @override
  void initState() {

    checkIAPDatabase();
    checkInAppPurchase();

    final Stream<List<PurchaseDetails>> purchaseUpdated = _inAppPurchase.purchaseStream;
    _subscription =  purchaseUpdated.listen((purchaseDetailsList)
    {
      _listenToPurchaseUpdated(purchaseDetailsList);
    },
      onDone: ()
      {
        _subscription?.cancel();
      },
      onError: (error)
      {
        print('initstate listen onError $error');
        showCustomAlertDialog(context, 'Error found while listening ${error.toString()}');
      },
    );
    super.initState();
  }
  Future<void> initStoreInfo() async {
    if (Platform.isIOS) {
      var iosPlatformAddition = _inAppPurchase
          .getPlatformAddition<InAppPurchaseStoreKitPlatformAddition>();
      await iosPlatformAddition.setDelegate(ExamplePaymentQueueDelegate());
    }
  }

  @override Future<void> dispose()async
  {
    if (Platform.isIOS)
    {
      final InAppPurchaseStoreKitPlatformAddition iosPlatformAddition = _inAppPurchase.getPlatformAddition<InAppPurchaseStoreKitPlatformAddition>();
      iosPlatformAddition.setDelegate(ExamplePaymentQueueDelegate());
    }
    print('dispose');
    _subscription?.cancel();
    super.dispose();
  }

  int daysBtwDates(int? subscribedDate)
  {
    final subDateTEST = subscribedDateFormat.format(DateTime.fromMillisecondsSinceEpoch(subscribedDate!));
    final currentDate =DateTime.now();
    final new_subscribedDate = DateTime.fromMillisecondsSinceEpoch(subscribedDate, isUtc:true);
    final difference = new_subscribedDate.difference(currentDate).inDays;
    print('difference btw days = $difference');

    return difference;
  }
  int getSubscriptionEndByAddingMonths(int subscribedDate,int monthsAdd)
  {
    final subDate = DateTime.fromMillisecondsSinceEpoch(subscribedDate);
    final lastingdate = DateTime(subDate.year, subDate.month + monthsAdd, subDate.day);
    print('Subscribed Ending Months = $lastingdate');

    return lastingdate.millisecondsSinceEpoch;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('SUBSCRIPTION' , style: TextStyle(color: Colors.black),),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        actions: [
          IconButton(
              onPressed: () {
                showCircularLoader(context, 'Checking old purchases...');
                checkIAPDatabase().then((value) => value.isNotEmpty && value.length > 0 ? showCustomAlertDialog(context, 'purchase restoredD').then((value) => Navigator.of(context).pop()) : showStoreRestoreDialog());
                Navigator.of(context).pop();
                // _inAppPurchase.restorePurchases();
              },
              icon: Icon(Icons.restore)
          )
        ],
      ),
      body: SingleChildScrollView(
          child:
          _loading
              ?
          Card(child: ListTile(leading: CircularProgressIndicator(color: MyColors.white_chinese_e0_bg,), title: Text('Fetching products...')))
              :
          _isAvailable
              ?
          _productsList.isEmpty
              ?
          Center(child: Text('No products are found on store.'))
              :
          showListView()
              :
          Text('Store Not available now')
      ),
    );
  }
  ListView showListView() {
    return ListView.builder(
        physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
        key: _listKey,
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: _productsList.length,
        itemBuilder: (BuildContext context, int index) {
          return _buildListItem(context, _productsList[index], index);
        });
  }
  Widget _buildListItem(BuildContext context, ProductDetails values, int index) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: ListTile(
          hoverColor: MyColors.transparent,
          contentPadding: EdgeInsets.all(4),
          title: MyWidgets.shadowContainer(
              height: 120,
              paddingL: 10,
              // spreadRadius: 2,
              cornerRadius: 10,
              shadowColor: MyColors.white_f1_bg,
              color: MyColors.white_chinese_e0_bg,
              child: Row(
                children: [
                  Expanded(
                      flex: 6,
                      child: Container(
                          margin: EdgeInsets.only(top: 5, bottom: 5),
                          child: Column(
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Spacer(),
                              Text(
                                values.title,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: MyStyles.customTextStyle(
                                    fontSize: 17,
                                    color: MyColors.black,
                                    fontWeight: FontWeight.w500),
                                textAlign: TextAlign.left,
                              ),
                              Text(
                                values.description,
                                maxLines: 4,
                                overflow: TextOverflow.ellipsis,
                                style: MyStyles.customTextStyle(
                                    fontSize: 15,
                                    color: MyColors.black,
                                    fontWeight: FontWeight.w200),
                                textAlign: TextAlign.left,
                              ),
                              Spacer(),
                            ],
                          ))
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(4, 0, 10, 0),
                    child: Container(width: 2,height: 120,color: Colors.blueGrey.shade100,),
                  ),
                  showingProductStatus(values),
                ],
              )
          )
      ),
    );
  }

  Expanded showingProductStatus(ProductDetails values)
  {
    var prodDBPurchase = this.iapDataList.isNotEmpty ? this.iapDataList.lastWhere((element) => element.productId == values.id,orElse: ()=> IAPPurchaseData()) : IAPPurchaseData();
    if(prodDBPurchase.productId != null)
    {
      print("prodDBPurchase.product_id not null");
      return Expanded(
        flex: 4,
        child: Container(
          // width: 75,
          decoration: BoxDecoration(
            color: MyColors.white_chinese_e0_bg,
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(10), bottomRight: Radius.circular(10)),
          ),
          child: purchasedProdView(values,prodDBPurchase),
        ),
      );
    }
    else
    {
      print("prodDBPurchase.product_id ISS null");
      return Expanded(
        flex: 4,
        child: Container(
          // width: 75,
          decoration: BoxDecoration(
            color: MyColors.white_chinese_e0_bg,
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(10), bottomRight: Radius.circular(10)),
          ),
          child: notPurchasedProdView(values),
        ),
      );
    }
  }

  Widget purchasedProdView(ProductDetails productDetail,IAPPurchaseData iapData)
  {
    var subscribedText = 'Purchased Already';

    var result = 0;
    var iapDat = iapDataList.length != 0 ? iapDataList.lastWhere((element) => element.productId == productDetail.id) : null;
    if(iapDat!=null){
    result = daysBtwDates(int.tryParse(iapDat.purchaseDateEnding.toString()));
    print('prod det = ${productDetail.title}  && result = ${iapDat.purchaseDateEnding} AND IAP END DATE IS $result days');
    }

    if(result>=0)
    {
      subscribedText = '$result days remaining';
    }
    if(productDetail.id.contains('non_consumable'))
    {
      subscribedText = 'Lifetime Subscribed';
    }
    if(result<0 && productDetail.id.contains('non_consumable') == false)
    {
      //this means the subscription has expired.Show Buy again
      return notPurchasedProdView(productDetail);
    }
    return Text(
      subscribedText,
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
      style: MyStyles.customTextStyle(
          fontSize: 16, color: MyColors.blue40, fontWeight: FontWeight.w600),
      textAlign: TextAlign.center,
    );
  }
  Widget notPurchasedProdView(ProductDetails productDetail)
  {
    late PurchaseParam purchaseParam;
    if (Platform.isAndroid)
    {
      // NOTE: If you are making a subscription purchase/upgrade/downgrade, we recommend you to
      // verify the latest status of you your subscription by using server side receipt validation
      // and update the UI accordingly. The subscription purchase status shown
      // inside the app may not be accurate.
      // final GooglePlayPurchaseDetails? oldSubscription = _getOldSubscription(
      //                 values, purchases);
      //
      // purchaseParam = GooglePlayPurchaseParam(productDetails: productDetails, applicationUserName: null,
      //                 changeSubscriptionParam: (oldSubscription != null)
      //                     ?
      //                 ChangeSubscriptionParam(
      //                   oldPurchaseDetails: oldSubscription,
      //                   prorationMode: ProrationMode.immediateWithTimeProration,
      //                 )
      //                     :
      //                 null
      //             );
    }
    else
    {
      purchaseParam = PurchaseParam(productDetails: productDetail, applicationUserName: null,);
    }
    return InkWell(
      onTap: (){
        purchaseNonConsumableFunc(purchaseParam, productDetail);
      },
      child: Card(color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 3, 0, 3),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                productDetail.price,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: MyStyles.customTextStyle(
                    fontSize: 16, color: MyColors.black, fontWeight: FontWeight.w500),
                textAlign: TextAlign.center,
              ),
              Text(
                'Buy Now',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: MyStyles.customTextStyle(
                    fontSize: 16, color: MyColors.blue40, fontWeight: FontWeight.w500),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
  showStoreRestoreDialog()  {
     showCustomCallbackAlertDialog(
        context: context,
        positiveText: 'YES',
        negativeText: 'NO',
        msg: 'Your Database shows no purchases before.Do you want to Restore from Store?',
        positiveClick: ()async { await _inAppPurchase.restorePurchases().then((value) => Navigator.of(context).pop());},
        negativeClick: () {Navigator.of(context).pop();});
  }

  Future<List<IAPPurchaseData>> checkIAPDatabase() async
  {

    //final userName = FirebaseAuth.instance.currentUser!.providerData.first.email.toString();
    await userOperations.getLastLoggedInUser().then((user)async {

      if(user.isNotEmpty&&FirebaseAuth.instance.currentUser!.providerData.first.email==user.first.userEmail) {



    await iapOperation.getAllPurchaseByUserId(user.first.id).then((value)
    {
      if(value.isEmpty)
        {
          print('\npurchased DATABASE empty');
          showCustomAlertDialog(context, 'no purchase found in his DATA user name ${user.first.id} ${user.first.userEmail}');
        }
      else
        {
          //this.iapDataList = value;
          setState(() {
            this.iapDataList = value;
          });
          iapDataList.forEach((element)
          {
            print('\npurchased DATABASE items are ${element.purchaseId.toString()} ${element.productId}  ${element.purchaseStatus} ');
          });
          return iapDataList;
        }
    })
        .onError((error, stackTrace) {print('my error print is $error $stackTrace');context.showSnackBar('$error');});
      }});
    return iapDataList;
  }


  Future<void> checkInAppPurchase() async {
    Set<String> _kIds = Platform.isIOS ? _iOSkProductIds.toSet() : _androidKProductIds.toSet();

    final bool available = await _inAppPurchase.isAvailable();
    if (!available)
    {
      setState(() {
        _isAvailable = false;
      });

      // The store cannot be reached or accessed. Update the UI accordingly.
      showCustomAlertDialog(context, 'The store can\'t be reached or access.').then((value) => Navigator.of(context).pop());
      return;
    }
    setState(()
    {
      _isAvailable = true;
    });

    if (Platform.isIOS)
    {
      final InAppPurchaseStoreKitPlatformAddition iosPlatformAddition = _inAppPurchase.getPlatformAddition<InAppPurchaseStoreKitPlatformAddition>();
      await iosPlatformAddition.setDelegate(ExamplePaymentQueueDelegate());
    }
    try
    {
      final ProductDetailsResponse response = await _inAppPurchase.queryProductDetails(_kIds); // .timeout(const Duration(seconds: 20));

      setState(()
      {
        _loading = false;
      });
      if (response.notFoundIDs.isNotEmpty)
      {
        // Handle the error.
        print('TRY items are response.error');
        showCustomAlertDialog(context, 'Some products are not found on store, which are ${response.notFoundIDs}');
      }
      if (response.productDetails.length == 0)
      {
        print('items are Length is ${response.productDetails.length}');
        setState(()
        {
          this._productsList = <ProductDetails>[];
        });
        showCustomAlertDialog(context, 'No products has found on AppStore to purchase.');
      }
      if (response.productDetails.length > 0)
      {
        setState(()
        {
          this._productsList = response.productDetails;
        });
      }
      _productsList.forEach((element)
      {
        print('\nitems id is ${element.id}\nitems name is ${element.description}\nitems title is ${element.title}\nitems Price is = ${element.price}');
      });
    }
    on InAppPurchaseException catch (ee)
    {
      print('items are InAppPurchaseException $ee');
      showCustomAlertDialog(context, 'items are InAppPurchaseException ${ee.message}');
    }
    on PlatformException catch (ee)
    {
      print('items are PlatformException $ee');
      showCustomAlertDialog(context, 'items are PlatformException ${ee.message}');
    }
    catch (e)
    {
      print('Can\'t connect to respective Store ${e.toString()}');
      showCustomAlertDialog(context, 'Can\'t connect to respective Store ${e.toString()}').then((value) => Navigator.of(context).pop());
    }
  }

  Future<void> purchaseNonConsumableFunc(PurchaseParam purchaseParam, ProductDetails productDetails) async
  {
    try
    {
      showCircularLoader(context, 'started buying ${productDetails.title} ');
      await _inAppPurchase.buyNonConsumable(purchaseParam: purchaseParam);
    }
    on PlatformException catch (e)
    {
      Navigator.of(context).pop();
      if (e.code == 'storekit_duplicate_product_object')
      {
        print('PlatformException  errors code is ${e.code} &&  msg is ${e.message}');
        showCustomAlertDialog(context, 'PlatformException  There is a pending transaction for the same product.${productDetails.title}&.${e.message}&& ${e.details}. Click ok to complete purchace')
            .then((value) async => await _inAppPurchase.completePurchase(_purchases.firstWhere((element) => element.productID == productDetails.id)));
      }
      else if (e.code == 'Authentication Failed')
      {
        print('PlatformException  errors code is ${e.code} &&  msg is ${e.message}');
        showCustomAlertDialog(context, 'PlatformException  Authentication Failed .${e.message}&& ${e.message}');
      }
      else
      {
        print('PlatformException  errors code is ${e.code} &&  msg is ${e.message}');
        showCustomAlertDialog(context, 'PlatformException   .${e.message}&& ${e.message}');
      }
    }
    catch (e)
    {
      Navigator.of(context).pop();
      showCustomAlertDialog(context, 'There is catch  inn.${e}&& ${e.toString()}');
      print('There is catch  inn.${e}&& ${e.toString()}');
    }
  }


  Future<void> _listenToPurchaseUpdated(List<PurchaseDetails> purchaseDetailsList) async
  {
    print('_listenToPurchaseUpdated started');
    if(purchaseDetailsList.isEmpty)
      {
        showCustomAlertDialog(context, 'We haven\'t found any purchased product.If you are facing any issues with In App Purchase Please feel free to contact our support team.')
            .then((value) => Navigator.of(context).pop());
      }
    purchaseDetailsList.forEach((PurchaseDetails purchaseDetails) async
    {
      if (purchaseDetails.status == PurchaseStatus.pending)
      {
        print('PurchaseStatus Pending.......');
      }
      else
      {
        if (purchaseDetails.status == PurchaseStatus.error)
        {
          Navigator.of(context).pop();
          handleError(purchaseDetails.error!);
          print('PurchaseStatus error.......');
          return;
        }
        else if (purchaseDetails.status == PurchaseStatus.canceled)
        {
          print('PurchaseStatus canceled.......');
          showCustomAlertDialog(context, 'User has cancelled to purchase pro_id ${purchaseDetails.productID} ** ${purchaseDetails.status}')
              .then((value) => Navigator.of(context).pop());
        }
        else if (purchaseDetails.status == PurchaseStatus.purchased || purchaseDetails.status == PurchaseStatus.restored)
        {
          // Navigator.of(context).pop();
          print('purchaseDetails.status == PurchaseStatus.purchased || purchaseDetails.status == PurchaseStatus.restored');

          showCustomAlertDialog(context, 'PurchaseStatus = ${purchaseDetails.status} for pro_id ${purchaseDetails.productID} &&  for purchs_id ${purchaseDetails.purchaseID}')
              .then((value) => Navigator.of(context).pop());

          //checking whether a same product id purchased or not.
          bool valid = await _verifyPurchase(purchaseDetails);
          print('after validation is $valid');
          if (valid)
          {
            setState(() {
              _purchases.add(purchaseDetails);
            });
            print('delivery product');
            deliverProduct(purchaseDetails);
          }
          else
          {
            _handleInvalidPurchase(purchaseDetails);
          }
        }
        if (Platform.isAndroid)
        {
          // if (!_kAutoConsume && purchaseDetails.productID == _kConsumableId) {
          //   final InAppPurchaseAndroidPlatformAddition androidAddition =
          //   _inAppPurchase.getPlatformAddition<
          //       InAppPurchaseAndroidPlatformAddition>();
          //   await androidAddition.consumePurchase(purchaseDetails);
          // }
        }
        if (purchaseDetails.pendingCompletePurchase)
        {
          print('_listenToPurchaseUpdated pendingCompletePurchase PurchaseStatus =${purchaseDetails.status} for id ${purchaseDetails.productID}');
          //alert coming first after restoring
          // showCustomAlertDialog(context, 'PurchaseStatus =${purchaseDetails.status} for id ${purchaseDetails.productID}')
          //     .then((value) async =>
          await _inAppPurchase.completePurchase(purchaseDetails).whenComplete(()async =>
          {
            // Navigator.of(context).pop()
          });
          // );

        }
      }
    });
  }


  Future<bool> _verifyPurchase(PurchaseDetails purchaseDetails)async {
    // IMPORTANT!! Always verify a purchase before delivering the product.
    // For the purpose of an example, we directly return true.

    final isExist = await iapOperation.productIDExist(purchaseDetails.productID.toString());
    if (isExist)
    {
      return false;
    }
    return true;
  }
  Future<void> deliverProduct(PurchaseDetails purchaseDetails) async {
    // IMPORTANT!! Always verify purchase details before delivering the product.
    //check database local


    final prodDetailPass = _productsList.firstWhere((element) => element.id == purchaseDetails.productID);

    print('saving prodDetailPass = $prodDetailPass');
    savePurchasedDataToLocalDB(purchaseDetails, prodDetailPass).then((value) => checkIAPDatabase());
  }

  Future<String?> _getDeviceName() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) { // import 'dart:io'
      var iosDeviceInfo = await deviceInfo.iosInfo;
      // print('${iosDeviceInfo.name!} +  AND + ${iosDeviceInfo.model!}  ${iosDeviceInfo.localizedModel!}  ${iosDeviceInfo.utsname!}  ${iosDeviceInfo.systemVersion!}');
      return iosDeviceInfo.name! + '++' + iosDeviceInfo.systemName!;
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // Unique ID on Android
    }
  }
  Future<void> savePurchasedDataToLocalDB(PurchaseDetails purchasedData,ProductDetails productData) async
  {
    await userOperations.getLastLoggedInUser().then((user)async {

      if(user.isNotEmpty&&FirebaseAuth.instance.currentUser!.providerData.first.email==user.first.userEmail) {
    //just after purchased a product/restored products
    if (purchasedData.productID != '' &&
        purchasedData.purchaseID != '' && purchasedData.purchaseID != null &&
        purchasedData.error == null &&
        purchasedData.status ==  PurchaseStatus.purchased || purchasedData.status ==  PurchaseStatus.restored)
    {
      var iapOperation = IAPOperations();
      var iapData = IAPPurchaseData();

      print('\n\nuser name is ${FirebaseAuth.instance.currentUser!.providerData.first.email}');
      //iapData.user_id = FirebaseAuth.instance.currentUser!.providerData.first.email;
      iapData.userId = user.first.id;
      iapData.productId = purchasedData.productID;
      //iapData.purchase_id = int.tryParse(purchasedData.purchaseID!);
      iapData.purchaseId = purchasedData.purchaseID.toString();
      iapData.purchaseStatus = purchasedData.status.name;
      iapData.purchaseTransactionDate = int.parse(purchasedData.transactionDate!);
      iapData.purchaseVerificationDataOrToken = purchasedData.verificationData.serverVerificationData;
      iapData.purchaseDateStarting = int.parse(purchasedData.transactionDate!);
      //iapData.purchase_date_ending = DateTime.now().millisecondsSinceEpoch;
      iapData.purchaseSource = purchasedData.verificationData.source;
      iapData.purchaseDevice = await _getDeviceName();

      //not auto renewing 3 months
      if(iapData.productId!.contains('Non_Renewing_Subscription'))
      {
        iapData.purchaseDateEnding = getSubscriptionEndByAddingMonths(int.parse(purchasedData.transactionDate!), 3);
      }
      //life tine
      else if(iapData.productId!.contains('non_consumable'))
      {
        iapData.purchaseDateEnding = DateTime.now().millisecondsSinceEpoch;
      }
      //auto renewing 1 year
      else if(iapData.productId!.contains('Renewing_Subscription'))
      {
        iapData.purchaseDateEnding = getSubscriptionEndByAddingMonths(int.parse(purchasedData.transactionDate!), 12);
      }

      print('\nAFTER iapData.purchase_date_ending is ${iapData.purchaseDateEnding}');


      print('purchasedata.purchaseID == ${iapData.purchaseId.toString()}');
      // print(purchasedData.status);
      // print(purchasedData.transactionDate);
      // print(purchasedData.verificationData.source);
      // print(iapData.user_id);
      // print(iapData.purchase_status);
      // print(iapData.purchase_transaction_date);
      // print(iapData.purchase_source);
      // print(iapData.purchase_device);


      await iapOperation.insertIAPdata(iapData)
          .then((value) => showCustomAlertDialog(context, 'SAVED to LOCAL DATABASE for pro_id ${iapData.productId}')).then((value) => Navigator.of(context).pop()) //. PurchaseStatus = ${purchaseDetails.status} for pro_id ${purchaseDetails.productID} &&  for purchs_id ${purchaseDetails.purchaseID}')
          .onError((error, stackTrace) => showCustomAlertDialog(context, 'ON ERROR pro_id ${iapData.productId} NOT SAVED to LOCAL DATABASE ${error.toString()}'));
    }}else
        showCustomCallbackAlertDialog(context: context,msg: 'Something went wrong. Please login again',positiveClick:await logoutFunction(context) ,negativeClick:await logoutFunction(context));
    });
  }

  void checkValidity(PurchaseDetails purchasedData,ProductDetails productData,IAPPurchaseData? iapData)async
  {
    await userOperations.getLastLoggedInUser().then((user) async {
      if (user.isNotEmpty) {
        if(user.isNotEmpty&&FirebaseAuth.instance.currentUser!.providerData.first.email==user.first.userEmail) {


    print('checkValidity Reached');
    var iapOperation = IAPOperations();
    List<IAPPurchaseData> iapData = <IAPPurchaseData>[];
    iapOperation.getAllPurchaseByUserIdAndProductId(user.first.id, productData.id)
        .then((value) => print('checkValidity result after find is ${value.first.productId} ${value.first.userId} ${value.first.iapId}'))
        .onError((error, stackTrace) => print('checkValidity result after find is  ${error.toString()}')); }

      }});
  }

  void _handleInvalidPurchase(PurchaseDetails purchaseDetails)
  {
    print('/_handleInvalidPurchase ${_productsList.length} && ${purchaseDetails.productID}');
    if(_productsList.isEmpty)
      return;
    final prodDetailPass = _productsList.firstWhere((element) => element.id == purchaseDetails.productID);
    checkValidity(purchaseDetails, prodDetailPass, null);

    // showCustomAlertDialog(context, 'already purchase id exist for ${purchaseDetails.productID}');
  }

  void handleError(IAPError error)
  {
      print('Handle Error Purchase is ${error.message} && details are ${error.code}');
      showCustomAlertDialog(context, 'Handle Error Purchase is ${error.message} && details are ${error.details}');
      // setState(() {
      //     = false;
      // });
  }


    Future<void> confirmPriceChange(BuildContext context) async {
      if (Platform.isAndroid)
      {
        final InAppPurchaseAndroidPlatformAddition androidAddition = _inAppPurchase.getPlatformAddition<InAppPurchaseAndroidPlatformAddition>();
        final BillingResultWrapper priceChangeConfirmationResult = await androidAddition.launchPriceChangeConfirmationFlow(sku: 'purchaseId',);
        if (priceChangeConfirmationResult.responseCode == BillingResponse.ok)
        {
          ScaffoldMessenger.of(context)
              .showSnackBar(const SnackBar(content: Text('Price change accepted'),
          ));
        }
        else
        {
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text(
                priceChangeConfirmationResult.debugMessage ?? 'Price change failed with code ${priceChangeConfirmationResult.responseCode}',
              ),
              ));
        }
      }
      if (Platform.isIOS)
      {
        final InAppPurchaseStoreKitPlatformAddition iapStoreKitPlatformAddition = _inAppPurchase.getPlatformAddition<InAppPurchaseStoreKitPlatformAddition>();
        await iapStoreKitPlatformAddition.showPriceConsentIfNeeded();
      }
    }
    GooglePlayPurchaseDetails? _getOldSubscription(ProductDetails productDetails, Map<String, PurchaseDetails> purchases)
    {
      // This is just to demonstrate a subscription upgrade or downgrade.
      // This method assumes that you have only 2 subscriptions under a group, 'subscription_silver' & 'subscription_gold'.
      // The 'subscription_silver' subscription can be upgraded to 'subscription_gold' and
      // the 'subscription_gold' subscription can be downgraded to 'subscription_silver'.
      // Please remember to replace the logic of finding the old subscription Id as per your app.
      // The old subscription is only required on Android since Apple handles this internally
      // by using the subscription group feature in iTunesConnect.

      GooglePlayPurchaseDetails? oldSubscription;
      // if (productDetails.id == _kSilverSubscriptionId && purchases[_kGoldSubscriptionId] != null) {
      //   oldSubscription = purchases[_kGoldSubscriptionId]! as GooglePlayPurchaseDetails;
      // }
      // else if (productDetails.id == _kGoldSubscriptionId && purchases[_kSilverSubscriptionId] != null)
      // {
      //   oldSubscription = purchases[_kSilverSubscriptionId]! as GooglePlayPurchaseDetails;
      // }
      return oldSubscription;
    }
  }


class ExamplePaymentQueueDelegate implements SKPaymentQueueDelegateWrapper {
  @override
  bool shouldContinueTransaction(
      SKPaymentTransactionWrapper transaction, SKStorefrontWrapper storefront) {
    return true;
  }

  @override
  bool shouldShowPriceConsent() {
    return false;
  }
}