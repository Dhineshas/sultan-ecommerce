import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:ecommerce/contactSupport.dart';
import 'package:ecommerce/databaseSettings.dart';
import 'package:ecommerce/report.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/FirebaseUtils.dart';
import 'package:ecommerce/utils/InAppPurchaseHandler.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Subscription.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:ecommerce/webView.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'extraChargeList.dart';


class SettingsScreenState extends StatefulWidget {
  const SettingsScreenState({Key? key}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreenState> {
  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    final databaseSyncsOpt=MyWidgets.shadowContainer(height: 45,
        color: MyColors.white_chinese_e0_bg,
        shadowColor: MyColors.white_f1_bg,
        cornerRadius:8,spreadRadius:0,blurRadius:0,child:InkWell(onTap:()async{
        // await goto(context, DatabaseScreenState());
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      DatabaseScreenState()) );
        },child:
        Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.only(left: 8),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        MyWidgets.textView(text: 'Database Settings'),
                        Spacer(),
                        Icon(
                          Icons.chevron_right_rounded,
                          size: 20,
                        ),
                        SizedBox(
                          width: 8,
                        )
                      ],
                    ),
                  )),
            ])));
    final extraChargeOpt=MyWidgets.shadowContainer(height: 45,
        color: MyColors.white_chinese_e0_bg,
        shadowColor: MyColors.white_f1_bg,
        cornerRadius:8,spreadRadius:0,blurRadius:0,child:InkWell(onTap:()async{
          await goto(context, ExtraChargeList());
        },child:
        Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.only(left: 8),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        MyWidgets.textView(text: 'Extra Charges'),
                        Spacer(),
                        Icon(
                          Icons.chevron_right_rounded,
                          size: 20,
                        ),
                        SizedBox(
                          width: 8,
                        )
                      ],
                    ),
                  )),
            ])));

    final repostOpt=MyWidgets.shadowContainer(height: 45,
        color: MyColors.white_chinese_e0_bg,
        shadowColor: MyColors.white_f1_bg,
        cornerRadius:8,spreadRadius:0,blurRadius:0,child:InkWell(onTap:()async{
          goto(context, ReportScreenState());
        },child:
        Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.only(left: 8),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        MyWidgets.textView(text: 'Reports'),
                        Spacer(),
                        Icon(
                          Icons.chevron_right_rounded,
                          size: 20,
                        ),
                        SizedBox(
                          width: 8,
                        )
                      ],
                    ),
                  )),
            ])));

    final contactUsOpt=MyWidgets.shadowContainer(height: 45,
        color: MyColors.white_chinese_e0_bg,
        shadowColor: MyColors.white_f1_bg,
        cornerRadius:8,spreadRadius:0,blurRadius:0,child:InkWell(onTap:()async{
          goto(context, ContactSupportpage());
        },child:
        Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.only(left: 8),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        MyWidgets.textView(text: 'Contact Us'),
                        Spacer(),
                        Icon(
                          Icons.chevron_right_rounded,
                          size: 20,
                        ),
                        SizedBox(
                          width: 8,
                        )
                      ],
                    ),
                  )),
            ])));
    final aboutUsOpt=MyWidgets.shadowContainer(height: 45,
        color: MyColors.white_chinese_e0_bg,
        shadowColor: MyColors.white_f1_bg,
        cornerRadius:8,spreadRadius:0,blurRadius:0,child:InkWell(onTap:()async{

        },child:
        Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.only(left: 8),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        MyWidgets.textView(text: 'About Us'),
                        Spacer(),
                        Icon(
                          Icons.chevron_right_rounded,
                          size: 20,
                        ),
                        SizedBox(
                          width: 8,
                        )
                      ],
                    ),
                  )),
            ])
        )
    );

    final terms=MyWidgets.shadowContainer(height: 45,
        color: MyColors.white_chinese_e0_bg,
        shadowColor: MyColors.white_f1_bg,
        cornerRadius:8,spreadRadius:0,blurRadius:0,child:InkWell(onTap:()async{
goto(context, WebViewScreenState());
        },child:
        Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.only(left: 8),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        MyWidgets.textView(text: 'Terms And Conditions'),
                        Spacer(),
                        Icon(
                          Icons.chevron_right_rounded,
                          size: 20,
                        ),
                        SizedBox(
                          width: 8,
                        )
                      ],
                    ),
                  )),
            ])
        )
    );
    final privacy=MyWidgets.shadowContainer(height: 45,
        color: MyColors.white_chinese_e0_bg,
        shadowColor: MyColors.white_f1_bg,
        cornerRadius:8,spreadRadius:0,blurRadius:0,child:InkWell(onTap:()async{
          goto(context, WebViewScreenState(isTerms:false));
        },child:
        Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.only(left: 8),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        MyWidgets.textView(text: 'Privacy policy'),
                        Spacer(),
                        Icon(
                          Icons.chevron_right_rounded,
                          size: 20,
                        ),
                        SizedBox(
                          width: 8,
                        )
                      ],
                    ),
                  )),
            ])
        )
    );


    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
          title: MyWidgets.textView(text: 'Settings',style: TextStyle(color: Colors.black),),
          leading: IconButton(
            icon: Icon(Icons.arrow_back,color: MyColors.black,),
            onPressed: () => Navigator.pop(context),
          ),
        ),
        body:  Container(
    // padding: EdgeInsets.all(15),
    height: MediaQuery.of(context).size.height,child:SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(
                parent: BouncingScrollPhysics()),
            child: Padding(padding : EdgeInsets.only(left: 10,right: 10),child:Column(children: <Widget>[
          SizedBox(
            height: 10,
          ),
          databaseSyncsOpt,
              SizedBox(
                height: 10,
              ),
              repostOpt,
              SizedBox(
                height: 10,
              ),
              extraChargeOpt,
              SizedBox(
                height: 10,
              ),
          contactUsOpt,
              SizedBox(
                height: 10,
              ),
              aboutUsOpt,
              SizedBox(
                height: 10,
              ),
              terms,
              SizedBox(
                height: 10,
              ),
              privacy,
              SizedBox(
                height: 10,
              ),
        ])
            )
        )
        )
    );
  }

}
