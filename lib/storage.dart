import 'dart:convert';
import 'dart:io';
import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/addStorageProduct.dart';
import 'package:ecommerce/addcreatedproducts.dart';
import 'package:ecommerce/database/product_item_operations.dart';
import 'package:ecommerce/database/storage_operations.dart';
import 'package:ecommerce/model/ProductItem.dart';
import 'package:ecommerce/model/StorageExtra.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'model/Project.dart';

class StorageScreenState extends StatefulWidget {
  const StorageScreenState({Key? key}) : super(key: key);

  @override
  _StorageScreenState createState() => _StorageScreenState();
}

class _StorageScreenState extends State<StorageScreenState> {
  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 1000), () {
      _getData();
      // _getDataSelectedInventoryItems();
    });

    super.initState();
  }

  //var productsItems = <ProductItem>[];
  var productsItems = <StorageExtra>[];
  bool isLoading = true;
  bool isListView = true;
  var productItemOperations = ProductItemOperations();
  var storageOperations = StorageOperations();
  var _scrollController = ScrollController();

  // final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  // final GlobalKey<AnimatedListState> _productListKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MyColors.white_f1_bg,
        appBar: MyWidgets.appBar(
          titleSpacing: 10,
          /*title: Text(
            'Storage',
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          actions: <Widget>[
            Center(
              child: Padding(
                padding: EdgeInsets.only(right: 0.0),
                child: GestureDetector(
                  onTap: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) =>  AddCreatedProductsScreenState(productItemIdArray: this.productItemIdArray,)),
                    ).then((value) => value==true? _getData():{});
                  },
                  child: Text('Add products.',
                    style: TextStyle(color: Colors.black, fontSize: 14),),
                ),
              ),
            ),
          ],*/
          elevation: 0,
          title: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  flex: 7,
                  child: ElevatedButton.icon(
                    icon: const Icon(
                      Icons.local_shipping_outlined,
                      color: MyColors.black,
                      size: 30,
                    ),
                    onPressed: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddCreatedProductsScreenState(
                                  productItemIdArray: this.productItemIdArray,
                                )),
                      ).then((value) => value == true ? _getData() : {});
                    },
                    label: Text(
                      "Add Completed Products",
                      style: MyStyles.customTextStyle(fontSize: 13),
                    ),
                    style: ElevatedButton.styleFrom(
                      elevation: 0,
                      primary: MyColors.white,
                      fixedSize: const Size(0, 44),
                      shape: MyDecorations.roundedRectangleBorder(10),
                    ),
                  )),
              /*SizedBox(
                width: 15,
              ),
              Expanded(
                  flex: 3,
                  child: MyWidgets.shadowContainer(
                      height: 44,
                      blurRadius: 0,
                      cornerRadius: 10,
                      spreadRadius: 0,
                      shadowColor: MyColors.white_f1_bg,
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  child: Center(
                                    child: Icon(
                                      Icons.list,
                                      color: MyColors.black,
                                    ),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      this.isListView = true;
                                    });
                                  },
                                )
                            ),
                            Container(
                                width: 0,
                                height: 30,
                                child: VerticalDivider(
                                    color: MyColors.grey_9d_bg)),
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  child: Center(
                                    child: Icon(
                                      Icons.grid_on_rounded,
                                      color: MyColors.black,
                                    ),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      this.isListView = false;
                                    });
                                  },
                                )
                            )
                          ])
                  )
              )*/
            ],
          ),

        ),
        bottomNavigationBar: MyWidgets.addNewButton(text:'Add New',onAddOrMovePressed: (){
          if(project!=null)
          MyBottomSheet().showBottomSheet(context,maxHeight:  0.75, callback: (value){
            if(value!=null&&value==true){
              _getData();
            }
          },child:   AddStorageProductScreenState(project: project)
          );
          else
            showCustomAlertDialog(context, 'Please create a project ');
        }),
        body: createListView(context));
  }

  Widget createListView(BuildContext context /*, AsyncSnapshot snapshot*/) {
    //values = snapshot.data ?? [];
    if (productsItems.isNotEmpty) {
      return FadeIn(
          child: Container(
              height: double.infinity,
              width: double.infinity, //MediaQuery.of(context).size.width - 20,
              child: /*this.isListView ?*/ showListView() /*: showGridView()*/
          )
      );
    } else
      return isLoading
          ? MyWidgets.buildCircularProgressIndicator()
          : Center(
              child: Text(
                'No Product Items.',
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.grey,
                ),
              ),
            );
  }

  Widget showListView() {
    return SafeArea(bottom: true,child: ListView.builder(
                // key: _listKey,
                physics: AlwaysScrollableScrollPhysics(
                    parent: BouncingScrollPhysics()),
                controller: _scrollController,
                shrinkWrap: true,
                itemCount: productsItems.length,
                itemBuilder: (BuildContext context, int index) {
                  // return _buildListItem(context, productsItems[index], index);

                  return _buildListItem(context, productsItems[index], index);
                }));
  }
  Widget showGridView() {
    return SafeArea(bottom: true,child:  GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
      // key: _listKey,
        physics: AlwaysScrollableScrollPhysics(
            parent: BouncingScrollPhysics()),
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: productsItems.length,
        itemBuilder: (BuildContext context, int index) {
          // return _buildListItem(context, productsItems[index], index);

          return _buildGridItem(context, productsItems[index], index);
        }));
  }

  /// Delete Click and delete item
  onDelete(int index) async {
    var id = this.productsItems[index].id;
    productsItems[index].storageSourceStatus==0?
    storageOperations.deleteStorageItemByProductItemId(id!).then((value) {
      removeItem(index);
    }):
    storageOperations.deleteStorageExtraItemByProductItemId(id!).then((value) {
      removeItem(index);
    });

  }

  removeItem(int index)async{
    setState(() {
      productItemIdArray.removeWhere((element) => element == productsItems[index].id);
      productsItems.removeAt(index);
    });
  }

  Widget _buildListItem(BuildContext context, StorageExtra value, int index) {
    return MyWidgets.shadowContainer(
        height: 150,
        marginL: 10,
        marginR: 10,
        marginB: 5,
        marginT: 5,
        spreadRadius: 2,
        cornerRadius: 10,
        shadowColor: MyColors.white_f1_bg,
        child: Column(
          children: <Widget>[
            Expanded(
                flex: 7,
                child: MyWidgets.shadowContainer(
                    marginL: 15,
                    marginT: 15,
                    marginR: 15,
                    marginB: 5,
                    cornerRadius: 0,
                    shadowColor: MyColors.white,
                    spreadRadius: 0,
                    blurRadius: 0,
                    child: Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              width: 66.0,
                              height: 66.0,
                              child: value.imageBase != null &&
                                      value.imageBase.toString().isNotEmpty
                                  //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                                  ? ClipRRect(
                                      borderRadius: BorderRadius.circular(35),
                                      child: Image.memory(
                                        base64Decode(
                                            value.imageBase.toString()),
                                        fit: BoxFit.cover,
                                        gaplessPlayback: true,
                                      ),
                                    )
                                  : ClipRRect(
                                      borderRadius: BorderRadius.circular(35),
                                      child: Container(
                                        color: MyColors.grey_70,
                                      ),
                                    )),
                          SizedBox(
                            width: 15,
                          ),
                          Expanded(
                            flex: 1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                //Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0)),

                                /*Text(
                                  value.name!,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18.0,
                                    color: Colors.black,
                                  ),
                                  textAlign: TextAlign.left,
                                  maxLines: 2,
                                ),*/

                                Row(
                                  children: <Widget>[
                                    Flexible(
                                      child: RichText(
                                        overflow: TextOverflow.ellipsis,
                                        strutStyle: StrutStyle(fontSize: 12.0),
                                        text: TextSpan(
                                            style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 15.0,
                                              color: Colors.black,
                                            ),
                                            text: value.name!),
                                      ),
                                    ),
                                   value.storageSourceStatus==0? Icon(Icons.verified,size: 13,):Container()
                                  ],
                                ),

                                Spacer(),

                                MyText.priceCurrencyText(context,
                                    price:
                                        'Cost: ${double.parse((value.cost!).toStringAsFixed(2))}',
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal),

                                Spacer(),
                                MyWidgets.shadowContainer(
                                    color: MyColors.white_f8,
                                    cornerRadius: 10,
                                    shadowColor: MyColors.white,
                                    spreadRadius: 0,
                                    blurRadius: 0,
                                    child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          MyWidgets.shadowContainer(
                                              height: 27,
                                              width: 27,
                                              marginB: 4,
                                              marginR: 4,
                                              marginT: 4,
                                              marginL: 4,
                                              color:
                                                  MyColors.white_chinese_e0_bg,
                                              cornerRadius: 10,
                                              shadowColor: MyColors.white_f8,
                                              blurRadius: 0,
                                              spreadRadius: 0,
                                              child: GestureDetector(
                                                child: Icon(
                                                  Icons.remove,
                                                  color: MyColors.grey_70,
                                                  size: 20,
                                                ),
                                                onTap: () => setState(() {
                                                  if (value.storageQuantity! >
                                                      1)
                                                    value.storageQuantity =
                                                        value.storageQuantity! -
                                                            1;
                                                  updateProductQuantity(value);
                                                }),
                                              )),
                                          SizedBox(
                                            width: 15,
                                          ),
                                          Text('${value.storageQuantity}',
                                              style: MyStyles.grey_70__HR_16),
                                          SizedBox(
                                            width: 15,
                                          ),
                                          MyWidgets.shadowContainer(
                                              height: 27,
                                              width: 27,
                                              marginB: 4,
                                              marginR: 4,
                                              marginT: 4,
                                              marginL: 4,
                                              color:
                                                  MyColors.white_chinese_e0_bg,
                                              cornerRadius: 10,
                                              shadowColor: MyColors.white_f8,
                                              blurRadius: 0,
                                              spreadRadius: 0,
                                              child: GestureDetector(
                                                child: Icon(
                                                  Icons.add,
                                                  color: MyColors.grey_70,
                                                  size: 20,
                                                ),
                                                onTap: () => setState(() {
                                                  value.storageQuantity =
                                                      value.storageQuantity! +
                                                          1;
                                                  updateProductQuantity(value);
                                                }),
                                              ))
                                        ])),
                                Spacer(),
                                //Padding(padding: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 10.0)),
                              ],
                            ),
                          ),
                          GestureDetector(
                            child: Icon(
                              CupertinoIcons.delete_simple,
                              color: MyColors.red_ff_bg,
                              size: 20,
                            ),
                            onTap: () async {
                              return await showCustomCallbackAlertDialog(
                                  context: context,
                                  positiveText: 'Delete',
                                  msg:
                                  'Are you sure you wish to delete item?',
                                  positiveClick: () {
                                    onDelete(index);
                                    Navigator.of(context).pop(true);
                                  },
                                  negativeClick: () {
                                    Navigator.of(context).pop(false);
                                  });
                            },
                          )
                        ]))),
            Expanded(
                flex: 2,
                child: Stack(alignment: Alignment.center, children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                      color: MyColors.white_chinese_e0_bg,
                    ), //BoxDecoration
                  ),
                  Text(
                    "Selling Price: ${double.parse((value.sellingPrice!).toStringAsFixed(2))} QAR",
                    style: MyStyles.customTextStyle(
                        fontSize: 14, fontWeight: FontWeight.w400),
                  )
                ]))
          ],
        ));
  }
  Widget _buildGridItem(BuildContext context, StorageExtra value, int index) {
    return MyWidgets.shadowContainer(
        height: 150,
        marginL: 10,
        marginR: 10,
        marginB: 5,
        marginT: 5,
        spreadRadius: 2,
        cornerRadius: 10,
        shadowColor: MyColors.white_f1_bg,
        child: Column(
          children: <Widget>[
            Expanded(
                flex: 7,
                child: MyWidgets.shadowContainer(
                    marginL: 15,
                    marginT: 15,
                    marginR: 15,
                    marginB: 5,
                    cornerRadius: 0,
                    shadowColor: MyColors.white,
                    spreadRadius: 0,
                    blurRadius: 0,
                    child: Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              width: 66.0,
                              height: 66.0,
                              child: value.imageBase != null &&
                                  value.imageBase.toString().isNotEmpty
                              //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                                  ? ClipRRect(
                                borderRadius: BorderRadius.circular(35),
                                child: Image.memory(
                                  base64Decode(
                                      value.imageBase.toString()),
                                  fit: BoxFit.cover,
                                  gaplessPlayback: true,
                                ),
                              )
                                  : ClipRRect(
                                borderRadius: BorderRadius.circular(35),
                                child: Container(
                                  color: MyColors.grey_70,
                                ),
                              )),
                          SizedBox(
                            width: 15,
                          ),
                          Expanded(
                            flex: 1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                //Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0)),

                                Text(
                                  value.name!,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18.0,
                                    color: Colors.black,
                                  ),
                                  textAlign: TextAlign.left,
                                  maxLines: 2,
                                ),

                                Spacer(),

                                MyText.priceCurrencyText(context,
                                    price:
                                    'Cost: ${double.parse((value.cost!).toStringAsFixed(2))}',
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal),

                                Spacer(),
                                MyWidgets.shadowContainer(
                                    color: MyColors.white_f8,
                                    cornerRadius: 10,
                                    shadowColor: MyColors.white,
                                    spreadRadius: 0,
                                    blurRadius: 0,
                                    child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                        children: <Widget>[
                                          MyWidgets.shadowContainer(
                                              height: 27,
                                              width: 27,
                                              marginB: 4,
                                              marginR: 4,
                                              marginT: 4,
                                              marginL: 4,
                                              color:
                                              MyColors.white_chinese_e0_bg,
                                              cornerRadius: 10,
                                              shadowColor: MyColors.white_f8,
                                              blurRadius: 0,
                                              spreadRadius: 0,
                                              child: GestureDetector(
                                                child: Icon(
                                                  Icons.remove,
                                                  color: MyColors.grey_70,
                                                  size: 20,
                                                ),
                                                onTap: () => setState(() {
                                                  if (value.storageQuantity! >
                                                      1)
                                                    value.storageQuantity =
                                                        value.storageQuantity! -
                                                            1;
                                                  updateProductQuantity(value);
                                                }),
                                              )),
                                          SizedBox(
                                            width: 15,
                                          ),
                                          Text('${value.storageQuantity}',
                                              style: MyStyles.grey_70__HR_16),
                                          SizedBox(
                                            width: 15,
                                          ),
                                          MyWidgets.shadowContainer(
                                              height: 27,
                                              width: 27,
                                              marginB: 4,
                                              marginR: 4,
                                              marginT: 4,
                                              marginL: 4,
                                              color:
                                              MyColors.white_chinese_e0_bg,
                                              cornerRadius: 10,
                                              shadowColor: MyColors.white_f8,
                                              blurRadius: 0,
                                              spreadRadius: 0,
                                              child: GestureDetector(
                                                child: Icon(
                                                  Icons.add,
                                                  color: MyColors.grey_70,
                                                  size: 20,
                                                ),
                                                onTap: () => setState(() {
                                                  value.storageQuantity =
                                                      value.storageQuantity! +
                                                          1;
                                                  updateProductQuantity(value);
                                                }),
                                              ))
                                        ])),
                                Spacer(),
                                //Padding(padding: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 10.0)),
                              ],
                            ),
                          ),
                          GestureDetector(
                            child: Icon(
                              CupertinoIcons.delete_simple,
                              color: MyColors.red_ff_bg,
                              size: 20,
                            ),
                            onTap: () async {
                              return await showCustomCallbackAlertDialog(
                                  context: context,
                                  positiveText: 'Delete',
                                  msg:
                                  'Are you sure you wish to delete item?',
                                  positiveClick: () {
                                    onDelete(index);
                                    Navigator.of(context).pop(true);
                                  },
                                  negativeClick: () {
                                    Navigator.of(context).pop(false);
                                  });
                            },
                          )
                        ]))),
            Expanded(
                flex: 2,
                child: Stack(alignment: Alignment.center, children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                      color: MyColors.white_chinese_e0_bg,
                    ), //BoxDecoration
                  ),
                  Text(
                    "Selling Price: ${double.parse((value.sellingPrice!).toStringAsFixed(2))} QAR",
                    style: MyStyles.customTextStyle(
                        fontSize: 14, fontWeight: FontWeight.w400),
                  )
                ]))
          ],
        ));
  }

  updateProductQuantity(StorageExtra value) async {
    value.storageSourceStatus==0?
    await storageOperations.updateStorageItemQuantity(value).then((value) => {

        })
   :

      await storageOperations.updateStorageExtraItemQuantity(value).then((value) => {

      });
  }

  var productItemIdArray = <int>[];
  Project? project;

  ///Fetch data from database
 /* Future<List<ProductItem>> _getData() async {
    await MyPref.getProject().then((project) async {
      this.project = project;
      if (project != null) {
        await productItemOperations
            .productItemsInStorageByProjectId(project.id)
            .then((value) {
          productsItems = value;
          productItemIdArray.addAll(productsItems.map((e) => e.id!));
        });
      } else
        showCustomAlertDialog(context, 'Please create a project ');
    });
    setState(() {
      isLoading = false;
    });
    return productsItems;
  }*/
  Future<List<StorageExtra>> _getData() async {
    await MyPref.getProject().then((project) async {
      this.project = project;
      if (project != null) {
        await storageOperations
            .productItemsInStorageAndStorageExtraByProjectId(project.id)
            .then((value) {
          productsItems = value;
          //productItemIdArray.addAll(productsItems.map((e) => e.id!));
          productItemIdArray.clear();
          productsItems.forEach((element) {
            if(element.storageSourceStatus==0)
              productItemIdArray.add(element.id!);
          });
        });
      } else
        showCustomAlertDialog(context, 'Please create a project ');
    });
    setState(() {
      isLoading = false;
    });
    return productsItems;
  }

}
