
import 'package:ecommerce/model/UserIssuesData.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'database/firestore_operations.dart';


class ContactSupportpage extends StatefulWidget {
  const ContactSupportpage({Key? key}) : super(key: key);

  @override
  State<ContactSupportpage> createState() => _ContactSupportpageState();
}

class _ContactSupportpageState extends State<ContactSupportpage> {


  final teUserNameTextController = TextEditingController();
  final teUserContactController = TextEditingController();
  final teUserEmailController = TextEditingController();
  final teUserIssuesController = TextEditingController();


  @override
  Widget build(BuildContext context) {


    final userNameTextVw= Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10,),
        MyWidgets.textView(text: 'Your Name'),
        SizedBox(height: 10,),
        MyWidgets.textViewContainer(
            height: 45, cornerRadius: 8, color: MyColors.white,
            child:  MyWidgets.textFromField(
              contentPaddingL: 20.0,
              contentPaddingT: 15.0,
              contentPaddingR: 20.0,
              contentPaddingB: 15.0,
              cornerRadius: 8,keyboardType: TextInputType.name,
              controller: teUserNameTextController,)
        ),
      ],
    );
    final emailTextVw= Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10,),
        MyWidgets.textView(text: 'Email Address'),
        SizedBox(height: 10,),
        MyWidgets.textViewContainer(
            height: 45, cornerRadius: 8, color: MyColors.white,
            child:
            MyWidgets.textFromField(
                contentPaddingL: 20.0,
                contentPaddingT: 15.0,
                contentPaddingR: 20.0,
                contentPaddingB: 15.0,
                cornerRadius: 8,
                keyboardType: TextInputType.emailAddress,controller: teUserEmailController)
        ),
      ],
    );
    final contactNoTextVw= Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10,),
        MyWidgets.textView(text: 'Mobile Number'),
        SizedBox(height: 10,),
        MyWidgets.textViewContainer(
            height: 45, cornerRadius: 8, color: MyColors.white,
            child:
            MyWidgets.textFromField(
                contentPaddingL: 20.0,
                contentPaddingT: 15.0,
                contentPaddingR: 20.0,
                contentPaddingB: 15.0,
                cornerRadius: 8,
                keyboardType: TextInputType.phone,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],controller: teUserContactController)
        ),
      ],
    );
    final userIssuesTextVw= Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10,),
        MyWidgets.textView(text: 'Write your issues here'),
        SizedBox(height: 10,),
        TextFormField(
          minLines: 5,
          maxLines: 15,
          keyboardType: TextInputType.multiline,
          controller: teUserIssuesController,
          decoration: InputDecoration(
            hintText: 'Description',
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: BorderSide(
                color: Colors.black,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: BorderSide(
                color: Colors.grey,
                width: 1.0,
              ),
            ),
            hintStyle: TextStyle(
                color: Colors.grey
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
            ),
          ),
        ),
      ],
    );
    final submitDataButton = MyWidgets.materialButton(context,text:  "Submit Data",
        onPressed:()async
        {

          if(_validate()){
            FocusScopeNode currentFocus = FocusScope.of(context);

            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
            submitDataToFirebase();
          }
        });

    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
          title: MyWidgets.textView(text: 'Contact Us',style: TextStyle(color: Colors.black),),
          leading: IconButton(
            icon: Icon(Icons.arrow_back,color: MyColors.black,),
            onPressed: () => Navigator.pop(context),
          ),
        ),
        body:  SingleChildScrollView(
            padding: EdgeInsets.only(left: 30, right: 30, top: 25, bottom: 20),
            physics: BouncingScrollPhysics(),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 50,
                ),
                userNameTextVw,
                contactNoTextVw,
                emailTextVw,
                userIssuesTextVw,
                SizedBox(
                  height: 20,
                ),
                submitDataButton
              ],
            ))
    );
  }

  bool _validate() {
    if (teUserNameTextController.text.isEmpty)
    {
      context.showSnackBar("Please enter name");
      return false;
    }
    else if (teUserContactController.text.isEmpty)
    {
      context.showSnackBar("Please enter your mobile number");
      return false;
    }
    else if (teUserEmailController.text.isEmpty)
    {
      context.showSnackBar("Please enter your Email id associated with this application");
      return false;
    }
    else if (!RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(teUserEmailController.text.toString().trim()))
    {
      context.showSnackBar("Please check email address");
      return false;
    }
    else if (teUserIssuesController.text.isEmpty)
    {
      context.showSnackBar("Please type your issues clearly. We will sort out and get back to you.");
      return false;
    }
    return true;
  }
  static var fireStoreOperations = FireStoreOperations();
  submitDataToFirebase()async
  {
    showNonCancellableCircularLoader(context, 'Sending data...');
    var userIssueDat = UserIssuesData();
    userIssueDat.username = teUserNameTextController.text;
    userIssueDat.userMobile = teUserContactController.text;
    userIssueDat.userEmailId = teUserEmailController.text.trim();
    userIssueDat.userIssue = teUserIssuesController.text;
    userIssueDat.complaint_start_Date = DateTime.now().millisecondsSinceEpoch;

    await fireStoreOperations.addFireStoreUserComplaint(userIssueDat).then((value) async {
      Navigator.pop(context);
      context.showSnackBar("Data sent successfully.We will get back to you as soon as possible.");
    });
  }
}