

import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/loginView.dart';
import 'package:ecommerce/main.dart';
import 'package:ecommerce/model/Project.dart';
import 'package:ecommerce/model/homeGridItems.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/InAppPurchaseHandler.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'database/home_operations.dart';
import 'database/user_operation.dart';

class SplashScreen extends StatefulWidget {

  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }




  @override
  void initState() {
    Future.delayed(Duration(seconds: 2), ()async {
      var firebase = FirebaseAuth.instance;
      /*firebase.authStateChanges().listen((User? user)async {
        if (user != null) {

          await userOperations.getLastLoggedInUser().then((loggedInUser)async {
           if(loggedInUser.isNotEmpty){
             print('current_user_splash ${user.uid}  ${loggedInUser.first.userUid}');

             if(user.uid==loggedInUser.first.userUid){

               Navigator.of(context).pushReplacement(
                   MaterialPageRoute(builder: (context) => MyHomePage()));
               return;
             }else print('current_user_splash');
           }else  print('current_user_splash');
          });
        }else{
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => LoginScreen()));
          print('current_user_splash');

        }
      });*/

      if (firebase.currentUser != null) {
        var userOperations = UserOperations();


      await userOperations.getLastLoggedInUser().then((loggedInUser) async {
        if (loggedInUser.isNotEmpty) {
            if (firebase.currentUser?.uid == loggedInUser.first.userUid) {
               await InAppPurchaseHandler.updateFireStoreSubscriptionToLocalDb(loggedInUser.first.userEmail!).then((value) =>
              Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                  MyHomePage()), (Route<dynamic> route) => false)
              );
            } else {
              _logoutUser();
            }
            return;
          }
         else {
          _logoutUser();
        }
      });
    }else{
        _logoutUser(gotoLogin: false);
      }
      });

      /*Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => LoginScreen()))*/;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MyWidgets.scaffold(body:SafeArea(child:FadeIn(child: Center(child: Container(
      width: 211,
      height: 207,
      color: MyColors.white_f1_bg,
      child:
       Image.asset('assets/images/splas_pos_xpress.png'),

      ),),
    )));
  }
  Future<void> _logoutUser({bool gotoLogin=true})async{

    if(gotoLogin){
      var userOperations = UserOperations();
  if(Platform.isAndroid)
  {
    await FirebaseAuth.instance.signOut().then((value)async{
      GoogleSignIn _googleSignIn = GoogleSignIn();
      await _googleSignIn.signOut().then((value) async{
        await userOperations.updateAllIsLoggedInState().then((user)async {
          MyPref.setProject(null);

          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
              LoginScreen()), (Route<dynamic> route) => false);

        });
      });
    });
  }
  else
  {
    // await FirebaseAuth.instance.signOut().then((value)async {
      await userOperations.updateAllIsLoggedInState().then((user)async {
        MyPref.setProject(null);
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
            LoginScreen()), (Route<dynamic> route) => false);

      });
    // });
  }
}else{
      await FirebaseAuth.instance.signOut().then((value)async {
        GoogleSignIn _googleSignIn = GoogleSignIn();
        await _googleSignIn.signOut().then((value) async{
          MyPref.setProject(null);
          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
              LoginScreen()), (Route<dynamic> route) => false);

      });
      });
    }}

}
