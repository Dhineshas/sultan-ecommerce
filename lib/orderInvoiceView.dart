import 'package:ecommerce/database/orders_operations.dart';
import 'package:ecommerce/model/Project.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/PdfInvoiceService.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io';
import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

import 'model/ProductItem.dart';

class OrderInvoicePDFScreen extends StatefulWidget {
  final int selectedOrderId;
  final int selectedOrderCreatedAt;

  const OrderInvoicePDFScreen(
      {required this.selectedOrderCreatedAt,
      required this.selectedOrderId,
      Key? key})
      : super(key: key);

  @override
  State<OrderInvoicePDFScreen> createState() => _OrderInvoicePDFScreenState();
}

class _OrderInvoicePDFScreenState extends State<OrderInvoicePDFScreen> {
  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 300), () {
      getPdf();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
          title: Text(
            'Order Invoice Pdf View',
            style: TextStyle(color: Colors.black),
          ),
          leading: IconButton(
            color: MyColors.black,
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
          ),
        ),
        bottomNavigationBar: Container(
            height: 40,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: InkWell(
                      onTap: () {
                        sharePdf();
                      },
                      child:
                          Stack(alignment: Alignment.center, children: <Widget>[
                        Container(color: MyColors.white_chinese_e0_bg
                            //BoxDecoration
                            ),
                        Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          direction: Axis.horizontal,
                          alignment: WrapAlignment.center,
                          children: <Widget>[
                            Icon(CupertinoIcons.share, color: MyColors.black),
                            Container(
                              margin: EdgeInsets.all(5),
                              child: Text(
                                "Share PDF",
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 15.0,
                                    color: MyColors.black),
                              ),
                            )
                          ],
                        )
                      ]) //Container
                      ),
                ),
                Flexible(
                    flex: 1,
                    fit: FlexFit.tight,
                    child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            Container(
                              color: MyColors.red_ff_bg,
                              //BoxDecoration
                            ),
                            Wrap(
                              crossAxisAlignment: WrapCrossAlignment.center,
                              direction: Axis.horizontal,
                              alignment: WrapAlignment.center,
                              children: <Widget>[
                                Icon(
                                  CupertinoIcons.xmark_circle,
                                  color: Colors.white,
                                ),
                                Container(
                                  margin: EdgeInsets.all(5),
                                  child: Text(
                                    "Close PDF",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 15.0,
                                      color: MyColors.white,
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ) //Container
                        ))
              ],
            )),
        body: pdfFile == null
            ? Center(child:InkWell(onTap:()async{
          getPdf();
        },child: Row(mainAxisSize:MainAxisSize.min,children: [MyWidgets.textView(text: 'No Data Found'),
        Icon(Icons.refresh)],)))
            : FadeIn(
                child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 18,
                    child: SfPdfViewer.file(pdfFile!),
                  ),
                ],
              )));
  }

  File? pdfFile;

  void getPdf() async {
    await MyPref.getProject().then((project) async {
      var operationOrder = OrderOperations();

      if (project != null) {
        await showCustomCallbackAlertDialog(
            context: context,
            msg:
                "Do you want to get Invoice of Current order or Invoice of selected product ?",
            positiveText: 'Current Order',
            negativeText: 'Single Product',
            positiveClick: () async {
              Navigator.pop(context);
              await operationOrder
                  .getInvoiceAll(project.id, widget.selectedOrderCreatedAt)
                  .then((value) => setPDFData(value, project, false));
            },
            negativeClick: () async {
              Navigator.pop(context);
              await operationOrder
                  .getInvoiceSingleItem(project.id, widget.selectedOrderId)
                  .then((value) => setPDFData(value, project, true));
            });

        return;
        // final PdfInvoiceService service = PdfInvoiceService();
        // List<OrderedProduct> soldProducts = [
        //   OrderedProduct("sunflower oil", 10 , 10),
        //   OrderedProduct("sugar", 20, 20),
        //   OrderedProduct("Oninon", 30, 30),
        //   OrderedProduct("Hamburger", 40, 40),
        //   OrderedProduct("sugar", 50, 50),
        //   OrderedProduct("Oninon", 60, 60),
        //   OrderedProduct("Hamburger", 70, 70),
        // ];
        //
        // await service.createInvoice(soldProducts, 100.0, 50.0).then((value) =>
        //     setState(() {
        //       pdfFile = value;
        //       print('generatedPdfFile   ${pdfFile!.path}');
        //     })
        // );
      }
    });
  }

  setPDFData(List<ProductItem> productList, Project projectCurrent,
      bool isSingleProductInvoice) async {
    print('my order print ===== $productList');
    final PdfInvoiceService service = PdfInvoiceService();
    await service
        .createInvoice(productList, /*soldProducts,*/ projectCurrent,
            isSingleProductInvoice)
        .then((value) => setState(() {
              pdfFile = value;
              print('generatedPdfFile   ${pdfFile!.path}');
            }));
  }

  sharePdf() async {
    await Share.shareFiles(['${pdfFile!.path}'],
        text: 'Share Inventory Report');
  }
}
