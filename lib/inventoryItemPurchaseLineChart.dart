import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/model/GraphProductItem.dart';
import 'package:ecommerce/upgrade.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/InAppPurchaseHandler.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Subscription.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'database/product_item_operations.dart';
import 'model/ProductItem.dart';

class InventoryItemPurchaseLineChart extends StatefulWidget {
  final int? inventoryItemId;

  const InventoryItemPurchaseLineChart({@required this.inventoryItemId, Key? key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => InventoryItemPurchaseLineChartState();
}

class InventoryItemPurchaseLineChartState extends State<InventoryItemPurchaseLineChart> {
  @override
  void initState() {
    setYearListAndGetData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final typeDropDownButton =   Padding(
            padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
            child: DropdownButton<int>(
              dropdownColor: MyColors.white_chinese_e0_bg,
              value: selectedYear,
              icon: const Icon(Icons.keyboard_arrow_down),
              elevation: 8,
              borderRadius: BorderRadius.all(Radius.circular(8)),
              style: const TextStyle(color: Colors.black),
              underline: Container(
                height: 1,
                color: MyColors.white_chinese_e0_bg,
              ),
              onChanged: (int? newValue) {
                setState(() {
                  selectedYear=newValue!;
                   _getData(selectedYear);
                });
              },
              items: yearList.map<DropdownMenuItem<int>>((int value) {
                return DropdownMenuItem<int>(
                  value: value,
                  child: Text(value.toString()),
                );
              }).toList(),
            ));

    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
          color: MyColors.white_f1_bg,
          title: Text(
            'Sales Analysis',
            style: TextStyle(color: Colors.black),
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),actions: <Widget>[
            yearList.isNotEmpty?FadeIn(delay: Duration(milliseconds: 300),child:Card(color: MyColors.white_chinese_e0_bg,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                ),
                elevation: 0,
                child:Row(children: [
                  SizedBox(width: 5,),
              Icon(
                Icons.calendar_month_rounded,
                color: MyColors.grey_70,
              ),
              typeDropDownButton
            ],)),):Container()

        ],

        ),

        body:AspectRatio(
          aspectRatio: 1,
          child: Card(
            elevation: 0,
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
            color: const Color(0xff2c4260),
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      makeTransactionsIcon(),
                      const SizedBox(
                        width: 38,
                      ),
                      const Text(
                        'Transactions',
                        style: TextStyle(color: Colors.white, fontSize: 22),
                      ),
                      const SizedBox(
                        width: 4,
                      ),
                      const Text(
                        'state',
                        style:
                        TextStyle(color: Color(0xff77839a), fontSize: 16),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 38,
                  ),
                  Expanded(
                    child:flSpotList.isNotEmpty?FadeIn(child:  LineChart(
                      sampleData2,
                      swapAnimationDuration: const Duration(milliseconds: 250),
                    ),
                  ):Center(
                      child: Text(
                        'No Data Found.',
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Color(0xff77839a),
                        ),
                      ),
                    )),
                  const SizedBox(
                    height: 12,
                  ),
                ],
              ),
            ),
          ),
        )




        /*flSpotList.isNotEmpty?FadeIn(child: Column(mainAxisSize:MainAxisSize.max,crossAxisAlignment: CrossAxisAlignment.center,children: [
          SizedBox(height: 30,),
          Container(height : 300,width:MediaQuery.of(context).size.width-15,child: LineChart(
            sampleData2,
            swapAnimationDuration: const Duration(milliseconds: 250),
          ))

        ],)):Center(
          child: Text(
            'No Data Found.',
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.grey,
            ),
          ),
        ),*/
   );


  }

 /* LineChartData get sampleData2 => LineChartData(
        lineTouchData: lineTouchData2,
        gridData: gridData,
        titlesData: titlesData2,
        borderData: borderData,
        lineBarsData: lineBarsData2,
        minX: 0,
        maxX: 11,
        maxY: 10,
        minY: 0,
      );*/

  LineChartData get sampleData2 => LineChartData(
    lineTouchData: lineTouchData2,
    gridData: gridData,
    titlesData: titlesData2,
    borderData: borderData,
    lineBarsData: lineBarsData2,
    minX: 0,
    maxX: 11,
    maxY: max,
    minY: 0,
  );

  LineTouchData get lineTouchData2 => LineTouchData(
        enabled: false,
      );

  FlTitlesData get titlesData2 => FlTitlesData(
        bottomTitles: AxisTitles(
          sideTitles: bottomTitles,
        ),
        rightTitles: AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        topTitles: AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        leftTitles: AxisTitles(
          sideTitles: leftTitles(),
        ),
      );

  List<LineChartBarData> get lineBarsData2 => [
        // lineChartBarData2_1,
        //lineChartBarData2_2,
        lineChartBarData2_3,
      ];

  Widget leftTitleWidgets(double value, TitleMeta meta) {
    print('y_values $value');
    const style = TextStyle(
      color: Color(0xff77839a),
      fontWeight: FontWeight.bold,
      fontSize: 14,
    );
    String text;
    /*switch (value.toInt()) {
      case 0:
        text = '0';
        break;
      case intermediate.toInt():
        text = '2m';
        break;
      case 3:
        text = '3m';
        break;
      case 4:
        text = '5m';
        break;
      case 5:
        text = '6m';
        break;
      default:
        return Container();
    }*/
    /*if (value.toInt() == 0) {
      text = '0';
    } else if (value == intermediate.toInt()) {
      text = '${intermediate.toInt()}';
    } else if (value == max.toInt()) {
      text = '${max.toInt()}';
    } else {
      return Container();
    }*/

    text = '${value}';

    return Text(text, style: style, textAlign: TextAlign.center);
  }

  SideTitles leftTitles() => SideTitles(
        getTitlesWidget: leftTitleWidgets,
        showTitles: true,
        interval: intermediate,
        reservedSize: 40,
      );

  Widget bottomTitleWidgets(double value, TitleMeta meta) {
    print('x_values $value');
    const style = TextStyle(
      color: Color(0xff77839a),
      fontWeight: FontWeight.bold,
      fontSize: 16,
    );
    Widget text;
    text =  Text((value.toInt()+1).toString(), style: style);
    /*switch (value.toInt()) {
      case 0:
        text = const Text('Jan', style: style);
        break;
      case 1:
        text = const Text('Feb', style: style);
        break;
      case 2:
        text = const Text('Mar', style: style);
        break;
      case 3:
        text = const Text('Apr', style: style);
        break;
      case 4:
        text = const Text('May', style: style);
        break;
      case 5:
        text = const Text('Jun', style: style);
        break;
      case 6:
        text = const Text('Jul', style: style);
        break;

      case 7:
        text = const Text('Aug', style: style);
        break;
      case 8:
        text = const Text('Sep', style: style);
        break;
      case 9:
        text = const Text('Oct', style: style);
        break;
      case 10:
        text = const Text('Nov', style: style);
        break;
      case 11:
        text = const Text('Dec', style: style);
        break;
      default:
        text = const Text('');
        break;
    }*/

    return SideTitleWidget(
      axisSide: meta.axisSide,
      space: 10,
      child: text,
    );
  }

  SideTitles get bottomTitles => SideTitles(
        showTitles: true,
        reservedSize: 32,
        interval: 1,
        getTitlesWidget: bottomTitleWidgets,
      );

  FlGridData get gridData => FlGridData(show: false);

  FlBorderData get borderData => FlBorderData(
        show: true,
        border: const Border(
          bottom: BorderSide(color: Color(0xff4e4965), width: 4),
          left: BorderSide(color: Colors.transparent),
          right: BorderSide(color: Colors.transparent),
          top: BorderSide(color: Colors.transparent),
        ),
      );

/*  LineChartBarData get lineChartBarData2_1 => LineChartBarData(
        isCurved: true,
        curveSmoothness: 0,
        color: const Color(0x444af699),
        barWidth: 4,
        isStrokeCapRound: true,
        dotData: FlDotData(show: false),
        belowBarData: BarAreaData(show: false),
        spots: const [
          FlSpot(1, 1),
          FlSpot(3, 4),
          FlSpot(5, 1.8),
          FlSpot(7, 5),
          FlSpot(10, 2),
          FlSpot(12, 2.2),
          FlSpot(13, 1.8),
        ],
      );*/

  /*LineChartBarData get lineChartBarData2_2 => LineChartBarData(
        isCurved: true,
        color: const Color(0x99aa4cfc),
        barWidth: 4,
        isStrokeCapRound: true,
        dotData: FlDotData(show: false),
        belowBarData: BarAreaData(
          show: true,
          color: const Color(0x33aa4cfc),
        ),
        spots: *//*const [
          FlSpot(0, 0),
          FlSpot(3, 2),
          FlSpot(6, 1.5),
          FlSpot(8, 1.9),
          FlSpot(10, 3),
        ],*//* flSpotList
      );*/

  LineChartBarData get lineChartBarData2_3 => LineChartBarData(
        isCurved: true,
        curveSmoothness: 0,
        color: const Color(0x4427b6fc),
        barWidth: 2,
        isStrokeCapRound: true,
        dotData: FlDotData(show: true),
        belowBarData: BarAreaData(show: false),
        spots:/*const [
          FlSpot(0, 0),
          FlSpot(3, 2),
          FlSpot(6, 1.5),
          FlSpot(8, 1.9),
          FlSpot(10, 3)
        ]*/flSpotList,
      );
  double max = 1;
  double intermediate = 1;
  var productItemOperations = ProductItemOperations();

  var flSpotList = <FlSpot>[];
  final dateFormat = DateFormat("yyyy-MM-dd hh:mm:ss.SSS");

  List<int> yearList = <int>[];
int selectedYear=0;
  void setYearListAndGetData()async{
  final trialStartDate= await MyPref.getDbTrialStartDate();
  selectedYear=currentYear;

  if(trialStartDate!=null&& trialStartDate!=''){

    DateTime parseDate = dateFormat.parse(trialStartDate.toString());

    for(var i=currentYear; i>parseDate.year-1; i--){
      yearList.add(i);
    }

  }

  await _getData(selectedYear);
}
  Future _getData(int selectedYear) async {
    final activeSubscription= await Subscription.getActiveSubscription();
    if(activeSubscription==1||activeSubscription==2)

    await MyPref.getProject().then((project) async {
      if (project != null) {
        await inventoryItemOperations
            .inventoriesItemHistoryById(widget.inventoryItemId,selectedYear)
            .then((value) {
          // value.sort((a, b) => a.name!.compareTo(b.name!));
          flSpotList.clear();
          if (value.isNotEmpty){
            setState(() {
              //max=value.first.maxOfTotalType!.toDouble();
              if(value.first.itemType==0)
                max = value.map((e) => e.maxOfTotalType).reduce((value, element) => (value! > element!) ? value : element)!;

              else
                max = value.map((e) => e.historyInventoryItemTotalType?.toDouble()).reduce((value, element) => (value! > element!) ? value : element)!;

              if(max.toInt().isOdd)
                max+=1;
              intermediate = max>1 ? max / 2: 1;

              flSpotList= List<FlSpot>.generate(12, (i) => FlSpot(i.toDouble(), 0.0));
              value.forEach((element) {
                print('data_ ${element.name}  ${element.historyInventoryItemTotalType}  ${element.historyAddedStock}   ${element.historyAddedDate}   ${element.historyYear}   ${element.historyMonth}  ${element.maxOfTotalType}  ${element.historyStockConsumed}');
                if (element.itemType == 0)
                flSpotList[element.historyMonth!.toInt() - 1] =FlSpot(element.historyMonth!.toInt() - 1,element.historyAddedStock!.toDouble());
                else
                flSpotList[element.historyMonth!.toInt() - 1] =FlSpot(element.historyMonth!.toInt() - 1,element.historyInventoryItemTotalType!.toDouble());


              });

            });
            return;
          }else{

          setState(() {

          });}
        });
      } else {
        showCustomAlertDialog(context, 'Please create a project ');
      }
    });

    else
      showCustomCallbackAlertDialog(context: context, msg: 'No active subscription plan found, Please upgrade to pro to continue.',positiveText: 'Upgrade', positiveClick: ()async{
        Navigator.pop(context);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    UpgradeScreenState()) );
      }, negativeClick: (){
        Navigator.pop(context);
      });
  }

  Widget makeTransactionsIcon() {
    const width = 4.5;
    const space = 3.5;
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          width: width,
          height: 10,
          color: Colors.white.withOpacity(0.4),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 28,
          color: Colors.white.withOpacity(0.8),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 42,
          color: Colors.white.withOpacity(1),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 28,
          color: Colors.white.withOpacity(0.8),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 10,
          color: Colors.white.withOpacity(0.4),
        ),
      ],
    );
  }
}
