import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/model/ExtraCharge.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/MyCustomSlidableAction.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';

import 'database/extra_charge_operations.dart';
import 'editInventoryItem.dart';
import 'model/Project.dart';

class ExtraChargeList extends StatefulWidget {
  const ExtraChargeList({Key? key}) : super(key: key);

  @override
  State<ExtraChargeList> createState() => _ExtraChargeListState();
}

class _ExtraChargeListState extends State<ExtraChargeList> {
  var items = <ExtraCharge>[];
  var _scrollController = ScrollController();
  final teExpenseChargeController = TextEditingController();
  final teExpenseDetailsController = TextEditingController();
  var extraChargeItemOperations = ExtraChargeOperations();
  bool isLoading = true;

  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final expenseAddDateFormat = DateFormat('dd-MMM-yyyy hh:mm a');

  Project? selectedProject;

  @override
  void initState() {
    MyPref.getProject()
        .then((value) => value == null
        ?
    {
      setState(()
      {
        isLoading = false;
      }),
      showCustomAlertDialog(context, 'Please create a project ')
    }
        :
    {
      Future.delayed(Duration(milliseconds: 1000), ()
      {
        _getData();
      })
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context)
  {
    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
          titleSpacing: 10,
          elevation: 0,
          title: Text('Extra Charge List',
            style: TextStyle(color: Colors.black),),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.black,
            ),
            onPressed: () => Navigator.pop(context),
          ),
          actions: <Widget>
          [
            IconButton(
              icon: Icon(Icons.add, color: MyColors.black_0101,),
              onPressed: ()
              {
                if (selectedProject != null)
                {
                  openAlertBox(null, context);
                }
                else
                  showCustomAlertDialog(context, 'Please create a project ');
              },
            )
          ],),
        body:
        this.isLoading
            ?
        MyWidgets.buildCircularProgressIndicator()
            :
        ListView.builder(
            physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
            key: _listKey,
            controller: _scrollController,
            shrinkWrap: true,
            itemCount: items.length,
            itemBuilder: (BuildContext context, int index) {
              return _buildListItem(context, items[index], index);
            })
    );
  }

  ///Fetch data from database
  Future<List<ExtraCharge>> _getData() async
  {
    await MyPref.getProject().then((value) async
    {
      selectedProject = value;
      if (value != null)
      {
        await extraChargeItemOperations
            .extraChargesByProjectId(value.id)
            .then((value) {
              items = value;
            });
      }
      else
        {
          showCustomAlertDialog(context, 'Please create a project ');
        }
      setState(()
      {
        isLoading = false;
      });
    });
    return items;
  }

  Widget _buildListItem(BuildContext context, ExtraCharge values, int index) {
    return Padding(
        padding: EdgeInsets.only(left: 0, right: 10),
        child: MySlidableWidgets.slidable(
          // The end action pane is the one at the right or the bottom side.
            endActionPane: ActionPane(
              motion: BehindMotion(),
              extentRatio: 0.50,
              children: <Widget>[
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext)
                  {
                    openAlertBox(items[index], context);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 94,
                    color: MyColors.grey_9d_bg,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.recycling_outlined,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Edit',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                MyCustomSlidableAction(
                  flex: 1,
                  onPressed: (BuildContext slidableContext) {
                    deleteExtraCharge(values, context);
                  },
                  backgroundColor: MyColors.transparent,
                  foregroundColor: Colors.transparent,
                  child: MyWidgets.container(
                    marginL: 1,
                    height: 94,
                    color: MyColors.yellow_ff,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.grey_9d_bg,
                        blurRadius: 1.0,
                        offset: Offset(0.7, 0),
                      ),
                    ],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.close_rounded,
                          size: 20,
                          color: MyColors.white,
                        ),
                        Text(
                          'Delete',
                          style: MyStyles.customTextStyle(
                            fontSize: 11,
                            color: MyColors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),

            // The child of the Slidable is what the user sees when the
            // component is not dragged.
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 0),
              child: ListTile(
                  hoverColor: MyColors.transparent,
                  contentPadding: EdgeInsets.all(0),
                  title: MyWidgets.shadowContainer(
                      height: 95,
                      paddingL: 10,
                      // spreadRadius: 2,
                      cornerRadius: 10,
                      shadowColor: MyColors.white_f1_bg,
                      color: MyColors.white,//values.isSelected==1?MyColors.white_chinese_e0_bg:MyColors.white,
                      child: Row(
                        children: [
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                              flex: 9,
                              child: Container(
                                  margin: EdgeInsets.only(top: 5, bottom: 5),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Spacer(),
                                      Text(
                                        values.extraChargeDetails!.toString(),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: MyStyles.customTextStyle(
                                            fontSize: 15,
                                            color: MyColors.black,
                                            fontWeight: FontWeight.w500),
                                        textAlign: TextAlign.left,
                                      ),
                                      Spacer(),
                                      MyText.priceCurrencyText(context,
                                          price:
                                          'Cost: ${values.extraChargeCost}',
                                          fontSize: 15,
                                          fontWeight: FontWeight.normal,
                                          fontColor: MyColors.grey_9d_bg),
                                      Spacer(),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                        child: Align(
                                          alignment: Alignment.bottomRight,
                                          child: Text(
                                            '${expenseAddDateFormat.format(DateTime.fromMillisecondsSinceEpoch(values.extraChargeDate!))}',
                                            style: MyStyles.customTextStyle(
                                                fontSize: 13,
                                                fontWeight: FontWeight.w300),
                                            textAlign: TextAlign.right,
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ),
                                      Spacer(),
                                    ],
                                  ))
                          ),
                          // Container(
                          //   width: 45,
                          //   decoration: BoxDecoration(
                          //     color: MyColors.white_chinese_e0_bg,
                          //     borderRadius: BorderRadius.only(
                          //         topRight: Radius.circular(10),
                          //         bottomRight: Radius.circular(10)),
                          //   ),
                          //   child: Center(
                          //     child: Icon(
                          //       Icons.chevron_right_rounded,
                          //       size: 25,
                          //       color: MyColors.black,
                          //     ),
                          //   ),
                          // ),
                        ],
                      )
                  )
              ),
            )
        )
    );
  }

  // Widget createListView(BuildContext context)
  // {
  //   if (items.isNotEmpty) {
  //     return FadeInUp(
  //         child: Container(
  //             height: double.infinity,
  //             width: double.infinity,
  //             child: ListView.builder(
  //                 physics: AlwaysScrollableScrollPhysics(
  //                     parent: BouncingScrollPhysics()),
  //                 key: _listKey,
  //                 controller: _scrollController,
  //                 shrinkWrap: true,
  //                 itemCount: items.length,
  //                 itemBuilder: (BuildContext context, int index) {
  //                   //return _buildItem(context, items[index],index);
  //
  //                   return Dismissible(
  //                     background: Container(
  //                       color: (Colors.teal[900])!,
  //                       child: Padding(
  //                         padding: const EdgeInsets.all(15),
  //                         child: Row(
  //                           children: [
  //                             Icon(Icons.edit_outlined, color: Colors.white),
  //                             Text('Edit',
  //                                 style: TextStyle(color: Colors.white)),
  //                           ],
  //                         ),
  //                       ),
  //                     ),
  //                     secondaryBackground: Container(
  //                       color: Colors.red,
  //                       child: Padding(
  //                         padding: const EdgeInsets.all(15),
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.end,
  //                           children: [
  //                             Icon(Icons.delete_forever_outlined,
  //                                 color: Colors.white),
  //                             Text('Delete',
  //                                 style: TextStyle(color: Colors.white)),
  //                           ],
  //                         ),
  //                       ),
  //                     ),
  //                     key: Key(items[index].extraChargeId.toString()),
  //                     confirmDismiss: (DismissDirection direction) async {
  //                       if (direction == DismissDirection.endToStart) {
  //                         /*return await showDialog(
  //               context: context,
  //               builder: (BuildContext context) {
  //                 if(Platform.isIOS)
  //                 {
  //                   return  CupertinoAlertDialog(
  //                     //title: Text(msg),
  //                     title: const Text("Confirm"),
  //                     content: const Text("Are you sure you wish to delete this item?"),
  //                     actions: <Widget>[
  //                       CupertinoDialogAction(
  //                         child: Text('DELETE'),
  //                         onPressed: () => {onDelete(index),
  //                 },
  //                       ),
  //                       CupertinoDialogAction(
  //                         child: Text('CANCEL'),
  //                         onPressed: () => Navigator.of(context).pop(false),
  //                       ),
  //                     ],
  //                   );
  //                 }
  //                 else return AlertDialog(
  //                   shape: RoundedRectangleBorder(
  //                     borderRadius: BorderRadius.circular(15),
  //                   ),
  //                   title: const Text("Confirm"),
  //                   content: const Text("Are you sure you wish to delete this item?"),
  //                   actions: <Widget>[
  //                     TextButton(
  //                         onPressed: () => {onDelete(index),
  //                         },
  //                         child: const Text("DELETE")
  //                     ),
  //                     TextButton(
  //                       onPressed: () => Navigator.of(context).pop(false),
  //                       child: const Text("CANCEL"),
  //                     ),
  //                   ],
  //                 );
  //               },
  //             );*/
  //
  //                         return await showCustomCallbackAlertDialog(
  //                             context: context,
  //                             positiveText: 'Delete',
  //                             msg:
  //                                 'Are you sure you wish to delete all project related data?',
  //                             positiveClick: () {
  //                               onDelete(index);
  //                             },
  //                             negativeClick: () {
  //                               Navigator.of(context).pop(false);
  //                             });
  //                       } else {
  //                         openAlertBox(items[index], context);
  //                       }
  //                     },
  //                     child: _buildItem(context, items[index], index),
  //                   );
  //                 })));
  //   } else
  //     return isLoading
  //         ? MyWidgets.buildCircularProgressIndicator()
  //         : Center(
  //             child: Text(
  //               'No Extra charges added.',
  //               style: TextStyle(
  //                 fontSize: 17.0,
  //                 color: Colors.grey,
  //               ),
  //             ),
  //           );
  // }

  /// Delete Click and delete item
  onDelete(int index) async {
    var id = items[index].extraChargeId;

    await extraChargeItemOperations
        .deleteExtraChargeById(id!)
        .then((value) async {
      setState(() {
        items.removeAt(index);
      });
    });
    Navigator.of(context).pop(true);
  }

  ///Construct cell for List View
  // Widget _buildItem(BuildContext context, ExtraCharge values, int index)
  // {
  //   return Container(
  //     child: ListTile(
  //         title: Column(children: <Widget>[
  //       Row(
  //         children: <Widget>[
  //           Column(
  //             children: <Widget>[
  //               Icon(
  //                 Icons.money_rounded,
  //                 size: 40,
  //               ),
  //             ],
  //           ),
  //           Padding(padding: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0)),
  //           Column(
  //             crossAxisAlignment: CrossAxisAlignment.start,
  //             mainAxisSize: MainAxisSize.min,
  //             children: <Widget>[
  //               Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 0.0, 5.0)),
  //               Container(
  //                 width: 200,
  //                 child: RichText(
  //                   maxLines: 1,
  //                   overflow: TextOverflow.ellipsis,
  //                   strutStyle: StrutStyle(fontSize: 12.0),
  //                   text: TextSpan(
  //                     style: TextStyle(
  //                         fontWeight: FontWeight.w500,
  //                         fontSize: 15,
  //                         color: Colors.red),
  //                     text: '${values.extraChargeCost!} QR',
  //                   ),
  //                 ),
  //               ),
  //               Padding(padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),
  //               Text(
  //                 '${values.extraChargeDetails!}',
  //                 style: TextStyle(
  //                   fontWeight: FontWeight.w700,
  //                   fontSize: 15.0,
  //                   color: Colors.black,
  //                 ),
  //                 textAlign: TextAlign.left,
  //                 // maxLines: 2,
  //               ),
  //               Padding(padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0)),
  //               Text(
  //                 '${expenseAddDateFormat.format(DateTime.fromMillisecondsSinceEpoch(values.extraChargeDate!))}',
  //                 style: TextStyle(
  //                   fontWeight: FontWeight.w300,
  //                   fontSize: 15.0,
  //                   color: Colors.black,
  //                 ),
  //                 textAlign: TextAlign.left,
  //                 maxLines: 2,
  //               ),
  //               Padding(padding: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 10.0)),
  //             ],
  //           ),
  //         ],
  //       ),
  //       Divider(
  //         height: 2,
  //       )
  //     ])),
  //   );
  // }

  openAlertBox(ExtraCharge? extraCharge, BuildContext context) {
    print('jhjhjjhjj  $extraCharge');
    if (selectedProject == null) {
      showCustomAlertDialog(context, 'Please create a project ');
    } else {
      if (extraCharge != null) {
        teExpenseChargeController.text = extraCharge.extraChargeCost.toString();
        teExpenseDetailsController.text =
            extraCharge.extraChargeDetails.toString();
      } else {
        teExpenseChargeController.text = "";
        teExpenseDetailsController.text = "";
      }

      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(25.0))),
              contentPadding: EdgeInsets.only(top: 10.0),
              content: Container(
                width: 300.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          /*flag ?*/
                          "Extra Charge Details" /*: "Add User"*/,
                          style: TextStyle(fontSize: 20.0),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Divider(
                      color: Colors.grey,
                      height: 4.0,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 15.0, right: 15.0),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Flexible(
                                    flex: 0,
                                    fit: FlexFit.tight,
                                    child: Text('Charge',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                        )) //Container
                                    ),
                                SizedBox(
                                  width: 10,
                                ),
                                Flexible(
                                    flex: 1,
                                    fit: FlexFit.tight,
                                    child: TextFormField(
                                        keyboardType:
                                            TextInputType.numberWithOptions(
                                                decimal: true),
                                        inputFormatters: [
                                          FilteringTextInputFormatter.allow(
                                              RegExp(r'^(\d+)?\.?\d{0,2}')),
                                        ],
                                        controller: teExpenseChargeController,
                                        obscureText: false,
                                        decoration: InputDecoration(
                                          hintText: "Paid Amount",
                                        )) //Container
                                    )
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Flexible(
                                    flex: 0,
                                    fit: FlexFit.tight,
                                    child: Text('Details',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                        )) //Container
                                    ),
                                SizedBox(
                                  width: 10,
                                ),
                                Flexible(
                                    flex: 1,
                                    fit: FlexFit.tight,
                                    child: TextFormField(
                                        keyboardType: TextInputType.text,
                                        controller: teExpenseDetailsController,
                                        obscureText: false,
                                        decoration: InputDecoration(
                                          hintText: "Expense details",
                                        )) //Container
                                    )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    InkWell(
                      onTap: () => {
                        if (_validate())
                          {
                            extraCharge != null
                                ? editUser(extraCharge, context)
                                : addExpense()
                          }
                      },
                      child: Container(
                        padding: EdgeInsets.only(top: 12.0, bottom: 12.0),
                        decoration: BoxDecoration(
                          color: MyColors.white_chinese_e0_bg,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(20.0),
                              bottomRight: Radius.circular(20.0)),
                        ),
                        child: Text(
                          extraCharge == null ? "Save Expense" : "Edit Expense",
                          style: TextStyle(color: MyColors.grey_9d_bg),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          });
    }
  }

  bool _validate() {
    if (teExpenseChargeController.text.isEmpty ||
        double.tryParse(teExpenseChargeController.text.trim()) == 0) {
      context.showSnackBar("Enter Expense Amount.");
      return false;
    } else if (teExpenseDetailsController.text.isEmpty) {
      context.showSnackBar("Enter Expense Details.");
      return false;
    }
    return true;
  }

  addExpense() async {
    if (selectedProject != null) {
      var expenseCharge = ExtraCharge();
      expenseCharge.projectId = selectedProject?.id;
      expenseCharge.extraChargeDetails = teExpenseDetailsController.text.trim();
      expenseCharge.extraChargeCost =
          double.parse(teExpenseChargeController.text.trim());

      await extraChargeItemOperations
          .insertExtraCharge(expenseCharge)
          .then((value) {
        clearControllerAndPop();
      });
    } else {
      context.showSnackBar("Select project");
    }
    // print('vkbvfvb ${expenseCharge.wagePerMinNew}  ${(((expenseCharge.salary)!/(expenseCharge.hoursPerDay!*30))/60)}');
  }

  ///edit Extra Charge
  editUser(ExtraCharge extraCharge, BuildContext context) async {
    await MyPref.getProject().then((value) async {
      if (value != null) {
        print('iiiiiiiii $extraCharge  ${teExpenseDetailsController.text}');
        var extra = ExtraCharge();
        extra.extraChargeId = extraCharge.extraChargeId;
        extra.extraChargeCost = double.parse(teExpenseChargeController.text);
        extra.extraChargeDetails =
            teExpenseDetailsController.text.toString().trim();
        extra.projectId = value.id;

        await extraChargeItemOperations
            .updateExtraCharge(extra)
            .then((value) async {
          clearControllerAndPop();
        });
      }
    });
  }

  deleteExtraCharge(ExtraCharge extraCharge, BuildContext context) async {
    var extra = ExtraCharge();
    extra.extraChargeId = extraCharge.extraChargeId;
    await extraChargeItemOperations
        .deleteExtraChargeById(extra.extraChargeId!)
        .then((value) async {
      clearControllerAndPop();
    });
  }

  clearControllerAndPop() async {
    teExpenseChargeController.text = '';
    teExpenseDetailsController.text = '';
    Navigator.of(context).pop();
    showtoast("Data Saved successfully");
    _getData();
  }
}
