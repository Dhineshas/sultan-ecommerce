import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/inventoryCategoryList.dart';
import 'package:ecommerce/settings.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MoreScreen extends StatefulWidget {
  @override
  _MoreScreenState createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  var _scrollController = ScrollController();
  @override
  void setState(VoidCallback fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MyColors.white_f1_bg,
        body: FadeInUp( child:Container(
            height: double.infinity,
            width: double.infinity,child: ListView(
          controller: _scrollController,
          padding: EdgeInsets.zero,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: ListTile(
                    leading: Icon(Icons.cloud_upload_outlined),
                    title: Text('Inventory List'),
                    onTap: () {
                      goto(context,InventoryCategoryListScreenState());
                    })),
            Divider(
              height: 1,
            ),
            ListTile(
                leading: Icon(Icons.settings_outlined),
                title: Text('settings'),
                onTap: () {
                  goto(context,SettingsScreenState());
                }),
            Divider(
              height: 1,
            ),
          ],
        ))));
  }


}
