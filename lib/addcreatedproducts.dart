import 'dart:convert';
import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/database/product_item_operations.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';
import 'database/storage_operations.dart';
import 'model/ProductItem.dart';
import 'model/InventoryRelation.dart';
import 'model/Storage.dart';

class AddCreatedProductsScreenState extends StatefulWidget {

  final List<int>? productItemIdArray;
  const AddCreatedProductsScreenState({@required this.productItemIdArray, Key? key})
      : super(key: key);

  @override
  _AddCreatedProductsScreenState createState() =>
      _AddCreatedProductsScreenState();
}

class _AddCreatedProductsScreenState extends State<AddCreatedProductsScreenState> {
  @override
  void initState() {

    Future.delayed( Duration(milliseconds: 1000), () {
      _getData();

    });
    super.initState();
    print("INIT");

    /*myFuture = _getData();*/
  }

  @override
  Widget build(BuildContext context) {
    return  MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
          leading:  IconButton(
              icon:  Icon(Icons.arrow_back_ios_new_rounded,color: MyColors.black,),
              onPressed: () {
                Navigator.pop(context);
              }
          ),
          title: Text('Select Products',
            style: TextStyle(color: Colors.black),
          ),

          actions: <Widget>[
           IconButton(
             icon: MyWidgets.textView(text: 'Done',style: MyStyles.customTextStyle(fontSize: 15,fontWeight: FontWeight.w500)),
             onPressed: () {
               clickSavePressed();
             },
           )
          ],
        ),
        body:items.isNotEmpty
            ? /*ListView.builder(
            key: _listKey,
            controller: _scrollController,
            shrinkWrap: true,
            itemCount: items.length,
            itemBuilder: (BuildContext context, int index) {
              return _buildItem(context, items[index]);
            })*/

        FadeIn(child:Container(
            height: double.infinity,
            width: double.infinity,child: ListView.builder(physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),itemCount: productCategory.length,itemBuilder: (context, headerIndex) {
          return StickyHeader(
              header: Container(
                height: 40.0,
                color: Colors.grey,
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                alignment: Alignment.centerLeft,
                child: MyWidgets.textView(text: '${productCategory[headerIndex]}',style: MyStyles.customTextStyle(fontSize: 14,color: MyColors.white)),

              ),
              content: Column(
                children: List<ProductItem>.generate(items.where((element) => productCategory[headerIndex]==element.productCategoryName).length, (index) => items.where((element) => productCategory[headerIndex]==element.productCategoryName).elementAt(index)).map((item) =>

                _buildListItem(item))
                    .toList(),
              )

          );})))
            :isLoading?MyWidgets.buildCircularProgressIndicator():  Center(child: Text(
          'No Products.',
          style: TextStyle(
            fontSize: 17.0,
            color: Colors.grey,
          ),
        ),)
    );
  }

  var productCategory = <String>[];
  var items = <ProductItem>[];
  bool insertItem = false;

  var storageItemOperations = StorageOperations();
  final teNameController = TextEditingController();
  var productItemOperations = ProductItemOperations();
  var relations = <InventoryRelation>[];
  bool isLoading = true;



  ///Fetch data from database
  Future<List<ProductItem>> _getData() async {
    await MyPref.getProject().then((project)async {
      if(project!=null){

        await productItemOperations.productItemsWithoutProductItemIdArrayWithCategoryNameByProjectId(widget.productItemIdArray!,project.id).then((value) {
          items = value;
          //Product list for edit product item
          items.forEach((productItem) async{
            if(!productCategory.contains(productItem.productCategoryName)){
              productCategory.add(productItem.productCategoryName.toString());
            }
          });
        });
      }else
        showCustomAlertDialog(context, 'Please create a project ');});



      /*for (var productItem in items) {
        if(!productCategory.contains(productItem.productCategoryName)){
          productCategory.add(productItem.productCategoryName.toString());
        }

    }*/

    setState(() {
      isLoading=false;
    });
    return items;
  }

  Widget _buildListItem(ProductItem values) {

    return InkWell(
        onTap: (){
          onItemClick(values);
        },child:Container(
        margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
        child: Column(
          children: <Widget>[
            Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  //image
                  /*Container(
                    margin: EdgeInsets.fromLTRB(5, 5, 10, 5),
                    child: values.imageBase != null &&
                        values.imageBase.toString().isNotEmpty
                        ? ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.memory(
                        base64Decode(values.imageBase.toString()),
                        width: 50,
                        height: 50,
                        fit: BoxFit.fitHeight,
                        gaplessPlayback: true,
                      ),
                    )
                        : Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(10)),
                      width: 50,
                      height: 50,
                    ),
                  ),*/
                  Container(
                    margin: EdgeInsets.all(4.0),
                    child: values.imageBase != null &&
                        values.imageBase.toString().isNotEmpty
                        ? ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.memory(
                        base64Decode(values.imageBase.toString()),
                        width: 40,
                        height: 40,
                        gaplessPlayback: true,
                        fit: BoxFit.fitHeight,
                      ),
                    )
                        : Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(10)),
                      width: 40,
                      height: 40,
                    ),
                  ),
                  SizedBox(width: 5,),
                  /*Text(
                    values.name!,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17.0,
                      color: Colors.black,
                    ),
                    textAlign: TextAlign.left,
                    maxLines: 2,
                  ),*/
                  MyWidgets.textView(text: values.name!,style: MyStyles.customTextStyle(fontSize: 14,fontWeight: FontWeight.w500),textAlign: TextAlign.left,),

                      Spacer(), IconButton(
                      color:values.checkedState == 1? Colors.black: Colors.transparent,
                      icon: new Icon(Icons.check),
                      onPressed: () => onItemClick(values)),


                  //name and count
                ]),Divider(height: 1),
          ],
        )));
  }
  ///On Item Click
  onItemClick(ProductItem values) {


    /*if(widget.productItem==null){*/
    setState(() {
      if (values.checkedState == 0) {
        values.checkedState = 1;
      } else {
        values.checkedState = 0;
      }
      print("Clicked position is ${values.name} and state is ${values.checkedState}");//  ${values.checkedState}");


    });
    }

  clickSavePressed()async{
    items.forEach((element)async {
      if(element.checkedState==1)
      {
        var storage = Storage();
        storage.productItemId = element.id;
        storage.quantity = 1;
        await storageItemOperations.insertStorage(storage);
      }
    });
    Navigator.pop(context, true);
  }

}
