import 'dart:convert';
import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/database/project_operations.dart';
import 'package:ecommerce/model/Inventory.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'database/inventory_operations.dart';
import 'package:image_picker/image_picker.dart';

import 'database/user_operation.dart';
import 'model/Project.dart';


class FirebaseAddFile extends StatefulWidget {
  final Project? project;
   FirebaseAddFile({@required this.project,Key? key}) : super(key: key);

  @override
  _FirebaseAddFile createState() => _FirebaseAddFile();
}

class _FirebaseAddFile extends State<FirebaseAddFile> {

  File? _image;

  final picker = ImagePicker();
  var projectItemImageBase64;
  @override
  void setState(VoidCallback fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final imageView =
    GestureDetector(
      onTap: () {
        _showPicker(context);

      },
      child: Container(
            decoration: BoxDecoration(
              // color: Colors.grey[200],
                borderRadius: BorderRadius.circular(75)
            ),
            height: 104,
            width: 104,
            child: projectItemImageBase64 != null?
            ClipRRect(
              borderRadius: BorderRadius.circular(75),
              child: Image.memory(
                base64Decode('${projectItemImageBase64}'),
                width: 100,
                height: 100,
                fit: BoxFit.cover,
              ),
            ):_image != null
                ? ClipRRect(
              borderRadius: BorderRadius.circular(75),
              child: Image.file(
                _image!,
                width: 100,
                height: 100,
                fit: BoxFit.cover,
              ),
            )
                : Container(
              decoration: BoxDecoration(
                // color: Colors.grey[200],
                  border: Border.all(color: Colors.black,width: 1),
                  borderRadius: BorderRadius.circular(75)
              ),
              width: 100,
              height: 100,
              child: Icon(
                Icons.image_rounded,
                color: Colors.grey[800],
              ),
            ),
          ));
    final createProjectTextView =  Align(alignment: Alignment.centerLeft,
      child: MyWidgets.textView(text: 'Upload File',
        style: MyStyles.customTextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 24,
        ),
      ),
    );

    final addPrjButton = MyWidgets.materialButton(context,text:  widget.project==null?"Create Project":"Update Project",onPressed:() { if(_validate()){
      if(_validate()){
        updateFile();
      }   }});
    return Scaffold(
      backgroundColor:  Colors.transparent,

        body:  SingleChildScrollView(
            padding: EdgeInsets.only(left: 30, right: 30, top: 25, bottom: 20),
            physics: BouncingScrollPhysics(),
              child: Column(
                children: <Widget>[
                  createProjectTextView,
                  SizedBox(
                    height: 30,
                  ),
                  imageView,

                  SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  addPrjButton
                ],
              ))
    );
  }

  void _showPicker(context) {
    /*showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  ListTile(
                    leading: Icon(Icons.photo_camera),
                    title: Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });*/
    MyImagePickerBottomSheet.showPicker(context, fileCallback: (image) {
      projectItemImageBase64 = null;
      setState(() {
        _image = image!=null?File(image.path):null;
      });
      return null;
    });
  }








 /* _imgFromCamera() async {
    var image =
    await picker.pickImage(source: ImageSource.camera, imageQuality: 0,maxHeight:150,maxWidth: 150);
    projectItemImageBase64 = null;
    setState(() {
      _image = image!=null?File(image.path):null;
    });
  }

  _imgFromGallery() async {
    var image =
    await picker.pickImage(source: ImageSource.gallery, imageQuality: 0,maxHeight:150,maxWidth: 150);
    projectItemImageBase64 = null;
    setState(() {
      _image = image!=null?File(image.path):null;


    });
  }*/

  bool _validate()  {
    if (projectItemImageBase64 == null && _image == null) {
      context.showSnackBar("Select Project Image");
      return false;
    }

    return true;
  }



  updateFile() async{

  }

}
