import 'package:ecommerce/model/UserCredentials.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/FirebaseUtils.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'main.dart';

class DatabaseScreenState extends StatefulWidget {
  const DatabaseScreenState({Key? key}) : super(key: key);

  @override
  _DatabaseScreenState createState() => _DatabaseScreenState();
}

class _DatabaseScreenState extends State<DatabaseScreenState> {
  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    getLatestDatabaseData();
    super.initState();
  }

  Future getLatestDatabaseData()async {
   await FirebaseUtils()
        .getLastDatabaseUploadedDate(context)
        .then((value) => value != null ? setUI(context, value) : null);
  }

  var lastUploadedDate = '';
  var lastUploadedDeviceId = '';
  var lastUploadedDeviceModel = '';

  setUI(BuildContext context, FullMetadata metaData) {
    final mydata = metaData.customMetadata;
    setState(() {
      this.lastUploadedDeviceId = mydata!['last-device'] ?? ''.toString();
      this.lastUploadedDeviceModel = mydata['last-device-details'] == null ||
              mydata['last-device-details'] == ''
          ? ''
          : '(${mydata['last-device-details'] ?? ''.toString()})';
      this.lastUploadedDate = metaData.updated.toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
          title: Text(
            'Database Details',
            style: TextStyle(color: Colors.black),
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.black,
            ),
            onPressed: () => Navigator.pop(context, true),
          ),
        ),
        body: Container(
            height: MediaQuery.of(context).size.height,
            child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(
                    parent: BouncingScrollPhysics()),
                padding:
                    EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
                child:
                    Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
                  showDatabaseInfo(),
                  SizedBox(
                    height: 20,
                  ),
                  showDatabaseActions()
                ]))));
  }

  Widget showDatabaseInfo() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(child: const Text('Last Uploaded Date')),
            Expanded(child: Text(this.lastUploadedDate)),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(child: const Text('Last Uploaded Device')),
            Expanded(
                child: Text(
                    '${this.lastUploadedDeviceId} ${this.lastUploadedDeviceModel}')),
          ],
        ),
      ],
    );
  }

  Widget showDatabaseActions() {
    final uploadButton = Expanded(
        flex: 1,
        child: Material(
            elevation: 1,
            borderRadius: BorderRadius.circular(8),
            color: MyColors.white_chinese_e0_bg,
            child: MaterialButton(
                onPressed: () async {
                  await showCustomCallbackAlertDialog(
                    context: context,
                    msg:
                        'Your Database was last synced on $lastUploadedDate. \nPlease make sure you have synced the latest Data from the last device you have used, and click Upload.',
                    positiveText: 'Upload',
                    positiveClick: () async {
                      Navigator.of(context).pop();
                      await FirebaseUtils()
                          .firebaseUploadFile(context)
                          .then((value)async {
                       await this.getLatestDatabaseData();
                      });
                    },
                    negativeText: 'Cancel',
                    negativeClick: () {
                      Navigator.of(context).pop();
                    },
                  );
                },
                child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      MyWidgets.textView(text: 'Sync Now'),
                      SizedBox(
                        width: 5,
                      ),
                      Icon(
                        Icons.cloud_upload,
                        size: 20,
                      )
                    ]))));
    final downloadButton = Expanded(
        flex: 1,
        child: Material(
            elevation: 1,
            borderRadius: BorderRadius.circular(8),
            color: MyColors.grey_607,
            child: MaterialButton(
                onPressed: () async {
                  showCustomCallbackAlertDialog(
                    context: context,
                    msg:
                        'Your Database was last synced on $lastUploadedDate. \nPlease make sure you have synced the latest Data from the last device you have used, and click Download.',
                    positiveText: 'Download',
                    positiveClick: () async {
                      Navigator.of(context).pop();
                      await downloadDb();
                    },
                    negativeText: 'Cancel',
                    negativeClick: () {
                      Navigator.of(context).pop();
                    },
                  );
                },
                child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      MyWidgets.textView(text: 'Download Now'),
                      SizedBox(
                        width: 5,
                      ),
                      Icon(
                        Icons.cloud_download,
                        size: 20,
                      )
                    ]))));

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        uploadButton,
        SizedBox(
          width: 10,
        ),
        downloadButton
      ],
    );
  }

  Future downloadDb() async {
    await FirebaseUtils().firebaseDownloadFile(context, callback: (taskState) {
      if (taskState == TaskState.success) {
        showCustomOkCallbackAlertDialog(
            context: context,
            msg: 'Downloaded successfully',
            positiveClick: () async {

              Navigator.pop(context);
             await Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => MyHomePage(),
                ),
                (route) => false,
              );
            },
            negativeClick: () {
              Navigator.pop(context);
            });
      }
    });
  }
}
