import 'dart:convert';
import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/model/Inventory.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'inventoryUnitMeasures.dart';
import 'database/inventory_item_operations.dart';
import 'editInventoryItem.dart';
import 'model/InventoryItem.dart';
import 'package:image_picker/image_picker.dart';

class InventoryItemDetailsScreenState extends StatefulWidget {
  //final Inventory? inventory;
  final int? inventoryItemId;


  const InventoryItemDetailsScreenState({/*@required this.inventory,*/@required this.inventoryItemId, Key? key})
      : super(key: key);

  @override
  _InventoryItemDetailsScreenState createState() => _InventoryItemDetailsScreenState();
}

class _InventoryItemDetailsScreenState extends State<InventoryItemDetailsScreenState> {
  File? _image;
  var inventoryItemOperations = InventoryItemOperations();
  int? selectedTypeIndex = 0;
  String? selectedTypeValue = '';
  String? dropdownSelectedTypeValue =  InventoryUnitMeasure.inventoryUnitMeasureArray[0];
  var productItemImageBase64;
  double? balanceStock=0.0;
  int? integerBalanceStock = 0;
  int? decimalBalanceStock = 0;
  bool isDataEditedAndPop=false;
  var unitMeasure = InventoryUnitMeasure();

  @override
  void setState(VoidCallback fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  @override
  void initState() {
    if(widget.inventoryItemId!=null){
     /* productItemImageBase64 = widget.inventoryItem?.imageBase;
      selectedTypeIndex=widget.inventoryItem?.itemType;
      dropdownSelectedTypeValue= InventoryTypeArray.inventoryTypeArray[selectedTypeIndex!];
      balanceStock=(widget.inventoryItem?.stock)!/(widget.inventoryItem?.piecesInType)!;
      integerBalanceStock=balanceStock?.toInt();
      decimalBalanceStock=((widget.inventoryItem?.stock)!%(widget.inventoryItem?.piecesInType)!).toInt();*/

      Future.delayed( Duration(milliseconds: 1000), () {
        _getData();

      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final inventoryItemText =
    Align(alignment: Alignment.centerLeft,
      child: MyWidgets.textView(text: 'Inventory Item',
        style: MyStyles.customTextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 24,
        ),
      ),
    );
    final imageView =
   /*Padding(
            padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(10)),
              height: 150,
              width: MediaQuery.of(context).size.width,
              child: productItemImageBase64 != null? ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.memory(
                  base64Decode('$productItemImageBase64'),
                  width: 100,
                  height: 100,
                  gaplessPlayback: true,
                  fit: BoxFit.fitHeight,
                ),
              ):_image != null
                  ? ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.file(
                        _image!,
                        width: 100,
                        height: 100,
                        fit: BoxFit.fitHeight,
                        gaplessPlayback: true,
                      ),
                    )
                  : Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(10)),
                      width: 100,
                      height: 100,
                      child: Icon(
                        Icons.camera_alt,
                        color: Colors.grey[800],
                      ),
                    ),
            ))*/
    Container(
      decoration: BoxDecoration(
        // color: Colors.grey[200],
          borderRadius: BorderRadius.circular(75)
      ),
      height: 104,
      width: 104,
      child: productItemImageBase64 != null?
      ClipRRect(
        borderRadius: BorderRadius.circular(75),
        child: Image.memory(
          base64Decode('$productItemImageBase64'),
          width: 100,
          height: 100,
          fit: BoxFit.cover,
        ),
      ):_image != null
          ? ClipRRect(
        borderRadius: BorderRadius.circular(75),
        child: Image.file(
          _image!,
          width: 100,
          height: 100,
          fit: BoxFit.cover,
        ),
      )
          : Container(
        decoration: BoxDecoration(
          // color: Colors.grey[200],
            border: Border.all(color: Colors.black,width: 1),
            borderRadius: BorderRadius.circular(75)
        ),
        width: 100,
        height: 100,
        child: Icon(
          Icons.image_rounded,
          color: Colors.grey[800],
        ),
      ),
    );

    final inventoryItemNameText =/*FadeInUp(delay: Duration(milliseconds: 200),child:  Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Name',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 15,
                color: Colors.grey,
              ),
            ),SizedBox(
              height: 10,
            ),

            Text(
              '${item?.name}',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 18,
                color: Colors.black,
              ),
            )
          ],
        )))*/Column(
      mainAxisSize: MainAxisSize.max,
       // mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text: 'Item Name'),
          SizedBox(height: 10,),
          MyWidgets.textViewContainer(
paddingL: 15.0,paddingR: 15.0,
            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child:
            SizedBox.expand(
              child:Align(alignment:Alignment.centerLeft,child:MyWidgets.textView(text: '${item?.name}',textAlign: TextAlign.left,style: MyStyles.customTextStyle(fontWeight: FontWeight.w500,
                fontSize: 16,
                ))),
            ),

          ),
        ]
    );
    final addDropDownButton =
    MyWidgets.textViewContainer(

        height: 45, cornerRadius: 8, color: MyColors.white,
        // color: Colors.white54,
        child: Padding(
              padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
              child: DropdownButton<String>(
                value: dropdownSelectedTypeValue,
                icon: const Icon(Icons.keyboard_arrow_down),
                elevation: 8,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                style: const TextStyle(color: Colors.black),
                /*underline: Container(
                  height: 2,
                  color: Colors.blue,
                ),*/
                onChanged: (String? newValue) {
                  setState(() {
                    //selectedTypeIndex = InventoryUnitMeasure.inventoryUnitMeasureArray.indexOf(newValue!);
                    dropdownSelectedTypeValue=newValue;
                    unitMeasure.convert(balanceStock!,selectedTypeValue! , newValue!).then((value) {

                      if(value==0){
                        dropdownSelectedTypeValue=selectedTypeValue;
                        showCustomAlertDialog(context,'This conversion cannot be performed');
                        return;
                      }
                      if(selectedTypeIndex!=InventoryUnitMeasure.inventoryUnitMeasureArray.indexOf(newValue))
                        showCustomAlertDialog(context,'$newValue $value');

                    });


                  });
                },
                items: InventoryUnitMeasure.inventoryUnitMeasureArray
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              )));

    final inventoryType =
    Row(crossAxisAlignment: CrossAxisAlignment.center,mainAxisSize: MainAxisSize.max,children: [
          /*Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Type',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 15,
                  color: Colors.grey,
                ),
              ),SizedBox(
                height: 10,
              ),

              Text(
                '$selectedTypeValue',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                  color: Colors.black,
                ),
              )
            ],
          )*/
      /*Column(
            mainAxisSize: MainAxisSize.max,
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10,),
              MyWidgets.textView(text: 'Item Type'),
              SizedBox(height: 10,),
              MyWidgets.textViewContainer(
                paddingL: 20.0,paddingT: 15.0,paddingR: 20.0,paddingB: 15.0,
                height: 45, cornerRadius: 8, color: MyColors.white,
                // color: Colors.white54,
                child:
               MyWidgets.textView(text: '$selectedTypeValue',textAlign: TextAlign.left,style: MyStyles.customTextStyle(fontWeight: FontWeight.w500,
                    fontSize: 16,
                  ),
                ),

              ),
            ]
        )*/Expanded(flex:1,child: Column(
          mainAxisSize: MainAxisSize.max,
          // mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10,),
            MyWidgets.textView(text: 'Item Type'),
            SizedBox(height: 10,),
            MyWidgets.textViewContainer(
                paddingL: 15.0,paddingR: 15.0,
              height: 45, cornerRadius: 8, color: MyColors.white,
              // color: Colors.white54,
              child:
              SizedBox.expand(
                child:Align(alignment:Alignment.centerLeft,child: MyWidgets.textView(text: '$selectedTypeValue',textAlign: TextAlign.left,style: MyStyles.customTextStyle(fontWeight: FontWeight.w500,
                fontSize: 16,
              ),
              )),)

            ),
          ]
      )),SizedBox(width: 10,),Column(
          mainAxisSize: MainAxisSize.max,
          // mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10,),
            MyWidgets.textView(text: 'Convert'),
            SizedBox(height: 10,),
            addDropDownButton,

          ]
      )


        ],);




    final totalBoxCountEditTxt = /*FadeInUp(delay: Duration(milliseconds: 400),child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
               item?.isPiecesInTypeUpdated==0?'Total $selectedTypeValue':'Total Stock',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 15,
                color: Colors.grey,
              ),
            ),SizedBox(
              height: 10,
            ),Text(
             selectedTypeIndex==0? item?.isPiecesInTypeUpdated==0? '$integerBalanceStock $selectedTypeValue\'s $decimalBalanceStock Piece\'s':'${item?.stock} Items':'$balanceStock $selectedTypeValue',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 18,
                color: Colors.black,
              ),
            ),
          ],
        )))*/

    Column(
        mainAxisSize: MainAxisSize.max,
        // mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text:
          item?.isPiecesInTypeUpdated==0? 'Total Boxes':'Total Stock'
          //item?.isPiecesInTypeUpdated==0?'Total $selectedTypeValue':'Total Stock'

            ,),
          SizedBox(height: 10,),
          MyWidgets.textViewContainer(
            paddingL: 15.0,paddingR: 15.0,
            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child:
            SizedBox.expand(
              child:Align(alignment:Alignment.centerLeft,child:MyWidgets.textView(text:
              //selectedTypeIndex==0? item?.isPiecesInTypeUpdated==0? '$integerBalanceStock $selectedTypeValue\'s $decimalBalanceStock Piece\'s':'${item?.stock} Items':'$balanceStock $selectedTypeValue',
              item?.isPiecesInTypeUpdated==0?'${integerBalanceStock} Boxes ':'${item?.stock} Items',
    textAlign: TextAlign.left,style: MyStyles.customTextStyle(fontWeight: FontWeight.w500,
                fontSize: 16,
              ))),
            ),

          ),
        ]
    );
    final piecesInBox =/*FadeInUp(delay: Duration(milliseconds: 500),child:  Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Pieces in the $selectedTypeValue',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 15,
                color: Colors.grey,
              ),
            ),SizedBox(
              height: 10,
            ),
            Text(
              '${item?.piecesInType}',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 18,
                color: Colors.black,
              ),
            ),
          ],
        )))*/
    Column(
        mainAxisSize: MainAxisSize.max,
        // mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text:  '$dropdownSelectedTypeValue in a box'
          //'Pieces in the $selectedTypeValue'
            ,),
          SizedBox(height: 10,),
          MyWidgets.textViewContainer(
            paddingL: 15.0,paddingR: 15.0,
            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child:
            SizedBox.expand(
              child:Align(alignment:Alignment.centerLeft,child:MyWidgets.textView(text: '${item?.piecesInType}',textAlign: TextAlign.left,style: MyStyles.customTextStyle(fontWeight: FontWeight.w500,
                fontSize: 16,
              ))),
            ),

          ),
        ]
    );
    final costPerBox = /*FadeInUp(delay: Duration(milliseconds: 600),child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Cost per $selectedTypeValue',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 15,
                color: Colors.grey,
              ),
            ),SizedBox(
              height: 10,
            ),Text(
              selectedTypeIndex==0?'${item?.costPerTypeNew}':'${item?.costPerItemInTypeNew}',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 18,
                color: Colors.black,
              ),
            )
          ],
        )))*/
    Column(
        mainAxisSize: MainAxisSize.max,
        // mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text: 'Cost per $selectedTypeValue',),
          SizedBox(height: 10,),
          MyWidgets.textViewContainer(
            paddingL: 15.0,paddingR: 15.0,
            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child:
            SizedBox.expand(
              child:Align(alignment:Alignment.centerLeft,child:MyWidgets.textView(text:  selectedTypeIndex==0?'${item?.costPerTypeNew}':'${item?.costPerItemInTypeNew}',textAlign: TextAlign.left,style: MyStyles.customTextStyle(fontWeight: FontWeight.w500,
                fontSize: 16,
              ))),
            ),

          ),
        ]
    );

    return Scaffold(

      /*appBar: AppBar(
        elevation: 0,
        title: MyWidgets.textView(
          text: 'Inventory Item',
              style: MyStyles.customTextStyle(fontWeight: FontWeight.w500,fontSize: 16)
        ),
        centerTitle: true,
        backgroundColor: MyColors.white,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, isDataEditedAndPop),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.edit_outlined,
              color: MyColors.grey_9d_bg,
              size: 16,
            ),
            onPressed: () {
              // do something
              if(item!=null)
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => EditInventoryItemScreenState(inventory: widget.inventory,inventoryItem: item,))).then((value) => {value==true? {isDataEditedAndPop=value,
                      _getData()} :{}});
            },
          )
        ],
      ),*/
      body:item!=null?FadeIn(child: SingleChildScrollView(

          padding: EdgeInsets.only(left: 30, right: 30, top: 25, bottom: 20),

              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  inventoryItemText,
                  SizedBox(
                    height: 25,
                  ),
                  imageView,

                  SizedBox(
                    height: 15,
                  ),
                  inventoryItemNameText,

                  inventoryType,


                  totalBoxCountEditTxt,

                  piecesInBox,

                  costPerBox,
                  SizedBox(
                    height: 20,
                  ),

                ],
              ))):isLoading?MyWidgets.buildCircularProgressIndicator():FadeIn(child:   Center(child: MyWidgets.textView(text:'No Details.',
        style: MyStyles.customTextStyle(fontSize: 16,fontWeight: FontWeight.w500),
      ),)
    ));
  }
  bool isLoading = true;
   InventoryItem? item;
  ///Fetch data from database
  Future<InventoryItem> _getData() async {
    await inventoryItemOperations.inventoriesItemsByInventoryItemId(widget.inventoryItemId).then((value) {
     item=value.first;
     setState(() {
     productItemImageBase64 = item?.imageBase;
     selectedTypeIndex=item?.itemType;
     selectedTypeValue= InventoryUnitMeasure.inventoryUnitMeasureArray[selectedTypeIndex!];
     dropdownSelectedTypeValue= InventoryUnitMeasure.inventoryUnitMeasureArray[selectedTypeIndex!];
     balanceStock=(item?.stock)!/(item?.piecesInType)!;
     integerBalanceStock=balanceStock?.toInt();
     decimalBalanceStock=((item?.stock)!%(item?.piecesInType)!).toInt();
     isLoading = false;
    });


    });
    return item!;
}
}
