import 'dart:convert';
import 'dart:io';
import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/model/Project.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'database/cart_operations.dart';
import 'database/cart_relation_operations.dart';
import 'database/inventory_item_operations.dart';
import 'database/orders_operations.dart';
import 'database/product_item_operations.dart';
import 'model/Cart.dart';
import 'model/InventoryItem.dart';
import 'model/Orders.dart';
import 'model/ProductItem.dart';

class CartListScreenState extends StatefulWidget {
  const CartListScreenState({Key? key}) : super(key: key);

  @override
  _CartListScreenState createState() => _CartListScreenState();
}

class _CartListScreenState extends State<CartListScreenState> {
  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 1000), () {
      _getData();
    });
    super.initState();
  }
@override
  void setState(VoidCallback fn) {
  if(mounted) {
    super.setState(fn);
  }
  }
  @override
  Widget build(BuildContext context) {
    return MyWidgets.scaffold(

        bottomNavigationBar:items.isNotEmpty?
        _buildCartSummary():null,
        body: createListView(context));

   // MyWidgets.scaffold(body:createListView(context));
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final teDeliveryChargeController = TextEditingController();
  final teDiscountChargeController = TextEditingController();

  var items = <ProductItem>[];
  double? cartDeliveryAmount = 0.00;
  double? cartDiscountAmount = 0.00;
  var cartGrossTotal = 0.00;
  var cartNetTotal = 0.00;

  bool isLoading = true;
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  var _scrollController = ScrollController();
  var productItemOperations = ProductItemOperations();
  var cartOperations = CartOperations();
Project? project;
  ///Fetch data from database
  Future<List<ProductItem>> _getData() async {

   await MyPref.getProject().then((project) async=>

     project==null?{
       setState(() { isLoading=false;}),
       showCustomAlertDialog(context, 'Please create a project ')

     }:{
this.project=project,

    //await productItemOperations.productItemsInCart().then((value) {
    await productItemOperations.productItemsInCartWithCartRelation(project.id!).then((value) {
      items = value;

      setState(() {
        isLoading = false;
        if(items.isNotEmpty){
        cartDeliveryAmount= items.first.cartDeliverCharge==null?0.0:items.first.cartDeliverCharge;
        cartDiscountAmount= items.first.cartDiscount==null?0.0:items.first.cartDiscount;


        calculateTotal();
        }

      });
    })}
    );

    return items;
  }

  ///create List View with
  Widget createListView(BuildContext context) {
    if (items.isNotEmpty) {
     return FadeIn( child:Container(
        // padding: EdgeInsets.all(15),
          height: double.infinity,
          width: double.infinity,child:SafeArea(bottom: true,child: ListView.builder(
         physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
         key: _listKey,
         controller: _scrollController,
         shrinkWrap: true,
         itemCount: items.length,
         itemBuilder: (BuildContext context, int index) {

           return _buildListItem(context, items[index], index);
         }))));
    }
    else
      return isLoading
          ?  MyWidgets.buildCircularProgressIndicator()
          : Center(
              child: Text(
                'Your cart is empty.',
                style: TextStyle(
                  fontSize: 17.0,
                  color: Colors.grey,
                ),
              ),
            );
  }


  Widget _buildCartSummary() {
   return FadeInUp( child: Container(
     margin:EdgeInsets.only(top: 12) ,
padding: EdgeInsets.only(top: 4),
       decoration: BoxDecoration(
         color: MyColors.white_f8,
         borderRadius: BorderRadius.only(
           topLeft: Radius.circular(20),
           topRight: Radius.circular(20),

         ),boxShadow: [
         BoxShadow(
           color: MyColors.grey_9d_bg.withOpacity(0.3),
           spreadRadius: 4,
           blurRadius: 4,
           offset: Offset(0, 4),
         ),
       ],
       ),child:
   SingleChildScrollView(child: Column(mainAxisSize:MainAxisSize.min,crossAxisAlignment:CrossAxisAlignment.stretch,children: [
     Column(children:<Widget>[
     MyWidgets.shadowContainer(color: MyColors.white,marginL: 10,marginT: 15,marginB: 5,marginR: 10,
       cornerRadius: 10,
       spreadRadius: 2,
       blurRadius: 2,
       shadowColor: MyColors.grey_9d_bg,child: TextFormField(
           keyboardType: TextInputType.numberWithOptions(decimal: true),
           inputFormatters: [FilteringTextInputFormatter.allow(RegExp(r'^(\d+)?\.?\d{0,2}')),
           ],
           controller: teDeliveryChargeController,
                 obscureText: false,
                 decoration: InputDecoration(
                   contentPadding:
                   EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                   border: InputBorder.none,
                   hintText:  'Delivery Fee',
                 )),
           ),

     Row(mainAxisSize:MainAxisSize.max,crossAxisAlignment: CrossAxisAlignment.center,mainAxisAlignment: MainAxisAlignment.center,children: [
       Expanded(flex:10,child: MyWidgets.shadowContainer(color: MyColors.white,marginL: 10,marginT: 5,marginB: 5,marginR: 10,
         cornerRadius: 10,
         spreadRadius: 2,
         blurRadius: 2,
         shadowColor: MyColors.grey_9d_bg,child: TextFormField(
             keyboardType: TextInputType.numberWithOptions(decimal: true),
             inputFormatters: [FilteringTextInputFormatter.allow(RegExp(r'^(\d+)?\.?\d{0,2}')),
             ],
             controller: teDiscountChargeController,
             obscureText: false,
             decoration: InputDecoration(
               contentPadding:
               EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
               border: InputBorder.none,
               hintText:'Discount',
             )),
       )),
       //SizedBox(width: 10,),
       Expanded(flex:3,child:Material(
         elevation: 0,
         borderRadius: BorderRadius.circular(8.0),
         color: MyColors.white_chinese_e0_bg,
         child: MaterialButton(
           minWidth: 100,
           onPressed: () {
             if(_validate())
             updateDeliveryChargeOrDiscount();
           },
           child:Text(
             'Submit',
             style: MyStyles.customTextStyle(fontSize: 13,fontWeight: FontWeight.w500),
           ),
         ),
       ),),SizedBox(width: 10,),],),

    MyWidgets.shadowContainer(color: MyColors.white_chinese_e0_bg,
        cornerRadius: 10,
        spreadRadius: 0,
        blurRadius: 0,
        shadowColor: MyColors.white_f1_bg,
        marginB: 5,marginL: 10,marginR: 10,marginT: 5,paddingB: 5,paddingL: 10,paddingR: 10,paddingT: 5,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 5,
            ),
            Row(children: <Widget>[
              Text('Items Count:',
                  style: MyStyles.black_17_w500),
              Spacer(),
              Text('${items.length} Items',
              style: MyStyles.black_17_w500)
            ]),
            SizedBox(
              height: 8,
            ),
            Row(children: <Widget>[
              Text('Total Price:',
                  style: MyStyles.black_17_w500),
              Spacer(),

            MyText.priceCurrencyText(context,price: '$cartGrossTotal',fontSize: 17)
            ]),
            SizedBox(
              height: 8,
            ),
            Row(children: <Widget>[
              Text('Deliver Charge:',
                  style: MyStyles.black_17_w500),
              Spacer(),

              MyText.priceCurrencyText(context,price: '$cartDeliveryAmount',fontSize: 17)
            ]),
            SizedBox(
              height: 8,
            ),
            Row(children: <Widget>[
              Text('Discount:',
                  style: MyStyles.black_17_w500),
              Spacer(),

              MyText.priceCurrencyText(context,price: '$cartDiscountAmount',fontSize: 17)
            ]),
            SizedBox(
              height: 8,
            ),
            Row(children: <Widget>[
              Text('Total To Pay:',
                  style: MyStyles.black_17_w500),
              Spacer(),

              MyText.priceCurrencyText(context,price: '$cartNetTotal',fontSize: 17)
            ]),
            SizedBox(
              height: 5,
            ),
          ],
        ),

      )]),MyWidgets.shadowContainer(marginT:5,paddingL:60,paddingB:7,paddingT:7,paddingR:60,child:

     Material(

       elevation: 1.5,
       borderRadius: BorderRadius.circular(8.0),
       color: MyColors.white_chinese_e0_bg,
       child: MaterialButton(
         minWidth: 100,
         onPressed: () {
           validateStock();
         },
         child:Text(
           'Purchase Now',
           style: MyStyles.customTextStyle(fontSize: 13,fontWeight: FontWeight.w500),
         ),
       ),
     )
       ,)]))));
  }

  Widget _buildListItem(BuildContext context, ProductItem values, int index) {
    return MyWidgets.shadowContainer(height:150,marginL:10,marginR:10,marginB:5,marginT:5,spreadRadius: 2, cornerRadius:10,shadowColor:MyColors.white_f1_bg,
        child: Column(children: <Widget>[
          Expanded(flex:7,child: MyWidgets.shadowContainer(marginL:15,marginT:15,marginR:15,marginB:5,cornerRadius: 0,shadowColor:MyColors.white,spreadRadius: 0,blurRadius: 0,
              child: Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: <Widget>[
                    Container(

                        width: 66.0,
                        height: 66.0,
                        child: values.imageBase != null &&
                            values.imageBase.toString().isNotEmpty
                        //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                            ?ClipRRect(
                          borderRadius: BorderRadius.circular(35),
                          child: Image.memory(
                            base64Decode(values.imageBase.toString()),
                            fit: BoxFit.cover,
                            gaplessPlayback: true,
                          ),
                        )
                            :ClipRRect(
                          borderRadius: BorderRadius.circular(35),
                          child: Container(color: MyColors.grey_70,
                          ),)
                    ),

                    SizedBox(width: 15,),
                    Expanded(flex:1,child:
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        //Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0)),

                        Text(
                          values.name!,
                          style: MyStyles.customTextStyle(fontWeight: FontWeight.w500,fontSize: 18.0,
                            color: Colors.black,),
                          textAlign: TextAlign.left,
                          maxLines: 2,
                        ),
                        // Padding(padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),
                        Spacer(),

                        MyText.priceCurrencyText(context,price: 'Cost: ${double.parse((values.cost!).toString())}',fontSize: 15,fontWeight: FontWeight.normal),
                        Spacer(),
                        MyWidgets.shadowContainer(color: MyColors.white_f8,cornerRadius:10,shadowColor: MyColors.white,spreadRadius: 0,blurRadius: 0,
                            child: Row(mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,children: <Widget>[
                                  MyWidgets.shadowContainer(height:27,width:27,marginB:4,marginR:4,marginT:4,marginL:4,color: MyColors.white_chinese_e0_bg,cornerRadius:10,shadowColor:MyColors.white_f8,blurRadius:0,spreadRadius:0,child: GestureDetector(child: Icon(Icons.remove,color: MyColors.grey_70,size: 20,),
                                    onTap: () => setState(() {
                                      if (values.cartQuantity! > 1)
                                        values.cartQuantity =
                                            values.cartQuantity! - 1;
                                      //updateProductQuantity(values);
                                      updateCartRelationProductQuantity(values);
                                  }),))
                                  ,
                                  SizedBox(width:15,),
                                  Text('${values.cartQuantity}',
                                      style:
                                      MyStyles.grey_70__HR_16),
                                  SizedBox(width:15,),
                                  MyWidgets.shadowContainer(height:27,width:27,marginB:4,marginR:4,marginT:4,marginL:4,color: MyColors.white_chinese_e0_bg,cornerRadius:10,shadowColor:MyColors.white_f8,blurRadius:0,spreadRadius:0,child: GestureDetector(child: Icon(Icons.add,color: MyColors.grey_70,size: 20,),
                                    onTap: () => setState((){
                                      values.cartQuantity =
                                      values.cartQuantity! + 1;
                                      //updateProductQuantity(values);
                                      updateCartRelationProductQuantity(values);
                                      }),))

                                ])),
                        Spacer(),
                        //Padding(padding: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 10.0)),
                      ],
                    ),),GestureDetector(child: Icon(CupertinoIcons.delete_simple,color: MyColors.red_ff_bg,size: 20,),onTap:
                        ()async{
                      return  await showCustomCallbackAlertDialog(context:context,positiveText: 'Delete',msg: 'Are you sure you wish to delete all project related data?',positiveClick:() {
                        onDeleteFromCArtRelation(index);
                        Navigator.of(context).pop(true);

                      },negativeClick: (){
                        Navigator.of(context).pop(false);
                      });},)
                  ]) )),
          Expanded(flex:2,child: Stack(alignment: Alignment.center, children: <Widget>[
            Container(

              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10)),
                color: MyColors.white_chinese_e0_bg,
              ), //BoxDecoration
            ),
            Text(
              "Selling Price: ${double.parse((values.sellingPrice!).toStringAsFixed(2))} QAR",
              style: MyStyles.customTextStyle(fontSize: 14,fontWeight: FontWeight.w400),
            )
          ]))
        ],));
  }


  var cartRelationOperations = CartRelationOperations();
  var orderOperations = OrderOperations();

  /// Delete Click and delete item
  onDeleteFromCArtRelation(int index) async {
    var id = items[index].id;
    await cartRelationOperations
        .deleteCartRelationItemByProductItemId(id!)
        .then((value){
      setState(() { items.removeAt(index);});
      calculateTotal();
      /*items.removeAt(index);
      if(items.isEmpty){
        setState(() {

        });
      }*/
    });
  }



  updateCartRelationProductQuantity(ProductItem value)async{
    await cartRelationOperations
        .updateCartRelationItemQuantity(value)
        .then((value) => {
      setState(() {
        calculateTotal();
      })
      // calculateTotal();
      /*_getData()*/
    });
  }


  double? calculateTotal() {
    cartGrossTotal = 0.00;
    items.forEach((element) {
      cartGrossTotal = cartGrossTotal + (element.sellingPrice!) * element.cartQuantity!;
    });

    setState(()
    {
     var netTotal= ((cartGrossTotal+cartDeliveryAmount!)-cartDiscountAmount!)<=0?0.0:((cartGrossTotal+cartDeliveryAmount!)-cartDiscountAmount!);
      cartNetTotal = netTotal;
    });
    return double.tryParse(cartNetTotal.toStringAsFixed(2));
  }
///step by step on purchase insert into purchase, update stock and clear cart
  purchaseNowClicked() async
  {
    if (items.isNotEmpty)
      {
        /*for (var element in items)
        {
          var order = Orders();
          order.productItemId = element.id;
          order.orderDeliveryCharge=cartDeliveryAmount;
          order.orderDiscount=cartDiscountAmount;
          order.orderGrossTotal=cartGrossTotal;
          order.orderNetTotal=cartNetTotal;
          order.quantity = element.cartQuantity;
          await orderOperations.insertOrder(order).then((value) => null);
        }*/

var orderCreated=DateTime.now().millisecondsSinceEpoch;

        await updateInventoryStock().then((value) async {
          items.forEach((element) async{
            var order = Orders();

            order.productItemId = element.id;
            order.orderCreated=orderCreated;
            order.orderDeliveryCharge=cartDeliveryAmount;
            order.orderDiscount=cartDiscountAmount;
            order.orderGrossTotal=cartGrossTotal;
            order.orderNetTotal=cartNetTotal;
            order.quantity = element.cartQuantity;
            await orderOperations.insertOrder(order).then((value) => null).onError((error, stackTrace) {

            });
          });

          await cartRelationOperations.clearCartRelation(items.first.cartId).then((value) {
        _getData().then((value) {
        showCustomAlertDialog(context, 'Order successful');
        });
        });
        }).onError((error, stackTrace){});




      }
  }


  ///purchase with insert into orders, update stock and clear cart with transaction (rollback the state on exception)- any doubt refer above step by step [purchaseNowClicked]

  /*purchaseNowWithTransaction() async{
    if (items.isNotEmpty)
    {
      var orderList = <Orders>[];
      for (var element in items)
      {
        var order = Orders();
        order.productItemId = element.id;
        order.orderDeliveryCharge=cartDeliveryAmount;
        order.orderDiscount=cartDiscountAmount;
        order.orderGrossTotal=cartGrossTotal;
        order.orderNetTotal=cartNetTotal;
        order.quantity = element.cartQuantity;
       orderList.add(order);
      }
      if(orderList.isNotEmpty)
      await cartRelationOperations.purchaseNowWithTransaction(orderList,items).then((value) {print('onUpdateHistorySuccess ');_getData().then((value) {
        showAlertDialog(context, 'Order successful');
      });}).onError((error, stackTrace) {print('onUpdateHistoryError  ${error}  ${stackTrace}');});
    }

  }*/

  var inventoryItemOperations = InventoryItemOperations();
  //var inventoryItems = <InventoryItem>[];

  Future<String> checkInventoryStock() async {
    //inventoryItems.clear();
    var invNames = '';
   // inventoryItems.clear();
    if (items.isNotEmpty) {
      for (var item in items) {
        await inventoryItemOperations
            .inventoriesItemsByProductItem(item.id)
            .then((value) {
          if (value.isNotEmpty) {
            //inventoryItems.addAll(value);
            value.forEach((element) {
              if (((element.selectedQuantity!) * (item.cartQuantity!)) >
                  (element.stock!))
                //inventoryItems.add(element);
                invNames = invNames + ' ${item.name}: ${element.name!},';
            });
          }
        });
      }
    }
    //return inventoryItems;
    return invNames.trim();
  }


  Future<void> updateInventoryStock() async {

    if (items.isNotEmpty) {
      for (var item in items) {
        await inventoryItemOperations
            .inventoriesItemsByProductItem(item.id)
            .then((inventoryItems) {
          if (inventoryItems.isNotEmpty)
            inventoryItems.forEach((inventory)async {
              if (((inventory.selectedQuantity!) * (item.cartQuantity!)) <= (inventory.stock!)) {
                var inventoryItem = InventoryItem();
                inventoryItem.id = inventory.id;
                inventoryItem.stock = inventory.stock! - ((inventory.selectedQuantity!) * (item.cartQuantity!));
                await inventoryItemOperations
                    .updateInventoryItemStock(inventoryItem).then((value) => null).onError((error, stackTrace) {});

              }
          });

        }).onError((error, stackTrace){});
      }
    }

  }



  validateStock() {
    checkInventoryStock().then((value) => value.isEmpty
        ? {cartNetTotal<=0?context.showSnackBar('Please update the delivery charge or discount'):purchaseNowClicked()}
        : showCustomAlertDialog(context, '$value out of stock.'.trim()));
  }


  updateDeliveryChargeOrDiscount()async{
    var cart=Cart();
    cart.id=items.first.cartId;
    cart.cartDeliveryCharge=double.tryParse(this.teDeliveryChargeController.text.trim());
    cart.cartDiscount=double.tryParse(this.teDiscountChargeController.text.trim());
    await cartOperations.updateCartItemDeliverChargeOrDiscount(cart).then((value)async {
     // Navigator.of(context).pop();
      _getData();
    });
  }

  bool _validate()
  {
    if (this.teDeliveryChargeController.text.isEmpty||double.tryParse(this.teDeliveryChargeController.text.trim())!<0.0)
    {
      context.showSnackBar("Enter Valid Delivery Amount.");
      return false;
    }
    else if (this.teDiscountChargeController.text.isEmpty||double.tryParse(this.teDiscountChargeController.text.trim())!<0.0)
    {
      context.showSnackBar("Enter Valid Discount Amount.");
      return false;
    }

    return true;
  }


}
