import 'dart:convert';
import 'dart:io';
import 'package:ecommerce/database/project_operations.dart';
import 'package:ecommerce/model/Inventory.dart';
import 'package:ecommerce/upgrade.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Subscription.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'database/inventory_operations.dart';
import 'package:image_picker/image_picker.dart';

import 'database/user_operation.dart';
import 'model/Project.dart';


class AddProjectScreenState extends StatefulWidget {
  final Project? project;
   AddProjectScreenState({@required this.project,Key? key}) : super(key: key);

  @override
  _AddProjectScreenState createState() => _AddProjectScreenState();
}

class _AddProjectScreenState extends State<AddProjectScreenState> {

  File? _image;

  final picker = ImagePicker();
  final teProjectName_en_Controller = TextEditingController();
  final teProjectContactController = TextEditingController();
  final teProjectEmailController = TextEditingController();


  var projectOperations = ProjectOperations();
  var userOperations = UserOperations();
  var projectItemImageBase64;
   bool isIapPurchased = false;
  @override
  void setState(VoidCallback fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  @override
  void initState() {
    if (widget.project != null) {
      teProjectName_en_Controller.text='${widget.project?.projectName}';
      projectItemImageBase64 = widget.project?.imageBase;
      teProjectContactController.text=widget.project?.contactNo??'';
      teProjectEmailController.text=widget.project?.email??'';
    }
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final imageView =
    GestureDetector(
      onTap: () {
        _showPicker(context);

      },
      child: Container(
            decoration: BoxDecoration(
              // color: Colors.grey[200],
                borderRadius: BorderRadius.circular(75)
            ),
            height: 104,
            width: 104,
            child: projectItemImageBase64 != null?
            ClipRRect(
              borderRadius: BorderRadius.circular(75),
              child: Image.memory(
                base64Decode('${projectItemImageBase64}'),
                width: 100,
                height: 100,
                fit: BoxFit.cover,
                gaplessPlayback: true,
              ),
            ):_image != null
                ? ClipRRect(
              borderRadius: BorderRadius.circular(75),
              child: Image.file(
                _image!,
                width: 100,
                height: 100,
                fit: BoxFit.cover,
                gaplessPlayback: true,
              ),
            )
                : Container(
              decoration: BoxDecoration(
                // color: Colors.grey[200],
                  border: Border.all(color: Colors.black,width: 1),
                  borderRadius: BorderRadius.circular(75)
              ),
              width: 100,
              height: 100,
              child: Icon(
                Icons.image_rounded,
                color: Colors.grey[800],
              ),
            ),
          ));
    final createProjectTextView =  Align(alignment: Alignment.centerLeft,
      child: MyWidgets.textView(text: 'Create New project',
        style: MyStyles.customTextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 24,
        ),
      ),
    );
    final projectNameTextVw=
    Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10,),
        MyWidgets.textView(text: 'Project Name'),
        SizedBox(height: 10,),
        MyWidgets.textViewContainer(
          height: 45, cornerRadius: 8, color: MyColors.white,
          child:  MyWidgets.textFromField(
    contentPaddingL: 20.0,
    contentPaddingT: 15.0,
    contentPaddingR: 20.0,
    contentPaddingB: 15.0,
    cornerRadius: 8,keyboardType: TextInputType.name,
            controller: teProjectName_en_Controller,)
        ),
      ],
    );

    final contactNoTextVw=
    Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10,),
        MyWidgets.textView(text: 'Contact Number'),
        SizedBox(height: 10,),
        MyWidgets.textViewContainer(
          height: 45, cornerRadius: 8, color: MyColors.white,
          child:
    MyWidgets.textFromField(
    contentPaddingL: 20.0,
    contentPaddingT: 15.0,
    contentPaddingR: 20.0,
    contentPaddingB: 15.0,
    cornerRadius: 8,
      keyboardType: TextInputType.phone,
      inputFormatters: [FilteringTextInputFormatter.digitsOnly],controller: teProjectContactController)
        ),
      ],
    );
    final emailNoTextVw=

    Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10,),
        MyWidgets.textView(text: 'Email Address'),
        SizedBox(height: 10,),
        MyWidgets.textViewContainer(
          height: 45, cornerRadius: 8, color: MyColors.white,
          child:
          MyWidgets.textFromField(
            contentPaddingL: 20.0,
            contentPaddingT: 15.0,
            contentPaddingR: 20.0,
            contentPaddingB: 15.0,
            cornerRadius: 8,
              keyboardType: TextInputType.emailAddress,
              controller: teProjectEmailController,)
        ),
      ],
    );
    final addPrjButton = MyWidgets.materialButton(context,text:  widget.project==null?"Create Project":"Update Project",onPressed:()async { if(_validate()){

        widget.project==null? await getActiveSubscriptionAndAddProject() : await updateProject();
         }});
    return Scaffold(
      backgroundColor:  Colors.transparent,

        body:  SingleChildScrollView(
            padding: EdgeInsets.only(left: 30, right: 30, top: 25, bottom: 20),
            physics: BouncingScrollPhysics(),
              child: Column(
                children: <Widget>[
                  createProjectTextView,
                  SizedBox(
                    height: 30,
                  ),
                  imageView,

                  SizedBox(
                    height: 20,
                  ),
                  projectNameTextVw,
                  contactNoTextVw,
                  emailNoTextVw,
                  SizedBox(
                    height: 30,
                  ),
                  addPrjButton
                ],
              ))
    );
  }

  void _showPicker(context) {
    /*showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  ListTile(
                    leading: Icon(Icons.photo_camera),
                    title: Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });*/
    MyImagePickerBottomSheet.showPicker(context, fileCallback: (image) {
      projectItemImageBase64 = null;
      setState(() {
        _image = image!=null?File(image.path):null;

      });
      return null;
    });
  }








 /* _imgFromCamera() async {
    var image =
    await picker.pickImage(source: ImageSource.camera, imageQuality: 0,maxHeight:150,maxWidth: 150);
    projectItemImageBase64 = null;
    setState(() {
      _image = image!=null?File(image.path):null;
    });
  }

  _imgFromGallery() async {
    var image =
    await picker.pickImage(source: ImageSource.gallery, imageQuality: 0,maxHeight:150,maxWidth: 150);
    projectItemImageBase64 = null;
    setState(() {
      _image = image!=null?File(image.path):null;


    });
  }*/

  bool _validate()  {
    if (projectItemImageBase64 == null && _image == null) {
      context.showSnackBar("Select Project Image");
      return false;
    }
    else if (teProjectName_en_Controller.text.isEmpty) {
      context.showSnackBar("Enter Project Name");
      return false;
    }
    else if (teProjectName_en_Controller.text.startsWith(RegExp(r'^\d+\.?\d{0,2}'))) {
      context.showSnackBar("Can\'t Start Project Name with Number");
      return false;
    }
    else if(teProjectContactController.text.isEmpty){
      context.showSnackBar("Please enter contact number");
      return false;
    }else if (teProjectEmailController.text.isEmpty) {
      context.showSnackBar("Please enter email address");
      return false;
    } else if (!RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(teProjectEmailController.text.toString().trim())) {
      context.showSnackBar("Please check email address");
      return false;
    }
    return true;
  }

  /*String getImageBase64() {
    if (_image != null) {
      var bytes = _image?.readAsBytesSync();
      var base64Image = base64Encode(bytes!);
      return base64Image.toString();
    }
    return "";
  }*/
  Future getActiveSubscriptionAndAddProject()async{
  await Subscription.getActiveSubscription().then((value)async {
    print('ActiveSubscription $value');
    switch(value){
      case 0:
        /*showCustomCallbackAlertDialog(context: context, msg: 'No active subscription plan found, Please upgrade to pro to continue.',positiveText: 'Upgrade', positiveClick: ()async{}, negativeClick: (){
          Navigator.pop(context);
        });*/
      navigateToUpgrade();
        break;
      case 1:
        await getProjectCountAndAddProject(value);
        break;
      case 2:
        await getProjectCountAndAddProject(value);
        break;
      case 3:
        await getProjectCountAndAddProject(value);
        break;
    }
  });
}

Future getProjectCountAndAddProject(int activeSubscription)async{
  await userOperations.getLastLoggedInUser().then((user)async {

    await projectOperations.projectCount(user.first.id).then((value)async {
      print('projectCount  $value');
      if(activeSubscription==1&&value>=5||activeSubscription==2&&value>=5){
        showCustomAlertDialog(context,'You have reached the limit of Projects');
        return;
      }else if(activeSubscription==3&&value>=1){
        showCustomAlertDialog(context,'You have reached the limit of Projects');

        return;
      }
      else
        await addProject();


    });
  });
}
navigateToUpgrade(){
  Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) =>
              UpgradeScreenState())).then((isIapPurchased) =>this.isIapPurchased=isIapPurchased );
}

  Future addProject()async {
    await userOperations.getLastLoggedInUser().then((user)async {

if(user.isNotEmpty) {
  var project = Project();
  project.userId = user.first.id;
  project.projectName = teProjectName_en_Controller.text.trim().toString();
  project.imageBase = getImageBase64(_image);
  project.contactNo=teProjectContactController.text.trim().toString();
  project.email=teProjectEmailController.text.trim().toString();
  await projectOperations.insertProject(project).then((projectId) async {
    teProjectName_en_Controller.text = "";
    teProjectContactController.text = "";
    teProjectEmailController.text = "";
    showtoast("Successfully Added Data");
    await projectOperations.project(projectId).then((project) {
      if (project.isNotEmpty)
        MyPref.setProject(project.first);
    });
    Navigator.pop(context, true);
  });
}else
  showCustomAlertDialog(context, 'PLease login to add projects.');
    });
  }

  updateProject() async{
    if (widget.project != null) {
      var project = Project();
      project.id = widget.project?.id;
      project.userId=widget.project?.userId;
      project.projectName = teProjectName_en_Controller.text.trim().toString();
      project.imageBase=projectItemImageBase64 != null
          ? projectItemImageBase64
          : getImageBase64(_image);
      project.contactNo=teProjectContactController.text.trim().toString();
      project.email=teProjectEmailController.text.trim().toString();
      await projectOperations.updateProject(project).then((value) {
        teProjectName_en_Controller.text = "";
        teProjectContactController.text = "";
        teProjectEmailController.text = "";

        Navigator.of(context).pop(true);
        showtoast("Data Saved successfully");
      });
    }
  }


}
