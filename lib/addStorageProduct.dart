import 'dart:convert';
import 'dart:io';
import 'package:ecommerce/database/project_operations.dart';
import 'package:ecommerce/model/Inventory.dart';
import 'package:ecommerce/model/StorageExtra.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'database/inventory_operations.dart';
import 'package:image_picker/image_picker.dart';

import 'database/storage_operations.dart';
import 'database/user_operation.dart';
import 'model/Project.dart';


class AddStorageProductScreenState extends StatefulWidget {
  final Project? project;
   AddStorageProductScreenState({@required this.project,Key? key}) : super(key: key);

  @override
  _AddStorageProductScreenState createState() => _AddStorageProductScreenState();
}

class _AddStorageProductScreenState extends State<AddStorageProductScreenState> {

  File? _image;

  final picker = ImagePicker();
  final teProductNameController = TextEditingController();
  final teProductQntyController = TextEditingController();
  final teProductCostController = TextEditingController();
  final teProductPriceController = TextEditingController();


  var storageOperations = StorageOperations();
  var userOperations = UserOperations();
  var projectItemImageBase64;
  @override
  void setState(VoidCallback fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  @override
  void initState() {
    /*if (widget.project != null) {
      teProductNameController.text='${widget.project?.projectName}';
      projectItemImageBase64 = widget.project?.imageBase;
    }*/
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final imageView =
    GestureDetector(
      onTap: () {
        _showPicker(context);

      },
      child: Container(
            decoration: BoxDecoration(
              // color: Colors.grey[200],
                borderRadius: BorderRadius.circular(75)
            ),
            height: 104,
            width: 104,
            child: projectItemImageBase64 != null?
            ClipRRect(
              borderRadius: BorderRadius.circular(75),
              child: Image.memory(
                base64Decode('$projectItemImageBase64'),
                width: 100,
                height: 100,
                fit: BoxFit.cover,
                gaplessPlayback: true,
              ),
            ):_image != null
                ? ClipRRect(
              borderRadius: BorderRadius.circular(75),
              child: Image.file(
                _image!,
                width: 100,
                height: 100,
                fit: BoxFit.cover,
                gaplessPlayback: true,
              ),
            )
                : Container(
              decoration: BoxDecoration(
                // color: Colors.grey[200],
                  border: Border.all(color: Colors.black,width: 1),
                  borderRadius: BorderRadius.circular(75)
              ),
              width: 100,
              height: 100,
              child: Icon(
                Icons.image_rounded,
                color: Colors.grey[800],
              ),
            ),
          ));
    final createProjectTextView =  Align(alignment: Alignment.centerLeft,
      child: MyWidgets.textView(text: 'Add New Product',
        style: MyStyles.customTextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 24,
        ),
      ),
    );
    final nameTextVw=
    Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10,),
        MyWidgets.textView(text: 'Product Name'),
        SizedBox(height: 10,),
        MyWidgets.textViewContainer(
          height: 45, cornerRadius: 8, color: MyColors.white,
          child:  MyWidgets.textFromField(
    contentPaddingL: 20.0,
    contentPaddingT: 15.0,
    contentPaddingR: 20.0,
    contentPaddingB: 15.0,
    cornerRadius: 8,keyboardType: TextInputType.name,
            controller: teProductNameController,)
        ),
      ],
    );
    final costTextVw=
    Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10,),
        MyWidgets.textView(text: 'Product Cost'),
        SizedBox(height: 10,),
        MyWidgets.textViewContainer(
            height: 45, cornerRadius: 8, color: MyColors.white,
            child:  MyWidgets.textFromField(
              contentPaddingL: 20.0,
              contentPaddingT: 15.0,
              contentPaddingR: 20.0,
              contentPaddingB: 15.0,
              cornerRadius: 8,keyboardType:
    TextInputType.numberWithOptions(decimal: true),
    inputFormatters: [
    FilteringTextInputFormatter.allow(
    RegExp(r'^\d+\.?\d{0,2}')),
    ],
              controller: teProductCostController,)
        ),
      ],
    );

    final priceTextVw=
    Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10,),
        MyWidgets.textView(text: 'Product Price'),
        SizedBox(height: 10,),
        MyWidgets.textViewContainer(
            height: 45, cornerRadius: 8, color: MyColors.white,
            child:  MyWidgets.textFromField(
              contentPaddingL: 20.0,
              contentPaddingT: 15.0,
              contentPaddingR: 20.0,
              contentPaddingB: 15.0,
              cornerRadius: 8,keyboardType:
            TextInputType.numberWithOptions(decimal: true),
              inputFormatters: [
                FilteringTextInputFormatter.allow(
                    RegExp(r'^\d+\.?\d{0,2}')),
              ],
              controller: teProductPriceController,)
        ),
      ],
    );
    final quantityTextVw=
    Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10,),
        MyWidgets.textView(text: 'Quantity'),
        SizedBox(height: 10,),
        MyWidgets.textViewContainer(
          height: 45, cornerRadius: 8, color: MyColors.white,
          child:
    MyWidgets.textFromField(
    contentPaddingL: 20.0,
    contentPaddingT: 15.0,
    contentPaddingR: 20.0,
    contentPaddingB: 15.0,
    cornerRadius: 8,
      keyboardType: TextInputType.phone,controller: teProductQntyController)
        ),
      ],
    );

    final addPrjButton = MyWidgets.materialButton(context,text:  widget.project==null?"Add Product":"Update Product",onPressed:() {

      if(_validate()){
        addStorageExtra();
         }});
    return Scaffold(
      backgroundColor:  Colors.transparent,

        body:  SingleChildScrollView(
            padding: EdgeInsets.only(left: 30, right: 30, top: 25, bottom: 20),
            physics: BouncingScrollPhysics(),
              child: Column(
                children: <Widget>[
                  createProjectTextView,
                  SizedBox(
                    height: 30,
                  ),
                  imageView,

                  SizedBox(
                    height: 20,
                  ),
                  nameTextVw,
                  quantityTextVw,
                  costTextVw,
                  priceTextVw,
                  SizedBox(
                    height: 30,
                  ),
                  addPrjButton
                ],
              ))
    );
  }

  void _showPicker(context) {

    MyImagePickerBottomSheet.showPicker(context, fileCallback: (image) {
      projectItemImageBase64 = null;
      setState(() {
        _image = image!=null?File(image.path):null;

      });
      return null;
    });
  }



  bool _validate()  {
    if (projectItemImageBase64 == null && _image == null) {
      context.showSnackBar("Select Product Image");
      return false;
    }
    else if (teProductNameController.text.isEmpty) {
      context.showSnackBar("Enter Product Name");
      return false;
    }
    // if (teProductNameController.text.startsWith(RegExp(r'^\d+\.?\d{0,2}'))) {
    //   context.showSnackBar("Can\'t Start Product Name with Number");
    //   return false;
    // }
    else if (teProductQntyController.text.isEmpty) {
      context.showSnackBar("Enter Product Quantity");
      return false;
    }else if (teProductCostController.text.isEmpty) {
      context.showSnackBar("Enter Product Cost");
      return false;
    }else if (teProductPriceController.text.isEmpty) {
      context.showSnackBar("Enter Product Price");
      return false;
    }else if (double.parse(teProductPriceController.text)<=double.parse(teProductCostController.text)) {
      context.showSnackBar("Product price must be greater than the cost");
      return false;
    }
    return true;
  }
  addStorageExtra()async {

      if(widget.project!=null) {
        await showCustomCallbackAlertDialog(context: context, msg: 'The profit from the extra product added out of the business will added with the business profit.',positiveText: 'Continue', negativeText: 'Cancel', positiveClick: ()async{
          Navigator.pop(context, true);
          var storageExtra = StorageExtra();
          storageExtra.projectId= widget.project?.id;
          storageExtra.name= teProductNameController.text.toString();
          storageExtra.imageBase = getImageBase64(_image);
          storageExtra.storageQuantity= int.tryParse( teProductQntyController.text);
          storageExtra.cost= double.tryParse( teProductCostController.text);
          storageExtra.sellingPrice= double.tryParse( teProductPriceController.text);

          await storageOperations.insertStorageExtra(storageExtra).then((value) async {
            teProductNameController.text = "";
            teProductQntyController.text = "";
            teProductCostController.text = "";
            teProductPriceController.text = "";
            showtoast("Successfully Added Data");


            Navigator.pop(context, true);
          });
        }, negativeClick: (){
          Navigator.pop(context);
        });

      }else
        showCustomAlertDialog(context, 'PLease login to add projects.');

  }




}
