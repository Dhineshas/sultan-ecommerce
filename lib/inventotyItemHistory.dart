import 'dart:convert';
import 'dart:io';
import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/addInventoryCategory.dart';
import 'package:ecommerce/inventotyItemList.dart';
import 'package:ecommerce/model/Inventory.dart';
import 'package:ecommerce/inventoryReportPdfView.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import 'inventoryUnitMeasures.dart';
import 'database/DatabaseHelper.dart';
import 'database/history_operations.dart';
import 'database/inventory_item_operations.dart';
import 'database/inventory_operations.dart';
import 'database/product_item_operations.dart';
import 'database/relation_operations.dart';
import 'model/History.dart';
import 'model/InventoryItem.dart';

class InventoryItemHistoryScreenState extends StatefulWidget {
  final InventoryItem? inventoryItem;
  const InventoryItemHistoryScreenState({@required this.inventoryItem,Key? key}) : super(key: key);

  @override
  _InventoryItemHistoryScreenState createState() => _InventoryItemHistoryScreenState();
}

class _InventoryItemHistoryScreenState extends State<InventoryItemHistoryScreenState> {

  @override
  void initState() {
    Future.delayed( Duration(milliseconds: 1000), () {
      _getData();

    });

    super.initState();
    print("INIT");
    // _scrollController = new ScrollController();
    //_scrollController.addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Inventory History',
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context,true),
          ),
        ),
        body:
            /*ElevatedButton(
          onPressed: () {
            // Navigate back to first route when tapped.
            Navigator.pop(context);
          },
          child: Text('Go back!'),

        )*/
            //getAllInventory());
    createListView(context));

  }

  var items = <InventoryItem>[];

  final _listKey = GlobalKey<AnimatedListState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  var _scrollController = ScrollController();

  var inventoryOperations = InventoryOperations();
  var inventoryItemOperations = InventoryItemOperations();
  var historyItemOperations = HistoryOperations();
  var relationOperations = RelationOperations();
  var inventoryRelationOperations = RelationOperations();
  var productItemOperations = ProductItemOperations();

  final purchaseDateFormat = DateFormat('dd-MMM-yyyy');
  /// Get all users data
 /* getAllInventory() {
    return FutureBuilder(
        future: _getData(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return createListView(context, snapshot);
        });
  }*/

  ///Fetch data from database
  Future<List<InventoryItem>> _getData() async {
    await inventoryItemOperations.inventoriesItemsByHistory(widget.inventoryItem?.id).then((value) {
      items = value;

    });
setState(() {
  isLoading = false;
});

/*await historyItemOperations.history().then((value) =>
value.forEach((element) {print('adddedHistory  ${element.id}  ${element.inventoryItemId}  ${element.updatedQuantity}');})
);*/
    return items;
  }
  bool isLoading = true;
  ///create List View with
  Widget createListView(BuildContext context) {
    if (items.isNotEmpty) {
    return FadeInUp(delay: Duration(milliseconds: 200),child: Container(
            height: double.infinity,
            width: double.infinity,child:
                ListView.builder(
                    physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
                    key: _listKey,
                    controller: _scrollController,
                    shrinkWrap: true,
                    itemCount: items.length,
                    itemBuilder: (BuildContext context, int index) {

                      //return _buildItem(context, items[index],index);
                      return Dismissible( background: Container(
                        color: (Colors.teal[900])!,
                        child: Padding(
                          padding: const EdgeInsets.all(15),
                          child: Row(
                            children: [
                              Icon(Icons.edit_outlined, color: Colors.white),
                              Text('Edit', style: TextStyle(color: Colors.white)),
                            ],
                          ),
                        ),
                      ),secondaryBackground: Container(
                        color: Colors.red,
                        child: Padding(
                          padding: const EdgeInsets.all(15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Icon(Icons.delete_forever_outlined, color: Colors.white),
                              Text('Delete', style: TextStyle(color: Colors.white)),
                            ],
                          ),
                        ),
                      ),key: Key(items[index].name!),confirmDismiss: (DismissDirection direction) async {
                        if(direction == DismissDirection.endToStart){
                          /*return await showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            if(Platform.isIOS){
                              return  CupertinoAlertDialog(
                                //title: Text(msg),
                                title: const Text("Confirm"),
                                content: const Text("Are you sure you wish to delete this item?"),
                                actions: <Widget>[
                                  CupertinoDialogAction(
                                    child: Text('DELETE'),
                                    onPressed: () => {onDelete(index),
                                      Navigator.of(context).pop(true)
                                    },
                                  ),
                                  CupertinoDialogAction(
                                    child: Text('CANCEL'),
                                    onPressed: () => Navigator.of(context).pop(false),
                                  ),
                                ],
                              );}else return AlertDialog(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              title: const Text("Confirm"),
                              content: const Text("Are you sure you wish to delete this item?"),
                              actions: <Widget>[
                                TextButton(
                                    onPressed: () => {onDelete(index),
                                      Navigator.of(context).pop(true)
                                    },
                                    child: const Text("DELETE")
                                ),
                                TextButton(
                                  onPressed: () => Navigator.of(context).pop(false),
                                  child: const Text("CANCEL"),
                                ),
                              ],
                            );
                          },
                        );*/

                          return await showCustomCallbackAlertDialog(context:context,positiveText: 'Delete',msg: 'Are you sure you wish to delete all project related data?',positiveClick:() {
                            onDelete(index);
                            Navigator.of(context).pop(true);

                          },negativeClick: (){
                            Navigator.of(context).pop(false);
                          });
                        }else{
                          onEditInventory(items[index]);
                        }},child:  _buildItem(context, items[index], index),);

                    }),

          ));
       }
    else
        return isLoading?LinearProgressIndicator(): Center(child: Text(
      'Inventory History is empty.',
      style: TextStyle(
        fontSize: 17.0,
        color: Colors.grey,
      ),
    ),);
  }

  ///Construct cell for List View
  Widget _buildItem(BuildContext context, InventoryItem values, int index) {
    return
      Container(
        child: ListTile(
            onTap: () {},
            title:
            Column( children: <Widget>[
              Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        child: values.imageBase != null &&
                            values.imageBase.toString().isNotEmpty
                            ? ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image.memory(
                            base64Decode(
                                values.imageBase.toString()),
                            width: 80,
                            height: 80,
                            fit: BoxFit.fitHeight,
                            gaplessPlayback: true,
                          ),
                        )
                            : Container(
                          decoration: BoxDecoration(
                              color: Colors.grey[200],
                              borderRadius: BorderRadius.circular(10)),
                          width: 80,
                          height: 80,
                        ),
                      )
                    ],
                  ),
                  Padding(padding: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0)),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 0.0, 5.0)),

                      Container(
                        width: 150,
                        child: RichText(
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          strutStyle: StrutStyle(fontSize: 12.0),
                          text: TextSpan(
                            style: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 15, color: Colors.black),
                            text: values.name!,
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),
                      Text(
                        '${values.historyInventoryItemTotalType} ${InventoryUnitMeasure.inventoryUnitMeasureArray[values.itemType!]}',
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 15.0,
                          color: Colors.green,
                        ),
                        textAlign: TextAlign.left,
                        maxLines: 2,
                      ),
                      Padding(padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),
                      Text(
                        'Purchased: ${purchaseDateFormat.format(DateTime.fromMillisecondsSinceEpoch(values.historyAddedDate!))}',
                        style: TextStyle(
                          fontWeight: FontWeight.w300,
                          fontSize: 15.0,
                          color: Colors.black,
                        ),
                        textAlign: TextAlign.left,
                        maxLines: 2,
                      ),
                      Padding(padding: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 10.0)),
                    ],
                  ),Spacer(),/*Column(
                    children: <Widget>[
                      IconButton(
                          color: Colors.black,
                          icon: new Icon(Icons.edit),
                          onPressed: () => onEdit(values, index, context)),
                      IconButton(
                          color: Colors.black,
                          icon: new Icon(Icons.delete),
                          onPressed: () => onDelete(index)),
                    ],
                  )*/
                ],
              ),Divider(height: 1,)
            ])
        ),
      );
  }

  ///On Item Click


  /// Delete Click and delete item
  onDelete(int index)async {
    var id = items[index].historyId;
    await historyItemOperations.deleteHistory(id!).then((productCount) async{

    });

  }

  ///edit User
  editUser(int id, BuildContext context)async {
    if (teInvOfTypeController.text.isNotEmpty) {
      var inventory = Inventory();
      inventory.id = id;
      inventory.name = teInvOfTypeController.text;

      await inventoryOperations.updateRawInventory(inventory).then((value) {
        teInvOfTypeController.text = "";

        Navigator.of(context).pop();
        showtoast("Data Saved successfully");

        _getData();
      });
    } else {
      showtoast("Please fill all the fields");
    }
  }

  /// Edit Click
  onSelectItem(Inventory inventory, bool checked, BuildContext context) {
    print("checkboxstate $checked");
    var invent = Inventory();
    invent.id = inventory.id;
    invent.name = inventory.name;

    inventoryOperations.updateInventory(invent).then((value) {
      showtoast("Data Saved successfully");

      setState(() {});
    });
  }

  /// Edit Click
  onEditInventory(InventoryItem inventoryItem){
    openAlertBox(inventoryItem);
  }
  final teInvOfTypeController = TextEditingController();
  final teInvPiecesPerTypeController = TextEditingController();
  final teInvTotalCostPerTypeController = TextEditingController();
  /// openAlertBox to add/edit user
  openAlertBox(InventoryItem inventoryItem) {
    teInvOfTypeController.text='${inventoryItem.historyInventoryItemTotalType}';
    teInvPiecesPerTypeController.text='${inventoryItem.historyInventoryItemPiecesInType}';
    teInvTotalCostPerTypeController.text='${inventoryItem.historyInventoryItemCostPerType}';

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25.0))),
            contentPadding: EdgeInsets.only(top: 10.0),
            content: Container(
              width: 300.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        /*flag ?*/
                        "Last updated details" /*: "Add User"*/,
                        style: TextStyle(fontSize: 22.0),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Divider(
                    color: Colors.grey,
                    height: 4.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 15.0, right: 15.0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[

                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child:Text(inventoryItem.itemType==0?'No. of ${InventoryUnitMeasure.inventoryUnitMeasureArray[inventoryItem.itemType!]} added':'${InventoryUnitMeasure.inventoryUnitMeasureArray[inventoryItem.itemType!]} added',style: TextStyle(fontWeight: FontWeight.w400,)) //Container
                              ),SizedBox(width: 10,),
                              Flexible(
                                  flex: 2,
                                  fit: FlexFit.tight,
                                  child:TextFormField(
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                                      controller: teInvOfTypeController,
                                      obscureText: false,
                                      decoration: InputDecoration(


                                      ))//Container
                              )
                            ],
                          ),

                          inventoryItem.itemType==0? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child:Text('No. of items per ${InventoryUnitMeasure.inventoryUnitMeasureArray[inventoryItem.itemType!]}',style: TextStyle(fontWeight: FontWeight.w400,)) //Container
                              ),SizedBox(width: 10,),Flexible(
                                  flex: 2,
                                  fit: FlexFit.tight,
                                  child:TextFormField(
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                                      controller: teInvPiecesPerTypeController,
                                      obscureText: false,
                                      decoration: InputDecoration(


                                      ))//Container
                              )
                            ],
                          ):Container(),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child:Text(inventoryItem.itemType==0?'Cost per ${InventoryUnitMeasure.inventoryUnitMeasureArray[inventoryItem.itemType!]}':'Total ${InventoryUnitMeasure.inventoryUnitMeasureArray[inventoryItem.itemType!]} Cost',style: TextStyle(fontWeight: FontWeight.w400,)) //Container
                              ),SizedBox(width: 10,),Flexible(
                                  flex: 2,
                                  fit: FlexFit.tight,
                                  child:TextFormField(
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                      FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}')),
                            ],
                                      controller: teInvTotalCostPerTypeController,
                                      obscureText: false,
                                      decoration: InputDecoration(


                                      ))//Container
                              )
                            ],
                          )
                          ,
                        ],
                      ),
                    ),
                  ),SizedBox(height: 10,),
                  InkWell(
                    onTap: () =>{if(_validate())/*updateHistory(inventoryItem)*/updateHistoryWithTransaction(inventoryItem)},
                    child: Container(
                      padding: EdgeInsets.only(top: 12.0, bottom: 12.0),
                      decoration: BoxDecoration(
                        color: Color(0xff01A0C7),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20.0),
                            bottomRight: Radius.circular(20.0)),
                      ),
                      child: Text(
                        "Save Changes",
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  ///update history and select and update inventory item and product item step by step
updateHistory(InventoryItem inventoryItem)async{
  var costPerTypeControllerText=double.tryParse(teInvTotalCostPerTypeController.text.trim());
  double? currentCostPerItemInType=0.00;
  currentCostPerItemInType=double.tryParse((double.tryParse(teInvTotalCostPerTypeController.text.trim())!/int.parse(teInvPiecesPerTypeController.text.trim())).toStringAsFixed(2));
  var history = History();
  history.id=inventoryItem.historyId;
  history.historyTotalType=int.tryParse(teInvOfTypeController.text.trim());
  history.historyPiecesInType=int.tryParse(teInvPiecesPerTypeController.text.trim());
  history.historyCostPerType=costPerTypeControllerText;
  history.historyCostPerItemInType=currentCostPerItemInType;




  await historyItemOperations.updateHistory(history).then((value) async{


    //check currently entered cost per item in box > inventory cost per item in box
    /* if(currentCostPerItemInType!>(widget.inventoryItem?.costPerItemInTypeNew)!){
      //update inventory with the currently entered cost per box and cost per item in box
      inventoryItem.costPerTypeOld=widget.inventoryItem?.costPerTypeNew;
      inventoryItem.costPerTypeNew=costPerTypeControllerText;


      inventoryItem.costPerItemInTypeOld=widget.inventoryItem?.costPerItemInTypeNew;
      inventoryItem.costPerItemInTypeNew=currentCostPerItemInType;
      history.historyCostPerItemInType=currentCostPerItemInType;
    }//check currently entered cost per item in box < inventory cost per item in box
    else if(currentCostPerItemInType<(widget.inventoryItem?.costPerItemInTypeNew)!){
      // 1st remove the current inventory history from array
      // 2nd update inventory with max of cost per box and cost per item in box from array
      inventoryItem.costPerTypeOld=widget.inventoryItem?.costPerTypeOld;


      inventoryItem.costPerItemInTypeOld=  widget.inventoryItem?.costPerItemInTypeOld;

      var items2 = <InventoryItem>[];
      items2.clear();
      items2.addAll(items);
      // items2.forEach((element) {print('hgftrdtrdrd   ${element.historyInventoryItemCostPerItemInType}');});

      items2.remove(inventoryItem);
      items2.sort((a, b) => a.costPerItemInTypeNew!.compareTo(b.costPerItemInTypeNew!));
      inventoryItem.costPerItemInTypeNew=items2.last.historyInventoryItemCostPerItemInType;
      history.historyCostPerItemInType=items2.last.historyInventoryItemCostPerItemInType;
      inventoryItem.costPerTypeNew=items2.last.historyInventoryItemCostPerType;
      // print('gfcgfcgfc  ${inventoryItem.costPerItemInTypeNew}');
      //items2.forEach((element) {print('hgftrdtrdrd   ${element.historyInventoryItemCostPerItemInType}');});

    }//check currently entered cost per item in box == inventory cost per item in box
    else  if(currentCostPerItemInType==(widget.inventoryItem?.costPerItemInTypeNew)!){
      //update the inventory cost per box and cost per item in box with same values

      inventoryItem.costPerTypeOld=widget.inventoryItem?.costPerTypeOld;
      inventoryItem.costPerTypeNew=widget.inventoryItem?.costPerTypeNew;

      inventoryItem.costPerItemInTypeOld= widget.inventoryItem?.costPerItemInTypeOld;
      inventoryItem.costPerItemInTypeNew=widget.inventoryItem?.costPerItemInTypeNew;
      history.historyCostPerItemInType=widget.inventoryItem?.costPerItemInTypeNew;
    }*/


    /*if(costPerTypeControllerText!>(widget.inventoryItem?.costPerTypeNew)!){
      inventoryItem.costPerTypeOld=widget.inventoryItem?.costPerTypeNew;
      inventoryItem.costPerTypeNew=costPerTypeControllerText;


    }else if(costPerTypeControllerText<(widget.inventoryItem?.costPerTypeNew)!){
      inventoryItem.costPerTypeOld=costPerTypeControllerText;
      inventoryItem.costPerTypeNew=widget.inventoryItem?.costPerTypeNew;


    }else if(costPerTypeControllerText==(widget.inventoryItem?.costPerTypeNew)!){
      inventoryItem.costPerTypeOld=widget.inventoryItem?.costPerTypeOld;
      inventoryItem.costPerTypeNew=widget.inventoryItem?.costPerTypeNew;

    }*/






    /*inventoryItem.isCostUpdated=costPerTypeControllerText!=(widget.inventoryItem?.costPerTypeNew)! ?1:0;
    inventoryItem.isPiecesInTypeUpdated=int.tryParse(teInvNoPiecesPerTypeController.text.trim())!=(widget.inventoryItem?.piecesInType)! ?1:0;

    var historyTotalItems=(inventoryItem.historyInventoryItemTotalType!*inventoryItem.historyInventoryItemPiecesInType!);
    var currentTotalItems=(int.tryParse(teInvNoOfTypeController.text.trim())!*int.tryParse(teInvNoPiecesPerTypeController.text.trim())!);
    if(historyTotalItems>currentTotalItems){
      if((widget.inventoryItem?.stock)!>=(historyTotalItems-currentTotalItems))
      inventoryItem.stock=(widget.inventoryItem?.stock)!-(historyTotalItems-currentTotalItems);
      else
        inventoryItem.stock=0;
    }else if(historyTotalItems<currentTotalItems){
      inventoryItem.stock=(widget.inventoryItem?.stock)!+(currentTotalItems-historyTotalItems);
    }else {
      inventoryItem.stock=(widget.inventoryItem?.stock);
    }

    inventoryItem.inventoryId = inventoryItem.inventoryId;*/

_getData().then((value)async {

   // await inventoryItemOperations.inventoriesItemsByHistory(widget.inventoryItem?.id).then((value)async {

if(items.isNotEmpty) {
  items.sort((a, b) => a.historyInventoryItemCostPerItemInType!.compareTo(b.historyInventoryItemCostPerItemInType!));
  InventoryItem maxCostInventoryItem = items.last;
  items.forEach((element) {print('vfbevefhbv  ${element.historyInventoryItemCostPerItemInType}   ${maxCostInventoryItem.historyInventoryItemCostPerItemInType}'); });

  inventoryItem.costPerTypeOld = widget.inventoryItem?.costPerTypeNew;
  inventoryItem.costPerTypeNew =
      maxCostInventoryItem.historyInventoryItemCostPerType;
  inventoryItem.costPerItemInTypeOld =
      widget.inventoryItem?.costPerItemInTypeNew;
  inventoryItem.costPerItemInTypeNew =
      maxCostInventoryItem.historyInventoryItemCostPerItemInType;
 // inventoryItem.piecesInType= maxCostInventoryItem.historyInventoryItemPiecesInType;
  inventoryItem.isCostUpdated =
  costPerTypeControllerText != (widget.inventoryItem?.costPerTypeNew)! ? 1 : 0;
  inventoryItem.isPiecesInTypeUpdated =
  int.tryParse(teInvPiecesPerTypeController.text.trim()) !=
      (widget.inventoryItem?.piecesInType)! ? 1 : 0;

  var historyTotalItems = (inventoryItem.historyInventoryItemTotalType! *
      inventoryItem.historyInventoryItemPiecesInType!);
  var currentTotalItems = (int.tryParse(teInvOfTypeController.text.trim())! *
      int.tryParse(teInvPiecesPerTypeController.text.trim())!);
  if (historyTotalItems > currentTotalItems) {
    if ((widget.inventoryItem?.stock)! >=
        (historyTotalItems - currentTotalItems))
      inventoryItem.stock = (widget.inventoryItem?.stock)! -
          (historyTotalItems - currentTotalItems);
    else
      inventoryItem.stock = 0;
  } else if (historyTotalItems < currentTotalItems) {
    inventoryItem.stock = (widget.inventoryItem?.stock)! +
        (currentTotalItems - historyTotalItems);
  } else {
    inventoryItem.stock = (widget.inventoryItem?.stock);
  }
  inventoryItem.inventoryId = inventoryItem.inventoryId;


  //update inventory item with the updated inventory
  await inventoryItemOperations.updateInventoryItem(inventoryItem).then((
      value) async {
    inventoryItem.isCostUpdated == 1 ||
        (currentCostPerItemInType != widget.inventoryItem?.costPerItemInTypeNew)
        ?
    //get all product id with inventory id used
    await inventoryRelationOperations
        .relationItemFromProductRelationByInventoryItemId(inventoryItem.id)
        .then((productIds) async {
      //recalculate and update all product item cost with updated inventory cost(above)
      if (productIds.isNotEmpty)
        await productItemOperations
            .getProductItemIdAndRecalculateCostByProductItemId(productIds).then((
            value) async {
          clearControllerAndPop();
        });
      else
        clearControllerAndPop();
    }) : clearControllerAndPop();
  });
}});
  });
}

///update history and select and update inventory item and product item with transaction(rollback the state on exception)- any doubt refer above step by step [updateHistory]
  updateHistoryWithTransaction(InventoryItem inventoryItem)async{
    var costPerTypeControllerText=double.tryParse(teInvTotalCostPerTypeController.text.trim());
    double? currentCostPerItemInType=0.00;
    //currentCostPerItemInType=double.tryParse((double.tryParse(teInvCostPerTypeController.text.trim())!/int.parse(teInvNoPiecesPerTypeController.text.trim())).toStringAsFixed(2));


//calculate cost per item on type box and others in separate
    if(inventoryItem.itemType==0){
      currentCostPerItemInType=double.tryParse((double.tryParse(teInvTotalCostPerTypeController.text.trim())!/int.parse(teInvPiecesPerTypeController.text.trim())).toStringAsFixed(2));
      inventoryItem.isPiecesInTypeUpdated =  int.tryParse(teInvPiecesPerTypeController.text.trim()) != (widget.inventoryItem?.piecesInType)! ? 1 : 0;
    }else{
      currentCostPerItemInType=double.tryParse((double.tryParse(teInvTotalCostPerTypeController.text.trim())!/int.parse(teInvOfTypeController.text.trim())).toStringAsFixed(2));
      inventoryItem.isPiecesInTypeUpdated=0;

    }

    var history = History();
    history.id=inventoryItem.historyId;
    history.historyTotalType=int.tryParse(teInvOfTypeController.text.trim());
    history.historyPiecesInType=int.tryParse(teInvPiecesPerTypeController.text.trim());
    history.historyCostPerType=costPerTypeControllerText;
    history.historyCostPerItemInType=currentCostPerItemInType;





    inventoryItem.costPerTypeOld = widget.inventoryItem?.costPerTypeNew;

    inventoryItem.costPerItemInTypeOld =
        widget.inventoryItem?.costPerItemInTypeNew;

    inventoryItem.isCostUpdated = costPerTypeControllerText != (widget.inventoryItem?.costPerTypeNew)! ? 1 : 0;
    //inventoryItem.isPiecesInTypeUpdated = int.tryParse(teInvNoPiecesPerTypeController.text.trim()) != (widget.inventoryItem?.piecesInType)! ? 1 : 0;

    var historyTotalItems = (inventoryItem.historyInventoryItemTotalType! * inventoryItem.historyInventoryItemPiecesInType!);
    var currentTotalItems = (int.tryParse(teInvOfTypeController.text.trim())! * int.tryParse(teInvPiecesPerTypeController.text.trim())!);
    if (historyTotalItems > currentTotalItems) {
      if ((widget.inventoryItem?.stock)! >=
          (historyTotalItems - currentTotalItems))
        inventoryItem.stock = (widget.inventoryItem?.stock)! -
            (historyTotalItems - currentTotalItems);
      else
        inventoryItem.stock = 0;
    } else if (historyTotalItems < currentTotalItems) {
      inventoryItem.stock = (widget.inventoryItem?.stock)! +
          (currentTotalItems - historyTotalItems);
    } else {
      inventoryItem.stock = (widget.inventoryItem?.stock);
    }


    inventoryItem.inventoryId = inventoryItem.inventoryId;

    inventoryItem.isCostUpdated == 1 || (currentCostPerItemInType != widget.inventoryItem?.costPerItemInTypeNew)
        ? await historyItemOperations.updateHistoryWithTransactionAndAddHistory(inventoryItem, history).then((value) {print('onUpdateHistorySuccess ');clearControllerAndPop();}).onError((error, stackTrace) {print('onUpdateHistoryError  ${error}  ${stackTrace}');clearControllerAndPop(isSuccess: false);context.showSnackBar('$error');})
        : await historyItemOperations.updateHistory(history).then((value) {clearControllerAndPop();}).onError((error, stackTrace) {clearControllerAndPop(isSuccess: false);context.showSnackBar('$error');});

  }



  clearControllerAndPop({bool isSuccess = true})async{
    teInvOfTypeController.text='';
    teInvPiecesPerTypeController.text='';
    teInvTotalCostPerTypeController.text='';
    Navigator.pop(context);
    if(isSuccess)
    _getData();
  }

  bool _validate() {
    if (teInvOfTypeController.text.isEmpty||int.tryParse(teInvOfTypeController.text.trim())==0) {
      context.showSnackBar("Enter No. of ${InventoryUnitMeasure.inventoryUnitMeasureArray[(widget.inventoryItem?.itemType)!]}");
      return false;
    }else   if (teInvPiecesPerTypeController.text.isEmpty||int.tryParse(teInvPiecesPerTypeController.text.trim())==0) {
      context.showSnackBar("Enter No. of Items in ${InventoryUnitMeasure.inventoryUnitMeasureArray[(widget.inventoryItem?.itemType)!]}");
      return false;
    }else   if (teInvTotalCostPerTypeController.text.isEmpty||double.tryParse(teInvTotalCostPerTypeController.text.trim())==0) {
      context.showSnackBar("Enter Cost per ${InventoryUnitMeasure.inventoryUnitMeasureArray[(widget.inventoryItem?.itemType)!]}");
      return false;
    }
    return true;
  }
}
