import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:ecommerce/databaseSettings.dart';
import 'package:ecommerce/productReportLineChart.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/FirebaseUtils.dart';
import 'package:ecommerce/utils/InAppPurchaseHandler.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Subscription.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ReportScreenState extends StatefulWidget {
  const ReportScreenState({Key? key}) : super(key: key);

  @override
  _ReportScreenState createState() => _ReportScreenState();
}

class _ReportScreenState extends State<ReportScreenState> {
  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
          title: MyWidgets.textView(
            text: 'Reports',
            style: TextStyle(color: Colors.black),
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.black,
            ),
            onPressed: () => Navigator.pop(context),
          ),
        ),
        body: FadeIn(
            child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            ListView.builder(
                physics: AlwaysScrollableScrollPhysics(
                    parent: BouncingScrollPhysics()),
                shrinkWrap: true,
                itemCount: 3,
                itemBuilder: (BuildContext context, int index) {
                  return _buildListItem(context, index);
                }),
            SizedBox(
              height: 10,
            ),
            /*MyWidgets.container(
                marginL: 15,
                marginR: 15,
                height: 30,
                topRightCornerRadius: 10,
                topLeftCornerRadius: 10,
                color: MyColors.white_chinese_e0_bg,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    SizedBox(
                      width: 10,
                    ),
                    MyWidgets.textView(text: 'Year'),
                    Spacer(),
                    Icon(
                      Icons.radio_button_off_rounded,
                      size: 20,
                    ),
                    SizedBox(
                      width: 10,
                    )
                  ],
                )),
            SizedBox(
              height: 5,
            ),
            MyWidgets.container(
                marginL: 15,
                marginR: 15,
                height: 30,
                bottomLeftCornerRadius: 10,
                bottomRightCornerRadius: 10,
                color: MyColors.white_chinese_e0_bg,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    SizedBox(
                      width: 10,
                    ),
                    MyWidgets.textView(text: 'Month'),
                    Spacer(),
                    Icon(
                      Icons.radio_button_off_rounded,
                      size: 20,
                    ),
                    SizedBox(
                      width: 10,
                    )
                  ],
                ))*/
          ],
        )));
  }

  Widget _buildListItem(BuildContext context, int index) {
    return ListTile(onTap:(){

      goto(context, ProductReportLineChartScreenState(reportType:index == 0? 'profit_report':index==1?'sales_report':'cost_report',));


    },title:MyWidgets.shadowContainer(
        marginL: 15,
        marginR: 15,
        marginB: 5,
        marginT: 5,
        cornerRadius: 10,
        height: 100,
        color: MyColors.grey_607,
        child: Stack(
          children: [
            Center(
                child: MyWidgets.textView(
                    text: index == 0
                        ? 'Profit'
                        : index == 1
                            ? 'Total sales'
                            : index == 2
                                ? 'Item Cost'
                                : '',
                    style: MyStyles.customTextStyle(
                        fontSize: 15, fontWeight: FontWeight.bold))),
            Align(
                alignment: Alignment.centerLeft,
                child: MyWidgets.container(
                    marginL: 15,
                    bottomLeftCornerRadius: 5,
                    bottomRightCornerRadius: 5,
                    topLeftCornerRadius: 5,
                    topRightCornerRadius: 5,
                    paddingB: 5,
                    paddingL: 5,
                    paddingR: 5,
                    paddingT: 5,
                    child: index == 0
                        ? Icon(
                            Icons.currency_pound_rounded,
                            size: 28,
                          )
                        : index == 1
                            ? Icon(
                                Icons.multiline_chart_rounded,
                                size: 28,
                              )
                            : index == 2
                                ? Icon(
                                    Icons.money_rounded,
                                    size: 28,
                                  )
                                : Container())),
          ],
        )));
  }
}
