import 'dart:async';
import 'dart:convert';
import 'dart:io';


import 'package:ecommerce/profile.dart';
import 'package:ecommerce/settings.dart';
import 'package:ecommerce/upgrade.dart';
import 'package:ecommerce/utils/Subscription.dart';


//import for SKProductWrapper
import 'package:ecommerce/utils/FirebaseUtils.dart';
import 'package:firebase_app_check/firebase_app_check.dart';
import 'package:flutter/foundation.dart';
//import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/inventory.dart';
import 'package:ecommerce/splash.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:ecommerce/workersList.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'addProject.dart';
import 'cartList.dart';
import 'database/orders_operations.dart';
import 'database/project_operations.dart';
import 'database/user_operation.dart';
import 'home.dart';
import 'model/Project.dart';
import 'model/UserCredentials.dart';
import 'orders.dart';
import 'package:firebase_core/firebase_core.dart';

//final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  /*Platform.isAndroid
      ? await Firebase.initializeApp(
          options: FirebaseOptions(
            apiKey: "AIzaSyBT8AS5INl0NtjHpJgX2-fX3UyA97zNjqc",
            appId: "1:513166783463:android:40d67d8fcd1a61015b0342",
            messagingSenderId: "513166783463",
            projectId: "xpress-pos-d6f5c",
            //'[YOUR_APP]/firebaseapp.com',
            //authDomain: 'auth.[com.sultanService.XpressPOS.firebaseio.com]'
          ),
        )
      : await Firebase.initializeApp();*/
  await Firebase.initializeApp();
  await FirebaseAppCheck.instance.activate(
    webRecaptchaSiteKey: 'recaptcha-v3-site-key',
    // Default provider for Android is the Play Integrity provider. You can use the "AndroidProvider" enum to choose
    // your preferred provider. Choose from:
    // 1. debug provider
    // 2. safety net provider
    // 3. play integrity provider
    androidProvider: AndroidProvider.playIntegrity,);

  /*if (Platform.isAndroid) {
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');

    final InitializationSettings initializationSettings =
        InitializationSettings(android: initializationSettingsAndroid);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (String? payload) async {
      if (payload != null) {
        debugPrint('notification payload: $payload');
      }
      *//*selectedNotificationPayload = payload;
        selectNotificationSubject.add(payload);*//*
    });
  } else if (Platform.isIOS) {
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
      requestSoundPermission: true,
      requestBadgePermission: false,
      requestAlertPermission: true,
      // onDidReceiveLocalNotification:
    );
    final InitializationSettings initializationSettings =
        InitializationSettings(iOS: initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      // onSelectNotification: onSelectNotification
    );
    final bool? result = await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: false,
          sound: true,
        );

    if (result == true) {
      print('Notific is true');
    }
    if (result == false) {
      print('Notific is false');
    }
  }*/

  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: /*LoginScreen()*/ /*MyHomePage()*/ SplashScreen(),
    );
  }
}


class MyHomePage extends StatefulWidget {


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
/*  late var _productsList = <ProductDetails>[];
  List<PurchaseDetails> _purchases = <PurchaseDetails>[];*/
  GlobalKey<HomeScreenState> globalHomeKey = GlobalKey();

  var userOperations = UserOperations();
  final teNameController = TextEditingController();
  final teAgeController = TextEditingController();
  int bottomBarSelectedIndex = 0;
  final _selectedItemColor = Colors.white;
  final _unselectedItemColor = Colors.black;
  final _selectedBgColor = Colors.black;
  final _unselectedBgColor = Colors.white;
  final _unselectedTextColor = Colors.grey;
  int _selectedIndex = 0;
  int _activeSubscription = 0;

  Color _getBgColor(int index) =>
      _selectedIndex == index ? _selectedBgColor : _unselectedBgColor;

  Color _getItemColor(int index) =>
      _selectedIndex == index ? _selectedBgColor : _unselectedTextColor;

  @override
  void initState() {

      getDBSubscriptionStatus();
    super.initState();
  }
 Future getDBSubscriptionStatus()async
 {

   await FirebaseUtils().getFirebaseDbFirstUploadMetaData(context).then((value)async {
    await Subscription.getActiveSubscription().then((value)async {
      _activeSubscription=value;

      await _getProjectData(activeSubscription: value);
      if(value==1||value==0){
        await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    UpgradeScreenState()) );
        return;
      }else    SchedulerBinding.instance.addPostFrameCallback((_) {
        showCustomAlertDialog(context,'Caution.! Always make sure to upload the database with the cloud to avoid data loss. Before upload make sure to download the cloud database and sync with the latest device you logged in, because the cloud database is considered as the primary device database. For more please verify the terms and conditions from settings.');

      });

    });
  });
}

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      bottomBarSelectedIndex = index;
    });
  }

  Widget _buildIcon(IconData iconData, String text, int index) => Column(
        children: [
          Container(
              width: /*double.infinity*/ 60,
              height: kBottomNavigationBarHeight,
              child: InkWell(
                  onTap: () => _onItemTapped(index),
                  child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 0,
                      color: _getBgColor(index),
                      child: Icon(iconData)))),
          Text(text,
              style: TextStyle(fontSize: 12, color: _getItemColor(index))),
        ],
      );

  Widget pageCaller(int index) {
    print('current bottomNav page $index  $updateHomeInit');
    switch (index) {
      case 0:
        {
          return HomeScreen(
            key: globalHomeKey,
            updateHomeInit: updateHomeInit,
            addMainProject: _addProjectFromHome,
          );
        }
      case 1:
        {
          //return  ProductCategoryListScreenState();
          return WorkersListScreenState();
        }
      case 2:
        {
          return CartListScreenState();
        }
      case 3:
        {
          return OrdersScreenState();
        }
      case 4:
        {
          // return MoreScreen();
          return InventoryScreenState();
        }
    }
    return Container();
  }

  void _addProjectFromHome() {
    gotoAddProject(null);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyWidgets.appBar(
        elevation: 0,
        toolbarHeight: 130,
        flexibleSpace: Column(
          children: [
            MyWidgets.appBar(
              elevation: 0,
              title: bottomBarSelectedIndex == 0
                  ? FadeIn(
                      child: GestureDetector(
                          onTap: () {
                            _showCreateProjectPicker(context);
                          },
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  MyWidgets.textView(
                                    text: selectedProjectName.length > 14
                                        ? '${selectedProjectName.substring(0, 10)}...'
                                        : '$selectedProjectName',
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 15),
                                    maxLine: 1,
                                  ),
                                  Icon(
                                    Icons.arrow_drop_down_rounded,
                                    color: Colors.blueGrey,
                                    size: 24,
                                  )
                                ],
                              )
                            ],
                          )))
                  : null,

              leading: this.showProfileIcon(context),



              actions: <Widget>[

                IconButton(
                  icon: Icon(
                    Icons.settings,
                    color: MyColors.black_0101,
                  ),
                  onPressed: () async {
                    //goto(context, SettingsScreenState());
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                SettingsScreenState()) ).then((value) => null);

                  },
                ),
              ],
            ),
            /*BottomNavigationBar(
          elevation: 0,
          backgroundColor:MyColors.WHITE,
          currentIndex: bottomBar_selectedIndex,
          type: BottomNavigationBarType.fixed,
          selectedItemColor:_selectedItemColor,
          unselectedItemColor: _unselectedItemColor,
          items:  <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon:Container(height:50,width:50,child:Card(elevation: 3,child: Icon(Icons.home),) ,),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.list),
              label: 'Products',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart),
              label: 'Cart',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.list_alt_outlined),
              label: 'Orders',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.more_horiz),
              label: 'More',
            ),
          ],
          onTap: (int selectedIndex) {
            _onItemTapped(selectedIndex);

          },

        ),*/

            BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              selectedFontSize: 0,
              backgroundColor: MyColors.white_f1_bg,
              elevation: 0,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: _buildIcon(Icons.home, 'Home', 0),
                  label: 'Home',
                ),
                BottomNavigationBarItem(
                  icon: _buildIcon(
                      Icons.supervisor_account_rounded, 'Workers', 1),
                  label: 'Workers',
                ),
                BottomNavigationBarItem(
                  icon: _buildIcon(Icons.shopping_cart, 'Cart', 2),
                  label: 'Cart',
                ),
                BottomNavigationBarItem(
                  icon: _buildIcon(Icons.list_alt_outlined, 'Orders', 3),
                  label: 'Orders',
                ),
                BottomNavigationBarItem(
                  icon: _buildIcon(Icons.inventory, 'Inventory', 4),
                  label: 'Inventory',
                ),
              ],
              currentIndex: _selectedIndex,
              selectedItemColor: _selectedItemColor,
              unselectedItemColor: _unselectedItemColor,
            ),
          ],
        ),
      ),
      body: /*_widgetOptions.elementAt(bottomBar_selectedIndex)*/ pageCaller(
          bottomBarSelectedIndex),
      backgroundColor: Colors.white,
      /* bottomNavigationBar: BottomNavigationBar(
        currentIndex: bottomBar_selectedIndex,
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'Products',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            label: 'Cart',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list_alt_outlined),
            label: 'Orders',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.more_horiz),
            label: 'More',
          ),
        ],
        onTap: (int selectedIndex) {
          _onItemTapped(selectedIndex);
          print("vhvjdJHV $selectedIndex");

     },

      ),*/
    );
    throw UnimplementedError();
  }

  var orderOperations = OrderOperations();

  onDeleteAllDataAndProject(int? index) async {
    var id = projects[index!].id;
    await orderOperations.isOrderNewExistByProjectId(id).then((bool) async {
      if (bool) {
        showCustomAlertDialog(context,
            'In order proceed, either deliver or cancel the order related to the current project.');
      } else {
        await projectOperations.deleteAllDataAndProject(id).then((value) async {
          print('onSuccess');
          await MyPref.setProject(null);
          setSelectedProject();

          setState(() {
            projects.removeAt(index);
            Navigator.of(context).pop(true);
            updateHomeState();
          });
        }).onError((error, stackTrace) {
          print('onDeleteError  ${error}  ${stackTrace}');

        });
      }
    });
  }

  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  var _scrollController = ScrollController();

  void _showCreateProjectPicker(context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15), topRight: Radius.circular(15)),
        ),
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  ListView.builder(
                      key: _listKey,
                      controller: _scrollController,
                      shrinkWrap: true,
                      itemCount: projects.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _buildProjectListItem(
                            context, projects[index], index);
                      }),
                  ListTile(
                      leading: Icon(
                        Icons.add_circle_outline_rounded,
                        size: 39,
                      ),
                      title: Text('Create Project'),
                      onTap: () {
                        Navigator.of(context).pop();
                        gotoAddProject(null);
                      }),
                ],
              ),
            ),
          );
        });
  }

  Widget _buildProjectListItem(
      BuildContext context, Project values, int index) {
    return ListTile(
        leading: Container(
          margin: values.imageBase != null
              ? EdgeInsets.fromLTRB(5, 0, 5, 0)
              : EdgeInsets.all(0),
          child:
              values.imageBase != null && values.imageBase.toString().isNotEmpty
                  ? Container(
                      width: 33.0,
                      height: 33.0,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: Image.memory(
                                base64Decode(values.imageBase.toString()),
                                width: 33.0,
                                height: 33.0,
                                fit: BoxFit.cover,
                                gaplessPlayback: true,
                              ).image)))
                  : Icon(
                      Icons.error_outline,
                      size: 38,
                    ),
        ),
        trailing: Wrap(
          direction: Axis.horizontal,
          children: <Widget>[
            IconButton(
              constraints: BoxConstraints(),
              padding: const EdgeInsets.all(0),
              onPressed: () {
                showDeleteAlertDialog(index);
              },
              icon: Icon(
                Icons.delete_outline,
                size: 15,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            IconButton(
              constraints: BoxConstraints(),
              padding: const EdgeInsets.all(0),
              onPressed: () {
                Navigator.of(context).pop();
                gotoAddProject(values);
              },
              icon: Icon(
                Icons.edit_outlined,
                size: 15,
              ),
            )
          ],
        ),
        title: Text('${values.projectName}'),
        onTap: () {
          Navigator.of(context).pop();
          selectDefaultProject(values);
        });
  }

  showDeleteAlertDialog(int? index) async {
    await showCustomCallbackAlertDialog(
        context: context,
        positiveText: 'Delete',
        msg: 'Are you sure you wish to delete all project related data?',
        positiveClick: () {
          onDeleteAllDataAndProject(index);
          Navigator.of(context).pop(true);
        },
        negativeClick: () {
          Navigator.of(context).pop(false);
        });
  }

  Widget showProfileIcon(BuildContext context)
  {
    return InkWell(
      onTap: () async
      {
        goto(context, ProfileScreenState());
        },
      child:
      Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
    // decoration: BoxDecoration(borderRadius: BorderRadius.circular(30)),
           height: 10,
            width: 10,
            child:
            ClipRRect(borderRadius: BorderRadius.circular(30),
              child:FirebaseAuth.instance.currentUser?.photoURL == null || FirebaseAuth.instance.currentUser?.photoURL?.isEmpty == true?Image.asset(
                'assets/images/profile_avatar_icon.png',
                fit: BoxFit.cover,
                gaplessPlayback: true,
                width: 10,
                height: 10,
              ): Image.network(
                FirebaseAuth.instance.currentUser!.photoURL!,
                width: 10,
                height: 10,
                fit: BoxFit.contain,
              ),
            )
        ),
      ),
    );

    }
  gotoAddProject(Project? project) async {
    await userOperations.getLastLoggedInUser().then((user) async {
      if (user.isNotEmpty) {
        MyBottomSheet().showBottomSheet(context,maxHeight: 0.75, callback: (value)async {
         // if (value != null && value == true) {
           await getDBSubscriptionStatus();
          await  _getProjectData(activeSubscription: _activeSubscription);
         // }
        }, child: AddProjectScreenState(project: project));

      } else
        showCustomAlertDialog(context, 'Please login to create projects');
    });
  }

  var projects = <Project>[];
  UserCredentials? userCredentials;
  var projectOperations = ProjectOperations();
  var selectedProjectName = 'Select project';
  bool updateHomeInit = false;

  Future _getProjectData({int activeSubscription=0}) async {
    /*await projectOperations.projects().then((value) =>
    value.forEach((element) {
      print('project_in_db ${element.id}  ${element.projectName} ${element.userId}');
    }));*/
    await userOperations.getLastLoggedInUser().then((user) async {
      if (user.isNotEmpty) {
        print('selected_user_id ${user.first.id}');
        userCredentials = user.first;
        //refer query if no subscription default project is set to null.
        await projectOperations.projectsByUserIDAndSubscription(user.first.id,_activeSubscription).then((value) async{
          projects = value;
          if (value.isNotEmpty){

            setSelectedProject();
          }
          else
            setState(() {
              //selectedProjectName='Select project';
              updateHomeState();
            });
        });
      }
      setState(() {
        //selectedProjectName='Select project';
        updateHomeState();
      });
    });

  }

  setSelectedProject() async {
    await MyPref.getProject().then((value) {
      setState(() {
        if (value != null) {
          selectedProjectName = (value.projectName)!;
          updateHomeState();
        } else
          selectedProjectName = 'Select project';
        updateHomeState();
      });
    });
  }

  selectDefaultProject(Project project) async {
    await MyPref.setProject(project);
    await setSelectedProject();
  }

  updateHomeState() {
    updateHomeInit = true;
    globalHomeKey.currentState?.updateHomeState();
  }
}

