import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecommerce/model/UserCredentials.dart';
import 'package:ecommerce/settings.dart';
import 'package:ecommerce/upgrade.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/FirebaseUtils.dart';
import 'package:ecommerce/utils/InAppPurchaseHandler.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

import 'loginView.dart';

class ProfileScreenState extends StatefulWidget {
  const ProfileScreenState({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreenState> {
  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    getUserData();
    super.initState();
  }

  final teNameController = TextEditingController();
  final tePhoneController = TextEditingController();
  final teEmailController = TextEditingController();
  String image = '';

  @override
  Widget build(BuildContext context) {
    final imageView = GestureDetector(
        onTap: () {},
        child: Container(
          decoration: BoxDecoration(
              // color: Colors.grey[200],
              borderRadius: BorderRadius.circular(75)),
          height: 104,
          width: 104,
          child: image.isNotEmpty
              ? ClipRRect(
                  borderRadius: BorderRadius.circular(75),
                  child: Image.network(
                    image,
                    width: 100,
                    height: 100,
                    fit: BoxFit.cover,
                    gaplessPlayback: true,
                  ),
                )
              : Image.asset(
                  'assets/images/profile_avatar_icon.png',
                  fit: BoxFit.cover,
                  gaplessPlayback: true,
                ),
        )
    );
    final nameTextVw = Container(
        padding: EdgeInsets.only(left: 8, right: 8),
        decoration: BoxDecoration(
            color: MyColors.white,
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: SizedBox(
            height: 55.0,
            child: TextField(
              controller: teNameController,
              style: TextStyle(fontWeight: FontWeight.w500),
              keyboardType: TextInputType.name,
              decoration: InputDecoration(
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                hintStyle: TextStyle(
                  height: 0.5,
                  // sets the distance between label and input
                ),
                hintText: '',
                // needed to create space between label and input
                labelStyle: TextStyle(
                  color: MyColors.grey_70,
                  decorationThickness: 0,
                ),
                labelText: 'Name',
              ),
            ))
    );
    final phoneTextVw = Container(
        padding: EdgeInsets.only(left: 8, right: 8),
        decoration: BoxDecoration(
            color: MyColors.white,
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: SizedBox(
            height: 55.0,
            child: TextField(
              enabled: false,
              controller: tePhoneController,
              style: TextStyle(fontWeight: FontWeight.w500),
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}')),
              ],
              decoration: InputDecoration(
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                hintStyle: TextStyle(
                  height: 0.5,
                  // sets the distance between label and input
                ),
                hintText: '',
                // needed to create space between label and input
                labelStyle: TextStyle(
                  color: MyColors.grey_70,
                  decorationThickness: 0,
                ),
                labelText: 'Phone No.',
              ),
            ))
    );
    final emailTextVw = Container(
        padding: EdgeInsets.only(left: 8, right: 8),
        decoration: BoxDecoration(
            color: MyColors.white,
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: SizedBox(
            height: 55.0,
            child: TextField(
              readOnly: true,
              controller: teEmailController,
              style: TextStyle(fontWeight: FontWeight.w500),
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                hintStyle: TextStyle(
                  height: 0.5,
                  // sets the distance between label and input
                ),
                hintText: '',
                // needed to create space between label and input
                labelStyle: TextStyle(
                  color: MyColors.grey_70,
                  decorationThickness: 0,
                ),
                labelText: 'Email',
              ),
            ))
    );
    final saveProfileChangesBtn = MyWidgets.shadowContainer(
        height: 45,
        color: MyColors.white_chinese_e0_bg,
        shadowColor: MyColors.white_f1_bg,
        cornerRadius: 8,
        spreadRadius: 0,
        blurRadius: 0,
        child: InkWell(
            onTap: ()async {
             await saveProfileAPI();
            },
            child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: Padding(
                        padding: EdgeInsets.only(left: 8),
                        child: MyWidgets.textView(text: 'Save changes'),
                      )),
                ]))
    );
    final proButton = Container(
        height: 45,
        child: Material(
            elevation: 1,
            borderRadius: BorderRadius.circular(8),
            color: MyColors.white_dutch_ec_bg,
            child: MaterialButton(
                onPressed: () async {
                  /*await InAppPurchaseHandler.handleInAppPurchase(context);*/ goto(
                      context, UpgradeScreenState());
                },
                child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      MyWidgets.textView(text: 'Upgrade to Pro'),
                      SizedBox(
                        width: 5,
                      ),
                      ImageIcon(
                        AssetImage('assets/images/icon_awesome_crown.png'),
                        size: 20,
                      ),
                    ])))
    );
    final updateProfileButton = Container(


        height: 45,
        child: Material(
            elevation: 1,
            borderRadius: BorderRadius.circular(8),
            color: MyColors.white_chinese_e0_bg,
            child: MaterialButton(
                onPressed: () async {
                  if(teEmailController.text.toString().isNotEmpty){
                  await userOperations.updateUserNameByEmail(teNameController.text.toString().trim(),teEmailController.text.toString().trim()).then((value) {
                    showCustomAlertDialog(context, 'Successfully updates.');
                  });
                  }else{
                    context.showSnackBar("Email field can't be empty.");
                  }
                },
                child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      MyWidgets.textView(text: 'Update Profile'),

                    ]))));
    final logOutButton = Container(
        height: 45,
        child: Material(
            elevation: 1,
            borderRadius: BorderRadius.circular(8),
            color: MyColors.white,
            child: MaterialButton(
                onPressed: () async {
                  await logoutFunction(context);
                },
                child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      MyWidgets.textView(text: 'Logout'),
                      SizedBox(
                        width: 5,
                      ),
                      RotatedBox(
                        quarterTurns: 2,
                        child: Icon(
                          Icons.logout_rounded,
                          size: 20,
                        ),
                      )
                    ]))));
    final deleteUserAccountButton = Padding(
      padding: const EdgeInsets.all(20.0),
      child: Container(
          height: 45,
          child: MaterialButton(
              onPressed: ()  {
                 deleteAccountAlert(context);
              },
              child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    MyWidgets.textView(text: 'Delete your Account',style: TextStyle(color: MyColors.red_ff_bg)),
                    SizedBox(
                      width: 5,
                    ),
                  ])
          )
      ),
    );

    return MyWidgets.scaffold(
        appBar: MyWidgets.appBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.black,
            ),
            onPressed: () => Navigator.pop(context, true),
          ),
        ),
        body: Container(
            // padding: EdgeInsets.all(15),
            height: MediaQuery.of(context).size.height,
            child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(
                    parent: BouncingScrollPhysics()),
                padding:
                    EdgeInsets.only(left: 30, right: 30, top: 10, bottom: 10),
                child:
                    Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
                  imageView,
                  SizedBox(
                    height: 20,
                  ),
                  nameTextVw,
                  SizedBox(
                    height: 20,
                  ),
                  phoneTextVw,
                  SizedBox(
                    height: 20,
                  ),
                  emailTextVw,
                  SizedBox(
                    height: 15,
                  ),
                      updateProfileButton,
                      SizedBox(
                        height: 10,
                      ),
                  Divider(),
                  SizedBox(
                    height: 10,
                  ),
                  // saveProfileChangesBtn,
                  SizedBox(
                    height: 100,
                  ),
                  proButton,
                  SizedBox(
                    height: 15,
                  ),
                  logOutButton,
                  deleteUserAccountButton,
                ])
            )
        ));
  }

  getUserData() async {
    if (await checkInternetConnectivity()) {
      await userOperations.getLastLoggedInUser().then((user) async {
        if (user.isNotEmpty) {
          if (user.isNotEmpty &&
              FirebaseAuth.instance.currentUser!.providerData.first.email ==
                  user.first.userEmail) {
            setUser(user.first);
          } else
            showCustomCallbackAlertDialog(
                context: context,
                msg: 'Something went wrong. Please login again',
                positiveClick: await logoutFunction(context),
                negativeClick: await logoutFunction(context));
        }
      });
      return;
    } else {
      await userOperations.getLastLoggedInUser().then((user) async {
        if (user.isNotEmpty) {
          setUser(user.first);
        }
      });
    }
    try {} catch (e) {}
  }


  Future<void> saveProfileAPI() async {

  }
  setUser(UserCredentials user) {
    setState(() {
      image = user.userPhoto ?? '';
      teNameController.text = user.userName ?? '';
      tePhoneController.text = user.userMobile ?? '';
      teEmailController.text = user.userEmail ?? '';
    });
  }

  deleteAccountAlert(BuildContext context)
  {
    showCustomCallbackAlertDialog(context: context,
        msg: "Are you sure want to delete this account now!. If you delete your account your all the Data from your devices you logged in and server will be deleted permanently."
            "While deleting your account you will be asked to re authenticate your account to confirm the user.",
        positiveText: "Delete Account",
        positiveClick: ()  {
          deleteAccountFunction(context);
        },
        negativeText: "Cancel",
        negativeClick: (){
            Navigator.pop(context);
        }
    );
  }
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final deleteEmailTextFController = TextEditingController();
  final deletePasswordTextFController = TextEditingController();
  deleteAccountFunction(BuildContext context) async
  {
    // await FirebaseAuth.instance.currentUser?.reauthenticateWithCredential(credential);
//checking whether password is already stored in firestore for this email id.
    await showDialog(
        context: context,
        // barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            contentPadding: EdgeInsets.only(top: 15.0 ),
            content: Container(
              width: 300.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  MyWidgets.textView(text: "Please Enter your Email & Password" , style: MyStyles.customTextStyle(fontSize: 17.0,fontWeight: FontWeight.bold,color: MyColors.grey_70),),
                  SizedBox(height: 10,),
                  Divider(
                    color: Colors.grey,
                    height: 4.0,
                  ),
                  Padding(padding: EdgeInsets.all(10),
                    child: MyWidgets.textView(text: "Please give your Email & Password to verify the user." , style: MyStyles.customTextStyle(fontSize: 15.0,fontWeight: FontWeight.normal,color: MyColors.grey_70),),
                  ),
                  SizedBox(height: 10,),
                  Padding(
                    padding: EdgeInsets.only(left: 15.0, right: 15.0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child: Text('Email Id',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w200,
                                      )) //Container
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                  flex: 2,
                                  fit: FlexFit.tight,
                                  child: TextFormField(
                                      keyboardType: TextInputType.emailAddress,
                                      controller: deleteEmailTextFController,
                                      decoration: InputDecoration()) //Container
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child: Text('Password',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w200,
                                      )) //Container
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                  flex: 2,
                                  fit: FlexFit.tight,
                                  child: TextFormField(
                                      keyboardType: TextInputType.visiblePassword,
                                      controller: deletePasswordTextFController,
                                      decoration: InputDecoration()) //Container
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  InkWell(
                    onTap: ()async => {
                      if (_validatePassword())
                        {
                          if(FirebaseAuth.instance.currentUser!.email == deleteEmailTextFController.text.trim().toLowerCase())
                            {
                              deleteUserPermanentlyFunc(deleteEmailTextFController.text.trim().trim().toLowerCase(), deletePasswordTextFController.text.trim())
                            }
                          else
                            {
                              showCustomAlertOkActionDialog(
                                  context: context,
                                  msg: "Entered Credential doesn't match with currently logged in user. Please login again.",
                                  positiveClick: (){
                                    logoutFunction(context);
                                  },
                              )
                            }
                        }
                    },
                    child: Container(
                      padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                      decoration: BoxDecoration(
                        color: MyColors.white_chinese_e0_bg,
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20.0),
                            bottomRight: Radius.circular(20.0)),
                      ),
                      child: MyWidgets.textView(text: "Continue Delete Account", style: MyStyles.customTextStyle(color: MyColors.grey_70),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  bool _validatePassword()
  {
    if (deleteEmailTextFController.text.isEmpty)
    {
      showCustomAlertDialog(context, "Please enter your Email id associated with this account.");
      return false;
    }
    else if (!RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(deleteEmailTextFController.text.toString().trim()))
    {
      context.showSnackBar("Please enter a valid email address");
      return false;
    }
    else if (deletePasswordTextFController.text.isEmpty)
    {
      context.showSnackBar("Please enter your password to continue");
      return false;
    }
    return true;
  }



  Future deleteUserPermanentlyFunc(String email, String password) async
    {
      final FirebaseAuth _auth = FirebaseAuth.instance;

      AuthCredential credentials =  EmailAuthProvider.credential(email: email, password: password);
      showNonCancellableCircularLoader(context, 'Re-authenticating user...');

      var isAccountHasApple = false;
      _auth.currentUser!.providerData.forEach((element) {
        if(element.providerId.contains('apple.com'))
        {
          isAccountHasApple = true;
          // callAppleTokenRefreshFunc(context);
          // callAppleRevokeFunc(context);
        }
      });



      //1.Re-authenticating user
      try
      {
        print('AAAAAAA');
        await _auth.currentUser?.reauthenticateWithCredential(credentials);
        print('BBBBBBB');
      }
      on FirebaseAuthException catch (e)
      {
        Navigator.pop(context);
        print('Re-authentication Error : ${e.message!} ${e.toString()}');
        showCustomAlertOkActionDialog(
            context: context,
            msg: '${e.message!}',
            positiveText: 'Login Using Email & Password Again',
            positiveClick: (){
              logoutFunction(context);
            });
        return;
      }

      // //2.checking whether the account is associated with apple also
      // Navigator.pop(context);
      // _auth.currentUser!.providerData.forEach((element) {
      //   if(element.providerId.contains('apple.com'))
      //   {
      //
      //     // callAppleTokenRefreshFunc(context);
      //     // callAppleRevokeFunc(context);
      //   }
      // });


      //3.Deleting Firebase Storage data
      try
      {
        print('CCCCCCCC');
        Navigator.pop(context);
        showNonCancellableCircularLoader(context, 'Deleting Database...');
        Reference ref = FirebaseUtils().storageRef;
        await ref.delete();
        print('DDDDDDDD');
      }
      on FirebaseException catch (e) {
        Navigator.pop(context);
        context.showSnackBar('Delete Storage Data Error : ${e.message!}');
        return;
      }




      //4.Deleting Firestore data
      try
      {
        print('EEEEEEEE');
        Navigator.pop(context);
        showNonCancellableCircularLoader(context, 'Deleting Whole Database...');
        await  FirebaseFirestore.instance.collection("users").doc(FirebaseAuth.instance.currentUser!.email).delete();
        print('FFFFFFFF');
      }
      on FirebaseAuthException catch (e)
      {
        Navigator.pop(context);
        context.showSnackBar('Delete Whole Data Error : ${e.message!}');
        return;
      }

      //5.Deleting account
      try
      {
        print('GGGGGGG');
        Navigator.pop(context);
        showNonCancellableCircularLoader(context, 'Deleting account...');
        await _auth.currentUser?.delete();
        print('HHHHHHH');

        if(isAccountHasApple)
          {
            showCustomAlertOkActionDialog(
                context: context,
                msg: 'Your account and all data has permanently removed from our cloud.You can remove ExpressPos from your apps using Apple ID,\nGoto Settings->\nYour Account Info->\nPassword&Security->\nApps using Apple ID. From here you can remove.',
                positiveClick: (){
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (context) => LoginScreen()),
                          (Route<dynamic> route) => false);
            });
          }
        else
          {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => LoginScreen()),
                    (Route<dynamic> route) => false);
          }
      }
      on FirebaseAuthException catch (e)
      {
        Navigator.pop(context);
        context.showSnackBar('Delete User Error : ${e.message!}');
        return;
      }
    }

  void callAppleTokenRefreshFunc(BuildContext context) async
  {
    checkInternetConnectivity().then((internet) async
    {
      if (internet != null && internet)
      {
        // showNonCancellableCircularLoader(context, 'Detecting apple revoke');
        var requestBody = {
          'client_id' : 'com.technologylab.xpressposAppleSignIn',
          // 'client_secret': 'VK5463KUYC',
          'client_secret': 'MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgGHwBWasfc33LaUjdn0hyz0tcW0hdYkzqMBWCTf6ppHOgCgYIKoZIzj0DAQehRANCAAR3S2HMNQ2XzRnRm56PfJAO4jTqgC3tFj09Gs5kmC04MVPeP0ZlWUykYYxlhqKwnbtUIPPMT5S8HUPyq6cELd+l',
          'grant_type' : 'refresh_token',
          'refresh_token': 'refresh_token',
        };

        var url = 'https://appleid.apple.com/auth/token';
        var client = http.Client();
        try
        {
          var response = await client.post(Uri.parse(url), body: requestBody, headers: {'Content-Type': 'application/x-www-form-urlencoded'});
          print('respoooooooo  ==== ${response.body}');
          print('respoooooooo  jsonDecode ==== ${jsonDecode(response.statusCode.toString())}');
          var decodedResponse = jsonDecode(utf8.decode(response.bodyBytes)) as Map;
          print('\n\n\ndecodedResponse  ==== ${decodedResponse}');
          var uri = Uri.parse(decodedResponse['uri'] as String);
          print('\n\n\nuri  ==== ${uri} ========\n\n\n');
          print(await client.get(uri));
        } finally
        {
          client.close();
        }

        return;
      }
      else {

      }
    });
  }
  //c13c040cf4f5945f4984dda9d87465397.0.srswy.tnrWNWTIekwQMkj9IB_unA


  void callAppleRevokeFunc(BuildContext context) async
  {
    checkInternetConnectivity().then((internet) async
    {
      if (internet != null && internet)
      {
        // showNonCancellableCircularLoader(context, 'Detecting apple revoke');
        var requestBody = {
          'client_id' : 'com.technologylab.xpressposAppleSignIn',
          'client_secret': 'MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgGHwBWasfc33LaUjdn0hyz0tcW0hdYkzqMBWCTf6ppHOgCgYIKoZIzj0DAQehRANCAAR3S2HMNQ2XzRnRm56PfJAO4jTqgC3tFj09Gs5kmC04MVPeP0ZlWUykYYxlhqKwnbtUIPPMT5S8HUPyq6cELd+l',
          'token': 'eyJraWQiOiJZdXlYb1kiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwczovL2FwcGxlaWQuYXBwbGUuY29tIiwiYXVkIjoiY29tLnRlY2hub2xvZ3lsYWIueHByZXNzcG9zIiwiZXhwIjoxNjcyNDAxMjIxLCJpYXQiOjE2NzIzMTQ4MjEsInN1YiI6IjAwMTI2OC41MGEwYWNhMjUwZmY0MDFkODBhMWEyYTQxMWI4NzEyNC4wNzEyIiwibm9uY2UiOiI3YmE1NmRkOTJiNmNlOTgyZjVlZDJkNDA0ZGZmZWQ5ZGYzZTc3NThiNGQ3OGY1YjI5ZDVlZWYzNDViMTVmOTJkIiwiY19oYXNoIjoiSlJIVi1IaFF5bmp6YTEySi0yMUdJQSIsImVtYWlsIjoicS5kZXZlbG9wZXIuZmFoYW5hc0BnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6InRydWUiLCJhdXRoX3RpbWUiOjE2NzIzMTQ4MjEsIm5vbmNlX3N1cHBvcnRlZCI6dHJ1ZX0.yiDLAyiDxjsio4NqOh2pK8yOJauFWaGbItJhIyheWeJfCklza5Q8c75WV5jRN9Jqpd9xwCzVn114owfzh1WTZxQvFVbsoyUfuGzzGUvBk4o8X3v9iViOJ1qEqoTGkLjBl2URhx7Ex_ZmOXrUoGIrr1ozD0Yg5fZ4D-TkovA_MEBbe9fntLxaN6nbKT46d6NbwNmo7-8BdBYJVc_u0Pal1Tv59sC5KuNfkmgq4NNsjWyCIExrObc2frSDkpeG54K24ne_kzcrsZEnNMCXgsGA08X_JJ_eGxDzAEISLmr7wkE4nmrTgMqZGy41ycHWbwjn896BQ-AzOgVsprQX9oxkqw',
          'token_type_hint': "access_token"
        };

        var url = 'https://appleid.apple.com/auth/revoke';
        var client = http.Client();
        try
        {
          var response = await client.post(Uri.parse(url), body: requestBody, headers: {'Content-Type': 'application/x-www-form-urlencoded'});
          print('respoooooooo  ==== ${response.toString()}');
          print('respoooooooo  jsonDecode ==== ${jsonDecode(response.body)}');
          var decodedResponse = jsonDecode(utf8.decode(response.bodyBytes)) as Map;
          print('\n\n\ndecodedResponse  ==== ${decodedResponse}');
          var uri = Uri.parse(decodedResponse['uri'] as String);
          print('\n\n\nuri  ==== ${uri} ========\n\n\n');
          print(await client.get(uri));
        } finally
        {
          client.close();
        }

        return;
      }
      else {

      }
    });
  }


}




// UserCredential(
//   additionalUserInfo: AdditionalUserInfo(
//          isNewUser: false,
//          profile: {},
//          providerId:
//          password, username: null
//       ),
//       credential: null,
//       user: User(
//           displayName: muhammed Fahanas,
//           email: q.developer.fahanas@gmail.com,
//           emailVerified: true,
//           isAnonymous: false,
//           metadata: UserMetadata(creationTime: 2022-12-28 10:RuWfgWluB1TtFiGDyfQpo9238A3217:07.072Z, lastSignInTime: 2022-12-28 10:42:43.010Z),
//           phoneNumber: null,
//           photoURL: https://lh3.googleusercontent.com/a/AEdFTp7_AGnYODGd4uHojOH0J7ZiG6ik20o0UYrivUrY=s96-c,
//           providerData, [
//             UserInfo(displayName: muhammed Fahanas, email: q.developer.fahanas@gmail.com, phoneNumber: null, photoURL: https:YrivUrY=s96-c, providerId: google.com, uid: 117450145254236099335),
//             UserInfo(displayName: null, email: q.developer.fahanas@gmail.com, phoneNumber: null, photoURL: null, providerId: apple.com, uid: 001268.50a0aca250ff401d80a1a2a411b87124.0712),
//             UserInfo(displayName: muhammed Fahanas, email: q.developer.fahanas@gmail.com, phoneNumber: null, photoURL: https:20o0UYrivUrY=s96-c, providerId: password, uid: q.developer.fahanas@gmail.com)
//           ],
//           refreshToken: ,
//           tenantId: null,
//           uid: RuWfgWluB1TtFiGDyfQpo9238A32
//       )
// )