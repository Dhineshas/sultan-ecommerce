import 'dart:convert';
import 'dart:io';
import 'package:ecommerce/database/project_operations.dart';
import 'package:ecommerce/model/Worker.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'database/product_item_operations.dart';
import 'database/workers_operations.dart';
import 'model/Project.dart';


class AddWorkerScreenState extends StatefulWidget {
  final Worker? worker;
   AddWorkerScreenState({@required this.worker,Key? key}) : super(key: key);

  @override
  _AddWorkerScreenState createState() => _AddWorkerScreenState();
}

class _AddWorkerScreenState extends State<AddWorkerScreenState> {

  File? _image;

  final picker = ImagePicker();
  final teWorkerNameController = TextEditingController();
  final teWorkerSalaryController = TextEditingController();
  final teWorkerHoursPerDayController = TextEditingController();
  var workersOperations = WorkersOperations();
  var productItemOperations = ProductItemOperations();

  var projectOperations = ProjectOperations();
  var projectItemImageBase64;

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    if (widget.worker != null) {
      teWorkerNameController.text = '${widget.worker?.name}';
      teWorkerSalaryController.text = '${widget.worker?.salary}';
      teWorkerHoursPerDayController.text = '${widget.worker?.hoursPerDay}';
      //projectItemImageBase64 = widget.worker?.imageBase;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final imageView =
    GestureDetector(
      onTap: () {
        _showPicker(context);
      },
      child:  Container(
            decoration: BoxDecoration(
              // color: Colors.grey[200],
                borderRadius: BorderRadius.circular(75)
            ),
            height: 104,
            width: 104,
            child: projectItemImageBase64 != null ?
            ClipRRect(
              borderRadius: BorderRadius.circular(75),
              child: Image.memory(
                base64Decode('${projectItemImageBase64}'),
                width: 100,
                height: 100,
                fit: BoxFit.cover,
                gaplessPlayback: true,
              ),
            ) : _image != null
                ? ClipRRect(
              borderRadius: BorderRadius.circular(75),
              child: Image.file(
                _image!,
                width: 100,
                height: 100,
                fit: BoxFit.cover,
                gaplessPlayback: true,
              ),
            )
                : Container(
              decoration: BoxDecoration(
                // color: Colors.grey[200],
                  border: Border.all(color: Colors.black, width: 1),
                  borderRadius: BorderRadius.circular(75)
              ),
              width: 100,
              height: 100,
              child: Icon(
                Icons.image_rounded,
                color: Colors.grey[800],
              ),
            ),
          ));
    final addWorkerTextView =
    Align(alignment: Alignment.centerLeft,
      child: MyWidgets.textView(text: 'Add Worker',
        style: MyStyles.customTextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 24,
        ),
      ),
    );

    final workerNameTextView =
    Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10,),
        MyWidgets.textView(text: 'Name'),
        SizedBox(height: 10,),
        MyWidgets.textViewContainer(
          height: 45, cornerRadius: 8, color: MyColors.white,
          // color: Colors.white54,
          child:Align(alignment:Alignment.centerLeft,child: MyWidgets.textFromField(
            contentPaddingL: 15.0,

            contentPaddingR: 15.0,

            cornerRadius: 8,
            keyboardType: TextInputType.name,
            controller: teWorkerNameController,
          )),
        ),
      ],
    );
    final workerSalaryTextView =

    Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10,),
        MyWidgets.textView(text: 'Salary',),
        SizedBox(height: 10,),
        MyWidgets.textViewContainer(
          height: 45, cornerRadius: 8, color: MyColors.white,
          child:Align(alignment:Alignment.centerLeft,child: MyWidgets.textFromField(
            contentPaddingL: 15.0,

            contentPaddingR: 15.0,

            cornerRadius: 8,
            keyboardType: TextInputType.phone,
            controller: teWorkerSalaryController,

          )),
        ),
      ],
    );

    final workingHoursPerDayTextView =
    Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          //MyWidgets.textView(text: 'Working Hours'),
          MyText.priceCurrencyText(context,price: 'Working Hours',exponent:'(Per Day)',fontSize: 14,fontWeight: FontWeight.normal),

          SizedBox(height: 10,),
          MyWidgets.textViewContainer(

            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child:Align(alignment:Alignment.centerLeft,child: MyWidgets.textFromField(

              contentPaddingL: 15.0,

              contentPaddingR: 15.0,

              cornerRadius: 8,
              keyboardType: TextInputType.phone,
              controller: teWorkerHoursPerDayController,
            )),
          ),
        ]
    );
    final addWorkerButton =
    MyWidgets.materialButton(context,text:widget.worker == null ? "Add Worker" : "Update Worker",onPressed:() { if(_validate()){
      addOrUpdateWorker(); }});
    return Scaffold(
        backgroundColor: Colors.transparent,

        body: SingleChildScrollView(
            padding: EdgeInsets.only(left: 30, right: 30, top: 25, bottom: 20),
            physics: BouncingScrollPhysics(),
            child: Column(
              children: <Widget>[
                addWorkerTextView,
                /*SizedBox(
                  height: 30,
                ),
                imageView,*/

                SizedBox(
                  height: 20,
                ),
                workerNameTextView,
                workerSalaryTextView,
                workingHoursPerDayTextView,
                SizedBox(
                  height: 30,
                ),
                addWorkerButton
              ],
            ))
    );
  }

  void _showPicker(context) {
    /*showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  ListTile(
                    leading: Icon(Icons.photo_camera),
                    title: Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });*/
    MyImagePickerBottomSheet.showPicker(context, fileCallback: (image) {
      projectItemImageBase64 = null;
      setState(() {
        _image = image != null ? File(image.path) : null;
      });
      return null;
    });
  }


  /* _imgFromCamera() async {
    var image =
    await picker.pickImage(source: ImageSource.camera, imageQuality: 0,maxHeight:150,maxWidth: 150);
    projectItemImageBase64 = null;
    setState(() {
      _image = image!=null?File(image.path):null;
    });
  }

  _imgFromGallery() async {
    var image =
    await picker.pickImage(source: ImageSource.gallery, imageQuality: 0,maxHeight:150,maxWidth: 150);
    projectItemImageBase64 = null;
    setState(() {
      _image = image!=null?File(image.path):null;


    });
  }*/
addOrUpdateWorker()async{
  await MyPref.getProject().then((project)async {
    if(project!=null){
      if(_validate()){widget.worker!=null?updateWorkerWithTransaction(widget.worker!):addWorker(project);}

    }else
    showCustomAlertDialog(context, 'Please create a project ');
  });
}
  bool _validate() {
    if (teWorkerNameController.text.isEmpty) {
      context.showSnackBar("Enter Worker Name");
      return false;
    }else if (teWorkerNameController.text.startsWith(RegExp(r'^\d+\.?\d{0,2}'))) {
      context.showSnackBar("Can\'t Start Worker Name with Number");
      return false;
    } else if (teWorkerSalaryController.text.isEmpty ||
        int.tryParse(teWorkerSalaryController.text.trim()) == 0) {
      context.showSnackBar("Enter Worker Salary");
      return false;
    } else if (teWorkerHoursPerDayController.text.isEmpty ||
        int.tryParse(teWorkerHoursPerDayController.text.trim()) == 0) {
      context.showSnackBar("Enter Worker Hours per Day");
      return false;
    } else if (int.tryParse(teWorkerHoursPerDayController.text.trim())! > 24) {
      context.showSnackBar("Please Check Worker Hours per Day");
      return false;
    }
    return true;
  }


  addWorker(Project? project) async {
    if (project != null) {
      var worker = Worker();
      worker.projectId = project.id;
      worker.name = teWorkerNameController.text..toString().trim();
      worker.salary = int.parse(teWorkerSalaryController.text.trim());
      worker.hoursPerDay = int.parse(teWorkerHoursPerDayController.text.trim());
      worker.wagePerMinNew =
      (((worker.salary)! / (worker.hoursPerDay! * 30)) / 60);
      await workersOperations.insertWorker(worker).then((value) {
        clearControllerAndPop();
      });
    }
  }
  updateWorkerWithTransaction(Worker w) async{

    var worker = Worker();
    worker.id = w.id;
    worker.projectId = w.projectId;
    worker.name = teWorkerNameController.text.trim().toString();
    worker.salary = int.parse(teWorkerSalaryController.text.trim());
    worker.hoursPerDay = int.parse(teWorkerHoursPerDayController.text.trim());
    double? currentPerMinWage=0.00;
    currentPerMinWage=(((worker.salary)!/(worker.hoursPerDay!*30))/60).toDouble();
    if(currentPerMinWage>w.wagePerMinNew!){
      worker.wagePerMinOld=w.wagePerMinNew;
      worker.wagePerMinNew=currentPerMinWage;
    }else if(currentPerMinWage<w.wagePerMinNew!){
      worker.wagePerMinOld= currentPerMinWage;
      worker.wagePerMinNew=w.wagePerMinNew;
    }else  if(currentPerMinWage==w.wagePerMinNew!){
      worker.wagePerMinOld= w.wagePerMinOld;
      worker.wagePerMinNew=w.wagePerMinNew;
    }


    (currentPerMinWage>w.wagePerMinNew!)?
    await workersOperations.updateWorkerWithTransaction(worker).then((value) {clearControllerAndPop();print('onUpdateIntSuccess ');}).onError((error, stackTrace) {clearControllerAndPop(isSuccess: false);print('onUpdateInvError  ${error}  ${stackTrace}');context.showSnackBar('$error');})
        :
    await workersOperations.updateWorker(worker).then((value){clearControllerAndPop();}).onError((error, stackTrace) {clearControllerAndPop(isSuccess: false);context.showSnackBar('$error');});
  }

  clearControllerAndPop({bool isSuccess = true}) async {
    teWorkerNameController.text = '';
    teWorkerSalaryController.text = '';
    teWorkerHoursPerDayController.text = '';
    Navigator.of(context).pop(isSuccess);
  }
}