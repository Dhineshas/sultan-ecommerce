import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/model/Inventory.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'inventoryUnitMeasures.dart';
import 'database/history_operations.dart';
import 'database/inventory_item_operations.dart';
import 'database/product_item_operations.dart';
import 'database/relation_operations.dart';
import 'model/History.dart';
import 'model/InventoryItem.dart';
import 'package:image_picker/image_picker.dart';

import 'model/ProductItem.dart';

class EditInventoryItemScreenState extends StatefulWidget {
  final Inventory? inventory;
  //final InventoryItem? inventoryItem;
  final int? inventoryItemId;


  const EditInventoryItemScreenState({@required this.inventory,/*@required this.inventoryItem,*/@required this.inventoryItemId, Key? key})
      : super(key: key);

  @override
  _EditInventoryItemScreenState createState() => _EditInventoryItemScreenState();
}

class _EditInventoryItemScreenState extends State<EditInventoryItemScreenState> {
  File? _image;
  final picker = ImagePicker();
  var inventoryItemOperations = InventoryItemOperations();
  var productItemOperations = ProductItemOperations();
  var inventoryRelationOperations = RelationOperations();
  final teInvNameController = TextEditingController();
  final teInvTotalTypeCountController = TextEditingController();
  final teInvPiecesInTypeController = TextEditingController();
  final teInvTotalCostPerTypeController = TextEditingController();
  int? selectedTypeIndex = 0;
  String dropdownSelectedTypeValue = '';
  var productItemImageBase64;
  double? balanceStock=0.0;
  int? integerBalanceStock = 0;
  int? decimalBalanceStock = 0;
  @override
  void setState(VoidCallback fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  @override
  void initState() {
    if(widget.inventoryItemId!=null){
     /* productItemImageBase64 = widget.inventoryItem?.imageBase;
      teInvNameController.text='${widget.inventoryItem?.name}';
      teInvTotalTypeCountController.text='0';
      teInvPiecesInTypeController.text='${widget.inventoryItem?.piecesInType}';
      selectedTypeIndex=widget.inventoryItem?.itemType;
      teInvTotalCostPerTypeController.text='${widget.inventoryItem?.costPerTypeNew}';
      dropdownSelectedTypeValue= InventoryUnitMeasure.inventoryUnitMeasureArray[selectedTypeIndex!];
      balanceStock=(widget.inventoryItem?.stock)!/(widget.inventoryItem?.piecesInType)!;
      integerBalanceStock=balanceStock?.toInt();
      //var parseDecimal=(widget.inventoryItem?.stock)!/(widget.inventoryItem?.totalType)!;
      decimalBalanceStock=((widget.inventoryItem?.stock)!%(widget.inventoryItem?.piecesInType)!).toInt();
      //decimalBalanceStock=int.tryParse(balanceStock.toString().split('.')[1]);*/

      Future.delayed( Duration(milliseconds: 1000), () {
        _getData();
print('lllllloooooo');
      });
    }
    print('ggggggggoooooo');
    super.initState();
  }
  bool isLoading = true;
  InventoryItem? item;
  Future<InventoryItem> _getData() async {
    await inventoryItemOperations.inventoriesItemsByInventoryItemId(widget.inventoryItemId).then((value) {
      print('projectIDD  ${value.first.projectId}  ${value.first.costPerTypeNew}');
      item=value.first;
      print('hhhhhhhhoooooo');
      setState(() {
        productItemImageBase64 = item?.imageBase;
        teInvNameController.text='${item?.name}';
        teInvTotalTypeCountController.text='0';
        teInvPiecesInTypeController.text='${item?.piecesInType}';
        selectedTypeIndex=item?.itemType;
        //teInvTotalCostPerTypeController.text='${item?.costPerTypeNew}';
        dropdownSelectedTypeValue= InventoryUnitMeasure.inventoryUnitMeasureArray[selectedTypeIndex!];
        balanceStock=(item?.stock)!/(item?.piecesInType)!;
        integerBalanceStock=balanceStock?.toInt();
        //var parseDecimal=(widget.inventoryItem?.stock)!/(widget.inventoryItem?.totalType)!;
        decimalBalanceStock=((item?.stock)!%(item?.piecesInType)!).toInt();
        //decimalBalanceStock=int.tryParse(balanceStock.toString().split('.')[1]);
        isLoading=false;
        print('iiiiiiiiioooooo');
      });


    });
    return item!;
  }
  @override
  Widget build(BuildContext context) {
    final updateInventoryItemText =
    Align(alignment: Alignment.centerLeft,
      child: MyWidgets.textView(text: 'Update Item',
        style: MyStyles.customTextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 24,
        ),
      ),
    );
    final imageView = /*FadeInUp(delay: Duration(milliseconds: 100),child:
    GestureDetector(
        onTap: () {
          _showPicker(context);
        },
        child: Padding(
            padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(10)),
              height: 150,
              width: MediaQuery.of(context).size.width,
              child: productItemImageBase64 != null? ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.memory(
                  base64Decode('${productItemImageBase64}'),
                  width: 100,
                  height: 100,
                  gaplessPlayback: true,
                  fit: BoxFit.fitHeight,
                ),
              ):_image != null
                  ? ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.file(
                        _image!,
                        width: 100,
                        height: 100,
                        fit: BoxFit.fitHeight,
                        gaplessPlayback: true,
                      ),
                    )
                  : Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(10)),
                      width: 100,
                      height: 100,
                      child: Icon(
                        Icons.camera_alt,
                        color: Colors.grey[800],
                      ),
                    ),
            )),
      ),
    )*/
    GestureDetector(
        onTap: () {
          _showPicker(context);

        },
        child: Container(
          decoration: BoxDecoration(
            // color: Colors.grey[200],
              borderRadius: BorderRadius.circular(75)
          ),
          height: 104,
          width: 104,
          child:productItemImageBase64 != null?
          ClipRRect(
            borderRadius: BorderRadius.circular(75),
            child: Image.memory(
              base64Decode('${productItemImageBase64}'),
              width: 100,
              height: 100,
              gaplessPlayback: true,
              fit: BoxFit.fitHeight,
            ),
          ):_image != null
              ? ClipRRect(
            borderRadius: BorderRadius.circular(75),
            child: Image.file(
              _image!,
              width: 100,
              height: 100,
              gaplessPlayback: true,
              fit: BoxFit.cover,
            ),
          )
              : Container(
            decoration: BoxDecoration(
              // color: Colors.grey[200],
                border: Border.all(color: Colors.black,width: 1),
                borderRadius: BorderRadius.circular(75)
            ),
            width: 100,
            height: 100,
            child: Icon(
              Icons.image_rounded,
              color: Colors.grey[800],
            ),
          ),
        ));

    final addInvEditTxt =/*FadeInUp(delay: Duration(milliseconds: 200),child:  Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Name',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 15,
                color: Colors.black,
              ),
            ),
            TextFormField(
                keyboardType: TextInputType.name,
                textCapitalization: TextCapitalization.words,
                controller: teInvNameController,
                obscureText: false,
                decoration: InputDecoration(
                  //contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  hintText: "Inventory Item Name",
                ))
          ],
        )))*/

    Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text: 'Item Name'),
          SizedBox(height: 10,),
          MyWidgets.textViewContainer(

            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child: MyWidgets.textFromField(

              contentPaddingL: 15.0,

              contentPaddingR: 15.0,

              cornerRadius: 8,
              keyboardType: TextInputType.name,
              controller: teInvNameController,
            ),
          ),
        ]
    );



    final inventoryType =/*FadeInUp(delay: Duration(milliseconds: 300),child:  Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Type',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 15,
                color: Colors.black,
              ),
            ),
            SizedBox(height: 15,),Text(
              '$dropdownSelectedTypeValue',
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 14,
                color: Colors.black,
              ),
            ),SizedBox(height: 15,),Divider(height: 1,color:Colors.black87 ,)
          ],
        )))*/
    Column(
        mainAxisSize: MainAxisSize.max,
        // mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text: 'Item Type',),
          SizedBox(height: 10,),
          MyWidgets.textViewContainer(
            paddingL: 15.0,paddingR: 15.0,
            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child:
            SizedBox.expand(
              child:Align(alignment:Alignment.centerLeft,child:MyWidgets.textView(text:'$dropdownSelectedTypeValue',
                  textAlign: TextAlign.left,style: MyStyles.customTextStyle(fontWeight: FontWeight.w400,
                    fontSize: 16,
                  )),
            ),)

          ),
        ]
    );


    final totalStockEditTxt =
            Row(mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child:/*Column(crossAxisAlignment: CrossAxisAlignment.start,children: [ Text(
                    widget.inventoryItem?.isPiecesInTypeUpdated==0? 'Total $dropdownSelectedTypeValue':'Total Stock',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 15,
                      color: Colors.black,
                    ),
                  ),SizedBox(height: 15,),Text(
                    widget.inventoryItem?.isPiecesInTypeUpdated==0?'${integerBalanceStock} ${dropdownSelectedTypeValue}\'s $decimalBalanceStock Piece\'s':'${widget.inventoryItem?.stock} Items',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 15,
                      color: Colors.black,
                    ),
                  ),SizedBox(height: 15,)],)*/
                  Column(
                      mainAxisSize: MainAxisSize.max,
                      // mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 10,),
                        MyWidgets.textView(text:
                        item?.isPiecesInTypeUpdated==0? 'Total Boxes':'Total Stock',
                        //item?.isPiecesInTypeUpdated==0? 'Total $dropdownSelectedTypeValue':'Total Stock',
                        ),
                        SizedBox(height: 10,),
                        MyWidgets.textViewContainer(
                            paddingL: 15.0,paddingR: 15.0,
                            height: 45, cornerRadius: 8, color: MyColors.white,
                            // color: Colors.white54,
                            child:

                            Align(alignment:Alignment.centerLeft,child:MyWidgets.textView(text:
                            item?.isPiecesInTypeUpdated==0?'${integerBalanceStock} Boxes ':'${item?.stock} Items',
                           // item?.isPiecesInTypeUpdated==0?'${integerBalanceStock} ${dropdownSelectedTypeValue}\'s $decimalBalanceStock Piece\'s':'${item?.stock} Items',

                                  textAlign: TextAlign.left,style: MyStyles.customTextStyle(fontWeight: FontWeight.w400,
                                    fontSize: 13,
                                  )),
                              ),

                        ),
                      ]
                  ),),
                SizedBox(width: 10,),
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child:/*Column(crossAxisAlignment: CrossAxisAlignment.start,children: [Text(
                    '$dropdownSelectedTypeValue\'s to be added',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 15,
                      color: Colors.black,
                    ),
                  ),TextFormField(
                      keyboardType: TextInputType.number,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      controller: teInvTotalTypeCountController,
                      obscureText: false,
                      decoration: InputDecoration(
                        hintText: 'Total $dropdownSelectedTypeValue',
                      )) ])*/

                  Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 10,),
                        MyWidgets.textView(text: 'Boxes to be added',),
                        SizedBox(height: 10,),
                        MyWidgets.textViewContainer(

                          height: 45, cornerRadius: 8, color: MyColors.white,
                          // color: Colors.white54,
                          child:Align(alignment:Alignment.centerLeft,child: MyWidgets.textFromField(

                            contentPaddingL: 15.0,

                            contentPaddingR: 15.0,

                            cornerRadius: 8,
                            keyboardType: TextInputType.number,
                            inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                            controller: teInvTotalTypeCountController,
                          )),
                        ),
                      ]
                  ))
            ],)
            ;
    final piecesInType =/*FadeInUp(delay: Duration(milliseconds: 500),child:  Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Pieces in the $dropdownSelectedTypeValue',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 15,
                color: Colors.black,
              ),
            ),
            TextFormField(
                keyboardType: TextInputType.number,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                controller: teInvPiecesInTypeController,
                obscureText: false,
                decoration: InputDecoration(
                  //contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  hintText: "Pieces in the $dropdownSelectedTypeValue",
                ))
          ],
        )))*/
    Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text: '$dropdownSelectedTypeValue in a box',),
          SizedBox(height: 10,),
          MyWidgets.textViewContainer(

            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child:Align(alignment:Alignment.centerLeft,child: MyWidgets.textFromField(

              contentPaddingL: 15.0,

              contentPaddingR: 15.0,

              cornerRadius: 8,
              keyboardType: TextInputType.number,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              controller: teInvPiecesInTypeController,
            )),
          ),
        ]
    );
    final totalCostPerType = /*FadeInUp(delay: Duration(milliseconds: 600),child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              selectedTypeIndex==0 ?"Cost per $dropdownSelectedTypeValue": "Total $dropdownSelectedTypeValue Cost",
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 15,
                color: Colors.black,
              ),
            ),
            TextFormField(
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}')),
                ],
                controller: teInvTotalCostPerTypeController,
                obscureText: false,
                decoration: InputDecoration(
                  // contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  hintText:selectedTypeIndex==0 ?"Cost per $dropdownSelectedTypeValue": "Total $dropdownSelectedTypeValue Cost",
                ))
          ],
        )))*/Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text:
          //selectedTypeIndex==0 ?"Cost per $dropdownSelectedTypeValue": "Total $dropdownSelectedTypeValue Cost",
         "Total Cost",
          ),
          SizedBox(height: 10,),
          Align(alignment:Alignment.centerLeft,child:  MyWidgets.textViewContainer(

            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child: MyWidgets.textFromField(
              contentPaddingL: 15.0,

              contentPaddingR: 15.0,
              cornerRadius: 8,
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}')),
              ],
              controller: teInvTotalCostPerTypeController,
            ),
          )),
        ]
    );
    final addInvButton = /*FadeInUp(delay: Duration(milliseconds: 700),child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(10.0),
          color: (Colors.teal[900])!,
          child: MaterialButton(
            minWidth: MediaQuery.of(context).size.width,
            onPressed: () {
              _validate() ?
              //updateInventoryItem()
              updateInventoryItemWithTransactionAndAddHistory() : {};
            },
            child: Text(
              "Update Inventory Item",
              textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white)
            ),
          ),
        )))*/
    Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(10.0),
      color: (Colors.black),
      child: MaterialButton(
        minWidth: MediaQuery
            .of(context)
            .size
            .width,
        onPressed: () {
          _validate() ?
          //updateInventoryItem()
          updateInventoryItemWithTransactionAndAddHistory() : {};
        },
        child: MyWidgets.textView(text:
        "Update Inventory Item",
          style: MyStyles.customTextStyle(
              color: Colors.white),
        ),
      ),
    );

    return Scaffold(
      /*appBar: AppBar(
        title: Text(
          'Update Inventory Item',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),*/
      body:item!=null?SingleChildScrollView(
          padding: EdgeInsets.only(left: 30, right: 30, top: 25, bottom: 20),


              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  updateInventoryItemText,
                  SizedBox(
                    height: 25,
                  ),
                  imageView,
                  SizedBox(
                    height: 20,
                  ),


                  addInvEditTxt,

                  inventoryType,

                  totalStockEditTxt,

                  piecesInType,

                  totalCostPerType,
                  SizedBox(
                    height: 30,
                  ),
                  addInvButton,
                  SizedBox(
                    height: 15,
                  )
                ],
              )):isLoading?MyWidgets.buildCircularProgressIndicator():FadeIn(child:   Center(child: MyWidgets.textView(text:'No Details.',
        style: MyStyles.customTextStyle(fontSize: 16,fontWeight: FontWeight.w500),
      ),)
      ),
    );
  }

  void _showPicker(context) {
    /*showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  ListTile(
                    leading: Icon(Icons.photo_camera),
                    title: Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });*/
    MyImagePickerBottomSheet.showPicker(context, fileCallback: (image) {
      productItemImageBase64 = null;
      setState(() {
        _image = image!=null?File(image.path):null;
      });
      return null;
    });
  }

 /*  _imgFromCamera() async {
    var image = await picker.pickImage(
        source: ImageSource.camera,
        imageQuality: 0,
        maxHeight: 150,
        maxWidth: 150);
    productItemImageBase64 = null;
    setState(() {
      _image = image!=null?File(image.path):null;
    });
  }

  _imgFromGallery() async {
    var image = await picker.pickImage(
        source: ImageSource.gallery,
        imageQuality: 0,
        maxHeight: 150,
        maxWidth: 150);
    productItemImageBase64 = null;
    setState(() {
      _image = image!=null?File(image.path):null;
    });
  }*/

  bool _validate() {
    if (productItemImageBase64 == null && _image == null) {
      context.showSnackBar("Select inventory image");
      return false;
    } else if (teInvNameController.text.isEmpty) {
      context.showSnackBar("Enter inventory name");
      return false;
    } else if (teInvTotalTypeCountController.text.trim().isEmpty) {
      context.showSnackBar("Enter $dropdownSelectedTypeValue to be added");
      return false;
    } else if (teInvPiecesInTypeController.text.isEmpty||int.tryParse(teInvPiecesInTypeController.text.trim())==0) {
      context.showSnackBar("Enter Pieces in $dropdownSelectedTypeValue");
      return false;
    } else if (teInvTotalCostPerTypeController.text.isEmpty||int.tryParse(teInvTotalCostPerTypeController.text.trim())==0) {
      context.showSnackBar("Enter cost per $dropdownSelectedTypeValue");
      return false;
    }else if(double.tryParse(teInvTotalCostPerTypeController.text.trim())!=(item?.costPerTypeNew)!){
      if (int.tryParse(teInvTotalTypeCountController.text.trim())==0) {
        context.showSnackBar("Unable to update price with current $dropdownSelectedTypeValue");
        return false;
      }else
        return true;

    }else if(int.tryParse(teInvPiecesInTypeController.text.trim())!=item?.piecesInType){
      if (int.tryParse(teInvTotalTypeCountController.text.trim())==0) {
        context.showSnackBar("Unable to update pieces in $dropdownSelectedTypeValue without new $dropdownSelectedTypeValue\n's");
        return false;
      }else
        return true;

    }
    return true;
  }

/*  String getImageBase64() {
    if (_image != null) {
      var bytes = _image?.readAsBytesSync();
      var base64Image = base64Encode(bytes!);

      return base64Image.toString();
    }
    return "";
  }*/

  updateInventoryItem() async{
    var inventoryItem = InventoryItem();
    var inventoryItemId=item?.id;
    var costPerTypeControllerText=double.tryParse(teInvTotalCostPerTypeController.text.trim());
    inventoryItem.id=inventoryItemId;
    inventoryItem.projectId=item?.projectId;
    inventoryItem.name = teInvNameController.text.trim().toString();
    inventoryItem.imageBase =productItemImageBase64 != null
        ? productItemImageBase64
        : getImageBase64(_image);
    inventoryItem.itemType = selectedTypeIndex;
    inventoryItem.totalType =
        int.tryParse(teInvTotalTypeCountController.text.trim());
    inventoryItem.piecesInType =
        int.tryParse(teInvPiecesInTypeController.text.trim());
    //inventoryItem.costPerTypeOld=costPerTypeControllerText!=widget.inventoryItem?.costPerTypeNew?widget.inventoryItem?.costPerTypeNew:widget.inventoryItem?.costPerTypeOld;
    //inventoryItem.costPerTypeNew = costPerTypeControllerText;

    if(costPerTypeControllerText!>(item?.costPerTypeNew)!){
      inventoryItem.costPerTypeOld=item?.costPerTypeNew;
      inventoryItem.costPerTypeNew=costPerTypeControllerText;


    }else if(costPerTypeControllerText<(item?.costPerTypeNew)!){
      inventoryItem.costPerTypeOld=costPerTypeControllerText;
      inventoryItem.costPerTypeNew=item?.costPerTypeNew;


    }else if(costPerTypeControllerText==(item?.costPerTypeNew)!){
      inventoryItem.costPerTypeOld=item?.costPerTypeOld;
      inventoryItem.costPerTypeNew=item?.costPerTypeNew;

    }
    double? currentCostPerItemInType=0.00;
    currentCostPerItemInType=double.tryParse((double.tryParse(teInvTotalCostPerTypeController.text.trim())!/int.parse(teInvPiecesInTypeController.text.trim())).toStringAsFixed(2));
    if(currentCostPerItemInType!>(item?.costPerItemInTypeNew)!){
      inventoryItem.costPerItemInTypeOld=item?.costPerItemInTypeNew;
      inventoryItem.costPerItemInTypeNew=currentCostPerItemInType;
    }else if(currentCostPerItemInType<(item?.costPerItemInTypeNew)!){
      inventoryItem.costPerItemInTypeOld= currentCostPerItemInType;
      inventoryItem.costPerItemInTypeNew=item?.costPerItemInTypeNew;
    }else  if(currentCostPerItemInType==(item?.costPerItemInTypeNew)!){
      inventoryItem.costPerItemInTypeOld= item?.costPerItemInTypeOld;
      inventoryItem.costPerItemInTypeNew=item?.costPerItemInTypeNew;
    }

    inventoryItem.isCostUpdated=costPerTypeControllerText>(item?.costPerTypeNew)! ?1:0;
    inventoryItem.isPiecesInTypeUpdated=int.tryParse(teInvPiecesInTypeController.text.trim())!=(item?.piecesInType)! ?1:0;
    inventoryItem.stock =(item?.stock)!+ (int.tryParse(teInvTotalTypeCountController.text.trim())! *
        int.tryParse(teInvPiecesInTypeController.text.trim())!);
    inventoryItem.inventoryId =widget.inventory==null?null: widget.inventory?.id;

   await inventoryItemOperations.updateInventoryItem(inventoryItem).then((value)async {





     inventoryItem.isCostUpdated==1||(currentCostPerItemInType!>(item?.costPerItemInTypeNew)!)
         ?

      await inventoryRelationOperations.relationItemFromProductRelationByInventoryItemId(inventoryItemId).then((productIds)async{



        if(productIds.isNotEmpty)
       await productItemOperations.getProductItemIdAndRecalculateCostByProductItemId(productIds).then((value)async {
         await addInventoryHistory(inventoryItem);
       });
        else
          await  addInventoryHistory(inventoryItem);

      })

         : await addInventoryHistory(inventoryItem);



    });
  }

  updateInventoryItemWithTransactionAndAddHistory() async{

    var inventoryItem = InventoryItem();
    var inventoryItemId=item?.id;
    var costPerTypeControllerText=double.tryParse(teInvTotalCostPerTypeController.text.trim());
    inventoryItem.id=inventoryItemId;
    inventoryItem.projectId=item?.projectId;
    inventoryItem.name = teInvNameController.text.trim().toString();
    inventoryItem.imageBase =productItemImageBase64 != null
        ? productItemImageBase64
        : getImageBase64(_image);
    inventoryItem.itemType = selectedTypeIndex;
    inventoryItem.totalType = int.tryParse(teInvTotalTypeCountController.text.trim());
    //inventoryItem.piecesInType = int.tryParse(teInvPiecesInTypeController.text.trim());


    if(costPerTypeControllerText!>(item?.costPerTypeNew)!){
      inventoryItem.costPerTypeOld=item?.costPerTypeNew;
      inventoryItem.costPerTypeNew=costPerTypeControllerText;


    }else if(costPerTypeControllerText<(item?.costPerTypeNew)!){
      inventoryItem.costPerTypeOld=costPerTypeControllerText;
      inventoryItem.costPerTypeNew=item?.costPerTypeNew;


    }else if(costPerTypeControllerText==(item?.costPerTypeNew)!){
      inventoryItem.costPerTypeOld=item?.costPerTypeOld;
      inventoryItem.costPerTypeNew=item?.costPerTypeNew;

    }
    double? currentCostPerItemInType=0.00;
    //currentCostPerItemInType=double.tryParse((double.tryParse(teInvCostPerTypeController.text.trim())!/int.parse(teInvPiecesInTypeController.text.trim())).toStringAsFixed(2));

    if(selectedTypeIndex==0){
    selectedTypeIndex==0? currentCostPerItemInType=double.tryParse((double.tryParse(teInvTotalCostPerTypeController.text.trim())!/int.parse(teInvPiecesInTypeController.text.trim())).toStringAsFixed(2))
   : currentCostPerItemInType=double.tryParse(((double.tryParse(teInvTotalCostPerTypeController.text.trim())!/integerBalanceStock!)/int.parse(teInvPiecesInTypeController.text.trim())).toStringAsFixed(2));

    inventoryItem.piecesInType = int.tryParse(teInvPiecesInTypeController.text.trim());
      inventoryItem.stock =(item?.stock)!+ (int.tryParse(teInvTotalTypeCountController.text.trim())! * int.tryParse(teInvPiecesInTypeController.text.trim())!);
      inventoryItem.isPiecesInTypeUpdated=int.tryParse(teInvPiecesInTypeController.text.trim())!=(item?.piecesInType)! ?1:0;

    }else{
      if(int.parse(teInvTotalTypeCountController.text.trim())==0)
        currentCostPerItemInType=item?.costPerItemInTypeNew!;

      else
         currentCostPerItemInType=double.tryParse((double.tryParse(teInvTotalCostPerTypeController.text.trim())!/(int.parse(teInvTotalTypeCountController.text.trim())*int.parse(teInvPiecesInTypeController.text.trim()))).toStringAsFixed(2));
      inventoryItem.piecesInType =int.parse(teInvPiecesInTypeController.text.trim());
     // inventoryItem.stock =(item?.stock)!+ (int.tryParse(teInvTotalTypeCountController.text.trim())!);
      inventoryItem.stock =(item?.stock)!+ (int.tryParse(teInvTotalTypeCountController.text.trim())! * int.tryParse(teInvPiecesInTypeController.text.trim())!);

      inventoryItem.isPiecesInTypeUpdated=0;

    }



    if(currentCostPerItemInType!>(item?.costPerItemInTypeNew)!){
      inventoryItem.costPerItemInTypeOld=item?.costPerItemInTypeNew;
      inventoryItem.costPerItemInTypeNew=currentCostPerItemInType;
    }else if(currentCostPerItemInType<(item?.costPerItemInTypeNew)!){
      inventoryItem.costPerItemInTypeOld= currentCostPerItemInType;
      inventoryItem.costPerItemInTypeNew=item?.costPerItemInTypeNew;
    }else  if(currentCostPerItemInType==(item?.costPerItemInTypeNew)!){
      inventoryItem.costPerItemInTypeOld= item?.costPerItemInTypeOld;
      inventoryItem.costPerItemInTypeNew=item?.costPerItemInTypeNew;
    }

    inventoryItem.isCostUpdated=costPerTypeControllerText>(item?.costPerTypeNew)! ?1:0;
   // inventoryItem.isPiecesInTypeUpdated=int.tryParse(teInvPiecesInTypeController.text.trim())!=(widget.inventoryItem?.piecesInType)! ?1:0;
    //inventoryItem.stock =(widget.inventoryItem?.stock)!+ (int.tryParse(teInvTotalTypeCountController.text.trim())! * int.tryParse(teInvPiecesInTypeController.text.trim())!);
    //inventoryItem.inventoryId = widget.inventory==null?null: widget.inventory?.id;
    if(widget.inventory==null){
      inventoryItem.inventoryId=null;
      inventoryItem.hasCategory=0;
    }else{
      inventoryItem.inventoryId= widget.inventory?.id;
      inventoryItem.hasCategory=1;
    }
      var history = History();
      history.inventoryItemId=inventoryItem.id;
      history.historyItemType=inventoryItem.itemType;
      history.historyTotalType=inventoryItem.totalType;
      history.historyPiecesInType=inventoryItem.piecesInType;
      history.historyCostPerType=costPerTypeControllerText;
      history.historyCostPerItemInType=currentCostPerItemInType;
      history.historyStock=inventoryItem.stock;

    inventoryItem.isCostUpdated==1||(currentCostPerItemInType!=(item?.costPerItemInTypeNew)!)||int.parse(teInvTotalTypeCountController.text.trim())>0
        ?

    await inventoryItemOperations.updateInventoryItemWithTransactionAndAddHistory(inventoryItem,history).then((value) {clearControllerAndPop();print('onTransactionUpdateIntSuccess ');}).onError((error, stackTrace) {print('onTransactionUpdateInvError  ${error}  ${stackTrace}');context.showSnackBar('$error');})
        :await inventoryItemOperations.updateInventoryItemNameOrImage(inventoryItem).then((value)async {
      print('onUpdateIntSuccess ');
          clearControllerAndPop();
        }).onError((error, stackTrace) {
      context.showSnackBar('$error');
    });

   return;
  }


  var historyItemOperations = HistoryOperations();
  addInventoryHistory(InventoryItem inventoryItem)async{
    var costPerTypeControllerText=double.tryParse(teInvTotalCostPerTypeController.text.trim());
    double? currentCostPerItemInType=0.00;
    currentCostPerItemInType=double.tryParse((double.tryParse(teInvTotalCostPerTypeController.text.trim())!/int.parse(teInvPiecesInTypeController.text.trim())).toStringAsFixed(2));
    if(inventoryItem.isCostUpdated==1||(int.tryParse(teInvTotalTypeCountController.text.trim()))!>0){
    var history = History();
    history.inventoryItemId=inventoryItem.id;
    history.historyItemType=inventoryItem.itemType;
    history.historyTotalType=inventoryItem.totalType;
    history.historyPiecesInType=inventoryItem.piecesInType;
    history.historyCostPerType=/*inventoryItem.costPerTypeNew*/costPerTypeControllerText;
    history.historyCostPerItemInType=/*inventoryItem.costPerItemInTypeNew*/currentCostPerItemInType;
    history.historyStock=inventoryItem.stock;
    await historyItemOperations.insertHistory(history).then((value) async{


      showtoast("Successfully Added History Data ");
      clearControllerAndPop();
    });
    }else
      clearControllerAndPop();
  }
   clearControllerAndPop()async{
     teInvNameController.text = "";
     teInvTotalTypeCountController.text = "";
     teInvPiecesInTypeController.text = "";
     teInvTotalCostPerTypeController.text = "";
     Navigator.pop(context, true);

     }
}
