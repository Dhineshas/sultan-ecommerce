import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/Inventory.dart';
import 'package:sqflite/sqflite.dart';

class InventoryOperations{
  late InventoryOperations inventoryOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<void> insertInventory(Inventory inventory) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'inventory_category',
      inventory.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  // A method that retrieves all the dogs from the dogs table.
  Future<List<Inventory>> inventories() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('inventory_category');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var inventory=Inventory();
      inventory.id= maps[i]['inventory_id'];
      inventory.name= maps[i]['inventory_name'];
      inventory.imageBase= maps[i]['inventory_image'];

      return inventory;
    });
  }


  Future<void> updateInventory(Inventory inventory) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Update the given Dog.
    await db.update(
      'inventory_category',
      inventory.toMap(),
      // Ensure that the Dog has a matching id.
      where: 'inventory_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [inventory.id],
    );
  }
  Future<void> updateRawInventory(Inventory inventory) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    await db.rawUpdate('UPDATE inventory_category SET inventory_name = ? WHERE inventory_id = ?',
        [inventory.name, inventory.id]);
  }
  Future<void> deleteInventory(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'inventory_category',
      // Use a `where` clause to delete a specific dog.
      where: 'inventory_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
  Future<void> deleteAllDataAndInventory(int? inventoryId) async {
    final db = await dbProvider.database;
    try{
      await db.transaction((txn) async{

        var batch =  txn.batch();
         batch.rawDelete('DELETE FROM history where inventory_item_id IN (SELECT inventory_item_id from inventory_item WHERE inventory_id =?)',[inventoryId]);
        batch.rawDelete('DELETE FROM inventory_item where inventory_id = ?', [inventoryId]);
        batch.rawDelete('DELETE FROM inventory_category where inventory_id = ?', [inventoryId]);


        await batch.commit(noResult: true);
      });}catch (e) {
      throw ("some arbitrary error");
    }
  }
  Future<void> deleteInventoryByProjectId(int? projectID) async {
    // Get a reference to the database.
    final db =  await dbProvider.database;

    await db.execute(
        'DELETE FROM inventory_category  WHERE project_id = ?',[projectID]
    );
  }
  Future<List<Inventory>> inventoriesByProjectIdWithInventoryItemCount(int? projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;
    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT ic.inventory_id, ic.inventory_name, ic.inventory_image, ic.project_id, count(ii.inventory_item_id) AS inventory_item_count FROM inventory_category ic LEFT OUTER JOIN inventory_item ii ON (ii.inventory_id = ic.inventory_id) WHERE ic.project_id= ? GROUP BY ic.inventory_id, ic.inventory_name, ic.inventory_image, ic.project_id',[projectID]);

    return List.generate(maps.length, (i) {
      var inventory=Inventory();
      inventory.id= maps[i]['inventory_id'];
      inventory.projectId= maps[i]['project_id'];
      inventory.name= maps[i]['inventory_name'];
      inventory.imageBase= maps[i]['inventory_image'];
      inventory.inventoryItemCount= maps[i]['inventory_item_count'];

      return inventory;
    });
  }

  Future<List<Inventory>> inventoriesByProjectIdWithoutSelectedCategory(int? projectID,List<int> inventoryCategoryIdArray) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;
    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT ic.inventory_id, ic.inventory_name, ic.inventory_image, ic.project_id FROM inventory_category ic WHERE ic.project_id= ? AND ic.inventory_id NOT IN (${inventoryCategoryIdArray.join(', ')})  GROUP BY ic.inventory_id, ic.inventory_name, ic.inventory_image, ic.project_id',[projectID]);

    return List.generate(maps.length, (i) {
      var inventory=Inventory();
      inventory.id= maps[i]['inventory_id'];
      inventory.projectId= maps[i]['project_id'];
      inventory.name= maps[i]['inventory_name'];
      inventory.imageBase= maps[i]['inventory_image'];
      inventory.inventoryItemCount= maps[i]['inventory_item_count'];

      return inventory;
    });
  }

  Future<List<Inventory>> inventoriesByProjectIdWithNullInventoryItemIdAndInventoryItemCount(int? projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

     List<Map<String, dynamic>> maps=[];
    /*final List<Map<String, dynamic>> maps1 = await db.rawQuery('''
    
     SELECT count(*) AS inventory_item_count FROM inventory_category ic  WHERE ic.project_id=$projectID
    ''');*/

  /*  final itemCountQuery =await db.rawQuery('''
     SELECT count(*) AS inventory_count FROM inventory_category ic  WHERE ic.project_id=$projectID
    ''');
    int? itemCount=int.tryParse(itemCountQuery.first['inventory_count'].toString())*//*maps1.first['inventory_item_count']*//*;
   if(itemCount!=null&&itemCount>0){
     maps = await db.rawQuery('''
SELECT ic.inventory_id AS id,      ic.project_id AS project_id,      ic.inventory_name AS name, ic.inventory_image AS image,      -1 AS has_category,                                          0.0 AS inventory_item_cost_new,                     -1 AS inventory_item_type, count(ii.inventory_item_id) AS inventory_item_count FROM inventory_category ic LEFT JOIN inventory_item ii ON (ii.inventory_id = ic.inventory_id) WHERE ic.project_id= ? GROUP BY ic.inventory_id, ic.inventory_name,  ic.project_id
UNION ALL
SELECT ii.inventory_item_id AS id, ii.project_id AS project_id, ii.inventory_item_name AS name, ii.inventory_item_image AS image,  ii.inventory_item_has_category AS has_category, ii.inventory_item_cost_per_item_in_type_new AS inventory_item_cost_new,  ii.inventory_item_type AS inventory_item_type,    ii.inventory_item_stock AS inventory_item_count  FROM inventory_item ii WHERE ii.inventory_id IS NULL AND ii.project_id = ?
    ''',[projectID,projectID]);
   }else{
 maps = await db.rawQuery('''
    SELECT ii.inventory_item_id AS id, ii.project_id AS project_id, ii.inventory_item_name AS name,ii.inventory_item_image AS image,ii.inventory_item_stock AS inventory_item_count,ii.inventory_item_has_category AS has_category,  ii.inventory_item_cost_per_item_in_type_new AS inventory_item_cost_new, ii.inventory_item_type AS inventory_item_type FROM inventory_item ii WHERE ii.inventory_id IS NULL AND ii.project_id = $projectID

''');
   }*/


    final itemCountQuery =await db.rawQuery('''
     SELECT count(*) AS inventory_count FROM inventory_category ic  WHERE ic.project_id=$projectID
    ''');
    int? itemCount=int.tryParse(itemCountQuery.first['inventory_count'].toString())/*maps1.first['inventory_item_count']*/;
    if(itemCount!=null&&itemCount>0){
      maps = await db.rawQuery('''
SELECT ic.inventory_id AS id,      ic.project_id AS project_id,      ic.inventory_name AS name, ic.inventory_image AS image,      -1 AS has_category,                                          0.0 AS inventory_item_cost_new,                     -1 AS inventory_item_type, count(ii.inventory_item_id) AS inventory_item_count, 0.0 AS inventory_item_stock                       FROM inventory_category ic LEFT JOIN inventory_item ii ON (ii.inventory_id = ic.inventory_id) WHERE ic.project_id= ? GROUP BY ic.inventory_id, ic.inventory_name,  ic.project_id
UNION ALL
SELECT ii.inventory_item_id AS id, ii.project_id AS project_id, ii.inventory_item_name AS name, ii.inventory_item_image AS image,  ii.inventory_item_has_category AS has_category, ii.inventory_item_cost_per_item_in_type_new AS inventory_item_cost_new,  ii.inventory_item_type AS inventory_item_type,    0 AS inventory_item_count, ii.inventory_item_stock AS inventory_item_stock   FROM inventory_item ii WHERE ii.inventory_id IS NULL AND ii.project_id = ?
    ''',[projectID,projectID]);
    }else{
      maps = await db.rawQuery('''
    SELECT ii.inventory_item_id AS id, ii.project_id AS project_id, ii.inventory_item_name AS name,ii.inventory_item_image AS image,ii.inventory_item_stock AS inventory_item_count,ii.inventory_item_has_category AS has_category,  ii.inventory_item_cost_per_item_in_type_new AS inventory_item_cost_new, ii.inventory_item_type AS inventory_item_type, 0 AS inventory_item_count, ii.inventory_item_stock AS inventory_item_stock FROM inventory_item ii WHERE ii.inventory_id IS NULL AND ii.project_id = $projectID

''');
    }

   return List.generate(maps.length, (i) {
      var inventory=Inventory();
      inventory.id= maps[i]['id'];
      inventory.projectId= maps[i]['project_id'];
      inventory.name= maps[i]['name'].toString();
      inventory.imageBase= maps[i]['image'];
      inventory.inventoryItemCount= maps[i]['inventory_item_count'];
      inventory.inventoryItemStock= maps[i]['inventory_item_stock'];
      inventory.costPerItemInTypeNew= maps[i]['inventory_item_cost_new'];
      inventory.hasCategory= maps[i]['has_category'];
      inventory.inventoryItemType= maps[i]['inventory_item_type'];


      return inventory;
    });
  }


/*
  Future<List<Inventory>> inventoriesByProjectIdWithNullInventoryIdItemAndInventoryItemCount(int? projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    List<Map<String, dynamic>> maps=[];
    final List<Map<String, dynamic>> maps1 = await db.rawQuery('''
    
     SELECT count(*) AS inventory_item_count FROM inventory_category ic  WHERE ic.project_id = ?
    ''',[projectID]);
    int? itemCount=maps1.first['inventory_item_count'];
    if(itemCount!=null&&itemCount>0){
      maps = await db.rawQuery('''

     SELECT ic.inventory_id AS id, ic.project_id AS project_id, ic.inventory_name AS name,ic.inventory_image AS image, count(ii.inventory_item_id) AS inventory_item_count,0.0 AS inventory_item_stock, -1 AS has_category FROM inventory_category ic LEFT JOIN inventory_item ii ON (ii.inventory_id = ic.inventory_id) WHERE ic.project_id = ?
    UNION ALL
    SELECT ii.inventory_item_id AS id, ii.project_id AS project_id, ii.inventory_item_name AS name,ii.inventory_item_image AS image,0 AS inventory_item_count, ii.inventory_item_stock AS inventory_item_stock,ii.inventory_item_has_category AS has_category FROM inventory_item ii WHERE ii.inventory_id IS NULL AND ii.project_id = ?
    ''',[projectID,projectID]);
    }else{
      maps = await db.rawQuery('''
    SELECT ii.inventory_item_id AS id, ii.project_id AS project_id, ii.inventory_item_name AS name,ii.inventory_item_image AS image,ii.inventory_item_stock AS inventory_item_stock,ii.inventory_item_has_category AS has_category FROM inventory_item ii WHERE ii.inventory_id IS NULL AND ii.project_id = ?
''',[projectID]);
    }



    */
/*final List<Map<String, dynamic>> maps3=[];
    maps3.clear();
    maps3.addAll(maps1);
    maps3.addAll(maps2);*//*

    print('isnullll ${maps}');
    return */
/*maps3.first['id']==null
        ?<Inventory>[]//empty list
        : *//*
List.generate(maps.length, (i) {
      var inventory=Inventory();
      inventory.id= maps[i]['id'];
      inventory.projectId= maps[i]['project_id'];
      inventory.projectId= maps[i]['project_id'];
      inventory.name= maps[i]['name'];
      inventory.imageBase= maps[i]['image'];
      inventory.inventoryItemCount= maps[i]['inventory_item_count'];
      inventory.inventoryItemStock= maps[i]['inventory_item_stock'];
      inventory.hasCategory= maps[i]['has_category'];

      return inventory;
    });
  }
*/
}