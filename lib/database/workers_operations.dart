import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/Worker.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:sqflite/sqflite.dart';

class WorkersOperations{
  late WorkersOperations workersOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<void> insertWorker(Worker worker) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'workers',
      worker.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  // A method that retrieves all the dogs from the dogs table.
  Future<List<Worker>> workers() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('workers');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var worker=Worker();
      worker.id= maps[i]['worker_id'];
      worker.projectId= maps[i]['project_id'];
      worker.name= maps[i]['worker_name'];
      worker.salary= maps[i]['worker_salary'];
      worker.hoursPerDay= maps[i]['worker_hours'];
      worker.wagePerMinOld= maps[i]['worker_wage_per_min_old'];
      worker.wagePerMinNew= maps[i]['worker_wage_per_min_new'];

      return worker;
    });
  }
  Future<List<Worker>> workersNyProjectId(int? projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM workers WHERE project_id = ?',[projectID]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var worker=Worker();
      worker.id= maps[i]['worker_id'];
      worker.projectId= maps[i]['project_id'];
      worker.name= maps[i]['worker_name'];
      worker.salary= maps[i]['worker_salary'];
      worker.hoursPerDay= maps[i]['worker_hours'];
      worker.wagePerMinOld= maps[i]['worker_wage_per_min_old'];
      worker.wagePerMinNew= maps[i]['worker_wage_per_min_new'];

      return worker;
    });
  }
  Future<List<Worker>> workersByProjectId(int? projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM workers WHERE project_id = ?',[projectID]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var worker=Worker();
      worker.id= maps[i]['worker_id'];
      worker.projectId= maps[i]['project_id'];
      worker.name= maps[i]['worker_name'].toString();
      worker.salary= maps[i]['worker_salary'];
      worker.hoursPerDay= maps[i]['worker_hours'];
      worker.wagePerMinOld= maps[i]['worker_wage_per_min_old'];
      worker.wagePerMinNew= maps[i]['worker_wage_per_min_new'];

      return worker;
    });
  }


  Future<void> updateWorker(Worker worker) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;
try{
    // Update the given Dog.
    await db.update(
      'workers',
      worker.toMap(),
      // Ensure that the Dog has a matching id.
      where: 'worker_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [worker.id],
    );
  }catch (e) {

  throw ("some arbitrary error");
  }
  }

  Future<void> updateWorkerWithTransaction(Worker worker) async {
    final db = await dbProvider.database;
    try{
      await db.transaction((txn) async{

        var batch =  txn.batch();

        txn.update(
          'workers',
          worker.toMap(),
          // Ensure that the Dog has a matching id.
          where: 'worker_id = ?',
          // Pass the Dog's id as a whereArg to prevent SQL injection.
          whereArgs: [worker.id],
        );

        List<Map<String, dynamic>> productItemIds = await txn.rawQuery(
            'SELECT DISTINCT *  FROM relation_product_workers WHERE worker_id = ?'
            ,[worker.id]);


        final productItemIdArray = productItemIds.map((e) => e['product_item_id']).toList();
        print('associatedProductID  $productItemIds       $productItemIdArray');


        final List<Map<String, dynamic>> toUpdateProductIdArray =  await txn.rawQuery('''
    SELECT product_item_id,product_item_price_by_percentage,SUM(printf("%.2f",cost)) as totalProductCost FROM 
    (
    SELECT 
      p.product_item_id as product_item_id,product_item_price_by_percentage,
      (w.worker_wage_per_min_new)*(pwr.work_duration*1.0) as cost
      FROM
      product_item p
      LEFT JOIN relation_product_workers pwr ON pwr.product_item_id=p.product_item_id
      LEFT JOIN workers w
      ON w.worker_id=pwr.worker_id
      WHERE p.product_item_id IN (${productItemIdArray.join(', ')})
      GROUP BY p.product_item_id
      UNION ALL
    SELECT 
      p.product_item_id,product_item_price_by_percentage,
      SUM(
          pir.inventory_item_quantity * (inventory_item_cost_per_item_in_type_new)
      ) AS cost
      FROM
      product_item p
      INNER JOIN relation_product_inventory pir ON p.product_item_id = pir.product_item_id
      INNER JOIN inventory_item inv ON inv.inventory_item_id = pir.inventory_item_id
      WHERE
      p.product_item_id IN (${productItemIdArray.join(', ')})
      GROUP BY p.product_item_id
   ) GROUP BY product_item_id
    
    ''');


        toUpdateProductIdArray.forEach((e)async {
          if((e['product_item_price_by_percentage'])>0){
            //custom round off and update product price
            final double value=(e['totalProductCost']+((e['totalProductCost']*e['product_item_price_by_percentage'])/100))!;
            final double roundOffPrice= value.toString().toCustomDoubleRoundOff;
            batch.rawUpdate('UPDATE product_item SET product_item_cost = ?,product_item_selling_price= ? WHERE product_item_id = ?',[e['totalProductCost'],roundOffPrice,e['product_item_id']]);
          }else
            batch.rawUpdate('UPDATE product_item SET product_item_cost = ?,product_item_is_inventory_item_updated = 1 WHERE product_item_id = ?',[e['totalProductCost'],e['product_item_id']]);

        });

        await batch.commit(noResult: true);
      });}catch (e) {

      throw ("some arbitrary error");
    }
  }


  Future<void> deleteWorker(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'workers',
      // Use a `where` clause to delete a specific dog.
      where: 'worker_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }

  Future<void> deleteWorkerByProjectId(int projectID) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.execute('DELETE FROM workers WHERE project_id = ?',[projectID]);

    }

  Future<List<Worker>> workersByProductItem(int? productItemId) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    //final List<Map<String, dynamic>> maps = await db.query('workers');
    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM relation_product_workers rw INNER JOIN workers wr ON (rw.product_item_id = $productItemId AND wr.worker_id == rw.worker_id)');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var worker=Worker();
      worker.id= maps[i]['worker_id'];
      worker.projectId= maps[i]['project_id'];
      worker.name= maps[i]['worker_name'];
      worker.salary= maps[i]['worker_salary'];
      worker.hoursPerDay= maps[i]['worker_hours'];
      worker.selectedMins= maps[i]['work_duration'];
      worker.wagePerMinOld= maps[i]['worker_wage_per_min_old'];
      worker.wagePerMinNew= maps[i]['worker_wage_per_min_new'];

      return worker;
    });
  }


  Future<List<int>>relationItemFromWorkerRelationByWorkerId(int? workerId) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.rawQuery(
        'SELECT DISTINCT *  FROM relation_product_workers WHERE worker_id = ?'
        ,[workerId]);
    return List.generate(maps.length, (i) {
      return maps[i]['product_item_id'];
    });

  }
}