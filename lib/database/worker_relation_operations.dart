import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/WorkerRelation.dart';
import 'package:sqflite/sqflite.dart';

class WorkerRelationOperations{
  late WorkerRelationOperations workerRelationOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<void> insertWorkerRelation(WorkerRelation relation) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'relation_product_workers',
      relation.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  // A method that retrieves all the dogs from the dogs table.
  Future<List<WorkerRelation>> workerRelationsItems() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('relation_product_workers');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var relation=WorkerRelation();
      relation.id= maps[i]['relation_id'];
      relation.productItemId= maps[i]['product_item_id'];
      relation.workerId= maps[i]['worker_id'];
      relation.workerHour= maps[i]['work_duration'];
      return relation;
    });
  }

  Future<List<WorkerRelation>> workersByProductId(WorkerRelation relation) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.query('relation_product_workers',// Use a `where` clause to delete a specific dog.
      where: 'product_item_id = ?',

      whereArgs: [relation.productItemId],);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var relation=WorkerRelation();
      relation.id= maps[i]['relation_id'];
      relation.productItemId= maps[i]['product_item_id'];
      relation.workerId= maps[i]['worker_id'];
      relation.workerHour= maps[i]['work_duration'];
      return relation;
    });
  }

  Future<void> updateWorkerRelationByProductItemId(WorkerRelation relation) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Update the given Dog.
    await db.update(
      'relation_product_workers',
      relation.toMap(),
      // Ensure that the Dog has a matching id.
      where: 'product_item_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [relation.productItemId],
    );
  }

  Future<void> deleteWorkerRelation(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'relation_product_workers',
      // Use a `where` clause to delete a specific dog.
      where: 'relation_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }

  Future<void> deleteWorkerRelationByWorkerId(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'relation_product_workers',
      // Use a `where` clause to delete a specific dog.
      where: 'worker_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
  Future<void> deleteWorkerRelationByProductItemId(int? id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;
try{
    // Remove the Dog from the database.
    await db.delete(
      'relation_product_workers',
      // Use a `where` clause to delete a specific dog.
      where: 'product_item_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }catch (e) {
  throw ("some arbitrary error");
  }
  }

  Future<void> deleteWorkerRelationByProductItemIdAndWorkerId(int? productItemId,int workerId) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'relation_product_workers',
      // Use a `where` clause to delete a specific dog.
      where: 'product_item_id = ? AND worker_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [productItemId,workerId],
    );



  }

  Future<void> deleteWorkerRelationByProductId(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    await db.execute(
      'DELETE FROM relation_product_workers where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id = ?)',[id]
    );

  }
  Future<void> deleteWorkerRelationByProjectId(int projectID) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    await db.execute('DELETE FROM relation_product_workers where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id IN (SELECT product_category_id FROM product_category WHERE project_id=?))' ,[projectID]);

  }
  Future<List<int>>relationItemFromWorkerRelationByWorkerId(int? workerId) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.rawQuery(
        'SELECT DISTINCT *  FROM relation_product_workers WHERE worker_id = ?'
        ,[workerId]);
    return List.generate(maps.length, (i) {
      return maps[i]['product_item_id'];
    });

  }
  Future<int?>relationItemCountFromWorkerRelationByWorkerId(int? workerId) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    var result = await db.rawQuery(
        'SELECT COUNT(*) FROM relation_product_workers WHERE worker_id = ?'
        ,[workerId]);
    int? count = Sqflite.firstIntValue(result);
    return count;

  }
}