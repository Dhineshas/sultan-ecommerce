import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/Cart.dart';
import 'package:ecommerce/model/ProductItem.dart';
import 'package:sqflite/sqflite.dart';

class CartOperations{
  late CartOperations cartOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<List<Cart>> insertCart(Cart cart) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'cart',
      cart.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );

    final List<Map<String, dynamic>> maps =  await db.rawQuery(
        'select * from cart where cart_id = LAST_INSERT_ROWID()'
    );

    return List.generate(maps.length, (i) {
      var cart =Cart();
      cart.id= maps[i]['cart_id'];
      cart.projectId= maps[i]['project_id'];
      cart.cartDeliveryCharge= maps[i]['cart_delivery_charge'];
      cart.cartDiscount= maps[i]['cart_discount'];

      return cart;
    });
  }

  // A method that retrieves all the dogs from the dogs table.
  Future<List<Cart>> cart() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('cart');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var cart =Cart();
      cart.id= maps[i]['cart_id'];
      cart.projectId= maps[i]['project_id'];
      cart.cartDeliveryCharge= maps[i]['cart_delivery_charge'];
      cart.cartDiscount= maps[i]['cart_discount'];

      return cart;
    });
  }
  Future<List<Cart>> cartItemsByProjectId(int projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.rawQuery('select * from cart where project_id = ?',[projectID]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var cart =Cart();
      cart.id= maps[i]['cart_id'];
      cart.projectId= maps[i]['project_id'];
      cart.cartDeliveryCharge= maps[i]['cart_delivery_charge'];
      cart.cartDiscount= maps[i]['cart_discount'];

      return cart;
    });
  }

  Future<void> updateCart(Cart cart) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Update the given Dog.
    await db.update(
      'cart',
      cart.toMap(),
      // Ensure that the Dog has a matching id.
      where: 'cart_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [cart.id],
    );
  }
  Future<void> deleteCart(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'cart',
      // Use a `where` clause to delete a specific dog.
      where: 'cart_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }

  Future<void> deleteCartByProjectId(int? projectID) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.execute('DELETE FROM cart WHERE project_id = ?',[projectID]);
  }
  Future<void> clearCart() async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'cart',
    );
  }

  Future<void> updateCartItemDeliverChargeOrDiscount(Cart cart) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    await db.rawUpdate('UPDATE cart SET cart_delivery_charge = ?, cart_discount =? WHERE cart_id = ?',
        [cart.cartDeliveryCharge,cart.cartDiscount, cart.id]);
  }
}