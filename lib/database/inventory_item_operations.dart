import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/History.dart';
import 'package:ecommerce/model/Inventory.dart';
import 'package:ecommerce/model/InventoryItem.dart';
import 'package:ecommerce/model/ProductItem.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:sqflite/sqflite.dart';

import '../model/GraphInventoryItem.dart';

class InventoryItemOperations{
  late InventoryItemOperations inventoryItemOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<List<InventoryItem>> insertInventoryItem(InventoryItem inventoryItem) async {
    // Get a reference to the database.

    final db = await dbProvider.database;
try{
    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'inventory_item',
      inventoryItem.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );

    final List<Map<String, dynamic>> maps =  await db.rawQuery(
        'select * from inventory_item where inventory_item_id = LAST_INSERT_ROWID()'
    );
    return List.generate(maps.length, (i) {
      var inventoryItem=InventoryItem();
      inventoryItem.id= maps[i]['inventory_item_id'];
      inventoryItem.name= maps[i]['inventory_item_name'].toString();
      inventoryItem.imageBase= maps[i]['inventory_item_image'];
      inventoryItem.itemType= maps[i]['inventory_item_type'];
      inventoryItem.totalType= maps[i]['inventory_item_total_type'];
      inventoryItem.piecesInType= maps[i]['inventory_item_pieces_in_type'];
      inventoryItem.costPerTypeOld= maps[i]['inventory_item_cost_per_type_old'];
      inventoryItem.costPerTypeNew= maps[i]['inventory_item_cost_per_type_new'];

      inventoryItem.isPiecesInTypeUpdated= maps[i]['inventory_item_is_pieces_in_type_updated'];
      inventoryItem.costPerItemInTypeOld= maps[i]['inventory_item_cost_per_item_in_type_old'];
      inventoryItem.costPerItemInTypeNew= maps[i]['inventory_item_cost_per_item_in_type_new'];

      inventoryItem.stock= maps[i]['inventory_item_stock'];
      inventoryItem.inventoryId= maps[i]['inventory_id'];
      inventoryItem.projectId= maps[i]['project_id'];
      inventoryItem.hasCategory= maps[i]['inventory_item_has_category'];

      return inventoryItem;
    });
  }catch (e) {
  throw ("some arbitrary error  $e");
  }
  }

  // A method that retrieves all the dogs from the dogs table.
  Future<List<InventoryItem>> inventoriesItems() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('inventory_item');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var inventoryItem=InventoryItem();
      inventoryItem.id= maps[i]['inventory_item_id'];
      inventoryItem.name= maps[i]['inventory_item_name'];
      inventoryItem.imageBase= maps[i]['inventory_item_image'];
      inventoryItem.itemType= maps[i]['inventory_item_type'];
      inventoryItem.totalType= maps[i]['inventory_item_total_type'];
      inventoryItem.piecesInType= maps[i]['inventory_item_pieces_in_type'];
      inventoryItem.costPerTypeOld= maps[i]['inventory_item_cost_per_type_old'];
      inventoryItem.costPerTypeNew= maps[i]['inventory_item_cost_per_type_new'];

      inventoryItem.isPiecesInTypeUpdated= maps[i]['inventory_item_is_pieces_in_type_updated'];
      inventoryItem.costPerItemInTypeOld= maps[i]['inventory_item_cost_per_item_in_type_old'];
      inventoryItem.costPerItemInTypeNew= maps[i]['inventory_item_cost_per_item_in_type_new'];

      inventoryItem.stock= maps[i]['inventory_item_stock'];
      inventoryItem.inventoryId= maps[i]['inventory_id'];
      inventoryItem.projectId= maps[i]['project_id'];
      inventoryItem.hasCategory= maps[i]['inventory_item_has_category'];

      return inventoryItem;
    });
  }
  Future<List<InventoryItem>> inventoriesItemsByInventoryId(Inventory inventory) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.query('inventory_item',// Use a `where` clause to delete a specific dog.
      where: 'inventory_id = ?',

      whereArgs: [inventory.id],);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var inventoryItem=InventoryItem();
      inventoryItem.id= maps[i]['inventory_item_id'];
      inventoryItem.name= maps[i]['inventory_item_name'].toString();
      inventoryItem.imageBase= maps[i]['inventory_item_image'];
      inventoryItem.itemType= maps[i]['inventory_item_type'];
      inventoryItem.totalType= maps[i]['inventory_item_total_type'];
      inventoryItem.piecesInType= maps[i]['inventory_item_pieces_in_type'];
      inventoryItem.costPerTypeOld= maps[i]['inventory_item_cost_per_type_old'];
      inventoryItem.costPerTypeNew= maps[i]['inventory_item_cost_per_type_new'];
      inventoryItem.isCostUpdated= maps[i]['inventory_item_is_cost_updated'];
      inventoryItem.isPiecesInTypeUpdated= maps[i]['inventory_item_is_pieces_in_type_updated'];
      inventoryItem.costPerItemInTypeOld= maps[i]['inventory_item_cost_per_item_in_type_old'];
      inventoryItem.costPerItemInTypeNew= maps[i]['inventory_item_cost_per_item_in_type_new'];
      inventoryItem.stock= maps[i]['inventory_item_stock'];
      inventoryItem.inventoryId= maps[i]['inventory_id'];
      inventoryItem.projectId= maps[i]['project_id'];
      inventoryItem.hasCategory= maps[i]['inventory_item_has_category'];
      return inventoryItem;
    });
  }

  Future<List<InventoryItem>> inventoriesItemsByInventoryItemId(int? inventoryId) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.query('inventory_item',// Use a `where` clause to delete a specific dog.
      where: 'inventory_item_id = ?',

      whereArgs: [inventoryId],);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var inventoryItem=InventoryItem();
      inventoryItem.id= maps[i]['inventory_item_id'];
      inventoryItem.name= maps[i]['inventory_item_name'].toString();
      inventoryItem.imageBase= maps[i]['inventory_item_image'];
      inventoryItem.itemType= maps[i]['inventory_item_type'];
      inventoryItem.totalType= maps[i]['inventory_item_total_type'];
      inventoryItem.piecesInType= maps[i]['inventory_item_pieces_in_type'];
      inventoryItem.costPerTypeOld= maps[i]['inventory_item_cost_per_type_old'];
      inventoryItem.costPerTypeNew= maps[i]['inventory_item_cost_per_type_new'];
      inventoryItem.isCostUpdated= maps[i]['inventory_item_is_cost_updated'];
      inventoryItem.isPiecesInTypeUpdated= maps[i]['inventory_item_is_pieces_in_type_updated'];
      inventoryItem.costPerItemInTypeOld= maps[i]['inventory_item_cost_per_item_in_type_old'];
      inventoryItem.costPerItemInTypeNew= maps[i]['inventory_item_cost_per_item_in_type_new'];
      inventoryItem.stock= maps[i]['inventory_item_stock'];
      inventoryItem.inventoryId= maps[i]['inventory_id'];
      inventoryItem.projectId= maps[i]['project_id'];
      inventoryItem.hasCategory= maps[i]['inventory_item_has_category'];
      return inventoryItem;
    });
  }

  Future<void> updateInventoryItem(InventoryItem inventoryItem) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;
try{
    // Update the given Dog.
    await db.update(
      'inventory_item',
      inventoryItem.toMap(),
      // Ensure that the Dog has a matching id.
      where: 'inventory_item_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [inventoryItem.id],
    );
}catch (e) {
  throw ("some arbitrary error");
}
  }

  Future<void> updateInventoryItemNameOrImage(InventoryItem inventoryItem) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;
    try{
      // Update the given Dog.
      await db.rawUpdate(
          'UPDATE inventory_item SET inventory_item_name = ? , inventory_item_image = ? WHERE inventory_item_id=?',
          [inventoryItem.name, inventoryItem.imageBase,inventoryItem.id]);
    }catch (e) {
      throw ("some arbitrary error");
    }
  }

  Future<void> updateInventoryItemWithTransactionAndAddHistory(InventoryItem inventoryItem,History history) async {
    final db = await dbProvider.database;
    try{
      await db.transaction((txn) async{

        var batch =  txn.batch();

        await txn.update(
          'inventory_item',
          inventoryItem.toMap(),
          // Ensure that the Dog has a matching id.
          where: 'inventory_item_id = ?',
          // Pass the Dog's id as a whereArg to prevent SQL injection.
          whereArgs: [inventoryItem.id],
        );

        List<Map<String, dynamic>> productItemIds = await txn.rawQuery(
            'SELECT DISTINCT product_item_id  FROM relation_product_inventory WHERE inventory_item_id = ?'
            ,[inventoryItem.id]);


        final productItemIdArray = productItemIds.map((e) => e['product_item_id']).toList();
       print('associatedProductID  $productItemIds       $productItemIdArray');


        final List<Map<String, dynamic>> toUpdateProductIdArray =  await txn.rawQuery('''
    SELECT product_item_id,product_item_price_by_percentage,SUM(printf("%.2f",cost)) as totalProductCost FROM 
    (
    SELECT 
      p.product_item_id as product_item_id,product_item_price_by_percentage,
      (w.worker_wage_per_min_new)*(pwr.work_duration*1.0) as cost
      FROM
      product_item p
      LEFT JOIN relation_product_workers pwr ON pwr.product_item_id=p.product_item_id
      LEFT JOIN workers w
      ON w.worker_id=pwr.worker_id
      WHERE p.product_item_id IN (${productItemIdArray.join(', ')})
      GROUP BY p.product_item_id
      UNION ALL
    SELECT 
      p.product_item_id,product_item_price_by_percentage,
      SUM(
          pir.inventory_item_quantity * (inventory_item_cost_per_item_in_type_new)
      ) AS cost
      FROM
      product_item p
      INNER JOIN relation_product_inventory pir ON p.product_item_id = pir.product_item_id
      INNER JOIN inventory_item inv ON inv.inventory_item_id = pir.inventory_item_id
      WHERE
      p.product_item_id IN (${productItemIdArray.join(', ')})
      GROUP BY p.product_item_id
   ) GROUP BY product_item_id
    
    ''');


        toUpdateProductIdArray.forEach((e)async {
          if((e['product_item_price_by_percentage'])>0){
            //custom round off and update product price
            final double value=(e['totalProductCost']+((e['totalProductCost']*e['product_item_price_by_percentage'])/100))!;
            final double roundOffPrice= value.toString().toCustomDoubleRoundOff;
             batch.rawUpdate('UPDATE product_item SET product_item_cost = ?,product_item_selling_price= ? WHERE product_item_id = ?',[e['totalProductCost'],roundOffPrice,e['product_item_id']]);
          }else
            batch.rawUpdate('UPDATE product_item SET product_item_cost = ?,product_item_is_inventory_item_updated = 1 WHERE product_item_id = ?',[e['totalProductCost'],e['product_item_id']]);

        });

        await txn.insert(
          'history',
          history.toMap(),
          conflictAlgorithm: ConflictAlgorithm.replace,
        );
        await batch.commit(noResult: true);
      });}catch (e) {

      throw ("some arbitrary error  $e");
    }
  }
  Future<void> updateRawInventoryItem(InventoryItem inventoryItem) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    await db.rawUpdate('UPDATE inventory_item SET inventory_item_name = ? WHERE inventory_item_id = ?',
        [inventoryItem.name, inventoryItem.id]);
  }
  Future<void> deleteInventoryItem(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'inventory_item',
      // Use a `where` clause to delete a specific dog.
      where: 'inventory_item_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
  Future<void> deleteAllDataAndInventoryItem(int? inventoryItemId) async {
    final db = await dbProvider.database;
    try{
      await db.transaction((txn) async{

        var batch =  txn.batch();

        batch.rawDelete('DELETE FROM history where inventory_item_id = ?', [inventoryItemId]);
        batch.rawDelete('DELETE FROM inventory_item where inventory_item_id = ?', [inventoryItemId]);


        await batch.commit(noResult: true);
      });}catch (e) {
      throw ("some arbitrary error");
    }
  }
  Future<void> deleteInventoryItemsByInventoryId(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'inventory_item',
      // Use a `where` clause to delete a specific dog.
      where: 'inventory_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }

  Future<List<InventoryItem>> inventoriesItemsWithCategory(int? projectID, int currentYear) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
   // final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM inventory_item ii INNER JOIN inventory_category ic ON (ii.inventory_id = ic.inventory_id)');

    /*final List<Map<String, dynamic>> maps = await db.rawQuery('''
     SELECT hs.history_inventory_item_stock AS history_inventory_item_stock,hs.history_inventory_item_pieces_in_type AS history_inventory_item_pieces_in_type, ii.inventory_item_id AS inventory_item_id, ii.inventory_item_name AS inventory_item_name, ii.inventory_item_image AS inventory_item_image, ii.inventory_id AS inventory_id, ic.inventory_name AS inventory_name, ii.inventory_item_type AS inventory_item_type,ii.inventory_item_total_type AS inventory_item_total_type, ii.inventory_item_pieces_in_type AS inventory_item_pieces_in_type, ii.inventory_item_cost_per_type_old AS inventory_item_cost_per_type_old, ii.inventory_item_cost_per_type_new AS inventory_item_cost_per_type_new, ii.inventory_item_cost_per_item_in_type_old AS inventory_item_cost_per_item_in_type_old, ii.inventory_item_cost_per_item_in_type_new AS inventory_item_cost_per_item_in_type_new, ii.inventory_item_stock AS inventory_item_stock FROM inventory_item ii INNER JOIN history hs ON (ii.inventory_item_id = hs.inventory_item_id) INNER JOIN inventory_category ic ON (ii.inventory_id = ic.inventory_id) WHERE ic.project_id = ? GROUP BY ii.inventory_item_id, ii.inventory_item_name
     UNION ALL
     SELECT hs.history_inventory_item_stock AS history_inventory_item_stock,hs.history_inventory_item_pieces_in_type AS history_inventory_item_pieces_in_type, ii.inventory_item_id AS inventory_item_id, ii.inventory_item_name AS inventory_item_name, ii.inventory_item_image AS inventory_item_image, null AS inventory_id, 'Others' AS inventory_name, ii.inventory_item_type AS inventory_item_type, ii.inventory_item_total_type AS inventory_item_total_type, ii.inventory_item_pieces_in_type AS inventory_item_pieces_in_type, ii.inventory_item_cost_per_type_old AS inventory_item_cost_per_type_old, ii.inventory_item_cost_per_type_new AS inventory_item_cost_per_type_new, ii.inventory_item_cost_per_item_in_type_old AS inventory_item_cost_per_item_in_type_old, ii.inventory_item_cost_per_item_in_type_new AS inventory_item_cost_per_item_in_type_new,ii.inventory_item_stock AS inventory_item_stock FROM inventory_item ii INNER JOIN history hs ON (ii.inventory_item_id = hs.inventory_item_id) WHERE ii.inventory_id IS NULL AND ii.project_id= ?
    ''',[projectID,projectID]);*/

    /*final List<Map<String, dynamic>> maps = await db.rawQuery('''
     SELECT DISTINCT SUM(hs.history_inventory_item_total_type) AS history_inventory_item_total_type, hs.history_inventory_item_stock AS history_inventory_item_stock, hs.history_inventory_item_total_type AS history_inventory_item_total_type, hs.history_inventory_item_pieces_in_type AS history_inventory_item_pieces_in_type, ii.inventory_item_id AS inventory_item_id, ii.inventory_item_name AS inventory_item_name, ii.inventory_item_image AS inventory_item_image, ii.inventory_id AS inventory_id, ic.inventory_name AS inventory_name, ii.inventory_item_type AS inventory_item_type,ii.inventory_item_total_type AS inventory_item_total_type, ii.inventory_item_pieces_in_type AS inventory_item_pieces_in_type, ii.inventory_item_cost_per_type_old AS inventory_item_cost_per_type_old, ii.inventory_item_cost_per_type_new AS inventory_item_cost_per_type_new, ii.inventory_item_cost_per_item_in_type_old AS inventory_item_cost_per_item_in_type_old, ii.inventory_item_cost_per_item_in_type_new AS inventory_item_cost_per_item_in_type_new, ii.inventory_item_stock AS inventory_item_stock FROM inventory_item ii INNER JOIN history hs ON (ii.inventory_item_id = hs.inventory_item_id) INNER JOIN inventory_category ic ON (ii.inventory_id = ic.inventory_id) WHERE ic.project_id = ?  GROUP BY hs.history_inventory_item_pieces_in_type
     UNION ALL
     SELECT DISTINCT SUM(hs.history_inventory_item_total_type) AS history_inventory_item_total_type, hs.history_inventory_item_stock AS history_inventory_item_stock,hs.history_inventory_item_total_type AS history_inventory_item_total_type, hs.history_inventory_item_pieces_in_type AS history_inventory_item_pieces_in_type, ii.inventory_item_id AS inventory_item_id, ii.inventory_item_name AS inventory_item_name, ii.inventory_item_image AS inventory_item_image, null AS inventory_id, 'Others' AS inventory_name, ii.inventory_item_type AS inventory_item_type, ii.inventory_item_total_type AS inventory_item_total_type, ii.inventory_item_pieces_in_type AS inventory_item_pieces_in_type, ii.inventory_item_cost_per_type_old AS inventory_item_cost_per_type_old, ii.inventory_item_cost_per_type_new AS inventory_item_cost_per_type_new, ii.inventory_item_cost_per_item_in_type_old AS inventory_item_cost_per_item_in_type_old, ii.inventory_item_cost_per_item_in_type_new AS inventory_item_cost_per_item_in_type_new,ii.inventory_item_stock AS inventory_item_stock FROM inventory_item ii INNER JOIN history hs ON (ii.inventory_item_id = hs.inventory_item_id) WHERE ii.inventory_id IS NULL AND ii.project_id= ? GROUP BY hs.history_inventory_item_pieces_in_type
    ''',[projectID,projectID]);*/


    /*final List<Map<String, dynamic>> maps = await db.rawQuery('''
     SELECT DISTINCT hs.history_inventory_item_stock AS history_inventory_item_stock, hs.history_inventory_item_total_type AS history_inventory_item_total_type, hs.history_inventory_item_pieces_in_type AS history_inventory_item_pieces_in_type, ii.inventory_item_id AS inventory_item_id, ii.inventory_item_name AS inventory_item_name, ii.inventory_item_image AS inventory_item_image, ii.inventory_id AS inventory_id, ic.inventory_name AS inventory_name, ii.inventory_item_type AS inventory_item_type,ii.inventory_item_total_type AS inventory_item_total_type, ii.inventory_item_pieces_in_type AS inventory_item_pieces_in_type, ii.inventory_item_cost_per_type_old AS inventory_item_cost_per_type_old, ii.inventory_item_cost_per_type_new AS inventory_item_cost_per_type_new, ii.inventory_item_cost_per_item_in_type_old AS inventory_item_cost_per_item_in_type_old, ii.inventory_item_cost_per_item_in_type_new AS inventory_item_cost_per_item_in_type_new, ii.inventory_item_stock AS inventory_item_stock FROM inventory_item ii INNER JOIN history hs ON (ii.inventory_item_id = hs.inventory_item_id) INNER JOIN inventory_category ic ON (ii.inventory_id = ic.inventory_id) WHERE ic.project_id = ?  GROUP BY (CASE WHEN inventory_item_type = 0 THEN hs.history_inventory_item_pieces_in_type ELSE ii.inventory_item_id END)
     UNION ALL
     SELECT DISTINCT hs.history_inventory_item_stock AS history_inventory_item_stock,hs.history_inventory_item_total_type AS history_inventory_item_total_type, hs.history_inventory_item_pieces_in_type AS history_inventory_item_pieces_in_type, ii.inventory_item_id AS inventory_item_id, ii.inventory_item_name AS inventory_item_name, ii.inventory_item_image AS inventory_item_image, null AS inventory_id, 'Others' AS inventory_name, ii.inventory_item_type AS inventory_item_type, ii.inventory_item_total_type AS inventory_item_total_type, ii.inventory_item_pieces_in_type AS inventory_item_pieces_in_type, ii.inventory_item_cost_per_type_old AS inventory_item_cost_per_type_old, ii.inventory_item_cost_per_type_new AS inventory_item_cost_per_type_new, ii.inventory_item_cost_per_item_in_type_old AS inventory_item_cost_per_item_in_type_old, ii.inventory_item_cost_per_item_in_type_new AS inventory_item_cost_per_item_in_type_new,ii.inventory_item_stock AS inventory_item_stock FROM inventory_item ii INNER JOIN history hs ON (ii.inventory_item_id = hs.inventory_item_id) WHERE ii.inventory_id IS NULL AND ii.project_id= ?  GROUP BY (CASE WHEN inventory_item_type = 0 THEN hs.history_inventory_item_pieces_in_type ELSE ii.inventory_item_id END)
     
    ''',[projectID,projectID]);*/

    final List<Map<String, dynamic>> maps = await db.rawQuery('''
     SELECT 
     CAST(strftime('%Y', hs.history_date/1000.0, 'unixepoch') AS INTEGER) AS history_year,
     SUM(hs.history_inventory_item_total_type) AS history_inventory_item_total_type_added, 
     hs.history_inventory_item_stock AS history_inventory_item_stock, 
     hs.history_inventory_item_total_type AS history_inventory_item_total_type, 
     hs.history_inventory_item_pieces_in_type AS history_inventory_item_pieces_in_type, 
     ii.inventory_item_id AS inventory_item_id, 
     ii.inventory_item_name AS inventory_item_name, 
     ii.inventory_item_image AS inventory_item_image, 
     ii.inventory_id AS inventory_id, 
     ic.inventory_name AS inventory_name, 
     ii.inventory_item_type AS inventory_item_type,
     ii.inventory_item_total_type AS inventory_item_total_type,
     ii.inventory_item_pieces_in_type AS inventory_item_pieces_in_type, 
     ii.inventory_item_cost_per_type_old AS inventory_item_cost_per_type_old, 
     ii.inventory_item_cost_per_type_new AS inventory_item_cost_per_type_new, 
     ii.inventory_item_cost_per_item_in_type_old AS inventory_item_cost_per_item_in_type_old, 
     ii.inventory_item_cost_per_item_in_type_new AS inventory_item_cost_per_item_in_type_new, 
     ii.inventory_item_stock AS inventory_item_stock 
     FROM inventory_item ii 
     INNER JOIN history hs ON (ii.inventory_item_id = hs.inventory_item_id) 
     INNER JOIN inventory_category ic ON (ii.inventory_id = ic.inventory_id) 
     WHERE ic.project_id = ? AND hs.history_inventory_item_flag LIKE ? AND history_year = ? 
     GROUP BY ii.inventory_item_id, hs.history_inventory_item_pieces_in_type
     
     UNION ALL
     
     SELECT 
     CAST(strftime('%Y', hs.history_date/1000.0, 'unixepoch') AS INTEGER) AS history_year,
     SUM(hs.history_inventory_item_total_type) AS history_inventory_item_total_type_added, 
     hs.history_inventory_item_stock AS history_inventory_item_stock,
     hs.history_inventory_item_total_type AS history_inventory_item_total_type, 
     hs.history_inventory_item_pieces_in_type AS history_inventory_item_pieces_in_type, 
     ii.inventory_item_id AS inventory_item_id, 
     ii.inventory_item_name AS inventory_item_name, 
     ii.inventory_item_image AS inventory_item_image, 
     null AS inventory_id, 
     'Others' AS inventory_name, 
     ii.inventory_item_type AS inventory_item_type, 
     ii.inventory_item_total_type AS inventory_item_total_type, 
     ii.inventory_item_pieces_in_type AS inventory_item_pieces_in_type, 
     ii.inventory_item_cost_per_type_old AS inventory_item_cost_per_type_old, 
     ii.inventory_item_cost_per_type_new AS inventory_item_cost_per_type_new, 
     ii.inventory_item_cost_per_item_in_type_old AS inventory_item_cost_per_item_in_type_old, 
     ii.inventory_item_cost_per_item_in_type_new AS inventory_item_cost_per_item_in_type_new,
     ii.inventory_item_stock AS inventory_item_stock 
     FROM inventory_item ii INNER JOIN history hs ON (ii.inventory_item_id = hs.inventory_item_id) 
     WHERE ii.inventory_id IS NULL AND ii.project_id= ? AND hs.history_inventory_item_flag LIKE ? AND history_year = ?
     GROUP BY ii.inventory_item_id, hs.history_inventory_item_pieces_in_type
     
    ''',[projectID,'purchase',currentYear,projectID,'purchase',currentYear]);

    //final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT DISTINCT * FROM inventory_item ii INNER JOIN history hs ON (ii.inventory_item_id = hs.inventory_item_id) INNER JOIN inventory_category ic WHERE ic.project_id= ?',[projectID]);


    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var inventoryItem=InventoryItem();
      inventoryItem.id= maps[i]['inventory_item_id'];
      inventoryItem.name= maps[i]['inventory_item_name'];
      inventoryItem.imageBase= maps[i]['inventory_item_image'];
      inventoryItem.itemType= maps[i]['inventory_item_type'];
      inventoryItem.totalType= maps[i]['inventory_item_total_type'];
      inventoryItem.piecesInType= maps[i]['inventory_item_pieces_in_type'];
      inventoryItem.costPerTypeOld= maps[i]['inventory_item_cost_per_type_old'];
      inventoryItem.costPerTypeNew= maps[i]['inventory_item_cost_per_type_new'];
      inventoryItem.isCostUpdated= maps[i]['inventory_item_is_cost_updated'];
      inventoryItem.isPiecesInTypeUpdated= maps[i]['inventory_item_is_pieces_in_type_updated'];
      inventoryItem.costPerItemInTypeOld= maps[i]['inventory_item_cost_per_item_in_type_old'];
      inventoryItem.costPerItemInTypeNew= maps[i]['inventory_item_cost_per_item_in_type_new'];
      inventoryItem.stock= maps[i]['inventory_item_stock'];
      inventoryItem.inventoryId= maps[i]['inventory_id'];
      inventoryItem.projectId= maps[i]['project_id'];
      inventoryItem.hasCategory= maps[i]['inventory_item_has_category'];
      inventoryItem.inventoryName= maps[i]['inventory_name'];



      inventoryItem.historyAddedStock= maps[i]['history_inventory_item_stock'];
      inventoryItem.historyInventoryItemPiecesInType= maps[i]['history_inventory_item_pieces_in_type'];
      // set total add type as history_inventory_item_total_type_added historyInventoryItemTotalType instead of history_inventory_item_total_type
      inventoryItem.historyInventoryItemTotalType= maps[i]['history_inventory_item_total_type_added'];

      return inventoryItem;
    });
  }

  Future<List<GraphInventoryItem>> inventoriesItemHistoryById(int? inventoryItemId, int currentYear) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.

   // final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT DISTINCT * FROM inventory_item ii INNER JOIN history hs ON (hs.inventory_item_id = ? AND ii.inventory_item_id = hs.inventory_item_id)',[inventoryItemId]);
    final List<Map<String, dynamic>> maps = await db.rawQuery('''
     SELECT MAX(hs.history_inventory_item_stock) AS max_value_item_type ,
     CAST(strftime('%Y', hs.history_date/1000.0, 'unixepoch') AS INTEGER) AS history_year, 
     CAST(strftime('%m', hs.history_date/1000.0, 'unixepoch') AS INTEGER) AS history_month, 
     SUM(CASE WHEN hs.history_inventory_item_flag LIKE ? THEN hs.history_inventory_item_total_type ELSE 0 END) AS history_inventory_item_total_type_added, 
     SUM(CASE WHEN hs.history_inventory_item_flag LIKE ? THEN hs.history_inventory_item_stock ELSE 0.0 END) AS history_inventory_item_stock_consumed , 
     hs.history_date AS history_date, hs.history_inventory_item_stock AS history_inventory_item_stock, hs.history_inventory_item_total_type AS history_inventory_item_total_type, 
     hs.history_inventory_item_pieces_in_type AS history_inventory_item_pieces_in_type, 
     ii.inventory_item_id AS inventory_item_id, 
     ii.inventory_item_name AS inventory_item_name, 
     ii.inventory_item_image AS inventory_item_image, 
     ii.inventory_id AS inventory_id, 
     ii.inventory_item_type AS inventory_item_type,ii.inventory_item_total_type AS inventory_item_total_type, 
     ii.inventory_item_pieces_in_type AS inventory_item_pieces_in_type, 
     ii.inventory_item_cost_per_type_old AS inventory_item_cost_per_type_old, 
     ii.inventory_item_cost_per_type_new AS inventory_item_cost_per_type_new, 
     ii.inventory_item_cost_per_item_in_type_old AS inventory_item_cost_per_item_in_type_old, 
     ii.inventory_item_cost_per_item_in_type_new AS inventory_item_cost_per_item_in_type_new, 
     ii.inventory_item_stock AS inventory_item_stock 
     
     FROM history hs INNER JOIN inventory_item ii ON (ii.inventory_item_id = hs.inventory_item_id) 
     WHERE hs.inventory_item_id = ? AND history_year = ?  GROUP BY history_year, history_month
    
    ''',['purchase','sales',inventoryItemId,currentYear]);

    //final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT DISTINCT * FROM inventory_item ii INNER JOIN history hs ON (ii.inventory_item_id = hs.inventory_item_id) INNER JOIN inventory_category ic WHERE ic.project_id= ?',[projectID]);


    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var graphInventoryItem=GraphInventoryItem();
      graphInventoryItem.id= maps[i]['inventory_item_id'];
      graphInventoryItem.name= maps[i]['inventory_item_name'].toString();
      graphInventoryItem.imageBase= maps[i]['inventory_item_image'];
      graphInventoryItem.itemType= maps[i]['inventory_item_type'];
      graphInventoryItem.totalType= maps[i]['inventory_item_total_type'];
      graphInventoryItem.piecesInType= maps[i]['inventory_item_pieces_in_type'];
      graphInventoryItem.costPerTypeOld= maps[i]['inventory_item_cost_per_type_old'];
      graphInventoryItem.costPerTypeNew= maps[i]['inventory_item_cost_per_type_new'];
      graphInventoryItem.isCostUpdated= maps[i]['inventory_item_is_cost_updated'];
      graphInventoryItem.isPiecesInTypeUpdated= maps[i]['inventory_item_is_pieces_in_type_updated'];
      graphInventoryItem.costPerItemInTypeOld= maps[i]['inventory_item_cost_per_item_in_type_old'];
      graphInventoryItem.costPerItemInTypeNew= maps[i]['inventory_item_cost_per_item_in_type_new'];
      graphInventoryItem.stock= maps[i]['inventory_item_stock'];
      graphInventoryItem.inventoryId= maps[i]['inventory_id'];
      graphInventoryItem.projectId= maps[i]['project_id'];
      graphInventoryItem.hasCategory= maps[i]['inventory_item_has_category'];
      //inventoryItem.inventoryName= maps[i]['inventory_name'];



      graphInventoryItem.historyAddedStock= maps[i]['history_inventory_item_stock'];
      graphInventoryItem.historyInventoryItemPiecesInType= maps[i]['history_inventory_item_pieces_in_type'];
      // set total add type as history_inventory_item_total_type_added historyInventoryItemTotalType instead of history_inventory_item_total_type
      graphInventoryItem.historyInventoryItemTotalType= maps[i]['history_inventory_item_total_type_added'];
      graphInventoryItem.historyStockConsumed= maps[i]['history_inventory_item_stock_consumed'];
      graphInventoryItem.historyAddedDate= maps[i]['history_date'];
      graphInventoryItem.maxOfTotalType= maps[i]['max_value_item_type'];
      graphInventoryItem.historyYear= maps[i]['history_year'];
      graphInventoryItem.historyMonth= maps[i]['history_month'];

      return graphInventoryItem;
    });
  }

  Future<List<InventoryItem>> inventoriesItemsByProjectIdWithCategory(int? projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM inventory_item ii INNER JOIN inventory_category ic ON (ii.inventory_id = ic.inventory_id) WHERE ic.project_id = ?',[projectID]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var inventoryItem=InventoryItem();
      inventoryItem.id= maps[i]['inventory_item_id'];
      inventoryItem.name= maps[i]['inventory_item_name'];
      inventoryItem.imageBase= maps[i]['inventory_item_image'];
      inventoryItem.itemType= maps[i]['inventory_item_type'];
      inventoryItem.totalType= maps[i]['inventory_item_total_type'];
      inventoryItem.piecesInType= maps[i]['inventory_item_pieces_in_type'];
      inventoryItem.costPerTypeOld= maps[i]['inventory_item_cost_per_type_old'];
      inventoryItem.costPerTypeNew= maps[i]['inventory_item_cost_per_type_new'];
      inventoryItem.isCostUpdated= maps[i]['inventory_item_is_cost_updated'];
      inventoryItem.isPiecesInTypeUpdated= maps[i]['inventory_item_is_pieces_in_type_updated'];
      inventoryItem.costPerItemInTypeOld= maps[i]['inventory_item_cost_per_item_in_type_old'];
      inventoryItem.costPerItemInTypeNew= maps[i]['inventory_item_cost_per_item_in_type_new'];
      inventoryItem.stock= maps[i]['inventory_item_stock'];
      inventoryItem.inventoryId= maps[i]['inventory_id'];
      inventoryItem.projectId= maps[i]['project_id'];
      inventoryItem.hasCategory= maps[i]['inventory_item_has_category'];
      inventoryItem.inventoryName= maps[i]['inventory_name'];

      return inventoryItem;
    });
  }


  Future<List<InventoryItem>> inventoriesItemsByProjectIdWithCategoryNew(int? projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.rawQuery('''
     SELECT ii.inventory_item_id AS inventory_item_id, 
     ii.inventory_item_name AS inventory_item_name, 
     ii.inventory_item_image AS inventory_item_image, 
     ii.inventory_id AS inventory_id, ic.inventory_name AS inventory_name, 
     ii.inventory_item_type AS inventory_item_type,
     ii.inventory_item_total_type AS inventory_item_total_type, 
     ii.inventory_item_pieces_in_type AS inventory_item_pieces_in_type, 
     ii.inventory_item_cost_per_type_old AS inventory_item_cost_per_type_old, 
     ii.inventory_item_cost_per_type_new AS inventory_item_cost_per_type_new, 
     ii.inventory_item_cost_per_item_in_type_old AS inventory_item_cost_per_item_in_type_old, 
     ii.inventory_item_cost_per_item_in_type_new AS inventory_item_cost_per_item_in_type_new, 
     ii.inventory_item_stock AS inventory_item_stock 
     
     FROM inventory_item ii INNER JOIN inventory_category ic ON (ii.inventory_id = ic.inventory_id) WHERE ic.project_id = ? GROUP BY ii.inventory_item_id, ii.inventory_item_name
     UNION ALL
     SELECT ii.inventory_item_id AS inventory_item_id, 
     ii.inventory_item_name AS inventory_item_name, 
     ii.inventory_item_image AS inventory_item_image, 
     null AS inventory_id, 
     'Others' AS inventory_name, 
     ii.inventory_item_type AS inventory_item_type, 
     ii.inventory_item_total_type AS inventory_item_total_type, 
     ii.inventory_item_pieces_in_type AS inventory_item_pieces_in_type, 
     ii.inventory_item_cost_per_type_old AS inventory_item_cost_per_type_old, 
     ii.inventory_item_cost_per_type_new AS inventory_item_cost_per_type_new, 
     ii.inventory_item_cost_per_item_in_type_old AS inventory_item_cost_per_item_in_type_old, 
     ii.inventory_item_cost_per_item_in_type_new AS inventory_item_cost_per_item_in_type_new,
     ii.inventory_item_stock AS inventory_item_stock 
     
     FROM inventory_item ii WHERE ii.inventory_id IS NULL AND ii.project_id=$projectID
    ''',[projectID]);
    //final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM inventory_item WHERE project_id = ?',[projectID]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var inventoryItem=InventoryItem();
      inventoryItem.id= maps[i]['inventory_item_id'];
      inventoryItem.name= maps[i]['inventory_item_name'].toString();
      inventoryItem.imageBase= maps[i]['inventory_item_image'];
      inventoryItem.itemType= maps[i]['inventory_item_type'];
      inventoryItem.totalType= maps[i]['inventory_item_total_type'];
      inventoryItem.piecesInType= maps[i]['inventory_item_pieces_in_type'];
      inventoryItem.costPerTypeOld= maps[i]['inventory_item_cost_per_type_old'];
      inventoryItem.costPerTypeNew= maps[i]['inventory_item_cost_per_type_new'];
      inventoryItem.isCostUpdated= maps[i]['inventory_item_is_cost_updated'];
      inventoryItem.isPiecesInTypeUpdated= maps[i]['inventory_item_is_pieces_in_type_updated'];
      inventoryItem.costPerItemInTypeOld= maps[i]['inventory_item_cost_per_item_in_type_old'];
      inventoryItem.costPerItemInTypeNew= maps[i]['inventory_item_cost_per_item_in_type_new'];
      inventoryItem.stock= maps[i]['inventory_item_stock'];
      inventoryItem.inventoryId= maps[i]['inventory_id'];
      inventoryItem.projectId= maps[i]['project_id'];
      inventoryItem.hasCategory= maps[i]['inventory_item_has_category'];
      inventoryItem.inventoryName= maps[i]['inventory_name'].toString();

      return inventoryItem;
    });
  }


  Future<List<InventoryItem>> inventoriesItemsByProductItem(int? productItemId ) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;
try{
    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM relation_product_inventory ri INNER JOIN inventory_item ii ON (ri.product_item_id = ? AND ii.inventory_item_id = ri.inventory_item_id)',[productItemId]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var inventoryItem=InventoryItem();
      inventoryItem.id= maps[i]['inventory_item_id'];
      inventoryItem.name= maps[i]['inventory_item_name'].toString();
      inventoryItem.imageBase= maps[i]['inventory_item_image'];
      inventoryItem.itemType= maps[i]['inventory_item_type'];
      inventoryItem.totalType= maps[i]['inventory_item_total_type'];
      inventoryItem.piecesInType= maps[i]['inventory_item_pieces_in_type'];
      inventoryItem.costPerTypeOld= maps[i]['inventory_item_cost_per_type_old'];
      inventoryItem.costPerTypeNew= maps[i]['inventory_item_cost_per_type_new'];
      inventoryItem.isCostUpdated= maps[i]['inventory_item_is_cost_updated'];
      inventoryItem.isPiecesInTypeUpdated= maps[i]['inventory_item_is_pieces_in_type_updated'];
      inventoryItem.costPerItemInTypeOld= maps[i]['inventory_item_cost_per_item_in_type_old'];
      inventoryItem.costPerItemInTypeNew= maps[i]['inventory_item_cost_per_item_in_type_new'];
      inventoryItem.stock= maps[i]['inventory_item_stock'];
      inventoryItem.inventoryId= maps[i]['inventory_id'];
      inventoryItem.projectId= maps[i]['project_id'];
      inventoryItem.hasCategory= maps[i]['inventory_item_has_category'];
      inventoryItem.inventoryName= maps[i]['inventory_name'];
      inventoryItem.selectedQuantity= maps[i]['inventory_item_quantity'];

      return inventoryItem;
    });

  }catch (e) {
  throw ("some arbitrary error");
  }
  }




  Future<List<InventoryItem>> inventoriesItemsByHistory(int? inventoryItemId) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT DISTINCT * FROM inventory_item ii INNER JOIN history hs ON (hs.inventory_item_id = ? AND ii.inventory_item_id = hs.inventory_item_id) WHERE hs.history_inventory_item_flag LIKE ?',[inventoryItemId,'purchase']);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var inventoryItem=InventoryItem();
      inventoryItem.id= maps[i]['inventory_item_id'];
      inventoryItem.name= maps[i]['inventory_item_name'].toString();
      inventoryItem.imageBase= maps[i]['inventory_item_image'];
      inventoryItem.itemType= maps[i]['inventory_item_type'];
      inventoryItem.totalType= maps[i]['inventory_item_total_type'];
      inventoryItem.piecesInType= maps[i]['inventory_item_pieces_in_type'];
      inventoryItem.costPerTypeOld= maps[i]['inventory_item_cost_per_type_old'];
      inventoryItem.costPerTypeNew= maps[i]['inventory_item_cost_per_type_new'];
      inventoryItem.isCostUpdated= maps[i]['inventory_item_is_cost_updated'];
      inventoryItem.isPiecesInTypeUpdated= maps[i]['inventory_item_is_pieces_in_type_updated'];
      inventoryItem.costPerItemInTypeOld= maps[i]['inventory_item_cost_per_item_in_type_old'];
      inventoryItem.costPerItemInTypeNew= maps[i]['inventory_item_cost_per_item_in_type_new'];
      inventoryItem.stock= maps[i]['inventory_item_stock'];
      inventoryItem.inventoryId= maps[i]['inventory_id'];
      inventoryItem.projectId= maps[i]['project_id'];
      inventoryItem.hasCategory= maps[i]['inventory_item_has_category'];
      inventoryItem.historyId= maps[i]['history_id'];
      inventoryItem.historyAddedDate= maps[i]['history_date'];
      inventoryItem.historyInventoryItemType= maps[i]['history_inventory_item_type'];
      inventoryItem.historyInventoryItemTotalType= maps[i]['history_inventory_item_total_type'];
      inventoryItem.historyInventoryItemPiecesInType= maps[i]['history_inventory_item_pieces_in_type'];
      inventoryItem.historyInventoryItemCostPerType= maps[i]['history_inventory_item_cost_per_type'];
      inventoryItem.historyInventoryItemCostPerItemInType= maps[i]['history_inventory_item_cost_per_item_in_type_new'];
      inventoryItem.historyAddedStock= maps[i]['history_inventory_item_stock'];
      inventoryItem.historyInventoryItemFlag= maps[i]['history_inventory_item_flag'];


      return inventoryItem;
    });
  }

  Future<List<InventoryItem>> allInventoriesItemsWithHistory() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT DISTINCT * FROM inventory_item ii INNER JOIN history hs ON(ii.inventory_item_id = hs.inventory_item_id)');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var inventoryItem=InventoryItem();
      inventoryItem.id= maps[i]['inventory_item_id'];
      inventoryItem.name= maps[i]['inventory_item_name'];
      inventoryItem.imageBase= maps[i]['inventory_item_image'];
      inventoryItem.itemType= maps[i]['inventory_item_type'];
      inventoryItem.totalType= maps[i]['inventory_item_total_type'];
      inventoryItem.piecesInType= maps[i]['inventory_item_pieces_in_type'];
      inventoryItem.costPerTypeOld= maps[i]['inventory_item_cost_per_type_old'];
      inventoryItem.costPerTypeNew= maps[i]['inventory_item_cost_per_type_new'];
      inventoryItem.isCostUpdated= maps[i]['inventory_item_is_cost_updated'];
      inventoryItem.isPiecesInTypeUpdated= maps[i]['inventory_item_is_pieces_in_type_updated'];
      inventoryItem.costPerItemInTypeOld= maps[i]['inventory_item_cost_per_item_in_type_old'];
      inventoryItem.costPerItemInTypeNew= maps[i]['inventory_item_cost_per_item_in_type_new'];
      inventoryItem.stock= maps[i]['inventory_item_stock'];
      inventoryItem.inventoryId= maps[i]['inventory_id'];
      inventoryItem.projectId= maps[i]['project_id'];
      inventoryItem.hasCategory= maps[i]['inventory_item_has_category'];
      inventoryItem.historyId= maps[i]['history_id'];
      inventoryItem.historyAddedDate= maps[i]['history_date'];
      inventoryItem.historyInventoryItemType= maps[i]['history_inventory_item_type'];
      inventoryItem.historyInventoryItemTotalType= maps[i]['history_inventory_item_total_type'];
      inventoryItem.historyInventoryItemPiecesInType= maps[i]['history_inventory_item_pieces_in_type'];
      inventoryItem.historyInventoryItemCostPerType= maps[i]['history_inventory_item_cost_per_type'];
      inventoryItem.historyInventoryItemCostPerItemInType= maps[i]['history_inventory_item_cost_per_item_in_type_new'];
      inventoryItem.historyAddedStock= maps[i]['history_inventory_item_stock'];


      /*final List<Map<String, dynamic>> historyMaps=maps[i]['history_data'];
      List.generate(historyMaps.length, (i) {
        var history=History();
        history.id= maps[i]['history_id'];
        history.historyDate= maps[i]['history_date'];
        history.inventoryItemId= maps[i]['inventory_item_id'];
        history.historyItemType= maps[i]['history_inventory_item_type'];
        history.historyTotalType= maps[i]['history_inventory_item_total_type'];
        history.historyPiecesInType= maps[i]['history_inventory_item_pieces_in_type'];
        history.historyCostPerType= maps[i]['history_inventory_item_total_cost'];
        history.historyCostPerItemInType= maps[i]['history_inventory_item_cost_per_item_in_type_new'];

      });*/

      return inventoryItem;
    });
  }

  Future<void> updateInventoryItemStock(InventoryItem inventoryItem) async {
    final db = await dbProvider.database;
try{
    await db.rawUpdate(
        'UPDATE inventory_item SET inventory_item_stock = ? WHERE inventory_item_id = ?',
        [inventoryItem.stock, inventoryItem.id]);
  }catch (e) {
  throw ("some arbitrary error");
  }
  }

  Future<void> updateInventoryItemIsUpdatedStatus(InventoryItem inventoryItem) async {
    final db = await dbProvider.database;

    await db.rawUpdate(
        'UPDATE inventory_item SET inventory_item_is_cost_updated = ? WHERE inventory_item_id = ?',
        [inventoryItem.isCostUpdated, inventoryItem.id]);
  }
  Future<void> deleteInventoryItemByProjectId(int? projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;
    await db.execute('DELETE FROM inventory_item where inventory_item_id IN (SELECT inventory_item_id from inventory_item WHERE inventory_id IN (SELECT inventory_id FROM inventory_category WHERE project_id=?))' ,[projectID]);

  }

  Future<void> updateInventoryItemCategoryId(int? categoryId,List<int> inventoryItemIdArray) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    await db.rawUpdate('UPDATE inventory_item SET inventory_id = ? WHERE inventory_item_id IN (${inventoryItemIdArray.join(', ')})',
        [categoryId]);
  }


  Future<void> updateInventoryItemWOnSwitchMeasure(InventoryItem inventoryItem,dynamic stockMultiplierValue) async {
    final db = await dbProvider.database;
    try{
      await db.transaction((txn) async{

        var batch =  txn.batch();
        await txn.rawUpdate(
            'UPDATE inventory_item SET inventory_item_stock = ?, inventory_item_cost_per_item_in_type_new = ?,inventory_item_type =?, inventory_item_pieces_in_type =?  WHERE inventory_item_id = ?',
            [inventoryItem.stock,inventoryItem.costPerItemInTypeNew,inventoryItem.itemType,inventoryItem.piecesInType, inventoryItem.id]);

        await txn.rawUpdate('UPDATE history SET history_inventory_item_type = ?,history_inventory_item_pieces_in_type =history_inventory_item_pieces_in_type * $stockMultiplierValue,  history_inventory_item_cost_per_item_in_type_new = history_inventory_item_cost_per_item_in_type_new/$stockMultiplierValue, history_inventory_item_stock = history_inventory_item_stock * $stockMultiplierValue WHERE inventory_item_id = ?' ,[inventoryItem.itemType,inventoryItem.id,]);


        await txn.rawUpdate('UPDATE relation_product_inventory SET inventory_item_quantity = inventory_item_quantity * $stockMultiplierValue where inventory_item_id = ?' ,[inventoryItem.id,]);

        await batch.commit(noResult: true);
      });}catch (e) {

      throw ("some arbitrary error  $e");
    }
  }

}