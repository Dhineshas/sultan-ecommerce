import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/Cart.dart';
import 'package:ecommerce/model/ProductItem.dart';
import 'package:ecommerce/model/Project.dart';
import 'package:sqflite/sqflite.dart';

import '../utils/Utils.dart';

class ProjectOperations{
  late ProjectOperations projectOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<int> insertProject(Project project) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    final projectId= await db.insert(
      'project',
      project.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return projectId;
  }

  // A method that retrieves all the dogs from the dogs table.
  Future<List<Project>> projects() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('project');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var project =Project();
      project.id= maps[i]['project_id'];
      project.userId= maps[i]['user_id'];
      project.projectName= maps[i]['project_name'];
      project.imageBase= maps[i]['project_image'];
      project.contactNo= maps[i]['project_contact_no'].toString();
      project.email= maps[i]['project_email'];

      return project;
    });
  }

  Future<List<Project>> projectsByUserID(int? userID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;


    final List<Map<String, dynamic>> maps = await db.query('project',where: 'user_id = ?',

      whereArgs: [userID],);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var project =Project();
      project.id= maps[i]['project_id'];
      project.userId= maps[i]['user_id'];
      project.projectName= maps[i]['project_name'];
      project.imageBase= maps[i]['project_image'];
      project.contactNo= maps[i]['project_contact_no'].toString();
      project.email= maps[i]['project_email'];

      return project;
    });
  }

  Future<List<Project>> projectsByUserIDAndSubscription(int? userID,int? activeSubscription) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;
     List<Map<String, dynamic>> maps=[];
switch(activeSubscription){
  case 0:
    MyPref.setProject(null);
    break;
  case 1:
     maps = await db.query('project',where: 'user_id = ?',limit: 5, whereArgs: [userID],);
    break;
  case 2:
     maps = await db.query('project',where: 'user_id = ?',limit: 5, whereArgs: [userID],);
    break;
  case 3:
     maps = await db.query('project',where: 'user_id = ?',limit: 1, whereArgs: [userID],);
    break;
}

    //final List<Map<String, dynamic>> maps = await db.query('project',where: 'user_id = ?', whereArgs: [userID],);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var project =Project();
      project.id= maps[i]['project_id'];
      project.userId= maps[i]['user_id'];
      project.projectName= maps[i]['project_name'];
      project.imageBase= maps[i]['project_image'];
      project.contactNo= maps[i]['project_contact_no'].toString();
      project.email= maps[i]['project_email'];

      return project;
    });
  }

  Future<List<Project>> project(int? projectId) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM project WHERE project_id = ?',[projectId]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var project =Project();
      project.id= maps[i]['project_id'];
      project.userId= maps[i]['user_id'];
      project.projectName= maps[i]['project_name'];
      project.imageBase= maps[i]['project_image'];
      project.contactNo= maps[i]['project_contact_no'].toString();
      project.email= maps[i]['project_email'];

      return project;
    });
  }


  Future<void> updateProject(Project project) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;
    // Update the given Dog.
    await db.update(
      'project',
      project.toMap(),
      // Ensure that the Dog has a matching id.
      where: 'project_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [project.id],
    );
  }
  Future<void> deleteProject(int id) async {
    // Get a reference to the database.
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'project',
      // Use a `where` clause to delete a specific dog.
      where: 'project_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }

  Future<void> deleteAllDataAndProject(int? projectID) async {
    final db =  await dbProvider.database;
    try{
   await db.transaction((txn) async{

      var batch =  txn.batch();
    // 1st delete all inventory item history related data from history table on project delete
     batch.rawDelete('DELETE FROM history where inventory_item_id IN (SELECT inventory_item_id from inventory_item WHERE inventory_id IN (SELECT inventory_id FROM inventory_category WHERE project_id=?))' ,[projectID]);
      // 1st delete all inventory item history related data from history table on project delete
      batch.rawDelete('DELETE FROM inventory_item where inventory_item_id IN (SELECT inventory_item_id from inventory_item WHERE inventory_id IN (SELECT inventory_id FROM inventory_category WHERE project_id=?))' ,[projectID]);
        // 3rd delete all inventory category related data from inventory category table on project delete
      batch.rawDelete('DELETE FROM inventory_category  WHERE project_id = ?',[projectID]);
          // 4th delete all product project related data from product project relation table on project delete
      batch.rawDelete('DELETE FROM relation_product_inventory where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id IN (SELECT product_category_id FROM product_category WHERE project_id=?))' ,[projectID]);
            // 5th delete all worker relation related data from worker relation table on project delete
      batch.rawDelete('DELETE FROM relation_product_workers where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id IN (SELECT product_category_id FROM product_category WHERE project_id=?))' ,[projectID]);
              // 6th delete all worker related data from worker table on project delete
      batch.rawDelete('DELETE FROM workers WHERE project_id = ?',[projectID]);
                // 7th delete all cart relation data from cart relation table on project delete
      batch.rawDelete('DELETE FROM relation_cart_product where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id IN (SELECT product_category_id FROM product_category WHERE project_id=?))' ,[projectID]);
                  // 8th delete all cart data from cart table on project delete
      batch.rawDelete('DELETE FROM cart WHERE project_id = ?',[projectID]);
                    // 9th delete all orders related data from orders table on project delete
      batch.rawDelete('DELETE FROM orders WHERE product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id IN (SELECT product_category_id FROM product_category WHERE project_id=?))' ,[projectID]);
                      // 10th delete all storage related data from storage table on project delete
      batch.rawDelete('DELETE FROM storage where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id IN (SELECT product_category_id FROM product_category WHERE project_id=?))' ,[projectID]);
                        //11th delete all product items related with project on project delete
      batch. rawDelete('DELETE FROM product_item where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id IN (SELECT product_category_id FROM product_category WHERE project_id=?))' ,[projectID]);
                          // 12th then delete all product category related with project on project
      batch.rawDelete('DELETE FROM product_category WHERE project_id = ?',[projectID]);
      // 13th then delete all Extra charges related with project on project
      batch.rawDelete('DELETE FROM extra_charge WHERE project_id = ?',[projectID]);
                            // 14th then delete all product item related with project on project
      batch.rawDelete('DELETE FROM project WHERE project_id = ?',[projectID]);

     await batch.commit(noResult: true);


    }); }catch (e){
      throw("some arbitrary error");
    }
  }

  ///reference for nested delete of all project related data on project delete (use in screen state)
/*var projectOperations=ProjectOperations();
var productCategoryOperations = ProductCategoryOperations();
var productItemOperations = ProductItemOperations();
var productItemRelationOperations = RelationOperations();
var workerRelationOperations = WorkerRelationOperations();
var workersOperations = WorkersOperations();
var cartRelationOperations = CartRelationOperations();
var cartOperations = CartOperations();
var orderOperations = OrderOperations();
var storageOperations = StorageOperations();
var historyItemOperations = HistoryOperations();
var inventoryItemOperations = InventoryItemOperations();
var inventoryOperations = InventoryOperations();
 onDelete(int? index) async{
    var id=projects[index!].id;
    await orderOperations.isOrderNewExistByProjectId(id).then((bool)async {
      if(bool){
        showAlertDialog(context,'In order proceed, either deliver or cancel the order related to the current project.');
      }else{
        // 1st delete all inventory item history related data from history table on project delete
        await historyItemOperations.deleteHistoryItemByProjectId(id!).then((value)async {
          // 1st delete all inventory item history related data from history table on project delete
          await inventoryItemOperations.deleteInventoryItemByProjectId(id).then((value)async {
            // 3rd delete all inventory category related data from inventory category table on project delete
            await inventoryOperations.deleteInventoryByProjectId(id).then((value)async {
              // 4th delete all product project related data from product project relation table on project delete
              await productItemRelationOperations.deleteRelationByProjectId(id).then((value)async {
                // 5th delete all worker relation related data from worker relation table on project delete
                await workerRelationOperations.deleteWorkerRelationByProjectId(id).then((value)async {
                  // 6th delete all worker related data from worker table on project delete
                  await workersOperations.deleteWorkerByProjectId(id).then((value) async{
                    // 7th delete all cart relation data from cart relation table on project delete
                    await cartRelationOperations.deleteCartRelationItemByProjectId(id).then((value)async {
                      // 8th delete all cart data from cart table on project delete
                      await cartOperations.deleteCartByProjectId(id).then((value)async {
                        // 9th delete all orders related data from orders table on project delete
                        await orderOperations.deleteOrderItemByProjectId(id).then((value)async {
                          // 10th delete all storage related data from storage table on project delete
                          await storageOperations.deleteStorageItemByProjectId(id).then((value)async {
                            //11th delete all product items related with project on project delete
                            await productItemOperations.deleteProductItemByProjectId(id).then((value)async {
                              // 12th then delete all product category related with project on project
                              await productCategoryOperations.deleteProductCategoryByProjectId(id).then((value)async {
                                // 13th then delete all product item related with project on project
                                await projectOperations.deleteProject(id).then((value)async {
                                  await MyPref.setProject(null);
                                  setState(() {
                                    projects.removeAt(index);
                                    Navigator.of(context).pop(true);
                                  });
                                });

                              });});});});});});});});});});});});

      }
    });
  }*/



  Future<int> projectCount(int? userId) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    var result = await db.rawQuery(
        'SELECT COUNT(project_id) FROM project WHERE user_id = ?'
        ,[userId]);
    int count = Sqflite.firstIntValue(result)!;
    return count;
  }
}