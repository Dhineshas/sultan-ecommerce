import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/Home.dart';
import 'package:sqflite/sqflite.dart';

import '../model/GraphInventoryItem.dart';
import '../model/GraphProductItem.dart';
import '../model/History.dart';

class HomeOperations {
  late HomeOperations homeOperations;
  final dbProvider = DatabaseHelper();


  Future<Home?> getHomeData(int? projectID, int currentYear) async {
    // Get a reference to the database.
    final db = await dbProvider.database;
    try{
      var expenseQuery = await db.rawQuery('SELECT  SUM(extra_charge_cost) AS total_expense FROM extra_charge WHERE project_id = ?',[projectID]);

      var totalExpense = expenseQuery.first["total_expense"]!=null?expenseQuery.first["total_expense"]:0.0;
      var storageQuery = await db.rawQuery('SELECT  SUM(storage_extra_product_item_selling_price*quantity-storage_extra_product_item_cost*quantity) AS storage_profit FROM storage_extra_product WHERE project_id = ?',[projectID]);
      var storageProfit = storageQuery.first["storage_profit"]!=null?storageQuery.first["storage_profit"]:0.0;

      var salaryQuery = await db.rawQuery('SELECT SUM(worker_salary) AS total_salary FROM workers WHERE project_id = ?',[projectID]);
      var totalSalary = salaryQuery.first["total_salary"]!=null?salaryQuery.first["total_salary"]:0.0;


      var query = await db.rawQuery('''SELECT 
    
    CAST(strftime('%Y', od.order_created/1000.0, 'unixepoch') AS INTEGER) AS order_year,
  
    SUM(od.order_quantity) AS sum_order_quantity,
    SUM(od.order_gross_total) AS sum_order_gross_total,
    SUM(od.order_net_total) AS sum_order_net_total,
    SUM(od.order_quantity*pi.product_item_cost) AS sum_order_total_product_cost,
    SUM(od.order_quantity*pi.product_item_selling_price) AS sum_order_total_product_price,
    SUM(od.order_quantity*(pi.product_item_selling_price-pi.product_item_cost)) AS sum_order_total_product_profit
    
     
    FROM orders od 
    INNER JOIN product_item pi ON (od.product_item_id = pi.product_item_id) WHERE pi.project_id = ? AND od.order_status = ? AND od.product_item_id = pi.product_item_id
     GROUP BY order_year
    ''',[projectID,2]);
      var totalProfit =query.last['sum_order_total_product_profit']!=null?query.last['sum_order_total_product_profit']:0.0;
      var totalSelling =query.last['sum_order_total_product_price']!=null?query.last['sum_order_total_product_price']:0.0;
      var totalCost =query.last['sum_order_total_product_cost']!=null?query.last['sum_order_total_product_cost']:0.0;
      var home = Home();
        home.totalProfit= double.tryParse(totalProfit.toString())!+double.tryParse(storageProfit.toString())!;
        home.totalExpense=double.parse(totalExpense.toString());
        home.totalSalary=double.parse(totalSalary.toString());
        home.totalCost=double.parse(totalCost.toString());
        home.totalSelling=double.parse(totalSelling.toString());

        return home;
    }catch(e){}

  }

  Future<List<GraphProductItem>> productItemsGrossAndNetInOrdersByProjectId(int? projectID, int currentYear) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;


    final List<Map<String, dynamic>> maps = await db.rawQuery('''SELECT 
    
    CAST(strftime('%Y', od.order_created/1000.0, 'unixepoch') AS INTEGER) AS order_year,
    CAST(strftime('%m', od.order_created/1000.0, 'unixepoch') AS INTEGER) AS order_month,
    pi.product_item_id AS product_item_id,
    pi.product_item_name AS product_item_name,
    SUM(od.order_quantity) AS sum_order_quantity,
    SUM(od.order_gross_total) AS sum_order_gross_total,
    SUM(od.order_net_total) AS sum_order_net_total,
    SUM(od.order_quantity*pi.product_item_cost) AS sum_order_total_product_cost,
    SUM(od.order_quantity*(pi.product_item_selling_price-pi.product_item_cost)) AS sum_order_total_product_profit
    
     
    FROM orders od 
    INNER JOIN product_item pi ON (od.product_item_id = pi.product_item_id) WHERE pi.project_id = ? AND od.order_status = ? AND order_year = ? AND od.product_item_id = pi.product_item_id
     GROUP BY order_year, order_month
    ''',[projectID,2,currentYear]);


    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var graphProductItem=GraphProductItem();
      graphProductItem.id= maps[i]['product_item_id'];
      graphProductItem.name= maps[i]['product_item_name'].toString();
      graphProductItem.orderQuantityTotal= maps[i]['sum_order_quantity'];
      graphProductItem.orderGrossTotal= maps[i]['sum_order_gross_total'];
      graphProductItem.orderNetTotal= maps[i]['sum_order_net_total'];
      graphProductItem.orderTotalProductCost= maps[i]['sum_order_total_product_cost'];
      graphProductItem.orderTotalProductProfit= maps[i]['sum_order_total_product_profit'];
      graphProductItem.orderYear= maps[i]['order_year'];
      graphProductItem.orderMonth= maps[i]['order_month'];

      return graphProductItem;
    });



  }

  Future<List<GraphInventoryItem>> inventoriesItemPurchaseHistory(int? projectID, int currentYear) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.

    // final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT DISTINCT * FROM inventory_item ii INNER JOIN history hs ON (hs.inventory_item_id = ? AND ii.inventory_item_id = hs.inventory_item_id)',[inventoryItemId]);
    final List<Map<String, dynamic>> maps = await db.rawQuery('''
     SELECT 
    
     CAST(strftime('%Y', hs.history_date/1000.0, 'unixepoch') AS INTEGER) AS history_year, 
     CAST(strftime('%m', hs.history_date/1000.0, 'unixepoch') AS INTEGER) AS history_month, 
     SUM(CASE WHEN ii.inventory_item_type LIKE ? THEN hs.history_inventory_item_cost_per_type*hs.history_inventory_item_total_type ELSE hs.history_inventory_item_cost_per_type END) AS history_inventory_total_purchase_cost,
     SUM(hs.history_inventory_item_total_type) AS history_inventory_item_total_type
      
      
     
     FROM history hs INNER JOIN inventory_item ii ON (ii.inventory_item_id = hs.inventory_item_id) 
     WHERE ii.project_id = ? AND hs.history_inventory_item_flag LIKE ? AND history_year = ? AND hs.inventory_item_id = ii.inventory_item_id  GROUP BY history_month
    
    ''',[0,projectID,'purchase',currentYear]);

    //final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT DISTINCT * FROM inventory_item ii INNER JOIN history hs ON (ii.inventory_item_id = hs.inventory_item_id) INNER JOIN inventory_category ic WHERE ic.project_id= ?',[projectID]);


    // Convert the List<Map<String, dynamic> into a List<Dog>.

    return List.generate(maps.length, (i) {
      var graphInventoryItem=GraphInventoryItem();

      graphInventoryItem.inventoryTotalPurchaseCost= maps[i]['history_inventory_total_purchase_cost'];
      graphInventoryItem.historyInventoryItemTotalType= maps[i]['history_inventory_item_total_type'];
      graphInventoryItem.historyYear= maps[i]['history_year'];
      graphInventoryItem.historyMonth= maps[i]['history_month'];

      return graphInventoryItem;
    });
  }
}