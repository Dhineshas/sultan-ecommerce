import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecommerce/model/FirestoreUser.dart';
import 'package:ecommerce/model/IAPPurchaceData.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../model/UserIssuesData.dart';


class FireStoreOperations {
  final fireStoreDb = FirebaseFirestore.instance;


  // Define a function that inserts dogs into the database
  Future<void> insertFireStoreUserData(FirestoreUser fireStoreUser) async {
    await fireStoreDb.collection("users")
        .doc(fireStoreUser.userEmail)
        .set(fireStoreUser.toMap()).then((value) {
      print("successfully added");
    }).onError((error, stackTrace) {
      print("Error writing document: $error");
    });




  }

  Future<void> updateFireStoreUserData(FirestoreUser fireStoreUser) async {
    await fireStoreDb.collection("users")
        .doc(fireStoreUser.userEmail)
        .update({'user_email':fireStoreUser.userEmail,'user_name':fireStoreUser.userName,'user_updated_at':fireStoreUser.userUpdatedAt}).then((value) {
      print("successfully added");
    }).onError((error, stackTrace) {
      print("Error writing document: $error");
    });
  }




  Future<void> updateFireStoreUserStorageUrl(String? userEmail,String storageUrl) async {
    await fireStoreDb.collection("users")
        .doc(userEmail)
        .update({'user_storage_url':storageUrl}).then((value) {
      print("successfully updates");

    }).onError((error, stackTrace)  {
      print("Error updating document: $error");
    });
  }

  Future<bool> checkDocumentExist(String? docID) async {

    try {
      var result = await fireStoreDb.doc("users/$docID").get();

      return result.exists;
    }catch (e) {
      // If any error
      return false;
    }
  }
  Future<bool> checkDocumentPasswordExist(String? docID) async {
    DocumentSnapshot? dat;
    try
    {
      var result = await fireStoreDb.doc("users/$docID").get()
          .then((DocumentSnapshot value ) =>{
            print('doc coll field ${value.data()}'),
            dat = value
      }
      );
      print('ddddddattttt aaaa= ${dat?["user_password"]}');
      if(dat != null)
        {
          if(dat?["user_password"] == '' || dat?["user_password"] == "" || dat?["user_password"] == null)
            {
              print('ddddddattttt ssss= ${dat?["user_password"]}');
              return false;
            }
          else
            {
              print('ddddddattttt dddd= ${dat?["user_password"]}');
              return true;
            }
        }
      return false;
    }
    catch (e) {
      // If any error
      return false;
    }
  }

  Future<void> addFireStoreSubscription(IAPPurchaseData iapPurchaseData) async {

    await fireStoreDb.collection("users")
        .doc(iapPurchaseData.userEmail)
        .collection('subscriptions').doc(iapPurchaseData.productId).set(iapPurchaseData.toMap());
  }

  Future<void> addFireStoreUserComplaint(UserIssuesData userIssueDat) async {

    await fireStoreDb.collection("users")
        .doc(FirebaseAuth.instance.currentUser?.email ?? "Logout Users Issues")
        .collection('ISSUES').doc(DateTime.now().millisecondsSinceEpoch.toString()).set(userIssueDat.toMap());
  }

  Future<List<IAPPurchaseData>>  getFireStoreSubscriptionDocument(String userEmail)async{

    final List<QueryDocumentSnapshot<Map<String, dynamic>>> maps = (await fireStoreDb.doc("users/$userEmail").collection('subscriptions').get()).docs;


    return List.generate(maps.length, (i) {
      var iapPurchaseData =IAPPurchaseData();
      iapPurchaseData.userId= maps[i]['user_id'];
      iapPurchaseData.productId= maps[i]['product_id'];
      iapPurchaseData.purchaseId= maps[i]['purchase_id'];
      iapPurchaseData.purchaseStatus= maps[i]['purchase_status'];
      iapPurchaseData.purchaseTransactionDate= maps[i]['purchase_transaction_date'];
      iapPurchaseData.purchaseVerificationDataOrToken= maps[i]['purchase_verification_data_or_token'];
      iapPurchaseData.purchaseDateStarting= maps[i]['purchase_date_starting'];
      iapPurchaseData.purchaseSource= maps[i]['purchase_source'];
      iapPurchaseData.purchaseDateEnding= maps[i]['purchase_date_ending'];
      iapPurchaseData.purchaseDevice= maps[i]['purchase_device'];
      iapPurchaseData.purchaseIsAutoRenewing= maps[i]['purchase_is_auto_renewing'];
      return iapPurchaseData;
    });
  }
  Future<void> updateFireStoreSubscriptionStatusToExpired(IAPPurchaseData iapPurchaseData) async {
    await fireStoreDb.collection("users")
        .doc(iapPurchaseData.userEmail)
        .collection('subscriptions').doc(iapPurchaseData.productId).update({'purchase_status': iapPurchaseData.purchaseStatus});
  }
}