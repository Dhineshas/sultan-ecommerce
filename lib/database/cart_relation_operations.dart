import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/Cart.dart';
import 'package:ecommerce/model/CartRelation.dart';
import 'package:ecommerce/model/InventoryItem.dart';
import 'package:ecommerce/model/Orders.dart';
import 'package:ecommerce/model/ProductItem.dart';
import 'package:sqflite/sqflite.dart';

class CartRelationOperations{
  late CartRelationOperations cartOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<void> insertCartRelation(CartRelation cartRelation) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'relation_cart_product',
      cartRelation.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  // A method that retrieves all the dogs from the dogs table.
  Future<List<CartRelation>> cartRelations() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('relation_cart_product');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var cartRelation=CartRelation();
      cartRelation.id= maps[i]['cart_relation_id'];
      cartRelation.cartId= maps[i]['cart_id'];
      cartRelation.productItemId= maps[i]['product_item_id'];
      cartRelation.quantity= maps[i]['cart_quantity'];


      return cartRelation;
    });
  }

  Future<bool> productItemIdExistInCartRelation(int? id) async {
    final db =  await dbProvider.database;
    /*var result = await db.rawQuery(
      'SELECT EXISTS(SELECT 1 FROM tagTable WHERE product_item_id="aaa")',
    );*/
    var result =await db.rawQuery('SELECT EXISTS(SELECT 1 FROM relation_cart_product WHERE product_item_id=?)',
        [id]);

    int? exists = Sqflite.firstIntValue(result);
    return exists == 1;
  }
  Future<void> updateCartRelation(Cart cart) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Update the given Dog.
    await db.update(
      'cart',
      cart.toMap(),
      // Ensure that the Dog has a matching id.
      where: 'cart_relation_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [cart.id],
    );
  }
  Future<void> deleteCartRelation(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'relation_cart_product',
      // Use a `where` clause to delete a specific dog.
      where: 'cart_relation_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
  Future<void> clearCartRelation(int? cartId) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;


    await db.delete(
      'relation_cart_product',
      // Use a `where` clause to delete a specific dog.
      where: 'cart_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [cartId],
    );
  }

  Future<void> deleteCartRelationItemByProductItemId(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'relation_cart_product',
      // Use a `where` clause to delete a specific dog.
      where: 'product_item_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
  Future<void> deleteCartRelationItemByProductId(int id) async {
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    await db.execute(
        'DELETE FROM relation_cart_product where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id = ?)',[id]
    );
  }

  Future<void> deleteCartRelationItemByProjectId(int projectID) async {
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    await db.execute('DELETE FROM relation_cart_product where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id IN (SELECT product_category_id FROM product_category WHERE project_id=?))' ,[projectID]);

  }
  Future<void> updateCartRelationItemQuantity(ProductItem value) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    await db.rawUpdate('UPDATE relation_cart_product SET cart_quantity = ? WHERE product_item_id = ?',
        [value.cartQuantity, value.id]);
  }

 /* Future<void> purchaseNowWithTransaction(List<Orders> orders, List<ProductItem> items) async {
    final db = await dbProvider.database;
    try{
      await db.transaction((txn) async{

        var batch =  txn.batch();


        if(orders.isNotEmpty){
          orders.forEach((order)async {
            batch.insert(
              'orders',
              order.toMap(),
              conflictAlgorithm: ConflictAlgorithm.replace,
            );
          });


        }

        items.forEach((item)async {
          final List<Map<String, dynamic>>  inventoryItems= await txn.rawQuery('SELECT * FROM relation_product_inventory ri INNER JOIN inventory_item ii ON (ri.product_item_id = ? AND ii.inventory_item_id = ri.inventory_item_id)',[item.id]);







          inventoryItems.forEach((inventory) async{

               if (((int.tryParse('${inventory['inventory_item_quantity']}'))! * (item.cartQuantity!)) <= (int.tryParse('${inventory['inventory_item_stock']}'))!) {

                 var inventoryItem = InventoryItem();
                 inventoryItem.id = int.tryParse('${inventory['inventory_item_id']}');
                 inventoryItem.stock =int.tryParse('${inventory['inventory_item_stock']}')! - ((int.tryParse('${inventory['inventory_item_quantity']}'))! * (item.cartQuantity!));
                 batch.rawUpdate(
                     'UPDATE inventory_item SET inventory_item_stock = ? WHERE inventory_item_id = ?',
                     [inventory['inventory_item_stock'],inventory['inventory_item_id']]);

                 var currentStock=await txn.rawQuery('SELECT inventory_item_stock FROM inventory_item WHERE inventory_item_id = ?',[inventoryItem.id]);

                 print('vbhbvhb $currentStock   ${inventory['inventory_item_id']}   ${inventory['inventory_item_quantity']} ${item.cartQuantity!} ${inventory['inventory_item_stock']}  ${inventoryItem.stock}');

               }



          });

        });





        await txn.delete(
          'relation_cart_product',
          // Ensure that the Dog has a matching id.
          where: 'cart_id = ?',
          // Pass the Dog's id as a whereArg to prevent SQL injection.
          whereArgs: [items.first.cartId],
        );

        await batch.commit(noResult: true);
      });}catch (e) {

      throw ("some arbitrary error");
    }
  }
*/
}