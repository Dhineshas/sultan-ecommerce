import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/ProductItem.dart';
import 'package:ecommerce/model/Storage.dart';
import 'package:sqflite/sqflite.dart';

import '../model/StorageExtra.dart';

class StorageOperations{
  late StorageOperations storageOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<void> insertStorage(Storage storage) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'storage',
      storage.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertStorageExtra(StorageExtra storageExtra) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'storage_extra_product',
      storageExtra.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }


  // A method that retrieves all the dogs from the dogs table.
  Future<List<Storage>> storage() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('storage');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var storage=Storage();
      storage.id= maps[i]['storage_id'];
      storage.productItemId= maps[i]['product_item_id'];
      storage.quantity= maps[i]['quantity'];

      return storage;
    });
  }

  Future<bool> productItemIdExist(int? id) async {
    final db =  await dbProvider.database;
    /*var result = await db.rawQuery(
      'SELECT EXISTS(SELECT 1 FROM tagTable WHERE product_item_id="aaa")',
    );*/
    var result =await db.rawQuery('SELECT EXISTS(SELECT 1 FROM storage WHERE product_item_id=?)',
        [id]);

    int? exists = Sqflite.firstIntValue(result);
    return exists == 1;
  }
  Future<void> updateStorage(Storage storage) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Update the given Dog.
    await db.update(
      'storage',
      storage.toMap(),
      // Ensure that the Dog has a matching id.
      where: 'storage_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [storage.id],
    );
  }
  Future<void> deleteStorage(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'storage',
      // Use a `where` clause to delete a specific dog.
      where: 'storage_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
  Future<void> clearStorage() async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'storage',
    );
  }

  Future<void> deleteStorageItemByProductItemId(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'storage',
      // Use a `where` clause to delete a specific dog.
      where: 'product_item_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }

  Future<void> deleteStorageExtraItemByProductItemId(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'storage_extra_product',
      // Use a `where` clause to delete a specific dog.
      where: 'storage_extra_product_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
  Future<void> deleteStorageItemByProductId(int id) async {
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    await db.execute(
        'DELETE FROM storage where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id = ?)',[id]
    );
  }
  Future<void> deleteStorageItemByProjectId(int projectID) async {
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    await db.execute('DELETE FROM storage where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id IN (SELECT product_category_id FROM product_category WHERE project_id=?))' ,[projectID]);

  }
  Future<void> updateStorageItemQuantity(StorageExtra value) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    await db.rawUpdate('UPDATE storage SET quantity = ? WHERE product_item_id = ?',
        [value.storageQuantity, value.id]);
  }

  Future<void> updateStorageExtraItemQuantity(StorageExtra value) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    await db.rawUpdate('UPDATE storage_extra_product SET quantity = ? WHERE storage_extra_product_id = ?',
        [value.storageQuantity, value.id]);
  }

  Future<List<StorageExtra>> productItemsInStorageAndStorageExtraByProjectId(int? projectID) async {
    // Get a reference to the database.
    /*UNION ALL

    SELECT se.storage_extra_product_id AS product_item_id, se.storage_extra_product_item_name AS product_item_name, se.storage_extra_product_item_image AS product_item_image, se.storage_extra_product_item_cost AS product_item_cost, se.quantity AS quantity
    FROM storage_extra_product se
    WHERE se.project_id = ?*/
    final db =  await dbProvider.database;

    /*final List<Map<String, dynamic>> maps = await db.rawQuery('''SELECT
    pi.product_item_id AS product_item_id, pi.product_item_name AS product_item_name, pi.product_item_image AS product_item_image, pi.product_item_selling_price AS product_item_selling_price, pi.product_item_cost AS product_item_cost, sg.quantity AS quantity
    FROM storage sg 
    INNER JOIN product_item pi ON (sg.product_item_id = pi.product_item_id)
    WHERE pi.project_id = ? GROUP BY  pi.product_item_id
    
    UNION ALL

    SELECT se.storage_extra_product_id AS product_item_id, se.storage_extra_product_item_name AS product_item_name, se.storage_extra_product_item_image AS product_item_image, se.storage_extra_product_item_cost AS product_item_selling_price, 0.0 AS product_item_cost, se.quantity AS quantity
    FROM storage_extra_product se
    WHERE se.project_id = ?
    ''',[projectID,projectID]);*/


    final List<Map<String, dynamic>> maps = await db.rawQuery('''SELECT 
    pi.product_item_id AS storage_extra_product_id, 
    pi.product_item_name AS storage_extra_product_item_name, 
    pi.product_item_image AS storage_extra_product_item_image, 
    pi.product_item_selling_price AS storage_extra_product_item_selling_price, 
    pi.product_item_cost AS storage_extra_product_item_cost, 
    sg.quantity AS quantity, 
    0 AS storage_extra_product_item_status
    FROM storage sg 
    INNER JOIN product_item pi ON (sg.product_item_id = pi.product_item_id)
    WHERE pi.project_id = ? GROUP BY  pi.product_item_id
    
    UNION ALL

    SELECT se.storage_extra_product_id AS storage_extra_product_id, 
    se.storage_extra_product_item_name AS storage_extra_product_item_name, 
    se.storage_extra_product_item_image AS storage_extra_product_item_image, 
    se.storage_extra_product_item_selling_price AS storage_extra_product_item_selling_price, 
    se.storage_extra_product_item_cost AS storage_extra_product_item_cost, 
    se.quantity AS quantity, 1 AS storage_extra_product_item_status
    FROM storage_extra_product se
    WHERE se.project_id = ?
    ''',[projectID,projectID]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    /*return List.generate(maps.length, (i) {
      var productItem=ProductItem();
      productItem.id= maps[i]['product_item_id'];
      productItem.name= maps[i]['product_item_name'];
      productItem.imageBase= maps[i]['product_item_image'];
      productItem.cost= maps[i]['product_item_cost'];
      productItem.sellingPrice= maps[i]['product_item_selling_price'];
      productItem.priceByPercentage= maps[i]['product_item_price_by_percentage'];
      productItem.isInventoryItemUpdated= maps[i]['product_item_is_inventory_item_updated'];
      productItem.productCategoryId= maps[i]['product_category_id'];
      productItem.storageQuantity= maps[i]['quantity'];
      productItem.projectId= maps[i]['project_id'];
      productItem.hasCategory= maps[i]['product_item_has_category'];

      return productItem;
    })*/

    return List.generate(maps.length, (i) {
      var storageExtra=StorageExtra();
      storageExtra.id= maps[i]['storage_extra_product_id'];
      storageExtra.name= maps[i]['storage_extra_product_item_name'].toString();
      storageExtra.imageBase= maps[i]['storage_extra_product_item_image'];
      storageExtra.cost= maps[i]['storage_extra_product_item_cost'];
      storageExtra.sellingPrice= maps[i]['storage_extra_product_item_selling_price'];
      storageExtra.storageQuantity= maps[i]['quantity'];
      storageExtra.projectId= maps[i]['project_id'];
      storageExtra.storageSourceStatus= maps[i]['storage_extra_product_item_status'];


      return storageExtra;
    });
  }

}