import 'dart:io';

import 'package:ecommerce/database/user_operation.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;
import 'package:path/path.dart';


class DatabaseHelper {
  static final DatabaseHelper _instance = new  DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;

  static Database? _db;

  /*Future<Database> get database async =>
      _db ??= await initDb();*/
  Future<Database> get database async{
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();


    var firebaseUser= FirebaseAuth.instance.currentUser;
   // var databaseName ='${firebaseUser?.providerData.first.email}${firebaseUser?.providerData.first.providerId}.db';
    var databaseName ='${firebaseUser?.providerData.first.email}.db';

    String path = join(documentsDirectory.path,"${databaseName}");
    if(_db!=null&&File(path).existsSync()&&_db?.path==File(path).path){
      print('db_not_null and file path exist');
      return _db!;
    } else if(_db==null&&File(path).existsSync()){
      print('db_null_with_path_exist');
      var theExistingDb = await openDatabase(path, version: 1, onCreate: (Database db, int version) async {}, onUpgrade: (db, oldVersion, newVersion) async {
        print('onUpgradeDB $oldVersion  $newVersion');

        /*var batch = db.batch();
        if (oldVersion == 1) {
          // We update existing table and create the new tables
          batch.execute('''
          ALTER TABLE project ADD description TEXT
          ''');
          print('pppfpfpfpfp $oldVersion  $newVersion');
        }
        await batch.commit();*/
      },);
        return _db=theExistingDb ;
    }else{
      print('db_null and no path');
      return _db= await initDb();
    }

  }



  initDb() async {

    WidgetsFlutterBinding.ensureInitialized();
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();

    var firebaseUser= FirebaseAuth.instance.currentUser;
    //var databaseName ='${firebaseUser?.providerData.first.email}${firebaseUser?.providerData.first.providerId}.db';
    var databaseName ='${firebaseUser?.providerData.first.email}.db';
    //String path = join(documentsDirectory.path, "main_database.db");


    print('open_db ${databaseName}');
    if(firebaseUser!=null)
    {
    String path = join(documentsDirectory.path,"${databaseName}");
    var theDb = await openDatabase(path, version: 1, onCreate: (Database db, int version) async {

      await db.execute('''
          create table user (
            user_id INTEGER PRIMARY KEY autoincrement,
            user_name STRING,
            user_photo STRING,
            user_email VARCHAR NOT NULL UNIQUE,
            user_uid STRING NOT NULL,
            user_mobile STRING,
            user_provider STRING,
            user_db_url STRING,
            user_is_login INTEGER NOT NULL DEFAULT 0)
          ''');

      await db.execute('''
          create table subscription (
            iap_id INTEGER PRIMARY KEY autoincrement,
            product_id STRING NOT NULL,
            user_id INTEGER NOT NULL,
            purchase_id STRING NOT NULL,
            purchase_status STRING NOT NULL,
            purchase_transaction_date INTEGER,
            purchase_verification_data_or_token STRING,
            purchase_date_starting INTEGER,
            purchase_date_ending INTEGER,
            purchase_source STRING,
            purchase_device STRING NULL,
            purchase_is_auto_renewing INTEGER NULL,
            FOREIGN KEY(user_id) REFERENCES user(user_id))
          ''');

      await db.execute('''
          create table project (
            project_id INTEGER PRIMARY KEY autoincrement,
            user_id INTEGER NOT NULL,
            project_name STRING NOT NULL,
            project_image STRING NOT NULL,
            project_contact_no STRING NOT NULL,
            project_email STRING NOT NULL,
            project_address STRING NULL,
            FOREIGN KEY(user_id) REFERENCES user(user_id))
          ''');

      await db.execute('''
          create table product_category (
            product_category_id INTEGER PRIMARY KEY autoincrement,
            project_id INTEGER NOT NULL,
            product_category_name STRING NOT NULL,
            product_category_image STRING NOT NULL,
            FOREIGN KEY(project_id) REFERENCES project(project_id))
          ''');


      await db.execute('''
          create table product_item (
            product_item_id INTEGER PRIMARY KEY autoincrement,
            product_item_name STRING NOT NULL,
            product_item_image STRING NOT NULL,
            product_category_id INTEGER,
            product_item_cost FLOAT NOT NULL,
            product_item_selling_price FLOAT NOT NULL DEFAULT 0.0,
            product_item_price_by_percentage INTEGER NOT NULL DEFAULT 0.0,
            product_item_is_inventory_item_updated INTEGER NOT NULL,
            project_id INTEGER NOT NULL,
            product_item_has_category INTEGER NOT NULL DEFAULT 0,
             FOREIGN KEY(project_id) REFERENCES project(project_id),
          FOREIGN KEY(product_category_id) REFERENCES product_category(product_category_id))
          ''');


      await db.execute('''
          create table inventory_category (
            inventory_id INTEGER PRIMARY KEY autoincrement,
            project_id INTEGER NOT NULL,
            inventory_name STRING NOT NULL,
            inventory_image STRING NOT NULL,
            FOREIGN KEY(project_id) REFERENCES project(project_id))
          ''');
      await db.execute('''
          create table inventory_item (
            inventory_item_id INTEGER PRIMARY KEY autoincrement,
            inventory_item_name STRING NOT NULL,
            inventory_item_image STRING NOT NULL,
            inventory_item_type INTEGER NOT NULL,
            inventory_item_total_type INTEGER NOT NULL,
            inventory_item_pieces_in_type INTEGER NOT NULL,
            inventory_item_cost_per_type_old FLOAT NOT NULL DEFAULT 0.0,
            inventory_item_cost_per_type_new FLOAT NOT NULL DEFAULT 0.0,
            inventory_item_is_cost_updated INTEGER NOT NULL,
            
            
            inventory_item_is_pieces_in_type_updated INTEGER NOT NULL,
            inventory_item_cost_per_item_in_type_old FLOAT NOT NULL DEFAULT 0.0,
            inventory_item_cost_per_item_in_type_new FLOAT NOT NULL DEFAULT 0.0,
            
            inventory_item_stock FLOAT NOT NULL,
            inventory_id INTEGER,
            project_id INTEGER NOT NULL,
            inventory_item_has_category INTEGER NOT NULL DEFAULT 0,
            CHECK(inventory_item_stock >= 0),
            FOREIGN KEY(project_id) REFERENCES project(project_id),
          FOREIGN KEY(inventory_id) REFERENCES inventory_category(inventory_id))
          ''');

      await db.execute('''
          create table relation_product_inventory (
            relation_id INTEGER PRIMARY KEY autoincrement,
            product_item_id INTEGER NOT NULL,
            inventory_item_id INTEGER NOT NULL,
            inventory_item_quantity FLOAT NOT NULL,
            UNIQUE (product_item_id, inventory_item_id),
            FOREIGN KEY(product_item_id) REFERENCES product_item(product_item_id),
            FOREIGN KEY(inventory_item_id) REFERENCES inventory_item(inventory_item_id))
          ''');

      await db.execute('''
          create table workers (
            worker_id INTEGER PRIMARY KEY autoincrement,
            project_id INTEGER NOT NULL,
            worker_name STRING NOT NULL,
            worker_salary INTEGER NOT NULL,
            worker_hours INTEGER NOT NULL,
            worker_wage_per_min_old FLOAT NOT NULL,
            worker_wage_per_min_new FLOAT NOT NULL,
            FOREIGN KEY(project_id) REFERENCES project(project_id))
          ''');
      await db.execute('''
          create table relation_product_workers (
            relation_id INTEGER PRIMARY KEY autoincrement,
            product_item_id INTEGER NOT NULL,
            worker_id INTEGER NOT NULL,
            work_duration INTEGER NOT NULL,
            UNIQUE (product_item_id, worker_id),
            FOREIGN KEY(product_item_id) REFERENCES product_item(product_item_id),
            FOREIGN KEY(worker_id) REFERENCES workers(worker_id))
          ''');

      await db.execute('''
          create table cart (
            cart_id INTEGER PRIMARY KEY autoincrement,
            project_id INTEGER NOT NULL,
            cart_delivery_charge FLOAT NOT NULL DEFAULT 0.0,
            cart_discount FLOAT NOT NULL DEFAULT 0.0,
            UNIQUE (cart_id, project_id),
            FOREIGN KEY(project_id) REFERENCES project(project_id))
          ''');

      await db.execute('''
          create table relation_cart_product (
            cart_relation_id INTEGER PRIMARY KEY autoincrement,
            cart_id INTEGER NOT NULL,
            product_item_id INTEGER NOT NULL,
            cart_quantity INTEGER NOT NULL,
            UNIQUE (cart_relation_id, cart_id),
            FOREIGN KEY(cart_id) REFERENCES cart(cart_id),
            FOREIGN KEY(product_item_id) REFERENCES product_item(product_item_id))
          ''');



      await db.execute('''
          create table orders (
            order_id INTEGER PRIMARY KEY autoincrement,
            order_created INTEGER NOT NULL,
            product_item_id INTEGER NOT NULL,
            order_delivery_charge FLOAT NOT NULL,
            order_discount FLOAT NOT NULL,
            order_gross_total FLOAT NOT NULL,
            order_net_total FLOAT NOT NULL,
            order_quantity INTEGER NOT NULL,
            order_status INTEGER NOT NULL,
            FOREIGN KEY(product_item_id) REFERENCES product_item(product_item_id))
          ''');

      await db.execute('''
          create table storage (
            storage_id INTEGER PRIMARY KEY autoincrement,
            product_item_id INTEGER NOT NULL UNIQUE,
            quantity INTEGER NOT NULL,
            FOREIGN KEY(product_item_id) REFERENCES product_item(product_item_id))
          ''');

      await db.execute('''
          create table storage_extra_product (
            storage_extra_product_id INTEGER PRIMARY KEY autoincrement,
            project_id INTEGER NOT NULL,
            storage_extra_product_item_name STRING NOT NULL,
            storage_extra_product_item_image STRING NOT NULL,
            storage_extra_product_item_cost FLOAT NOT NULL,
            storage_extra_product_item_selling_price FLOAT NOT NULL,
            quantity INTEGER NOT NULL,
            FOREIGN KEY(project_id) REFERENCES project(project_id))
          ''');

      await db.execute('''
          create table history (
            history_id INTEGER PRIMARY KEY autoincrement,
            history_date INTEGER NOT NULL,
            inventory_item_id INTEGER NOT NULL,
            
            history_inventory_item_type INTEGER NOT NULL,
          
            history_inventory_item_total_type INTEGER NOT NULL,
            history_inventory_item_pieces_in_type INTEGER NOT NULL,
            history_inventory_item_cost_per_type FLOAT NOT NULL,
            history_inventory_item_cost_per_item_in_type_new FLOAT NOT NULL,
            
            history_inventory_item_stock FLOAT NOT NULL,
            history_inventory_item_flag STRING NOT NULL,
            
            UNIQUE (history_id, inventory_item_id),
            FOREIGN KEY(inventory_item_id) REFERENCES inventory_item(inventory_item_id))
          ''');

      await db.execute('''
          create table extra_charge (
            extra_charge_id INTEGER PRIMARY KEY autoincrement,
            project_id INTEGER NOT NULL,
            extra_charge_cost FLOAT NOT NULL,
            extra_charge_details STRING NOT NULL,
            extra_charge_date INTEGER NOT NULL)
          ''');



    },onUpgrade: (db, oldVersion, newVersion) async {
      var batch = db.batch();
      if (oldVersion == 1) {
        // We update existing table and create the new tables
        /*batch.execute('''
          ALTER TABLE project ADD description TEXT
          ''');*/
        print('onUpgradeDB $oldVersion  $newVersion');
      }
      await batch.commit();
    });
    return theDb;
    }


}
  DatabaseHelper.internal();


}