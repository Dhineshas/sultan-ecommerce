
import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/UserCredentials.dart';
import 'package:sqflite/sqflite.dart';

import 'DatabaseHelper.dart';

class UserOperations{
  late UserOperations credentialOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<void> insertUser(UserCredentials cred) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //updateUserNameByEmail
    // In this case, replace any previous data.
    await db.insert(
      'user',
      cred.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<UserCredentials>> users() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('user');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var user=UserCredentials();
      user.id= maps[i]['user_id'];
      user.userName= maps[i]['user_name'];
      user.userPhoto= maps[i]['user_photo'];
      user.userEmail= maps[i]['user_email'];
      user.userUid= maps[i]['user_uid'];
      user.userMobile= maps[i]['user_mobile'];
      user.userProvider= maps[i]['user_provider'];
      user.userDbUrl= maps[i]['user_db_url'];
      user.userIsLogin= maps[i]['user_is_login'];
      return user;
    });
  }

  Future<void> updateUser(UserCredentials cred) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Update the given Dog.
    await db.update(
      'user',
      cred.toMap(),
      // Ensure that the Dog has a matching id.
      where: 'user_uid = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [cred.userUid],
    );
  }
  Future<void> updateUserByUid(UserCredentials cred) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Update the given Dog.
    await db.rawQuery('UPDATE user SET user_name = ?, user_photo = ?, user_email = ?, user_mobile = ?, user_provider = ?, user_is_login= ?  WHERE user_uid = ?',
        [cred.userName,cred.userPhoto==null?'':cred.userPhoto,cred.userEmail,cred.userMobile==null?'':cred.userMobile,cred.userProvider,1,cred.userUid]);
  }

  Future<void> updateUserByEmail(UserCredentials cred) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Update the given Dog.
    await db.rawQuery('UPDATE user SET user_name = ?, user_photo = ?, user_email = ?, user_mobile = ?, user_provider = ?, user_is_login= ?, user_uid =?  WHERE user_email = ?',
        [cred.userName??'',cred.userPhoto==null?'':cred.userPhoto,cred.userEmail,cred.userMobile==null?'':cred.userMobile,cred.userProvider,1,cred.userUid,cred.userEmail]);
  }

  Future<void> updateUserNameByEmail(String name,String email) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Update the given Dog.
    await db.rawQuery('UPDATE user SET user_name = ? WHERE user_email = ?',
        [name??'',email]);
  }

  Future<bool> userUidIdExist(String? uId) async {
    final db =  await dbProvider.database;

    var result =await db.rawQuery('SELECT EXISTS(SELECT 1 FROM user WHERE user_uid=?)',
        [uId]);

    int? exists = Sqflite.firstIntValue(result);
    return exists == 1;
  }

  Future<bool> userEmailExist(String? email) async {
    final db =  await dbProvider.database;

    var result =await db.rawQuery('SELECT EXISTS(SELECT 1 FROM user WHERE user_email=?)',
        [email]);

    int? exists = Sqflite.firstIntValue(result);
    return exists == 1;
  }

  Future<void> updateAllIsLoggedInState() async {
    final db =  await dbProvider.database;

    await db.rawQuery('UPDATE user SET user_is_login = ?',
        [0]);

  }

  Future<List<UserCredentials>> getLastLoggedInUser() async {
    final db =  await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.query('user', where: 'user_is_login = ?',
      whereArgs: [1], );

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var user=UserCredentials();
      user.id= maps[i]['user_id'];
      user.userName= maps[i]['user_name'];
      user.userPhoto= maps[i]['user_photo'];
      user.userEmail= maps[i]['user_email'];
      user.userUid= maps[i]['user_uid'];
      user.userMobile= maps[i]['user_mobile'];
      user.userProvider= maps[i]['user_provider'];
      user.userDbUrl= maps[i]['user_db_url'];
      user.userIsLogin= maps[i]['user_is_login'];
      return user;
    });
  }
  Future<void> updateUserDbUrl(UserCredentials cred) async {
    final db =  await dbProvider.database;

    await db.rawQuery('UPDATE user SET user_db_url = ? WHERE user_id = ?',
        [cred.userDbUrl,cred.id]);

  }
}