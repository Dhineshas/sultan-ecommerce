import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/Cart.dart';
import 'package:ecommerce/model/CartRelation.dart';
import 'package:ecommerce/model/ProductItem.dart';
import 'package:ecommerce/model/ProjectProductRelation.dart';
import 'package:sqflite/sqflite.dart';

/*
class ProjectProductRelationOperations{
  late ProjectProductRelationOperations cartOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<void> insertProjectProductRelation(ProjectProductRelation projectProductRelation) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'project_product_category_relation',
      projectProductRelation.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  // A method that retrieves all the dogs from the dogs table.
  Future<List<ProjectProductRelation>> projectProductRelation() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('project_product_category_relation');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var projectProductRelation=ProjectProductRelation();
      projectProductRelation.projectId= maps[i]['project_id'];
      projectProductRelation.productCategoryId= maps[i]['product_category_id'];
      return projectProductRelation;
    });
  }

  Future<bool> productIdExistInProjectProductRelation(int? id) async {
    final db =  await dbProvider.database;
    */
/*var result = await db.rawQuery(
      'SELECT EXISTS(SELECT 1 FROM tagTable WHERE product_item_id="aaa")',
    );*//*

    var result =await db.rawQuery('SELECT EXISTS(SELECT 1 FROM project_product_category_relation WHERE product_category_id=?)',
        [id]);

    int? exists = Sqflite.firstIntValue(result);
    return exists == 1;
  }
  Future<void> updateCartRelation(Cart cart) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Update the given Dog.
    await db.update(
      'project_product_category_relation',
      cart.toMap(),
      // Ensure that the Dog has a matching id.
      where: 'product_category_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [cart.id],
    );
  }
  Future<void> deleteProjectProductRelation(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'project_product_category_relation',
      // Use a `where` clause to delete a specific dog.
      where: 'product_category_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
  Future<void> clearProjectProductRelation() async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'project_product_category_relation',
    );
  }


  Future<void> deleteProjectProductRelationByProductId(int id) async {
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    await db.execute(
        'DELETE FROM relation_cart_product where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id = ?)'[id]
    );
  }


}*/
