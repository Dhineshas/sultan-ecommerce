
import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/ExtraCharge.dart';
import 'package:ecommerce/model/Worker.dart';
import 'package:sqflite/sqflite.dart';

class ExtraChargeOperations{
  late ExtraChargeOperations extraChargeOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<void> insertExtraCharge(ExtraCharge extraCharge) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'extra_charge',
      extraCharge.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  // A method that retrieves all the dogs from the dogs table.
  Future<List<ExtraCharge>> allExtraCharges() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('extra_charge');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var extraCharge=ExtraCharge();
      extraCharge.extraChargeId= maps[i]['extra_charge_id'];
      extraCharge.projectId= maps[i]['project_id'];
      extraCharge.extraChargeDetails= maps[i]['extra_charge_details'];
      extraCharge.extraChargeCost= maps[i]['extra_charge_cost'];
      extraCharge.extraChargeDate= maps[i]['extra_charge_date'];

      return extraCharge;
    });
  }
  Future<List<ExtraCharge>> extraChargesByProjectId( int? projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.rawQuery('select * from extra_charge where project_id = ?',[projectID]);


    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var extraCharge=ExtraCharge();
      extraCharge.extraChargeId= maps[i]['extra_charge_id'];
      extraCharge.projectId= maps[i]['project_id'];
      extraCharge.extraChargeDetails= maps[i]['extra_charge_details'].toString();
      extraCharge.extraChargeCost= maps[i]['extra_charge_cost'];
      extraCharge.extraChargeDate= maps[i]['extra_charge_date'];

      return extraCharge;
    });
  }
  Future<void> updateExtraCharge(ExtraCharge extraCharge) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    try{
      // Update the given Dog.
      await db.rawUpdate(
          'UPDATE extra_charge SET extra_charge_details = ?, extra_charge_cost = ? WHERE extra_charge_id = ? AND project_id = ?',
          [extraCharge.extraChargeDetails, extraCharge.extraChargeCost,extraCharge.extraChargeId,extraCharge.projectId]);
    }catch (e) {
      throw ("some arbitrary error");
    }
  }


  Future<void> deleteExtraChargeById(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'extra_charge',
      // Use a `where` clause to delete a specific dog.
      where: 'extra_charge_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
}