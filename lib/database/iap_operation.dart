
import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/IAPPurchaceData.dart';
import 'package:ecommerce/model/UserCredentials.dart';
import 'package:sqflite/sqflite.dart';

import 'DatabaseHelper.dart';

class IAPOperations{
  late IAPOperations iapOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<void> insertIAPdata(IAPPurchaseData purchase) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //iap_purchase_table
    // In this case, replace any previous data.


    // print('printing before insert purchaced  ${purchase.purchase_source} ${purchase.purchase_id} ${purchase.purchase_transaction_date} ${purchase.product_id} ${purchase.purchase_status} ${purchase.user_id}  ${purchase.purchase_date_ending} ${purchase.purchase_date_starting} ${purchase.purchase_verification_data}');
    await db.insert(
      'subscription',
      purchase.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }
  Future<void> updateIAPDataByProductId(IAPPurchaseData purchase) async {
    // Get a reference to the database.

    final db = await dbProvider.database;
    await db.rawQuery('UPDATE subscription SET purchase_id = ?, purchase_status = ?, purchase_transaction_date = ?, purchase_date_starting = ?, purchase_date_ending = ?, purchase_source = ?, purchase_verification_data_or_token = ?,purchase_source = ?, purchase_device = ?,purchase_is_auto_renewing = ?  WHERE product_id = ?',
        [purchase.purchaseId,purchase.purchaseStatus,purchase.purchaseTransactionDate,purchase.purchaseDateStarting,purchase.purchaseDateEnding,purchase.purchaseSource,purchase.purchaseVerificationDataOrToken,purchase.purchaseSource,purchase.purchaseDevice,purchase.purchaseIsAutoRenewing,purchase.productId]);
  }
  Future<void> updateAllIAPDataToExpired(String? purchaseStatus) async {
    // Get a reference to the database.

    final db = await dbProvider.database;
    await db.rawQuery('UPDATE subscription SET purchase_status = ?',[purchaseStatus]);
  }
  Future<void> updateAlliosIAPDataToExpired(String? purchaseStatus) async {
    // Get a reference to the database.

    final db = await dbProvider.database;
    await db.rawQuery('UPDATE subscription SET purchase_status = ?',[purchaseStatus]);
  }

  Future<void> updateIAPDataStatus(IAPPurchaseData purchase) async {
    // Get a reference to the database.

    final db = await dbProvider.database;
    await db.rawQuery('UPDATE subscription SET purchase_status = ? WHERE product_id = ?',[purchase.purchaseStatus,purchase.productId,]);
  }
  Future<void> updateIosIAPDataStatus(IAPPurchaseData purchase) async {
    // Get a reference to the database.

    final db = await dbProvider.database;
    await db.rawQuery('UPDATE subscription SET purchase_status = ? WHERE product_id = ? AND purchase_source = ?',[purchase.purchaseStatus,purchase.productId,'google.com']);
  }
  /*Future<List<IAPPurchaseData>> getAllPurchase() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM subscription');//await db.query('iap_purchase_table');
    // Convert the List<Map<String, dynamic> into a List<Dog>.


    return List. generate(maps.length, (i) {

      var purchases =IAPPurchaseData();
      purchases.iapId= maps[i]['iap_id'];
      purchases.userId= maps[i]['user_id'];
      purchases.productId= maps[i]['product_id'];
      purchases. purchaseId= maps[i]['purchase_id'];
      purchases.purchaseStatus= maps[i]['purchase_status'];
      purchases.purchaseTransactionDate= maps[i]['purchase_transaction_date'];
      purchases.purchaseVerificationDataOrToken= maps[i]['purchase_verification_data_or_token'];
      purchases.purchaseDateStarting= maps[i]['purchase_date_starting'];
      purchases.purchaseSource= maps[i]['purchase_source'];
      purchases.purchaseDateEnding= maps[i]['purchase_date_ending'];
      purchases.purchaseDevice= maps[i]['purchase_device'];
      purchases.purchaseIsAutoRenewing= maps[i]['purchase_is_auto_renewing'];
      // print('purchaced iap_id = ${purchases.iap_id} \nuser_id = ${purchases.user_id}  \purchase_id = ${purchases.purchase_id}  \npurchase_source = ${purchases.purchase_source} \npurchase_transaction_date = ${purchases.purchase_transaction_date} \nproduct_id = ${purchases.product_id} \npurchase_status = ${purchases.purchase_status}  \npurchase_date_ending = ${purchases.purchase_date_ending} \npurchase_date_starting ${purchases.purchase_date_starting} \npurchase_verification_data =  ${purchases.purchase_verification_data}');
      return purchases;
    });
  }*/

  Future<List<IAPPurchaseData>> getAllPurchaseByUserId(int? userID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM subscription WHERE user_id = ?',[userID]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var purchaces =IAPPurchaseData();
      purchaces.iapId= maps[i]['iap_id'];
      purchaces.userId= maps[i]['user_id'];
      purchaces.productId= maps[i]['product_id'];
      purchaces.purchaseId= maps[i]['purchase_id'];
      purchaces.purchaseStatus= maps[i]['purchase_status'];
      purchaces.purchaseTransactionDate= maps[i]['purchase_transaction_date'];
      purchaces.purchaseVerificationDataOrToken= maps[i]['purchase_verification_data_or_token'];
      purchaces.purchaseDateStarting= maps[i]['purchase_date_starting'];
      purchaces.purchaseSource= maps[i]['purchase_source'];
      purchaces.purchaseDateEnding= maps[i]['purchase_date_ending'];
      purchaces.purchaseDevice= maps[i]['purchase_device'];
      return purchaces;
    });
  }

  Future<List<IAPPurchaseData>> getAllPurchaseByUserIdAndProductId(int? userID,String? productId) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM subscription WHERE user_id = ? AND product_id = ?',[userID,productId]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var purchaces =IAPPurchaseData();
      purchaces.iapId= maps[i]['iap_id'];
      purchaces.userId= maps[i]['user_id'];
      purchaces.productId= maps[i]['product_id'];
      purchaces.purchaseId= maps[i]['purchase_id'];
      purchaces.purchaseStatus= maps[i]['purchase_status'];
      purchaces.purchaseTransactionDate= maps[i]['purchase_transaction_date'];
      purchaces.purchaseVerificationDataOrToken= maps[i]['purchase_verification_data_or_token'];
      purchaces.purchaseDateStarting= maps[i]['purchase_date_starting'];
      purchaces.purchaseSource= maps[i]['purchase_source'];
      purchaces.purchaseDateEnding= maps[i]['purchase_date_ending'];
      purchaces.purchaseDevice= maps[i]['purchase_device'];
      return purchaces;
    });
  }


  Future<List<IAPPurchaseData>> getAllPurchase() async
  {
    // Get a reference to the database.
    final db =  await dbProvider.database;

    //final List<Map<String, dynamic>> maps = await db.query('subscription', where: 'purchase_source = ?', whereArgs: ['google.com'],);
    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM subscription');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var purchaces=IAPPurchaseData();
      purchaces.iapId= maps[i]['iap_id'];
      purchaces.userId= maps[i]['user_id'];
      purchaces.productId= maps[i]['product_id'];
      purchaces.purchaseId= maps[i]['purchase_id'];
      purchaces.purchaseStatus= maps[i]['purchase_status'];
      purchaces.purchaseTransactionDate= maps[i]['purchase_transaction_date'];
      purchaces.purchaseVerificationDataOrToken= maps[i]['purchase_verification_data_or_token'];
      purchaces.purchaseDateStarting= maps[i]['purchase_date_starting'];
      purchaces.purchaseSource= maps[i]['purchase_source'];
      purchaces.purchaseDateEnding= maps[i]['purchase_date_ending'];
      purchaces.purchaseDevice= maps[i]['purchase_device'];
      return purchaces;
    });
  }
  // Future<List<IAPPurchaseData>> getAllPurchaseByiOS() async
  // {
  //   // Get a reference to the database.
  //   final db =  await dbProvider.database;
  //
  //   final List<Map<String, dynamic>> maps = await db.query('subscription', where: 'purchase_source = ?', whereArgs: ['apple.com'],);
  //
  //   // Convert the List<Map<String, dynamic> into a List<Dog>.
  //   return List.generate(maps.length, (i) {
  //     var purchaces=IAPPurchaseData();
  //     purchaces.iapId= maps[i]['iap_id'];
  //     purchaces.userId= maps[i]['user_id'];
  //     purchaces.productId= maps[i]['product_id'];
  //     purchaces.purchaseId= maps[i]['purchase_id'];
  //     purchaces.purchaseStatus= maps[i]['purchase_status'];
  //     purchaces.purchaseTransactionDate= maps[i]['purchase_transaction_date'];
  //     purchaces.purchaseVerificationDataOrToken= maps[i]['purchase_verification_data_or_token'];
  //     purchaces.purchaseDateStarting= maps[i]['purchase_date_starting'];
  //     purchaces.purchaseSource= maps[i]['purchase_source'];
  //     purchaces.purchaseDateEnding= maps[i]['purchase_date_ending'];
  //     purchaces.purchaseDevice= maps[i]['purchase_device'];
  //     return purchaces;
  //   });
  // }

  Future<void> updatePurchaseStatus(IAPPurchaseData purchase) async
  {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Update the given Dog.
    await db.rawQuery('UPDATE subscription SET purchase_status = ?, purchase_transaction_date = ?, purchase_verification_data = ?, purchase_date_starting = ?, purchase_date_ending = ?, purchase_source = ?  WHERE product_id = ?',
        [purchase.purchaseStatus,purchase.purchaseTransactionDate,purchase.purchaseVerificationDataOrToken,purchase.purchaseDateStarting,purchase.purchaseDateEnding,purchase.productId,purchase.purchaseSource]);
  }


  Future<bool> productIDExist(String? productId) async
  {
    final db =  await dbProvider.database;
    var result =await db.rawQuery('SELECT EXISTS(SELECT 1 FROM subscription WHERE product_id = ?)', [productId]);

    int? exists = Sqflite.firstIntValue(result);
    return exists == 1;
  }

  Future<bool> isProductIDExist(IAPPurchaseData purchase) async
  {
    final db =  await dbProvider.database;
    var result =await db.rawQuery('SELECT EXISTS(SELECT 1 FROM subscription WHERE product_id = ?)', [purchase.productId]);

    int? exists = Sqflite.firstIntValue(result);
    return exists == 1;
  }

  Future<bool> isProductIDExistForSource(IAPPurchaseData purchase) async
  {
    final db =  await dbProvider.database;
    var result =await db.rawQuery('SELECT EXISTS(SELECT 1 FROM subscription WHERE product_id = ?)', [purchase.productId]);

    int? exists = Sqflite.firstIntValue(result);
    return exists == 1;
  }
  Future<bool> productExpired(String? productId) async
  {
    final db =  await dbProvider.database;
    var result =await db.rawQuery('SELECT EXISTS(SELECT 1 FROM subscription WHERE product_id = ?)', [productId]);

    int? exists = Sqflite.firstIntValue(result);
    /*result.forEach((element) {
      print('result purchaseIdExist is $element');
    });*/
    return exists == 1;
  }

  Future<bool> isSubscriptionPlanActive(String? productId) async
  {
    final db =  await dbProvider.database;
    var result =await db.rawQuery('SELECT EXISTS(SELECT 1 FROM subscription WHERE product_id = ? AND purchase_status = ?)', [productId,'purchased',]);

    int? exists = Sqflite.firstIntValue(result);
    print('result purchaseIdExist is $exists  $productId');
    return exists == 1;
  }

  // Future<bool> isIOSSubscriptionPlanActive(String? productId) async
  // {
  //   final db =  await dbProvider.database;
  //   var result =await db.rawQuery('SELECT EXISTS(SELECT 1 FROM subscription WHERE product_id = ? AND purchase_status = ? AND purchase_source = ?)', [productId,'purchased','apple.com']);
  //
  //   int? exists = Sqflite.firstIntValue(result);
  //   result.forEach((element) {
  //     print('result purchaseIdExist is $element');
  //   });
  //   return exists == 1;
  // }
}