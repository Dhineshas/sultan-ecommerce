import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/InventoryItem.dart';
import 'package:ecommerce/model/InventoryRelation.dart';
import 'package:ecommerce/model/ProductItem.dart';
import 'package:flutter/cupertino.dart';
import 'package:sqflite/sqflite.dart';

class RelationOperations{
  late RelationOperations relationOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<void> insertRelation(InventoryRelation relation) async {
    // Get a reference to the database.

    final db = await dbProvider.database;
try{
    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'relation_product_inventory',
      relation.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }catch (e) {
  throw ("some arbitrary error");
  }
  }
  // A method that retrieves all the dogs from the dogs table.
  Future<List<InventoryRelation>> relationsItems() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('relation_product_inventory');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var inventoryRelation=InventoryRelation();
      inventoryRelation.id= maps[i]['relation_id'];
      inventoryRelation.productItemId= maps[i]['product_item_id'];
      inventoryRelation.inventoryItemId= maps[i]['inventory_item_id'];
      inventoryRelation.inventoryItemQuantity= maps[i]['inventory_item_quantity'];
      return inventoryRelation;
    });
  }

  Future<List<InventoryRelation>> inventoriesItemsByProductId(InventoryRelation relation) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.query('relation_product_inventory',// Use a `where` clause to delete a specific dog.
      where: 'product_item_id = ?',

      whereArgs: [relation.productItemId],);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var relation=InventoryRelation();
      relation.id= maps[i]['relation_id'];
      relation.productItemId= maps[i]['product_item_id'];
      relation.inventoryItemId= maps[i]['inventory_item_id'];
      relation.inventoryItemQuantity= maps[i]['inventory_item_quantity'];
      return relation;
    });
  }

  Future<void> updateRelationByProductItemId(InventoryRelation relation) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Update the given Dog.
    await db.update(
      'relation_product_inventory',
      relation.toMap(),
      // Ensure that the Dog has a matching id.
      where: 'product_item_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [relation.productItemId],
    );
  }

  Future<void> deleteRelation(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'relation_product_inventory',
      // Use a `where` clause to delete a specific dog.
      where: 'relation_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }

  Future<void> deleteRelationByInventoryItemId(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'relation_product_inventory',
      // Use a `where` clause to delete a specific dog.
      where: 'inventory_item_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
  Future<void> deleteRelationByProductItemId(int? id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;
try{
    // Remove the Dog from the database.
    await db.delete(
      'relation_product_inventory',
      // Use a `where` clause to delete a specific dog.
      where: 'product_item_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }catch (e) {
  throw ("some arbitrary error");
  }
  }

  Future<void> deleteRelationByProductItemIdAndInventoryItemId(int? productItemId,int inventoryItemId) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'relation_product_inventory',
      // Use a `where` clause to delete a specific dog.
      where: 'product_item_id = ? AND inventory_item_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [productItemId,inventoryItemId],
    );



  }

  Future<void> deleteRelationByProductId(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    await db.execute(
      'DELETE FROM relation_product_inventory where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id = ?)',[id]
    );
  }

  Future<void> deleteRelationByProjectId(int projectID) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;
 await db.execute('DELETE FROM relation_product_inventory where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id IN (SELECT product_category_id FROM product_category WHERE project_id=?))' ,[projectID]);


    // Convert the List<Map<String, dynamic> into a List<Dog>.

  }


  Future<List<InventoryItem>> selectedProductInventoryItems() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('inventory_item');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var inventoryItem=InventoryItem();
      inventoryItem.id= maps[i]['inventory_item_id'];
      inventoryItem.name= maps[i]['inventory_item_name'];
      inventoryItem.imageBase= maps[i]['inventory_item_image'];
      inventoryItem.itemType= maps[i]['inventory_item_type'];
      inventoryItem.totalType= maps[i]['inventory_item_total_box'];
      inventoryItem.piecesInType= maps[i]['inventory_item_pieces_in_box'];
      inventoryItem.costPerTypeNew= maps[i]['inventory_item_cost_per_box'];
      inventoryItem.inventoryId= maps[i]['inventory_id'];

      return inventoryItem;
    });
  }

  Future<List<int>>relationItemFromProductRelationByInventoryItemId(int? inventoryItemId) async {
    // Get a reference to the database.
try{
    final db = await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.rawQuery(
        'SELECT DISTINCT *  FROM relation_product_inventory WHERE inventory_item_id = ?'
        ,[inventoryItemId]);
    return List.generate(maps.length, (i) {
      return maps[i]['product_item_id'];
    });
  }catch (e) {
  throw ("some arbitrary error");
  }
  }

  Future<int?>relationItemCountFromProductRelationByInventoryItemId(int? inventoryItemId) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    var result = await db.rawQuery(
        'SELECT COUNT(*) FROM relation_product_inventory WHERE inventory_item_id = ?'
        ,[inventoryItemId]);
    int? count = Sqflite.firstIntValue(result);
    return count;
  }

  Future<int?> relationItemCountFromProductRelationByInventoryId(int inventoryId) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    var result = await db.rawQuery('''
    SELECT COUNT(*) 
    FROM 
    relation_product_inventory pr
    INNER JOIN inventory_item ii ON pr.inventory_item_id=ii.inventory_item_id
    INNER JOIN inventory_category ic ON ic.inventory_id=ii.inventory_id
    WHERE
    ii.inventory_id=?
    ''',[inventoryId]);
    int? count = Sqflite.firstIntValue(result);
    return count;

  }

}