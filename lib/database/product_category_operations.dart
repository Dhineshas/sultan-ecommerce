import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/ProductCategory.dart';
import 'package:sqflite/sqflite.dart';

class ProductCategoryOperations {
  late ProductCategoryOperations productCategoryOperations;
  final dbProvider = DatabaseHelper();

  // Define a function that inserts dogs into the database
  Future<void> insertProductCategory(ProductCategory productCategory) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'product_category',
      productCategory.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  // A method that retrieves all the dogs from the dogs table.
  Future<List<ProductCategory>> productCategories() async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('product_category');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productCategory = ProductCategory();
      productCategory.id = maps[i]['product_category_id'];
      productCategory.projectId = maps[i]['project_id'];
      productCategory.name = maps[i]['product_category_name'];
      productCategory.imageBase = maps[i]['product_category_image'];

      return productCategory;
    });
  }

  Future<void> updateProductCategory(ProductCategory productCategory) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Update the given Dog.
    await db.update(
      'product_category',
      productCategory.toMap(),
      // Ensure that the Dog has a matching id.
      where: 'product_category_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [productCategory.id],
    );
  }

  Future<void> updateRawProductCategory(ProductCategory productCategory) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    await db.rawUpdate(
        'UPDATE product_category SET product_category_name = ? WHERE product_category_id = ?',
        [
          productCategory.name,
          productCategory.id
        ]);
  }

  Future<void> deleteProductCategory(int id) async {
    // Get a reference to the database.
    final db = await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'product_category',
      // Use a `where` clause to delete a specific dog.
      where: 'product_category_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }

  Future<void> deleteProductCategoryByProjectId(int? projectID) async {
    // Get a reference to the database.
    final db = await dbProvider.database;

    await db.execute(
        'DELETE FROM product_category WHERE project_id = ?', [projectID]);
  }

  Future<List<ProductCategory>> productCategoriesByProjectWithProductItemCount(
      int? projectID) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.rawQuery(
        'SELECT pc.product_category_id, pc.project_id, pc.product_category_name, pc.product_category_image, count(pi.product_item_id) AS product_item_count FROM product_category pc LEFT OUTER JOIN product_item pi ON (pi.product_category_id = pc.product_category_id) WHERE pc.project_id= ? GROUP BY pc.product_category_id, pc.project_id, pc.product_category_name, pc.product_category_image',
        [projectID]);

    // final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT pc.product_category_id, pc.project_id, pc.product_category_name, pc.product_category_image, count(pi.product_item_id) AS product_item_count FROM product_category pc LEFT OUTER JOIN product_item pi ON (pi.product_category_id = pc.product_category_id) GROUP BY pc.product_category_id, pc.product_category_name, pc.product_category_image');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productCategory = ProductCategory();
      productCategory.id = maps[i]['product_category_id'];
      productCategory.projectId = maps[i]['project_id'];
      productCategory.name = maps[i]['product_category_name'];
      productCategory.imageBase = maps[i]['product_category_image'];
      productCategory.productItemCount = maps[i]['product_item_count'];

      return productCategory;
    });
  }

  Future<List<ProductCategory>> productCategoriesByProjectWithNullProductItemIdAndProductItemCount(
      int? projectID) async {
    // Get a reference to the database.

    final db = await dbProvider.database;
    List<Map<String, dynamic>> maps=[];
    final itemCountQuery =await db.rawQuery('''
     SELECT count(*) AS product_count FROM product_category pc  WHERE pc.project_id = ?
    ''',[projectID]);
    int? itemCount=int.tryParse(itemCountQuery.first['product_count'].toString());

    if(itemCount!=null&&itemCount>0){
      maps = await db.rawQuery('''
           
     SELECT pc.product_category_id AS id, pc.project_id AS project_id, pc.product_category_name AS name, pc.product_category_image AS image, count(pi.product_item_id) AS product_item_count, 0.0 AS cost, 0.0 AS selling_price, -1 AS is_inventory_item_updated, -1 AS has_category FROM product_category pc LEFT JOIN product_item pi ON (pi.product_category_id = pc.product_category_id) WHERE pc.project_id= ? GROUP BY  pc.product_category_id, pc.product_category_name,  pc.project_id
    UNION ALL
    SELECT pi.product_item_id AS id, pi.project_id AS project_id, pi.product_item_name AS name,pi.product_item_image AS image,0 AS product_item_count, pi.product_item_cost as cost, pi.product_item_selling_price AS selling_price, pi.product_item_is_inventory_item_updated AS is_inventory_item_updated, pi.product_item_has_category AS has_category FROM product_item pi WHERE pi.product_category_id IS NULL AND pi.project_id = ?
    ''',[projectID,projectID]);
    }else{
      maps = await db.rawQuery('''
    SELECT pi.product_item_id AS id, pi.project_id AS project_id, pi.product_item_name AS name,pi.product_item_image AS image,0 AS product_item_count, pi.product_item_cost as cost, pi.product_item_selling_price AS selling_price, pi.product_item_is_inventory_item_updated AS is_inventory_item_updated, pi.product_item_has_category AS has_category FROM product_item pi WHERE pi.product_category_id IS NULL AND pi.project_id = ?
''',[projectID]);
    }
    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productCategory = ProductCategory();
      productCategory.id = maps[i]['id'];
      productCategory.projectId = maps[i]['project_id'];
      productCategory.name = maps[i]['name'].toString();
      productCategory.imageBase = maps[i]['image'];
      productCategory.productItemCount = maps[i]['product_item_count'];
      productCategory.hasCategory = maps[i]['has_category'];
      productCategory.cost = maps[i]['cost'];
      productCategory.sellingPrice = maps[i]['selling_price'];
      productCategory.isInventoryItemUpdated = maps[i]['is_inventory_item_updated'];

      return productCategory;
    });
  }

  Future<void> deleteAllDataAndProductCategory(int? id) async {
    final db = await dbProvider.database;
    try{
      await db.transaction((txn) async{

        var batch =  txn.batch();
        // 1st delete all product category related data from relation table on product category delete
        batch.rawDelete('DELETE FROM relation_product_inventory where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id = ?)', [id]);
        // 2nd delete all product category related data from worker relation table on product category delete
        batch.rawDelete('DELETE FROM relation_product_workers where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id = ?)', [id]);
        // 3rd delete all product category related data from cart relation table on product category delete
        batch.rawDelete('DELETE FROM relation_cart_product where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id = ?)', [id]);
        // 4th delete all product category related data from orders table on product category delete
        batch.rawDelete('DELETE FROM orders WHERE product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id = ?)', [id]);
        // 5th delete all product category related data from storage table on product category delete
        batch.rawDelete('DELETE FROM storage where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id = ?)', [id]);
        //6th delete all product items related with product category on product category delete
        batch.rawDelete('DELETE FROM product_item where product_category_id = ?', [id]);
        // 8th then delete product category
        batch.rawDelete('DELETE FROM product_category where product_category_id = ?', [id]);
        await batch.commit(noResult: true);
      });}catch (e) {
        throw ("some arbitrary error");
      }
  }

  Future<List<ProductCategory>> productCategoriesByProjectIdWithoutSelectedCategory(
      int? projectID,List<int> productCategoryIdArray) async {
    // Get a reference to the database.

    final db = await dbProvider.database;
    final List<Map<String, dynamic>> maps = await db.rawQuery(
        'SELECT pc.product_category_id, pc.project_id, pc.product_category_name, pc.product_category_image FROM product_category pc WHERE pc.project_id= ? AND pc.product_category_id NOT IN (${productCategoryIdArray.join(', ')}) GROUP BY pc.product_category_id, pc.project_id, pc.product_category_name, pc.product_category_image',
        [projectID]);

    // final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT pc.product_category_id, pc.project_id, pc.product_category_name, pc.product_category_image, count(pi.product_item_id) AS product_item_count FROM product_category pc LEFT OUTER JOIN product_item pi ON (pi.product_category_id = pc.product_category_id) GROUP BY pc.product_category_id, pc.product_category_name, pc.product_category_image');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productCategory = ProductCategory();
      productCategory.id = maps[i]['product_category_id'];
      productCategory.projectId = maps[i]['project_id'];
      productCategory.name = maps[i]['product_category_name'];
      productCategory.imageBase = maps[i]['product_category_image'];
      productCategory.productItemCount = maps[i]['product_item_count'];

      return productCategory;
    });
  }
}
