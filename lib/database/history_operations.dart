import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/inventoryUnitMeasures.dart';
import 'package:ecommerce/model/History.dart';
import 'package:ecommerce/model/InventoryItem.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:sqflite/sqflite.dart';

class HistoryOperations{
  late HistoryOperations storageOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<void> insertHistory(History history) async {
    // Get a reference to the database.

    final db = await dbProvider.database;
try{
    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'history',
      history.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }catch (e) {
  throw ("some arbitrary error");
  }
  }

  // A method that retrieves all the dogs from the dogs table.
  Future<List<History>> history() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('history');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var history=History();
      history.id= maps[i]['history_id'];
      history.historyDate= maps[i]['history_date'];
      history.inventoryItemId= maps[i]['inventory_item_id'];
      history.historyItemType= maps[i]['history_inventory_item_type'];
      history.historyTotalType= maps[i]['history_inventory_item_total_type'];
      history.historyPiecesInType= maps[i]['history_inventory_item_pieces_in_type'];
      history.historyCostPerType= maps[i]['history_inventory_item_total_cost'];
      history.historyCostPerItemInType= maps[i]['history_inventory_item_cost_per_item_in_type_new'];
      history.historyStock= maps[i]['history_inventory_item_stock'];

      return history;
    });
  }


  /*Future<void> updateHistory(History history) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Update the given Dog.
    await db.update(
      'history',
      history.toMap(),
      // Ensure that the Dog has a matching id.
      where: 'history_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [history.id],
    );
  }*/
  Future<void> deleteHistory(int historyId) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'history',
      // Use a `where` clause to delete a specific dog.
      where: 'history_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [historyId],
    );
  }
  Future<void> clearHistory() async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'history',
    );
  }

  Future<void> deleteHistoryItemByInventoryItemId(int inventoryItemId) async {
    // Get a reference to the database.
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'history',
      // Use a `where` clause to delete a specific dog.
      where: 'inventory_item_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [inventoryItemId],
    );
  }
  Future<void> deleteHistoryItemByInventoryId(int inventoryId) async {
    final db =  await dbProvider.database;

    await db.execute(
        'DELETE FROM history where inventory_item_id IN (SELECT inventory_item_id from inventory_item WHERE inventory_id = ?)',[inventoryId]
    );
  }
  Future<void> deleteHistoryItemByProjectId(int? projectID) async {
    // Get a reference to the database.
    final db =  await dbProvider.database;

    await db.execute('DELETE FROM history where inventory_item_id IN (SELECT inventory_item_id from inventory_item WHERE inventory_id IN (SELECT inventory_id FROM inventory_category WHERE project_id=?))' ,[projectID]);

  }
  Future<void> updateHistory(History history) async {
    // Get a reference to the database.
    final db =  await dbProvider.database;
try{
    await db.rawUpdate('UPDATE history SET history_inventory_item_total_type = ?, history_inventory_item_pieces_in_type= ?, history_inventory_item_cost_per_type = ?, history_inventory_item_cost_per_item_in_type_new =? WHERE history_id = ?',
        [history.historyTotalType,history.historyPiecesInType,history.historyCostPerType,history.historyCostPerItemInType, history.id]);
  }catch (e) {

  throw ("some arbitrary error");
  }
  }

  Future<void> updateHistoryWithTransactionAndAddHistory(InventoryItem inventoryItem,History history) async {
    final db = await dbProvider.database;
    try{
      await db.transaction((txn) async{

        var batch =  txn.batch();

        await txn.rawUpdate('UPDATE history SET history_inventory_item_total_type = ?, history_inventory_item_pieces_in_type= ?, history_inventory_item_cost_per_type = ?, history_inventory_item_cost_per_item_in_type_new =?,history_inventory_item_stock =? WHERE history_id = ?',
            [history.historyTotalType,history.historyPiecesInType,history.historyCostPerType,history.historyCostPerItemInType,history.historyStock, history.id]);



        List<Map<String, dynamic>> inventoryItems2 = await txn.rawQuery('SELECT DISTINCT *, MAX(hs.history_inventory_item_cost_per_item_in_type_new) FROM inventory_item ii INNER JOIN history hs ON (hs.inventory_item_id = ? AND ii.inventory_item_id = hs.inventory_item_id)',[inventoryItem.id]);


        inventoryItem.costPerTypeNew = inventoryItems2.first['history_inventory_item_cost_per_type'];
        inventoryItem.costPerItemInTypeNew =inventoryItems2.first['history_inventory_item_cost_per_item_in_type_new'];

        await txn.update(
          'inventory_item',
          inventoryItem.toMap(),
          // Ensure that the Dog has a matching id.
          where: 'inventory_item_id = ?',
          // Pass the Dog's id as a whereArg to prevent SQL injection.
          whereArgs: [inventoryItem.id],
        );


        List<Map<String, dynamic>> productItemIds = await txn.rawQuery(
            'SELECT DISTINCT product_item_id  FROM relation_product_inventory WHERE inventory_item_id = ?'
            ,[inventoryItem.id]);


        final productItemIdArray = productItemIds.map((e) => e['product_item_id']).toList();
        print('associatedProductID  $productItemIds       $productItemIdArray');


        final List<Map<String, dynamic>> toUpdateProductIdArray =  await txn.rawQuery('''
    SELECT product_item_id,product_item_price_by_percentage,SUM(printf("%.2f",cost)) as totalProductCost FROM 
    (
    SELECT 
      p.product_item_id as product_item_id,product_item_price_by_percentage,
      (w.worker_wage_per_min_new)*(pwr.work_duration*1.0) as cost
      FROM
      product_item p
      LEFT JOIN relation_product_workers pwr ON pwr.product_item_id=p.product_item_id
      LEFT JOIN workers w
      ON w.worker_id=pwr.worker_id
      WHERE p.product_item_id IN (${productItemIdArray.join(', ')})
      GROUP BY p.product_item_id
      UNION ALL
    SELECT 
      p.product_item_id,product_item_price_by_percentage,
      SUM(
          pir.inventory_item_quantity * (inventory_item_cost_per_item_in_type_new)
      ) AS cost
      FROM
      product_item p
      INNER JOIN relation_product_inventory pir ON p.product_item_id = pir.product_item_id
      INNER JOIN inventory_item inv ON inv.inventory_item_id = pir.inventory_item_id
      WHERE
      p.product_item_id IN (${productItemIdArray.join(', ')})
      GROUP BY p.product_item_id
   ) GROUP BY product_item_id
    
    ''');


        toUpdateProductIdArray.forEach((e)async {
          if((e['product_item_price_by_percentage'])>0){
            //custom round off and update product price
            final double value=(e['totalProductCost']+((e['totalProductCost']*e['product_item_price_by_percentage'])/100))!;
            final double roundOffPrice= value.toString().toCustomDoubleRoundOff;
            batch.rawUpdate('UPDATE product_item SET product_item_cost = ?,product_item_selling_price= ? WHERE product_item_id = ?',[e['totalProductCost'],roundOffPrice,e['product_item_id']]);
          }else
            batch.rawUpdate('UPDATE product_item SET product_item_cost = ?,product_item_is_inventory_item_updated = 1 WHERE product_item_id = ?',[e['totalProductCost'],e['product_item_id']]);

        });

        await batch.commit(noResult: true);
      });}catch (e) {

      throw ("some arbitrary error");
    }
  }


}