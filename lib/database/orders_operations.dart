import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/Orders.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:sqflite/sqflite.dart';

import '../model/ProductItem.dart';

class OrderOperations {
  late OrderOperations cartOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<void> insertOrder(Orders order) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    try{
    await db.insert(
      'orders',
      order.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    }catch (e) {
      throw ("some arbitrary error");
    }
  }

  // A method that retrieves all the dogs from the dogs table.
  Future<List<Orders>> orders() async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('orders');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var order = Orders();
      order.id = maps[i]['order_id'];
      order.orderCreated = maps[i]['order_created'];
      order.productItemId = maps[i]['product_item_id'];
      order.quantity = maps[i]['order_quantity'];
      order.orderStatus = maps[i]['order_status'];

      return order;
    });
  }

  Future<void> updateCart(Orders order) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    // Update the given Dog.
    await db.update(
      'orders',
      order.toMap(),
      // Ensure that the Dog has a matching id.
      where: 'order_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [order.id],
    );
  }
  Future<bool> isProductItemIdExist(int? id) async {
    final db =  await dbProvider.database;

    var result =await db.rawQuery('SELECT EXISTS(SELECT 1 FROM orders WHERE product_item_id=?)',
        [id]);

    int? exists = Sqflite.firstIntValue(result);
    return exists == 1;
  }
  Future<void> deleteOrderItemByProductItemId(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'orders',
      // Use a `where` clause to delete a specific dog.
      where: 'product_item_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }

  Future<bool> isOrderNewExistByProjectId(int? projectID) async {
    // Get a reference to the database.
    final db =  await dbProvider.database;
    var result =await db.rawQuery('''SELECT EXISTS(SELECT 1
    FROM orders od 
    INNER JOIN product_item pi ON (od.product_item_id = pi.product_item_id)
   INNER JOIN product_category pc ON (pc.product_category_id = pi.product_category_id) WHERE od.order_status = ? AND pc.project_id = ?)''',
        [0,projectID]);

    int? exists = Sqflite.firstIntValue(result);
    return exists == 1;
  }

  Future<bool> isOrderNewExistByProductId(int? productID) async {
    // Get a reference to the database.
    final db =  await dbProvider.database;
    var result =await db.rawQuery('''SELECT EXISTS(SELECT 1
    FROM orders od 
    INNER JOIN product_item pi ON (od.product_item_id = pi.product_item_id)
   INNER JOIN product_category pc ON (pc.product_category_id = pi.product_category_id) WHERE od.order_status = ? AND pc.product_category_id = ?)''',
        [0,productID]);

    int? exists = Sqflite.firstIntValue(result);
    return exists == 1;
  }

  Future<bool> isOrderNewExistByProductItemId(int? productItemID) async {
    // Get a reference to the database.
    final db =  await dbProvider.database;
    var result =await db.rawQuery('''SELECT EXISTS(SELECT 1
    FROM orders od 
    INNER JOIN product_item pi ON (od.product_item_id = pi.product_item_id)
   WHERE od.order_status = ? AND pi.product_item_id = ?)''',
        [0,productItemID]);

    int? exists = Sqflite.firstIntValue(result);
    return exists == 1;
  }

  Future<void> deleteOrderItemByProductId(int id) async {
    final db =  await dbProvider.database;

    await db.execute(
        'DELETE FROM orders WHERE product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id = ?)',[id]
    );
  }
  Future<void> deleteOrderItemByProjectId(int projectID) async {
    final db =  await dbProvider.database;


    await db.execute('DELETE FROM orders WHERE product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id IN (SELECT product_category_id FROM product_category WHERE project_id=?))' ,[projectID]);

  }
  Future<void> updateOrderStatus(Orders order) async {
    // Get a reference to the database.
    final db =  await dbProvider.database;

    await db.rawUpdate('UPDATE orders SET order_status = ? WHERE order_id = ?',
        [order.orderStatus, order.id]);
  }

  Future<List<ProductItem>> getInvoiceAll(int? projectID,int orderCreated) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.rawQuery('''SELECT * 
    FROM orders od 
    INNER JOIN product_item pi ON (od.product_item_id = pi.product_item_id) WHERE pi.project_id = ? AND od.order_created = ? AND order_status != ?
    ''',[projectID,orderCreated,1]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productItem=ProductItem();
      productItem.id= maps[i]['product_item_id'];
      productItem.name= maps[i]['product_item_name'].toString();
      productItem.cost= maps[i]['product_item_cost'];
      productItem.sellingPrice= maps[i]['product_item_selling_price'];
      productItem.priceByPercentage= maps[i]['product_item_price_by_percentage'];
      productItem.isInventoryItemUpdated= maps[i]['product_item_is_inventory_item_updated'];
      productItem.orderDeliveryCharge= maps[i]['order_delivery_charge'];
      productItem.orderDiscount= maps[i]['order_discount'];
      productItem.orderGrossTotal= maps[i]['order_gross_total'];
      productItem.orderNetTotal= maps[i]['order_net_total'];
      productItem.orderQuantity= maps[i]['order_quantity'];
      productItem.createdAt= maps[i]['order_created'];
      productItem.orderStatus= maps[i]['order_status'];
      productItem.orderId= maps[i]['order_id'];
      productItem.projectId= maps[i]['project_id'];
      productItem.hasCategory= maps[i]['product_item_has_category'];
      return productItem;
    });
  }

  Future<List<ProductItem>> getInvoiceSingleItem(int? projectID,int orderID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.rawQuery('''SELECT * 
    FROM orders od 
    INNER JOIN product_item pi ON (od.product_item_id = pi.product_item_id) WHERE pi.project_id = ? AND od.order_id = ? AND order_status != ?
    ''',[projectID,orderID,1]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productItem=ProductItem();
      productItem.id= maps[i]['product_item_id'];
      productItem.name= maps[i]['product_item_name'].toString();
      productItem.cost= maps[i]['product_item_cost'];
      productItem.sellingPrice= maps[i]['product_item_selling_price'];
      productItem.priceByPercentage= maps[i]['product_item_price_by_percentage'];
      productItem.isInventoryItemUpdated= maps[i]['product_item_is_inventory_item_updated'];
      productItem.orderDeliveryCharge= maps[i]['order_delivery_charge'];
      productItem.orderDiscount= maps[i]['order_discount'];
      productItem.orderGrossTotal= maps[i]['order_gross_total'];
      productItem.orderNetTotal= maps[i]['order_net_total'];
      productItem.orderQuantity= maps[i]['order_quantity'];
      productItem.createdAt= maps[i]['order_created'];
      productItem.orderStatus= maps[i]['order_status'];
      productItem.orderId= maps[i]['order_id'];
      productItem.projectId= maps[i]['project_id'];
      productItem.hasCategory= maps[i]['product_item_has_category'];
      return productItem;
    });
  }

  Future<List<Orders>> totalProductReport(int? projectID, int selectedYear) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.rawQuery('''SELECT 
    
    CAST(strftime('%Y', od.order_created/1000.0, 'unixepoch') AS INTEGER) AS order_year,
    CAST(strftime('%m', od.order_created/1000.0, 'unixepoch') AS INTEGER) AS order_month,
    SUM(od.order_quantity*(pi.product_item_selling_price-pi.product_item_cost)) AS sum_order_total_product_profit,
    SUM(od.order_quantity*pi.product_item_selling_price) AS sum_order_total_product_price,
    SUM(od.order_quantity*pi.product_item_cost) AS sum_order_total_product_cost,
    od.order_id AS order_id,
    od.order_created AS order_created,
    od.order_quantity AS order_quantity,
    od.order_status AS order_status,
    pi.product_item_id AS product_item_id
    
     
    FROM orders od 
    INNER JOIN product_item pi ON (od.product_item_id = pi.product_item_id) WHERE pi.project_id = ? AND od.order_status = ? AND order_year = ? AND od.product_item_id = pi.product_item_id
     GROUP BY order_year, order_month
    ''',[projectID,2,selectedYear]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var order = Orders();
      order.id = maps[i]['order_id'];
      order.orderCreated = maps[i]['order_created'];
      order.productItemId = maps[i]['product_item_id'];
      order.quantity = maps[i]['order_quantity'];
      order.orderStatus = maps[i]['order_status'];
      order.totalProfit=maps[i]['sum_order_total_product_profit'];
      order.totalSales=maps[i]['sum_order_total_product_price'];
      order.totalCost=maps[i]['sum_order_total_product_cost'];
      order.orderYear=maps[i]['order_year'];
      order.orderMonth=maps[i]['order_month'];

      return order;
    });
  }

}