import 'package:ecommerce/database/DatabaseHelper.dart';
import 'package:ecommerce/model/InventoryItem.dart';
import 'package:ecommerce/model/InventoryRelation.dart';
import 'package:ecommerce/model/ProductCategory.dart';
import 'package:ecommerce/model/ProductItem.dart';
import 'package:ecommerce/model/Worker.dart';
import 'package:ecommerce/model/WorkerRelation.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:sqflite/sqflite.dart';

import '../model/GraphProductItem.dart';

class ProductItemOperations{
  late ProductItemOperations inventoryItemOperations;
  final dbProvider = DatabaseHelper();


  // Define a function that inserts dogs into the database
  Future<List<ProductItem>> insertProductItem(ProductItem productItem) async {
    // Get a reference to the database.

    final db = await dbProvider.database;
try{
    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
     await db.insert(
      'product_item',
      productItem.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );


    final List<Map<String, dynamic>> maps =  await db.rawQuery(
        'select * from product_item where product_item_id = LAST_INSERT_ROWID()'
    );


    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productItem=ProductItem();
      productItem.id= maps[i]['product_item_id'];
      productItem.name= maps[i]['product_item_name'];
      productItem.imageBase= maps[i]['product_item_image'];
      productItem.priceByPercentage= maps[i]['product_item_price_by_percentage'];
      productItem.isInventoryItemUpdated= maps[i]['product_item_is_inventory_item_updated'];
      productItem.productCategoryId= maps[i]['product_category_id'];
      productItem.projectId= maps[i]['project_id'];
      productItem.hasCategory= maps[i]['product_item_has_category'];
      return productItem;
    });
  }catch (e) {
  throw ("some arbitrary error");
  }
  }
  Future<void> insertProductItemWithTxn(ProductItem productItem, List<InventoryItem> inventoryItems, List<Worker> workers) async {
    // Get a reference to the database.

    final db = await dbProvider.database;
    try{
      // Insert the Dog into the correct table. You might also specify the
      // `conflictAlgorithm` to use in case the same dog is inserted twice.
      //
      // In this case, replace any previous data.
      await db.transaction((txn) async{


        final insertedId = await txn.insert(
        'product_item',
        productItem.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
        print('insertIDD: ${insertedId}');
        /*List<Map<String, Object?>> id2 = await txn.rawQuery(
            'select * from product_item where product_item_id = LAST_INSERT_ROWID()');

        print('inserted2: ${id2.first['product_item_id']}');*/



        if(inventoryItems.isNotEmpty){
          inventoryItems.forEach((element)async {
            var relation = InventoryRelation();
            relation.productItemId = insertedId;
            relation.inventoryItemId = element.id;
            relation.inventoryItemQuantity = element.selectedQuantity;
            await txn.insert(
              'relation_product_inventory',
              relation.toMap(),
              conflictAlgorithm: ConflictAlgorithm.ignore,
            );
          });


        }

        if(workers.isNotEmpty){
          workers.forEach((element)async {
            var relation = WorkerRelation();
            relation.productItemId = insertedId;
            relation.workerId = element.id;
            relation.workerHour =element.selectedMins;

             await txn.insert(
                'relation_product_workers',
                relation.toMap(),
                conflictAlgorithm: ConflictAlgorithm.ignore,
              );

          });}

      });
    }catch (e) {
      throw ("some arbitrary error");
    }
  }

  Future<void> updateProductItemWithTxn(ProductItem productItem, List<InventoryItem> inventoryItems, List<Worker> workers) async {
    // Get a reference to the database.

    final db = await dbProvider.database;
    try{
      // Insert the Dog into the correct table. You might also specify the
      // `conflictAlgorithm` to use in case the same dog is inserted twice.
      //
      // In this case, replace any previous data.
      await db.transaction((txn) async{
        var batch =  txn.batch();

        await txn.update(
          'product_item',
          productItem.toMap(),
          // Ensure that the Dog has a matching id.
          where: 'product_item_id = ?',
          // Pass the Dog's id as a whereArg to prevent SQL injection.
          whereArgs: [productItem.id],
        );

        await txn.delete(
          'relation_product_inventory',
          // Use a `where` clause to delete a specific dog.
          where: 'product_item_id = ?',
          // Pass the Dog's id as a whereArg to prevent SQL injection.
          whereArgs: [productItem.id],
        );

        await txn.delete(
          'relation_product_workers',
          // Use a `where` clause to delete a specific dog.
          where: 'product_item_id = ?',
          // Pass the Dog's id as a whereArg to prevent SQL injection.
          whereArgs: [productItem.id],
        );

        if(inventoryItems.isNotEmpty){
          inventoryItems.forEach((element)async {
            var relation = InventoryRelation();
            relation.productItemId = productItem.id;
            relation.inventoryItemId = element.id;
            relation.inventoryItemQuantity = element.selectedQuantity;
            batch.insert(
              'relation_product_inventory',
              relation.toMap(),
              conflictAlgorithm: ConflictAlgorithm.ignore,
            );
          });


        }

        if(workers.isNotEmpty){
          workers.forEach((element)async {
            var relation = WorkerRelation();
            relation.productItemId = productItem.id;
            relation.workerId = element.id;
            relation.workerHour =element.selectedMins;

            batch.insert(
              'relation_product_workers',
              relation.toMap(),
              conflictAlgorithm: ConflictAlgorithm.ignore,
            );

          });}
        await batch.commit(noResult: true);
      });

    }catch (e) {
      throw ("some arbitrary error");
    }
  }

  // A method that retrieves all the dogs from the dogs table.
  Future<List<ProductItem>> productItems() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('product_item');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productItem=ProductItem();
      productItem.id= maps[i]['product_item_id'];
      productItem.name= maps[i]['product_item_name'];
      productItem.imageBase= maps[i]['product_item_image'];
      productItem.cost= maps[i]['product_item_cost'];
      productItem.sellingPrice= maps[i]['product_item_selling_price'];
      productItem.priceByPercentage= maps[i]['product_item_price_by_percentage'];
      productItem.isInventoryItemUpdated= maps[i]['product_item_is_inventory_item_updated'];
      productItem.productCategoryId= maps[i]['product_category_id'];
      productItem.projectId= maps[i]['project_id'];
      productItem.hasCategory= maps[i]['product_item_has_category'];
      return productItem;
    });
  }
  // A method that retrieves all the dogs from the dogs table.
  Future<List<ProductItem>> productItemsWithoutProductItemIdArrayWithCategoryNameByProjectId(List<int> productItemIdArray,int? projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Query the table for all The Dogs.
    //final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM product_item pi INNER JOIN product_category pc ON (pi.product_category_id = pc.product_category_id) WHERE pi.product_item_id NOT IN (${productItemIdArray.join(', ')}) AND pc.project_id = ?',[projectID]);
    final List<Map<String, dynamic>> maps = await db.rawQuery('''SELECT 
    pi.product_item_id AS product_item_id,
    pi.product_item_name AS product_item_name,
    pi.product_item_image AS product_item_image,
    pc.product_category_id AS product_category_id,
    pc.product_category_name AS product_category_name
    
    FROM product_item pi INNER JOIN product_category pc ON (pi.product_category_id = pc.product_category_id) WHERE pi.product_item_id NOT IN (${productItemIdArray.join(', ')}) AND pc.project_id = ?
    
    UNION ALL
    
    SELECT 
    pi.product_item_id AS product_item_id,
    pi.product_item_name AS product_item_name,
    pi.product_item_image AS product_item_image,
    null AS product_category_id,
    'Others' AS product_category_name
    
    FROM product_item pi
    WHERE pi.product_category_id IS NULL AND pi.product_item_id NOT IN (${productItemIdArray.join(', ')}) AND pi.project_id=?
    
      
    ''',[projectID,projectID]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productItem=ProductItem();
      productItem.id= maps[i]['product_item_id'];
      productItem.name= maps[i]['product_item_name'];
      productItem.imageBase= maps[i]['product_item_image'];
      productItem.cost= maps[i]['product_item_cost'];
      productItem.sellingPrice= maps[i]['product_item_selling_price'];
      productItem.priceByPercentage= maps[i]['product_item_price_by_percentage'];
      productItem.isInventoryItemUpdated= maps[i]['product_item_is_inventory_item_updated'];
      productItem.productCategoryId= maps[i]['product_category_id'];
      productItem.productCategoryName= maps[i]['product_category_name'];
      productItem.projectId= maps[i]['project_id'];
      productItem.hasCategory= maps[i]['product_item_has_category'];
      return productItem;
    });
  }
  Future<List<ProductItem>> productItemsByProductCategoryId(ProductCategory productCategory) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.query('product_item',// Use a `where` clause to delete a specific dog.
      where: 'product_category_id = ?',

      whereArgs: [productCategory.id],);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productItem=ProductItem();
      productItem.id= maps[i]['product_item_id'];
      productItem.name= maps[i]['product_item_name'].toString();
      productItem.imageBase= maps[i]['product_item_image'];
      productItem.cost= maps[i]['product_item_cost'];
      productItem.sellingPrice= maps[i]['product_item_selling_price'];
      productItem.priceByPercentage= maps[i]['product_item_price_by_percentage'];
      productItem.isInventoryItemUpdated= maps[i]['product_item_is_inventory_item_updated'];
      productItem.productCategoryId= maps[i]['product_category_id'];
      productItem.projectId= maps[i]['project_id'];
      productItem.hasCategory= maps[i]['product_item_has_category'];
      return productItem;
    });
  }

  Future<List<ProductItem>> productItemsByProductItemId(int? productItemId) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.query('product_item',// Use a `where` clause to delete a specific dog.
      where: 'product_item_id = ?',

      whereArgs: [productItemId],);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productItem=ProductItem();
      productItem.id= maps[i]['product_item_id'];
      productItem.name= maps[i]['product_item_name'].toString();
      productItem.imageBase= maps[i]['product_item_image'];
      productItem.cost= maps[i]['product_item_cost'];
      productItem.sellingPrice= maps[i]['product_item_selling_price'];
      productItem.priceByPercentage= maps[i]['product_item_price_by_percentage'];
      productItem.isInventoryItemUpdated= maps[i]['product_item_is_inventory_item_updated'];
      productItem.productCategoryId= maps[i]['product_category_id'];
      productItem.projectId= maps[i]['project_id'];
      productItem.hasCategory= maps[i]['product_item_has_category'];
      return productItem;
    });
  }

  Future<void> updateProductItem(ProductItem productItem) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;
try{
    // Update the given Dog.
    await db.update(
      'product_item',
      productItem.toMap(),
      // Ensure that the Dog has a matching id.
      where: 'product_item_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [productItem.id],
    );
  }catch (e) {
  throw ("some arbitrary error");
  }
  }
  Future<void> updateRawProductItem(ProductItem productItem) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    await db.rawUpdate('UPDATE product_item SET product_item_name = ? WHERE product_item_id = ?',
        [productItem.name, productItem.id]);
  }
  Future<void> deleteProductItem(int id) async {
    // Get a reference to the database.
    final dbProvider = DatabaseHelper();
    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'product_item',
      // Use a `where` clause to delete a specific dog.
      where: 'product_item_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
  Future<void> deleteProductItemByProductCategoryId(int id) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.delete(
      'product_item',
      // Use a `where` clause to delete a specific dog.
      where: 'product_category_id = ?',
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
  Future<void> deleteProductItemByProjectId(int projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    // Remove the Dog from the database.
    await db.execute('DELETE FROM product_item where product_item_id IN (SELECT product_item_id from product_item WHERE product_category_id IN (SELECT product_category_id FROM product_category WHERE project_id=?))' ,[projectID]);

  }
  Future getLastInsertedId() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    await db.rawQuery(
        'select product_item_id from product_item where product_item_id = LAST_INSERT_ROWID()'
    );
  }

  Future<List<ProductItem>> productItemsInCart() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM cart cr INNER JOIN product_item pi ON (cr.product_item_id = pi.product_item_id)');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productItem=ProductItem();
      productItem.id= maps[i]['product_item_id'];
      productItem.name= maps[i]['product_item_name'];
      productItem.imageBase= maps[i]['product_item_image'];
      productItem.cost= maps[i]['product_item_cost'];
      productItem.sellingPrice= maps[i]['product_item_selling_price'];
      productItem.priceByPercentage= maps[i]['product_item_price_by_percentage'];
      productItem.isInventoryItemUpdated= maps[i]['product_item_is_inventory_item_updated'];
      productItem.productCategoryId= maps[i]['product_category_id'];
      productItem.cartQuantity= maps[i]['cart_quantity'];
      productItem.projectId= maps[i]['project_id'];
      productItem.hasCategory= maps[i]['product_item_has_category'];

      return productItem;
    });

  }

  Future<List<ProductItem>> productItemsInCartWithCartRelation(int projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    /*SELECT
    pi.product_item_id, pi.product_item_name, pi.product_item_image, pi.product_item_cost, pi.product_item_selling_price, pi.product_item_price_by_percentage,
    pi.product_item_is_inventory_item_updated, pi.product_category_id, crn.cart_id, crn.cart_delivery_charge, crn.cart_discount, cpr.cart_quantity, cpr.cart_relation_id,

    SUM(pi.product_item_selling_price) AS cart_gross_total,
    SUM(pi.product_item_selling_price)+(crn.cart_delivery_charge)-(crn.cart_discount) AS cart_net_total
    FROM cart crn
    INNER JOIN relation_cart_product cpr ON (crn.cart_id = cpr.cart_id)
    INNER JOIN product_item pi ON (cpr.product_item_id = pi.product_item_id)*/


    final List<Map<String, dynamic>> maps = await db.rawQuery('''SELECT *
    FROM cart cr
    INNER JOIN relation_cart_product cpr ON (cr.cart_id = cpr.cart_id)
    INNER JOIN product_item pi ON (cpr.product_item_id = pi.product_item_id) WHERE cr.project_id = ?''',[projectID]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.

    return List.generate(maps.length, (i) {
      var productItem=ProductItem();
      productItem.id= maps[i]['product_item_id'];
      productItem.name= maps[i]['product_item_name'].toString();
      productItem.imageBase= maps[i]['product_item_image'];
      productItem.cost= maps[i]['product_item_cost'];
      productItem.sellingPrice= maps[i]['product_item_selling_price'];
      productItem.priceByPercentage= maps[i]['product_item_price_by_percentage'];
      productItem.isInventoryItemUpdated= maps[i]['product_item_is_inventory_item_updated'];
      productItem.productCategoryId= maps[i]['product_category_id'];
      productItem.cartQuantity= maps[i]['cart_quantity'];
      productItem.cartId= maps[i]['cart_id'];
      productItem.cartDeliverCharge= maps[i]['cart_delivery_charge'];
      productItem.cartDiscount= maps[i]['cart_discount'];
      productItem.cartRelationId= maps[i]['cart_relation_id'];
      /*productItem.cartGrossTotal= maps[i]['cart_gross_total']==null?0.0:maps[i]['cart_gross_total'];
      productItem.cartNetTotal= maps[i]['cart_net_total']==null?0.0:maps[i]['cart_net_total'];*/
      productItem.projectId= maps[i]['project_id'];
      productItem.hasCategory= maps[i]['product_item_has_category'];


      return productItem;
    });

  }

  Future<List<ProductItem>> productItemsInStorage() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM storage st INNER JOIN product_item pi ON (st.product_item_id = pi.product_item_id)');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productItem=ProductItem();
      productItem.id= maps[i]['product_item_id'];
      productItem.name= maps[i]['product_item_name'];
      productItem.imageBase= maps[i]['product_item_image'];
      productItem.cost= maps[i]['product_item_cost'];
      productItem.sellingPrice= maps[i]['product_item_selling_price'];
      productItem.priceByPercentage= maps[i]['product_item_price_by_percentage'];
      productItem.isInventoryItemUpdated= maps[i]['product_item_is_inventory_item_updated'];
      productItem.productCategoryId= maps[i]['product_category_id'];
      productItem.storageQuantity= maps[i]['quantity'];
      productItem.projectId= maps[i]['project_id'];
      productItem.hasCategory= maps[i]['product_item_has_category'];

      return productItem;
    });
  }
  Future<List<ProductItem>> productItemsInStorageByProjectId(int? projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.rawQuery('''SELECT * 
    FROM storage sg 
    INNER JOIN product_item pi ON (sg.product_item_id = pi.product_item_id)
    INNER JOIN product_category pc ON (pc.product_category_id = pi.product_category_id) WHERE pc.project_id = ?
    ''',[projectID]);
    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productItem=ProductItem();
      productItem.id= maps[i]['product_item_id'];
      productItem.name= maps[i]['product_item_name'];
      productItem.imageBase= maps[i]['product_item_image'];
      productItem.cost= maps[i]['product_item_cost'];
      productItem.sellingPrice= maps[i]['product_item_selling_price'];
      productItem.priceByPercentage= maps[i]['product_item_price_by_percentage'];
      productItem.isInventoryItemUpdated= maps[i]['product_item_is_inventory_item_updated'];
      productItem.productCategoryId= maps[i]['product_category_id'];
      productItem.storageQuantity= maps[i]['quantity'];
      productItem.projectId= maps[i]['project_id'];
      productItem.hasCategory= maps[i]['product_item_has_category'];

      return productItem;
    });
  }

  Future<List<ProductItem>> productItemsInOrders() async {
    // Get a reference to the database.

    final db =  await dbProvider.database;

    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM orders od INNER JOIN product_item pi ON (od.product_item_id = pi.product_item_id)');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productItem=ProductItem();
      productItem.id= maps[i]['product_item_id'];
      productItem.name= maps[i]['product_item_name'];
      productItem.imageBase= maps[i]['product_item_image'];
      productItem.cost= maps[i]['product_item_cost'];
      productItem.sellingPrice= maps[i]['product_item_selling_price'];
      productItem.priceByPercentage= maps[i]['product_item_price_by_percentage'];
      productItem.isInventoryItemUpdated= maps[i]['product_item_is_inventory_item_updated'];
      productItem.orderDeliveryCharge= maps[i]['order_delivery_charge'];
      productItem.orderDiscount= maps[i]['order_discount'];
      productItem.orderGrossTotal= maps[i]['order_gross_total'];
      productItem.orderNetTotal= maps[i]['order_net_total'];
      productItem.orderQuantity= maps[i]['order_quantity'];
      productItem.createdAt= maps[i]['order_created'];
      productItem.orderStatus= maps[i]['order_status'];
      productItem.orderId= maps[i]['order_id'];
      productItem.projectId= maps[i]['project_id'];
      productItem.hasCategory= maps[i]['product_item_has_category'];
      return productItem;
    });
  }

  Future<List<ProductItem>> productItemsInOrdersByProjectId(int? projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;
    /*final List<Map<String, dynamic>> maps = await db.rawQuery('''SELECT *
    FROM orders od 
    INNER JOIN product_item pi ON (od.product_item_id = pi.product_item_id)
   INNER JOIN product_category pc ON (pc.product_category_id = pi.product_category_id) WHERE pc.project_id = ?
    ''',[projectID]);*/

    final List<Map<String, dynamic>> maps = await db.rawQuery('''SELECT * 
    FROM orders od 
    INNER JOIN product_item pi ON (od.product_item_id = pi.product_item_id) WHERE pi.project_id = ?
    ''',[projectID]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var productItem=ProductItem();
      productItem.id= maps[i]['product_item_id'];
      productItem.name= maps[i]['product_item_name'].toString();
      productItem.imageBase= maps[i]['product_item_image'];
      productItem.cost= maps[i]['product_item_cost'];
      productItem.sellingPrice= maps[i]['product_item_selling_price'];
      productItem.priceByPercentage= maps[i]['product_item_price_by_percentage'];
      productItem.isInventoryItemUpdated= maps[i]['product_item_is_inventory_item_updated'];
      productItem.orderDeliveryCharge= maps[i]['order_delivery_charge'];
      productItem.orderDiscount= maps[i]['order_discount'];
      productItem.orderGrossTotal= maps[i]['order_gross_total'];
      productItem.orderNetTotal= maps[i]['order_net_total'];
      productItem.orderQuantity= maps[i]['order_quantity'];
      productItem.createdAt= maps[i]['order_created'];
      productItem.orderStatus= maps[i]['order_status'];
      productItem.orderId= maps[i]['order_id'];
      productItem.projectId= maps[i]['project_id'];
      productItem.hasCategory= maps[i]['product_item_has_category'];
      return productItem;
    });
  }



  Future<List<GraphProductItem>> productItemsInOrdersByProjectIdAndMonth(int? projectID,int? productItemId, int currentYear) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;


    final List<Map<String, dynamic>> maps = await db.rawQuery('''SELECT 
    
    CAST(strftime('%Y', od.order_created/1000.0, 'unixepoch') AS INTEGER) AS order_year,
    CAST(strftime('%m', od.order_created/1000.0, 'unixepoch') AS INTEGER) AS order_month,
    pi.product_item_id AS product_item_id,
    pi.product_item_name AS product_item_name,
    SUM(od.order_quantity) AS sum_order_quantity
    
     
    FROM orders od 
    INNER JOIN product_item pi ON (od.product_item_id = pi.product_item_id) WHERE pi.project_id = ? AND od.product_item_id = ? AND od.order_status = ? AND order_year =?
     GROUP BY order_year, order_month
    ''',[projectID,productItemId,2,currentYear]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var graphProductItem=GraphProductItem();
      graphProductItem.id= maps[i]['product_item_id'];
      graphProductItem.name= maps[i]['product_item_name'].toString();
      graphProductItem.orderQuantityTotal= maps[i]['sum_order_quantity'];
      graphProductItem.orderYear= maps[i]['order_year'];
      graphProductItem.orderMonth= maps[i]['order_month'];

      return graphProductItem;
    });
  }

  Future<List<GraphProductItem>> productItemsGrossAndNetInOrdersByProjectId(int? projectID) async {
    // Get a reference to the database.

    final db =  await dbProvider.database;


    final List<Map<String, dynamic>> maps = await db.rawQuery('''SELECT 
    
    CAST(strftime('%Y', od.order_created/1000.0, 'unixepoch') AS INTEGER) AS order_year,
    CAST(strftime('%m', od.order_created/1000.0, 'unixepoch') AS INTEGER) AS order_month,
    pi.product_item_id AS product_item_id,
    pi.product_item_name AS product_item_name,
    SUM(od.order_quantity) AS sum_order_quantity,
    SUM(od.order_gross_total) AS sum_order_gross_total,
    SUM(od.order_net_total) AS sum_order_net_total,
    SUM(od.order_quantity*pi.product_item_cost) AS sum_order_total_product_cost,
    SUM(od.order_quantity*(pi.product_item_selling_price-pi.product_item_cost)) AS sum_order_total_product_profit
    
     
    FROM orders od 
    INNER JOIN product_item pi ON (od.product_item_id = pi.product_item_id) WHERE pi.project_id = ? AND od.order_status = ? AND od.product_item_id = pi.product_item_id
     GROUP BY order_year, order_month
    ''',[projectID,2]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      var graphProductItem=GraphProductItem();
      graphProductItem.id= maps[i]['product_item_id'];
      graphProductItem.name= maps[i]['product_item_name'];
      graphProductItem.orderQuantityTotal= maps[i]['sum_order_quantity'];
      graphProductItem.orderGrossTotal= maps[i]['sum_order_gross_total'];
      graphProductItem.orderNetTotal= maps[i]['sum_order_net_total'];
      graphProductItem.orderTotalProductCost= maps[i]['sum_order_total_product_cost'];
      graphProductItem.orderTotalProductProfit= maps[i]['sum_order_total_product_profit'];
      graphProductItem.orderYear= maps[i]['order_year'];
      graphProductItem.orderMonth= maps[i]['order_month'];

      return graphProductItem;
    });
  }

  Future<void> updateProductItemIsInventoryItemStatusByInventoryItemId(int? inventoryItemId) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    await db.rawUpdate(
        'UPDATE product_item SET product_item_is_inventory_item_updated = 1 where product_item_id IN (SELECT DISTINCT product_item_id from relation_product_inventory WHERE inventory_item_id = ?) AND product_item_price_by_percentage > 0'
        ,[inventoryItemId]);

  }


  Future<void> updateProductItemIsInventoryItemStatusByProductItemId(ProductItem productItem) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    await db.rawUpdate('UPDATE product_item SET product_item_is_inventory_item_updated = ? where product_item_id = ?'
        ,[productItem.isInventoryItemUpdated,productItem.id]);
  }

  Future<void> deleteAllDataAndProductItem(int? id) async {
    final db = await dbProvider.database;
    try{
      await db.transaction((txn) async{

        var batch =  txn.batch();
        // 1st delete all product item related data from relation table on product item delete
        batch.rawDelete('DELETE FROM relation_product_inventory where product_item_id = ?', [id]);
        // 2nd delete all product item related data from worker relation table on product item delete
        batch.rawDelete('DELETE FROM relation_product_workers where product_item_id = ?', [id]);
        // 3rd delete all product item related data from cart table on product item delete
        batch.rawDelete('DELETE FROM relation_cart_product where product_item_id = ?', [id]);
        // 4th delete all product item related data from orders table on product item delete
        batch.rawDelete('DELETE FROM orders where product_item_id = ?', [id]);
        // 5th delete all product item related data from storage table on product item delete
        batch.rawDelete('DELETE FROM storage where product_item_id = ?', [id]);
        // 6th then delete the product item from product_item table
        batch.rawDelete('DELETE FROM product_item where product_item_id = ?', [id]);

        await batch.commit(noResult: true);
      });}catch (e) {
      throw ("some arbitrary error");
    }
  }

  /*Future<void> productItemCostByProductItemId(List<int> productItemIdArray) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

  final List<Map<String, dynamic>> maps =  await db.rawQuery('''SELECT 
      p.product_item_id,
      SUM(
          pir.inventory_item_quantity * (inv.inventory_item_cost_per_type_new/inv.inventory_item_pieces_in_type)
      ) AS cost
      FROM
      product_item p
      INNER JOIN relation_product_inventory pir ON p.product_item_id = pir.product_item_id
      INNER JOIN inventory_item inv ON inv.inventory_item_id = pir.inventory_item_id
      WHERE
      p.product_item_id IN (${productItemIdArray.join(', ')})
    GROUP BY p.product_item_id
    ''');
    List.generate(maps.length, (i) {
      debugPrint("cost&productId ${maps[i]['cost']}");
    });
  }*/

  /*Future<void> productItemCostByProductItemId(List<int> productItemIdArray) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    final List<Map<String, dynamic>> maps =  await db.rawQuery('''SELECT 
      p.product_item_id,
      printf("%.2f",((((w.worker_salary*1.0)/(w.worker_hours*30))/60)*1.0)*(pwr.work_duration*1.0)) as cost2
      FROM
      product_item p
      LEFT JOIN relation_product_workers pwr ON pwr.product_item_id=p.product_item_id
      LEFT JOIN workers w
      ON w.worker_id=pwr.worker_id
      WHERE p.product_item_id IN (${productItemIdArray.join(', ')})
      GROUP BY p.product_item_id
    ''');
    List.generate(maps.length, (i) {
      debugPrint("cost&productId ${maps[i]['cost']}  ${maps[i]['cost2']}");
    });
  }*/

  Future<void> getProductItemIdAndRecalculateCostByProductItemId(List<int> productItemIdArray) async {
    // Get a reference to the database.

    final db = await dbProvider.database;
try{
    final List<Map<String, dynamic>> maps =  await db.rawQuery('''
    SELECT product_item_id,product_item_price_by_percentage,SUM(printf("%.2f",cost)) as totalProductCost FROM 
    (
    SELECT 
      p.product_item_id as product_item_id,product_item_price_by_percentage,
      (w.worker_wage_per_min_new)*(pwr.work_duration*1.0) as cost
      FROM
      product_item p
      LEFT JOIN relation_product_workers pwr ON pwr.product_item_id=p.product_item_id
      LEFT JOIN workers w
      ON w.worker_id=pwr.worker_id
      WHERE p.product_item_id IN (${productItemIdArray.join(', ')})
      GROUP BY p.product_item_id
      UNION ALL
    SELECT 
      p.product_item_id,product_item_price_by_percentage,
      SUM(
          pir.inventory_item_quantity * (inventory_item_cost_per_item_in_type_new)
      ) AS cost
      FROM
      product_item p
      INNER JOIN relation_product_inventory pir ON p.product_item_id = pir.product_item_id
      INNER JOIN inventory_item inv ON inv.inventory_item_id = pir.inventory_item_id
      WHERE
      p.product_item_id IN (${productItemIdArray.join(', ')})
      GROUP BY p.product_item_id
   ) GROUP BY product_item_id
    
    ''');
    List.generate(maps.length, (i) async{
      debugPrint("cost&productId ${maps[i]['product_item_id']}  ${maps[i]['product_item_price_by_percentage']}  ${maps[i]['totalProductCost']}");
      if((maps[i]['product_item_price_by_percentage'])>0){
        //custom round off and update product price
        final double value=(maps[i]['totalProductCost']+((maps[i]['totalProductCost']*maps[i]['product_item_price_by_percentage'])/100))!;
        final double roundOffPrice= value.toString().toCustomDoubleRoundOff;
        await db.rawUpdate('UPDATE product_item SET product_item_cost = ?,product_item_selling_price= ? WHERE product_item_id = ?',[maps[i]['totalProductCost'],roundOffPrice,maps[i]['product_item_id']]);
      }else
        await db.rawUpdate('UPDATE product_item SET product_item_cost = ?,product_item_is_inventory_item_updated = 1 WHERE product_item_id = ?',[maps[i]['totalProductCost'],maps[i]['product_item_id']]);
    });

  }catch (e) {
  throw ("some arbitrary error");
  }
  }
  Future<void> updateProductItemCategoryId(int? categoryId,List<int> productItemIdArray) async {
    // Get a reference to the database.

    final db = await dbProvider.database;

    await db.rawUpdate('UPDATE product_item SET product_category_id = ? WHERE product_item_id IN (${productItemIdArray.join(', ')})',
        [categoryId]);
  }

}

/*
SUM(
CASE WHEN (inv.inventory_item_cost_per_item_in_type_new>inv.inventory_item_cost_per_item_in_type_old)
THEN  pir.inventory_item_quantity * (inventory_item_cost_per_item_in_type_new)
ELSE  pir.inventory_item_quantity * (inventory_item_cost_per_item_in_type_old)
END
) */
