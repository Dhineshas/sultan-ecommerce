import 'dart:convert';
import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/database/product_category_operations.dart';
import 'package:ecommerce/model/Project.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:image_picker/image_picker.dart';

import 'database/project_product_relation_operations.dart';
import 'model/ProductCategory.dart';
import 'model/ProjectProductRelation.dart';

class AddProductCategoryScreenState extends StatefulWidget {
  final ProductCategory? productCategory;
  const AddProductCategoryScreenState({@required this.productCategory,Key? key}) : super(key: key);

  @override
  _AddProductCategoryScreenState createState() => _AddProductCategoryScreenState();
}

class _AddProductCategoryScreenState extends State<AddProductCategoryScreenState> {

  File? _image;
  final picker = ImagePicker();
  final teProductCategoryNameController = TextEditingController();
  var productCategoryOperations = ProductCategoryOperations();
  var productItemImageBase64;

  Project? selectedProject ;
  @override
  void setState(VoidCallback fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  @override
  void initState() {
    if (widget.productCategory != null) {
      teProductCategoryNameController.text='${widget.productCategory?.name}';
      productItemImageBase64 = widget.productCategory?.imageBase;
    }
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final addProductCategory =
    Align(alignment: Alignment.centerLeft,
      child: MyWidgets.textView(text: 'Add Product Category',
        style: MyStyles.customTextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 24,
        ),
      ),
    );

    final imageView =
    GestureDetector(
        onTap: () {
          _showPicker(context);

        },
        child: Container(
          decoration: BoxDecoration(
            // color: Colors.grey[200],
              borderRadius: BorderRadius.circular(75)
          ),
          height: 104,
          width: 104,
          child: productItemImageBase64 != null?
          ClipRRect(
            borderRadius: BorderRadius.circular(75),
            child: Image.memory(
              base64Decode('$productItemImageBase64'),
              width: 100,
              height: 100,
              fit: BoxFit.cover,
              gaplessPlayback: true,
            ),
          ):_image != null
              ? ClipRRect(
            borderRadius: BorderRadius.circular(75),
            child: Image.file(
              _image!,
              width: 100,
              height: 100,
              fit: BoxFit.cover,
              gaplessPlayback: true,
            ),
          )
              : Container(
            decoration: BoxDecoration(
              // color: Colors.grey[200],
                border: Border.all(color: Colors.black,width: 1),
                borderRadius: BorderRadius.circular(75)
            ),
            width: 100,
            height: 100,
            child: Icon(
              Icons.image_rounded,
              color: Colors.grey[800],
            ),
          ),
        ));

    final addProductCategoryEditTxt =
    Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text: 'Name'),
          SizedBox(height: 10,),
          MyWidgets.textViewContainer(

            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child:Align(alignment:Alignment.centerLeft,child: MyWidgets.textFromField(

              contentPaddingL: 15.0,

              contentPaddingR: 15.0,

              cornerRadius: 8,
              keyboardType: TextInputType.name,
              controller: teProductCategoryNameController,
            ),
          ),)
        ]
    );


    final addProductCategoryButton =
    MyWidgets.materialButton(context,text: widget.productCategory==null?"Add Product Category":"Update Product Category",onPressed:() { if(_validate()){
      if(_validate()){
        widget.productCategory==null? getSelectedProjectAndAddProductCategory() : updateProductCategory();
      }   }});
    return Scaffold(
      backgroundColor: Colors.transparent,
      /*appBar: AppBar(
        title: Text(
          'Add Product Category',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),*/
      body: SingleChildScrollView(
          padding: EdgeInsets.only(left: 30, right: 30, top: 25, bottom: 20),
          physics: BouncingScrollPhysics(),
              child: Column(
                children: <Widget>[
                  addProductCategory,
                  SizedBox(
                    height: 30,
                  ),
                  // imageView,
                  //
                  // SizedBox(
                  //   height: 20,
                  // ),
                  addProductCategoryEditTxt,
                  SizedBox(
                    height: 30,
                  ),
                  addProductCategoryButton
                ],
              )),
    );
  }

  void _showPicker(context) {
    /*showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  ListTile(
                    leading: Icon(Icons.photo_camera),
                    title: Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );


        });*/

    MyImagePickerBottomSheet.showPicker(context, fileCallback: (image) {
      productItemImageBase64 = null;
      setState(() {
        _image = image!=null?File(image.path):null;
      });
      return null;
    });
  }

/*  _imgFromCamera() async {
    var image =
    await picker.pickImage(source: ImageSource.camera, imageQuality: 0,maxHeight:150,maxWidth: 150);
    productItemImageBase64 = null;
    setState(() {
      _image = image!=null?File(image.path):null;
    });
  }

  _imgFromGallery() async {
    var image =
    await picker.pickImage(source: ImageSource.gallery, imageQuality: 0,maxHeight:150,maxWidth: 150);
    productItemImageBase64 = null;
    setState(() {
      _image = image!=null?File(image.path):null;

    });
  }*/

  bool _validate()  {

    //  if (productItemImageBase64 == null && _image == null) {
    //   context.showSnackBar("Select Product Category Image");
    //   return false;
    // } else
      if (teProductCategoryNameController.text.isEmpty) {
      context.showSnackBar("Enter Product Category Name");
      return false;
    }
     //  else if (teProductCategoryNameController.text.startsWith(RegExp(r'^\d+\.?\d{0,2}'))) {
     //   context.showSnackBar("Can\'t Start Product Category Name with Number");
     //   return false;
     // }
    return true;
  }

 /* String getImageBase64() {
    if (_image != null) {
      var bytes = _image?.readAsBytesSync();
      var base64Image = base64Encode(bytes!);
      return base64Image.toString();
    }
    return "";
  }*/

  addProductCategory()async {
    var productCategory = ProductCategory();
    productCategory.projectId=selectedProject?.id;
    productCategory.name = teProductCategoryNameController.text.toString();
    productCategory.imageBase=getImageBase64(_image);
    await productCategoryOperations.insertProductCategory(productCategory).then((productCategory)async {

      clearTextAndPop();
    });
  }

  clearTextAndPop(){
    teProductCategoryNameController.text = "";

    showtoast("Successfully Added Data");
    Navigator.pop(context,true);
  }
  updateProductCategory()async {
    if (widget.productCategory != null) {
    var productCategory = ProductCategory();
    productCategory.id = widget.productCategory?.id;
    productCategory.projectId=widget.productCategory?.projectId;
    productCategory.name = teProductCategoryNameController.text.toString();
    productCategory.imageBase = productItemImageBase64 != null
        ? productItemImageBase64
        : getImageBase64(_image);
    await productCategoryOperations
        .updateProductCategory(productCategory)
        .then((value) {
      teProductCategoryNameController.text = "";
      showtoast("Data Saved successfully");
      Navigator.pop(context,true);


    });
  }}

  getSelectedProjectAndAddProductCategory()async{
    await MyPref.getProject().then((value) {
        selectedProject=value;
        if(value!=null)
        addProductCategory();
        else
          context.showSnackBar("Select project");
    });
  }
}
