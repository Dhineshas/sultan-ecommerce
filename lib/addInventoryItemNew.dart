import 'dart:convert';
import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/model/Inventory.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'inventoryUnitMeasures.dart';
import 'database/history_operations.dart';
import 'database/inventory_item_operations.dart';
import 'model/History.dart';
import 'model/InventoryItem.dart';
import 'package:image_picker/image_picker.dart';

import 'model/Project.dart';

class AddInventoryItemScreenStateNew extends StatefulWidget {
  final Inventory? inventory;

  const AddInventoryItemScreenStateNew({@required this.inventory, Key? key})
      : super(key: key);

  @override
  _AddInventoryItemScreenState createState() => _AddInventoryItemScreenState();
}

class _AddInventoryItemScreenState extends State<AddInventoryItemScreenStateNew> {
  File? _image;
  final picker = ImagePicker();
  var inventoryItemOperations = InventoryItemOperations();
  var historyItemOperations = HistoryOperations();
  final teInvNameController = TextEditingController();
  final teInvTotalTypeCountController = TextEditingController();
  final teInvPiecesInTypeController = TextEditingController();
  final teInvCostPerTypeController = TextEditingController();
  final List<String> itemType = <String>['Box', 'Length'];
  int selectedTypeIndex = 0;
  String dropdownSelectedTypeValue = InventoryUnitMeasure.inventoryUnitMeasureArray[0];

  @override
  void setState(VoidCallback fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  Project? project;
  @override
  void initState() {
     MyPref.getProject().then((project) async {
      this.project = project;});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final addInventoryListItemText =
    Align(alignment: Alignment.centerLeft,
      child: MyWidgets.textView(text: 'Add Item',
        style: MyStyles.customTextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 24,
        ),
      ),
    );
    final imageView =
    GestureDetector(
        onTap: () {
          _showPicker(context);

        },
        child: Container(
          decoration: BoxDecoration(
            // color: Colors.grey[200],
              borderRadius: BorderRadius.circular(75)
          ),
          height: 104,
          width: 104,
          child:_image != null?
          ClipRRect(
            borderRadius: BorderRadius.circular(75),
            child: Image.file(
            _image!,
            width: 100,
            height: 100,
            fit: BoxFit.fitHeight,
              gaplessPlayback: true,
          ),
          ):_image != null
              ? ClipRRect(
            borderRadius: BorderRadius.circular(75),
            child: Image.file(
              _image!,
              width: 100,
              height: 100,
              fit: BoxFit.cover,
              gaplessPlayback: true,
            ),
          )
              : Container(
            decoration: BoxDecoration(
              // color: Colors.grey[200],
                border: Border.all(color: Colors.black,width: 1),
                borderRadius: BorderRadius.circular(75)
            ),
            width: 100,
            height: 100,
            child: Icon(
              Icons.image_rounded,
              color: Colors.grey[800],
            ),
          ),
        ));

    final addInventoryItemEditTxt =Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text: 'Item Name'),
          SizedBox(height: 10,),
          MyWidgets.textViewContainer(

            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child: Align(alignment:Alignment.centerLeft,child:MyWidgets.textFromField(

              contentPaddingL: 15.0,

              contentPaddingR: 15.0,

              cornerRadius: 8,
              keyboardType: TextInputType.name,
              controller: teInvNameController,
            )),
          ),
        ]
    );

    final toggleButton = Container(
        width: 215,
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.grey[350],
        ),
        child: Center(
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: itemType.length,
            itemBuilder: (BuildContext context, int index) => InkWell(
              child: Card(
                color: selectedTypeIndex == index
                    ? Colors.white
                    : Colors.grey[350],
                elevation: selectedTypeIndex == index ? 5 : 0,
                child: Container(
                  width: 100,
                  child: Center(child: Text('${itemType[index]}')),
                ),
              ),
              onTap: () => setState(() {
                selectedTypeIndex = index;
              }),
            ),
          ),
        ));


    final totalTypeEditTxtWithDropDown =


       Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10,),
            MyWidgets.textView(text:'Total Box',),
            SizedBox(height: 10,),
        Row(mainAxisSize: MainAxisSize.max,crossAxisAlignment: CrossAxisAlignment.center,children: [
    Expanded(flex:1,child:
          MyWidgets.textViewContainer(

              height: 45, cornerRadius: 8, color: MyColors.white,
              // color: Colors.white54,
              child:Align(alignment:Alignment.centerLeft,child: MyWidgets.textFromField(

                contentPaddingL: 15.0,

                contentPaddingR: 15.0,

                cornerRadius: 8,
                keyboardType: TextInputType.number,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                controller: teInvTotalTypeCountController,
              )),
            )),SizedBox(width: 10,),
          MyWidgets.textViewContainer(

              height: 45, cornerRadius: 8, color: MyColors.white,
              // color: Colors.white54,
              child: Padding(
                  padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                  child: DropdownButton<String>(
                    value: dropdownSelectedTypeValue,
                    icon: const Icon(Icons.keyboard_arrow_down),
                    elevation: 8,

                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    style: const TextStyle(color: Colors.black),
                    /*underline: Container(
                      height: 2,
                      color: Colors.blue,
                    ),*/
                    onChanged: (String? newValue) {
                      setState(() {
                        selectedTypeIndex =
                            InventoryUnitMeasure.inventoryUnitMeasureArray.indexOf(newValue!);
                        dropdownSelectedTypeValue = newValue;
                      });
                    },
                    items: InventoryUnitMeasure.inventoryUnitMeasureArray
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  )))
        ])
          ]
      );
    final piecesInType =/*selectedTypeIndex!=0?Container():*/
    Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text:selectedTypeIndex==0?'Pieces in a Box': '$dropdownSelectedTypeValue in a Box',),
          SizedBox(height: 10,),
          MyWidgets.textViewContainer(

            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child: Align(alignment:Alignment.centerLeft,child:MyWidgets.textFromField(

              contentPaddingL: 15.0,

              contentPaddingR: 15.0,

              cornerRadius: 8,
              keyboardType: TextInputType.number,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              controller: teInvPiecesInTypeController,
            )),
          ),
        ]
    );
    final totalCostPerType =
    Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text:"Total cost",),
          SizedBox(height: 10,),
          MyWidgets.textViewContainer(

            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child:Align(alignment:Alignment.centerLeft,child: MyWidgets.textFromField(

              contentPaddingL: 15.0,

              contentPaddingR: 15.0,

              cornerRadius: 8,
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}')),
              ],
              controller: teInvCostPerTypeController,
            )),
          ),
        ]
    );
    final addInventoryItemButton =

    MyWidgets.materialButton(context,text: "Add Inventory Item",onPressed:() {

      _validate() ? addInventoryItem() : {};    });

    return Scaffold(
      /*appBar: AppBar(
        title: Text(
          'Add Inventory Item',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),*/
      body: SingleChildScrollView(
          padding: EdgeInsets.only(left: 30, right: 30, top: 25, bottom: 20),

          child: SizedBox(

              child: Column(
                children: <Widget>[
                  addInventoryListItemText,
                  SizedBox(
                    height: 25,
                  ),
                  imageView,
                  SizedBox(
                    height: 20,
                  ),

                  addInventoryItemEditTxt,

                  totalTypeEditTxtWithDropDown,

                  piecesInType,

                  totalCostPerType,
                  SizedBox(
                    height: 30,
                  ),
                  addInventoryItemButton,
                  SizedBox(
                    height: 15,
                  )
                ],
              ))),
    );
  }

  void _showPicker(context) {

    MyImagePickerBottomSheet.showPicker(context, fileCallback: (image) {

      setState(() {
        _image = image!=null?File(image.path):null;
      });
      return null;
    });
  }

  bool _validate() {
    if (_image == null) {
      context.showSnackBar("Select inventory image");
      return false;
    } else if (teInvNameController.text.isEmpty) {
      context.showSnackBar("Enter Inventory name");
      return false;
    }
    // else if (teInvNameController.text.startsWith(RegExp(r'^\d+\.?\d{0,2}'))) {
    //   context.showSnackBar("Can\'t Start Inventory Name with Number");
    //   return false;
    // }
    else if (teInvTotalTypeCountController.text.isEmpty||int.tryParse(teInvTotalTypeCountController.text.trim())==0) {
      context.showSnackBar("Enter total $dropdownSelectedTypeValue");
      return false;
    } else if (selectedTypeIndex==0&&teInvPiecesInTypeController.text.isEmpty||int.tryParse(teInvPiecesInTypeController.text.trim())==0) {
      context.showSnackBar("Enter Pieces in $dropdownSelectedTypeValue");
      return false;
    } else if (teInvCostPerTypeController.text.isEmpty||int.tryParse(teInvCostPerTypeController.text.trim())==0) {
      context.showSnackBar("Enter cost per $dropdownSelectedTypeValue");
      return false;
    }
    return true;
  }



  addInventoryItem() async{
    if(project!=null){
    var inventoryItem = InventoryItem();
    inventoryItem.name = teInvNameController.text.trim().toString();
    inventoryItem.imageBase = getImageBase64(_image);
    inventoryItem.itemType = selectedTypeIndex;
    inventoryItem.totalType = int.parse(teInvTotalTypeCountController.text.trim());
   // inventoryItem.piecesInType = int.parse(teInvPiecesInTypeController.text.trim());
    //if(selectedTypeIndex==0) {
      inventoryItem.piecesInType = int.parse(teInvPiecesInTypeController.text.trim());
      inventoryItem.costPerItemInTypeNew=double.tryParse(((double.tryParse(teInvCostPerTypeController.text.trim())!/int.parse(teInvPiecesInTypeController.text.trim()))/int.parse(teInvTotalTypeCountController.text.trim())).toString());
      inventoryItem.stock = double.tryParse(teInvTotalTypeCountController.text.trim())! * int.tryParse(teInvPiecesInTypeController.text.trim())!;

    /*}else{
      inventoryItem.piecesInType =1;
      inventoryItem.costPerItemInTypeNew=double.tryParse((double.tryParse(teInvCostPerTypeController.text.trim())!/int.parse(teInvTotalTypeCountController.text.trim())).toStringAsFixed(2));
      inventoryItem.stock = double.tryParse(teInvTotalTypeCountController.text.trim())!;
    }*/
    inventoryItem.costPerTypeNew =
        double.tryParse(teInvCostPerTypeController.text.trim());


   // inventoryItem.costPerItemInTypeNew=double.tryParse((double.tryParse(teInvCostPerTypeController.text.trim())!/int.parse(teInvPiecesInTypeController.text.trim())).toStringAsFixed(2));

    //inventoryItem.stock = int.tryParse(teInvTotalTypeCountController.text.trim())! * int.tryParse(teInvPiecesInTypeController.text.trim())!;
if(widget.inventory==null){
  inventoryItem.inventoryId=null;
  inventoryItem.hasCategory=0;
}else{
  inventoryItem.inventoryId= widget.inventory?.id;
  inventoryItem.hasCategory=1;
}
    //inventoryItem.inventoryId =widget.inventory==null?null: widget.inventory?.id;
    inventoryItem.projectId=this.project?.id;
   await inventoryItemOperations.insertInventoryItem(inventoryItem).then((value)async {

     value.isNotEmpty? await addInventoryHistory(value):{};


    });}
  }

  addInventoryHistory(List<InventoryItem> value)async{

    var history = History();
    history.inventoryItemId=value.first.id;
    history.historyItemType=value.first.itemType;
    history.historyTotalType=value.first.totalType;
    history.historyPiecesInType=value.first.piecesInType;
    history.historyCostPerType=value.first.costPerTypeNew;
    history.historyCostPerItemInType=value.first.costPerItemInTypeNew;
    history.historyStock=value.first.stock;
    print('dateAdded ${history.historyDate}');
    await historyItemOperations.insertHistory(history).then((value) {
      teInvNameController.text = "";
      teInvTotalTypeCountController.text = "";
      teInvPiecesInTypeController.text = "";
      teInvCostPerTypeController.text = "";
      showtoast("Successfully Added Data");
      Navigator.pop(context, true);
    });
  }

}
