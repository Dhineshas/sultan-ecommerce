import 'dart:convert';
import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/allWorkersList.dart';
import 'package:ecommerce/database/worker_relation_operations.dart';
import 'package:ecommerce/database/workers_operations.dart';
import 'package:ecommerce/productItemLineChart.dart';
import 'package:ecommerce/model/Cart.dart';
import 'package:ecommerce/model/Worker.dart';
import 'package:ecommerce/model/WorkerRelation.dart';
import 'package:ecommerce/upgrade.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Subscription.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'addProduct.dart';
import 'addProductNew.dart';
import 'allInventotyItemList.dart';
import 'database/cart_operations.dart';
import 'database/cart_relation_operations.dart';
import 'database/inventory_item_operations.dart';
import 'database/product_item_operations.dart';
import 'database/relation_operations.dart';
import 'model/CartRelation.dart';
import 'model/InventoryItem.dart';
import 'package:image_picker/image_picker.dart';

import 'model/ProductCategory.dart';
import 'model/ProductItem.dart';
import 'model/InventoryRelation.dart';

class ProductItemDetailsScreenState extends StatefulWidget {
  final ProductCategory? productCategory;
  final int? productItemId;

  const ProductItemDetailsScreenState(
      {@required this.productCategory, @required this.productItemId, Key? key})
      : super(key: key);

  @override
  _ProductItemDetailsScreenState createState() =>
      _ProductItemDetailsScreenState();
}

class _ProductItemDetailsScreenState
    extends State<ProductItemDetailsScreenState> {
  var productItemOperations = ProductItemOperations();
  var relationOperations = RelationOperations();
  var workerRelationOperations = WorkerRelationOperations();
  var inventoryItemOperations = InventoryItemOperations();
  var workersOperations = WorkersOperations();

  var cartOperations = CartOperations();
  var cartRelationOperations = CartRelationOperations();
  var productItemImageBase64;
  var totalInventoryCost = 0.00;
  var totalWorkersCost = 0.00;
  bool isLoading = true;
  bool isProductItemUpdated = false;

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 1000), () {
      if (widget.productItemId != null) {
        /*productItemId=widget.productItem?.id;
        productItemName=widget.productItem?.name;
        productItemImageBase64 = widget.productItem?.imageBase;
        productItemCost=widget.productItem?.cost;
        productItemSellingPrice=widget.productItem?.sellingPrice;
        percentage=widget.productItem?.priceByPercentage;*/

        //_getDataSelectedInventoryItems();
        _getData();
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final productNameText = Align(
        alignment: Alignment.topLeft,
        child: ConstrainedBox(
          constraints: BoxConstraints(minWidth: 20),
          child: IntrinsicWidth(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                MyWidgets.textView(
                    text: productItemName!,
                    maxLine: 2,
                    style: MyStyles.customTextStyle(
                        fontSize: 20, fontWeight: FontWeight.w500)),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddProductNewScreenState(
                                    productCategory: widget.productCategory,
                                    productItemId: item?.id,
                                  ))).then((value) => value == true
                          ? {isProductItemUpdated = value, _getData()}
                          : {});
                    },
                    icon: Icon(
                      Icons.edit,
                      size: 20,
                    ))
              ],
            ),
          ),
        ));
    final imageView = Container(
        decoration: BoxDecoration(
            // color: Colors.grey[200],
            borderRadius: BorderRadius.circular(75)),
        height: 104,
        width: 104,
        child: productItemImageBase64 != null
            ? ClipRRect(
                borderRadius: BorderRadius.circular(75),
                child: Image.memory(
                  base64Decode('$productItemImageBase64'),
                  width: 100,
                  height: 100,
                  fit: BoxFit.cover,
                  gaplessPlayback: true,
                ),
              )
            : Container(
                decoration: BoxDecoration(
                    // color: Colors.grey[200],
                    border: Border.all(color: Colors.black, width: 1),
                    borderRadius: BorderRadius.circular(75)),
                width: 100,
                height: 100,
                child: Icon(
                  Icons.image_rounded,
                  color: Colors.grey[800],
                ),
              ));

    final addedItems = Align(
        alignment: Alignment.topLeft,
        child: MyWidgets.textView(
            text: 'Items',
            style: MyStyles.customTextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
            textAlign: TextAlign.left));
    ;

    final addedWorkers = workers2.isNotEmpty
        ? Align(
            alignment: Alignment.topLeft,
            child: MyWidgets.textView(
                text: 'Workers',
                style: MyStyles.customTextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                ),
                textAlign: TextAlign.left))
        : Container();

    final selectedInventoryItemListVIew = Container(
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
            color: MyColors.white,
            border: Border.all(color: MyColors.grey_70, width: 0.5),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: 150,
          ),
          child: inventoryItems2.isNotEmpty
              ? ListView.builder(
                  key: _inventoryListKey,
                  controller: _scrollController,
                  shrinkWrap: true,
                  itemCount: inventoryItems2.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _buildItem(context, inventoryItems2[index], index);
                  })
              : Container(),
        ));

    final selectedWorkersListVIew = workers2.isNotEmpty
        ? Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: MyColors.white,
                border: Border.all(color: MyColors.grey_70, width: 0.5),
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: 100,
                ),
                child: ListView.builder(
                    key: _workerListKey,
                    controller: _scrollController,
                    shrinkWrap: true,
                    itemCount: workers2.length,
                    itemBuilder: (BuildContext context, int index) {
                      return _buildWorkerItem(context, workers2[index], index);
                    })))
        : Container();

    final addToCartButton =
        MyWidgets.materialButton(context, text: 'Add to cart', onPressed: () {
      addCartAndCartRelation();
    });

    final costPriceContainer = MyWidgets.shadowContainer(
        height: 95,
        cornerRadius: 10,
        color: MyColors.white_chinese_e0_bg,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Spacer(),
            Padding(
                padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Flexible(
                        flex: 7,
                        fit: FlexFit.tight,
                        child: MyWidgets.textView(
                            text: 'Cost: ', textAlign: TextAlign.start)),
                    Flexible(
                        flex: 4,
                        fit: FlexFit.tight,
                        child: MyWidgets.textViewContainer(
                          paddingR: 5,
                          paddingL: 5,
                          height: 35,
                          cornerRadius: 8,
                          color: MyColors.white,
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: MyWidgets.textView(
                                text:
                                    '${double.tryParse((productItemCost)!.toStringAsFixed(1))} QR'),
                          ),
                        ) //Container
                        )
                  ],
                )),
            Spacer(),
            Padding(
                padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Flexible(
                        flex: 7,
                        fit: FlexFit.tight,
                        child: MyWidgets.textView(
                            text: 'Price: ', textAlign: TextAlign.start)),
                    Flexible(
                        flex: 4,
                        fit: FlexFit.tight,
                        child: MyWidgets.textViewContainer(
                          paddingR: 5,
                          paddingL: 5,
                          height: 35,
                          cornerRadius: 8,
                          color: MyColors.white,
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: MyWidgets.textView(
                                text:
                                    '${double.tryParse((productItemSellingPrice)!.toStringAsFixed(1))} QR'),
                          ),
                        ) //Container
                        ),
                  ],
                )),
            Spacer(),
          ],
        ));

    final addCostEditTxt = FadeInUp(
        delay: Duration(milliseconds: 400),
        child: Padding(
            padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Flexible(
                    flex: 0,
                    fit: FlexFit.tight,
                    child: Text(
                      //'COST : ${double.parse(((totalInventoryCost+totalWorkersCost)).toStringAsFixed(2))} QAR',
                      'COST : ${double.tryParse((productItemCost)!.toStringAsFixed(1))} QAR',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 18,
                          color: Colors.black),
                    ) //Container
                    ),
              ],
            )));
    //var totalCost=double.tryParse((totalInventoryCost+totalWorkersCost).toStringAsFixed(2));
//var price= (widget.productItem?.priceByPercentage)!>0?double.tryParse((totalCost!+(totalCost)*(widget.productItem?.priceByPercentage)!/100).toStringAsFixed(2)):(widget.productItem?.sellingPrice);

    final addPriceEditTxt = FadeInUp(
        delay: Duration(milliseconds: 500),
        child: Padding(
            padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Flexible(
                    flex: 0,
                    fit: FlexFit.tight,
                    child: Text(
                      //'PRICE :${(widget.productItem?.priceByPercentage)!>0?double.tryParse(((totalInventoryCost+totalWorkersCost)+(totalInventoryCost+totalWorkersCost)*(widget.productItem?.priceByPercentage)!/100).toStringAsFixed(2)):(widget.productItem?.sellingPrice)} QAR',
                      'PRICE :${double.tryParse((productItemSellingPrice)!.toStringAsFixed(1))} QAR',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 18,
                          color: Colors.black),
                    ) //Container
                    ),
              ],
            )));
    return MyWidgets.scaffold(
      appBar: MyWidgets.appBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: MyColors.black,
          ),
          onPressed: () {
            Navigator.pop(context, isProductItemUpdated);
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.multiline_chart_outlined,
              color: Colors.black,
            ),
            onPressed: ()async {
              await validateSubscriptionAndNavigate(ProductItemLineChart(productItemId: widget.productItemId,));

            },
          )
        ],
      ),
      body: isLoading
          ? MyWidgets.buildCircularProgressIndicator()
          : SingleChildScrollView(
              padding:
                  EdgeInsets.only(left: 25, right: 25, top: 10, bottom: 10),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  productNameText,
                  SizedBox(
                    height: 5,
                  ),
                  imageView,
                  SizedBox(
                    height: 20,
                  ),
                  // addDuration,

                  /* SizedBox(
            height: 15,
          ),*/

                  SizedBox(
                    height: 15,
                  ),
                  addedItems,
                  SizedBox(
                    height: 10,
                  ),
                  selectedInventoryItemListVIew,

                  SizedBox(
                    height: 20,
                  ),
                  addedWorkers,
                  SizedBox(
                    height: 10,
                  ),
                  selectedWorkersListVIew,
                  SizedBox(
                    height: 20,
                  ),
                  /*addCostEditTxt,
          SizedBox(
            height: 25,
          ),
          addPriceEditTxt,
          SizedBox(
            height: 15,
          ),*/
                  costPriceContainer,
                  SizedBox(
                    height: 15,
                  ),
                  addToCartButton,
                  SizedBox(
                    height: 15,
                  )
                ],
              )),
    );
  }
  Future validateSubscriptionAndNavigate(Widget widget)async{
    await Subscription.getActiveSubscription().then((value)async {
      if(value==0){

        await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    UpgradeScreenState()));

      }else await  goto(context,widget);
    });

  }
  //var inventoryItems = <InventoryItem>[];
  var inventoryItems2 = <InventoryItem>[];

//  var workers = <Worker>[];
  var workers2 = <Worker>[];
  final GlobalKey<AnimatedListState> _inventoryListKey = GlobalKey();
  final GlobalKey<AnimatedListState> _workerListKey = GlobalKey();
  var _scrollController = ScrollController();

  //var relations = <InventoryRelation>[];
  //var workRelations = <WorkerRelation>[];

  ///Construct cell for List View
  Widget _buildItem(BuildContext context, InventoryItem value, int index) {
    final teItemQtyController = TextEditingController();
    teItemQtyController.text = '${value.selectedQuantity}';
    return Column(children: <Widget>[
      Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(4.0),
            child:
                value.imageBase != null && value.imageBase.toString().isNotEmpty
                    ? ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.memory(
                          base64Decode(value.imageBase.toString()),
                          width: 40,
                          height: 40,
                          gaplessPlayback: true,
                          fit: BoxFit.fitHeight,
                        ),
                      )
                    : Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(10)),
                        width: 40,
                        height: 40,
                      ),
          ),
          Padding(padding: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0)),
          Expanded(
            flex: 1,
            child: RichText(
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              strutStyle: StrutStyle(fontSize: 12.0),
              text: TextSpan(
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 15,
                    color: Colors.black),
                text: value.name,
              ),
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Text('X ${value.selectedQuantity}')
        ],
      ),
      Divider(
        height: 1,
      )
    ]);
  }

  Widget _buildWorkerItem(BuildContext context, Worker value, int index) {
    var selectedMin;
    if (value.selectedMins != null && value.selectedMins! > 0) {
      final selectedDuration = Duration(minutes: value.selectedMins!);
      selectedMin = selectedDuration.inHours.toString() +
          ' hours ' +
          (selectedDuration.inMinutes % 60).toString() +
          ' minutes ';
    }
    return Column(children: <Widget>[
      /*Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(5.0),
            child:  Icon(
              Icons.account_circle_outlined,
              size: 40,
            ),
          ),
          Padding(padding: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0)),
          Flexible(child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 0.0, 5.0)),

              Container(
                width: 200,
                child: RichText(
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  strutStyle: StrutStyle(fontSize: 12.0),
                  text: TextSpan(
                    style: TextStyle(
                        fontWeight: FontWeight.w500, fontSize: 15, color: Colors.black),
                    text:'Name : ${value.name!}' ,
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),
              Text(
                'Salary : ${value.salary!}' ,
                style: TextStyle(
                  fontWeight: FontWeight.w300,
                  fontSize: 15.0,
                  color: Colors.black,
                ),
                textAlign: TextAlign.left,
                maxLines: 2,
              ), */ /*Padding(padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),Text(
                'Working Hours : ${value.hoursPerDay!}' ,
                style: TextStyle(
                  fontWeight: FontWeight.w300,
                  fontSize: 15.0,
                  color: Colors.black,
                ),
                textAlign: TextAlign.left,
                maxLines: 2,
              ),*/ /*
              Padding(padding: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 10.0)),
            ],
          ))
          ,
          Text(selectedMin==null?'0 Minutes':'$selectedMin',style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 12, color: Colors.black),),

        ],
      )*/

      Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(5.0),
            child: Icon(
              Icons.account_circle_outlined,
              size: 40,
            ),
          ),
          // Padding(padding: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0)),
          Expanded(
              flex: 6,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),

                  /*RichText(
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  strutStyle: StrutStyle(fontSize: 12.0),
                  text: TextSpan(
                    style: TextStyle(
                        fontWeight: FontWeight.w500, fontSize: 15, color: Colors.black),
                    text:'Name : ${value.name!}' ,
                  ),
                ),*/
                  MyWidgets.textView(
                      text: 'Name :${value.name!}',
                      maxLine: 1,
                      style: MyStyles.customTextStyle(
                          fontWeight: FontWeight.w500, fontSize: 15),
                      textAlign: TextAlign.left),

                  SizedBox(
                    height: 5,
                  ),
                  /*Text(
                'Salary : ${value.salary!}' ,
                style: TextStyle(
                  fontWeight: FontWeight.w300,
                  fontSize: 15.0,
                  color: Colors.black,
                ),
                textAlign: TextAlign.left,
                maxLines: 2,
              )*/
                  MyWidgets.textView(
                      text: 'Salary :${value.salary!}',
                      maxLine: 1,
                      style: MyStyles.customTextStyle(fontSize: 15),
                      textAlign: TextAlign.left),
                  SizedBox(
                    height: 10,
                  ),
                  // Padding(padding: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 10.0)),
                ],
              )),
          Expanded(
            flex: 4,
            child: MyWidgets.textView(
              text: selectedMin == null ? '0 Minutes' : '$selectedMin',
              style: MyStyles.customTextStyle(
                  fontWeight: FontWeight.w500, fontSize: 12),
            ),
          )
        ],
      ),
      Divider(
        height: 1,
      )
    ]);
  }

  void updateProductItemCost() {
    totalInventoryCost = 0.00;
    inventoryItems2.forEach((element) {
      totalInventoryCost = totalInventoryCost +
          (element.costPerTypeNew! / element.piecesInType!) *
              element.selectedQuantity!;
    });
  }

  void updateWorkerCost() {
    totalWorkersCost = 0.00;
    workers2.forEach((element) {
      if (element.selectedMins != null && element.selectedMins! > 0)
        totalWorkersCost = totalWorkersCost +
            (((element.salary!) / (element.hoursPerDay! * 30)) / 60) *
                element.selectedMins!;
    });
  }

  double? productItemSellingPrice = 0.0;
  double? productItemCost = 0.0;
  String? productItemName = '';
  int? productItemId;
  int? percentage = 0;
  ProductItem? item;

  Future<ProductItem> _getData() async {
    await productItemOperations
        .productItemsByProductItemId(widget.productItemId)
        .then((value) {
      item = value.first;
      if (item != null) {
        productItemId = item?.id;
        productItemName = item?.name;
        productItemImageBase64 = item?.imageBase;
        productItemSellingPrice = item?.sellingPrice;
        productItemCost = item?.cost;
        percentage = item?.priceByPercentage;
        _getDataSelectedInventoryItems();
      }
    });
    return item!;
  }

  ///Fetch data from database
  Future<List<InventoryItem>> _getDataSelectedInventoryItems() async {
    /* await inventoryItemOperations.inventoriesItems().then((value) {
      inventoryItems = value;
    });

    if (widget.productItem != null) {
      await relationOperations.relationsItems().then((value) {
        relations = value;
      });



      for (var inventoryItem in inventoryItems) {
        for (var relationItem in relations) {
          if (widget.productItem!=null&&widget.productItem?.id == relationItem.productItemId &&
              inventoryItem.id == relationItem.inventoryItemId) {
            inventoryItem.selectedQuantity=relationItem.inventoryItemQuantity;
            inventoryItems2.add(inventoryItem);

          }
        }



      }

    }*/

    if (widget.productItemId != null) {
      await inventoryItemOperations
          .inventoriesItemsByProductItem(widget.productItemId)
          .then((value) {
        inventoryItems2 = value;
        _getDataSelectedWorkersItems();
      });
    }
    //setState(() {updateProductItemCost();});
    return inventoryItems2;
  }

  ///Fetch data from database
  Future<List<Worker>> _getDataSelectedWorkersItems() async {
    /*await workersOperations.workers().then((value) {
      workers = value;
    });

    if (widget.productItem != null) {
      await workerRelationOperations.workerRelationsItems().then((value) {
        workRelations = value;

      });



      for (var worker in workers) {
        for (var workerRelation in workRelations) {
          if (widget.productItem!=null&&widget.productItem?.id == workerRelation.productItemId &&
              worker.id == workerRelation.workerId) {
            worker.selectedMins=workerRelation.workerHour;
            workers2.add(worker);

          }
        }



      }

    }*/
    await workersOperations
        .workersByProductItem(widget.productItemId)
        .then((value) {
      workers2 = value;
    });

    setState(() {
      isLoading = false;
      //updateProductItemCost();
      //updateWorkerCost();
    });
    return workers2;
  }

  addCartAndCartRelation() async {
    await MyPref.getProject().then((project) async => project == null
        ? {showCustomAlertDialog(context, 'Please create a project ')}
        : {
            if (checkInventoryStock().isEmpty)
              {
                if (checkPrice())
                  {
                    if (await cartRelationOperations
                        .productItemIdExistInCartRelation(productItemId))
                      {
                        context
                            .showSnackBar('Product item already exist in cart')
                      }
                    else
                      {
                        await cartOperations
                            .cartItemsByProjectId(project.id!)
                            .then((cartItem) async {
                          if (cartItem.isEmpty) {
                            var cart = Cart();
                            cart.projectId = project.id;
                            await cartOperations
                                .insertCart(cart)
                                .then((cartItem) async {
                              await insertIntoCartRelation(cartItem);
                            });
                          } else {
                            await insertIntoCartRelation(cartItem);
                          }
                        })
                      }
                  }
              }
            else
              {
                context.showSnackBar(
                    "${checkInventoryStock()} Stock not available")
              }
          });
  }

  insertIntoCartRelation(List<Cart> cartList) async {
    if (cartList.isNotEmpty) {
      var cartRelation = CartRelation();
      cartRelation.cartId = cartList.single.id;
      cartRelation.productItemId = productItemId;
      cartRelation.quantity = 1;
      await cartRelationOperations
          .insertCartRelation(cartRelation)
          .then((value) {
        showtoast('Product item added to the cart');
      });
    }
  }

  String checkInventoryStock() {
    var invNames = '';
    inventoryItems2.forEach((element) {
      if ((element.stock)! < element.selectedQuantity!) {
        invNames = invNames + ' ${element.name!}';
      }
    });
    return invNames.trim();
  }

  bool checkPrice() {
    if (item?.cost != null &&
        item?.sellingPrice != null &&
        (item?.cost)! > (item?.sellingPrice)!) {
      context.showSnackBar("Please check the price");
      return false;
    }
    return true;
  }
}
