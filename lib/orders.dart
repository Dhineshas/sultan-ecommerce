import 'dart:convert';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/inventoryUnitMeasures.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'database/DatabaseHelper.dart';
import 'database/history_operations.dart';
import 'database/inventory_item_operations.dart';
import 'database/orders_operations.dart';
import 'database/product_item_operations.dart';
import 'model/History.dart';
import 'model/InventoryItem.dart';
import 'model/Orders.dart';
import 'model/ProductItem.dart';
import 'orderInvoiceView.dart';

class OrdersScreenState extends StatefulWidget {
  const OrdersScreenState({Key? key}) : super(key: key);

  @override
  _OrdersScreenState createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrdersScreenState> {
  @override
  void setState(VoidCallback fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  @override
  void initState() {
    Future.delayed( Duration(milliseconds: 1000), () {
      _getData();});

    super.initState();
    print(
        "INIT  ${DateTime.now().millisecondsSinceEpoch}   ${DateTime.fromMillisecondsSinceEpoch(DateTime.now().millisecondsSinceEpoch)}");
  }

  var inventoryItemOperations = InventoryItemOperations();
  var _scrollController = ScrollController();
  bool isLoading = true;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(

      length: 3,
      child: Scaffold(
          backgroundColor: MyColors.white_f1_bg,
        appBar:PreferredSize(preferredSize: Size(MediaQuery.of(context).size.width,60),
        child:Container(margin:EdgeInsets.fromLTRB(10, 10, 10, 0),decoration: MyDecorations.whiteBGRounded10,child: TabBar(
          padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
          indicator: MyDecorations.greenShadowBGRounded10,
          tabs: [
            Tab(
              child: Text(
                'All',
                style: TextStyle(fontSize:12,color: Colors.black),
              ),
            ),
            Tab(
              child: Text(
                'Delivered',
                style: TextStyle(fontSize:12,color: Colors.black),
              ),
            ),
            Tab(
              child: Text(
                'Cancelled',
                style: TextStyle(fontSize:12,color: Colors.black),
              ),
            ),
          ],
        ),) ,) ,/*AppBar(
          backgroundColor: MyColors.WHITE_BG,
          bottom: TabBar(
            indicator: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.0, 1.0), //(x,y)
                  blurRadius: 6.0,
                ),
              ],
                borderRadius: BorderRadius.circular(10), // Creates border
                color: Colors.white),
            tabs: [
              Tab(
                child: Text(
                  'All',
                  style: TextStyle(color: Colors.black),
                ),
              ),
              Tab(
                child: Text(
                  'Delivered',
                  style: TextStyle(color: Colors.black),
                ),
              ),
              Tab(
                child: Text(
                  'Cancelled',
                  style: TextStyle(color: Colors.black),
                ),
              ),
            ],
          ),
          title: Text(
            'Orders',
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
        ),*/
        body:isLoading?MyWidgets.buildCircularProgressIndicator(): FadeInUp( child: TabBarView(
          physics: AlwaysScrollableScrollPhysics(
              parent: BouncingScrollPhysics()),
          children: [
            _listViewBuilder(),
            _listViewBuilder(1),
            _listViewBuilder(0),
          ],
        ),)
      ),
    );
  }

  Widget _listViewBuilder([int orderStatus=-1]) {
    if(orderStatus==-1&&allItems.isEmpty){
     return Center(child: Text(
        'No Orders',
        style: TextStyle(
          fontSize: 17.0,
          color: Colors.grey,
        ),
      ),);
    }else if(orderStatus==0&&canceledItems.isEmpty){
      return Center(child: Text(
        'No cancelled orders',
        style: TextStyle(
          fontSize: 17.0,
          color: Colors.grey,
        ),
      ),);
    }else if(orderStatus==1&&deliveredItems.isEmpty){
      return Center(child: Text(
        'No delivered Orders',
        style: TextStyle(
          fontSize: 17.0,
          color: Colors.grey,
        ),
      ),);
    } else
    return SafeArea(bottom: true,child:  ListView.builder(
        // key: _listKey,
        physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
        controller: _scrollController,
        shrinkWrap: true,
        itemCount:orderStatus==0? canceledItems.length:orderStatus==1?deliveredItems.length:allItems.length,
        itemBuilder: (BuildContext context, int index) {
          return _buildListItemNew(context, orderStatus==0? canceledItems[index]:orderStatus==1?deliveredItems[index]:allItems[index], index);
        }));
  }

  final orderIdDateFormat = DateFormat('ddMMMyyyyhhmmss');
  final purchaseDateFormat = DateFormat('dd-MMM-yyyy');

  Widget _buildListItem(BuildContext context, ProductItem values, int index) {
    return Container(
        margin: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
        decoration: BoxDecoration(
            border: Border.all(
              color: (Colors.teal[900])!,
            ),
            borderRadius: BorderRadius.all(Radius.circular(15))),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  //image
                  Container(
                    margin: EdgeInsets.fromLTRB(5, 5, 10, 5),
                    child: values.imageBase != null &&
                            values.imageBase.toString().isNotEmpty
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Image.memory(
                              base64Decode(values.imageBase.toString()),
                              width: 80,
                              height: 80,
                              fit: BoxFit.fitHeight,
                              gaplessPlayback: true,
                            ),
                          )
                        : Container(
                            decoration: BoxDecoration(
                                color: Colors.grey[200],
                                borderRadius: BorderRadius.circular(10)),
                            width: 80,
                            height: 80,
                          ),
                  ),
                  //name and count

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0)),
                      Text(
                        '#${orderIdDateFormat.format(DateTime.fromMillisecondsSinceEpoch(values.createdAt!))}',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 18.0,
                          color: Colors.black,
                        ),
                        textAlign: TextAlign.left,
                        maxLines: 2,
                      ),
                      Padding(padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),

                      Text(
                        'Order Date: ${purchaseDateFormat.format(DateTime.fromMillisecondsSinceEpoch(values.createdAt!))}',
                        style: TextStyle(
                          fontWeight: FontWeight.w300,
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        textAlign: TextAlign.left,
                        maxLines: 2,
                      ),
                      Padding(padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),
                      Text(
                        'Amount Paid: ${double.parse((values.sellingPrice! * values.orderQuantity!).toStringAsFixed(2))} QAR',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 15.0,
                          color: Colors.black,
                        ),
                        textAlign: TextAlign.left,
                        maxLines: 2,
                      )

                      //Padding(padding: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 10.0)),
                    ],
                  ),
                  Spacer(),

                  //Spacer()
                ]),
            values.orderStatus==0?Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                    flex: 1,
                    fit: FlexFit.tight,
                    child:
                    InkWell(onTap:(){
                      updateOrderStatus(false,values);
                    },child:  Stack(alignment: Alignment.center, children: <Widget>[
                      Container(
                        height: 35,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(14)),
                          color:values.orderStatus==2?(Colors.grey[400])!: (Colors.teal[900])!,
                        ), //BoxDecoration
                      ),
                      Text(
                        values.orderStatus==0||values.orderStatus==1?"Deliver Order":"Order Delivered",
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 15.0,
                          color: Colors.white,
                        ),
                      )
                    ]) //Container
                    ),),
                Flexible(
                    flex: 1,
                    fit: FlexFit.tight,
                    child:InkWell(onTap:(){
                      updateOrderStatus(true,values);
                    },child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                       Container(
                          height: 35,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(14)),
                            color:  values.orderStatus==1||values.orderStatus==2?(Colors.grey[400])!:(Colors.red[900])!,
                          ), //BoxDecoration
                        ),
                        Text(
                          values.orderStatus==1?"Order Canceled":"Cancel Order",
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 15.0,
                            color: Colors.white,
                          ),
                        )
                      ],
                    ) //Container
                    ))
              ],
            ):values.orderStatus==1?

               InkWell(onTap:(){
                      updateOrderStatus(true,values);
                    },child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        Container(
                          height: 35,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(14),
                                bottomLeft: Radius.circular(14)),
                            color:  (Colors.grey[400])!,
                          ), //BoxDecoration
                        ),
                        Text(
                          "Order Canceled",
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 15.0,
                            color: Colors.white,
                          ),
                        )
                      ],
                    ) //Container
                    )
              :values.orderStatus==2?


                  InkWell(onTap:(){
                    updateOrderStatus(false,values);
                  },child:  Stack(alignment: Alignment.center, children: <Widget>[
                    Container(
                      height: 35,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(14),
                            bottomLeft: Radius.circular(14)),
                        color:(Colors.grey[400])!,
                      ), //BoxDecoration
                    ),
                    Text(
                     "Order Delivered",
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 15.0,
                        color: Colors.white,
                      ),
                    )
                  ]) //Container
                  ,)


                :Container(),
          ],
        ));
  }

  Widget _buildListItemNew(BuildContext context, ProductItem values, int index) {
    return MyWidgets.shadowContainer(height:129,marginL:10,marginR:10,marginB:5,marginT:5,spreadRadius: 2, cornerRadius:10,shadowColor:MyColors.white_f1_bg,
        child:  Column(mainAxisSize:MainAxisSize.max,
            //crossAxisAlignment: CrossAxisAlignment.stretch,

            children: <Widget>[
            Expanded(flex:6,child:
            MyWidgets.shadowContainer(marginL:15,marginT:15,marginR:15,marginB:5,cornerRadius: 0,shadowColor:MyColors.white,spreadRadius: 0,blurRadius: 0,
                child: Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,

                    children: <Widget>[
                      Container(

                          width: 66.0,
                          height: 66.0,
                          child: values.imageBase != null &&
                              values.imageBase.toString().isNotEmpty
                          //don,t remove ClipRRect or don,t update ClipRRect with any other widget- image flicker on setstate
                              ?ClipRRect(
                            borderRadius: BorderRadius.circular(35),
                            child: Image.memory(
                              base64Decode(values.imageBase.toString()),
                              fit: BoxFit.cover,
                              gaplessPlayback: true,
                            ),
                          )
                              :ClipRRect(
                            borderRadius: BorderRadius.circular(35),
                            child: Container(color: MyColors.grey_70,
                            ),)
                      ),

                      SizedBox(width: 15,),
                      Expanded(flex:1,child:
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          //Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0)),

                          Text(
                            '#${orderIdDateFormat.format(DateTime.fromMillisecondsSinceEpoch(values.createdAt!))}',

                            style: MyStyles.customTextStyle(fontWeight: FontWeight.w400,fontSize: 17.0,
                              color: MyColors.black,),
                            textAlign: TextAlign.left,
                            maxLines: 2,
                          ),
                          // Padding(padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0)),
                          Spacer(),
                          Text(
                            'Date: ${purchaseDateFormat.format(DateTime.fromMillisecondsSinceEpoch(values.createdAt!))}',
                            style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 14.0,
                              color: MyColors.grey_9d_bg,
                            ),
                            textAlign: TextAlign.left,
                            maxLines: 2,
                          ),
                          Spacer(),

                          MyText.priceCurrencyText(context,price: 'Amount Paid: ${double.parse((values.sellingPrice! * values.orderQuantity!).toStringAsFixed(2))}',fontSize: 15,fontWeight: FontWeight.normal),
                          Spacer(),
                        values.orderStatus!=1? Column(mainAxisSize:MainAxisSize.max,children: [
                         GestureDetector(
                             onTap:(){
                               goto(context, OrderInvoicePDFScreen(selectedOrderCreatedAt: values.createdAt!, selectedOrderId: values.orderId!));

                             },child: Row(mainAxisSize:MainAxisSize.max,mainAxisAlignment:MainAxisAlignment.end,children: [
                          Icon(Icons.receipt_long_outlined,size: 15,), MyWidgets.textView(text: 'View Invoice')
                         ],) ),
                         ],):Container()
                          //Padding(padding: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 10.0)),
                        ],
                      ),)
                    ]) )),
            Expanded(flex:2,child:
            values.orderStatus==0?Row(

              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child:
                  InkWell(onTap:(){
                    updateOrderStatus(false,values);
                  },child:  Stack(alignment: Alignment.center, children: <Widget>[
                    MyWidgets.container(
                        bottomLeftCornerRadius: 10,color:values.orderStatus==2?MyColors.red_ff_bg: MyColors.white_chinese_e0_bg,
                    ),

                    Text(
                      values.orderStatus==0||values.orderStatus==1?"Deliver Order":"Order Delivered",
                      style:MyStyles.customTextStyle(fontWeight: FontWeight.w500,
                        fontSize: 15.0,
                        ),
                    )
                  ]) //Container
                  ),),
                Flexible(
                    flex: 1,
                    fit: FlexFit.tight,
                    child:InkWell(onTap:(){
                      updateOrderStatus(true,values);
                    },child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        MyWidgets.container(bottomRightCornerRadius: 10,
                          color:  values.orderStatus==1||values.orderStatus==2?MyColors.white_chinese_e0_bg:MyColors.red_ff_bg,),
                        Text(
                          values.orderStatus==1?"Order Canceled":"Cancel Order",
                          style: MyStyles.customTextStyle(fontWeight: FontWeight.w500,
                            fontSize: 15.0,
                              color: MyColors.white
                          )
                        )
                      ],
                    ) //Container
                    ))
              ],
            ):values.orderStatus==1?

            InkWell(onTap:(){
              updateOrderStatus(true,values);
            },child:
            MyWidgets.container(color:MyColors.red_ff_bg,bottomRightCornerRadius: 10,
                bottomLeftCornerRadius: 10,
                child:Center(child:Text(
                  "Order Canceled",
                  style: MyStyles.customTextStyle(fontWeight: FontWeight.w500,
                    fontSize: 15.0,
                    color: MyColors.white
                  ),
                )))
            //Container
            )
                :values.orderStatus==2?


            InkWell(onTap:(){
              updateOrderStatus(false,values);
            },child:
              MyWidgets.container(color:MyColors.white_chinese_e0_bg,bottomRightCornerRadius: 10,
                  bottomLeftCornerRadius: 10,child:
                  Center(child: Text(
                "Order Delivered",
                style: MyStyles.customTextStyle(fontWeight: FontWeight.w500,
                    fontSize: 15.0,
                ),
              )))

              ,)


                :MyWidgets.container(),)
          ],),
        );

  }

    var allItems = <ProductItem>[];
  var deliveredItems = <ProductItem>[];
  var canceledItems = <ProductItem>[];
  var productItemOperations = ProductItemOperations();
  Future<List<ProductItem>>_getData()async {

    await MyPref.getProject().then((project)async{
      if(project!=null){
        allItems.clear();
        deliveredItems.clear();
        canceledItems.clear();
        await productItemOperations.productItemsInOrdersByProjectId(project.id).then((value) {
         allItems = value;
          deliveredItems.addAll(allItems.where((element) => element.orderStatus == 2));
          canceledItems.addAll(allItems.where((element) => element.orderStatus == 1));
        });
      }else
        showCustomAlertDialog(context, 'Please create a project ',);
      setState(() {isLoading=false;});
    });


    return allItems;


  }
  var orderOperations = OrderOperations();
  Future<void> updateOrderStatus(bool cancelOrder,ProductItem productItem) async {
   if(productItem.orderStatus==0) {
     var order = Orders();
    order.id = productItem.orderId;
    order.orderStatus = cancelOrder==true ?1:2;
    await orderOperations.updateOrderStatus(order).then((value) => {
      if(cancelOrder){
        updateInventoryStock(productItem)
      }else{updateHistoryInventoryStock(productItem)}, _getData()});

   }else if(productItem.orderStatus==1) {
     //context.showSnackBar('Order already cancelled');
     showCustomAlertDialog(context, 'Order already cancelled');
   }else if(productItem.orderStatus==2) {
     //context.showSnackBar('Order already Delivered');
     showCustomAlertDialog(context, 'Order already Delivered');
   }

  }

  Future<void>updateInventoryStock(ProductItem productItem)async{
        await inventoryItemOperations.inventoriesItemsByProductItem(productItem.id).then((value)async {
          if(value.isNotEmpty){
            value.forEach((element)async {
             // if(((element.selectedQuantity!)*(productItem.orderQuantity!))>=(element.stock!)) {
              if(((element.selectedQuantity!)*(productItem.orderQuantity!))>0) {
                var inventoryItem = InventoryItem();
                inventoryItem.id=element.id;
                inventoryItem.stock=element.stock!+((element.selectedQuantity!)*(productItem.orderQuantity!));
                await inventoryItemOperations.updateInventoryItemStock(inventoryItem);

              }

            });
          }

        });
  }
  var historyItemOperations = HistoryOperations();
  Future<void> updateHistoryInventoryStock(ProductItem productItem) async {

        await inventoryItemOperations
            .inventoriesItemsByProductItem(productItem.id)
            .then((inventoryItems) {
          if (inventoryItems.isNotEmpty)
            inventoryItems.forEach((inventory)async {
              /*if (((inventory.selectedQuantity!) * (item.cartQuantity!)) <= (inventory.stock!)) {
                var inventoryItem = InventoryItem();
                inventoryItem.id = inventory.id;
                inventoryItem.stock = inventory.stock! - ((inventory.selectedQuantity!) * (item.cartQuantity!));
                await inventoryItemOperations
                    .updateInventoryItemStock(inventoryItem).then((value) => null).onError((error, stackTrace) {});

              }*/

              var history = History();
              history.inventoryItemId=inventory.id;
              history.historyItemType=inventory.itemType;
              history.historyTotalType=inventory.totalType;
              history.historyPiecesInType=inventory.piecesInType;
              history.historyCostPerType=inventory.costPerTypeNew;
              history.historyCostPerItemInType=inventory.costPerItemInTypeNew;
              history.historyFlag='sales';
              history.historyStock=((inventory.selectedQuantity!) * (productItem.orderQuantity!));
              await historyItemOperations.insertHistory(history).then((value) {

              });

              print('order_product_inventories ${inventory.name} ${inventory.totalType} ${inventory.selectedQuantity} ${productItem.orderQuantity}');
            });

        }).onError((error, stackTrace){});


  }

}
