import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/homeLineChart.dart';
import 'package:ecommerce/model/Project.dart';
import 'package:ecommerce/model/homeGridItems.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Subscription.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'database/home_operations.dart';

class HomeScreen extends StatefulWidget {
  final bool? updateHomeInit;
  final Function? addMainProject;

  const HomeScreen({
    Key? key,
    @required this.updateHomeInit,
    @required this.addMainProject,
  }) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    //downloadFile();
    print('handleInitFromMain ${widget.updateHomeInit}');
    if (widget.updateHomeInit != null && widget.updateHomeInit == true) {
      _getData();
    }

    super.initState();
  }

  @override
  void updateHomeState() {
    _getData();
  }



  @override
  Widget build(BuildContext context) {
    return MyWidgets.scaffold(body:SafeArea(bottom :true,child:  createView()));
  }

  ///Fetch data from database
  Future<int> _getData() async {
    await MyPref.getProject().then((value) {
      project = value;
      // print('getProjectData $value');
      setState(() {
        if (value == null) {
          projectStatus = 1;
        } else {
          projectStatus = 2;
          _getHomeData();

        }
      });
    });
    return projectStatus!;
  }

  Widget createView() {
    print('projectStatusSnapshot  $projectStatus');
    if (projectStatus == 1) {
      return _buildEmptyProjectWidget();
    } else if (projectStatus == 2) {
      return Container(
          height: double.infinity,
          width: double.infinity,
          child: FadeIn(
              child: ListView.builder(
                  physics: AlwaysScrollableScrollPhysics(
                      parent: BouncingScrollPhysics()),
                  shrinkWrap: true,
                  itemCount: _listOption.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _buildListItem(context, index);
                  })));
    } else
      return MyWidgets.buildCircularProgressIndicator();
  }

  List<HomeGridItems> _gridOptions = <HomeGridItems>[
    /* HomeGridItems(backgroundColor: MyColors.platinum_e4_bg,iconData: Icons.point_of_sale_sharp,title: 'Total Sales',value: 0.0),
    HomeGridItems(backgroundColor: MyColors.grey_bright_ef_bg,iconData: Icons.currency_bitcoin_rounded,title: 'Items Cost',value: 0.0),
    HomeGridItems(backgroundColor: MyColors.white_dutch_ec_bg,iconData: Icons.handshake_rounded,title: 'Salaries',value: 0.0),
    HomeGridItems(backgroundColor: MyColors.platinum_ec_bg,iconData: Icons.currency_bitcoin_rounded,title: 'Extra Cost',value: 0.0),*/
  ];
  List<HomeGridItems> _listOption = <HomeGridItems>[];

  Widget _buildListItem(BuildContext context, int index) {

    //list view items 0 index profit view and 1 index grid view
    return Padding(
        padding: const EdgeInsets.fromLTRB(12, 10, 12, 5),
        child:
        index==0?
        productProfitFlSpotList.isNotEmpty?
        Column(mainAxisSize:MainAxisSize.max,crossAxisAlignment: CrossAxisAlignment.center,children: [
          SizedBox(height: 15,),
          MyWidgets.container(color:MyColors.transparent,paddingR:15,height : 200,child: LineChart(
            sampleData2,
            swapAnimationDuration: const Duration(milliseconds: 250),
          ))
        ],):Container():
        index == 1
            ? _listOption.isNotEmpty
                ? MyWidgets.shadowContainer(
                    height: 140,
                    color: MyColors.white_chinese_e0_bg,
                    cornerRadius: 10,
                    shadowColor: MyColors.white_f1_bg,
                    blurRadius: 0,
                    spreadRadius: 0,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                            child: Align(
                                alignment: Alignment.centerLeft,
                                child: Padding(
                                    padding: const EdgeInsets.all(15),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Icon(
                                          _listOption[index].iconData,
                                          size: 30,
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          _listOption[index].title!,
                                          style: MyStyles.customTextStyle(
                                              fontSize: 18,
                                              color: MyColors.grey_70,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    )))),
                        Expanded(
                            child: Align(
                                alignment: Alignment.center,
                                child: MyText.priceCurrencyText(context,
                                    price: '${_listOption[index].value}',
                                    fontSize: 20))),
                        Expanded(child: Container())
                      ],
                    ))
                : Container()
            //home grid view
            :

        GridView.builder(
                physics: const NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 1.15,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20),
                itemCount: _gridOptions.length,
                itemBuilder: (BuildContext ctx, index) {
                  return MyWidgets.shadowContainer(
                      color: _gridOptions[index].backgroundColor,
                      cornerRadius: 10,
                      shadowColor: MyColors.white_f1_bg,
                      blurRadius: 0,
                      spreadRadius: 0,
                      child: Padding(
                          padding: const EdgeInsets.all(15),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Icon(
                                _gridOptions[index].iconData!,
                                size: 30,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                _gridOptions[index].title!,
                                style: MyStyles.customTextStyle(
                                    fontSize: 18,
                                    color: MyColors.grey_70,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              MyText.priceCurrencyText(context,
                                  price: '${_gridOptions[index].value}',
                                  fontSize: 20)
                            ],
                          )));
                })

    );
  }

  double? totalProfit = 0.00;
  double? totalExtraCost = 0.00;
  double? totalSalary = 0.00;
  double? totalCost = 0.00;
  double? totalSellPrice = 0.00;
  var homeOperations = HomeOperations();



  Future _getHomeData() async {
    print('selectProjectId ${project?.id}');
    if (project != null)
      await homeOperations.getHomeData(project?.id,currentYear).then((value)async {
        print('selectedProjectData ${value?.totalSalary}');

          totalProfit = value?.totalProfit;
          totalExtraCost = value?.totalExpense;
          totalSalary = value?.totalSalary;
          totalCost = value?.totalCost;
          totalSellPrice = value?.totalSelling;

          _listOption.clear();
          _gridOptions.clear();

          //dummy HomeGridItems() for parent listview builder item count with profit and nested grid view
          _listOption.add(
            HomeGridItems(),
          );
          _listOption.add(
            HomeGridItems(
                backgroundColor: MyColors.white_chinese_e0_bg,
                iconData: Icons.currency_pound_rounded,
                title: 'Profit',
                value: tryToDouble(value: totalProfit)),
          );
          _listOption.add(
            HomeGridItems(),
          );

          _gridOptions.add(
            HomeGridItems(
                backgroundColor: MyColors.platinum_e4_bg,
                iconData: Icons.multiline_chart_rounded,
                title: 'Total Sales',
                value: tryToDouble(value: totalSellPrice)),
          );
          _gridOptions.add(
            HomeGridItems(
                backgroundColor: MyColors.grey_bright_ef_bg,
                iconData: Icons.currency_bitcoin_rounded,
                title: 'Items Cost',
                value: tryToDouble(value: totalCost)),
          );
          _gridOptions.add(
            HomeGridItems(
                backgroundColor: MyColors.white_dutch_ec_bg,
                iconData: Icons.handshake_rounded,
                title: 'Salaries',
                value: tryToDouble(value: totalSalary)),
          );
          _gridOptions.add(
            HomeGridItems(
                backgroundColor: MyColors.platinum_ec_bg,
                iconData: Icons.currency_ruble_rounded,
                title: 'Extra Cost',
                value: tryToDouble(value: totalExtraCost)),
          );
        await _getProductGraphData(project?.id);

      });

  }

  Project? project;
  int? projectStatus = 0;

  Widget _buildEmptyProjectWidget() {
    return Center(
        child: SingleChildScrollView(
            child: FadeIn(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 36,
            width: 82,
            child: FittedBox(
              child: Image.asset('assets/images/slogan_oops_.png'),
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 21,
            width: 138,
            child: FittedBox(
              child: Image.asset('assets/images/slogan_no_project.png'),
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(height: 71),
          Container(
            height: 198.5,
            width: 203.3,
            child: FittedBox(
              child: Image.asset('assets/images/undraw_no_data.png'),
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(height: 64),
          ElevatedButton.icon(
            icon: const Icon(
              Icons.add_circle,
              color: Colors.white,
            ),
            onPressed: () {
              /*gotoAddProject(null);*/ widget.addMainProject!();
            },
            label: Text(
              "Create Project",
              style:
                  MyStyles.customTextStyle(fontSize: 13, color: MyColors.white),
            ),
            style: ElevatedButton.styleFrom(
              primary: MyColors.black_0101,
              fixedSize: const Size(295, 44),
              shape: MyDecorations.roundedRectangleBorder(8),
            ),
          )
        ],
      ),
    )));
  }












  LineChartData get sampleData2 => LineChartData(
    lineTouchData: lineTouchData2,
    gridData: gridData,
    titlesData: titlesData2,
    borderData: borderData,
    lineBarsData: lineBarsData2,
    minX: 0,
    maxX: 11,
    maxY: 5,
    minY: 0,
  );

  LineTouchData get lineTouchData2 => LineTouchData(
    enabled: false,
  );

  FlTitlesData get titlesData2 => FlTitlesData(
    bottomTitles: AxisTitles(
      sideTitles: bottomTitles,
    ),
    rightTitles: AxisTitles(
      sideTitles: SideTitles(showTitles: false),
    ),
    topTitles: AxisTitles(
      sideTitles: SideTitles(showTitles: false),
    ),
    leftTitles: AxisTitles(
      sideTitles: leftTitles(),
    ),
  );

  List<LineChartBarData> get lineBarsData2 => [
    lineChartBarDataProfit,
    lineChartBarDataSales,
    lineChartBarDataInventoryCost
  ];

  Widget leftTitleWidgets(double value, TitleMeta meta) {
    print('y_values $value');
    const style = TextStyle(
      color: MyColors.black,
      fontWeight: FontWeight.normal,
      fontSize: 14,
    );
    String text;
    switch (value.toInt()) {
      case 0:
        text = '0%';
        break;
      case 1:
        text = '20%';
        break;
      case 2:
        text = '40%';
        break;
      case 3:
        text = '60%';
        break;
      case 4:
        text = '80%';
        break;
      case 5:
        text = '100%';
        break;
      default:
        return Container();
    }

    return Text(text, style: style, textAlign: TextAlign.center);
  }

  SideTitles leftTitles() => SideTitles(
    getTitlesWidget: leftTitleWidgets,
    showTitles: true,
    interval: 1,
    reservedSize: 40,
  );


  Widget bottomTitleWidgets(double value, TitleMeta meta) {
    print('x_values $value');
    const style = TextStyle(
      color: MyColors.black,
      fontWeight: FontWeight.normal,
      fontSize: 14,
    );
    Widget text;

    text =  Text('${value.toInt()+1}', style: style);

    return SideTitleWidget(
      axisSide: meta.axisSide,
      space: 10,
      child: text,
    );
  }

  SideTitles get bottomTitles => SideTitles(
    showTitles: true,
    reservedSize: 32,
    interval: 1,
    getTitlesWidget: bottomTitleWidgets,
  );


  FlGridData get gridData => FlGridData(show: true,);


  FlBorderData get borderData =>  FlBorderData(
      show: true,
      border: Border.all(color: MyColors.grey_607, width: 1));


  LineChartBarData get lineChartBarDataProfit => LineChartBarData(
      isCurved: true,
      curveSmoothness: 0,
      color: MyColors.white_chinese_e0_bg,
      barWidth: 2,
      isStrokeCapRound: true,
      dotData: FlDotData(show: true),
      belowBarData: BarAreaData(show: false),
      spots:productProfitFlSpotList
  );

  LineChartBarData get lineChartBarDataSales=> LineChartBarData(
      isCurved: true,
      curveSmoothness: 0,
      color: MyColors.platinum_e4_bg,
      barWidth: 2,
      isStrokeCapRound: true,
      dotData: FlDotData(show: true),
      belowBarData: BarAreaData(show: false),
      spots:productSalesFlSpotList
  );
  LineChartBarData get lineChartBarDataInventoryCost=> LineChartBarData(
      isCurved: true,
      curveSmoothness: 0,
      color: MyColors.grey_bright_ef_bg,
      barWidth: 2,
      isStrokeCapRound: true,
      dotData: FlDotData(show: true),
      belowBarData: BarAreaData(show: false),
      spots:inventoryCostFlSpotList
  );


  var productProfitFlSpotList = <FlSpot>[];
  var productSalesFlSpotList = <FlSpot>[];
  var inventoryCostFlSpotList = <FlSpot>[];
  Future<void> _getProductGraphData(int? projectId) async {
   final activeSubscription= await Subscription.getActiveSubscription();
   print('homeActiveSubscription  $activeSubscription');
    if (projectId != null&&activeSubscription==1||activeSubscription==2) {
      await homeOperations
          .productItemsGrossAndNetInOrdersByProjectId(
          projectId, currentYear)
          .then((value) async {
        productProfitFlSpotList.clear();
        productSalesFlSpotList.clear();
        inventoryCostFlSpotList.clear();
        //profitFlSpotList = List<FlSpot>.generate(12, (i) => FlSpot(i.toDouble(), 0.0));
        for (double i = 0; i <= 11; i++) {
          productProfitFlSpotList.add(FlSpot(i, 0.0));
          productSalesFlSpotList.add(FlSpot(i, 0.0));
          inventoryCostFlSpotList.add(FlSpot(i, 0.0));
        }
        if (value.isNotEmpty) {
          value.forEach((element) {
            print('order_profit_products ${element.name} ${element
                .orderGrossTotal}  ${element.orderNetTotal}   ${element
                .orderQuantityTotal}  ${element
                .orderTotalProductCost}  ${element
                .orderTotalProductProfit}  ${element.orderMonth}  ${element
                .orderYear}');
            if (element.orderGrossTotal! > 0) {
              var profitPercentage = (element.orderTotalProductProfit! /
                  element.orderGrossTotal!) * 100;
              //make % value to rage between 0 to 5
              var profitPercentageToDecimal = (profitPercentage * 5) / 100;
              productProfitFlSpotList[element.orderMonth!.toInt() - 1] = FlSpot(
                  element.orderMonth!.toInt() - 1, profitPercentageToDecimal);

              var salesPercentage = (element.orderTotalProductCost! /
                  element.orderGrossTotal!) * 100;
              //make % value to rage between 0 to 5
              var salesPercentageToDecimal = (salesPercentage * 5) / 100;

              productSalesFlSpotList[element.orderMonth!.toInt() - 1] = FlSpot(
                  element.orderMonth!.toInt() - 1, salesPercentageToDecimal);
            }
          });
        }
      });

      await _getInventoryGraphData(projectId);
return;
    }else
      setState(() {
        productProfitFlSpotList.clear();
      productSalesFlSpotList.clear();
      inventoryCostFlSpotList.clear(); });


  }
  Future<void> _getInventoryGraphData(int? projectId) async {
    await homeOperations.inventoriesItemPurchaseHistory(projectId,currentYear).then((value)async{
      if(value.isNotEmpty){
      double? total = value.map((e) => e.inventoryTotalPurchaseCost).reduce((a, b)  => a! + b!)??0.0;
      value.forEach((element) {
        if (element.inventoryTotalPurchaseCost! > 0) {
          var inventoryPurchasePercentage = ( element.inventoryTotalPurchaseCost!/total) * 100;
          var purchasePercentageToDecimal = (inventoryPurchasePercentage * 5) / 100;

          inventoryCostFlSpotList[element.historyMonth!.toInt() - 1] = FlSpot(element.historyMonth!.toInt() - 1, purchasePercentageToDecimal);
        }
        print('inventory_purchase_data  $total ${element.inventoryTotalPurchaseCost}   ${element.historyInventoryItemTotalType}   ${element.historyMonth}');

      });}
      setState(() { });
    } );
  }

  }
