import 'dart:convert';
import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/allWorkersList.dart';
import 'package:ecommerce/database/worker_relation_operations.dart';
import 'package:ecommerce/database/workers_operations.dart';
import 'package:ecommerce/model/Worker.dart';
import 'package:ecommerce/model/WorkerRelation.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'allInventotyItemListNew.dart';
import 'database/inventory_item_operations.dart';
import 'database/product_item_operations.dart';
import 'database/relation_operations.dart';
import 'inventoryUnitMeasures.dart';
import 'model/InventoryItem.dart';
import 'package:image_picker/image_picker.dart';

import 'model/ProductCategory.dart';
import 'model/ProductItem.dart';
import 'model/InventoryRelation.dart';
import 'model/Project.dart';

class AddProductNewScreenState extends StatefulWidget {
  final ProductCategory? productCategory;
  final int? productItemId;

  const AddProductNewScreenState(
      {@required this.productCategory, @required this.productItemId, Key? key})
      : super(key: key);

  @override
  _AddProductNewScreenState createState() => _AddProductNewScreenState();
}

class _AddProductNewScreenState extends State<AddProductNewScreenState> {
  File? _image;
  final picker = ImagePicker();
  final tePrdNameController = TextEditingController();
  final tePrdCostController = TextEditingController();
  final tePrdPriceController = TextEditingController();
  final teAlertDialogQtyController = TextEditingController();

  var unitMeasure = InventoryUnitMeasure();
  var productItemOperations = ProductItemOperations();
  var relationOperations = RelationOperations();
  var workerRelationOperations = WorkerRelationOperations();
  var inventoryItemOperations = InventoryItemOperations();
  var workersOperations = WorkersOperations();
  var productItemImageBase64;
  var totalInventoryCost = 0.00;
  var totalWorkersCost = 0.00;
  bool isLoading = true;
  Project? project;

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {

    MyPref.getProject().then((project) async {
      this.project = project;
      if (project != null) {
        if (widget.productItemId != null) {
          /*tePrdNameController.text = '${widget.productItem?.name}';
      tePrdPriceController.text = '${(widget.productItem?.sellingPrice)}';
      productItemImageBase64 = widget.productItem?.imageBase;
      percentage=widget.productItem?.priceByPercentage!;
      Future.delayed( Duration(milliseconds: 1000), () {
      _getDataSelectedInventoryItems();});*/
          _getData();
        } else
          setState(() {
            isLoading = false;
          });
      }
    });
    //isLoading=false;

    super.initState();
  }

  ProductItem? item;

  Future<ProductItem> _getData() async {
    await productItemOperations
        .productItemsByProductItemId(widget.productItemId)
        .then((value) {
      item = value.first;
      if (item != null) {
        tePrdNameController.text = '${item?.name}';
        tePrdPriceController.text = '${(item?.sellingPrice)}';
        productItemImageBase64 = item?.imageBase;
        percentage = item?.priceByPercentage!;
        Future.delayed(Duration(milliseconds: 1000), () {
          _getDataSelectedInventoryItems();
        });
      }
    });
    return item!;
  }

  @override
  Widget build(BuildContext context) {
    final imageView = /*FadeInUp(delay: Duration(milliseconds: 100),child:
 GestureDetector(
        onTap: () {
          _showPicker(context);
        },
        child: Padding(
            padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(10)),
                height: 150,
                width: MediaQuery.of(context).size.width,
                child: productItemImageBase64 != null
                    ? ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.memory(
                          base64Decode('${productItemImageBase64}'),
                          width: 100,
                          height: 100,
                          gaplessPlayback: true,
                          fit: BoxFit.fitHeight,
                        ),
                      )
                    : _image != null
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Image.file(_image!,
                                width: 100,
                                height: 100,
                                fit: BoxFit.fitHeight,
                                gaplessPlayback: true),
                          )
                        : Container(
                            decoration: BoxDecoration(
                                color: Colors.grey[200],
                                borderRadius: BorderRadius.circular(10)),
                            width: 100,
                            height: 100,
                            child: Icon(
                              Icons.camera_alt,
                              color: Colors.grey[800],
                            ),
                          ))),
      ),
    )*/

        GestureDetector(
            onTap: () {
              _showPicker(context);
            },
            child: Container(
              decoration: BoxDecoration(
                  // color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(75)),
              height: 104,
              width: 104,
              child: productItemImageBase64 != null
                  ? ClipRRect(
                      borderRadius: BorderRadius.circular(75),
                      child: Image.memory(
                        base64Decode('$productItemImageBase64'),
                        width: 100,
                        height: 100,
                        fit: BoxFit.cover,
                        gaplessPlayback: true,
                      ),
                    )
                  : _image != null
                      ? ClipRRect(
                          borderRadius: BorderRadius.circular(75),
                          child: Image.file(
                            _image!,
                            width: 100,
                            height: 100,
                            fit: BoxFit.cover,
                            gaplessPlayback: true,
                          ),
                        )
                      : Container(
                          decoration: BoxDecoration(
                              // color: Colors.grey[200],
                              border: Border.all(color: Colors.black, width: 1),
                              borderRadius: BorderRadius.circular(75)),
                          width: 100,
                          height: 100,
                          child: Icon(
                            Icons.image_rounded,
                            color: Colors.grey[800],
                          ),
                        ),
            ));

    final productNameEditTxt2 = Align(
        alignment: Alignment.topLeft,
        child: ConstrainedBox(
          constraints: BoxConstraints(minWidth: 20),
          child: IntrinsicWidth(
            child: TextFormField(
                style: MyStyles.customTextStyle(
                    fontSize: 20, fontWeight: FontWeight.bold),
                keyboardType: TextInputType.name,
                textCapitalization: TextCapitalization.words,
                controller: tePrdNameController,
                obscureText: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      //borderRadius:  BorderRadius.circular(0),
                      borderSide: BorderSide.none),
                  suffixIcon: Icon(
                    Icons.edit,
                    size: 20,
                  ),
                  hintText: 'Name',
                  contentPadding: EdgeInsets.only(top: 15, bottom: 15),
                  fillColor: MyColors.white,
                  filled: true,
                )),
          ),
        ));

    /* final addDuration = Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child:
        InkWell(
            onTap: () {
              showCupertinoModalPopup<void>(
                  context: context, builder: (BuildContext context){
                return _buildBottomPicker(timePicker());
              });
            },child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.baseline,
          textBaseline: TextBaseline.alphabetic,
          children: [Text('Preparation Time : ',style: TextStyle(
            fontWeight: FontWeight.bold, fontSize: 15, color: Colors.black),), Text(time==null?'0 Minutes':time,style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 12, color: Colors.black),)],
        ),)
        );*/


    final selectWorkersDuration =
    Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text: 'Preparation Time:'),
          SizedBox(height: 10,),
          MyWidgets.textViewContainer(
            width: MediaQuery
        .of(context)
        .size
        .width,
            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
              paddingL: 15,
            paddingR: 15,
            child: InkWell(
              child:Align(alignment:Alignment.centerLeft,child:MyWidgets.textView(
                textAlign: TextAlign.start,
                text:selectedTimeText==null? '0 Minutes':"$selectedTimeText",
                style: MyStyles.customTextStyle(fontSize: 12),
              ) )/*,Text('${(value.salary!/30)/(value.hoursPerDay!)/60}')*/,
              onTap: () {
                workers.isEmpty ?context.showSnackBar('Please add workers first.'):
                showCupertinoModalPopup<void>(
                    context: context,
                    builder: (BuildContext context) {
                      return _buildBottomPicker(timePicker2());
                    });
              },
            )
            )
        ]
    );


    final addInventoryItem = InkWell(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [Icon(Icons.add_circle), Text('Add/Remove Items')],
      ),
      onTap: () {
        /*Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => AllInventoryItemListScreenState(
                          productItem: widget.productItem != null
                              ? widget.productItem
                              : null,
                          selectedInventoryItem: inventoryItems,
                        ))).then((value) => {
                  setState(() {
                    if (value != null) {
                      inventoryItems.clear();
                      inventoryItems.addAll(value);
                      updateProductItemCost();
                    }
                  })
                });*/
        MyBottomSheet().showBottomSheet(context, maxHeight: 0.80,
            callback: (value) {
          setState(() {
            if (value != null) {
              inventoryItems.clear();
              inventoryItems.addAll(value);
              updateProductItemCost();
            }
          });
        },
            child: AllInventoryItemListNewScreenState(
              productItem: item != null ? item : null,
              selectedInventoryItem: inventoryItems,
            ));
      },
    );
    final selectedInventoryItemListVIew = ListView.builder(
        key: _inventoryListKey,
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: inventoryItems.length,
        itemBuilder: (BuildContext context, int index) {
          return _buildItem(context, inventoryItems[index], index);
        });

    final borderContainerWithAddItemAndItemListView = Container(
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
            color: MyColors.white,
            border: Border.all(color: MyColors.grey_70, width: 0.5),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: 250,
          ),
          child: Align(
              alignment: inventoryItems.isEmpty
                  ? Alignment.center
                  : Alignment.topCenter,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [addInventoryItem, selectedInventoryItemListVIew],
              )),
        ));

    final addWorker = InkWell(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [Icon(Icons.add_circle), Text('Add/Remove Workers')],
      ),
      onTap: () {
        /*Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => AllWorkersListScreenState(productItem: item != null
                        ? item
                        : null,selectedWorkers: workers,))).then((value) => {
              setState(() {
                if (value != null) {
                  workers.clear();
                  workers.addAll(value);
                  updateWorkerCost();
                }
              })
            });*/

        MyBottomSheet().showBottomSheet(context, maxHeight: 0.80,
            callback: (value) {
          setState(() {
            if (value != null) {
              workers.clear();
              workers.addAll(value);
              setDurationForAllWorkers(selectedTimeInt);


            }
          });
        },
            child: AllWorkersListScreenState(
              productItem: item != null ? item : null,
              selectedWorkers: workers,
            ));
      },
    );

    final selectedWorkersListVIew = ListView.builder(
        key: _workerListKey,
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: workers.length,
        itemBuilder: (BuildContext context, int index) {
          return _buildWorkerItem(context, workers[index], index);
        });
    final borderContainerWithAddWorkerAndWorkerListView = Container(
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
            color: MyColors.white,
            border: Border.all(color: MyColors.grey_70, width: 0.5),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: 150,
          ),
          child: Align(
              alignment:
                  workers.isEmpty ? Alignment.center : Alignment.topCenter,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [addWorker, selectedWorkersListVIew],
              )),
        ));
    final addInvButton = /*FadeInUp(delay: Duration(milliseconds: 600),child:Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(10.0),
          color: (Colors.teal[900])!,
          child: MaterialButton(
            minWidth: MediaQuery.of(context).size.width,
            onPressed: () {
              */ /*_validate() ? addProductItem() : {};*/ /*

              if (_validate()) {
                widget.productItem != null
                    ? updateProductItemWithTransaction()//updateProductItem()
                    : //addProductItem();
                addProductItemWithTransaction();
              }
            },
            child: Text(
              widget.productItem != null
                  ? 'Update Product Item'
                  : 'Add Product Item',
              textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white)
            ),
          ),
        )));*/
        MyWidgets.materialButton(context,
            text: item != null ? 'Update Product Item' : 'Add Product Item',
            onPressed: () {
      if (_validate()) {
        item != null
            ? updateProductItemWithTransaction() //updateProductItem()
            : //addProductItem();
            addProductItemWithTransaction();
      }
    });
    tePrdCostController.text =
        '${double.parse(((totalInventoryCost + totalWorkersCost)).toString())} QR';
    final addCostEditTxt = Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            Flexible(
                flex: 6,
                fit: FlexFit.tight,
                child: /*Text(
                  'COST : ',
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 18,
                      color: Colors.black),
                )*/
                    MyWidgets.textView(
                        text: 'Cost: ', textAlign: TextAlign.start)),
            Flexible(
                flex: 5,
                fit: FlexFit.tight,
                child:
                    /*TextFormField(
                    autofocus: false,
                    readOnly: true,
                    keyboardType: TextInputType.name,
                    textCapitalization: TextCapitalization.words,
                    controller: tePrdCostController,
                    obscureText: false,
                    decoration: InputDecoration(
                      isDense: true,
                      hintText: "Cost",
                    ))*/
                    MyWidgets.textViewContainer(
                  height: 35,
                  cornerRadius: 8,
                  color: MyColors.white,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: MyWidgets.textFromField(
                      readOnly: true,
                      contentPaddingL: 15.0,
                      contentPaddingR: 15.0,
                      cornerRadius: 8,
                      keyboardType: TextInputType.name,
                      controller: tePrdCostController,
                    ),
                  ),
                ) //Container
                )
          ],
        ));

    final addPriceEditTxt = Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            Flexible(
                flex: 6,
                fit: FlexFit.tight,
                child: /*Text(
                  'PRICE : ',
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 18,
                      color: Colors.black),
                ) */ //Container
                    MyWidgets.textView(
                        text: 'Price: ', textAlign: TextAlign.start)),
            Flexible(
                flex: 5,
                fit: FlexFit.tight,
                child: /*Stack(
                  children: [
                    TextFormField(
                    keyboardType:
                    TextInputType.numberWithOptions(decimal: true),
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(
                          RegExp(r'^\d+\.?\d{0,2}')),
                    ],
                    controller: tePrdPriceController,
                    obscureText: false,
                    decoration: InputDecoration(
                      isDense: true,
                      hintText: "Price",
                    ),onChanged:(text){
                      if(text.isNotEmpty&&double.tryParse(text)!>0&&percentage!>0){
                      setState(() {
                        percentage=0;
                      });}
                  },),
                  Align(alignment:Alignment.centerRight,
                    child:InkWell(onTap:(){

                      if((totalInventoryCost+totalWorkersCost)>0)
                      showCupertinoModalPopup<void>(
                          context: context, builder: (BuildContext context){
                        return _buildBottomPicker(percentPicker());
                      });
                    },child:Padding(
                        padding:const EdgeInsets.fromLTRB(10, 0,18, 0),child:Text(
                      percentage==0?'%':'$percentage%',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 19,
                          color: Colors.blue),
                    ) ,)  ),)],)*/

                    MyWidgets.textViewContainer(
                  height: 35, cornerRadius: 8, color: MyColors.white,
                  // color: Colors.white54,
                  child: Row(children: [
                    Expanded(
                        flex: 1,
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: TextFormField(
                            keyboardType:
                                TextInputType.numberWithOptions(decimal: true),
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                  RegExp(r'^\d+\.?\d{0,2}')),
                            ],
                            controller: tePrdPriceController,
                            obscureText: false,
                            decoration: InputDecoration(
                              isDense: true,
                              hintText: "Price",
                              contentPadding:
                                  EdgeInsets.only(left: 15, right: 15),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8),
                                  borderSide: BorderSide.none),
                              filled: true,
                              fillColor: MyColors.white,
                            ),
                            onChanged: (text) {
                              if (text.isNotEmpty &&
                                  double.tryParse(text)! > 0 &&
                                  percentage! > 0) {
                                setState(() {
                                  percentage = 0;
                                });
                              }
                            },
                          ),
                        )),
                    InkWell(
                        onTap: () {
                          if ((totalInventoryCost + totalWorkersCost) > 0)
                            showCupertinoModalPopup<void>(
                                context: context,
                                builder: (BuildContext context) {
                                  return _buildBottomPicker(percentPicker());
                                });
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(10, 0, 18, 0),
                          child: /*Text(
                     percentage==0?'%':'$percentage%',
                     style: TextStyle(
                         fontWeight: FontWeight.bold,
                         fontSize: 19,
                         color: Colors.blue),
                   )*/
                              MyWidgets.textView(
                                  text: percentage == 0 ? '%' : '$percentage%',
                                  style:
                                      MyStyles.customTextStyle(fontSize: 16)),
                        ))
                  ]),
                )),
          ],
        ));

    final costPriceContainer = MyWidgets.shadowContainer(
        height: 95,
        cornerRadius: 10,
        color: MyColors.white_chinese_e0_bg,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Spacer(),
            addCostEditTxt,
            Spacer(),
            addPriceEditTxt,
            Spacer(),
          ],
        ));
    return Scaffold(
        backgroundColor: MyColors.white,
        appBar: MyWidgets.appBar(
          color: MyColors.white,
          leading: IconButton(
            color: MyColors.black,
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
          ),
        ),
        body: !isLoading
            ? SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(
                    parent: BouncingScrollPhysics()),
                padding:
                    EdgeInsets.only(left: 30, right: 30, top: 10, bottom: 10),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    productNameEditTxt2,
                    SizedBox(
                      height: 5,
                    ),
                    imageView,
                    SizedBox(
                      height: 15,
                    ),
                    selectWorkersDuration,
                    SizedBox(
                      height: 15,
                    ),
                    borderContainerWithAddWorkerAndWorkerListView,
                    SizedBox(
                      height: 15,
                    ),
                    borderContainerWithAddItemAndItemListView,

                    SizedBox(
                      height: 15,
                    ),
                    costPriceContainer,
                    SizedBox(
                      height: 15,
                    ),
                    addInvButton,
                    SizedBox(
                      height: 15,
                    )
                  ],
                ))
            : MyWidgets.buildCircularProgressIndicator());
  }

  var inventoryItems = <InventoryItem>[];
  var workers = <Worker>[];
  final GlobalKey<AnimatedListState> _inventoryListKey = GlobalKey();
  final GlobalKey<AnimatedListState> _workerListKey = GlobalKey();
  var _scrollController = ScrollController();
  var relations = <InventoryRelation>[];
  var workRelations = <WorkerRelation>[];

  ///Construct cell for List View
  Widget _buildItem(BuildContext context, InventoryItem value, int index) {
    final teItemQtyController = TextEditingController();
    teItemQtyController.text = '${value.selectedQuantity}';
    return Column(children: <Widget>[
      Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(4.0),
            child:
                value.imageBase != null && value.imageBase.toString().isNotEmpty
                    ? ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.memory(
                          base64Decode(value.imageBase.toString()),
                          width: 40,
                          height: 40,
                          gaplessPlayback: true,
                          fit: BoxFit.fitHeight,
                        ),
                      )
                    : Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(10)),
                        width: 40,
                        height: 40,
                      ),
          ),
          //Padding(padding: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0)),
          Expanded(
            flex: 1,
            child: RichText(
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              strutStyle: StrutStyle(fontSize: 12.0),
              text: TextSpan(
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 15,
                    color: Colors.black),
                text: value.name,
              ),
            ),
          ),
          //Spacer(),
          /*Container(
            child: Row(children: <Widget>[
              IconButton(
                  color: Colors.grey,
                  icon:  Icon(Icons.remove_circle),
                  onPressed: () => setState(() {
                        if (value.selectedQuantity! > 1)
                          value.selectedQuantity=value.selectedQuantity!-1;
                        updateProductItemCost();
                      })),
              Container(
                  width: 65,
                  height: 30,
                  child: TextField(
                      onChanged: (text) {
                        setState(() {
                        if(text.isNotEmpty){
                          value.selectedQuantity=int.parse(text);
                          updateProductItemCost();
                        }
                        else
                            value.selectedQuantity=1;
                        });
                      },
                      controller: teItemQtyController,
                      textAlign: TextAlign.center,
                      keyboardType: TextInputType.number,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.zero,
                        border: OutlineInputBorder(),
                      ),
                      style: TextStyle(fontSize: 15, color: Colors.black))),
              IconButton(
                  color: Colors.grey,
                  icon:  Icon(Icons.add_circle),
                  onPressed: () => setState(() {
                        value.selectedQuantity=value.selectedQuantity!+1;
                        updateProductItemCost();
                      })),
            ]),
          )*/

          MyWidgets.shadowContainer(
              color: MyColors.white_f8,
              cornerRadius: 10,
              shadowColor: MyColors.white,
              spreadRadius: 0,
              blurRadius: 0,
              child: Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    MyWidgets.shadowContainer(
                        height: 27,
                        width: 27,
                        marginB: 4,
                        marginR: 4,
                        marginT: 4,
                        marginL: 4,
                        color: MyColors.white_chinese_e0_bg,
                        cornerRadius: 10,
                        shadowColor: MyColors.white_f8,
                        blurRadius: 0,
                        spreadRadius: 0,
                        child: GestureDetector(
                          child: Icon(
                            Icons.remove,
                            color: MyColors.grey_70,
                            size: 20,
                          ),
                          onTap: () => setState(() {

                            if ((value.selectedQuantity?.toInt())! > 1){
                              value.selectedQuantity = double.tryParse((value.selectedQuantity! - 1).toString());
                              print('minusValue   ${value.selectedQuantity}');
                            updateProductItemCost();}
                          }),
                        )),
                    SizedBox(
                      width: 15,
                    ),
                    InkWell(
                      child: Text('${teItemQtyController.text}',
                          style: MyStyles.grey_70__HR_16),
                      onTap: () {

                        openAlertBox(value);
                      },
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    MyWidgets.shadowContainer(
                        height: 27,
                        width: 27,
                        marginB: 4,
                        marginR: 4,
                        marginT: 4,
                        marginL: 4,
                        color: MyColors.white_chinese_e0_bg,
                        cornerRadius: 10,
                        shadowColor: MyColors.white_f8,
                        blurRadius: 0,
                        spreadRadius: 0,
                        child: GestureDetector(
                          child: Icon(
                            Icons.add,
                            color: MyColors.grey_70,
                            size: 20,
                          ),
                          onTap: () => setState(() {

                            value.selectedQuantity = double.tryParse((value.selectedQuantity! + 1).toString());

                            print('addValue   ${value.selectedQuantity}');
                            updateProductItemCost();
                          }),
                        ))
                  ]))
        ],
      ),
      SizedBox(height: 5),
      Divider(
        height: 1.5,
      )
    ]);
  }

  Widget _buildWorkerItem(BuildContext context, Worker value, int index) {
    /*var selectedMin;
    if (value.selectedMins != null && value.selectedMins! > 0) {
      final selectedDuration = Duration(minutes: value.selectedMins!);
      selectedMin = selectedDuration.inHours.toString() +
          ' hours ' +
          (selectedDuration.inMinutes % 60).toString() +
          ' minutes ';
    }*/
    return Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
      Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(5.0),
            child: Icon(
              Icons.account_circle_outlined,
              size: 40,
            ),
          ),
          Expanded(
              flex: 6,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  MyWidgets.textView(
                      text: 'Name :${value.name!}',
                      maxLine: 1,
                      style: MyStyles.customTextStyle(
                          fontWeight: FontWeight.w500, fontSize: 15),
                      textAlign: TextAlign.left),

                  SizedBox(
                    height: 5,
                  ),
                  MyWidgets.textView(
                      text: 'Salary :${value.salary!}',
                      maxLine: 1,
                      style: MyStyles.customTextStyle(fontSize: 15),
                      textAlign: TextAlign.left),
                  SizedBox(
                    height: 10,
                  ),
                  // Padding(padding: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 10.0)),
                ],
              )),
          /*Expanded(
              flex: 4,
              child: Align(
                  alignment: Alignment.bottomLeft,
                  child: InkWell(
                    child: MyWidgets.textView(
                      text: selectedMin == null ? '0 Minutes' : '$selectedMin',
                      style: MyStyles.customTextStyle(fontSize: 12),
                    ),
                    onTap: () {
                      showCupertinoModalPopup<void>(
                          context: context,
                          builder: (BuildContext context) {
                            return _buildBottomPicker(timePicker(value));
                          });
                    },
                  )))*/
        ],
      ),
      Divider(
        height: 1,
      )
    ]);
  }

  Future<void>  updateProductItemCost()async {
    totalInventoryCost = 0.00;
    inventoryItems.forEach((element) async {
      totalInventoryCost = double.tryParse((totalInventoryCost +
              (element.costPerItemInTypeNew)! * element.selectedQuantity!)
          .toString())!;
    });

    updatePriceByPercentage();
  }

  Future<void> updateWorkerCost()async {
    totalWorkersCost = 0.00;
    workers.forEach((element) async {
      if (element.selectedMins != null && element.selectedMins! > 0)
        totalWorkersCost = double.tryParse((totalWorkersCost +
                (element.wagePerMinNew)! * element.selectedMins!)
            .toString())!;
    });
    updatePriceByPercentage();
  }

  void updatePriceByPercentage() {
    var totalCost = (totalInventoryCost + totalWorkersCost);
    if (percentage! > 0 && totalCost > 0) {
      var costPercentage =
          ((totalInventoryCost + totalWorkersCost) * percentage!) / 100;
      tePrdPriceController.text =
          '${(totalCost + costPercentage).toString().toCustomDoubleRoundOff}';
    }
  }

  void _showPicker(context) {
    /*showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text('Photo Library'),
                      onTap: () {
                         */ /*imgFromGallery().then((value) => setState(() {
                           _image = File(value);
                         }));*/ /*
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  ListTile(
                    leading: Icon(Icons.photo_camera),
                    title: Text('Camera'),
                    onTap: () {
                     */ /* imgFromCamera().then((value) => setState(() {
                        _image = File(value);
                      }));*/ /*
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });*/
    MyImagePickerBottomSheet.showPicker(context, fileCallback: (image) {
      productItemImageBase64 = null;
      setState(() {
        _image = image != null ? File(image.path) : null;
      });
      return null;
    });
  }

  /*_imgFromCamera() async {
    var image = await picker.pickImage(
        source: ImageSource.camera,
        imageQuality: 0,
        maxHeight: 150,
        maxWidth: 150);
    productItemImageBase64 = null;
    setState(() {
      _image = image!=null?File(image.path):null;
    });
  }

  _imgFromGallery() async {
    var image = await picker.pickImage(
        source: ImageSource.gallery,
        imageQuality: 0,
        maxHeight: 150,
        maxWidth: 150);
    productItemImageBase64 = null;
    setState(() {
      _image = image!=null?File(image.path):null;
    });
  }*/

  bool _validate() {
    if (productItemImageBase64 == null && _image == null) {
      context.showSnackBar("Select Product Item Image");
      return false;
    } else if (tePrdNameController.text.isEmpty) {
      context.showSnackBar("Enter Product Item Name");
      return false;
    }
    // else if (tePrdNameController.text.startsWith(RegExp(r'^\d+\.?\d{0,2}'))) {
    //   context.showSnackBar("Can\'t Start Product Item Name with Number");
    //   return false;
    // }
    else if (inventoryItems.isEmpty) {
      context.showSnackBar("Select inventory");
      return false;
    }/* else if (workers.isEmpty) {
      context.showSnackBar("Select workers");
      return false;
    } else if (isWorkDurationSelected()) {
      context.showSnackBar("Select work duration for all selected workers");
      return false;
    }*/ else if (workers.isNotEmpty&&selectedTimeInt==0) {
      context.showSnackBar("Select preparation time.");
      return false;
    } else if (tePrdCostController.text.isEmpty ||
        double.tryParse(tePrdCostController.text.trim()) == 0) {
      context.showSnackBar("Please check the product cost");
      return false;
    } else if (tePrdPriceController.text.isEmpty ||
        double.tryParse(tePrdPriceController.text.trim()) == 0 ||
        double.parse(tePrdPriceController.text.trim()) <=
            totalInventoryCost + totalWorkersCost) {
      context.showSnackBar("Please check the selling price");
      return false;
    } else if (checkInventoryStock().isNotEmpty) {
      context.showSnackBar("${checkInventoryStock()} Stock not available");
      return false;
    }

    return true;
  }

  /*String getImageBase64() {
    if (_image != null) {
      var bytes = _image?.readAsBytesSync();
      var base64Image = base64Encode(bytes!);

      return base64Image.toString();
    }
    return "";
  }*/

  addProductItem() async {
    var productItem = ProductItem();
    productItem.name = tePrdNameController.text.trim().toString();
    productItem.imageBase = getImageBase64(_image);
    productItem.cost = double.tryParse(
        (totalInventoryCost + totalWorkersCost).toString());
    //productItem.sellingPrice = double.tryParse(tePrdPriceController.text.trim());
    productItem.sellingPrice =
        tePrdPriceController.text.toString().trim().toCustomDoubleRoundOff;
    productItem.productCategoryId = widget.productCategory?.id;
    productItem.priceByPercentage = percentage! > 0 ? percentage : 0;
    await productItemOperations.insertProductItem(productItem).then((value) {
      tePrdNameController.text = "";
      print(
          'getLastInsertedId ${value.first.id}  ${value.first.name}  ${value.first.productCategoryId}');
      if (value.isNotEmpty && inventoryItems.isNotEmpty) {
        addProductItemRelation(value.first.id);
      }
      showtoast("Successfully Added Data");

      Navigator.pop(context, true);
    });
  }

  addProductItemWithTransaction() async {
    if (project != null) {
      var productItem = ProductItem();
      productItem.name = tePrdNameController.text.trim().toString();
      productItem.imageBase = getImageBase64(_image);
      productItem.cost = double.tryParse(
          (totalInventoryCost + totalWorkersCost).toString());
      productItem.sellingPrice =
          tePrdPriceController.text.toString().trim().toCustomDoubleRoundOff;

      productItem.projectId = project?.id;
      productItem.priceByPercentage = percentage! > 0 ? percentage : 0;
      if (widget.productCategory == null) {
        productItem.productCategoryId = null;
        productItem.hasCategory = 0;
      } else {
        productItem.productCategoryId = widget.productCategory?.id;
        productItem.hasCategory = 1;
      }
      await productItemOperations
          .insertProductItemWithTxn(productItem, inventoryItems, workers)
          .then((value) {
        Navigator.pop(context, true);
      }).onError((error, stackTrace) {
        print('onDeleteError  ${error}  ${stackTrace}');
      });
    }
  }

  updateProductItemWithTransaction() async {
    if (project != null) {
      print(
          'updatingData ${totalInventoryCost + totalWorkersCost}   ${widget.productItemId}  ${tePrdNameController.text.trim().toString()}  ${widget.productCategory?.id}  ${productItemImageBase64}');
      var productItem = ProductItem();
      productItem.id = widget.productItemId;
      productItem.name = tePrdNameController.text.trim().toString();
      productItem.imageBase = productItemImageBase64 != null
          ? productItemImageBase64
          : getImageBase64(_image);
      productItem.cost = double.tryParse((totalInventoryCost + totalWorkersCost).toString());
      productItem.sellingPrice =
          tePrdPriceController.text.toString().trim().toCustomDoubleRoundOff;
      productItem.priceByPercentage = percentage! > 0 ? percentage : 0;
      productItem.productCategoryId = widget.productCategory?.id;
      productItem.projectId = project?.id;
      if (widget.productCategory == null) {
        productItem.productCategoryId = null;
        productItem.hasCategory = 0;
      } else {
        productItem.productCategoryId = widget.productCategory?.id;
        productItem.hasCategory = 1;
      }
      //update the product_item
      await productItemOperations
          .updateProductItemWithTxn(productItem, inventoryItems, workers)
          .then((value) {
        Navigator.pop(context, true);
      }).onError((error, stackTrace) {
        print('onDeleteError  ${error}  ${stackTrace}');
      });
    }else
      context.showSnackBar('Something went wrong, Please try again later');
  }

  updateProductItem() async {
    print('updatingData ${totalInventoryCost + totalWorkersCost}   ${widget.productItemId}  ${tePrdNameController.text.trim().toString()}  ${widget.productCategory?.id}  ${productItemImageBase64}');
    var productItem = ProductItem();
    productItem.id = widget.productItemId;
    productItem.name = tePrdNameController.text.trim().toString();
    productItem.imageBase = productItemImageBase64 != null
        ? productItemImageBase64
        : getImageBase64(_image);
    productItem.cost = double.tryParse(
        (totalInventoryCost + totalWorkersCost).toString());
    // productItem.sellingPrice = double.tryParse(tePrdPriceController.text.trim());
    productItem.sellingPrice =
        tePrdPriceController.text.toString().trim().toCustomDoubleRoundOff;
    productItem.priceByPercentage = percentage! > 0 ? percentage : 0;
    productItem.productCategoryId = widget.productCategory?.id;

    //update the product_item
    await productItemOperations
        .updateProductItem(productItem)
        .then((value) async {
      tePrdNameController.text = "";

      //then delete all relation with combination product_item_id and inventory_item_id
      /*inventoryItems2.forEach((element) {
        relationOperations
            .deleteRelationByProductItemId(widget.productItem?.id)
            .then((value) {
          //then insert thr relation  with combination product_item_id and inventory_item_id again
          addProductItemRelation(widget.productItem?.id);
        });
      });*/

      await relationOperations.deleteRelationByProductItemId(widget.productItemId)
          .then((value) async {
        //then insert thr relation  with combination product_item_id and inventory_item_id again
        await workerRelationOperations
            .deleteWorkerRelationByProductItemId(widget.productItemId)
            .then((value) async =>
                await addProductItemRelation(widget.productItemId));
      });

      showtoast("Successfully Updates Data");

      Navigator.pop(context, true);
    });
  }

  addProductItemRelation(int? productItemId) async {
    if (inventoryItems.isNotEmpty) {
      for (var element in inventoryItems) {
        var relation = InventoryRelation();
        relation.productItemId = productItemId;
        relation.inventoryItemId = element.id;
        relation.inventoryItemQuantity = element.selectedQuantity;
        await relationOperations.insertRelation(relation).then((value) {});
      }
      await addWorkerRelation(productItemId);
    }
  }

  addWorkerRelation(int? productItemId) async {
    /*workers2.forEach((element) {
      var relation = WorkerRelation();
      relation.productItemId = productItemId;
      relation.workerId = element.id;
      relation.workerHour =element.selectedMins;
       workerRelationOperations.insertWorkerRelation(relation).then((value) {});
    });*/
    if (workers.isNotEmpty)
      for (var element in workers) {
        var relation = WorkerRelation();
        relation.productItemId = productItemId;
        relation.workerId = element.id;
        relation.workerHour = element.selectedMins;
        await workerRelationOperations
            .insertWorkerRelation(relation)
            .then((value) {});
      }
  }

  ///Fetch data from database
  Future<List<InventoryItem>> _getDataSelectedInventoryItems() async {
    if (widget.productItemId != null) {
      await inventoryItemOperations
          .inventoriesItemsByProductItem(widget.productItemId)
          .then((value) {
        inventoryItems = value;
      });
      _getDataSelectedWorkersItems();
    }
    //setState(() {updateProductItemCost();});
    return inventoryItems;
  }

  ///Fetch data from database
  Future<List<Worker>> _getDataSelectedWorkersItems() async {
    await workersOperations
        .workersByProductItem(widget.productItemId)
        .then((value) {
      workers = value;
    });
if(workers.isNotEmpty){
  convertTotalMinToHrAndMin(workers.first.selectedMins!);
}
    setState(() {
      isLoading = false;
      updateProductItemCost();
      updateWorkerCost();
    });
    return workers;
  }

  double _kPickerSheetHeight = 216.0;

  Widget timePicker(Worker worker) {
    return CupertinoTimerPicker(
      mode: CupertinoTimerPickerMode.hm,
      minuteInterval: 1,
      initialTimerDuration: Duration(minutes: worker.selectedMins!),
      onTimerDurationChanged: (Duration changedtimer) {
        setState(() {
          worker.selectedMins =
              changedtimer.inHours + (changedtimer.inMinutes % 60);
          var selectedHr = changedtimer.inHours;
          var hrToMin = selectedHr * 60;
          var selectedMin = changedtimer.inMinutes % 60;
          var totalMin = hrToMin + selectedMin;

          worker.selectedMins = totalMin;

          //debugPrint('selected Duration $hrToMin  ${totalMin}  $hr  ${(perHr/60)*totalMin}');
          updateWorkerCost();
        });
      },
    );
  }

String? selectedTimeText;
int selectedTimeInt=0;
  Widget timePicker2() {
    return CupertinoTimerPicker(
      mode: CupertinoTimerPickerMode.hm,
      minuteInterval: 1,
      initialTimerDuration: Duration(minutes: selectedTimeInt),
      onTimerDurationChanged: (Duration changedtimer) {
        setState(() {
          var selectedHr = changedtimer.inHours;
          var hrToMin = selectedHr * 60;
          var selectedMin = changedtimer.inMinutes % 60;
          var totalMin = hrToMin + selectedMin;
          print('time_change  $totalMin');
          selectedTimeInt=totalMin;
          convertTotalMinToHrAndMin(totalMin);
          setDurationForAllWorkers(totalMin);

        });

      },
    );
  }
void setDurationForAllWorkers(int totalMin)async{
  if(workers.isNotEmpty&&totalMin>0) {
    workers.forEach((worker)async {
       worker.selectedMins = totalMin;
    });

  }else{
    selectedTimeInt=0;
    selectedTimeText=null;

  }
 await updateWorkerCost();
}
  void convertTotalMinToHrAndMin(int totalMin){
    selectedTimeInt=totalMin;
    final selectedDuration = Duration(minutes: totalMin);
    selectedTimeText = selectedDuration.inHours.toString() +
        ' hours ' +
        (selectedDuration.inMinutes % 60).toString() +
        ' minutes ';
  }
  var percentageList = List<int>.generate(100, (i) => i + 1);
  int? percentage = 0;

  Widget percentPicker() {
    return CupertinoPicker(
        itemExtent: 40.0,
        onSelectedItemChanged: (int index) {
          setState(() {
            percentage = percentageList[index];
            var productPrice = totalInventoryCost + totalWorkersCost;
            tePrdPriceController.text =
                '${(productPrice + ((productPrice * (percentageList[index])) / 100)).toString()}';
          });
        },
        scrollController: FixedExtentScrollController(
            initialItem: percentage! > 1 ? percentage! - 1 : 0),
        children: List<Widget>.generate(percentageList.length, (int index) {
          return Center(
            child: Text('${percentageList[index]}%'),
          );
        }));
  }

  Widget _buildBottomPicker(Widget picker) {
    return Container(
      height: _kPickerSheetHeight,
      padding: const EdgeInsets.only(top: 6.0),
      color: CupertinoColors.white,
      child: DefaultTextStyle(
        style: const TextStyle(
          color: CupertinoColors.black,
          fontSize: 22.0,
        ),
        child: GestureDetector(
          // Blocks taps from propagating to the modal sheet and popping.
          onTap: () {},
          child: SafeArea(
            top: false,
            child: picker,
          ),
        ),
      ),
    );
  }

  String checkInventoryStock() {
    var invNames = '';
    inventoryItems.forEach((element) {
      if (element.stock! < element.selectedQuantity!) {
        invNames = invNames + ' ${element.name!}';
      }
    });
    return invNames.trim();
  }

  bool isWorkDurationSelected() {
    for (var element in workers) {
      if (element.selectedMins == 0) {
        return true;
      }
    }
    return false;
  }

  openAlertBox(InventoryItem value) async {
    value.convertedItemType = value.itemType;

    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
              contentPadding: EdgeInsets.only(top: 15.0),
              content: StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                return Container(
                  width: 300.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      MyWidgets.textView(
                        text: "Updated details",
                        style: MyStyles.customTextStyle(
                            fontSize: 17.0,
                            fontWeight: FontWeight.bold,
                            color: MyColors.grey_70),
                      ),
                      SizedBox(
                        height: 10,
                      ),

                      // Spacer(),
                      /*IconButton(
                        iconSize: 20,
                        icon: Icon(
                          Icons.close_rounded,
                          color: MyColors.grey_70,
                        ), onPressed: () {

                      },)*/

                      Divider(
                        color: Colors.grey,
                        height: 4.0,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 15.0, right: 15.0),
                        child: Form(
                          // key: _formKey,
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Flexible(
                                      flex: 1,
                                      fit: FlexFit.tight,
                                      child: Text('Select unit mesure',
                                          style: TextStyle(
                                            fontWeight: FontWeight.w200,
                                          )) //Container
                                      ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Flexible(
                                      flex: 1,
                                      fit: FlexFit.tight,
                                      child: MyWidgets.textViewContainer(
                                          height: 45,
                                          cornerRadius: 8,
                                          color: MyColors.white,
                                          // color: Colors.white54,
                                          child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      12, 0, 12, 0),
                                              child: DropdownButton<String>(
                                                value: InventoryUnitMeasure
                                                        .inventoryUnitMeasureArray[
                                                    value.convertedItemType!],
                                                icon: const Icon(
                                                    Icons.keyboard_arrow_down),
                                                elevation: 8,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                style: const TextStyle(
                                                    color: Colors.black),
                                                /*underline: Container(
                  height: 2,
                  color: Colors.blue,
                ),*/
                                                onChanged: (String? newValue) {
                                                  setState(() {
                                                    //selectedTypeIndex = InventoryUnitMeasure.inventoryUnitMeasureArray.indexOf(newValue!);
                                                    /*dropdownSelectedTypeValue=newValue;
                                                unitMeasure.convert(balanceStock!,selectedTypeValue! , newValue!).then((value) {

                                                  if(value==0){
                                                    dropdownSelectedTypeValue=selectedTypeValue;
                                                    showCustomAlertDialog(context,'This conversion cannot be performed');
                                                    return;
                                                  }
                                                  if(selectedTypeIndex!=InventoryUnitMeasure.inventoryUnitMeasureArray.indexOf(newValue))
                                                    showCustomAlertDialog(context,'$newValue $value');

                                                });*/
                                                    unitMeasure
                                                        .convert(
                                                            1.0,
                                                            InventoryUnitMeasure
                                                                    .inventoryUnitMeasureArray[
                                                                value
                                                                    .itemType!],
                                                            newValue!)
                                                        .then((convertValue) {
                                                      if (convertValue == 0) {
                                                        //dropdownSelectedTypeValue=selectedTypeValue;
                                                        showCustomAlertDialog(
                                                            context,
                                                            'This conversion cannot be performed');
                                                        return;
                                                      }
                                                      if (value.convertedItemType !=
                                                              null &&
                                                          value.convertedItemType !=
                                                              InventoryUnitMeasure
                                                                  .inventoryUnitMeasureArray
                                                                  .indexOf(
                                                                      newValue))
                                                        value.convertedItemType =
                                                            InventoryUnitMeasure
                                                                .inventoryUnitMeasureArray
                                                                .indexOf(
                                                                    newValue);
                                                    });
                                                  });
                                                },
                                                items: InventoryUnitMeasure
                                                    .inventoryUnitMeasureArray
                                                    .map<
                                                            DropdownMenuItem<
                                                                String>>(
                                                        (String value) {
                                                  return DropdownMenuItem<
                                                      String>(
                                                    value: value,
                                                    child: Text(value),
                                                  );
                                                }).toList(),
                                              ))) //Container
                                      )
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Flexible(
                                      flex: 1,
                                      fit: FlexFit.tight,
                                      child: Text('Enter Quantity',
                                          style: TextStyle(
                                            fontWeight: FontWeight.w200,
                                          )) //Container
                                      ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Flexible(
                                      flex: 1,
                                      fit: FlexFit.tight,
                                      child: TextFormField(
                                          keyboardType:
                                              TextInputType.numberWithOptions(
                                                  decimal: true),
                                          inputFormatters: [
                                            FilteringTextInputFormatter.allow(
                                                RegExp(r'^\d+\.?\d{0,2}')),
                                          ],
                                          controller:
                                              teAlertDialogQtyController,
                                          obscureText: false,
                                          decoration:
                                              InputDecoration()) //Container
                                      )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      InkWell(
                        onTap: () => {
                          teAlertDialogQtyController.text.toString().isEmpty||double.tryParse(teAlertDialogQtyController.text)==0?
                              context.showSnackBar('Enter quantity'):

                            unitMeasure
                                .convert(
                                    double.tryParse(teAlertDialogQtyController.text)!,
                                    InventoryUnitMeasure
                                            .inventoryUnitMeasureArray[
                                        value.convertedItemType!],
                                    InventoryUnitMeasure
                                            .inventoryUnitMeasureArray[
                                        value.itemType!])
                                .then((convertValue) {
                              if (convertValue == 0) {
                                showCustomAlertDialog(context,
                                    'This conversion cannot be performed');
                                return;
                              }
                              this.setState(() {
                                value.selectedQuantity = convertValue;
                              print('convertValue   ${value.selectedQuantity} $convertValue');
                              updateProductItemCost();
                              Navigator.of(context).pop();
                            });
                          })
                        },
                        child: Container(
                          padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                          decoration: BoxDecoration(
                            color: MyColors.white_chinese_e0_bg,
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(20.0),
                                bottomRight: Radius.circular(20.0)),
                          ),
                          child: MyWidgets.textView(
                            text: "Save Changes",
                            style: MyStyles.customTextStyle(
                                color: MyColors.grey_70),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              }));
        });
  }
}
