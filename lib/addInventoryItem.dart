import 'dart:convert';
import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:ecommerce/model/Inventory.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'inventoryUnitMeasures.dart';
import 'database/history_operations.dart';
import 'database/inventory_item_operations.dart';
import 'model/History.dart';
import 'model/InventoryItem.dart';
import 'package:image_picker/image_picker.dart';

import 'model/Project.dart';

class AddInventoryItemScreenState extends StatefulWidget {
  final Inventory? inventory;

  const AddInventoryItemScreenState({@required this.inventory, Key? key})
      : super(key: key);

  @override
  _AddInventoryItemScreenState createState() => _AddInventoryItemScreenState();
}

class _AddInventoryItemScreenState extends State<AddInventoryItemScreenState> {
  File? _image;
  final picker = ImagePicker();
  var inventoryItemOperations = InventoryItemOperations();
  var historyItemOperations = HistoryOperations();
  final teInvNameController = TextEditingController();
  final teInvTotalTypeCountController = TextEditingController();
  final teInvPiecesInTypeController = TextEditingController();
  final teInvCostPerTypeController = TextEditingController();
  final List<String> itemType = <String>['Box', 'Length'];
  int selectedTypeIndex = 0;
  String dropdownSelectedTypeValue = InventoryUnitMeasure.inventoryUnitMeasureArray[0];

  @override
  void setState(VoidCallback fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  Project? project;
  @override
  void initState() {
     MyPref.getProject().then((project) async {
      this.project = project;});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final addInventoryListItemText =
    Align(alignment: Alignment.centerLeft,
      child: MyWidgets.textView(text: 'Add Item',
        style: MyStyles.customTextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 24,
        ),
      ),
    );
    final imageView = /*FadeInUp(delay: Duration(milliseconds: 100),child:
    GestureDetector(
        onTap: () {
          _showPicker(context);
        },
        child: Padding(
            padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(10)),
              height: 150,
              width: MediaQuery.of(context).size.width,
              child: _image != null
                  ? ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.file(
                        _image!,
                        width: 100,
                        height: 100,
                        fit: BoxFit.fitHeight,
                      ),
                    )
                  : Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(10)),
                      width: 100,
                      height: 100,
                      child: Icon(
                        Icons.camera_alt,
                        color: Colors.grey[800],
                      ),
                    ),
            )),
      ),
    );*/
    GestureDetector(
        onTap: () {
          _showPicker(context);

        },
        child: Container(
          decoration: BoxDecoration(
            // color: Colors.grey[200],
              borderRadius: BorderRadius.circular(75)
          ),
          height: 104,
          width: 104,
          child:_image != null?
          ClipRRect(
            borderRadius: BorderRadius.circular(75),
            child: Image.file(
            _image!,
            width: 100,
            height: 100,
            fit: BoxFit.fitHeight,
              gaplessPlayback: true,
          ),
          ):_image != null
              ? ClipRRect(
            borderRadius: BorderRadius.circular(75),
            child: Image.file(
              _image!,
              width: 100,
              height: 100,
              fit: BoxFit.cover,
              gaplessPlayback: true,
            ),
          )
              : Container(
            decoration: BoxDecoration(
              // color: Colors.grey[200],
                border: Border.all(color: Colors.black,width: 1),
                borderRadius: BorderRadius.circular(75)
            ),
            width: 100,
            height: 100,
            child: Icon(
              Icons.image_rounded,
              color: Colors.grey[800],
            ),
          ),
        ));

    final addInventoryItemEditTxt =/*FadeInUp(delay: Duration(milliseconds: 200),child:  Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Name',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 15,
                color: Colors.black,
              ),
            ),
            TextFormField(
                keyboardType: TextInputType.name,
                textCapitalization: TextCapitalization.words,
                controller: teInvNameController,
                obscureText: false,
                decoration: InputDecoration(
                  //contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  hintText: "Inventory Item Name",
                ))
          ],
        )))*/Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text: 'Item Name'),
          SizedBox(height: 10,),
          MyWidgets.textViewContainer(

            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child: Align(alignment:Alignment.centerLeft,child:MyWidgets.textFromField(

              contentPaddingL: 15.0,

              contentPaddingR: 15.0,

              cornerRadius: 8,
              keyboardType: TextInputType.name,
              controller: teInvNameController,
            )),
          ),
        ]
    );

    final toggleButton = Container(
        width: 215,
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.grey[350],
        ),
        child: Center(
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: itemType.length,
            itemBuilder: (BuildContext context, int index) => InkWell(
              child: Card(
                color: selectedTypeIndex == index
                    ? Colors.white
                    : Colors.grey[350],
                elevation: selectedTypeIndex == index ? 5 : 0,
                child: Container(
                  width: 100,
                  child: Center(child: Text('${itemType[index]}')),
                ),
              ),
              onTap: () => setState(() {
                selectedTypeIndex = index;
              }),
            ),
          ),
        ));


    /*final typeDropDownButton = FadeInUp(delay: Duration(milliseconds: 300),child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
        elevation: 5,
        child: Padding(
            padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
            child: DropdownButton<String>(
              value: dropdownSelectedTypeValue,
              icon: const Icon(Icons.keyboard_arrow_down),
              elevation: 8,
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              style: const TextStyle(color: Colors.black),
              underline: Container(
                height: 2,
                color: Colors.blue,
              ),
              onChanged: (String? newValue) {
                setState(() {
                  selectedTypeIndex =
                      InventoryUnitMeasure.inventoryUnitMeasureArray.indexOf(newValue!);
                  dropdownSelectedTypeValue = newValue;
                });
              },
              items: InventoryUnitMeasure.inventoryUnitMeasureArray
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ))));*/

    final totalTypeEditTxtWithDropDown = /*FadeInUp(delay: Duration(milliseconds: 400),child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Total $dropdownSelectedTypeValue',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 15,
                color: Colors.black,
              ),
            ),
            TextFormField(
                keyboardType: TextInputType.number,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                controller: teInvTotalTypeCountController,
                obscureText: false,
                decoration: InputDecoration(
                  hintText: 'Total $dropdownSelectedTypeValue',
                ))
          ],
        )))*/



       Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10,),
            MyWidgets.textView(text:'Total $dropdownSelectedTypeValue',),
            SizedBox(height: 10,),
        Row(mainAxisSize: MainAxisSize.max,crossAxisAlignment: CrossAxisAlignment.center,children: [
    Expanded(flex:1,child:
          MyWidgets.textViewContainer(

              height: 45, cornerRadius: 8, color: MyColors.white,
              // color: Colors.white54,
              child:Align(alignment:Alignment.centerLeft,child: MyWidgets.textFromField(

                contentPaddingL: 15.0,

                contentPaddingR: 15.0,

                cornerRadius: 8,
                keyboardType: TextInputType.number,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                controller: teInvTotalTypeCountController,
              )),
            )),SizedBox(width: 10,),
          MyWidgets.textViewContainer(

              height: 45, cornerRadius: 8, color: MyColors.white,
              // color: Colors.white54,
              child: Padding(
                  padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                  child: DropdownButton<String>(
                    value: dropdownSelectedTypeValue,
                    icon: const Icon(Icons.keyboard_arrow_down),
                    elevation: 8,

                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    style: const TextStyle(color: Colors.black),
                    /*underline: Container(
                      height: 2,
                      color: Colors.blue,
                    ),*/
                    onChanged: (String? newValue) {
                      setState(() {
                        selectedTypeIndex =
                            InventoryUnitMeasure.inventoryUnitMeasureArray.indexOf(newValue!);
                        dropdownSelectedTypeValue = newValue;
                      });
                    },
                    items: InventoryUnitMeasure.inventoryUnitMeasureArray
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  )))
        ])
          ]
      );
    final piecesInType =selectedTypeIndex!=0?Container():/*FadeInUp(delay: Duration(milliseconds: 500),child:  Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Pieces in the $dropdownSelectedTypeValue',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 15,
                color: Colors.black,
              ),
            ),
            TextFormField(
                keyboardType: TextInputType.number,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                controller: teInvPiecesInTypeController,
                obscureText: false,
                decoration: InputDecoration(
                  //contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  hintText: "Pieces in the $dropdownSelectedTypeValue",
                ))
          ],
        )))*/
    Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text: 'Pieces in the $dropdownSelectedTypeValue',),
          SizedBox(height: 10,),
          MyWidgets.textViewContainer(

            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child: Align(alignment:Alignment.centerLeft,child:MyWidgets.textFromField(

              contentPaddingL: 15.0,

              contentPaddingR: 15.0,

              cornerRadius: 8,
              keyboardType: TextInputType.number,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              controller: teInvPiecesInTypeController,
            )),
          ),
        ]
    );
    final totalCostPerType = /*FadeInUp(delay: Duration(milliseconds: 600),child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              selectedTypeIndex==0?"Cost per $dropdownSelectedTypeValue":"Total $dropdownSelectedTypeValue cost",
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 15,
                color: Colors.black,
              ),
            ),
            TextFormField(
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}')),
                ],
                controller: teInvCostPerTypeController,
                obscureText: false,
                decoration: InputDecoration(
                  // contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  hintText: selectedTypeIndex==0?"Cost per $dropdownSelectedTypeValue":"Total $dropdownSelectedTypeValue cost",
                ))
          ],
        )))*/
    Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          MyWidgets.textView(text:selectedTypeIndex==0?"Cost per $dropdownSelectedTypeValue":"Total $dropdownSelectedTypeValue cost",),
          SizedBox(height: 10,),
          MyWidgets.textViewContainer(

            height: 45, cornerRadius: 8, color: MyColors.white,
            // color: Colors.white54,
            child:Align(alignment:Alignment.centerLeft,child: MyWidgets.textFromField(

              contentPaddingL: 15.0,

              contentPaddingR: 15.0,

              cornerRadius: 8,
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}')),
              ],
              controller: teInvCostPerTypeController,
            )),
          ),
        ]
    );
    final addInventoryItemButton = /*FadeInUp(delay: Duration(milliseconds: 700),child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(10.0),
          color: (Colors.teal[900])!,
          child: MaterialButton(
            minWidth: MediaQuery.of(context).size.width,
            onPressed: () {
              _validate() ? addInventoryItem() : {};
            },
            child: Text(
              "Add Inventory Item",
              textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white)
            ),
          ),
        )))*/

    MyWidgets.materialButton(context,text: "Add Inventory Item",onPressed:() {

      _validate() ? addInventoryItem() : {};    });

    return Scaffold(
      /*appBar: AppBar(
        title: Text(
          'Add Inventory Item',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),*/
      body: SingleChildScrollView(
          padding: EdgeInsets.only(left: 30, right: 30, top: 25, bottom: 20),

          child: SizedBox(

              child: Column(
                children: <Widget>[
                  addInventoryListItemText,
                  SizedBox(
                    height: 25,
                  ),
                  imageView,
                  SizedBox(
                    height: 20,
                  ),

                  addInventoryItemEditTxt,

                  totalTypeEditTxtWithDropDown,

                  piecesInType,

                  totalCostPerType,
                  SizedBox(
                    height: 30,
                  ),
                  addInventoryItemButton,
                  SizedBox(
                    height: 15,
                  )
                ],
              ))),
    );
  }

  void _showPicker(context) {
    /*showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  ListTile(
                    leading: Icon(Icons.photo_camera),
                    title: Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });*/
    MyImagePickerBottomSheet.showPicker(context, fileCallback: (image) {

      setState(() {
        _image = image!=null?File(image.path):null;
      });
      return null;
    });
  }

   /*_imgFromCamera() async {
    var image = await picker.pickImage(
        source: ImageSource.camera,
        imageQuality: 0,
        maxHeight: 150,
        maxWidth: 150);

    setState(() {
      _image = image!=null?File(image.path):null;
    });
  }

  _imgFromGallery() async {
    var image = await picker.pickImage(
        source: ImageSource.gallery,
        imageQuality: 0,
        maxHeight: 150,
        maxWidth: 150);

    setState(() {
      _image = image!=null?File(image.path):null;
    });
  }*/

  bool _validate() {
    if (_image == null) {
      context.showSnackBar("Select inventory image");
      return false;
    } else if (teInvNameController.text.isEmpty) {
      context.showSnackBar("Enter Inventory name");
      return false;
    }
    // else if (teInvNameController.text.startsWith(RegExp(r'^\d+\.?\d{0,2}'))) {
    //   context.showSnackBar("Can\'t Start Inventory Name with Number");
    //   return false;
    // }
    else if (teInvTotalTypeCountController.text.isEmpty||int.tryParse(teInvTotalTypeCountController.text.trim())==0) {
      context.showSnackBar("Enter total $dropdownSelectedTypeValue");
      return false;
    } else if (selectedTypeIndex==0&&teInvPiecesInTypeController.text.isEmpty||int.tryParse(teInvPiecesInTypeController.text.trim())==0) {
      context.showSnackBar("Enter Pieces in $dropdownSelectedTypeValue");
      return false;
    } else if (teInvCostPerTypeController.text.isEmpty||int.tryParse(teInvCostPerTypeController.text.trim())==0) {
      context.showSnackBar("Enter cost per $dropdownSelectedTypeValue");
      return false;
    }
    return true;
  }

  /*String getImageBase64() {
    if (_image != null) {
      var bytes = _image?.readAsBytesSync();
      var base64Image = base64Encode(bytes!);

      return base64Image.toString();
    }
    return "";
  }*/

  addInventoryItem() async{
    if(project!=null){
    var inventoryItem = InventoryItem();
    inventoryItem.name = teInvNameController.text.trim().toString();
    inventoryItem.imageBase = getImageBase64(_image);
    inventoryItem.itemType = selectedTypeIndex;
    inventoryItem.totalType = int.parse(teInvTotalTypeCountController.text.trim());
   // inventoryItem.piecesInType = int.parse(teInvPiecesInTypeController.text.trim());
    if(selectedTypeIndex==0) {
      inventoryItem.piecesInType = int.parse(teInvPiecesInTypeController.text.trim());
      inventoryItem.costPerItemInTypeNew=double.tryParse((double.tryParse(teInvCostPerTypeController.text.trim())!/int.parse(teInvPiecesInTypeController.text.trim())).toStringAsFixed(2));
      inventoryItem.stock = double.tryParse(teInvTotalTypeCountController.text.trim())! * int.tryParse(teInvPiecesInTypeController.text.trim())!;

    }else{
      inventoryItem.piecesInType =1;
      inventoryItem.costPerItemInTypeNew=double.tryParse((double.tryParse(teInvCostPerTypeController.text.trim())!/int.parse(teInvTotalTypeCountController.text.trim())).toStringAsFixed(2));
      inventoryItem.stock = double.tryParse(teInvTotalTypeCountController.text.trim())!;
    }
    inventoryItem.costPerTypeNew =
        double.tryParse(teInvCostPerTypeController.text.trim());


   // inventoryItem.costPerItemInTypeNew=double.tryParse((double.tryParse(teInvCostPerTypeController.text.trim())!/int.parse(teInvPiecesInTypeController.text.trim())).toStringAsFixed(2));

    //inventoryItem.stock = int.tryParse(teInvTotalTypeCountController.text.trim())! * int.tryParse(teInvPiecesInTypeController.text.trim())!;
if(widget.inventory==null){
  inventoryItem.inventoryId=null;
  inventoryItem.hasCategory=0;
}else{
  inventoryItem.inventoryId= widget.inventory?.id;
  inventoryItem.hasCategory=1;
}
    //inventoryItem.inventoryId =widget.inventory==null?null: widget.inventory?.id;
    inventoryItem.projectId=this.project?.id;
   await inventoryItemOperations.insertInventoryItem(inventoryItem).then((value)async {

     value.isNotEmpty? await addInventoryHistory(value):{};


    });}
  }

  addInventoryHistory(List<InventoryItem> value)async{

    var history = History();
    history.inventoryItemId=value.first.id;
    history.historyItemType=value.first.itemType;
    history.historyTotalType=value.first.totalType;
    history.historyPiecesInType=value.first.piecesInType;
    history.historyCostPerType=value.first.costPerTypeNew;
    history.historyCostPerItemInType=value.first.costPerItemInTypeNew;
    history.historyStock=value.first.stock;
    print('dateAdded ${history.historyDate}');
    await historyItemOperations.insertHistory(history).then((value) {
      teInvNameController.text = "";
      teInvTotalTypeCountController.text = "";
      teInvPiecesInTypeController.text = "";
      teInvCostPerTypeController.text = "";
      showtoast("Successfully Added Data");
      Navigator.pop(context, true);
    });
  }

}
