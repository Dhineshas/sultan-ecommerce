import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'database/inventory_item_operations.dart';
import 'database/relation_operations.dart';
import 'inventoryUnitMeasures.dart';
import 'model/InventoryItem.dart';

class SwitchMeasureScreenState extends StatefulWidget {
  final int? inventoryItemId;
  const SwitchMeasureScreenState({@required this.inventoryItemId, Key? key})
      : super(key: key);
  @override
  _SwitchMeasureScreenState createState() => _SwitchMeasureScreenState();
}

class _SwitchMeasureScreenState extends State<SwitchMeasureScreenState> {


  @override
  void setState(VoidCallback fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  @override
  void initState() {
    if(widget.inventoryItemId!=null){
      /*selectedIndex=widget.inventoryItem?.itemType;
      balanceStock=(widget.inventoryItem?.stock)!/(widget.inventoryItem?.piecesInType)!;
      print('inventory item unit measure details  type_index:${widget.inventoryItem?.itemType}  type:${inventoryUnitMeasureArray[(widget.inventoryItem?.itemType)!]}  stock: ${widget.inventoryItem?.stock}  balanceStock: $balanceStock');*/
      Future.delayed( Duration(milliseconds: 1000), () {
        _getData();

      });
    }
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: createListView(),
    );
  }
  final _listKey = GlobalKey<AnimatedListState>();
  var _scrollController = ScrollController();
  int? selectedIndex = 0;
  double? balanceStock=0.0;
  var unitMeasure = InventoryUnitMeasure();
  var inventoryUnitMeasureArray = InventoryUnitMeasure.inventoryUnitMeasureArray;
  var inventoryItemOperations = InventoryItemOperations();
  var relationOperations = RelationOperations();
  bool isLoading = true;
  Widget createListView() {
    if (inventoryItem!=null) {
    return  Padding(padding:EdgeInsets.fromLTRB(20, 0, 20, 20) ,child:Column(mainAxisSize:MainAxisSize.max,crossAxisAlignment:CrossAxisAlignment.center,children: [
      SizedBox(height: 20,),
      Align(alignment: Alignment.centerLeft,
        child: MyWidgets.textView(text: 'Switch to new measure',
          style: MyStyles.customTextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 24,
          ),
        ),
      ),SizedBox(height: 10,),
      Expanded(
          flex: 1,
          child: ListView.builder(
              physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
              key: _listKey,
              controller: _scrollController,
              shrinkWrap: true,
              itemCount: inventoryUnitMeasureArray.length,
              itemBuilder: (BuildContext context, int index) {
                return _buildItems(context, inventoryUnitMeasureArray[index],index);
              })
      ),SizedBox(height: 10,),MyWidgets.materialButton(context,text:'Save',textColor:MyColors.black,color:MyColors.white_chinese_e0_bg,onPressed: (){

      })
    ],));}else
      return isLoading
          ? MyWidgets.buildCircularProgressIndicator()
          : Center(
        child: Text(
          'Something went wrong',
          style: TextStyle(
            fontSize: 17.0,
            color: Colors.grey,
          ),
        ),
      );
  }
  Widget _buildItems(BuildContext context, String inventoryUnitMeasureArrayItem, int index ){
    return
      ListTile(onTap:(){
        if(balanceStock!>0)
        setState(() {
         /* unitMeasure.convert(balanceStock!,inventoryUnitMeasureArray[(inventoryItem?.itemType)!], inventoryUnitMeasureArray[index]).then((unitMeasureResult)async {
            if(unitMeasureResult==0){
              selectedIndex=inventoryItem?.itemType;

              showCustomAlertDialog(context,'This conversion cannot be performed');
              return;
            }
            selectedIndex=index;
            showCustomAlertDialog(context,'${inventoryUnitMeasureArray[selectedIndex!]}: $unitMeasureResult');



          });*/



          unitMeasure.convert2(inventoryItem?.stock??0.0,inventoryItem?.costPerItemInTypeNew??0.0,inventoryUnitMeasureArray[(inventoryItem?.itemType)!], inventoryUnitMeasureArray[index]).then((value)async{
            if(value?.stockConversionResult==0){
              selectedIndex=this.inventoryItem?.itemType;
              showCustomAlertDialog(context,'This conversion cannot be performed',);
              return;
            }
            selectedIndex=index;
            showCustomCallbackAlertDialog(context:context,msg:'Do you wish to save ${inventoryUnitMeasureArray[selectedIndex!]} as default unit for the inventory item?',
            positiveClick:()async{
              Navigator.of(context).pop();
              print('conversionResult ${value?.stockConversionResult}  ${value?.perStockConversionCost}  ${value?.stockMultiplierValue}   ${inventoryUnitMeasureArray[index]}  $index');
              var inventoryItem = InventoryItem();
              inventoryItem.id=this.inventoryItem?.id;
              inventoryItem.itemType=index;
              inventoryItem.piecesInType=((this.inventoryItem!.piecesInType!)*(value?.stockMultiplierValue??0.0)).toInt();
              inventoryItem.stock=value?.stockConversionResult;
              inventoryItem.costPerItemInTypeNew=value?.perStockConversionCost;


              await inventoryItemOperations.updateInventoryItemWOnSwitchMeasure(inventoryItem,value?.stockMultiplierValue).then((value) =>

                  showCustomAlertOkActionDialog(context:context,msg:'Updates saved successfully.',positiveClick: (){
                    Navigator.of(context).pop();
                    Navigator.of(context).pop(true);
                  })
              );

            },negativeClick:(){
                  Navigator.of(context).pop();
                });







          }


          );
        });
        else
          showCustomAlertDialog(context,'Out of stock');

      },title: MyWidgets.textView(text: inventoryUnitMeasureArrayItem,textAlign: TextAlign.start),trailing: Icon(selectedIndex==index?Icons.radio_button_checked_rounded:Icons.radio_button_off_rounded,color: MyColors.black,size: 20,),
      );
  }

InventoryItem? inventoryItem;
  Future<void> _getData() async {
    await inventoryItemOperations.inventoriesItemsByInventoryItemId(widget.inventoryItemId).then((value) {
      inventoryItem=value.first;
      selectedIndex=inventoryItem?.itemType;
      balanceStock=(inventoryItem?.stock)!/(inventoryItem?.piecesInType)!;
      print('inventory item unit measure details  item_type:${inventoryItem?.itemType}  type:${inventoryUnitMeasureArray[(inventoryItem?.itemType)!]}  item_total_type: ${inventoryItem?.totalType}  stock: ${inventoryItem?.stock}  balanceStock: $balanceStock  cost: ${inventoryItem?.costPerItemInTypeNew}  ${inventoryItem?.costPerTypeNew}');
      setState(() {
        isLoading = false;
      });
    });
  }

}