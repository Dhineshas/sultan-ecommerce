import 'package:ecommerce/model/GraphProductItem.dart';
import 'package:ecommerce/utils/Constants.dart';
import 'package:ecommerce/utils/MyColors.dart';
import 'package:ecommerce/utils/Utils.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'database/product_item_operations.dart';
import 'model/ProductItem.dart';

class HomeLineChart extends StatefulWidget {


  const HomeLineChart({Key? key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => HomeLineChartState();
}

class HomeLineChartState extends State<HomeLineChart> {
  @override
  void initState() {
   _getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return
      flSpotList.isNotEmpty?Column(mainAxisSize:MainAxisSize.max,crossAxisAlignment: CrossAxisAlignment.center,children: [
        SizedBox(height: 15,),
        MyWidgets.container(color:MyColors.transparent,paddingR:15,height : 200,child: LineChart(
          sampleData2,
          swapAnimationDuration: const Duration(milliseconds: 250),
        ))
      ],)
          :MyWidgets.container();
  }



  LineChartData get sampleData2 => LineChartData(
    lineTouchData: lineTouchData2,
    gridData: gridData,
    titlesData: titlesData2,
    borderData: borderData,
    lineBarsData: lineBarsData2,
    minX: 0,
    maxX: 11,
    maxY: 5,
    minY: 0,
  );

  LineTouchData get lineTouchData2 => LineTouchData(
    enabled: false,
  );

  FlTitlesData get titlesData2 => FlTitlesData(
    bottomTitles: AxisTitles(
      sideTitles: bottomTitles,
    ),
    rightTitles: AxisTitles(
      sideTitles: SideTitles(showTitles: false),
    ),
    topTitles: AxisTitles(
      sideTitles: SideTitles(showTitles: false),
    ),
    leftTitles: AxisTitles(
      sideTitles: leftTitles(),
    ),
  );

  List<LineChartBarData> get lineBarsData2 => [
    // lineChartBarData2_1,
    //lineChartBarData2_2,
    lineChartBarData2_3,
  ];

  Widget leftTitleWidgets(double value, TitleMeta meta) {
    print('y_values $value');
    const style = TextStyle(
      color: Color(0xff75729e),
      fontWeight: FontWeight.bold,
      fontSize: 14,
    );
    String text;
    switch (value.toInt()) {
      case 0:
        text = '0%';
        break;
      case 1:
        text = '20%';
        break;
      case 2:
        text = '40%';
        break;
      case 3:
        text = '60%';
        break;
      case 4:
        text = '80%';
        break;
      case 5:
        text = '100%';
        break;
      default:
        return Container();
    }

    return Text(text, style: style, textAlign: TextAlign.center);
  }

  SideTitles leftTitles() => SideTitles(
    getTitlesWidget: leftTitleWidgets,
    showTitles: true,
    interval: 1,
    reservedSize: 40,
  );

  Widget bottomTitleWidgets(double value, TitleMeta meta) {
    print('x_values $value');
    const style = TextStyle(
      color: Color(0xff72719b),
      fontWeight: FontWeight.bold,
      fontSize: 16,
    );
    Widget text;

    text =  Text('${value.toInt()+1}', style: style);

    return SideTitleWidget(
      axisSide: meta.axisSide,
      space: 10,
      child: text,
    );
  }

  SideTitles get bottomTitles => SideTitles(
    showTitles: true,
    reservedSize: 32,
    interval: 1,
    getTitlesWidget: bottomTitleWidgets,
  );


  FlGridData get gridData => FlGridData(show: true,);


  FlBorderData get borderData =>  FlBorderData(
  show: true,
  border: Border.all(color: MyColors.grey_607, width: 1));

  LineChartBarData get lineChartBarData2_3 => LineChartBarData(
    isCurved: true,
    curveSmoothness: 0,
    color: MyColors.white_chinese_e0_bg,
    barWidth: 2,
    isStrokeCapRound: true,
    dotData: FlDotData(show: true),
    belowBarData: BarAreaData(show: false),
    spots:flSpotList
  );

  var productItemOperations = ProductItemOperations();

  var flSpotList = <FlSpot>[];

  Future<void> _getData() async {
    await MyPref.getProject().then((project) async {
      if (project != null) {
        await productItemOperations
            .productItemsGrossAndNetInOrdersByProjectId(
            project.id)
            .then((value) {
          if(value.isNotEmpty)
            setState(() {


              flSpotList= List<FlSpot>.generate(12, (i) => FlSpot(i.toDouble(), 0.0));

              value.forEach((element) {
                print('order_profit_products ${element.name} ${element.orderGrossTotal}  ${element.orderNetTotal}   ${element.orderQuantityTotal}  ${element.orderTotalProductCost}  ${element.orderTotalProductProfit}  ${element.orderMonth}  ${element.orderYear}');
                if(element.orderNetTotal!>0){
                  var profitPercentage=(element.orderTotalProductProfit!/element.orderNetTotal!)*100;
                  var profitPercentageToDecimal=(profitPercentage*5)/100;
                  flSpotList[element.orderMonth!.toInt() - 1] =FlSpot(element.orderMonth!.toInt() - 1,profitPercentageToDecimal);
                }
              });


            });



        });
      } else
        showCustomAlertDialog(
          context,
          'Please create a project ',
        );
    });
  }
}
