package com.technologylab.expresspos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import com.technologylab.expresspos.databinding.ActivitySecondBinding
import com.technologylab.expresspos.databinding.ActivityWebViewBinding
import io.flutter.embedding.android.FlutterActivity

class WebViewActivity : FlutterActivity() {
    private lateinit var binding: ActivityWebViewBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWebViewBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.back.setOnClickListener {
            onBackPressed()
        }
        val isTerms=intent.getBooleanExtra("terms",true)
        binding.textWebViewTitle.text=if (isTerms)"Terms And Conditions" else "Privacy Policy"

        binding.webView.settings.javaScriptEnabled = true
        binding.webView.settings.loadWithOverviewMode = true
        binding.webView.webChromeClient = WebChromeClient()
        binding.webView.loadUrl(if (isTerms)"https://technologylab.global/express_pos/termsandconditions.html" else "https://technologylab.global/express_pos/privacypolicy.html")

    }
}