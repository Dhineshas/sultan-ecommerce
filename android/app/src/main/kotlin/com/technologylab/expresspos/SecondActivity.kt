package com.technologylab.expresspos


import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.billingclient.api.*
import com.android.billingclient.api.QueryProductDetailsParams.Product
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.crashlytics.internal.model.ImmutableList
import com.technologylab.expresspos.databinding.ActivitySecondBinding
import io.flutter.embedding.android.FlutterActivity


class SecondActivity : FlutterActivity() {
    private lateinit var binding: ActivitySecondBinding
    private var billingClient: BillingClient? = null
    var productDetailsArrayList = arrayListOf<ProductDetails>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySecondBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        //setContentView(R.layout.activity_second)
        var acitveSubsciption=intent.getIntExtra("activeSub",0)
        binding.back.setOnClickListener {
            onBackPressed()
        }

        binding.txtTerms.setOnClickListener {
            gotoWebView()
        }
        binding.txtPolicy.setOnClickListener {
            gotoWebView(false)
        }
        binding.txtAllSubscription.setOnClickListener {
            openPlaystoreAccount()
        }

        with(binding.recyclerView) {
            setHasFixedSize(true)
            val inAppProductListTypeLayoutManager =
                LinearLayoutManager(this@SecondActivity, RecyclerView.HORIZONTAL, false)
            layoutManager = inAppProductListTypeLayoutManager

            adapter = ProductTypeAdapter(productDetailsArrayList) {
                println("selectedSubscription  ${acitveSubsciption}  ${productDetailsArrayList[it].productId}")

                if(acitveSubsciption==2){
                    showAlertDialog(true)

                }else if(acitveSubsciption==3&&productDetailsArrayList[it].productId=="com.expresspos.standard.non_renewing"){
                    showAlertDialog(false)
                }
                else
                  launchBillingFlow(productDetailsArrayList[it])
            }
            //adapter?.notifyDataSetChanged()

        }

        response.observe(this, { response ->

            binding.recyclerView.adapter?.notifyDataSetChanged()
        })
        //billingClient = BillingClient.newBuilder(this).enablePendingPurchases().setListener(this).build()
        setUpBillingClient()

    }

    override fun onDestroy() {
        terminateBillingConnection()
        super.onDestroy()

    }

    private fun showAlertDialog(isPremium: Boolean) {
        AlertDialog.Builder(this.context,R.style.myDialog)
            .setMessage(if (isPremium)"You are already subscribed to the premium plan. You can cancel the subscription from Playstore at any time. Please ignore if you already cancelled." else "You are already subscribed to the current plan.")
            .setPositiveButton("OK", null)
            .show()
    }
    private fun setUpBillingClient() {
        billingClient = BillingClient.newBuilder(this)
            .setListener(purchaseUpdateListener)
            .enablePendingPurchases()
            .build()
        startConnection()
    }

    private val purchaseUpdateListener = PurchasesUpdatedListener { billingResult, purchases ->
        println("purchaseUpdateListener  ${purchases}")
        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && !purchases.isNullOrEmpty()) {
            purchases.forEach {
                acknowledgePurchases(it)
            }
        }

    }

    private fun startConnection() {
        billingClient?.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                    Log.v("TAG_INAPP", "Setup Billing Done")
                    println("Setup Billing Done")
                    // The BillingClient is ready. You can query purchases here.

                    //queryPurchases()
                    queryAvailableProducts()

                }
            }

            override fun onBillingServiceDisconnected() {
                Log.v("TAG_INAPP", "Billing client Disconnected")
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        })
    }


    val response = MutableLiveData<Int>()
    private fun queryAvailableProducts() {

        productDetailsArrayList.clear()
       /* val inAppProductList = ImmutableList.from(
            Product.newBuilder()
                .setProductId("com.expresspos.projectcreation.non_consumable")
                .setProductType(BillingClient.ProductType.INAPP)
                .build()
        )*/

        val subProductList = ImmutableList.from(
            /*Product.newBuilder()
                .setProductId("com.exprepos.project.n_r_s")
                .setProductType(BillingClient.ProductType.SUBS)
                .build(),
            Product.newBuilder()
                .setProductId("com.exprepos.project.n_r_s_2")
                .setProductType(BillingClient.ProductType.SUBS)
                .build(),
            Product.newBuilder()
                .setProductId("com.exprepos.project.n_r_s_3")
                .setProductType(BillingClient.ProductType.SUBS)
                .build(),*/
            Product.newBuilder()
                .setProductId("com.expresspos.standard.non_renewing")
                .setProductType(BillingClient.ProductType.SUBS)
                .build(),
            Product.newBuilder()
                .setProductId("com.expresspos.premium.auto_renewing")
                .setProductType(BillingClient.ProductType.SUBS)
                .build()
        )

      /*  val queryInAppProductDetailsParams =
            QueryProductDetailsParams.newBuilder()
                .setProductList(inAppProductList)
                .build()*/

        val querySubProductDetailsParams =
            QueryProductDetailsParams.newBuilder()
                .setProductList(subProductList)
                .build()


        /*billingClient?.queryProductDetailsAsync(queryInAppProductDetailsParams) { billingResult, productDetailsList ->

            // process returned productDetailsList
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && !productDetailsList.isNullOrEmpty()) {

                productDetailsArrayList.addAll(productDetailsList)
                response.postValue(billingResult.responseCode)
                println("productDetailsListvvv :${productDetailsArrayList.size}")
                println("productDetailsListvvvInAppProduct :${productDetailsArrayList}")
            }
        }*/


        billingClient?.queryProductDetailsAsync(querySubProductDetailsParams) { billingResult,
                                                                                productDetailsList ->
            // check billingResult
            // process returned productDetailsList
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && !productDetailsList.isNullOrEmpty()) {
                productDetailsArrayList.addAll(productDetailsList)
                response.postValue(billingResult.responseCode)
                //binding.recyclerView.adapter?.notifyDataSetChanged()
                //println("productDetailsListvvvSubProduct :${productDetailsArrayList}")

                productDetailsArrayList.forEach {
                    println("productDetailsListvvvSubProduct :${it.productId}")
                }
                /*for (skuDetails in productDetailsList) {
                    println("skuDetailsList : ${skuDetails} ")


                    //Log.v("TAG_INAPP","skuDetailsList : ${skuDetailsList}")
                    //This list should contain the products added above

                }*/


            }
        }


    }

    fun queryPurchases() {

        // Query for existing subscription products that have been purchased.
        billingClient?.queryPurchasesAsync(
            QueryPurchasesParams.newBuilder().setProductType(BillingClient.ProductType.SUBS).build()
        ) { billingResult, purchaseList ->
            println("queryPurchasesSize ${purchaseList.size}")
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && !purchaseList.isNullOrEmpty()) {
                println("queryPurchases $purchaseList")
            }
        }

        billingClient?.queryPurchasesAsync(
            QueryPurchasesParams.newBuilder().setProductType(BillingClient.ProductType.INAPP)
                .build()
        ) { billingResult, purchaseList ->
            println("queryPurchasesSize ${purchaseList.size}")
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && !purchaseList.isNullOrEmpty()) {

                println("queryPurchases $purchaseList")
            }
        }
    }

    fun launchBillingFlow(productDetails: ProductDetails) {

        if (billingClient?.isReady == true) {

            val productDetailsParamsList = listOf(
                BillingFlowParams.ProductDetailsParams.newBuilder()
                    // retrieve a value for "productDetails" by calling queryProductDetailsAsync()
                    .setProductDetails(productDetails)
                    // to get an offer token, call ProductDetails.subscriptionOfferDetails()
                    // for a list of offers that are available to the user
                    .setOfferToken(
                        productDetails.subscriptionOfferDetails?.first()?.offerToken ?: ""
                    )
                    .build()
            )

            val billingFlowParams = BillingFlowParams.newBuilder()
                .setProductDetailsParamsList(productDetailsParamsList)
                .build()
            billingClient?.launchBillingFlow(this, billingFlowParams)
            return

        }
        println("launchBillingFlow: BillingClient is not ready")

    }

    private fun acknowledgePurchases(purchase: Purchase?) {
        purchase?.let {
            if (!it.isAcknowledged) {
                val params = AcknowledgePurchaseParams.newBuilder()
                    .setPurchaseToken(it.purchaseToken)
                    .build()

                billingClient?.acknowledgePurchase(params) { billingResult ->
                    println("purchasesAcknowledge ${it.developerPayload}")
                    if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && it.purchaseState == Purchase.PurchaseState.PURCHASED
                    ) {
                        println("purchasesAcknowledge PURCHASED")
                        setResult(
                            Activity.RESULT_OK,
                            intent.putExtra("PURCHASED_DATA", it.originalJson)
                        )

                    }
                    finish()
                }

                /*val consumeParams =
                    ConsumeParams.newBuilder()
                        .setPurchaseToken(purchase.purchaseToken)
                        .build()

                billingClient.consumeAsync(consumeParams){billingResult, b ->
                    println("purchasesAcknowledge   ${billingResult}   ${it.signature}")
                }*/
            }
        }
    }



    private fun openPlaystoreAccount() {
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/account/subscriptions")));
        } catch (e: ActivityNotFoundException) {

            e.printStackTrace()
        }
    }
private fun gotoWebView(isTerms: Boolean=true){
    val intent= Intent(this, WebViewActivity::class.java)
    intent.putExtra("terms",isTerms)
    startActivity(intent)
}
    private fun terminateBillingConnection() {
        println("Terminating connection")
        billingClient?.endConnection()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(Activity.RESULT_OK)
        finish()
    }
}
