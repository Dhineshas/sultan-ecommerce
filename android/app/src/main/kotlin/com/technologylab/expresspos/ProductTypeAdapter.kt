package com.technologylab.expresspos

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.billingclient.api.ProductDetails
import com.technologylab.expresspos.databinding.ItemInAppProductsTypeAdapterBinding
import java.util.*

class ProductTypeAdapter(
    val productDetailsArrayList: ArrayList<ProductDetails>,
    val function: (Int) -> Unit
) : RecyclerView.Adapter<ProductTypeAdapter.VH>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val binding = ItemInAppProductsTypeAdapterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VH(binding)
    }


    override fun getItemCount(): Int {
        return productDetailsArrayList.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(position)
    }
    inner class VH(val binding: ItemInAppProductsTypeAdapterBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
                productDetailsArrayList[position].let {
                    if (it.productId.contains("com.expresspos.premium.auto_renewing")){
                        binding.txtPlan.text=binding.root.resources.getString(R.string.premium)
                        binding.txtRecommended.visibility=View.VISIBLE
                        binding.txtProjectCount.text=binding.root.resources.getString(R.string.create_5_projects)
                        binding.txtGraphAccess.text=binding.root.resources.getString(R.string.unlimited_access_to_graph)
                        binding.txtReportAccess.text=binding.root.resources.getString(R.string.unlimited_access_to_reports)
                        binding.txtInvoiceAccess.text=binding.root.resources.getString(R.string.generate_invoice)
                        binding.imgProjectCount.setImageDrawable(binding.root.resources.getDrawable(R.mipmap.check_green))
                        binding.imgGraphAccess.setImageDrawable(binding.root.resources.getDrawable(R.mipmap.check_green))
                        binding.imgReportAccess.setImageDrawable(binding.root.resources.getDrawable(R.mipmap.check_green))
                        binding.imgInvoiceAccess.setImageDrawable(binding.root.resources.getDrawable(R.mipmap.check_green))
                    }else{
                        binding.txtRecommended.visibility=View.INVISIBLE
                        binding.txtPlan.text=binding.root.resources.getString(R.string.standard)
                        binding.txtProjectCount.text=binding.root.resources.getString(R.string.create_1_projects)
                        binding.txtGraphAccess.text=binding.root.resources.getString(R.string.no_access_to_graph)
                        binding.txtReportAccess.text=binding.root.resources.getString(R.string.no_access_to_reports)
                        binding.txtInvoiceAccess.text=binding.root.resources.getString(R.string.generate_invoice)
                        binding.imgProjectCount.setImageDrawable(binding.root.resources.getDrawable(R.mipmap.check_green))
                        binding.imgGraphAccess.setImageDrawable(binding.root.resources.getDrawable(R.mipmap.cross_red))
                        binding.imgReportAccess.setImageDrawable(binding.root.resources.getDrawable(R.mipmap.cross_red))
                        binding.imgInvoiceAccess.setImageDrawable(binding.root.resources.getDrawable(R.mipmap.check_green))
                    }
                    binding.txtProductTitle.text=it.title
                    binding.txtProductPrice.text=if (it.productType=="inapp") it.oneTimePurchaseOfferDetails?.formattedPrice?:"" else it.subscriptionOfferDetails?.first()?.pricingPhases?.pricingPhaseList?.first()?.formattedPrice?:""
                    binding.root.setOnClickListener {
                        function.invoke(position)
                    }
                }
        }
    }

}
