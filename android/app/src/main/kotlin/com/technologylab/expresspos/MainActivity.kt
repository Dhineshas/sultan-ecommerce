package com.technologylab.expresspos

import android.app.Activity
import android.content.Intent
import android.util.Log
import androidx.annotation.NonNull
import com.android.billingclient.api.*
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel



class MainActivity: FlutterActivity() {

    private val CHANNEL = "samples.flutter.dev/iap"
    private val REQUEST_CODE=101;
    private lateinit var result:MethodChannel.Result
    private var billingClient: BillingClient? =null
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler {
            // This method is invoked on the main thread.
                call, result ->
            when (call.method) {
                "getInAppPurchase" -> {
                    /*val batteryLevel = getBatteryLevel()

                        if (batteryLevel != -1) {
                            result.success(batteryLevel)
                        } else {
                            result.error("UNAVAILABLE", "Battery level not available.", null)
                        }*/
                    this.result=result

                    val acitveSubsciption = call.argument<Int>("activeSub")

                    val intent=Intent(this, SecondActivity::class.java)
                    intent.putExtra("activeSub",acitveSubsciption)
                    startActivityForResult(intent,REQUEST_CODE)
                    //result.success(90)
                }
                "activeInAppPurchases" -> {

                    this.result=result
                    setUpBillingClient()
                }
                else -> {
                    result.notImplemented()
                }
            }
        }
    }
    private fun setUpBillingClient() {
        billingClient = BillingClient.newBuilder(this)
            .setListener(purchaseUpdateListener)
            .enablePendingPurchases()
            .build()
        startConnection()
    }
    private val purchaseUpdateListener =  PurchasesUpdatedListener { billingResult, purchases ->
        println("purchaseUpdateListener  ${purchases}")

    }
    private fun startConnection() {
        billingClient?.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode ==  BillingClient.BillingResponseCode.OK) {
                    Log.v("TAG_INAPP","Setup Billing Done")
                    // The BillingClient is ready. You can query purchases here.

                    queryPurchases()


                }
            }
            override fun onBillingServiceDisconnected() {
                Log.v("TAG_INAPP","Billing client Disconnected")
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        })
    }
    var productArrayList = arrayListOf<String>()

    fun queryPurchases() {
        productArrayList.clear()
        // Query for existing subscription products that have been purchased.
        billingClient?.queryPurchasesAsync(
            QueryPurchasesParams.newBuilder().setProductType(BillingClient.ProductType.SUBS).build()
        ) { subsBillingResult, subsPurchaseList ->
            println("queryPurchasesSize ${subsPurchaseList.size}")
            if (subsBillingResult.responseCode == BillingClient.BillingResponseCode.OK&& !subsPurchaseList.isNullOrEmpty()) {

                productArrayList.addAll(subsPurchaseList.map { it.originalJson })
                println("queryPurchasesgggg ${ productArrayList}]")
            }




            billingClient?.queryPurchasesAsync(
                QueryPurchasesParams.newBuilder().setProductType(BillingClient.ProductType.INAPP).build()
            ) { inappBillingResult, inappPurchaseList ->
                println("queryPurchasesSize ${inappPurchaseList.size}")
                if (inappBillingResult.responseCode == BillingClient.BillingResponseCode.OK&& !inappPurchaseList.isNullOrEmpty()) {
                    productArrayList.addAll(inappPurchaseList.map { it.originalJson })
                    println("queryPurchases $inappPurchaseList")
                }

                result?.success(productArrayList.toString())
            }
        }


    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
           println("DATAPURCHASED ${data?.getStringExtra("PURCHASED_DATA")}")
            if (data!=null&&data.hasExtra("PURCHASED_DATA")){
            val purchaseResult:String?=data?.getStringExtra("PURCHASED_DATA")

            result?.success(purchaseResult)
            return
            }else{
                //result.notImplemented()
                result.success("")
            }
        }
    }
    private fun terminateBillingConnection() {
        println("Terminating connection")
        billingClient?.endConnection()
    }


    override fun onDestroy() {
        terminateBillingConnection()
        super.onDestroy()

    }


}
